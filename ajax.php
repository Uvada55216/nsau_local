<?php
// version: 3.5.10
// date: 2014-03-01
global $Engine, $Auth, $EE;

if(isset($MODULE_OUTPUT['scripts_mode'])) {
	switch($MODULE_OUTPUT['scripts_mode']) {
		case 'table_generator': {
			$EE["head_extra3"][] = <<<END
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$('table.nsau_table>tbody>tr').each(function(e) {
				var row = $(this);
				var code = $(this).find('td:first').text();
				var request = {'module': 5, 'mode': 'json', 'params': 'table_generator', 'data': {'code': code} };
				var x;
				$.ajax({
					'url': '/ajax_mode/',
					dataType: 'json',
					data: request,
					success: function(json) {

						$(row).find('td:eq(0)').attr('itemprop', 'specialVacant');
						$(row).find('td:eq(1)').attr('itemprop', 'nameProgVacant');


						if($(row).find('td:eq(2)>p').text()  == '������ �����������') {
							$(row).find('td:eq(2)').attr('itemprop', 'bachelorVacant');
						}
				
						if($(row).find('td:eq(2)>p').text() == '������ ������������') {
							$(row).find('td:eq(2)').attr('itemprop', 'magistracyVacant');
						}			
				
						if($(row).find('td:eq(2)>p').text()  == '������ �����������') {
							$(row).find('td:eq(2)').attr('itemprop', 'specialityVacant');
						}						





						$(row).find('td:eq(3)').attr('itemprop', '');
						$(row).find('td:eq(4)').attr('itemprop', '');
						$(row).find('td:eq(5)').attr('itemprop', 'numberBFVacant');
						$(row).find('td:eq(6)').attr('itemprop', 'numberBRVacant');
						$(row).find('td:eq(7)').attr('itemprop', 'numberBMVacant');
						$(row).find('td:eq(8)').attr('itemprop', 'numberPVacant');
						// $("<td itemprop='eduLevel'>"+json+"</td>").insertAfter($(row).find('td[itemprop=eduName]'));
					}
				});			
			});
		});
	</script>
END;
		}
			break;






		case 'exam_ege': {
			$EE["head_extra3"][] = <<<END
	<script src="/themes/styles/bootstrap/js/jquery.min.js"></script> 
    <script src="/themes/styles/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function($) 
		{
			//$("#modal_exam_edit").modal('show');

			/*������� ��������� ���� ���������� ��������*/
			jQuery('.add_exam').click (function() 
			{
				$("#modal_summary_info").modal('show');
			});

			/*���������� ��������*/
			jQuery('.add_exam_modal').click (function() 
			{
				var nomer = $("#modal_add_nomer").val();
				var predmet = $("#modal_add_predmet").val();
				var spec = $("#modal_add_spec").val();
				var ball_vyz = $("#modal_add_ball_vyz").val();
				var ball_mins = $("#modal_add_ball_mins").val();
				var prikaz = $("#modal_add_prikaz").val();
				var request = {'module': 5, 'mode': 'json', 'params': 'exam_ege_add', 'data': {'nomer': nomer, 'predmet': predmet, 'spec': spec, 'ball_vyz': ball_vyz, 'ball_mins': ball_mins, 'prikaz': prikaz} };			
                $.ajax(
                {
                    type: 'POST',
                    dataType: 'json',
                    url: '/ajax_mode/',
                    data: request,
                    success: function( data )
                    {
                        $(".status").html("");
                        if (data!="error2" && data!="error3" && data!="error4") 
                        {
                        	$("#modal_summary_info").modal('hide');
                        	$('.status').html("<div class='alert alert-success'> ������� ������� �������� </div>");
							$('#exam_table tr:last').after('<tr><td>'+nomer+'</td><td>'+predmet+'</td><td>'+spec+'</td><td>'+ball_vyz+'</td><td>'+ball_mins+'</td><td>'+prikaz+'</td><td><button class="btn btn-nsau btn-xs delete_exam" type="button" id="'+data+'"><span class="glyphicon glyphicon-trash"></span></button></td></tr>');
                        	
                        }
                        if (data=="error2") 
                        {
                        	$("#modal_summary_info").modal('hide');
                        	$('.status').html("<div class='alert alert-danger'> ������ ���������� �������� </div>");
                        }
                        if (data=="error3") 
                        {
                        	$("#modal_summary_info").modal('hide');
                        	$('.status').html("<div class='alert alert-danger'> � ��� ��� ���� ��� �������� </div>");
                        }  
                        if (data=="error4") 
                        {
                        	$("#modal_summary_info").modal('hide');
                        	$('.status').html("<div class='alert alert-danger'> ���������� �� ��� ���� </div>");
                        }             
                    }
                });	
			});

			/*������� ���������� ��������*/
			jQuery('.close_add').click (function() 
			{
				$("#modal_summary_info").modal('hide');
			});

			/*�������� ��������*/
			jQuery('.delete_exam').click (function() 
			{
				
	        	if(confirm('�� ������������� ������ ������� ���� �������?')) 
	        	{
	                var id = $(this).attr('id');
	                var request = {'module': 5, 'mode': 'json', 'params': 'exam_ege_delete', 'data': {'id': id} };			
	                $.ajax(
	                {
	                    type: 'POST',
	                    dataType: 'json',
	                    url: '/ajax_mode/',
	                    data: request,
	                    success: function( data )
	                    {
	                        $(".status").html("");
	                        if (data==1) 
	                        {
	                        	$('.status').html("<div class='alert alert-success'> ������� ������� ������ </div>");
								$('#exam_'+id).remove();
	                        }
	                        if (data==2) 
	                        {
	                        	$('.status').html("<div class='alert alert-danger'> ������ �������� �������� </div>");
	                        }
	                        if (data==3) 
	                        {
	                        	$('.status').html("<div class='alert alert-danger'> � ��� ��� ���� ��� �������� </div>");
	                        }                   
	                    }
	                });
	            }
			});

			/*�������������� ��������*/
			jQuery('.edit_exam').click (function() 
			{
				$("#modal_exam_edit").modal('show');
				var id = $(this).attr('id');
				var request = {'module': 5, 'mode': 'json', 'params': 'exam_get', 'data': {'id': id} };			
                $.ajax(
                {
                    type: 'POST',
                    dataType: 'json',
                    url: '/ajax_mode/',
                    data: request,
                    success: function( data )
                    {
						console.log(data);
						$("#modal_edit_nomer").val(data.n_pp);     
						$("#modal_edit_predmet").val(data.name);
						$("#modal_edit_ball_vyz").val(data.min_vyz);
						$("#modal_edit_ball_mins").val(data.min_mins); 
						$("#modal_edit_prikaz").val(data.prikaz);    
						$("#modal_edit_id").val(data.id);  
                    }
                });	
			});

			/*������� �������������� ��������*/
			jQuery('.close_edit').click (function() 
			{
				$("#modal_exam_edit").modal('hide');
			});

			/*�������������� ��������*/
			jQuery('.edit_exam_save').click (function() 
			{
				var id = $("#modal_edit_id").val(); 
				var nomer = $("#modal_edit_nomer").val();
				var predmet = $("#modal_edit_predmet").val();
				var spec = $("#modal_edit_spec").val();
				var ball_vyz = $("#modal_edit_ball_vyz").val();
				var ball_mins = $("#modal_edit_ball_mins").val();
				var prikaz = $("#modal_edit_prikaz").val();
				var request = {'module': 5, 'mode': 'json', 'params': 'exam_ege_edit', 'data': {'id': id, 'nomer': nomer, 'predmet': predmet, 'spec': spec, 'ball_vyz': ball_vyz, 'ball_mins': ball_mins, 'prikaz': prikaz} };			
                $.ajax(
                {
                    type: 'POST',
                    dataType: 'json',
                    url: '/ajax_mode/',
                    data: request,
                    success: function( data )
                    {

                        $(".status").html("");
                        if (data!="error2" && data!="error3" && data!="error4") 
                        {
                        	$('#exam_'+id).remove();
                        	$("#modal_exam_edit").modal('hide');
                        	$('.status').html("<div class='alert alert-success'> ������� ������� �������������� </div>");
							$('#exam_table tr:last').after('<tr><td>'+nomer+'</td><td>'+predmet+'</td><td>'+spec+'</td><td>'+ball_vyz+'</td><td>'+ball_mins+'</td><td>'+prikaz+'</td><td><button class="btn btn-nsau btn-xs delete_exam" type="button" id="'+id+'"><span class="glyphicon glyphicon-trash"></span></button><button class="btn btn-nsau btn-xs edit_exam" id="'+id+'" type="button" style="margin-top: 2px;"><span class="glyphicon glyphicon-pencil"></span></button></td></tr>');
                        	location.reload();
                        }
                        if (data=="error2") 
                        {
                        	$("#modal_summary_info").modal('hide');
                        	$('.status').html("<div class='alert alert-danger'> ������ �������������� �������� </div>");
                        }
                        if (data=="error3") 
                        {
                        	$("#modal_summary_info").modal('hide');
                        	$('.status').html("<div class='alert alert-danger'> � ��� ��� ���� ��� �������������� </div>");
                        }  
                        if (data=="error4") 
                        {
                        	$("#modal_summary_info").modal('hide');
                        	$('.status').html("<div class='alert alert-danger'> ���������� �� ��� ���� </div>");
                        }             
                    }
                });	
			});



		});
	</script>
END;
		}
			break;









		case 'timetable_subjects': {
			$EE["head_extra3"][] = <<<END
	<script type="text/javascript">
					function ajax_load_data(data, params)
			{
				return new Promise(function(resolve) {
				var request = {'module': '5', 'mode': 'json', 'params': params, 'data': data};
				jQuery.ajax({
					type: 'POST',
					url: '/ajax_mode/',
					dataType: 'json',
					data: request,
					success: resolve
				});
				});
			}
		jQuery(document).ready(function($) {
			subj_exists = function() {
				var i = 1;
				$('.dep'+$('#department_id').val()).each(
					function() {
						if (this.text.toLowerCase() == $('#subject_name').val().toLowerCase()) {
							alert('�� ���� ������� ��� ���������� ���������� � ����� ���������');
							i = 0;
						}
					}
				);
				return i;
			};

			TimetableSubjects = function(moduleId) {
				if($('.subj_list .delet_subj').length) {
					$('.subj_list .delet_subj').click (function() {
						var subj_id = $(this).attr('href');
						subj_id = subj_id.split('/');
						if(!confirm('�� �������, ��� ������ ������� ����������? ')) return false;
						if(subj_id[0] == 'delete') {
							subj_id = subj_id[1];
							var request = {'module': moduleId, 'mode': 'html_code', 'params': 'ajax_delete_subj', 'data': {'subj_id': subj_id} };
							$.ajax({
								type: 'POST',
								url: '/ajax_mode/',
								data: request,
								success: function( data ) {
									if(!(!data || !data.documentElement)) {
										var rootNodeName = data.documentElement.nodeName;
										if(rootNodeName != 'parsererror') {
											xmlDocElem = data.documentElement;
											if(rootNodeName == 'del_subj') {
												var process = xmlDocElem.firstChild.data;
												var error_msg = xmlDocElem.getElementsByTagName('error_msg');
												if(process == 1) {
													$('#subj_item_'+subj_id).remove();
												} else {
													if(error_msg.length > 0) {
														error_msg = error_msg[0].firstChild.data;
														alert('������ ��������. ' + error_msg);
													}
													else {
														alert('������ ��������. �� ���������� ������� ���������.');
													}
												};
											};
										};
									};
								}
							});
						};
						return false;
					});
					$('.subj_list .hide_subj, .show_subj').live('click', function(e) {
						var subj_id = $(this).attr('href');
						subj_id = subj_id.split('/');
						if(subj_id[0] == 'hide') {
							subj_id = subj_id[1];
							var request = {'module': moduleId, 'mode': 'json', 'params': 'ajax_hide_subj', 'data': {'subj_id': subj_id, 'mode': 'hide'} };
							$.ajax({
								type: 'POST',
								url: '/ajax_mode/',
								data: request,
								success: function( data ) {
									$('#subj_item_'+subj_id).children('a').css("color", "gray");
									$('#subj_item_'+subj_id).find("a.hide_subj")
																					.removeClass("hide_subj")
																					.addClass("show_subj")
																					.attr("href", "show/"+subj_id+"/")
																					.text("��������");

								}
							});
						};
						if(subj_id[0] == 'show') {
							subj_id = subj_id[1];
							var request = {'module': moduleId, 'mode': 'json', 'params': 'ajax_hide_subj', 'data': {'subj_id': subj_id, 'mode': 'show'} };
							$.ajax({
								type: 'POST',
								url: '/ajax_mode/',
								data: request,
								success: function( data ) {
									$('#subj_item_'+subj_id).children('a').css("color", "");
									$('#subj_item_'+subj_id).find("a.show_subj")
																					.removeClass("show_subj")
																					.addClass("hide_subj")
																					.attr("href", "hide/"+subj_id+"/")
																					.text("������");

								}
							});

						}
						return false;
					});
				};
			};
///////////////////////////////////////////////////////
//������� �������� � ����� ���������� �� ������

//�������� ���
			$("#move_subj_attach").live("click", function(){
				var to_id = $(".to_select").val();
				var to_name = $(".to_select option:selected").text();
				var from_id  = $(".from_id").attr('class').split(" ");
				from_id = from_id[1];
				if(from_id != to_id) {
					$(".move_log").toggleClass("hidden");
					$("#save_button").toggleClass("hidden");
					$(".to_subj_list, .from_subj_list").remove();
					$(".cur_table_to, .cur_table_from, .cur_table_from_izop, .cur_table_to_izop").remove();
					$(".file_table_from, .file_table_to").remove();
					var data = {'from_id': from_id, 'to_id': to_id, 'mode': 'show'};
					//ajax_load_data(data, "ajax_move_subject").then(function(response) {
					var request = {'module': '5', 'mode': 'json', 'params': 'ajax_move_subject', 'data': data};
					$.ajax({
						type: 'POST',
						url: '/ajax_mode/',
						dataType: 'json',
						data: request,
						success: function(response) {

								$(".move_tmp").clone().removeClass("move_tmp").addClass("to_subj_list").css("color", "red").html("<strong>"+to_name+":</strong><br>").appendTo(".move_log");
								$(".move_tmp").clone().removeClass("move_tmp").addClass("to_subj_list").html("<strong>�����:</strong><br>").appendTo(".move_log");
///////////////////					///////����
//�����
								if(!response["to_files"]) $(".move_tmp").clone().removeClass("move_tmp").addClass("to_subj_list").html("������ ���:<br>").appendTo(".move_log");
								else {
									var cur_table = $(".tmp_table_file").clone().removeClass("tmp_table_file hidden").addClass("file_table_to");
									cur_table.appendTo($(".move_log"));
									for(i in response["to_files"]) {
										var row  = $(".file_table_to .tmp_table_tr").clone().removeClass("tmp_table_tr hidden");
										row.find(".td_name").html(response["to_files"][i]["name"]);
										row.find(".td_subj").html(response["to_files"][i]["subj_name"]);
										row.find(".td_spec").html(response["to_files"][i]["spec_name"]);
										row.find(".td_code").html(response["to_files"][i]["spec_code"]);
										row.find(".td_user").html(response["to_files"][i]["username"]);
										row.appendTo($(".file_table_to"));
									}
								}
//�����
								$(".move_tmp").clone().removeClass("move_tmp").addClass("to_subj_list").html("<strong>������� �����:</strong><br>").appendTo(".move_log");
								if(response["to_curr"])
								{
									var cur_table = $(".tmp_table").clone().removeClass("tmp_table hidden").addClass("cur_table_to");
									cur_table.appendTo($(".move_log"));
									for(i in response["to_curr"]) {
										var row  = $(".cur_table_to .tmp_table_tr").clone().removeClass("tmp_table_tr hidden");
										row.find(".td_spec").html(response["to_curr"][i]["spec_name"]);
										row.find(".td_subj").html(response["to_curr"][i]["subj_name"]);
										row.find(".td_group").html(response["to_curr"][i]["group_name"]);
										row.find(".td_code").html(response["to_curr"][i]["spec_code"]);
										row.find(".td_direc").html(response["to_curr"][i]["spec_direction"]);
										row.appendTo($(".cur_table_to"));
									}
								} else $(".move_tmp").clone().removeClass("move_tmp").addClass("to_subj_list").html("������� ������ ���.<br>").appendTo(".move_log");
// ����� ����
								$(".move_tmp").clone().removeClass("move_tmp").addClass("to_subj_list").html("<strong>������� ����� ����:</strong><br>").appendTo(".move_log");
								if(response["to_curr_izop"])
								{
									var cur_table = $(".tmp_table").clone().removeClass("tmp_table hidden").addClass("cur_table_to_izop");
									cur_table.find(".tmp_th").html("�������");
									cur_table.appendTo($(".move_log"));
									for(i in response["to_curr_izop"]) {
										var row  = $(".cur_table_to_izop .tmp_table_tr").clone().removeClass("tmp_table_tr hidden");
										row.find(".td_spec").html(response["to_curr_izop"][i]["spec_name"]);
										row.find(".td_subj").html(response["to_curr_izop"][i]["subj_name"]);
										row.find(".td_group").html(response["to_curr_izop"][i]["semester"]);
										row.find(".td_code").html(response["to_curr_izop"][i]["spec_code"]);
										row.find(".td_direc").html(response["to_curr_izop"][i]["spec_direction"]);
										row.appendTo($(".cur_table_to_izop"));
									}
								} else $(".move_tmp").clone().removeClass("move_tmp").addClass("to_subj_list").html("������� ������ ���� ���.<br>").appendTo(".move_log");


//////////////////////////////////////////////from
// �����
								$(".move_tmp").clone().removeClass("move_tmp").addClass("to_subj_list").css("color", "red").html("<strong>����� ����������:</strong><br>").appendTo(".move_log");
								$(".move_tmp").clone().removeClass("move_tmp").addClass("to_subj_list").html("<strong>�����:</strong><br>").appendTo(".move_log");
								if(!response["from_files"]) $(".move_tmp").clone().removeClass("move_tmp").addClass("to_subj_list").html("������ ���:<br>").appendTo(".move_log");
								else {
									var cur_table = $(".tmp_table_file").clone().removeClass("tmp_table_file hidden").addClass("file_table_from");
									cur_table.appendTo($(".move_log"));
									for(i in response["from_files"]) {
										var row  = $(".file_table_from .tmp_table_tr").clone().removeClass("tmp_table_tr hidden");
										row.find(".td_name").html(response["from_files"][i]["name"]);
										row.find(".td_subj").html(response["from_files"][i]["subj_name"]);
										row.find(".td_spec").html(response["from_files"][i]["spec_name"]);
										row.find(".td_code").html(response["from_files"][i]["spec_code"]);
										row.find(".td_user").html(response["from_files"][i]["username"]);
										row.appendTo($(".file_table_from"));
									}
								}
// �����
								$(".move_tmp").clone().removeClass("move_tmp").addClass("from_subj_list").html("<strong>������� �����:</strong><br>").appendTo(".move_log");
								if(response["from_curr"])
								{
									var cur_table = $(".tmp_table").clone().removeClass("tmp_table hidden").addClass("cur_table_from");
									cur_table.appendTo($(".move_log"));
									for(i in response["from_curr"]) {
										var row  = $(".cur_table_from .tmp_table_tr").clone().removeClass("tmp_table_tr hidden");
										row.find(".td_spec").html(response["from_curr"][i]["spec_name"]);
										row.find(".td_subj").html(response["from_curr"][i]["subj_name"]);
										row.find(".td_group").html(response["from_curr"][i]["group_name"]);
										row.find(".td_code").html(response["from_curr"][i]["spec_code"]);
										row.find(".td_direc").html(response["from_curr"][i]["spec_direction"]);
										row.appendTo($(".cur_table_from"));
									}
								}
								else $(".move_tmp").clone().removeClass("move_tmp").addClass("from_subj_list").html("������� ������ ���.<br>").appendTo(".move_log");
// �����	����
								$(".move_tmp").clone().removeClass("move_tmp").addClass("from_subj_list").html("<strong>������� ����� ����:</strong><br>").appendTo(".move_log");
								if(response["from_curr_izop"])
								{
									var cur_table = $(".tmp_table").clone().removeClass("tmp_table hidden").addClass("cur_table_from_izop");
									cur_table.find(".tmp_th").html("�������");
									cur_table.appendTo($(".move_log"));
									for(i in response["from_curr_izop"]) {
										var row  = $(".cur_table_from_izop .tmp_table_tr").clone().removeClass("tmp_table_tr hidden");
										row.find(".td_spec").html(response["from_curr_izop"][i]["spec_name"]);
										row.find(".td_subj").html(response["from_curr_izop"][i]["subj_name"]);
										row.find(".td_group").html(response["from_curr_izop"][i]["semester"]);
										row.find(".td_code").html(response["from_curr_izop"][i]["spec_code"]);
										row.find(".td_direc").html(response["from_curr_izop"][i]["spec_direction"]);
										row.appendTo($(".cur_table_from_izop"));
									}
								}
								else $(".move_tmp").clone().removeClass("move_tmp").addClass("from_subj_list").html("������� ������ ���� ���.<br>").appendTo(".move_log");
						}
					});
				}
				return false;
			});
//������
			$("#move_cancel").click(function(){
					$("#save_button").toggleClass("hidden");
					$(".move_log").toggleClass("hidden");
					$(".to_subj_list").remove();
					return false;
			});
//co�������
			$("#move_ok").click(function(){
				var to_id = $(".to_select").val();
				var to_name = $(".to_select option:selected").text();
				var from_id  = $(".from_id").attr('class').split(" ");
				from_id = from_id[1];
				var current_id  = $(".to_select").attr('class').split(" ");
				current_id = current_id[1];
				if(from_id != to_id) {
					$(".to_subj_list").remove();
					var data = {'from_id': from_id, 'to_id': to_id, 'mode': 'edit', 'current_id': current_id};
					var request = {'module': '5', 'mode': 'json', 'params': 'ajax_move_subject', 'data': data};
					jQuery.ajax({
						type: 'POST',
						url: '/ajax_mode/',
						dataType: 'json',
						data: request,
						success: function(response) {
						$(".move_log").toggleClass("hidden");
						$("#save_button").toggleClass("hidden");
						if(!response["error"])
							for(i in response["response"])
								$(".output_msg").clone().removeClass("output_msg hidden").insertAfter($(".move_log")).css("color", "green").html(response["response"][i]).delay(6640000).fadeOut(3000);
						else
							for(i in response["error"])
								$(".output_msg").clone().removeClass("output_msg hidden").insertAfter($(".move_log")).css("color", "red").html(response["error"][i]).delay(6640000).fadeOut(3000);
						}
					});
				}
				return false;
			});
		});
	</script>
END;
		}
			break;


		case 'ajax_people_restore_password': {
			$EE["head_extra3"][] = <<<END
	<script type="text/javascript">
		jQuery(document).ready(function($) 
		{
			var printdata;
            jQuery('.restore_password').click(function()
            {
            	if(confirm('�� ������������� ������ �������� ������ ������ � �������� ����� ������ ��� ����� �� ������?')) 
               	{
	                var id = $(this).attr('id');
	                if (id) 
	                {
	                	var request = {'module': 5, 'mode': 'json', 'params': 'people_restore_password', 'data': {'id': id} };
	                	$.ajax(
	                	{
		                    type: 'POST',
		                    dataType: 'json',
		                    url: '/ajax_mode/',
		                    data: request,
		                    success: function( data )
		                    {
		                        $("#new_restore_data").html("<b>������ ��� ����� � ������ ������� �� nsau.edu.ru.</b> <br><br> <b>���:</b> "+data["name"]+"<br><b>Email:</b> "+data["email"]+"<br><br><b>�����:</b> "+data["login"]+"<br><b>������: "+data["password"]);
		                    	printdata = "<b>������ ��� ����� � ������ ������� �� nsau.edu.ru.</b> <br><br> <b>���:</b> "+data["name"]+"<br><b>Email:</b> "+data["email"]+"<br><br><b>�����:</b> "+data["login"]+"<br><b>������: "+data["password"];
		                    }
	                	});
	            	}
            	}
            });

            jQuery('.print_new_data').click(function()
            {
            	var html_to_print=printdata;
            	var iframe=$('<iframe id="print_frame">'); // ������� iframe � ����������
				$('body').append(iframe); //��������� ��� ���������� � iframe � ��� body (� ����� �����)
				var doc = $('#print_frame')[0].contentDocument || $('#print_frame')[0].contentWindow.document;
				var win = $('#print_frame')[0].contentWindow || $('#print_frame')[0];
				doc.getElementsByTagName('body')[0].innerHTML=html_to_print;
				win.print();
				$('iframe').remove();
            });
		});




	</script>
END;
		}
			break;

		case 'nsau_wifi': {
			$EE["head_extra3"][] = <<<END
	<script type="text/javascript">
		jQuery(document).ready(function($) 
		{
    		$(".wifi_login").prop("disabled", true);
    		$(".wifi_password").prop("disabled", true);
        	var request = {'module': 5, 'mode': 'json', 'params': 'ajax_nsau_wifi_get_password', 'data': {} };
        	$.ajax(
        	{
                type: 'POST',
                dataType: 'json',
                url: '/ajax_mode/',
                data: request,
                success: function( data )
                {
                	if (data) 
                	{
                		$(".wifi_login").val(data.login);
                		$(".wifi_password").val(data.pass);
                		$(".set_wifi_password").text("�������� ������");
                	}
                	else
                	{
                		$(".set_wifi_password").text("������� ������");
                	}
                }
        	});

	        jQuery('.set_wifi_password').click(function()
            {
            	//if(confirm('�� ������������� ������ �������� ������ ������ � �������� ����� ������ ��� ����� �� ������?')) 
               	//{
                	var request = {'module': 5, 'mode': 'json', 'params': 'ajax_nsau_wifi_set_password', 'data': {} };
                	$.ajax(
                	{
	                    type: 'POST',
	                    dataType: 'json',
	                    url: '/ajax_mode/',
	                    data: request,
		                beforeSend: function() 
		                {
		                    $(".wifi_password").addClass("loading");
		                },
	                    success: function( data )
	                    {
	                    	$(".wifi_password").removeClass("loading");
		                	if (data) 
		                	{
		                		$(".wifi_login").val(data.login);
		                		$(".wifi_password").val(data.pass);
		                	}
	                    }
                	});
            	//}
            });
		});
	</script>
END;
		}
			break;



		case 'curriculum': {
			$EE["head_extra3"][] = <<<END
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$("#add_cycle").click(function() {
				$("dt:not(.checked)").find("input").remove();
				$("dd:not(.checked)").find("input").remove();
			});
			Curriculum = function(moduleId) {
				if($('#curriculum .subject_group_item form').length) {
					$('#curriculum .subject_group_item label input').click(function() {
						if($(this).parents('.edit_form').hasClass('del_sgroup')) {
							$(this).parents('.edit_form').removeClass('del_sgroup').addClass('edit_sgroup');
						} else {
							$(this).parents('.edit_form').addClass('del_sgroup').removeClass('edit_sgroup');
						};
					});
				};
				if($('#curriculum .select_subject').length) {
					$('#curriculum .select_subject li dd span').text($('#curriculum .select_sgroup').find(':selected').attr('title')+'.');
					$('#curriculum .select_sgroup').change(function() {
						$('#curriculum .select_subject li dd span').text($(this).find(':selected').attr('title')+'.');
					});
					$('#curriculum .select_subject dl').hide();
					$('#curriculum .select_subject li > label').click(function() {
						$(this).next('dl').slideToggle();
					});
					$('#curriculum .select_subject dt label input:checked').parent().parent().addClass('checked').next('dd').addClass('checked');
					$('#curriculum .select_subject dt label').click(function() {
						if($(this).find('input').is(':checked')) {
							$(this).parent().addClass('checked').next('dd').addClass('checked');
						} else {
							$(this).parent().removeClass('checked').next('dd').removeClass('checked');
						}
					});
				}
			};
		});
	</script>
END;
		}
			break;

		case 'timetable_auditorium': {
			$EE["head_extra3"][] = <<<END
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			TimetableAuditorium = function() {
				$('.editable-select').editableSelect({
					bg_iframe: true,
					onSelect: function(list_item) {
						this.select.get(0).options[list_item.index()].selected = 'selected';
					}
				});
			};
		});
	</script>
END;
		}
			break;

		case 'speciality_directory': {
			$EE["head_extra3"][] = <<<END
	<script src="/scripts/jquery/jquery.maskedinput.js" type="text/javascript"></script>
	<script type="text/javascript" src="/scripts/jquery/jquery.autocomplete.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function($) {

			
			/*�������� ������ �������������*/
			$(".show_old").live("click", function() 
			{
				$(".old").show();
				$(".hide_old").show();
				$(".show_old").hide();
			});



			/*������ ������ �������������*/
			$(".hide_old").live("click", function() 
			{
				$(".old").hide();
				$(".hide_old").hide();
				$(".show_old").show();
			});
			$("input.date").mask("99.99.9999",{placeholder:"��.��.����"});
			// $('.editable-select').editableSelect({
                // bg_iframe: true,
                // onSelect: function(list_item) {
                  // this.select.get(0).options[list_item.index()].selected = 'selected';

                // }
            // }).parents('form').submit(function(){
              // $(this).find('select.editable-select').removeAttr('disabled');

            // });
ajaxSearchFiles = function(moduleId) {
	var field = $('.files_list').find('option');
				$('.files_search').autocomplete({
					// var th = $(this);
					delay: 500,
					source: function(request, response) {
						//if(request.term.length < 3) return;

						var request = {'module': moduleId, 'mode': 'json', 'params': 'ajax_search_file', 'data': {'q': request.term} };

						$.ajax({
						'url': '/ajax_mode/',
						dataType: 'json',
						data: request,
						success: function(json) {
						//alert(getProps(json));
								response($.map(json, function(item) {

								return {
									label: item.name,
									value: item.id,
									title: item.full_path
								}
							}));
						}
					})
					},
					select: function(event, ui) {
						var title = ui.item.label.split(' > ');
						title = title[title.length - 1];
						$(this).closest("td").find('.files_search').attr('value', title);
						$(this).closest("td").find('.files_list').html("<option value='"+ui.item.value+"'>"+title+"</option>");
						$(this).closest("td").find('.files_list, #subject_search + a').show();
						$(this).closest("td").find('.files_search').hide();

						return false;
					},
					focus: function(event, ui) {
					var title = ui.item.title;
					//	title = title[title.length - 1];
        $('#ui-active-menuitem').attr('title', title);


						return false;
				    }
				});


}







		});
	</script>
END;
		}
			break;

		case 'timetable_subjects_by_departments': {
			$EE["head_extra3"][] = <<<END
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			TimetableSubjectsByDepartments = function() {
				sendform_sbd = function(form) {
	                if (form.subject.value == 0) {
	                    alert('����������, �������� �������');
	                    form.subject.focus();
	                    return false;
	                };
	                if (form.faculty.value == 0) {
	                    alert('����������, �������� ���������');
	                    form.faculty.focus();
	                    return false;
	                };
	                if (form.teacher_id.value == 0) {
	                    alert('����������, �������� �������������');
	                    form.teacher_id.focus();
	                    return false;
	                };
	                if (form.groups.value == "") {
	                    alert('����������, ������� ������ �����');
	                    form.groups.focus();
	                    return false;
	                };
	                if (form.auditorium.value == "") {
	                    alert('����������, ������� ������ ���������');
	                    form.auditorium.focus();
	                    return false;
	                };
	                if (form.rate.value == 0) {
	                    alert('����������, �������� ������ ���������� ������');
	                    form.rate.focus();
	                    return false;
	                };
	                return true;
	            };
			};
		});
	</script>
END;
		}
			break;

		case 'timetable_groups': {
			$EE["head_extra3"][] = <<<END
	<script type="text/javascript">
		jQuery(document).ready(function($) 
		{
			$("#new_group").live("keyup", function() 
			{
				var group_name = $(this).val();
				var first_char = group_name.charAt(0).toLowerCase();
				//alert(first_char);
				if (first_char == "�" || first_char == "�" || first_char == "a" || first_char == "y") 
				{
					if (first_char == "a" || first_char == "�")
					{
						$('#group_qualification option:contains(�����������)').prop('selected', true);
					}
					//���������� ����
					if (group_name.charAt(2)) 
					{
						$('#group_course option[value="'+group_name.charAt(2)+'"]').prop('selected', true);
					}
					else
					{
						$('#group_course option[value="1"]').prop('selected', true);
					}	

					if (group_name.charAt(1)) 
					{
						if (group_name.charAt(0)==9 || group_name.charAt(0) == 5 || group_name.charAt(1)==9 || group_name.charAt(1) == 5) 
						{
							$("#opt_0, #opt_4, #opt_1, #opt_22, #opt_3, #opt_2, #opt_6, #opt_5, #opt_666").show();
							$("#option_prev").text("����������� ����������");
						}
						else
						{
							$("#opt_0, #opt_4, #opt_1, #opt_22, #opt_3, #opt_2, #opt_6, #opt_5, #opt_666").hide();
							$("#opt_"+group_name.charAt(1)).show();	
							$("#option_prev").text(  $("#opt_"+group_name.charAt(1)).attr('label'));
						}
					}
					else
					{
						//$("#option_prev").text("����������� ����������");
					}	

					if(group_name.charAt(3) || group_name.charAt(4))
					{
						var request = {'module': 5, 'mode': 'json', 'params': 'ajax_get_isset_group', 'data': {'group': group_name} };
						$.ajax({
							type: 'POST',
							dataType: 'json',
							url: '/ajax_mode/',
							data: request,
							success: function( data ) 
							{
								if (data) 
								{
									//������ ��� ����������, ������������ � ������� � ������� ������ �������������
									$("#new_group").css("background-color", "#80071a");
									$("#new_group").css("color", "#fff");
									$(".ajax_edit_group").attr("href", "edit/"+data.id+"/")
									$(".ajax_edit_group").show();
								}
								else
								{
									//������ ���, ������������ � ������� � ��������
									$("#new_group").css("background-color", "#409c3c");
									$("#new_group").css("color", "#fff");
									$(".ajax_edit_group").hide();
								}
							}
						});
					}
					else
					{
						//���������� ������ ������� ���
						$("#new_group").css("background-color", "#fff");
						$("#new_group").css("color", "#555");
						$(".ajax_edit_group").hide();
					}
				}
				else
				{
					//���������� ����
					if (group_name.charAt(1)) 
					{
						$('#group_course option[value="'+group_name.charAt(1)+'"]').prop('selected', true);
					}
					else
					{
						$('#group_course option[value="1"]').prop('selected', true);
					}
					//���������� ����������� ����������
					if (group_name.charAt(0)) 
					{
						if (group_name.charAt(0)==9 || group_name.charAt(0) == 5) 
						{
							$("#opt_0, #opt_4, #opt_1, #opt_22, #opt_3, #opt_2, #opt_6, #opt_5, #opt_666").show();
							$("#option_prev").text("����������� ����������");
						}
						else
						{
							$("#opt_0, #opt_4, #opt_1, #opt_22, #opt_3, #opt_2, #opt_6, #opt_5, #opt_666").hide();
							$("#opt_"+group_name.charAt(0)).show();
							$("#option_prev").text(  $("#opt_"+group_name.charAt(0)).attr('label'));
						}
					}
					else
					{
						$("#option_prev").text("����������� ����������");
					}

					if(group_name.charAt(3) || group_name.charAt(4))
					{
						var request = {'module': 5, 'mode': 'json', 'params': 'ajax_get_isset_group', 'data': {'group': group_name} };
						$.ajax({
							type: 'POST',
							dataType: 'json',
							url: '/ajax_mode/',
							data: request,
							success: function( data ) 
							{
								if (data) 
								{
									//������ ��� ����������, ������������ � ������� � ������� ������ �������������
									$("#new_group").css("background-color", "#80071a");
									$("#new_group").css("color", "#fff");
									$(".ajax_edit_group").attr("href", "edit/"+data.id+"/")
									$(".ajax_edit_group").show();
								}
								else
								{
									//������ ���, ������������ � ������� � ��������
									$("#new_group").css("background-color", "#409c3c");
									$("#new_group").css("color", "#fff");
									$(".ajax_edit_group").hide();
								}
							}
						});
					}
					else
					{
						//���������� ������ ������� ���
						$("#new_group").css("background-color", "#fff");
						$("#new_group").css("color", "#555");
						$(".ajax_edit_group").hide();
					}
				}
			});

			//������ ������ ����������� �� ����������� ����������
			$("#napr").change(function()  
			{
				var gr = $(this).attr('value');
				gr = gr.split("|")

				var request = {'module': 5, 'mode': 'json', 'params': 'ajax_get_level', 'data': {'spec': gr[1]} };
				$.ajax({
					type: 'POST',
					dataType: 'json',
					url: '/ajax_mode/',
					data: request,
					success: function( data ) 
					{
						
						$('#group_qualification option[value="'+data+'"]').prop('selected', true);
					}
				});
			});


		});
	</script>
END;
		}
			break;
		case 'input_calendar': {
			$EE["head_extra3"][] = <<<END
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			InputCalendar = function() {
				$('input.date_time').datepicker({
					closeText: '�������', // Display text for close link
					prevText: '����������', // Display text for previous month link
					nextText: '���������', // Display text for next month link
					currentText: '�������', // Display text for current month link
					monthNames: ['������','�������','����','������','���','����', '����','������','��������','�������','������','�������'], // Names of months for drop-down and formatting
					monthNamesShort: ['���', '���', '���', '���', '���', '����', '����', '���', '���', '���', '���', '���'], // For formatting
					dayNames: ['�����������', '�����������', '�������', '�����', '�������', '�������', '�������'], // For formatting
					dayNamesShort: ['���', '���', '����', '���', '���', '���', '���'], // For formatting
					dayNamesMin: ['��','��','��','��','��','��','��'], // Column headings for days starting at Sunday
					weekHeader: 'Wk', // Column header for week of the year
					dateFormat: 'yy-mm-dd', // See format options on parseDate);
					//timeFormat: 'hh:mm:ss',
					//ampm: false,
					//hourMin: 8,
					//hourMax: 18,
					//timeOnlyTitle: '�������� �����',
					//timeText: '�����',
					//hourText: '����',
					//minuteText: '������',
					//secondText: '�������',
					//init_this_time: true,
					firstDay: 1,
				});
			};
		});
	</script>
END;
		}
			break;

		case 'global_people': {
			$EE["head_extra3"][] = <<<END
	<script src="/scripts/jquery/jquery.maskedinput.js" type="text/javascript"></script>
	<script type="text/javascript">
jQuery(document).ready(function($) {



	$('td.add_department_post').live('click', function() {
		var row = $(this).closest('tbody').find('tr.post_row:first').clone();
		row.insertAfter($('tr.post_row:last'));
	});
	$('td.del_department_post').live('click', function() {
		if($('tr.post_row').length == 1) {
			alert('���� ��������� ����������� ��� ����������.');
			return false;
		}
		$(this).closest('tr').detach();
	});




	$("input.check_forms").live("click", function(){
		var err = false;
		$("input.date:visible:not(.fix_table)").each(function(e){
			var part = $(this).val().split(".");
			if(((part[0]<1) || (part[0]>31)) || ((part[1]<1) || (part[1]>12)) || ((part[2]<1901) || (part[2]>2030))) {
				alert("� ����� '������ � �������������� �����������' ����������� ������� ����.");
				err = true;
				return false;
			}
		});
		// $("td.date:visible:not(.fix_table)").each(function(e){
			// var part = $(this).val().split(".");
			// if(((part[0]<1) || (part[0]>31)) || ((part[1]<1) || (part[1]>12)) || ((part[2]<1901) || (part[2]>2030))) {
				// alert("� ����� '����� ��������� ������������' ����������� ������� ����.");
				// err = true;
				// return false;
			// }
		// });
		$(".edu_year:visible:not(.fix_table)").each(function(e){
			if(($(this).val()<1901) || ($(this).val()>2030)) {
				alert("� ����� '�������� ������ ���������������� �����������' ����������� ������ ���.");
				err = true;
				return false;
			}
		});
		if(err) return false;
	});


//////////////�����������
	$("input.date").mask("99.99.9999",{placeholder:"��.��.����"});
	$("#add_education").live("click", function() {

		var univ = $("#university").val();
		var edu_level = $("#edu_level").val();
		var y_end = $("#year_end").val();
		var spec = $("#speciality").val();
		var qual = $("#qualification").val();
		if(!univ || !y_end || !spec || !qual || !edu_level) {
			alert("��� ���� ����������� ��� ����������.");
			return false;
		}
		var tr = $("tr.etmp").clone().removeClass("hidden").removeClass("etmp").addClass("edu");
		tr.find(".puniversity").find("input").attr("value", univ);
		tr.find(".pedu_level").find("select").attr("value", edu_level);
		tr.find(".pyear_end").find("input").attr("value", y_end);
		tr.find(".pspeciality").find("input").attr("value", spec);
		tr.find(".pqualification").find("input").attr("value", qual);
		tr.insertAfter("thead.edu");
		$("input.edu_year").mask("9999",{placeholder:"����"});
		$("#university, #year_end, #speciality, #qualification").attr("value", "");
		$("tr.empty_edu:not(.hidden)").addClass("hidden");
		return false;
	});

	$(".delete_education").live("click", function() {
		$(this).closest("tr").remove();
		if($("tr.edu").length==0)
			$("tr.empty_edu").removeClass("hidden");
		return false;
	});
//////////////�������������� �����������
	$("#add_add_education").live("click", function() {
		var univ = $("#a_university").val();
		var cert = $("#certificate").val();
		var ddoc = $("#date_doc").val();
		var drank = $("#date_rank").val();
		if(!univ || !cert || !ddoc) {
			alert("��� ���� ����������� ��� ����������.");
			return false;
		}

		var tr = $("tr.atmp").clone().removeClass("hidden").removeClass("atmp").addClass("addictional_edu");
		tr.find("select").addClass("fix_table");
		tr.find(".auniversity").find("input").attr("value", univ);
		tr.find(".acertificate").find("option[value='"+cert+"']").attr("selected", "selected");
		tr.find(".adate_doc").find("input").attr("value", ddoc).addClass("date");
		tr.find(".adate_rank").find("input").attr("value", drank).addClass("date");
		tr.insertAfter("thead.add_edu");
		$("input.date").mask("99.99.9999",{placeholder:"��.��.����"});
		$("#a_university, #date_doc, #date_rank, #certificate").attr("value", "");
		// $("#certificate").find("option:first").attr("selected", "selected");
		$("tr.empty_add_edu:not(.hidden)").addClass("hidden");
		return false;
	});

	$(".delete_add_education").live("click", function() {
		$(this).closest("tr").remove();
		if($("tr.addictional_edu").length==0)
			$("tr.empty_add_edu").removeClass("hidden");
		return false;
	});





















//////////////////////////////////////////////////////////////////////////////////////
///////prof development ajax delete and insert////////////////////////////////////////
		$("#phone").mask("+7 (383) 999-99-99");
		$("#date_begin, #date_end").mask("99.99.9999",{placeholder:"��.��.����"});
		$(".edu_year").mask("9999",{placeholder:"����"});


		$(".delete_prof_development").live("click", function(){
			if(!confirm('�� �������, ��� ������ ������� ���������� � ��������� ������������? ')) return false;
			var id = $(this).closest("tr").attr("data-id");
			var tr = $(this);
			var request = {'module': 5, 'mode': 'json', 'params': 'ajax_delete_prof_development', 'data': {'id': id}};
			$.ajax({
				type: 'POST',
				dataType: 'json',
				url: '/ajax_mode/',
				data: request,
				success: function( data ) {
					if(data=="deleted") {
						tr.closest("tr").remove();
						if($("tr.prof").length==0)
							$("tr.empty_prof").removeClass("hidden");
					}
					else
						return false;

				}
			});

			return false;
		});


		var lost_text;
		$("tr.prof").find("td").live("click", function(){

			if ($(this).children('input').attr("value") || $(this).children('select').attr("value"))
			{
				
			}
			else
			{
			var text = $(this).text();
			var clas = $(this).attr("class").split(" ");
			var fild = clas[0];
			var mode = clas[1];
			var mask = clas[2];
			var id = $(this).closest("tr").attr("data-id");
			$(".edit_fild").closest("td").html(lost_text);
			var text = $(this).text();
			lost_text = text;
			if(mode == "input") {
				$(this).html("<input type='text' id='tmpinput' value='"+text+"' class='form-control "+mask+" edit_fild'>");
				var tmpp = this;
				
				if(mask=="date") {
					$("input."+mask+"").mask("99.99.9999",{placeholder:"��.��.����"});
				}
				$('input.edit_fild').focusout(function(event){
					var keyCode = event.keyCode ? event.keyCode :
					event.charCode ? event.charCode :
					event.which ? event.which : void 0;
					//if(keyCode == 13)
					//{
						//alert("saved")
						var val = $("input.edit_fild").val();
						var tmp_var = $(".form-control."+mask+".edit_fild").val();
						if(mask=="date") {
							if(!checkDate(val)) {
								alert("� ����� '����� ��������� ������������' ����������� ������� ����.");
								return false;
							}
						}
						var request = {'module': 5, 'mode': 'json', 'params': 'ajax_edit_prof_development', 'data': {'id': id, 'val': val, 'fild': fild}};
						$.ajax({
							type: 'POST',
							dataType: 'json',
							url: '/ajax_mode/',
							data: request,
							success: function( data ) {
								if(data=="upd")
								{
									$(tmpp).closest("td").html(val);
								}
								else {
									$("input.edit_fild").closest("td").html(lost_text);
									alert("���� ������ ���� �����������.");
								}
							}
						});
						return false;
					//}
				});

			} else
			if(mode == "select") {
				$(this).html("");
				var sel = $("#"+fild+"").clone().addClass("edit_fild");
				$(this).append(sel);
				var tmpp = this;
				$('.edit_fild').focusout(function(event){
					var keyCode = event.keyCode ? event.keyCode :
					event.charCode ? event.charCode :
					event.which ? event.which : void 0;

					//if(keyCode == 13)
					//{
						//alert("saved")
						var val = $("select.edit_fild").find("option:selected").val();
						var text = $("select.edit_fild").find("option:selected").text();
						var request = {'module': 5, 'mode': 'json', 'params': 'ajax_edit_prof_development', 'data': {'id': id, 'val': val, 'fild': fild}};
						$.ajax({
							type: 'POST',
							dataType: 'json',
							url: '/ajax_mode/',
							data: request,
							success: function( data ) {
								if(data=="upd"){
									$(tmpp).closest("td").html(text);
								}
								else {
									$("input.edit_fild").closest("td").html(lost_text);
									alert("���� ������ ���� �����������.");
								}
							}
						});
						return false;
					//}
				});
			}
		}
		});

		function checkDate(date) {
			var part = date.split(".");
			if(((part[0]<1) || (part[0]>31)) || ((part[1]<1) || (part[1]>12)) || ((part[2]<1901) || (part[2]>2030)))
				return false;
			else
				return true;
		}

		$("#add_prof_development").live("click", function(){
			var course = $("#course").val();
			var location = $("#location").val();
			var hours = $("#hours").val();
			var date_begin = $("#date_begin").val();
			var date_end = $("#date_end").val();
			var edu_type = $("#edu_type").val();
			var edu_type_text = $("#edu_type").find("option:selected").text();
			var doc_type = $("#doc_type").val();
			var doc_type_text = $("#doc_type").find("option:selected").text();
			var people_id = $(this).closest("table").attr("data-id");

			if(!course || !location || !hours || !date_begin || !date_begin || !date_end || !edu_type || !doc_type) {
				alert("��� ���� ����������� ��� ����������.");
				return false;
			}

			if(!checkDate(date_begin) || !checkDate(date_end)) {
				alert("� ����� '����� ��������� ������������' ����������� ������� ����.");
				return false;
			}



			var tr = $(this);
			var request = {'module': 5, 'mode': 'json', 'params': 'ajax_add_prof_development', 'data': {'course': course, 'location': location, 'hours': hours, 'date_begin': date_begin, 'date_end': date_end, 'edu_type': edu_type, 'doc_type': doc_type, 'people_id': people_id}};

			$.ajax({
				type: 'POST',
				dataType: 'json',
				url: '/ajax_mode/',
				data: request,
				success: function( data ) {
					var tr = $("tr.ptmp").clone(true).removeClass("ptmp").removeClass("hidden").addClass("prof");
					tr.attr("data-id", data);
					tr.find("td.pcourse").html(course).removeClass("pcourse").addClass("course input");
					tr.find("td.plocation").html(location).removeClass("plocation").addClass("location input");
					tr.find("td.phours").html(hours).removeClass("phours").addClass("hours input");
					tr.find("td.pdate_begin").html(date_begin).removeClass("pdate_begin").addClass("date_begin input date");
					tr.find("td.pdate_end").html(date_end).removeClass("pdate_end").addClass("date_end input date");
					tr.find("td.pedu_type").html(edu_type_text).removeClass("pedu_type").addClass("edu_type select");
					tr.find("td.pdoc_type").html(doc_type_text).removeClass("pdoc_type").addClass("doc_type select");
					tr.insertAfter("#prof_dev_body");
					$("tr.empty_prof:not(.hidden)").addClass("hidden");
					$("#course, #location, #hours, #date_begin, #date_end").attr("value", "");
				}
			});

			return false;
		});

		////////PEOPLE MENU
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$("a.people_menu").live("click", function() {

			$("a.people_menu").removeClass("current");
			$(this).addClass("current");
			var pmode = $(this).attr("class").split(" ");
			var mode = pmode[1];
			if(mode=="current") return false;
			var people_id = $("input.people_id").val();
			var moduleId = 5;
			if(mode=="timetable") moduleId = 33;
			if(mode=="stimetable") moduleId = 33;
			if(mode=="show_folio_nir") moduleId = 43;
			if(mode=="show_folio_vr") moduleId = 43;
			//mode - ����������� ��� �������������� ����. � ���� �����. �� ���� "people_menu MODE"
			var request = {'module': moduleId, 'mode': 'html_code', 'params': 'ajax_people_menu_'+mode, 'data': people_id};
			$.ajax({
				dataType : 'html',
				type: 'POST',
				data: request,
				url: '/ajax_mode/',
				resetForm: true,
				beforeSend: function(position,total) {
					$("div.people_content").html("<div  style='text-align: center'><img src='http://nsau.edu.ru/themes/images/loading_news.gif'></div>");
				},
				success: function(data) {
					$("div.people_content").html(data);
				}
			});
			return false;
		});






	$("a.show_tt").live("click", function(){
		var table = $("table.show_tt").detach();
		table.insertAfter($(this)).toggleClass("hidden");
		return false;
	});
	FoldersList = function() {
		$('div.folder_list').click(function() {
		var option = $(this);
		var option_val = option.attr('id').split('_');
		$('div #subj_id_'+option_val[2]).slideToggle();
		});

		$('ul.tabs').delegate('li:not(.current)', 'click', function() {
		var scrollTop = window.pageYOffset || document.documentElement.scrollTop;
		$('div.slide_file_list').hide();
		$(this).addClass('current').siblings().removeClass('current')
		.parents('div.section').find('div.box').hide().eq($(this).index()).fadeIn(150);
		window.scrollTo(0,scrollTop);
		});
	};



var _URL = window.URL || window.webkitURL;
$("#user_photo").change(function(e) {
    var file, img;
	var file_size = event.target.files[0];
	if ((file = this.files[0])) {
        img = new Image();
		$("#max_size").attr({ style: 'display: block; color: red' });
		$("#max_size").empty();
        img.onload = function() {
			if (file_size.size > 2097152){
				$("#user_photo").attr({ value: '' });
				$("#max_size").append("������ ���������� ���� ����� ����� 2��!");
			}
			 if(this.width>1024) {
				$("#user_photo").attr({ value: '' });
				$("#max_size").append("<br>������ ����� 1024 ��������.");
			}
			 if(this.height>1024) {
				$("#user_photo").attr({ value: '' });
				$("#max_size").append("<br>������ ����� 1024 ��������.");
			}
			if((file_size.size <= 2097152) && (this.width<=1024) && (this.height<=1024)) {
				$("#max_size").attr({ style: 'display: none;' });
			}
		};
        img.onerror = function() {
            	$("#user_photo").attr({ value: '' });
				$("#max_size").append("������ ���������������� ������ �����.<br> �������������� �������: GIF, JPEG, PNG.");
        };
        img.src = _URL.createObjectURL(file);
    }
});
			GlobalPeople = function(moduleId) {
				// var is_curator = '{$MODULE_DATA['output']['editperson']['is_curator']}';
				// if (is_curator == '')
					// is_curator = '{$MODULE_DATA['output']['display_variant']['add_item']['is_curator']}';
				// if (is_curator == 'on')
					// is_curator = '1';
				// $('#curator_text, #curator_groups_cont').css('display', is_curator == '1' ? 'block' : 'none');
				$('#curator_selector').change(function() {
					$('#curator_text_parent,#curator_groups_cont').css('display', this.checked ? 'block' : 'none');
				});

				$("select.change_people_cat").live("change", function() {
					var cat = $(this).val();
					var people_id = $("input.h_people_id").val();
					var request = {'module': '5', 'mode': 'json', 'params': 'ajax_change_people_cat', 'data': {'people_id': people_id, 'cat': cat} };
					$.ajax({
						type: 'POST',
						url: '/ajax_mode/',
						data: request,
						success: function(json){
							window.location = "/people/edit/"+people_id;
						}
					});
				});

				if($('#people_cat_select').length) {
					$('#people_form').attr('class', 'people_cat_'+$('#people_cat_select').val());
					if($('#people_form #responsible input').is(':checked') || $('#people_cat_select').val() == 3) {
						$('#usergroup').removeClass('hidden');
					} else {

					};
					$('#people_form .add_status input').removeAttr('checked');
					$('#people_form .add_status .people_cat_'+$('#people_cat_select').val()+':first input').attr({'checked':'checked'});

					$('#people_cat_select').change(function() {
						$('#people_form').attr('class', 'people_cat_'+jQuery(this).val());
						$('#people_form .add_status input').removeAttr('checked');
						$('#people_form .add_status .people_cat_'+$('#people_cat_select').val()+':first input').attr({'checked':'checked'});
						if($('#people_cat_select').val() == 3 || ($('#people_form #responsible input').is(':checked') && $('#people_cat_select').val() == 2)) {
							// $('#usergroup').removeClass('hidden');
						} else {
							// $('#usergroup').addClass('hidden');
							// $('#people_form #responsible input').removeAttr('checked');
						};
					});
				};

				$('#people_form #responsible input').click(function() {
					if($(this).is(':checked')) {
						$('#usergroup').removeClass('hidden');
					} else {
						$('#usergroup').addClass('hidden');
					};
				});


				/*function template(row, arr)
				{
					row.find('.als_d').text(arr);

				}*/



				function ajax_load_subjlist(depId)
					{

						var request = {'module': '5', 'mode': 'json', 'params': 'ajax_load_subjects', 'data': {'dep_id': depId} };
							$.ajax({
								type: 'POST',
								url: '/ajax_mode/',
								data: request,
								success: function(json){

						var cart = JSON.parse ( json );
						$("#als_dep_name").text(cart.data.department);
						$("#content12").text(cart.data.subjects[418]);
						for (var i in cart.data.subjects)
						{
						$('#als_dep_name').after('<dd id="subid_'+i+'_'+depId+'" title="'+cart.data.subjects[i]+'">'+cart.data.subjects[i]+'</dd>');
						}

						$('#subject_select').on('click', 'dd', function() {
								$('#subject_select dd').click(function() {
								$('#subject_select dd').removeClass('selected');
								$(this).addClass('selected');
								}).dblclick(function() {
									if(!$(this).hasClass('hidden')) {
									var option = $(this);
									var option_val = option.attr('id').split('_');
									$('#subject_list_'+option_val[2]).append('<li><input type=hidden name=subject['+option_val[2]+']['+option_val[1]+'] value='+option_val[1]+' />'+option.text()+'<em class="inline-block"></em></li>');
									option.addClass('hidden');
									option.removeClass('selected');
									};
								});
							});


								}
							});
					}


         $('#department_select dd').click(function() {
					$('#department_select dd').removeClass('selected');
					$(this).addClass('selected');
				}).dblclick(function() {

					if(!$(this).hasClass('hidden')) {
						var option = $(this);
						var option_val = option.attr('id').split('_');
						option_val = option_val[1];
						ajax_load_subjlist(option_val);
						$('#department_list').append('<li><input type=hidden name=department['+option_val+'] value='+option_val+'><b>'+option.text()+'</b><em class="inline-block"  id="new_dep"></em></li>');
						if ($('#curator_groups')) {
							$('#curator_groups').append('<optgroup id=\''+option_val+'\' label=\''+option.text()+'\'><option></option></optgroup>');
						}
						$('#new_dep').after('<ul id="subject_list_'+option_val+'"></ul>');
						$("em").removeAttr("id");
						option.addClass('hidden');
						option.removeClass('selected');
					};
				});
				$('#subject_select dd').click(function() {
					$('#subject_select dd').removeClass('selected');
					$(this).addClass('selected');
				}).dblclick(function() {
					if(!$(this).hasClass('hidden')) {
						var option = $(this);
						var option_val = option.attr('id').split('_');
						//option_val = option_val[1];
						$('#subject_list_'+option_val[2]).append('<li><input type=hidden name=subject['+option_val[2]+']['+option_val[1]+'] value='+option_val[1]+'>'+option.text()+'<em class="inline-block"></em></li>');

						option.addClass('hidden');
						option.removeClass('selected');
					};
				});

	/*
				$('#department_list li em').live('click', function(){
					var dep_val = $(this).parent().find('input').attr('value');
					var option = $('#department_select #depid_'+dep_val);
					option.removeClass('hidden');
					$(this).parent().remove();
				});
		*/

				$('#department_list li em').live('click', function(){
					var dep_val = $(this).parent().find('input').attr('value');
					var option = $('#department_select #depid_'+dep_val);
					option.removeClass('hidden');
					$(this).parent().remove();
				});



				SetCuratorGroupsSelectHandlers = function(container, sourceSelect,destSelect,sourceSelector, destSelector, destDataName) {

					$('#'+destSelector).on('click', function() {
						var optFrom = $('#'+destSelect+' option:selected');
						if (optFrom.length && optFrom.text()) {
							optFrom.each(function() {
								$('#'+container+' input[value='+$(this).val()+']').remove();
							});
							var optFromParents = optFrom.parent();
							var optTo = $('#'+sourceSelect+' option:first');
							optFrom.insertBefore(optTo); // �� ��������� ������, ����� ������� ������ ��� ������, ��� unreal
							optFromParents.each(function() {
								if (!$(this).children().length)
									$('<option></option>').appendTo($(this));
							});
						}
					});

					$('#'+sourceSelector).on("click", function() {
						var optFrom = $('#'+sourceSelect+' option:selected');
						if (optFrom.length && optFrom.text()) {
							var optTo = $('#'+destSelect+' option:selected').length ? $('#'+destSelect+' option:selected') : $('#'+destSelect+' option:first');
							if (optTo.length > 1)
								optTo = optTo.eq(optTo.length-1);
							var optToParentId = optTo.parent().attr('id');
							if (optTo.text())
								optFrom.insertAfter(optTo);
							else
								optTo.replaceWith(optFrom);
							optFrom.each(function() {
								$('<input type=\'hidden\' name=\''+destDataName+'['+optToParentId+']['+$(this).val()+']\' value=\''+$(this).val()+'\'></input>').appendTo($('#'+destSelect).parent());
							});
						}
					});
				}

				SetCuratorGroupsSelectHandlers('curator_groups_cont', 'all_groups', 'curator_groups', 'all_groups_selector', 'curator_groups_selector', 'curator_groups');

			};
		});


	</script>
END;
		}
			break;

		case 'ajax_edit_photo': {
			$EE["head_extra3"][] = <<<END
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			AjaxEditPhoto = function(moduleId) {
				ajax_edit_photo = function(uri, elem_block) {
					var form = '<img src="'+uri+'" id="cropbox" /><form class="coords_form" action="\" method="post" onsubmit="return checkCoords();"><input type="hidden" id="pos_x" name="x" value="0" /><input type="hidden" id="pos_y" name="y" value="0" /><input type="hidden" id="width" name="width" value="200" /><input type="hidden" id="height" name="height" value="200" /><input type="submit" value="���������" /><input type="reset" value="������" /></form>';
					$.colorbox({
						html: form,
						transition: 'none',
						auto_size: true,
						close: false,
						onComplete: function(){
							var h = $('#cropbox').height();
							var w = $('#cropbox').width();
							if(h/w >= 1) {
								$('#cropbox').width('200px');
							} else {
								$('#cropbox').height('200px');
							}
							$('#cropbox').Jcrop({
								aspectRatio: 1,
								setSelect:   [ 0, 0, 200, 200 ],
								allowSelect: false,
								allowResize: false,
								allowMove: true,
								onSelect: function(c) {
									$('#pos_x').val(c.x);
									$('#pos_y').val(c.y);
									$('#width').val(c.w);
									$('#height').val(c.h);
								}
							});
							$('.coords_form input[type=submit]').click(function() {
								elem_block.find('em').removeClass('hidden');
								var request = {'module': moduleId, 'mode': 'html_code', 'params': 'ajax_edit_photo', 'data': {'pos_x': $('#pos_x').val(), 'pos_y': $('#pos_y').val(), 'width': $('#width').val(), 'height': $('#height').val(), 'img_uri': uri}};
								$.ajax({
									type: 'POST',
									url: '/ajax_mode/',
									data: request,
									success: function( data ) {
										elem_block.find('img').replaceWith(data);
										elem_block.find('em').addClass('hidden');
									}
								});
								$.colorbox.close();
								return false;
							});
							$('.coords_form input[type=reset]').click(function() {
								$.colorbox.close();
								return false;
							});
						}
					});
				}
				if($('#people_form .user_photo a').length) {
					$('#people_form .user_photo a').click(function() {
						var uri = $(this).attr('href');
						ajax_edit_photo(uri, $(this).parent());
						return false;
					});
				}
				if($('.edit_photo').length) {
					$('.edit_photo').click(function() {
						var uri = $(this).parent().find('.big_photo').attr('href');
						ajax_edit_photo(uri, $(this).parent());
						return false;
					});
				}
			};
		});
	</script>
END;
		}
			break;

		case 'speciality': {
			$EE["head_extra3"][] = <<<END
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			SpecialityChangeTitle = function() {
				var title_changed = false;
				change_title = function(elem, title, descr) {
					if (!title_changed) {
						hrefs = elem.getElementsByTagName('A');
						for (i=0; i < hrefs.length; i++) {
							hrefs[i].setAttribute('href', hrefs[i].getAttribute('href').replace( /title=(\S+)/, '&title='+encodeURI(title)));
							if (descr) {
								hrefs[i].setAttribute('href', hrefs[i].getAttribute('href').replace( /descr=(\S*)/, '&descr='+encodeURI(descr)));
							};
						};
						title_changed = true;
					};
				};
			};
		});
	</script>
END;
		}
			break;



		case 'timetable_make': {
			$EE["head_extra3"][] = <<<END
		<link media="print" type="text/css" href="/themes/styles/timetable_print.css" rel="stylesheet" />
		<script type="text/javascript" src="/scripts/cal.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function ($) {
				var edu_type;
				$("#edform").live("change", function() {
					$("input.select_group:checked").removeAttr("checked");
					if($(this).val()==2) {
						if($("#course").val()>0)
							$("tr.week").removeClass("hidden");
					}
					else {
						$("tr.week").addClass("hidden");
					}

				});
				$("#course").live("change", function() {//#fac,
					$("input.select_group:checked").removeAttr("checked");
					if($("#edform").val()==2) {
						$("tr.week").removeClass("hidden");
					}
					else {
						$("tr.week").addClass("hidden");
					}
				});


				$("input.select_group").live("click", function(){
					var gr_id = $(this).val();
					var path = "?gr="+gr_id+"";
					if($("#edform").val()==2) {
						var week = $("input:checked.week").val();
						path = path+"&week="+week+"";
					}
					document.location.href = path;
				});

				$("input.week").live("click", function() {
					var gr_id = "{$_GET['gr']}";
					if($("input.select_group:checked").length!=0)
						if(gr_id>0) {
							var week = $(this).val();
							var path = "?gr="+gr_id+""+"&week="+week+"";
							document.location.href = path;
						}
				});
				var selectedWk;
				$("input.week").live("change", function(){
					if($("#edform").val()==2)
						var max = 1;
					else
						var max = 4;
					var checkedGr = $(".groups_to_print:checked").length;
					var selectedGr="";
					if(checkedGr<=max) {
						$(".groups_to_print:checked").each(function(i){
							selectedGr += "z"+$(this).val();
						})
					}
					var selectedWk = "";
					$("input.week:checked").each(function(i){
						selectedWk += "z"+$(this).val();
					})
					if(selectedGr) {
						$("td.sel_gr").html("<a href=/timetable-print/?gr="+selectedGr+((selectedWk!="") ? "&weeks="+selectedWk : "")+">������� ����������</a>");
					}
					else {
						$("td.sel_gr").html((max>1) ? "�������� �� 1 �� "+max+" �����." : "��� �������� ��������� �������� ����� 1 ������.");
					}
				});


				selgroups = function() {
					if ($("#edform").val() && $("#fac").val() && $("#course").val()) {
						$(".groups").css("display", "none");
						$(".gr_" + $("#edform").val() + "_" + $("#fac").val() + "_" + $("#course").val()).css("display", "block");
					};
				};
				$("table.sel_gr select").live("change", function(){
					if($("#edform").val()==2)
						$("td.sel_gr").html("�������� 1 ������ � �� 4 ������.");
					else
						$("td.sel_gr").html("�������� �� 1 �� 4 �����.");
					$(".groups_to_print").removeAttr("checked");
				})
				$("input.groups_to_print").live("change", function(){
					if($("#edform").val()==2)
						var max = 1;
					else
						var max = 4;
					var checkedGr = $(".groups_to_print:checked").length;
					var selectedGr="";
					if(checkedGr<=max) {
						$(".groups_to_print:checked").each(function(i){
							selectedGr += "z"+$(this).val();
						})
					}
					var selectedWk = "";
					$("input.week:checked").each(function(i){
						selectedWk += "z"+$(this).val();
					})
					if(selectedGr) {
						$("td.sel_gr").html("<a href=/timetable-print/?gr="+selectedGr+((selectedWk!="" && $("#edform").val()==2) ? "&weeks="+selectedWk : "")+">������� ����������</a>");
					}
					else {
						$("td.sel_gr").html((max>1) ? "�������� �� 1 �� "+max+" �����." : "��� �������� ��������� �������� ����� 1 ������.");
					}
				});

				selgroups();
				$(".groups_to_print").removeAttr("checked");











				$("select.teach_list").live("hover", function(){
					var teachlist = $("#teachlist").children("option").clone(true);
					$(this).append(teachlist);
				});
				$("select.aud_list").live("hover", function(){
					var audlist = $("#audlist").children("option").clone(true);
					if($(this).children("option").length<=2) {
						$(this).children("option").remove();
						$(this).append(audlist);
					}
				});
				$("select.subjs").live("hover", function(){
					$(this).html($("#subjlist").html());
					if($(this).attr("def"))
						$(this).val($(this).attr("def"));
				});

				$("#timetable_save_button").click(function() {
					$("input.search").remove();
					$("select.teach_list").remove();
					$("select.aud_list").remove();
					$("input.comm").each(function(i){
					if(!$(this).val())
						$(this).remove();
					});
					$("select.subjs").each(function(i){
						if($(this).attr("def")=="")
							$(this).remove();
					});
					$("input.values[value='']").remove();
				});


				defgroup = function(edform,fac,course,id) {
					$("#edform").val(edform).change();
					$("#fac").val(fac).change();
					$("#course").val(course).change();
					$("input[name=groups][value=" + id + "]").attr('checked', true);

					selgroups();
				};

				select_filter = function(id,part,listname) {
					var first = "";

					$("#"+id).html($("#"+listname).html());

					$("#"+id+" option").each(function(i) {
						if(this.text.toLowerCase().indexOf(part.toLowerCase()) == -1)
							$(this).remove();
						else if (!first)
							first = this.value;
					});

					$("#"+id).val(first).change();
				};

				install_subjects = function() {
					$(".subjs").each(function(i) {
						$(this).html($("#subjlist").html());
						if($(this).attr("def"))
							$(this).val($(this).attr("def")).change();
					});
				};

				addtolist = function(type,id) {
					if(id) {
						$("#"+type+"_ids_"+id).val($("#"+type+"_ids_"+id).val() + $("#"+type+"_"+id).val() + ";");
						$("#"+type+"_names_"+id).html($("#"+type+"_names_"+id).html() + "<span id='" + type + "_id_" + $("#"+type+"_"+id).val() + "_" + id + "'>- " + $("#"+type+"_"+id+" option:selected").text() + "  [<a style='cursor:pointer;' onclick='delfromlist(\"" + type + "\", \"" + id + "\", " + $("#"+type+"_"+id).val() + ")'>x</a>]<br /></span>");
					}
				};

				delfromlist = function(type,id,del_id) {
					var arr = explode(';', $("#"+type+"_ids_"+id).val());
					arr.splice(arr.indexOf(""+del_id), 1);
					arr = arr.join(';');
					$("#" + type + "_id_" + del_id + "_" + id).remove();
					$("#"+type+"_ids_"+id).val(arr);
				};

				clear_row = function(id) {
				//	alert(id);
					$("#teach_names_"+id).html("");
					$("#aud_names_"+id).html("");
					$("#teach_ids_"+id).val("");
					$("#aud_ids_"+id).val("");
					$("#subj_"+id).val("");
					$("#subj_"+id).find("option").remove();
					$("#subj_"+id).attr("def", "");
					$("span#subgr_"+id).css("display", "none");
					$("#aud_"+id).val("");
					$("#type_"+id).val(0);
					$("#subgroup_"+id).val("");
					$("#teach_"+id).val("");
					$("#comment_from_"+id).val("");
					$("#comment_to_"+id).val("");
					$("#addcomm_"+id).val("");
				};

				show_doublepair = function(wid,id) {
					$('#tr_'+wid+'_'+id+'_double').toggle('normal');
					//$('#tr_'+wid+'_'+id+'_double').find("td").attr("bgcolor", "");
					$('#tr_'+wid+'_'+id+'_double').addClass("double");
					$('.rowspan_'+id).attr('rowspan', parseInt($('.rowspan_'+id).attr('rowspan'))+1);
					$('#addouble_'+wid+'_'+id).css('display','none');
					$('#deldouble_'+wid+'_'+id).css('display','block');
				};
				show_triplepair = function(wid,id) {
					$('#tr_'+wid+'_'+id+'_triple').toggle('normal');
					//$('#tr_'+wid+'_'+id+'_triple').find("td").attr("bgcolor", "");
					$('#tr_'+wid+'_'+id+'_triple').addClass("double");
					$('.rowspan_'+id).attr('rowspan', parseInt($('.rowspan_'+id).attr('rowspan'))+1);
					$('#adtriple_'+wid+'_'+id).css('display','none');
					$('#deldouble_'+wid+'_'+id).css('display','block');
				};

				hide_doublepair = function(wid,id) {
					clear_row(wid+'_'+id+"_d");
					$('#tr_'+wid+'_'+id+'_double').css('display','none');
					$('.rowspan_'+id).attr('rowspan', parseInt($('.rowspan_'+id).attr('rowspan'))-1);
					$('#addouble_'+wid+'_'+id).css('display','block');
				};
				hide_triplepair = function(wid,id) {
					clear_row(wid+'_'+id+"_t");
					$('#tr_'+wid+'_'+id+'_triple').css('display','none');
					$('.rowspan_'+id).attr('rowspan', parseInt($('.rowspan_'+id).attr('rowspan'))-1);
					$('#adtriple_'+wid+'_'+id).css('display','block');
				};

				function explode( delimiter, string ) {
					var emptyArray = { 0: '' };

					if ( arguments.length != 2
						|| typeof arguments[0] == 'undefined'
						|| typeof arguments[1] == 'undefined' )
					{
						return null;
					}

					if ( delimiter === ''
						|| delimiter === false
						|| delimiter === null )
					{
						return false;
					}

					if ( typeof delimiter == 'function'
						|| typeof delimiter == 'object'
						|| typeof string == 'function'
						|| typeof string == 'object' )
					{
						return emptyArray;
					}

					if ( delimiter === true ) {
						delimiter = '1';
					}

					return string.toString().split ( delimiter.toString() );
				}
////������� � ajax
//////////////////
//����������
	var izop_week = "{$_GET['week']}";
	$("select.subjs").live("change", function(){
		var id = $(this).val();
		var field_id = $(this).attr("id").split("_");
		var w = field_id[1];
		var d = field_id[2];
		var p = field_id[3];
		var clone = field_id[4];
		if(!clone) clone = 0;
		else if(clone=="d") clone = 1;
		else if(clone=="t") clone = 2;
		$(this).attr("def", id);
		var request = {'module': 5, 'mode': 'json', 'params': 'ajax_edit_timetable', 'data': {'mode': "subj", 'week': w, 'day': d, 'pair': p, 'izop_week': izop_week, 'data': {'id': id, 'n': clone}, 'group': "{$_GET['gr']}"}};
			$.ajax({
				type: 'POST',
				dataType: 'json',
				url: '/ajax_mode/',
				data: request,
				success: function( data ) {
					//alert(data);
				}
			});
	});
//////////////////
//��� �������
	$("select.type").live("change", function(){
		var id = $(this).val();
		var field_id = $(this).attr("id").split("_");
		var w = field_id[1];
		var d = field_id[2];
		var p = field_id[3];
		var clone = field_id[4];
		if(!clone) clone = 0;
		else if(clone=="d") clone = 1;
		else if(clone=="t") clone = 2;
		var request = {'module': 5, 'mode': 'json', 'params': 'ajax_edit_timetable', 'data': {'mode': "type", 'week': w, 'day': d, 'pair': p, 'izop_week': izop_week, 'data': {'id': id, 'n': clone}, 'group': "{$_GET['gr']}"}};
			$.ajax({
				type: 'POST',
				dataType: 'json',
				url: '/ajax_mode/',
				data: request,
				success: function( data ) {
					//alert(data);
				}
			});
	});
//////////////////
//���������
	$("input.subgr").live("click", function(){
		var id = $(this).val();
		var field_id = $(this).attr("name").split("_");
		var w = field_id[1];
		var d = field_id[2];
		var p = field_id[3];
		var clone = field_id[4];
		if(!clone) clone = 0;
		else if(clone=="d") clone = 1;
		else if(clone=="t") clone = 2;
		var request = {'module': 5, 'mode': 'json', 'params': 'ajax_edit_timetable', 'data': {'mode': "subgr", 'week': w, 'day': d, 'pair': p, 'izop_week': izop_week, 'data': {'id': id, 'n': clone}, 'group': "{$_GET['gr']}"}};
			$.ajax({
				type: 'POST',
				dataType: 'json',
				url: '/ajax_mode/',
				data: request,
				success: function( data ) {
					//alert(data);
				}
			});
	});
//////////////////
//���������	��������
	$("input.aud_add").live("click", function(){
		var field_id = $(this).attr("id").split("_");
		var w = field_id[1];
		var d = field_id[2];
		var p = field_id[3];
		var clone = field_id[4];
		if(!clone) clone = 0;
		else if(clone=="d") clone = 1;
		else if(clone=="t") clone = 2;
		var id = $("#aud_"+w+"_"+d+"_"+p+(field_id[4] ? "_"+field_id[4] : "")).val();
		var request = {'module': 5, 'mode': 'json', 'params': 'ajax_edit_timetable', 'data': {'mode': "aud_add", 'week': w, 'day': d, 'pair': p, 'izop_week': izop_week, 'data': {'id': id, 'n': clone}, 'group': "{$_GET['gr']}"}};
			$.ajax({
				type: 'POST',
				dataType: 'json',
				url: '/ajax_mode/',
				data: request,
				success: function( data ) {
					if(data.error)
						alert(data.error);
					else
						addtolist('aud',w+'_'+d+'_'+p+(field_id[4] ? "_"+field_id[4] : ""));
				}
			});
			return false;
	});
//���������	�������
	$("a.del_aud").live("click", function(){
		var field_id = $(this).attr("id").split("_");
		var id = field_id[1];
		var w = field_id[2];
		var d = field_id[3];
		var p = field_id[4];
		var clone = field_id[5];
		if(!clone) clone = 0;
		else if(clone=="d") clone = 1;
		else if(clone=="t") clone = 2;
		var request = {'module': 5, 'mode': 'json', 'params': 'ajax_edit_timetable', 'data': {'mode': "aud_del", 'week': w, 'day': d, 'pair': p, 'izop_week': izop_week, 'data': {'id': id, 'n': clone}, 'group': "{$_GET['gr']}"}};
			$.ajax({
				type: 'POST',
				url: '/ajax_mode/',
				data: request,
				success: function( data ) {

				}
			});
	});
//////////////////
//����������
	$("input.add_comment").live("click", function(){
		var b = $(this);
		var field_id = $(this).attr("id").split("_");
		var w = field_id[1];
		var d = field_id[2];
		var p = field_id[3];
		var clone = field_id[4];
		if(!clone) clone = 0;
		else if(clone=="d") clone = 1;
		else if(clone=="t") clone = 2;
		var text = $("#addcomm_"+w+"_"+d+"_"+p+(field_id[4] ? "_"+field_id[4] : "")).val();
		var request = {'module': 5, 'mode': 'json', 'params': 'ajax_edit_timetable', 'data': {'mode': "comm_add", 'week': w, 'day': d, 'pair': p, 'izop_week': izop_week, 'data': {'text': text, 'n': clone}, 'group': "{$_GET['gr']}"}};
			$.ajax({
				type: 'POST',
				dataType: 'json',
				url: '/ajax_mode/',
				data: request,
				success: function( data ) {
					if(data.error)
						alert(data.error);
					else {
						b.parent().find("p").remove();
						b.closest("td").append("<p>����������</p>");
						b.parent().find("p").fadeOut(3000);
					}

				}
			});
			return false;
	});
//////////////////
//�������������	��������
	$("input.teach_add").live("click", function(){
		var field_id = $(this).attr("id").split("_");
		var w = field_id[1];
		var d = field_id[2];
		var p = field_id[3];
		var clone = field_id[4];
		if(!clone) clone = 0;
		else if(clone=="d") clone = 1;
		else if(clone=="t") clone = 2;
		var id = $("#teach_"+w+"_"+d+"_"+p+(field_id[4] ? "_"+field_id[4] : "")).val();
		var request = {'module': 5, 'mode': 'json', 'params': 'ajax_edit_timetable', 'data': {'mode': "teach_add", 'week': w, 'day': d, 'pair': p, 'izop_week': izop_week, 'data': {'id': id, 'n': clone}, 'group': "{$_GET['gr']}"}};
			$.ajax({
				type: 'POST',
				dataType: 'json',
				url: '/ajax_mode/',
				data: request,
				success: function( data ) {
					if(data.error)
						alert(data.error);
					else
						addtolist('teach',w+'_'+d+'_'+p+(field_id[4] ? "_"+field_id[4] : ""));
				}
			});
			return false;
	});
//�������������	�������
	$("a.del_teach").live("click", function(){
		var field_id = $(this).attr("id").split("_");
		var id = field_id[1];
		var w = field_id[2];
		var d = field_id[3];
		var p = field_id[4];
		var clone = field_id[5];
		if(!clone) clone = 0;
		else if(clone=="d") clone = 1;
		else if(clone=="t") clone = 2;
		var request = {'module': 5, 'mode': 'json', 'params': 'ajax_edit_timetable', 'data': {'mode': "teach_del", 'week': w, 'day': d, 'pair': p, 'izop_week': izop_week, 'data': {'id': id, 'n': clone}, 'group': "{$_GET['gr']}"}};
			$.ajax({
				type: 'POST',
				dataType: 'json',
				url: '/ajax_mode/',
				data: request,
				success: function( data ) {

				}
			});
	});
//////////////////
//������ ����
//��. /scripts/cal.js     213

//////////////////
//���������� ����
	$("input.copy").live("click", function(){
		var field_id = $(this).attr("id").split("_");
		var w = field_id[1];
		var d = field_id[2];
		var p = field_id[3];
		var clone = field_id[4];
		if(!clone) clone = 0;
		else if(clone=="d") clone = 1;
		else if(clone=="t") clone = 2;
		var id = $("."+$(this).attr("id")).val();
		var request = {'module': 5, 'mode': 'json', 'params': 'ajax_edit_timetable', 'data': {'mode': "copy", 'izop_week': izop_week, 'week': w, 'day': d, 'pair': p, 'data': {'id': id, 'n': clone}, 'group': "{$_GET['gr']}"}};
			$.ajax({
				type: 'POST',
				dataType: 'json',
				url: '/ajax_mode/',
				data: request,
				success: function( data ) {
					if(data.error)
						alert(data.error);
					if(data.good)
						$(".output_"+w+"_"+d+"_"+p+(field_id[4] ? "_"+field_id[4] : "")).html(data.good);
				}
			});
		return false;
	});
	$("td.show_button").live("mouseover", function(){
		$(this).find("input.copy").stop().animate({opacity:'1'},700);
	});
	$("td.show_button").live("mouseout", function(){
		$(this).find("input.copy").stop().animate({opacity:'0'},700);
	});
//////////////////
//�������� ����
	$("img.clear_pair").live("click", function(){
		var field_id = $(this).attr("id").split("_");
		var w = field_id[1];
		var d = field_id[2];
		var p = field_id[3];
		var clone = field_id[4];
		if(!clone) clone = 0;
		else if(clone=="d") clone = 1;
		else if(clone=="t") clone = 2;
		var request = {'module': 5, 'mode': 'json', 'params': 'ajax_edit_timetable', 'data': {'mode': "clear", 'izop_week': izop_week, 'week': w, 'day': d, 'pair': p, 'data': {'n': clone}, 'group': "{$_GET['gr']}"}};
			$.ajax({
				type: 'POST',
				dataType: 'json',
				url: '/ajax_mode/',
				data: request,
				success: function( data ) {
					if(data.error)
						alert(data.error);
					//if(data.good)
					//	$(".output_"+w+"_"+d+"_"+p+(field_id[4] ? "_"+field_id[4] : "")).html(data.good);
				}
			});
		return false;
	});
//////////////////
//������
//��. /scripts/cal.js     213



				$('#actual_from').simpleDatepicker();
				$('#actual_to').simpleDatepicker();
				$('.comm').simpleDatepicker();
			});

			function subgroup_switch(type,a1,a2,a3,d) {	//no jquery for using before the full page is loaded
				var is_d = '';
				if (d==1) is_d = '_d';
				if (d==2) is_d = '_t';
				if (type == 1 || type == 2)
					document.getElementById("subgr_"+a1+"_"+a2+"_"+a3+is_d).style.display = "block";
				else
					document.getElementById("subgr_"+a1+"_"+a2+"_"+a3+is_d).style.display = "none";
			}
		</script>
END;
		}
			break;

		case 'timetable_graphics': {
			$EE["head_extra3"][] = <<<END
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			SendformGraphics = function() {
				sendform_graphics = function(form) {
	                if (form.subject_id.value == 0) {
	                    alert('����������, �������� �������');
	                    form.subject_id.focus();
	                    return false;
	                };
	                if (form.groups.value == "") {
	                    alert('����������, ������� ������ �����');
	                    form.groups.focus();
	                    return false;
	                };
	                if (form.kol_groups.value == "") {
	                    alert('����������, ������� ���������� �����');
	                    form.kol_groups.focus();
	                    return false;
	                };
	                if (form.lecture_hours.value == "") {
	                    alert('����������, ������� ���������� ���������� �����');
	                    form.lecture_hours.focus();
	                    return false;
	                };
	                if (form.practice_hours.value == "") {
	                    alert('����������, ������� ���������� ����� ������������ �������');
	                    form.practice_hours.focus();
	                    return false;
	                };
	                return true;
	            };
			};
		});
	</script>
END;
		}
			break;

		case 'sendform': {
			$EE["head_extra3"][] = <<<END
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			Send_Forms = function() {
				sendform = function(form) {
					if (form.last_name.value == "") {
						alert('����������, ������� ������� ��������');
						form.last_name.focus();
						return false;
					};
					if (form.first_name.value == "") {
						alert('����������, ������� ��� ��������');
						form.name.focus();
						return false;
					};
					if (form.patronymic.value == "") {
						alert('����������, ������� �������� ��������');
						form.patronymic.focus();
						return false;
					};
					if (form.username.value == "") {
						alert('����������, ������� ��� ������������ ��������');
						form.patronymic.focus();
						return false;
					};
					if (form.email.value == "") {
						alert('����������, ������� E-mail ��������');
						form.email.focus();
						return false;
					};
					if (form.password1.value == "") {
						alert('����������, ������� ������ ��������');
						form.password1.focus();
						return false;
					};
					if (form.password1.value != form.password2.value) {
						alert('��������� ������ �� ���������');
						form.password1.focus();
						return false;
					};
					return true;
				};
			};
		});
	</script>
END;
		}
			break;

		case 'employees': {
			$EE["head_extra3"][] = <<<END
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$("input.teacher_search").live("keyup", function() {
				var needle = $(this).val();
				// if(needle.length>2) {
					var request = {'module': 5, 'mode': 'html_code', 'params': 'employees_ajax_search', 'data': {'needle': needle} };
					$.ajax({
						type: 'POST',
						dataType: 'html',
						url: '/ajax_mode/',
						data: request,
						success: function( data ) {

							$("div.teachers").html(data);
						}
					});
				// }
			});
		});
	</script>
END;
		}
			break;
		case 'students_progress': {
			$EE["head_extra3"][] = <<<END
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$("select.change_semester").live("change", function() {
				var s = $(this).find("option:selected").val();
				var request = {'module': 5, 'mode': 'html_code', 'params': 'ajax_students_progress', 'data': {'s': s} };
				$.ajax({
					type: 'POST',
					dataType: 'html',
					url: '/ajax_mode/',
					data: request,
					success: function( data ) {
						$("div.progress_content").html(data);
					}
				});
			});
		});
	</script>
END;
		}
			break;
		case 'students_upload': {
			$EE["head_extra3"][] = <<<END
	<script type="text/javascript">
		jQuery(document).ready(function($) {
            // $("input[value='���������']").live("dblclick", function() {
            	// alert("1");
            	// return false;
            // });
			if("{$MODULE_DATA['output']['download_files'][1]}" != "")
				location.href="{$MODULE_DATA['output']['download_files'][1]}";
			if("{$MODULE_DATA['output']['download_files'][2]}" != "")
				location.href="{$MODULE_DATA['output']['download_files'][2]}";
			if("{$MODULE_DATA['output']['download_files'][3]}" != "")
				location.href="{$MODULE_DATA['output']['download_files'][3]}";
		});
	</script>
END;
		}
			break;

		case '1':
			echo "loooool";
			break;

		case 'student_work': {
			$EE["head_extra3"][] = <<<END
<script src="/scripts/jquery/jquery.maskedinput.js" type="text/javascript"></script>
<script type="text/javascript" src="/scripts/jquery/jquery.autocomplete.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function($) {

			$("#file_form_file").live("change", function()
			{
				if ((this.value.split('.').pop()!="pdf") && (this.value.split('.').pop()!="PDF")) 
				{
					alert('��������� ����� ������ pdf ����.');
					$(this).val("");
				}
			});		

			$("#file_form_feedback").live("change", function()
			{
				if ((this.value.split('.').pop()!="pdf") && (this.value.split('.').pop()!="PDF"))  
				{
					alert('��������� ����� ������ pdf ����.');
					$(this).val("");
				}
			});	

			
			$("a.add_student_work").click(function(e){
				var request = {'module': 5, 'mode': 'html_code', 'params': 'sw_add_form', 'data': {'id': 1} };
				$.ajax({
					type: 'POST',
					dataType: 'html',
					url: '/ajax_mode/',
					data: request,
					success: function( data ) {
						$("div.sw_add_form").html(data);
						$(".date_year").mask("9999",{placeholder:"����"});
					}
				});
				$(".overlay, .sw_add_form").toggleClass("hidden");
			});
			$("div.close, div.overlay, input[data-action=close]").live("click", function() {
				$(".overlay, .sw_add_form, .sw_edit_form").addClass("hidden").html("");
				// $("div.sw_add_form");
				return false;
			});
			$("a.edit").live("click", function() {
				var id = $(this).attr("data-id");
				var request = {'module': 5, 'mode': 'html_code', 'params': 'sw_edit_form', 'data': {'id': id} };
				$.ajax({
					type: 'POST',
					dataType: 'html',
					url: '/ajax_mode/',
					data: request,
					success: function( data ) {
						$("div.sw_edit_form").html(data);
						$(".date_year").mask("9999",{placeholder:"����"});
					}
				});
				$(".overlay, .sw_edit_form").toggleClass("hidden");
				return false;
			});

			$("a.delete").live("click", function() {
				var id = $(this).attr("data-id");
				if(!confirm('�� �������, ��� ������ ������� ������? ')) return false;
				var request = {'module': 5, 'mode': 'html_code', 'params': 'sw_delete_work', 'data': {'id': id} };
				$.ajax({
					type: 'POST',
					dataType: 'html',
					url: '/ajax_mode/',
					data: request,
					success: function( data ) 
					{

					}
				});
				$("tr#"+id).remove();
				//$(this).closest("div").remove();
				return false;
			});
			$("input.sw_group").live("keyup", function() {
				var needle = $(this).val();
				if(needle.length>2) {
					var request = {'module': 5, 'mode': 'html_code', 'params': 'sw_group_search', 'data': {'needle': needle} };
					$.ajax({
						type: 'POST',
						dataType: 'html',
						url: '/ajax_mode/',
						data: request,
						success: function( data ) {
							$("div.sw_search_group_content").html(data);
							var curId = $("div.sw_search_group_content").find("select.sw_group_select").find("option:selected").val();
							var request = {'module': 5, 'mode': 'html_code', 'params': 'sw_group_select', 'data': {'id': curId} };
							$.ajax({
								type: 'POST',
								dataType: 'html',
								url: '/ajax_mode/',
								data: request,
								success: function( data ) {
										$("div.sw_select_group_content").html(data);
								}
							});
						}
					});
				} else {
					$(".sw_select_group_content, .sw_search_group_content").html("");
				}
			});
			$("select.sw_group_select").live("change", function() {
				var id = $(this).find("option:selected").val();
				var request = {'module': 5, 'mode': 'html_code', 'params': 'sw_group_select', 'data': {'id': id} };
				$.ajax({
					type: 'POST',
					dataType: 'html',
					url: '/ajax_mode/',
					data: request,
					success: function( data ) {
							$("div.sw_select_group_content").html(data);
					}
				});

			});


			$("select.sw_type").live("change", function() {

				var type = $(this).find("option:selected").val();
				if(type==34 || type==35 || type==36) {
					$("div.sw_subj_container").removeClass("hidden");
				}
				else {
					if($("div.sw_subj_container").attr("class")=="sw_subj_container")
						$("div.sw_subj_container").addClass("hidden");
				}
			});
							ajaxSearchSubj = function(moduleId) {
				var field = $('#subject_list').find('option');
				$('#subject_search').autocomplete({
					delay: 500,
					source: function(request, response) {
						//if(request.term.length < 3) return;
						var request = {'module': moduleId, 'mode': 'json', 'params': 'ajax_autocomplete_subj', 'data': {'q': request.term} };

						$.ajax({
						'url': '/ajax_mode/',
						dataType: 'json',
						data: request,
						success: function(json) {
								response($.map(json, function(item) {

								return {
									label: item.name,
									value: item.id
								}
							}));
						}
					})
					},
					select: function(event, ui) {
						var title = ui.item.label.split(' > ');
						title = title[title.length - 1];
						$('#subject_search').attr('value', title);
						$('#subject_list').html("<option value='"+ui.item.value+"'>"+title+"</option>");
						$('#subject_list').val(ui.item.value);
						$('#subject_list, #subject_search + a').show();
						$('#subject_search').hide();

						return false;
					},
					focus: function(event, ui) {
						//$('#subject_list, #subject_search + a').show();
						//$('#subject_search').hide();
						return false;
				    }
				});

				$('#subject_search').bind('keypress', function(e) {
					if(e.keyCode == 27 || e.keyCode == 13) {
						$('#subject_list, #subject_search + a').show();
						$('#subject_search').hide();
					}

					if(e.keyCode == 13) {
						e.preventDefault();
						return false;
					}
				});
			}
ajaxSearchSubj(7);


				$(".sw_department").live("change", function()
				{
					var id = $(this).val();
		            var request = {'module': 5, 'mode': 'json', 'params': 'ajax_get_disc', 'data': {'id': id} };      
		            $.ajax(
		            {
						'url': '/ajax_mode/',
						dataType: 'json',
						data: request,
		                //async: false,
		                success: function (data) 
		                {
		                	if (data) 
		                	{
		                		$('#esubject_list').empty();
		                		$.each(data, function(id,data)
			                    {
			                    	$('#esubject_list').append($('<option>', { value : id }).text(data));
			                    });
		                	}

		                }
		            });
				});

				$("#esubject_search").live("keyup", function() {
					$("#esubject_search").trigger("subj_search_event");
				});
				$("#esubject_search").live("subj_search_event", function(e) {
					var field = $('#esubject_list').find('option');
		
					$('#esubject_search').autocomplete({



					delay: 500,
					source: function(request, response) {
						//if(request.term.length < 3) return;
						var request = {'module': 7, 'mode': 'json', 'params': 'ajax_autocomplete_subj', 'data': {'q': request.term} };

						$.ajax({
						'url': '/ajax_mode/',
						dataType: 'json',
						data: request,
						success: function(json) {
								response($.map(json, function(item) {

								return {
									label: item.name,
									value: item.id
								}
							}));
						}
					})
					},
					select: function(event, ui) {
						var title = ui.item.label.split(' > ');
						title = title[title.length - 1];
						$('#esubject_search').attr('value', title);
						$('#esubject_list').html("<option value='"+ui.item.value+"'>"+title+"</option>");
						$('#esubject_list').val(ui.item.value);
						$('#esubject_list, #esubject_search + a').show();
						$('#esubject_search').hide();


						return false;
					},
					focus: function(event, ui) {
						//$('#subject_list, #subject_search + a').show();
						//$('#subject_search').hide();
						return false;
				    }
				});


});


			//downloader///////////////
			$("p.add_file").live("click", function() {
				$("input.file_form").toggleClass("hidden");
			});

			// $("input.file_form").live("click", function() {
				// $("input.file_form").toggleClass("hidden");
				// $("span.red").remove();
			// });

			$("input.file_form").live("change", function() 
			{
				$(this).clone().insertAfter($("p.add_file"));
				var input = $(this).removeClass("file_form").addClass("hidden");
				input.attr("name", "files[]");//
				var filename = $(this)[0]["files"][0]["name"];
				console.log($(this).files);
				console.log($(this));
				var row = $(".file_form_row.tmp.hidden").clone();
				row.removeClass("hidden").removeClass("tmp");
				row.append(input);
				row.find("span.descr").html(filename);
				$("div.files_form").append(row);
				$("span.red.file_err_msg").remove();
			});

			$("span.del_file").live("click", function() {
				$(this).closest("div").remove();
				return false;
			});
			$("span.del_loaded_file").live("click", function() {
				var id = $(this).attr("data-id");
				if(!confirm('�� �������, ��� ������ ������� ����? ')) return false;
				$("div.form").append("<input type='hidden' value='"+id+"' class='file_for_del' name='files_for_del[]'/>");

				var request = {'module': 5, 'mode': 'html_code', 'params': 'sw_delete_file', 'data': {'id': id} };
				$.ajax({
					type: 'POST',
					dataType: 'html',
					url: '/ajax_mode/',
					data: request,
					success: function( data ) {

					}
				});
				$(this).closest("div").remove();
				return false;
			});
			/////////////////////////
			$(".cl_teacher_search").live("click", function(){
				if($(".teacher_print:visible").length > 0) {
					$(".teacher_print, .teacher_list").hide();
					$(".teacher_search").show();
				} else {
					$(".teacher_search, .teacher_list").toggle();
				}
			});
			$(".cl_teacher_print").live("click", function(){
				if($(".teacher_search:visible").length > 0) {
					$(".teacher_list, .teacher_search").hide();
					$(".teacher_print").show();
				} else {
					$(".teacher_print, .teacher_list").toggle();
				}
			});
			$(".cl_student_print").live("click", function(){
				$(".student_print, .sw_student_fio").toggle();
			});
			/////////////////////////





				$(".teacher_search").live("keyup", function() {
					$(".teacher_search").trigger("teacher_search_event");
				});
				$(".teacher_search").live("teacher_search_event", function(e) {

					$('.teacher_search').autocomplete({
						delay: 500,
						source: function(request, response) {
							var request = {'module': 7, 'mode': 'json', 'params': 'ajax_search_autor', 'data': {'s': request.term} };
							$.ajax({
							'url': '/ajax_mode/',
							dataType: 'json',
							data: request,
							success: function(json) {

									response($.map(json, function(item) {
									return {
										label: item.name,
										value: item.people_id
									}
								}));
							}
						})
						},
						select: function(event, ui) {
							var title = ui.item.label.split(' > ');
							title = title[title.length - 1];
							$('.teacher_list').html("<option value='"+ui.item.value+"'>"+ui.item.label+"</option>");
							$('.teacher_list, .subject_search + a').show();
							$('.teacher_search').hide();
							$('.teacher_print').attr("value", "").hide();
							return false;
						},
						focus: function(event, ui) {
							//$(this).parent().find('.subject_list').val(ui.item.value);
							//$(this).parent().find('.subject_list, .subject_search + a').show();
							//$(this).parent().find('.subject_search').hide();

									return false;
							}
					});
				});


			// $("input[data-action=close]").live("click", function() {
			// 	return false;
			// });

			$("input.check_form").live("click", function() {
				var error = false;

				if($(".file_form_row:not(.tmp)").length == 0) {
					$("div.files_form").html("<span class='red file_err_msg'>���� �� ������</span>");
					error = true;
				}

				var year = $("input.date_year").val();
				if(year<2000 || year>2020) {
					$("span.date_error").html("������� ������ ���");
					error = true;
				} else {
					$("span.date_error").html("");
				}

				var wtype = $(".sw_type").find("option:selected").val();
				var wstudent = $(".sw_student_fio").find("option:selected").val();
				var wname = $(".sw_name").val();


				var request = {'module': 5, 'mode': 'json', 'params': 'sw_check_work_exist', 'data': {'student_id': wstudent, 'work_type': wtype, 'work_name': wname} };
				$.ajax({
					type: 'POST',
					dataType: 'json',
					url: '/ajax_mode/',
					data: request,
					success: function( data ) {
						if(data==1) {
							// $("p.red.err_msg").html("������ ������ ��� ���������.").removeClass("hidden");
							error = true;
						} else {
							$("p.red.err_msg").html("").addClass("hidden");
						}
					}
				});






				if(error) {
					//return false;
				}
			});





		});


	</script>
END;
		}
			break;

		case 'student_registr' : {
			$EE["head_extra3"][] = <<<END
	<script type="text/javascript">
  jQuery(document).ready(function($) {
    generate_password = function(form, syllableNum, numPass, useNums) {
      if(typeof(form) == 'string') form = document.forms[form];
      syllableNum = typeof(syllableNum) != 'undefined' ? syllableNum : 3;
      numPass = typeof(numPass) != 'undefined' ? numPass : 10;
      useNums = typeof(useNums) != 'undefined' ? useNums : true;
      console.log(form);
      console.log(form.password1.value);
      console.log(form.password2.value);

      rand = function(from, to) {
          from = typeof(from) != 'undefined' ? from : 0;    // ���������
          to = typeof(to) != 'undefined' ? to : from + 1;    // �� ���������
          return Math.round(from + Math.random()*(to - from));
      };

      getRandChar = function(a) {
          return a.charAt(rand(0,a.length-1));
      }


      form.getByClass = function(className, index) {
        index = typeof(index) != "undefined" ? index : 0;
        if(this.querySelectorAll) return this.querySelectorAll("."+className)[index];
        return this.getElementsByClassName(className)[index];
      }

      var cCommon = "";
      var cAll = "bcdfghklmnprstvzjqwx";    // ��� ���������
      var vAll = "aeiouy";    // ��� �������
      var lAll = cAll + vAll;    // ��� �����

      if (form.password1.value)
        form.getByClass('instead').innerHTML = " ������ " + form.password1.value;
      else
        form.getByClass('instead').innerHTML = "";

      var elem = form.getByClass('genpass');
      elem.innerHTML = "";
      for(var j = 0; j < numPass; ++j) {
          // ����������� ������������ ����������� ��������� ����� ����� �������
          var numProb = -1, numProbStep = 0.25;
          for(var i = 0; i < syllableNum; ++i) {
              if(Math.round(Math.random())) {
                  elem.innerHTML +=   getRandChar(vAll) +
                                      getRandChar(lAll);
              } else {
                  elem.innerHTML += getRandChar(vAll);
              }
              if(useNums && Math.round(Math.random() + numProb)) {
                  elem.innerHTML += rand(0,9);
                  numProb += numProbStep;
              }
          }
      if (numPass > 1)
        elem.innerHTML += "<br />";
      }
      form.getByClass('copypass').style.display = 'block';

      copypass = function() {
        var password = form.getElementsByClassName('genpass')[0].innerHTML;
        console.log(password);
        form.password1.value = password;
        form.password2.value = password;

        form.getByClass('copypass').style.display = 'none';
      }
    }
  });
  </script>
END;
		}
			break;
	}
}


if (isset($MODULE_DATA["output"]["mode"]) && $MODULE_DATA["output"]["mode"]=="timetable_subjects") {
	/*	$EE["head_extra3"][] = "
			 <script type='text/javascript'>
			jQuery(document).ready(function() {
				if(jQuery('.subj_list .delet_subj').length) {
					jQuery('.subj_list .delet_subj').click (function() {
						var subj_id = jQuery(this).attr('href');
						subj_id = subj_id.split('/');
						if(subj_id[0] == 'delete') {
							subj_id = subj_id[1];
							var request = {'module': ".$MODULE_OUTPUT['module_id'].", 'page_uri': '".$Engine->EngineUri()."', 'mode': 'html_code', 'params': 'ajax_delete_subj', 'data': {'subj_id': subj_id} };
							jQuery.ajax({
								type: 'POST',
								url: '/ajax_mode/',
								data: request,
								success: function( data ) {
									if(!(!data || !data.documentElement)) {
										var rootNodeName = data.documentElement.nodeName;
										if(rootNodeName != 'parsererror') {
											xmlDocElem = data.documentElement;
											if(rootNodeName == 'del_subj') {
												var process = xmlDocElem.firstChild.data;
												if(process == 1) {
													jQuery('#subj_item_'+subj_id).remove();
												} else {
													alert('������ ��������. �� ���������� ������� ���������.');
												}
											}
										}
									}
								}
							});
						}
						return false;
					});
				}
			  });
			</script>
		";*/
}
if (isset($MODULE_OUTPUT["mode"]) && $MODULE_OUTPUT["mode"]=="curriculum" && isset($MODULE_OUTPUT['curriculum_allowed']) && $MODULE_OUTPUT['curriculum_allowed']) {
	/*$EE["head_extra3"][] = "
	 	<script type='text/javascript'>
		jQuery(document).ready(function() {
			if(jQuery('#curriculum .subject_group_item form').length) {
				jQuery('#curriculum .subject_group_item label input').click(function() {
					if(jQuery(this).parents('.edit_form').hasClass('del_sgroup')) {
						jQuery(this).parents('.edit_form').removeClass('del_sgroup').addClass('edit_sgroup');
					} else {
						jQuery(this).parents('.edit_form').addClass('del_sgroup').removeClass('edit_sgroup');
					}
				});
			}
    		if(jQuery('#curriculum .select_subject').length) {
				jQuery('#curriculum .select_subject li dd span').text(jQuery('#curriculum .select_sgroup').find(':selected').attr('title')+'.');
				jQuery('#curriculum .select_sgroup').change(function() {
					jQuery('#curriculum .select_subject li dd span').text(jQuery(this).find(':selected').attr('title')+'.');
				});
				jQuery('#curriculum .select_subject dl').hide();
				jQuery('#curriculum .select_subject li > label').click(function() {
					jQuery(this).next('dl').slideToggle();
				});
				jQuery('#curriculum .select_subject dt label input:checked').parent().parent().addClass('checked').next('dd').addClass('checked');
				jQuery('#curriculum .select_subject dt label').click(function() {
					if(jQuery(this).find('input').is(':checked')) {
						jQuery(this).parent().addClass('checked').next('dd').addClass('checked');
					} else {
						jQuery(this).parent().removeClass('checked').next('dd').removeClass('checked');
					}
				});
			}
  		});
		</script>";*/
}

if (isset($MODULE_DATA["output"]["mode"]) && $MODULE_DATA["output"]["mode"]=="timetable_auditorium") {
	/*$EE["head_extra3"][] = "
	<script>
		$(document).ready(function() {
			$('.editable-select').editableSelect({
				bg_iframe: true,
				onSelect: function(list_item) {
					this.select.get(0).options[list_item.index()].selected = 'selected';
				}
			});
		});
	</script>";*/
}

if (isset($MODULE_DATA["output"]["mode"]) && $MODULE_DATA["output"]["mode"]=="people") {
	/*$EE["head_extra3"][] = "<script type=\"text/javascript\" src=\"" . HTTP_COMMON_SCRIPTS . "highslide/highslide.js\"></script>
	   <script type=\"text/javascript\">
		   hs.graphicsDir = '" . HTTP_COMMON_SCRIPTS . "highslide/graphics/';
		   hs.showCredits = false;
		   hs.fullExpandTitle = '������ ������';
		   hs.restoreTitle = '������ ����� ��������� �����������, �������-������� ������������ ��� �������� � ����������� � ���������� �����������';
		   hs.loadingText = '��������&hellip;';
		   hs.loadingTitle = 'ٸ������ ����� ��� ������';
		   hs.allowMultipleInstances = false;
	   </script>";*/
}

if (isset($MODULE_DATA["output"]["mode"]) && ($MODULE_DATA["output"]["mode"]=="timetable_subjects_by_departments" || $MODULE_DATA["output"]["mode"]=="groups")) {
	/*$EE["head_extra3"][] = "<script type='text/javascript' src='" .HTTP_COMMON_SCRIPTS. "calendar.js'></script>
	<script type=\"text/javascript\">
		function showCalendarField(e, fieldObj)
			{
				e.cancelBubble=true;
				fieldObj.select();
				lcs(fieldObj, false);
			}
	</script>
	";*/
}

if ($MODULE_OUTPUT["allow_peopledit"] && isset($MODULE_DATA["output"]["mode"]) && ($MODULE_DATA["output"]["mode"]=="add_people" || $MODULE_DATA["output"]["mode"]=="edit_people" || $MODULE_DATA["output"]["mode"]=="global_people")) {
	/*$EE["head_extra3"][] = "
		<script type='text/javascript' src='".HTTP_COMMON_SCRIPTS."jquery/jquery.colorbox.js'></script>
		<link rel='stylesheet' href='".$EE["http_styles"]."colorbox.css' type='text/css' media='screen' />
		<script src='".HTTP_COMMON_SCRIPTS."jquery/jquery.Jcrop.min.js'></script>
		<link rel='stylesheet' href='".$EE["http_styles"]."jquery.Jcrop.css' type='text/css' />
		<script type='text/javascript'>
			jQuery(document).ready(function() {
				function ajax_edit_photo(uri, elem_block) {
					var form = '<img src=\"'+uri+'\" id=\"cropbox\" /><form class=\"coords_form\" action=\"\\\" method=\"post\" onsubmit=\"return checkCoords();\"><input type=\"hidden\" id=\"pos_x\" name=\"x\" value=\"0\" /><input type=\"hidden\" id=\"pos_y\" name=\"y\" value=\"0\" /><input type=\"hidden\" id=\"width\" name=\"width\" value=\"200\" /><input type=\"hidden\" id=\"height\" name=\"height\" value=\"200\" /><input type=\"submit\" value=\"���������\" /><input type=\"reset\" value=\"������\" /></form>';
					jQuery.colorbox({
						html: form,
						transition: 'none',
						auto_size: true,
						close: false,
						onComplete: function(){
							var h = jQuery('#cropbox').height();
							var w = jQuery('#cropbox').width();
							if(h/w >= 1) {
								jQuery('#cropbox').width('200px');
							} else {
								jQuery('#cropbox').height('200px');
							}
							jQuery('#cropbox').Jcrop({
								aspectRatio: 1,
								setSelect:   [ 0, 0, 200, 200 ],
								allowSelect: false,
								allowResize: false,
								allowMove: true,
								onSelect: function(c) {
									jQuery('#pos_x').val(c.x);
									jQuery('#pos_y').val(c.y);
									jQuery('#width').val(c.w);
									jQuery('#height').val(c.h);
								}
							});
							jQuery('.coords_form input[type=submit]').click(function() {
								elem_block.find('em').removeClass('hidden');
								var request = {'module': ".$MODULE_OUTPUT['module_id'].", 'page_uri': '".$Engine->EngineUri()."', 'mode': 'html_code', 'params': 'ajax_edit_photo', 'data': {'pos_x': jQuery('#pos_x').val(), 'pos_y': jQuery('#pos_y').val(), 'width': jQuery('#width').val(), 'height': jQuery('#height').val(), 'img_uri': uri}};
								jQuery.ajax({
									type: 'POST',
									url: '/ajax_mode/',
									data: request,
									success: function( data ) {
										elem_block.find('img').replaceWith(data);
										elem_block.find('em').addClass('hidden');
									}
								});
								jQuery.colorbox.close();
								return false;
							});
							jQuery('.coords_form input[type=reset]').click(function() {
								jQuery.colorbox.close();
								return false;
							});
						}
					});
				}
				if(jQuery('#people_form .user_photo a').length) {
					jQuery('#people_form .user_photo a').click(function() {
						var uri = jQuery(this).attr('href');
						ajax_edit_photo(uri, jQuery(this).parent());
						return false;
					});
				}
				if(jQuery('.edit_photo').length) {
					jQuery('.edit_photo').click(function() {
						var uri = jQuery(this).parent().find('.big_photo').attr('href');
						ajax_edit_photo(uri, jQuery(this).parent());
						return false;
					});
				}

				if(jQuery('#people_cat_select').length) {
					jQuery('#people_form').attr('class', 'people_cat_'+jQuery('#people_cat_select').val());
					if(jQuery('#people_form #responsible input').is(':checked') || jQuery('#people_cat_select').val() == 3) {
						jQuery('#usergroup').removeClass('hidden');
					} else {

					}
					jQuery('#people_form .add_status input').removeAttr('checked');
					jQuery('#people_form .add_status .people_cat_'+jQuery('#people_cat_select').val()+':first input').attr({'checked':'checked'});

					jQuery('#people_cat_select').change(function() {
						jQuery('#people_form').attr('class', 'people_cat_'+jQuery(this).val());

						jQuery('#people_form .add_status input').removeAttr('checked');
						jQuery('#people_form .add_status .people_cat_'+jQuery('#people_cat_select').val()+':first input').attr({'checked':'checked'});
						if(jQuery('#people_cat_select').val() == 3 || (jQuery('#people_form #responsible input').is(':checked') && jQuery('#people_cat_select').val() == 2)) {
							jQuery('#usergroup').removeClass('hidden');
						} else {
							jQuery('#usergroup').addClass('hidden');
							jQuery('#people_form #responsible input').removeAttr('checked');
						}
					});
				}

				jQuery('#people_form #responsible input').click(function() {
					if(jQuery(this).is(':checked')) {
						jQuery('#usergroup').removeClass('hidden');
					} else {
						jQuery('#usergroup').addClass('hidden');
					}
				});
				jQuery('#department_select dd').click(function() {
					jQuery('#department_select dd').removeClass('selected');
					jQuery(this).addClass('selected');
				}).dblclick(function() {
					if(!jQuery(this).hasClass('hidden')) {
						var option = jQuery(this);
						var option_val = option.attr('id').split('_');
						option_val = option_val[1];

						jQuery('#department_list').append('<li><input type=hidden name=department['+option_val+'] value='+option_val+' />'+option.text()+'<em class=\"inline-block\"></em></li>');
						option.addClass('hidden');
						option.removeClass('selected');
					}
				});
				jQuery('#department_list li em').live('click', function(){
					var dep_val = jQuery(this).parent().find('input').attr('value');
					var option = jQuery('#department_select #depid_'+dep_val);
					option.removeClass('hidden');
					jQuery(this).parent().remove();
				});
			});
		</script>";*/
}


/* $EE["head_extra3"][] = "<script type=\"text/javascript\">
 	var title_changed = false;
	function change_title(elem, title, descr) {
		if (!title_changed) {
			hrefs = elem.getElementsByTagName('A');
			for (i=0; i < hrefs.length; i++) {
				hrefs[i].setAttribute('href', hrefs[i].getAttribute('href').replace( /title=(\S+)/, '&title='+encodeURI(title)));
				if (descr)
					hrefs[i].setAttribute('href', hrefs[i].getAttribute('href').replace( /descr=(\S*)/, '&descr='+encodeURI(descr)));
			}
			title_changed = true;
		}
	}

	function set_handlers() {
			yandexer = document.getElementById('yandex_icons');
			if (yandexer) {
				if (yandexer.addEventListener) {
					yandexer.addEventListener('mouseover', change_title, false);
				} else {
					yandexer.attachEvent('onmouseover', change_title);
				}
			}
		}
	</script>"; */

/*if($Engine->OperationAllowed(5, "nsau.js.access", -1, $Auth->usergroup_id))
{
    if(!function_exists('FormCheck'))
    {
	    function FormCheck()
	    {
	       global $EE;
	    $EE["head_extra3"][] = "<script type=\"text/javascript\">


		var title_changed = false;
		function change_title(elem, title, descr) {
			if (!title_changed) {
				hrefs = elem.getElementsByTagName('A');
				for (i=0; i < hrefs.length; i++) {
					hrefs[i].setAttribute('href', hrefs[i].getAttribute('href').replace( /title=(\S+)/, '&title='+encodeURI(title)));
					if (descr)
						hrefs[i].setAttribute('href', hrefs[i].getAttribute('href').replace( /descr=(\S*)/, '&descr='+encodeURI(descr)));
				}
			title_changed = true;
			}
		}

		function confirmSubmit() {
	        var agree=confirm(\"�� �������, ��� ������ ������� ��������� �������?\");
	        if (agree)
	            return true ;
	        else
	            return false ;
	    };

	    function expulsion() {
	        var agree=confirm(\"�� �������, ��� ������ ��������� ������� ��������?\");
	        if (agree)
	            return true ;
	        else
				return false ;
	    };

	    function sendform() {
	        if (document.forms[0].last_name.value == \"\") {
	            alert('����������, ������� ������� ��������');
	            document.add_student.last_name.focus();
	            return false;
	        }
	        if (document.forms[0].first_name.value == \"\") {
	            alert('����������, ������� ��� ��������');
	            document.add_student.name.focus();
	            return false;
	        }
	        if (document.forms[0].patronymic.value == \"\") {
	            alert('����������, ������� �������� ��������');
	            document.add_student.patronymic.focus();
	            return false;
	        }
	        if (document.forms[0].username.value == \"\") {
	            alert('����������, ������� ��� ������������ ��������');
	            document.add_student.patronymic.focus();
	            return false;
	        }
	        if (document.forms[0].email.value == \"\") {
	            alert('����������, ������� E-mail ��������');
	            document.add_student.email.focus();
	            return false;
	        }
	        if (document.forms[0].password1.value == \"\") {
	            alert('����������, ������� ������ ��������');
	            document.forms[0].password1.focus();
	            return false;
	        }
	        if (document.forms[0].password1.value != document.forms[0].password2.value) {
	            alert('��������� ������ �� ���������');
	            document.forms[0].password1.focus();
	            return false;
	        }
	        return true;
	    };

		function show_fields(num) {
			if (num==0) {
				document.getElementById('olduser').style.display = 'none';
				document.getElementById('newuser').style.display = 'block';
			}
			if (num==1) {
				document.getElementById('olduser').style.display = 'block';
				document.getElementById('newuser').style.display = 'none';
			}
			if (num==2) {
				document.getElementById('olduser').style.display = 'none';
				document.getElementById('newuser').style.display = 'none';
			}
		}

	            function sendform_sbd()
	            {

	                if (document.forms[0].subject.value == 0)
	                {
	                    alert('����������, �������� �������');
	                    document.forms[0].subject.focus();
	                    return false;
	                }
	                if (document.forms[0].faculty.value == 0)
	                {
	                    alert('����������, �������� ���������');
	                    document.forms[0].faculty.focus();
	                    return false;
	                }
	                if (document.forms[0].teacher_id.value == 0)
	                {
	                    alert('����������, �������� �������������');
	                    document.forms[0].teacher_id.focus();
	                    return false;
	                }
	                if (document.forms[0].groups.value == \"\")
	                {
	                    alert('����������, ������� ������ �����');
	                    document.forms[0].groups.focus();
	                    return false;
	                }
	                if (document.forms[0].auditorium.value == \"\")
	                {
	                    alert('����������, ������� ������ ���������');
	                    document.forms[0].auditorium.focus();
	                    return false;
	                }
	                if (document.forms[0].rate.value == 0)
	                {
	                    alert('����������, �������� ������ ���������� ������');
	                    document.forms[0].rate.focus();
	                    return false;
	                }
	                return true;
	            };

	            function sendform_graphics()
	            {
	                if (document.forms[0].subject_id.value == 0)
	                {
	                    alert('����������, �������� �������');
	                    document.forms[0].subject_id.focus();
	                    return false;
	                }
	                if (document.forms[0].groups.value == \"\")
	                {
	                    alert('����������, ������� ������ �����');
	                    document.forms[0].groups.focus();
	                    return false;
	                }
	                if (document.forms[0].kol_groups.value == \"\")
	                {
	                    alert('����������, ������� ���������� �����');
	                    document.forms[0].kol_groups.focus();
	                    return false;
	                }
	                if (document.forms[0].lecture_hours.value == \"\")
	                {
	                    alert('����������, ������� ���������� ���������� �����');
	                    document.forms[0].lecture_hours.focus();
	                    return false;
	                }
	                if (document.forms[0].practice_hours.value == \"\")
	                {
	                    alert('����������, ������� ���������� ����� ������������ �������');
	                    document.forms[0].practice_hours.focus();
	                    return false;
	                }
	                return true;
	            };
				</script>";
		    };
		    //if(!isset($EE["head_extra3"]))
	    	{
	        FormCheck();
	    	}
	    }

    }*/
?>

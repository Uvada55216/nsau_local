<?php
class EAbit
// version: 1.18
// date: 2014-07-24
{
    var $module_id;
    var $node_id;
    var $module_uri;
    var $privileges;
    var $output;

    var $mode;
    var $db_prefix;
    var $maintain_cache;
    var $status_id;
    
    static $basic_mode_specs_codes = array(62, 65);
    static $spo_mode_specs_codes = array(51); 
    static $magistracy_mode_specs_codes = array(68);
    static $graduate_mode_specs_codes = array(Null);//
    
    var $isMagistracy = false; 
    
    /*
    � params �������: 	1-� ��������: ����� ������ ( 'list' - ������ ������ ������������, 
    												'request' - ������ ���������� � ���������� �������� ���������, 
    												'edit_spec' - �������������� ������ � ���������� ���� �� ��������������);
     					2-� ��������: � ������ 'list' - id ������� ����������
    */   
    function EAbit($global_params, $params, $module_id, $node_id = NULL, $module_uri = NULL, $additional = NULL)
    {
        global $DB, $Engine, $Auth;
				

// error_reporting(E_ALL);
 
 // ini_set('display_errors', 'on');

        $this->module_id = $this->output["module_id"] = $module_id;
        $this->node_id = $node_id;
        $this->module_uri = $module_uri;
        $this->additional = $additional;
        
        $parts = explode(";", $global_params);
        $this->db_prefix = $parts[0];
        
        $this->isMagistracy = ($parts[2] == "magistracy" ? true : false);
        
        $this->output = array();
		$this->output["module_id"] = $this->module_id;
        $this->output["messages"] = array("good" => array(), "bad" => array());
        
        $parts = explode(";", $params);
        $this->output["scripts_mode"] = $this->output["mode"] = $this->mode = $parts[0];
        $this->output["abitprogressfile"] = $abitProgressFile = '/files/abit.tmp';
        $this->output['plugins'][] = 'jquery.form';
		
        switch ($this->mode) {
        	case "abitfile_processing":
        		 
        		// $f = fopen($_SERVER['DOCUMENT_ROOT'].$abitProgressFile, 'w');
				// fwrite($f, 2);
				// fclose($f);
					
				// $time0 = time();
				// set_time_limit(0);
				// if (isset($_POST["type"]) && $_POST["type"]=="spo") {
					// $sql = "CALL ab22_spo(51);";
				// }
				// elseif (isset($_POST["type"]) && $_POST["type"]=="spo-ex") {
					// $sql = "CALL ab23_spo(51, '�������');";
				// } 
				// else if (isset($_POST["type"]) && $_POST["type"]=="vpo") {
					// $sql = "CALL ab22('�����');";
				// } 
				// else if (isset($_POST["type"]) && $_POST["type"]=="vpo_ev") {
					// $sql = "CALL ab22('��������');";
				// } 
				// else if (isset($_POST["type"]) && $_POST["type"]=="izop") {
					// $sql = "CALL ab23_izop('�������');";
				// } else {
					$sql = "CALL ab23_spo();";
				// }
				// if(!empty($sql))		
				// if ($DB->Exec($sql)) {
					// $workInterval = time()-$time0; 
					// $min = floor($workInterval/60); $sec = $workInterval - $min*60;
					// $workInterval = $min.' ���. '.$sec.' ���.';
					// $this->output["messages"]["good"][] = "����������� ���������";
					// if (isset($_POST["type"]) && $_POST["type"]=="spo") {
						// $Engine->LogAction($this->module_id, "spofile", -1, "upload_spo - ".$workInterval);
						// $filetype = "spo";
					// } else if (isset($_POST["type"]) && $_POST["type"]=="spo-ex") {
						// $Engine->LogAction($this->module_id, "vpofile", -1, "upload_spo_ex - ".$workInterval);
						// $filetype = "spo_ex";
					// } else if (isset($_POST["type"]) && $_POST["type"]=="vpo") {
						// $Engine->LogAction($this->module_id, "vpofile", -1, "upload_vpo - ".$workInterval);
						// $filetype = "vpo";
					// } else if (isset($_POST["type"]) && $_POST["type"]=="vpo_ev") {
						// $Engine->LogAction($this->module_id, "vpofile", -1, "upload_vpo_ev - ".$workInterval);
						// $filetype = "vpo_ev";
					// } else if (isset($_POST["type"]) && $_POST["type"]=="izop") {
						// $Engine->LogAction($this->module_id, "izopfile", -1, "upload_izop - ".$workInterval);
						// $filetype = "izop";
					// } else if (isset($_POST["type"]) && $_POST["type"]=="graduate") {
						// $Engine->LogAction($this->module_id, "graduatefile", -1, "upload_graduate - ".$workInterval);
						// $filetype = "graduate";
					// } else {
						// $Engine->LogAction($this->module_id, "fullfile", -1, "upload_full - ".$workInterval);
						// $filetype = "all";
					// }
          
          // $DB->SetTable("engine_dbupdates_log"); 
          // $DB->AddValue("update_time", date("Y-m-d H:i:s", time())); 
          // $DB->AddCondFS("name", "=", "nsau_spec_abit"."_".$filetype); 
          // $DB->Update(1);   
				// } else {
					// $this->output["messages"]["bad"][] = "��������� ������� ������: ".$DB->Error();
				// }
							
				
				// $f = fopen($_SERVER['DOCUMENT_ROOT'].$abitProgressFile, 'w');
				// fwrite($f, '');
				// fclose($f);
        	// break;
        	
        	case "ajax_abitfile" : 
        	
			case "abitfile": {
										
if($Auth->user_id==31251 && 1==2) {
		// header('Content-Type: text/html; charset=windows-1251');
		// header('P3P: CP="NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM"');
		// header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		// header('Cache-Control: no-store, no-cache, must-revalidate');
		// header('Cache-Control: post-check=0, pre-check=0', FALSE);
		// header('Pragma: no-cache');
		// header('Content-transfer-encoding: binary');
		// header('Content-Disposition: attachment; filename=1231.xls');
		// header('Content-Type: application/x-unknown');		
	// $DB->SetTable("nsau_teachers", "nt");
	// $DB->AddTable("nsau_people", "np");
	// $DB->AddTable("auth_users", "au");
	// $DB->AddTable("nsau_departments", "nd");
	// $DB->AddTable("nsau_faculties", "nf");
	// $DB->AddCondFS("nt.department_id", "=", 93);
	// $DB->AddAltFS("nt.department_id", "=", 90);
	// $DB->AddAltFS("nt.department_id", "=", 94);
	// $DB->AddAltFS("nt.department_id", "=", 87);
	// $DB->AddAltFS("nt.department_id", "=", 60);
	// $DB->AddAltFS("nt.department_id", "=", 88);
	// $DB->AddAltFS("nt.department_id", "=", 96);
	// $DB->AddAltFS("nt.department_id", "=", 89);
	// $DB->AddAltFS("nt.department_id", "=", 92);
	// $DB->AddAltFS("nt.department_id", "=", 91);
	// $DB->AddAltFS("nt.department_id", "=", 86);
	// $DB->AppendAlts();
	// $DB->AddCondFS("np.status_id", "!=", 9);
	// $DB->AddCondFF("np.user_id", "=", "au.id");
	// $DB->AddCondFF("np.id", "=", "nt.people_id");
	// $DB->AddCondFF("nt.department_id", "=", "nd.id");
	// $DB->AddCondFF("nd.faculty_id", "=", "nf.id");
	// $DB->AddField("nd.name", "dname");
	// $DB->AddField("nd.id", "did");
	// $DB->AddField("nf.name", "fname");
	// $DB->AddField("np.name", "name");
	// $DB->AddField("np.last_name", "last_name");
	// $DB->AddField("np.patronymic", "patronymic");
	// $DB->AddField("au.email", "email");
	// $res = $DB->Select();
	// while($row = $DB->FetchAssoc($res))
		// $result[$row["did"]][] = $row;
	
	// CF::Debug($result);
	
	
	
	
	?>
	<table>
	<thead>
		<tr>
			<th>Login</th>
			<th>LastName</th>
			<th>FirstName</th>
			<th>MiddleName</th>
			<th>DepartmentName</th>
			<th>GroupName</th>
			<th>FacultyName</th>
			<th>Email</th>
		</tr>
	</thead>
	<tbody>
	<?foreach($result as $dep) {?>
		<?foreach($dep as $p) {?>
			<tr>
				<td></td>
				<td><?=$p["last_name"]?></td>
				<td><?=$p["name"]?></td>
				<td><?=$p["patronymic"]?></td>
				<td><?=$p["dname"]?></td>
				<td></td>
				<td><?=$p["fname"]?></td>
				<td><?=$p["email"]?></td>
			</tr>
		<?}?>
	<?}?>
	</tbody>
	</table>
	
	
	
	
	
	
	
	
<?
/*//////////////////////////////////////////������������*/	
// set_time_limit(0);
	// $handle = @fopen(INCLUDES . "gmu_progress_new.csv", "r");
	// if ($handle) { 
		// while (($buffer = fgets($handle)) !== false) {
			// $parts = explode(";", $buffer);
			// $code = trim($parts[0]);
			// $arr[$code] = $code;
		// }
	// }
	
	// foreach($arr as $code) {
		// $DB->SetTable("nsau_students_progress");
		// $DB->AddCondFS("code", "=", $code);
		// $DB->Delete();
	// }

	// $handle = @fopen(INCLUDES . "gmu_progress_new.csv", "r");
	// if ($handle) { 
		// while (($buffer = fgets($handle)) !== false) {
			// $parts = explode(";", $buffer);			
				// $code = trim($parts[0]);
				// $semester = trim($parts[1]);
				// $subj = trim($parts[2]);
				// $hours = trim($parts[3]);
				// $type = trim($parts[4]);
				// $val = trim($parts[5]);
			
		
					
				// $asql = "INSERT INTO `nsau`.`nsau_students_progress` (`id`, `code`, `semester`, `subject`, `hours`, `type`, `value`) 
									// VALUES (NULL, '".$code."', '".$semester."', '".$subj."', '".$hours."', '".$type."', '".$val."');";
				// $DB->Exec($asql);

			// }

		// }
		// if (!feof($handle)) {
				// echo "Error: unexpected fgets() fail\n";
		// }
		// fclose($handle);
/*/////////////////////////////////////////������������*/	


/*////////////////////////////////////////���. ���������*/	
	// $handle = @fopen(INCLUDES . "logpassefev.csv", "r");
	// $newfile = @fopen(INCLUDES . "logpassefev2.csv", "r+");
	// if ($handle) { 
		// while (($buffer = fgets($handle)) !== false) {
			// $parts = explode(";", $buffer);				
				// $DB->SetTable("nsau_groups");
				// $DB->AddCondFS("name", "=", $parts[0]);
				// $gr = $DB->FetchAssoc($DB->Select(1));
				// $group = $gr["id"];
				// $username = $parts[2];
				// $user = explode(" ", $username);
				// $lastname = $user[0];
				// $name = $user[1];
				// $patronymic = $user[2];
				// $pass = trim(CF::translit($parts[3]));
				// $login = strtolower(CF::translit(trim($parts[4]))).mt_rand(1000,9999);
				// $passmd5 = md5($pass);
				// $code = $parts[1];
				
				// fputs ($newfile, $buffer.";".$login.";".$pass."\r\n");

				// $asql = "INSERT INTO `nsau`.`auth_users` 
				// (`id`, `is_active`, `ban_reason`, `username`, `password`, `displayed_name`, `email`, `usergroup_id`,
				// `create_time`, `create_user_id`, `last_login_time`, `last_ip`, `last_ua`, `prev_login_time`, `prev_ip`, `prev_ua`)
				// VALUES (NULL, '1', NULL, '".$login."', '".$passmd5."', '".$username."', NULL, '3',
				// '', '0', NULL, NULL, NULL, NULL, NULL, NULL);";
				// $DB->Exec($asql);
				// $user_id = $DB->LastInsertID();
				// $psql = "INSERT INTO `nsau`.`nsau_people` 
					// (`id`, `user_id`, `last_name`, `name`, `patronymic`, `male`, `status_id`, `people_cat`, `year`,
					// `id_group`, `subgroup`, `photo`, `comment`, `pass_num`, `pass_series`, `library_card`, `personal_fid`, `prad`,
					// `exp_full`, `exp_teach`, `post`, `email`, `phone`, `location`, `location_building`, `education`, `add_education`) 
					// VALUES (NULL, '".$user_id."', '".$lastname."', '".$name."', '".$patronymic."', '1', '8', '1', '', 
					// '".$group."', NULL, NULL, NULL, NULL, NULL, '".$code."', NULL, NULL,
					// NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);";
				// $DB->Exec($psql);
			// }

		// }
		// if (!feof($handle)) {
				// echo "Error: unexpected fgets() fail\n";
		// }
		// fclose($handle);
		// fclose($newfile);
/*////////////////////////////////////////���. ���������*/		

/*////////////////////////////////////////���. ���������*/	
	// $handle = @fopen(INCLUDES . "in.csv", "r");
	// $newfile = @fopen(INCLUDES . "in2.csv", "r+");
	// if ($handle) { 
		// while (($buffer = fgets($handle)) !== false) {
			// $parts = explode(";", $buffer);				
				// $DB->SetTable("nsau_groups");
				// $DB->AddCondFS("name", "=", $parts[0]);
				// $gr = $DB->FetchAssoc($DB->Select(1));
				// $group = $gr["id"];
				// $username = trim(mb_convert_case($parts[2], MB_CASE_TITLE, "cp1251"));
				// $user = explode(" ", $username);
				// $lastname = $user[0];
				// $name = $user[1];	
				// $patronymic = $user[2];
				// $pass = str_replace("/", "", str_replace("-", "", (trim(CF::translit($parts[1])))));
				// $login = strtolower(CF::translit(trim($lastname))).mt_rand(1000,9999);
				// $passmd5 = md5($pass);
				// $code = $parts[1];
				
				// fputs ($newfile, $parts[0].";".trim($lastname)." ".trim($name)." ".trim($patronymic).";".$login.";".$pass."\r\n");

				// $asql = "INSERT INTO `nsau`.`auth_users` 
				// (`id`, `is_active`, `ban_reason`, `username`, `password`, `displayed_name`, `email`, `usergroup_id`,
				// `create_time`, `create_user_id`, `last_login_time`, `last_ip`, `last_ua`, `prev_login_time`, `prev_ip`, `prev_ua`)
				// VALUES (NULL, '1', NULL, '".$login."', '".$passmd5."', '".$username."', NULL, '3',
				// '', '0', NULL, NULL, NULL, NULL, NULL, NULL);";
				// $DB->Exec($asql);
				// $user_id = $DB->LastInsertID();
				// $psql = "INSERT INTO `nsau`.`nsau_people` 
					// (`id`, `user_id`, `last_name`, `name`, `patronymic`, `male`, `status_id`, `people_cat`, `year`,
					// `id_group`, `subgroup`, `photo`, `comment`, `pass_num`, `pass_series`, `library_card`, `personal_fid`, `prad`,
					// `exp_full`, `exp_teach`, `post`, `email`, `phone`, `location`, `location_building`, `education`, `add_education`) 
					// VALUES (NULL, '".$user_id."', '".$lastname."', '".$name."', '".$patronymic."', '1', '8', '1', '', 
					// '".$group."', NULL, NULL, NULL, NULL, NULL, '".$code."', NULL, NULL,
					// NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);";
				// $DB->Exec($psql);
			// }

		// }
		// if (!feof($handle)) {
				// echo "Error: unexpected fgets() fail\n";
		// }
		// fclose($handle);
		// fclose($newfile);
/*////////////////////////////////////////���. ���������*/	


?>		
										
	<?		//	die;						
										
				}							
										
										
										
										
										
										
										
										
				$this->output["abitfile"] = 1;
				if($Engine->OperationAllowed($this->module_id, "places_info.handle.vpo", -1, $Auth->usergroup_id)) {
					$this->output["handle_vpo"] = 1;
				}
				if($Engine->OperationAllowed($this->module_id, "places_info.handle.izop", -1, $Auth->usergroup_id)) {
					$this->output["handle_izop"] = 1;
				}
				if($Engine->OperationAllowed($this->module_id, "places_info.handle.spo", -1, $Auth->usergroup_id)) {
					$this->output["handle_spo"] = 1;
				}
				if($Engine->OperationAllowed($this->module_id, "places_info.handle.graduate", -1, $Auth->usergroup_id)) {
					$this->output["handle_graduate"] = 1;
				}
				if (isset($_FILES['abit'])) { 
					// while (file_get_contents($_SERVER['DOCUMENT_ROOT'].$abitProgressFile)) {
					// }  
					$fname = explode(".", $_FILES['abit']['name']);
					if ($fname[count($fname)-1] != "csv") {
						$this->output["messages"]["bad"][] = "��������� ���� � ������� csv";
					} else {
						$f = fopen($_SERVER['DOCUMENT_ROOT'].$abitProgressFile, 'w');
						fwrite($f, 1);
						fclose($f);
						$f = fopen($_FILES['abit']['tmp_name'], "r");
						$content = fread($f, filesize($_FILES['abit']['tmp_name']));
						if (!file_exists($_SERVER['DOCUMENT_ROOT'].'/files/abit')) {
							chdir($_SERVER['DOCUMENT_ROOT'].'/files');
							mkdir('abit');
						} 
						$df = fopen($_SERVER['DOCUMENT_ROOT'].'/files/abit/'.date('j-m-y-h-i-s').'.csv', "w+");
						fwrite($df, $content);
						fclose($df);
						fclose($f);			
						$parts = explode("\r\n", $content);
						$headers = explode(";", $parts[0]);
							array_shift($parts);
						
						
						
						
						
						
						if($_POST["type"] == "graduate") {
							$exams = array("����������� ����"=>10, "���������"=>11, "����������� ����������"=>12);
							$anal_boolean = array("������" => 1, "����" => 0, "" => 0);
							$acceptance = array("������" => "", "��������" => 1, "������������ � ����������" => 4);
							$edu_type = array("������ ���������� ������" => 6, "��������� ������" => 4);
							$fields = array(
													6 => "speciality_id", 4 => "qualification", 5 => "type", 15 => "type_id", 8 => "documents", 
													3 => "position", 0 =>"people_id", 16 => "ege", 1 => "status1", 2 => "status2", 9 => "acceptance",
													13 => "out", 14 => "took", 7 => "form", 10 => "exam1", 11 => "exam2", 12 => "exam3",
													18 => "ent", 17 => "score", 20 => "dogovor"
												);
							$j = 0;
							$del = 0;
							$err = 0;
							foreach($parts as $part) {
								$part = explode(";", $part);
								if(count($part)!=14 &&(count($part)!=1)) {
									$this->output["messages"]["bad"][] = "�������� ���������� �����: ".count($part)." ������ "."14";	
									$toobad = 1;
									$err = 1;
									break;
								}	else if(!$del) {
									deleteSpecAbit();
									$del=1;
								}				
								
								if($j!=2) {
									if($j==0) {
										$pre = array();
										for($i=0;$i<=8;$i++)
											$pre[$i] = trim($part[$i]);
										$pre[9] = $part[11];
										$pre[13] = $anal_boolean[$part[12]];
										$pre[14] = $anal_boolean[$part[13]];
									}
									$pre[$exams[$part[9]]] = $part[10];
								}
								else {
									$pre[$exams[$part[9]]] = $part[10];
									$pre[0]  =  addPeople($part[0], $part[1], $part[2]);// int people_id addPeople(string full_name, int pass_series, int pass_num); �������� id �������� � nsau_people, ���� ��� ������� ������.
									$pre[1] = $pre[2] = 9;
									$pre[3]  =  $edu_type[$part[3]];//������.������
									$pre[4]  =  3;//������������ id - ��������, �.-��������
									$pre[5]  = $part[5];//��� ������������� �� �����
									$pre[6]  = $part[6];//��� �������������, ���� �� �����
									$pre[7]  =  mb_strtolower($part[7]);//����� - ���, �����
									$pre[9]  =  $acceptance[$pre[9]];//������ ����������
									$pre[15] = getTypeIdGraduate($part[6]); // type_id � nsau_types
									$pre[16] = $pre[10]+$pre[11]+$pre[12]; //ege
									$pre[17] = $pre[10]+$pre[11]+$pre[12]; //score
									$pre[18] = "��������"; //ent - �������� ��� �������������
									// $pre[19] = getQualName(3); //������������
									$pre[20] = ""; //dogovor
									//ksort($pre);
									insertSpecAbit($pre, $fields);
									// echo "1";
									$abit[] = $pre;
									$j=0;
									$part = array();
									continue;
								}
								$j++;								
							}
						
							if(empty($err)) {					
								$Engine->LogAction($this->module_id, "graduatefile", -1, "upload_graduate");
								$filetype = "graduate";               
								$DB->SetTable("engine_dbupdates_log"); 
								$DB->AddValue("update_time", date("Y-m-d H:i:s", time()));  
								$DB->AddCondFS("name", "=", "nsau_spec_abit"."_".$filetype);
								$DB->Update(1); 						
							$this->output["messages"]["good"][] = "��������� ".count($parts)." ������������.";
							}
						}	elseif($_POST["type"] == "spo" || $_POST["type"] == "spo-ex") {
							
							
							$form = '';
							$fieldNum = 26;							
							// $studyForms = array('spo'=> '���', 'vpo_ev'=>'��������', 'vpo'=> '�����', 'izop' => '�������', 'spo-ex' => '�������');
							// $curForm = isset($studyForms[$_POST["type"]]) ? $studyForms[$_POST["type"]] : '';
							
							
							foreach ($parts as $ind => $part) {
								if ($part && !$error) {
									$string = explode(";", $part);
									if (count($string) !=  $fieldNum) {
										$this->output["messages"]["bad"][] = "�������� ���������� �����: ".count($string)." ������ ".$fieldNum;
										$error = true;
									} 
									$usable_fields = array(0=>"position", 1=>"speciality_id", 2=>"type", 5=>"paragraf", 6=>"dogovor", 7=>"notice", 
																					8=>"documents", 11=>"status1", 12=>"status2", 13=>"out", 14=>"took", 15=>"client", 
																					16=>"form", 17=>"acceptance", 18=>"ent", 19=>"exam1", 20=>"exam2", 21=>"exam3", 22=>"our_exam1", 
																					23=>"our_exam2",  4=>"score");
									
									// $score = ($string[19] ? $string[19] : $string[22])  + ($string[20] ? $string[20] : $string[23]) + ($string[21] ? $string[21] : $string[24]) + $string[25] + $string[26];
				 					if($string[15][0] == "\""){
										$string[15] = trim($string[15]);
										$string[15][0] = "";
										$string[15][(iconv_strlen($string[15], 'cp1251')-1)] = "";
										$string[15] = str_replace("\"\"", "\"", $string[15]);
									}
									
									$created_fields = array("ege"=>$string[4], "score"=>str_replace(",", ".", $string[4]), "qualification"=>$this->getQualId($string[24]),
																					 "type_id"=>$this->getTypeIdSpo($string[1], $string[25]), "people_id"=>$this->addPeople($string[3], $string[10], $string[9]), "abit_file_type" => $_POST["type"]);
									
									
									
									foreach($usable_fields as $id => $fname) {
										$string[$id] = ($string[$id] == "����" ? 0 : ($string[$id] == "������" ? 1 : $string[$id]));
										$complite[$ind][$fname] = $string[$id];
									}
									foreach($created_fields as $fname => $val) {
										$val = ($val == "����" ? 0 : ($val == "������" ? 1 : $val));
										$complite[$ind][$fname] = $val;
									}
									
									
								}
							} 
							
							if (!$error) {
								$DB->SetTable("nsau_spec_abit");
								$DB->AddCondFS("abit_file_type", "=", $_POST["type"]);
								$DB->Delete();
								foreach($complite as $comp) {
									$DB->SetTable("nsau_spec_abit");
									foreach($comp as $field => $val) 
										$DB->AddValue($field, $val);
									$DB->Insert();
								}
							}
							// CF::Debug($complite);die;
							$DB->SetTable("engine_dbupdates_log"); 
							$DB->AddValue("update_time", date("Y-m-d H:i:s", time()));  
							$DB->AddCondFS("name", "=", "nsau_spec_abit"."_".str_replace("-", "_", $_POST["type"]));
							$DB->Update(1); 
							$this->output["messages"]["good"][] = "��������� ".count($parts)." ������������.";
							
							
						}				
						else {	
							$form = '';
							$fieldNum = 29;							
							// $studyForms = array('spo'=> '���', 'vpo_ev'=>'��������', 'vpo'=> '�����', 'izop' => '�������', 'spo-ex' => '�������');
							// $curForm = isset($studyForms[$_POST["type"]]) ? $studyForms[$_POST["type"]] : '';
							
						
							
							foreach ($parts as $ind => $part) {
								if ($part && !$error) {
									$string = explode(";", $part);
									if (count($string) !=  $fieldNum) {
										$this->output["messages"]["bad"][] = "�������� ���������� �����: ".count($string)." ������ ".$fieldNum;
										$error = true;
									} 
									$usable_fields = array(0=>"position", 1=>"speciality_id", 2=>"type", 5=>"paragraf", 6=>"dogovor", 7=>"notice", 
																					8=>"documents", 11=>"status1", 12=>"status2", 13=>"out", 14=>"took", 15=>"client", 
																					16=>"form", 17=>"acceptance", 18=>"ent", 19=>"exam1", 20=>"exam2", 21=>"exam3", 22=>"our_exam1", 
																					23=>"our_exam2", 24=>"our_exam3", 25=>"gain1", 26=>"gain2", 27=>"accepted");
									
									$score = ($string[19] ? $string[19] : $string[22])  + ($string[20] ? $string[20] : $string[23]) + ($string[21] ? $string[21] : $string[24]) + $string[25] + $string[26];
									
									if($string[15][0] == "\""){
										$string[15] = trim($string[15]);
										$string[15][0] = "";
										$string[15][(iconv_strlen($string[15], 'cp1251')-1)] = "";
										$string[15] = str_replace("\"\"", "\"", $string[15]);
									}
									$created_fields = array("ege"=>($string[19]+$string[20]+$string[21]), "score"=>$score, "qualification"=>$this->getQualId($string[28]),
																					 "type_id"=>$this->getTypeId($string[1], ($_POST["type"]=="izop" ? "izop" : "nsau")), "people_id"=>$this->addPeople($string[3], $string[10], $string[9]), "abit_file_type" => $_POST["type"]);
									
									
									
									foreach($usable_fields as $id => $fname) {
										$string[$id] = ($string[$id] == "����" ? 0 : ($string[$id] == "������" ? 1 : $string[$id]));
										$complite[$ind][$fname] = $string[$id];
									}
									foreach($created_fields as $fname => $val) {
										$val = ($val == "����" ? 0 : ($val == "������" ? 1 : $val));
										$complite[$ind][$fname] = $val;
									}
									
									
								}
							} 
							
							if (!$error) {
								$DB->SetTable("nsau_spec_abit");
								$DB->AddCondFS("abit_file_type", "=", $_POST["type"]);
								$DB->Delete();
								foreach($complite as $comp) {
									$DB->SetTable("nsau_spec_abit");
									foreach($comp as $field => $val) 
										$DB->AddValue($field, $val);
									$DB->Insert();
								}
							}
							// CF::Debug($complite);die;
							$DB->SetTable("engine_dbupdates_log"); 
							$DB->AddValue("update_time", date("Y-m-d H:i:s", time()));  
							$DB->AddCondFS("name", "=", "nsau_spec_abit"."_".$_POST["type"]);
							$DB->Update(1); 
							$this->output["messages"]["good"][] = "��������� ".count($parts)." ������������.";
						}

						$f = fopen($_SERVER['DOCUMENT_ROOT'].$abitProgressFile, 'w');
						fwrite($f, '');
						fclose($f);

					}
				}
			}
			break;
			
			case "list": {
				$parts2 = explode("/", $module_uri);
        if(!empty($parts2) && CF::IsNaturalNumeric($parts2[0])) $_GET['type'] = $parts2[0];
        $this->status_id = $parts[1];
				if (isset($parts[2])) {
					if ($parts[2] == "spo")
						$this->People(1, 0, 0, 0 , 0, "spo");
					else if ($parts[2] == "spo-ex")
						$this->People(1,0,1, 0 , 0, "spo_ex");
					else if ($parts[2] == "vpo-ev")
						$this->People(0,1, 0, 0, 0,  "vpo");
					else if ($parts[2] == "magistracy")
						$this->People(0,0,0,1 ,0, "vpo");
					else if ($parts[2] == "magistracy-ex")
					{
						$this->People(0,0,1,1 , 0, "vpo");
						$this->output["is_ext"] = 1; 
					}
					else if ($parts[2] == "graduate")
						$this->People(0,0,0,0 , 1, "graduate");
					else if ($parts[2] == "graduate-ex")
						$this->People(0,0,1,0 , 1, "graduate");
				} else 
        			$this->People(0,0, 0, 0,0, "vpo");
						//$this->output["test"] = $parts[2];
			}
			break;
                    
			case "request": {
				$this->GetRequestsInfo(isset($parts[2])?$parts[2]:'all');
			}
			break;
				
			case "edit_spec": {
				if (isset($_POST['data_str'])) {
					$this->UpdateSpecsInfo($_POST['data_str']);
				}
				
		        if(isset($_POST["mode"]) && ($_POST["mode"] == "add")) {
			      $DB->SetTable("nsau_spec_type");
			      $DB->AddCondFS("code", "=", trim($_POST["code"]));
			      $res = $DB->Select(1);
			      $row = $DB->FetchObject();
			      if(!$row) {
			        $this->output["messages"]["bad"][] = "��� ������������� � ����� �����"; 
			      }
			      else {
			        $DB->SetTable($this->db_prefix."types");
			        $DB->AddValues(array("code" => $_POST["code"], "type" => $_POST["type"], "qual_id" => $_POST["qualification"]));
			        if(isset($_POST["budget"])) $DB->AddValue("budget", $_POST["budget"]);
			        if(isset($_POST["budget_ext"])) $DB->AddValue("budget_ext", $_POST["budget_ext"]);
							
			        if(isset($_POST["purpose"])) $DB->AddValue("purpose", $_POST["purpose"]);
							
			        if(isset($_POST["commerce"])) $DB->AddValue("commerce", $_POST["commerce"]);
			        if(isset($_POST["commerce_ext"])) $DB->AddValue("commerce_ext", $_POST["commerce_ext"]);
			        if(isset($_POST["commerce_int_ext"])) $DB->AddValue("commerce_int_ext", $_POST["commerce_int_ext"]);
							
			        if(isset($_POST["classes"])) $DB->AddValue("classes", $_POST["classes"]);
							
			        if(isset($_POST["has_ext"])) $DB->AddValue("has_ext", $_POST["has_ext"]);
			        if(isset($_POST["has_int_ext"])) $DB->AddValue("has_int_ext", $_POST["has_int_ext"]);
			        // if(isset($_POST["sortorder"])) $DB->AddValue("sortorder", $_POST["sortorder"]);
			        //$DB->AddValue("pid", $_POST["parent"]);
			  			  // if($Auth->user_id==31251)
									// echo $DB->InsertQuery();
			        if($DB->Insert()) {
								$Engine->LogAction($this->module_id, "nsau_types", -1, "add");
			          CF::Redirect($Engine->engine_uri);
			        }
			        else {
			          $this->output["messages"]["bad"][] = "�� ������� �������� �����������"; 
			        }
			        //
			      }
				}
				
				if(isset($_POST["mode"]) && ($_POST["mode"] == "save")) {
					echo 1;
					$DB->SetTable("nsau_spec_type");
					$DB->AddCondFS("code", "=", trim($_POST["code"]));
					$res = $DB->Select(1);
					$row = $DB->FetchObject();
					if(!$row) {
						$this->output["messages"]["bad"][] = "��� ������������� � ����� �����";
					}
					else {
						$DB->SetTable($this->db_prefix."types");
						$DB->AddCondFS("id", "=", $_POST["id"]);
						$DB->AddValues(array("code" => $_POST["code"], "type" => $_POST["type"], "qual_id" => $_POST["qualification"]));
			        if(isset($_POST["budget"])) $DB->AddValue("budget", $_POST["budget"]);
			        if(isset($_POST["budget_ext"])) $DB->AddValue("budget_ext", $_POST["budget_ext"]);
							
			        if(isset($_POST["purpose"])) $DB->AddValue("purpose", $_POST["purpose"]);
							
			        if(isset($_POST["commerce"])) $DB->AddValue("commerce", $_POST["commerce"]);
			        if(isset($_POST["commerce_ext"])) $DB->AddValue("commerce_ext", $_POST["commerce_ext"]);
			        if(isset($_POST["commerce_int_ext"])) $DB->AddValue("commerce_int_ext", $_POST["commerce_int_ext"]);
							
			        if(isset($_POST["classes"])) $DB->AddValue("classes", $_POST["classes"]);
							
			        if(isset($_POST["has_ext"])) $DB->AddValue("has_ext", $_POST["has_ext"]);
			        if(isset($_POST["has_int_ext"])) $DB->AddValue("has_int_ext", $_POST["has_int_ext"]);
						// if(isset($_POST["sortorder"])) $DB->AddValue("sortorder", $_POST["sortorder"]);
						//$DB->AddValue("pid", $_POST["parent"]);
						// if($Auth->user_id==31251)
							// echo $DB->UpdateQuery();
				
						if($DB->Update()) {
							$Engine->LogAction($this->module_id, "nsau_types", $_POST["id"], "edit");
							CF::Redirect($Engine->engine_uri);
						}
						else {
							$this->output["messages"]["bad"][] = "�� ������� �������� �����������";
						}
						//
					}
				}
		    
		        $DB->SetTable("nsau_faculties"); 
		        $res = $DB->Select();
		        
		        $this->output["faculties"] = array(); 
		        
		        while($row = $DB->FetchObject()) {
		          $this->output["faculties"][] = $row; 
		        }
		        
		        $DB->SetTable($this->db_prefix . "qualifications"); 
		        $res = $DB->Select();
		        
		        $this->output["qualifications"] = array(); 
		        
		        while($row = $DB->FetchObject()) {
		          $this->output["qualifications"][] = $row; 
		        }
        
				if(!isset($_GET["edittype"])) 
					$this->GetSpecsInfo();
				else 
					$this->EditType($_GET["edittype"]); 
			}
			break;
					
			case "daystogo": {
				$this->DaysToGo();
			}
			break;
				
			default: {
				die("EMenu module error #1100 at node $this->node_id: unknown mode &mdash; $this->mode.");
			}
			break;
		}
	}
	
	function People($isSpo = 0, $isEvening = 0, $isExt = 0, $isMagistracy = 0, $isGraduate = 0, $mode = false)
	{
		global $DB;
		$this->output['isEvening'] = $isEvening;
		$this->output['isExt'] = $isExt; 
	    $this->output['isSpo'] = $isSpo;
	    $this->output['isMagistracy'] = $isMagistracy;
	    $this->output['isGraduate'] = $isGraduate;
		$DB->SetTable("nsau_abit_types");
		$atres = $DB->Select();
		$this->output["at"][0] = "������ ����� (������ � ��������)";
		$this->output["at"][-1] = "<strong>����� ����������� �� ����������� ������������ ����������� � ����� ���������� ���������</strong>";
		while($atrow = $DB->FetchAssoc($atres))
			$this->output["at"][$atrow["position"]] = $atrow["descr"];
		$this->TypesList($isSpo, $isEvening, $isExt, $isMagistracy, $isGraduate);
		/*$DB->SetTable($this->db_prefix."people", "p");
		$DB->AddTable($this->db_prefix."spec_abit", "a");		
		$DB->AddTable($this->db_prefix."specialities", "s");		
		$DB->AddField("a.speciality_id");
		$DB->AddField("a.qualification");
		$DB->AddField("a.type");
		$DB->AddField("a.type_id");
		$DB->AddField("a.dogovor");
		$DB->AddField("a.documents");
		$DB->AddField("a.notice");
		$DB->AddField("a.position");
		$DB->AddField("a.ege");
		$DB->AddField("a.acceptance");
		$DB->AddField("a.form");
		$DB->AddField("s.name", "s_name");
		$DB->AddField("p.id");
		$DB->AddField("p.last_name");
		$DB->AddField("p.name");
		$DB->AddField("p.patronymic");
		//$DB->AddCondFS("a.position", "=", 3);
		$DB->AddCondFF("p.id","=","a.people_id");
		$DB->AddCondFF("s.code","=","a.speciality_id");
		$DB->AddCondFS("p.status_id","=",$this->status_id);
		//$DB->AddCondFS("a.qualification","=",51);
		//$DB->AddCondFS("a.speciality_id","=","080114"); 
		$DB->AddCondFS("a.out","=","0");
		$DB->AddCondFS("a.took","=","0");

		$DB->AddOrder("a.type");

		if(isset($_GET["type"]))
		{
			$DB->AddTable($this->db_prefix."abit_types", "t");
			$DB->AddField("t.descr");
			//$DB->AddCondFF("t.position","=","a.position");
			$DB->AddCondFS("a.type_id","=",$_GET["type"]);
			$DB->AddOrder("a.position");
  		}

		//$DB->AddOrder("a.notice", 1);
		//$DB->AddGrouping("p.id");
		$DB->AddOrder("a.status1");
		$DB->AddOrder("a.dogovor", 1);
		//$DB->AddOrder("a.acceptance");
		$DB->AddOrder("a.ege", 1);
		$DB->AddOrder("a.documents", 1);
		$DB->AddOrder("a.status2", 1);
		$DB->AddOrder("p.last_name");*/
		
		if (isset($_GET["type"])) { 
			$sql = "select p.*, a.*, t.*  from nsau_people p, nsau_spec_abit a left join nsau_abit_types t on t.position=a.position left join ".$this->db_prefix."qualifications q on q.id=a.qualification where a.out=0 and a.took=0 and a.form='".($isEvening ? '��������' : ($isSpo ? ($isExt ? '�������' : '�����') : ($isMagistracy ? ($isExt ? '�������' : '�����') : ($isGraduate ? ($isExt ? '�������' : '�����') :  '�����'))))."' and a.people_id=p.id and a.type_id=".$_GET["type"]." and a.speciality_id ".($isSpo ? " REGEXP '[[:alnum:]]{2}\.02\.[[:alnum:]]{2}' " : " NOT REGEXP '[[:alnum:]]{2}\.02\.[[:alnum:]]{2}' ")." ".($isMagistracy ? " and q.old_type = 'magistracy' " : "");
			$sql .= "order by replace(a.score, ',', '.') + 0.0 desc, a.position, a.status1, a.acceptance desc, a.dogovor desc, a.exam2 desc,   a.status2 desc, p.last_name";
		}
		else
			$sql = "select * from nsau_people p, nsau_spec_abit a left join nsau_abit_types t on t.position=a.position where a.out=0 and a.took=0 and a.form='".($isEvening ? '��������' : ($isSpo ? ($isExt ? '�������' : '�����') : ($isMagistracy ? ($isExt ? '�������' : '�����') : ($isGraduate ? ($isExt ? '�������' : '�����') : '�����'))))."' and a.people_id=p.id and a.speciality_id ".($isSpo ? " REGEXP '[[:alnum:]]{2}\.02\.[[:alnum:]]{2}' " : " NOT REGEXP '[[:alnum:]]{2}\.02\.[[:alnum:]]{2}' ")." group by p.last_name, p.name, p.patronymic order by a.position, a.status1, a.dogovor desc, (replace(a.score, ',', '.') + 0.0) desc, a.status2 desc, p.last_name";
		
		// 1. �������(������� �����, ������/������� ����� �������)a.acceptance desc,
// 2. ������������ � ���������� 
// 3. ���������(��������, �����)
// 4. ���
		//echo $sql; 
	    /*$DB->SetTable("nsau_people", "p");
		$DB->AddTable("nsau_spec_abit", "a");
		$DB->AddCondFF("a.people_id", "=", "p.id");
		$DB->AddCondFS("p.status_id","=",$this->status_id);
		$DB->AddCondFS("a.out","=","0");
		$DB->AddCondFS("a.took","=","0");*/
		
		//$DB->AddGroupings("last_name", "name", "patronymic");
		
		//$DB->AddOrder("a.type");
/*		if (isset($_GET["type"])) {
			$DB->AddCondFS("a.type_id", "=", $_GET["type"]);
			$DB->AddOrder("a.position");
		}*/
		
		//echo $sql;//die($DB->SelectQuery());
        //$res = $DB->Select();
				// die($sql);
		$res = $DB->Exec($sql);
        
        while($row = $DB->FetchAssoc($res))
        {
            if($row["ent"] == '') $row["ent"] = '��������';
            if($isMagistracy) $row['ent'] = '';
						if($row["notice"]=="������ � ��������")
							$row["position"] = 0;
						if($row["notice"]=="����������-�����")
							$row["position"] = -1;
            $this->output["people"][$row["position"]][] = $row;   
						if(($row["acceptance"]==1) || ($row["acceptance"]==3))
							$this->output["acceptance_count"]++;									
        }
				ksort($this->output["people"]);
        //die(print_r($this->output["people"])); 
        if(isset($_GET["type"]))
		{
			$DB->SetTable($this->db_prefix."types");
			$DB->AddExp("*");
			if($isGraduate && $isExt) 
				$DB->AddField("budget_ext", "budget");
			if($isSpo && $isExt) 
				$DB->AddField("budget_ext", "budget");
			$DB->AddCondFS("id","=",$_GET["type"]);
			$res = $DB->Select(1);
			if($row = $DB->FetchAssoc($res))
			{
        		$this->output["type"] = $row;
            
	            $this->output["exams"] = array();  
	            
	            $DB->SetTable($this->db_prefix . "exams", "e");
	            $DB->AddTable($this->db_prefix . "spec_exams", "se");
	            $DB->AddTable($this->db_prefix . "spec_type", "st"); 
	            $DB->AddJoin($this->db_prefix . "qualifications q.id", "se.qual_id", "nsau_spec_exams"); 
	            $DB->AddCondFS("se.speciality_code", "=", $row["code"]); 
	            $DB->AddCondFF("se.exam_id", "=", "e.id");
	            $DB->AddCondFF("se.spec_type_id", "=", "st.id");
	            $DB->AddExp('e.*'); 
	            $DB->AddField("st.qual_id");
	            $DB->AddField("q.name", "qual_name");  
	            // echo $DB->SelectQuery(); 
	            
	            $res_exams = $DB->Select(); 
	            
	            while($row_exams = $DB->FetchAssoc($res_exams)) {
							//	$this->output["test"][$row_exams['id']] = $row_exams;
	              $this->output["exams"][$row_exams['id']] = $row_exams;
	            } 
        	}        		
  		}


      $this->output["is_magistracy"] = $isMagistracy;
      $this->output["is_spo"] = $isSpo;
      $this->output["is_graduate"] = $isGraduate;
  		$this->output["date"] = $this->getUpdateDate(true, $mode);
	}
	
	function TypesList($isSpo, $isEvening, $isExt = 0, $isMagistracy = 0 , $isGraduate = 0)
	{
		global $DB;
		$DB->SetTable($this->db_prefix."types", "t");
		//$DB->AddCondFS("t.code", $isSpo ? "LIKE" : "NOT LIKE", "%.51");
	    $DB->AddCondFS("s.type", $isSpo ? "=" : "!=", "secondary");
	    $DB->AddCondFS("s.type", $isMagistracy ? "=" : "!=", "magistracy");
	    $DB->AddCondFS("s.type", $isGraduate ? "=" : "!=", "graduate");
		if ($isEvening) 
			$DB->AddCondFS("t.is_evening", "=", 1);
		if ($isExt)
			$DB->AddCondFS("t.has_ext", "=", 1);
		// if (!$isExt && $isGraduate)
			 // $DB->AddCondFS("t.has_ext", "!=", 0);
		
		$DB->AddOrder("t.code"); 
	    $DB->AddTable("nsau_spec_type", "s");
	    $DB->AddExp("t.*");
	    $DB->AddCondFF("t.code", "=", "s.code"); 
	    $DB->AddCondFS("s.has_vacancies", "=", "1");
	    
	    // echo $DB->SelectQuery(); 
    
		$res = $DB->Select();
		while($row = $DB->FetchAssoc($res))
		{
        	$this->output["types"][$row["id"]] = $row;
        }		
	}
	
	
	function GetSpecsInfo() {
		global $DB, $Engine, $Auth;
		
		$DB->SetTable($this->db_prefix."types");
		$DB->AddField($this->db_prefix  ."types.id");
		$DB->AddField($this->db_prefix  ."types.code");
		$DB->AddField($this->db_prefix  ."types.type");
		$DB->AddField($this->db_prefix  ."types.has_ext");
		$DB->AddField($this->db_prefix  ."types.has_int_ext");
		$DB->AddField($this->db_prefix  ."types.budget");
		$DB->AddField($this->db_prefix  ."types.budget_ext");
		$DB->AddField($this->db_prefix  ."types.commerce");
		$DB->AddField($this->db_prefix  ."types.commerce_ext");
		$DB->AddField($this->db_prefix  ."types.commerce_int_ext");
		$DB->AddField($this->db_prefix  ."types.purpose");
		
	    //$DB->AddJoin( $this->db_prefix  . "specialities.code", "code", $this->db_prefix."types");
	    $DB->AddTable($this->db_prefix  . "specialities"); 
	    $DB->AddField($this->db_prefix  ."specialities.id", "spec_id");
	    $DB->AddJoin( $this->db_prefix  . "faculties.id", $this->db_prefix."specialities.id_faculty", $this->db_prefix."specialities");
		$DB->AddField( $this->db_prefix  . "faculties.name", "faculty_name");
		$DB->AddOrder($this->db_prefix  . "faculties.pos");
    
	    $DB->AddTable($this->db_prefix  . "spec_type");
	    $DB->AddField($this->db_prefix  . "spec_type.type", "spec_type");
	    $DB->AddCondFF($this->db_prefix."types.code", "=", "nsau_spec_type.code");
	    $DB->AddCondFF($this->db_prefix."specialities.code", "=", "nsau_spec_type.code");  
	    $DB->AddCondFS($this->db_prefix  . "spec_type.has_vacancies", "=", "1");
      if(isset($_GET["has_ext"])) $DB->AddCondFS($this->db_prefix  . "types.has_ext", "=", $_GET["has_ext"]);; 
      if(isset($_GET["qual"])) { 
        $DB->AddJoin($this->db_prefix  . "qualifications.id", $this->db_prefix  . "spec_type.qual_id");
        $DB->AddCondFS($this->db_prefix  . "qualifications.old_type", "=", $_GET["qual"]); 
      }
      // if($Auth->user_id==31251)
	    // echo $DB->SelectQuery(); 
		
		$res = $DB->Select();
		
		$rows = array();
		$sum = array("has_ext" => false, "budget" => 0, "budget_ext" => 0, "commerce" => 0, "commerce_ext" => 0, "commerce_int_ext" => 0, "purpose" => 0);
		while($row = $DB->FetchObject())
		{	
			if (!isset($rows[$row->faculty_name])) 
				$rows[$row->faculty_name] = array("specs"=>array(), "has_ext" => false, "budget"=>0, "budget_ext" => 0, "commerce"=>0, "purpose"=>0);
			$rows[$row->faculty_name]["specs"][$row->id] = $row;
        	$rows[$row->faculty_name]["budget"] += $row->budget;
        	$rows[$row->faculty_name]["budget_ext"] += $row->budget_ext;
        	$rows[$row->faculty_name]["commerce"] += $row->commerce;
        	$rows[$row->faculty_name]["commerce_ext"] += $row->commerce_ext;
        	$rows[$row->faculty_name]["commerce_int_ext"] += $row->commerce_int_ext;
       	  	$rows[$row->faculty_name]["purpose"] += $row->purpose;
       	  	$rows[$row->faculty_name]["has_ext"] |= $row->has_ext;
       	  	$rows[$row->faculty_name]["has_int_ext"] |= $row->has_ext;
       	  	$sum["budget"] += $row->budget;
       	  	$sum["budget_ext"] += $row->budget_ext;
        	$sum["commerce"] += $row->commerce;
        	$sum["commerce_ext"] += $row->commerce_ext;
        	$sum["commerce_int_ext"] += $row->commerce_int_ext;
        	$sum["purpose"] += $row->purpose;
        	$sum["has_ext"] |= $row->has_ext;
        	$sum["has_int_ext"] |= $row->has_int_ext;
		}
		$rows["sum"] = $sum;
		$this->output["edit_info"] = $rows;
		$this->output["uri"] = $Engine->FolderURIById();
		$this->output["auth"] = $Engine->OperationAllowed(1, "module.items.handle", 0, $Auth->usergroup_id);
		$DB->FreeRes();
	}
	
	
	function GetRequestsInfo($request_mode)
	{	
		global $DB, $Engine;
		$eveningSpecsPlaceCount = 25;
		$DB->SetTable($this->db_prefix."types");
    
		$DB->AddField($this->db_prefix  ."types.id");
		$DB->AddField($this->db_prefix  ."types.code");
		$DB->AddField($this->db_prefix  ."types.type");
		if($request_mode != "spo-ex" && $request_mode != "magistracy-ex" && $request_mode != "graduate-ex")
			$DB->AddField($this->db_prefix  ."types.budget");
		else 
			$DB->AddField($this->db_prefix  ."types.budget_ext", "budget");
		$DB->AddField($this->db_prefix  ."types.commerce");
		$DB->AddField($this->db_prefix  ."types.commerce_ext");
		$DB->AddField($this->db_prefix  ."types.commerce_int_ext");
		//$DB->AddJoin( $this->db_prefix  . "faculties.id", "faculty_id", $this->db_prefix."types");
	    $DB->AddTable($this->db_prefix  . "spec_type");
		$DB->AddField( $this->db_prefix  . "faculties.name", "faculty_name");
    
	    $DB->AddTable($this->db_prefix  . "specialities"); 
	    $DB->AddField($this->db_prefix  ."specialities.id", "spec_id");
	    $DB->AddCondFF($this->db_prefix."specialities.code", "=", "nsau_spec_type.code"); 
	    $DB->AddJoin( $this->db_prefix  . "faculties.id", $this->db_prefix."specialities.id_faculty", $this->db_prefix."specialities");
	    $DB->AddField( $this->db_prefix  . "faculties.name", "faculty_name");
		$DB->AddOrder($this->db_prefix  . "faculties.pos"); 
    
	    $DB->AddCondFF($this->db_prefix  ."types.code", "=", $this->db_prefix  . "spec_type.code");
	    $DB->AddCondFS($this->db_prefix  . "spec_type.has_vacancies", "=", 1);  
			
		switch($request_mode){
			case 'basic': {
				foreach (self::$basic_mode_specs_codes as $spec_code) {
					//$DB->AddAltFS("code", " LIKE ", "%".$spec_code);
			        //$DB->AddAltFS("nsau_spec_type.type_code", "=", $spec_code);
			        $DB->AddAltFS($this->db_prefix  . "spec_type.type", "=", "higher"); 
			        $DB->AddAltFS($this->db_prefix  . "spec_type.type", "=", "bachelor"); 
				}
				
			}
			break;
			
			case 'vpo-ev': {
				foreach (self::$basic_mode_specs_codes as $spec_code) {
					//$DB->AddAltFS("code", " LIKE ", "%".$spec_code);
			        $DB->AddAltFS($this->db_prefix  . "spec_type.type_code", "=", $spec_code);
			        $DB->AddAltFS($this->db_prefix  . "spec_type.type", "=", "higher"); 
			        $DB->AddAltFS($this->db_prefix  . "spec_type.type", "=", "bachelor"); 
				}
				$DB->AddCondFS("is_evening", "=", 1);
				
			}
			break;
			
			case 'spo': {
				foreach (self::$spo_mode_specs_codes as $spec_code) {
					//$DB->AddAltFS("code", " LIKE ", "%".$spec_code);
			        //$DB->AddAltFS("nsau_spec_type.type_code", "=", $spec_code);
			        $DB->AddAltFS($this->db_prefix  . "spec_type.type", "=", "secondary"); 
				}
			}
			break;
			
			case 'spo-ex': {
				foreach (self::$spo_mode_specs_codes as $spec_code) {
					//$DB->AddAltFS("code", " LIKE ", "%".$spec_code);
			        //$DB->AddAltFS("nsau_spec_type.type_code", "=", $spec_code);
			        $DB->AddAltFS($this->db_prefix  . "spec_type.type", "=", "secondary"); 
				}
				$DB->AddCondFS("has_ext", "=", 1);
			}
			break; 
      
		    case 'magistracy': {
				foreach (self::$magistracy_mode_specs_codes as $spec_code) {
					//$DB->AddAltFS("code", " LIKE ", "%".$spec_code);
			          //$DB->AddAltFS("nsau_spec_type.type_code", "=", $spec_code);
			          $DB->AddAltFS($this->db_prefix  . "spec_type.type", "=", "magistracy"); 
				}				
			}
			break;
			
			case 'magistracy-ex': {
				foreach (self::$magistracy_mode_specs_codes as $spec_code) {
					//$DB->AddAltFS("code", " LIKE ", "%".$spec_code);
					//$DB->AddAltFS("nsau_spec_type.type_code", "=", $spec_code);
					$DB->AddAltFS($this->db_prefix  . "spec_type.type", "=", "magistracy");
				}
				$DB->AddCondFS("has_ext", "=", 1);
			}
			break;
		   case 'graduate': {
				foreach (self::$graduate_mode_specs_codes as $spec_code) {
					//$DB->AddAltFS("code", " LIKE ", "%".$spec_code);
			          //$DB->AddAltFS("nsau_spec_type.type_code", "=", $spec_code);
			          $DB->AddAltFS($this->db_prefix  . "spec_type.type", "=", "graduate"); 
				}			
			// $DB->AddCondFS("has_ext", "!=", 0);	
			}
			break;		
		   case 'graduate-ex': {
				foreach (self::$graduate_mode_specs_codes as $spec_code) {
					//$DB->AddAltFS("code", " LIKE ", "%".$spec_code);
			          //$DB->AddAltFS("nsau_spec_type.type_code", "=", $spec_code);
			          $DB->AddAltFS($this->db_prefix  . "spec_type.type", "=", "graduate"); 
				}		
				$DB->AddCondFS("has_ext", "=", 1);				
			}
			break;					
			default: {
				$request_mode = 'all';
			}
		}
	    //echo $DB->SelectQuery();
		
		$DB->AddOrder($this->db_prefix  . "faculties.pos");
		$DB->AddOrder($this->db_prefix  . "types.code");
		
		// echo $DB->SelectQuery(); 
		$res = $DB->Select(); 
		
		$rows = array();
		$codes = array();
		while($row = $DB->FetchObject()) {
			$rows[$row->id] = $row;
			$codes[$row->id] = $row->code;
			$row->req_count = 0;
		}
		$DB->FreeRes();

		
		$DB->SetTable($this->db_prefix."spec_abit");
		$DB->AddExp("count(*)", "req_count");
		$DB->AddJoin( $this->db_prefix  . "types.id", "type_id", $this->db_prefix."spec_abit");
		$DB->AddField($this->db_prefix  ."types.code");
		$DB->AddField($this->db_prefix  ."types.id");
		$DB->AddJoin( $this->db_prefix  . "faculties.id", $this->db_prefix  ."types.faculty_id", $this->db_prefix."spec_abit");
	    /*$DB->AddTable("nsau_spec_type");
	    $DB->AddCondFF($this->db_prefix  ."types.code", "=", "nsau_spec_type.code");
	    $DB->AddCondFS("nsau_spec_type.has_vacancies", "=", 1); 
	    $DB->AddTable("nsau_qualifications");
	    //$DB->AddCondFF($this->db_prefix."spec_abit.qual_name", "=", "nsau_qualifications.name");
	    //$DB->AddCondFF("nsau_spec_type.qual_id", "=", "nsau_qualifications.id");
	    $DB->AddCondFF($this->db_prefix."spec_abit.qualification", "=", "nsau_qualifications.id");
	    $DB->AddCondFF("nsau_spec_type.qual_id", "=", $this->db_prefix."spec_abit.qualification");*/
    
		if ($request_mode == 'vpo-ev')
			$DB->AddCondFS("form", "=", "��������");
		else if ($request_mode == 'spo') {
			$DB->AddCondFS("form", "=", "�����");
			$DB->AddTable($this->db_prefix."qualifications");
			//$DB->AddCondFF("nsau_spec_type.qual_id", "=", $this->db_prefix."spec_abit.qualification");
			$DB->AddCondFF($this->db_prefix."spec_abit.qualification", "=", "nsau_qualifications.id");
			// $DB->AddCondFS($this->db_prefix."qualifications.old_type", "=", "secondary");
		}
		else if ($request_mode == 'spo-ex') {
			$DB->AddCondFS("form", "=", "�������");
			//$DB->AddCondFS("qualification", "=", "51"); 
		      $DB->AddCondFS("speciality_id", " REGEXP ", "[[:alnum:]]{2}\.02\.[[:alnum:]]{2}");
		      //$DB->AddCondFS();
		      //$DB->AddTable($this->db_prefix  ."spec_type");
		    //$DB->AddCondFF($this->db_prefix  ."types.code", "=", $this->db_prefix  ."spec_type.code");
		    //$DB->AddCondFS($this->db_prefix  ."spec_type.has_vacancies", "=", 1); 
		    //$DB->AddTable("nsau_qualifications"); 
		      //$DB->AddCondFS($this->db_prefix  ."spec_type.type", "=", "secondary");
		      $DB->AddTable($this->db_prefix."qualifications");
		      //$DB->AddCondFF("nsau_spec_type.qual_id", "=", $this->db_prefix."spec_abit.qualification");
		      $DB->AddCondFF($this->db_prefix."spec_abit.qualification", "=", "nsau_qualifications.id");
		      // $DB->AddCondFS($this->db_prefix."qualifications.old_type", "=", "secondary");
		}
		else if ($request_mode == 'magistracy') {
			$DB->AddCondFS("form", "=", "�����");
		}
		else if ($request_mode == 'magistracy-ex') 
		{
			$DB->AddCondFS("form", "=", "�������");
			$fix=1;
			$this->output["test_mag"]=1;

			//$DB->AddCondFS("took", "=", 0);
		}
		else if ($request_mode == 'graduate-ex') {
			$DB->AddCondFS("form", "=", "�������");
		}
		else if ($request_mode == 'graduate') {
			$DB->AddCondFS("form", "=", "�����");
		}
		else if ($request_mode == 'izop') {
			$DB->AddCondFS("form", "=", "�������");
			//$DB->AddCondFS("qualification", "!=", "51");
		    $DB->AddCondFS("nsau_spec_type.type", "!=", "secondary");
		}
		else {
			$DB->AddCondFS("form", "=", "�����");
			$DB->AddTable($this->db_prefix."qualifications");
			//$DB->AddCondFF("nsau_spec_type.qual_id", "=", $this->db_prefix."spec_abit.qualification");
			$DB->AddCondFF($this->db_prefix."spec_abit.qualification", "=", "nsau_qualifications.id");
			$DB->AddAltFS($this->db_prefix."qualifications.old_type", "=", "higher");
			$DB->AddAltFS($this->db_prefix."qualifications.old_type", "=", "bachelor");
			$DB->AppendAlts();
		}
		$DB->AddGrouping( $this->db_prefix."spec_abit.type_id");
		/*$sql = $DB->SelectQuery();
		$sql_parts = explode("GROUP BY", $sql);
		$sql_parts[0] .= " WHERE (qualification=51 and people_id not in (select people_id from nsau_spec_abit where qualification!=51)) or qualification!=51 ";*/
		
		//print_r($rows);
		// echo $DB->SelectQuery();exit; 
		
		$res = $DB->Select();


		
		
		//$sql = implode("GROUP BY", $sql_parts);
		
		//$res = $DB->Exec($sql);
		
		while($row = $DB->FetchObject()) {
			/*foreach ($codes as $ind=>$code) {
				if ($row->code == $code) {
					$rows[$ind]->req_count = $row->req_count;
				}
			}*/
			if(isset($rows[$row->id]))
			{
				$rows[$row->id]->req_count = $row->req_count; 
			}
			
		}
		$DB->FreeRes();
		
		foreach ($rows as $row) {	
			if ($request_mode == 'vpo-ev') {
				$row->commerce = $eveningSpecsPlaceCount;
				$row->budget = 0;
			}
			$row->contest = round($row->req_count/($row->budget+$row->commerce),2);
			$row->contest_not_free = round($row->req_count/($row->commerce),2);
        	$sum["budget"] += $row->budget;       	
					if (($request_mode == 'spo-ex') || ($request_mode == 'graduate-ex')) {
						$sum[$row->faculty_name]["commerce"] += $row->commerce_ext;
						$sum["commerce"] += $row->commerce_ext;
					} else {
						$sum[$row->faculty_name]["commerce"] += $row->commerce;
						$sum["commerce"] += $row->commerce;
					}
					$this->output["requests_info"][$row->faculty_name]["specs"][] = $row;
					if ($fix==1) 
					{
						$this->output["test"][$row->code] = $this->fix_agro($row->code);
					}
					
        	$this->output["requests_info"][$row->faculty_name]["budget"] += $row->budget;
        	$this->output["requests_info"][$row->faculty_name]["commerce"] = $sum[$row->faculty_name]["commerce"];
        	
        	$this->output["requests_info"][$row->faculty_name]["req_count"] += $row->req_count;
        	$sum["req_count"] +=  $row->req_count;
        	$this->output["requests_info"][$row->faculty_name]["contest"] = round($this->output["requests_info"][$row->faculty_name]["req_count"]/($this->output["requests_info"][$row->faculty_name]["budget"]+$this->output["requests_info"][$row->faculty_name]["commerce"]), 2);  
        	// $this->output["requests_info"][$row->faculty_name]["contest_not_free"] = 22;  
    }
        $sum["contest"] = round($sum["req_count"]/($sum["budget"]+$sum["commerce"]), 2);
        $this->output["requests_info"]["sum"] = $sum;
        
        $DB->FreeRes();
		
		$this->output["abit_list_base_href"] = preg_replace('/request\S+/', 'list'.($request_mode == 'spo' ? '-spo' : ($request_mode == 'vpo-ev' ? '-vpo-ev' : ($request_mode == 'spo-ex' ? '-spo-ex' : '')/*''*/)).'/', $Engine->engine_uri)."?type=";
		if($request_mode == "magistracy") $this->output["abit_list_base_href"] = '/magistrantu/list/?type=';
		if($request_mode == "magistracy-ex") $this->output["abit_list_base_href"] = '/magistrantu/list-ex/?type=';
		if($request_mode == "graduate") $this->output["abit_list_base_href"] = '/aspirantura/list/?type=';
		if($request_mode == "graduate-ex") $this->output["abit_list_base_href"] = '/aspirantura/list-ex/?type=';	


		$DB->SetTable("engine_nodes");
		$DB->AddCondFS("module_id", "=", $this->module_id);
		$filetype = "vpo";
		if($request_mode == "magistracy-ex") {

			$DB->AddCondFS("params", "REGEXP", " *list;[0-9];magistracy-ex *$");
			$filetype = "vpo";
		}
		if($request_mode == "magistracy") {
			$DB->AddCondFS("params", "REGEXP", " *list;[0-9];magistracy *$");
			$filetype = "vpo";
		} 
		if($request_mode == "vpo-ev") {
			$DB->AddCondFS("params", "REGEXP", " *list;[0-9];vpo-ev *$");
			$filetype = "vpo_ev";
		} 
		if($request_mode == "spo") {
			$DB->AddCondFS("params", "REGEXP", " *list;[0-9];spo *$");
			$filetype = "spo";
		}
    if($request_mode == "spo-ex") {
			$DB->AddCondFS("params", "REGEXP", " *list;[0-9];spo-ex *$");
			$filetype = "spo_ex";
		}
    if($request_mode == "graduate-ex") {
			$DB->AddCondFS("params", "REGEXP", " *list;[0-9];graduate-ex *$");
			$filetype = "graduate";
		}
    if($request_mode == "graduate") {
			$DB->AddCondFS("params", "REGEXP", " *list;[0-9];graduate *$");
			$filetype = "graduate";
		}
		//echo $DB->SelectQuery(); 
		$res = $DB->Select(1);
		
		while ($row = $DB->FetchAssoc($res)) {
			$this->output["abit_list_base_href"] = $Engine->FolderURIbyID($row["folder_id"])."?type=";
		}
		
		$this->output["request_mode"] = $request_mode;
		$this->output["date"] = $this->getUpdateDate(false, $filetype);
	}
	
	
	function fix_agro ($test)
	{
		global $DB;
		$DB->SetTable($this->db_prefix."spec_abit");
		$DB->AddCondFS("speciality_id", "=", $test);
		$DB->AddCondFS("form", "=", "�������");
		$res = $DB->Select();

		while($row = $DB->FetchObject($res)) {
			$result++;
		}
		return $result;
	}
	function getUpdateDate($with_time = false, $mode = false) {
		global $DB;
		$DB->Init();
    $DB->SetTable("engine_dbupdates_log");
     
		$DB->AddField("update_time");
		$DB->AddCondFS("name", "=", $this->db_prefix."spec_abit"."_".$mode);
		$res = $DB->Select();
		$row = $DB->FetchObject();
		$updateInfo = explode(' ', $row->update_time);
		
		$updateDate = ($with_time ? implode(':', array_splice(explode(':', $updateInfo[1]), 0, 2)) .' '. implode('.', array_reverse(explode('-', $updateInfo[0]))) : implode('.', array_reverse(explode('-', $updateInfo[0]))));
		$DB->FreeRes();
		return $updateDate;
	}
	
	function UpdateSpecsInfo($post_data) { 
		global $DB, $Auth, $Engine;
    
		$data_parts = explode(';', $post_data);
		array_pop($data_parts);
		foreach($data_parts as $data_part) {
			list($key, $value) = explode('=', $data_part);
			if (preg_match('/\-/', $key)) { 
				$key_parts = preg_split('/\-/', $key);
				$field = $key_parts[0];
				$id = $key_parts[1]; 
				$DB->SetTable($this->db_prefix."types");
				$DB->AddCondFS("id", "=", $id);
				$DB->AddValue($field, $value);
				$res = $DB->Update();
				$DB->FreeRes();
			}
		}
		CF::Redirect($Engine->engine_uri);
	}
	
	function EditType($type_id) {
		global $DB, $Auth, $Engine; 
		
		$DB->SetTable($this->db_prefix . "types");
		$DB->AddCondFS("id", "=", $type_id);
		
		$res = $DB->Select(1); 
		while($row = $DB->FetchObject($res)) {
			$this->output["edit_type"] = $row;
		}
	}
	
	function DaysToGo() {
		
		//$changetime1 = mktime(0,0,0,8,15,2012);
		
		$curtime = time();
		
		$changepoints = array(
			
			array('notice'=>'�� ��������� �����<br /> ���������� ��������', 'time'=> mktime(0,0,0,7,27,2012), 'show_mode' => 'calendar'),
			array('notice'=>'�� ��������� �����<br /> ���������� �� ��� ��������', 'time'=> mktime(0,0,0,8,15,2012), 'show_mode' => 'calendar'),
			array('notice'=>'�� ���������� �����<br /> ���������� �� ��� ��������', 'time'=> mktime(0,0,0,8,27,2013), 'show_mode' => 'calendar'),
			array('notice'=>'��������� ���������� �� ��� ����������� �� 22.08.2012 17:00', 'time'=> mktime(17,0,0,8,22,2012), 'show_mode' => 'text'),
			array('notice'=>'������ ������� � 3.09 �� ����������', 'time'=> mktime(0,0,0,9,22,2012), 'show_mode' => 'text'),
	

		);

		foreach($changepoints as $ind=>$changepoint) {
			
			if ($curtime < $changepoint['time']) {
				

				$this->output["show_mode"] = $changepoint['show_mode'];

				if ($this->output["show_mode"] == 'calendar') {
					$secstogo = $changepoint['time'] - $curtime;
					$daystogo = (int)($secstogo/(60*60*24));
					$ost = $daystogo%10;
					if ($ost==1 && ($daystogo < 10 || $daystogo > 20))
						$word = "����";
					elseif (($ost==2 || $ost==3 || $ost==4) && ($daystogo < 10 || $daystogo > 20))
						$word = "���";
					else
						$word = "����";
					$this->output['word'] = $word;
					$this->output["days"] = $daystogo;
				}
				$this->output["notice"] = $changepoint['notice'];
				$this->output["has_info"] = 1;
				return;
			}
		}
		
		/*if ($curtime < $changetime1 && $daystogo)
			$this->output["daystogo"] = "�� ��������� �����<br /> ���������� ��������";
		else 
			$this->output["daystogo"] = "��������� ���������� ����������� �� 09.08 13:00";
		$this->output["word"] = $word;
		$this->output["days"] = $daystogo;*/
		
		//$this->output["scripts_mode"] = $this->output["mode"] = "daystogo";
	}
	
	
function getTypeIdSpo($code, $classes) {
	global $DB;
	$DB->SetTable("nsau_types");
	$DB->AddCondFS("code", "=", $code);
	$DB->AddCondFS("classes", "=", $classes);
	$row = $DB->FetchAssoc($DB->Select(1));
	return $row["id"];
}	
	
	
function getTypeId($code, $pref) {
	global $DB;
	$DB->SetTable($pref."_types");
	$DB->AddCondFS("code", "=", $code);
	$row = $DB->FetchAssoc($DB->Select(1));
	return $row["id"];
}
function getQualId($qual) {
	global $DB;
	$DB->SetTable("nsau_qualifications");
	$DB->AddCondFS("name", "=", $qual);
	$row = $DB->FetchAssoc($DB->Select(1));
	return $row["id"];
}
function addPeople($full_name, $pass_series, $pass_num) {
	global $DB;
	$arr = explode(" ", trim($full_name));
	$name = $arr[1];
	$last_name = $arr[0];
	$patronymic = $arr[2];
	$DB->SetTable("nsau_people");
	$DB->AddCondFS("pass_series", "=", trim($pass_series));
	$DB->AddCondFS("pass_num", "=", trim($pass_num));
	$rp = $DB->Select(1);
	if($p = $DB->FetchAssoc($rp)) {
		$p_full = $p["last_name"]." ".$p["name"]." ".$p["patronymic"];
		if(trim($p_full)!=trim($full_name)) {
			$DB->SetTable("nsau_people");
			$DB->AddCondFS("pass_series", "=", trim($pass_series));
			$DB->AddCondFS("pass_num", "=", trim($pass_num));
			$DB->AddValue("name", $name);
			$DB->AddValue("last_name", $last_name);
			$DB->AddValue("patronymic", $patronymic);
			$DB->Update();		
			return $p["id"];			
		} else
			return $p["id"];
	}
	else {
		$DB->SetTable("nsau_people");
		$DB->AddValue("name", $name);
		$DB->AddValue("last_name", $last_name);
		$DB->AddValue("patronymic", $patronymic);
		$DB->AddValue("pass_series", $pass_series);
		$DB->AddValue("pass_num", $pass_num);
		$DB->AddValue("male", 1);
		$DB->AddValue("status_id", 7);
		$DB->AddValue("people_cat", 1);
		$DB->AddValue("year", 2016);
		$DB->Insert();
		return $DB->LastInsertID();
	}
}
	
	
    function Output()
    {
    	return $this->output;
    }
	
}


function str_replace_limit($search, $replace, $subject, &$count, $limit = -1){
$count = 0;
// Invalid $limit provided
if(!($limit===strval(intval(strval($limit))))){
  trigger_error('Invalid $limit `'.$limit.'` provided. Expecting an '.
    'integer', E_USER_WARNING);
  return $subject;
}
// Invalid $limit provided
if($limit<-1){
  trigger_error('Invalid $limit `'.$limit.'` provided. Expecting -1 or '.
    'a positive integer', E_USER_WARNING);
  return $subject;
}
// No replacements necessary
if($limit===0){
  trigger_error('Invalid $limit `'.$limit.'` provided. Expecting -1 or '.
    'a positive integer', E_USER_NOTICE);
  return $subject;
}
// Use str_replace() when possible
if($limit===-1){
  return str_replace($search, $replace, $subject, $count);
}
if(is_array($subject)){
  // Loop through $subject values
  foreach($subject as $key => $this_subject){
   // Skip values that are arrays
   if(!is_array($this_subject)){
    // Call this function again
    $this_function = __FUNCTION__;
    $subject[$key] = $this_function($search, $replace, $this_subject, $this_count, $limit);
    // Adjust $count
    $count += $this_count;
    // Adjust $limit
    if($limit!=-1){
     $limit -= $this_count;
    }
    // Reached $limit
    if($limit===0){
     return $subject;
    }
   }
  }
  return $subject;
} elseif(is_array($search)){
  // Clear keys of $search
  $search = array_values($search);
  // Clear keys of $replace
  if(is_array($replace)){
   $replace = array_values($replace);
  }
  // Loop through $search
  foreach($search as $key => $this_search){
   // Don't support multi-dimensional arrays
   $this_search = strval($this_search);
   // If $replace is an array, use $replace[$key] if exists, else ''
   if(is_array($replace)){
    if(array_key_exists($key, $replace)){
     $this_replace = strval($replace[$key]);
    } else {
     $this_replace = '';
    }
   } else {
    $this_replace = strval($replace);
   }
   // Call this function again for
   $this_function = __FUNCTION__;
   $subject = $this_function($this_search, $this_replace, $subject, $this_count, $limit);
   // Adjust $count
   $count += $this_count;
   // Adjust $limit
   if($limit!=-1){
    $limit -= $this_count;
   }
   // Reached $limit
   if($limit===0){
    return $subject;
   }
  }
  return $subject;
} else {
  $search = strval($search);
  $replace = strval($replace);
  // Get position of first $search
  $pos = strpos($subject, $search);
  // Return $subject if $search cannot be found
  if($pos===false){
   return $subject;
  }
  // Get length of $search
  $search_len = strlen($search);
  // Loop until $search cannot be found or $limit is reached
  for($i=0;(($i<$limit)||($limit===-1));$i++){
   $subject = substr_replace($subject, $replace, $pos, $search_len);
   // Increase $count
   $count++;
   // Get location of next $search
   $pos = strpos($subject, $search);
   // Break out of loop
   if($pos===false){
    break;
   }
  }
  return $subject;
}
}


function parseStr($str) {
	$arr = explode(";", $str);
	foreach($arr as $val) {
		if(!empty($val))
			$out[] = $val;
	}
	return $out;
}

function strtolower_ru($text) {
	$alfavitlover = array('�','�','�','�','�','�','�','�', '�','�','�','�','�','�','�','�', '�','�','�','�','�','�','�','�', '�','�','�','�','�','�','�','�','�');
	$alfavitupper = array('�','�','�','�','�','�','�','�', '�','�','�','�','�','�','�','�', '�','�','�','�','�','�','�','�', '�','�','�','�','�','�','�','�','�');
		
	return str_replace($alfavitupper,$alfavitlover,strtolower($text));
}

function ucfirst_ru($text) {
	$alfavitlover = array('�','�','�','�','�','�','�','�', '�','�','�','�','�','�','�','�', '�','�','�','�','�','�','�','�', '�','�','�','�','�','�','�','�','�');
	$alfavitupper = array('�','�','�','�','�','�','�','�', '�','�','�','�','�','�','�','�', '�','�','�','�','�','�','�','�', '�','�','�','�','�','�','�','�','�');
  
  $line = iconv("UTF-8", "Windows-1251", $text); // convert to windows-1251
  $line = ucfirst($line);
  $line = iconv("Windows-1251", "UTF-8", $line); // convert back to utf-8
  
  return $line;  
		
	return str_replace_limit($alfavitupper,$alfavitlover,strtolower($text),$count, 1);
}

function getTypeIdGraduate($code) {
	global $DB;
	$DB->SetTable("nsau_types");
	$DB->AddCondFS("code", "=", $code);
	$DB->AddCondFS("qual_id", "=", 3);
	$res = $DB->Select(1);
	$row = $DB->FetchAssoc($res);
	return $row["id"];
}

function getQualName($id) {
	global $DB;
	$DB->SetTable("nsau_qualifications");
	$DB->AddCondFS("id", "=", $id);
	$DB->AddField("name");
	$res = $DB->Select(1);
	$row = $DB->FetchAssoc($res);
	return $row["name"];
}

function deleteSpecAbit() {
	global $DB;
		//delete old
	$DB->SetTable("nsau_spec_abit");
	$DB->AddCondFS("qualification", "=", 3);
	$DB->Delete();
}

function insertSpecAbit($arr, $fields) {
	global $DB;
	//add new
	$DB->SetTable("nsau_spec_abit");
	foreach($arr as $id => $val) {
		$DB->AddValue($fields[$id], $val);
		// echo $fields[$id]."=>".$val."    -    ";
	}
	// echo "<br />";
	// die($DB->InsertQuery());
	$DB->Insert();
}
function getTypeId($code, $pref) {
	global $DB;
	$DB->SetTable($pref."_types");
	$DB->AddCondFS("code", "=", $code);
	$row = $DB->FetchAssoc($DB->Select(1));
	return $row["id"];
}
function getQualId($qual) {
	global $DB;
	$DB->SetTable("nsau_qualifications");
	$DB->AddCondFS("name", "=", $qual);
	$row = $DB->FetchAssoc($DB->Select(1));
	return $row["id"];
}
function addPeople($full_name, $pass_series, $pass_num) {
	global $DB;
	$arr = explode(" ", trim($full_name));
	$name = $arr[1];
	$last_name = $arr[0];
	$patronymic = $arr[2];
	$DB->AddTable("nsau_people");
	$DB->AddCondFS("pass_series", "=", trim($pass_series));
	$DB->AddCondFS("pass_num", "=", trim($pass_num));
	$rp = $DB->Select(1);
	if($p = $DB->FetchAssoc($rp)) {
		$p_full = $p["last_name"]." ".$p["name"]." ".$p["patronymic"];
		if(trim($p_full)!=trim($full_name)) {
			$DB->SetTable("nsau_people");
			$DB->AddCondFS("pass_series", "=", trim($pass_series));
			$DB->AddCondFS("pass_num", "=", trim($pass_num));
			$DB->AddValue("name", $name);
			$DB->AddValue("last_name", $last_name);
			$DB->AddValue("patronymic", $patronymic);
			$DB->Update();		
			return $p["id"];			
		} else
			return $p["id"];
	}
	else { 
		$arr = explode(" ", trim($full_name));
		$name = $arr[1];
		$last_name = $arr[0];
		$patronymic = $arr[2];
		$DB->SetTable("nsau_people");
		$DB->AddValue("name", $name);
		$DB->AddValue("last_name", $last_name);
		$DB->AddValue("patronymic", $patronymic);
		$DB->AddValue("pass_series", $pass_series);
		$DB->AddValue("pass_num", $pass_num);
		$DB->AddValue("male", 1);
		$DB->AddValue("status_id", 7);
		$DB->AddValue("people_cat", 1);
		$DB->AddValue("year", 2016);
		$DB->Insert();
		return $DB->LastInsertID();
	}
}
?>
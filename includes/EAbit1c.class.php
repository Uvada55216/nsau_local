<?php
use core1C\Helper1C as H;
use abit\AbitCollection;
use abit\AbitRequests;
use abit\Speciality;
use abit\SpecialityCollection;
use abit\RequestParams;
use abit\AbitClient1c;
use abit\AbitFilter;
use requests\Request;


class EAbit1c
{
	protected $output;

	public $node_id;
	public $module_id;
	public $module_uri;
	public $mode;
	
	public function __construct($global_params, $params, $module_id, $node_id, $module_uri)
	{
		
		global $Engine, $Auth, $DB, $DB2, $DB_LOGS;
		$this->node_id = $node_id;
		$this->module_id = $module_id;
		
		if ($params && preg_match('/;/', $params)) {
			$paramParts = explode(';', $params);
			$this->output["mode"] = $this->mode = $paramParts[0];
			$this->subParams = array_slice($paramParts, 1);
		} else{
			$this->output["mode"] = $this->mode = $params;
		}
		$this->output["user_displayed_name"] = $Auth->user_displayed_name;
		$this->output["module_id"] = $this->module_id;

		if(!empty($Engine->module_uri)) {
			$this->module_uri = explode('/', $Engine->module_uri);
		}
		
		switch ($this->mode)
		{
			case "pk_reports": {
				$request = Request::init()->ajax();
				$mode = $this->module_uri[0];
				$subMode = $this->module_uri[1];
				$this->output['scripts_mode'] = $this->output['mode'] = 'pk_reports_main';
				$this->output['specialityForm'] = $subMode == 'extramural' ? 'Заочная' : 'Очная';										
				if(!empty($request['type'])) {
					$this->output['mode'] = '_pk_reports_table';
					header("Content-type: text/html; charset=windows-1251");
					switch ($request['type']) {
						case 'full-time':
							$this->output['specialityForm'] = 'Очная';	
						break;
						case 'extramural':
							$this->output['specialityForm'] = 'Заочная';
						break;
					}				
				}
				$this->output['idPk'] = $this->subParams[0] ? $this->subParams[0] : $request['idPk'];				
				$Params = new RequestParams();
				$Params->setParam('idPk', $this->output['idPk'])
						->setParam('specialityForm', $this->output['specialityForm']);

				try {
					$Client = new AbitClient1c();
					$Client->setCacheLifetime(2);
					// $Client->disableCache();
					switch($mode) {
						case "report": {
							$Engine->main_template = "_blank";
							$this->excelHeaders('report_'.date('d.m.Y'));
							$Specialities = new SpecialityCollection($Params, $Client);
							$this->output['specialities'] = $Specialities->getFullSpecialities();
							\helpers\TplHelper::beginCase('EAbit1c', '_block_simple_report_style');
							ob_start(function($buffer) {
								return (str_replace("<a", "<span", $buffer));
							});
							\helpers\TplHelper::beginCase('EAbit1c', '_pk_reports_table', $this->output);
							ob_end_flush();
							exit;
						} break;
						case "speciality": {
							$this->output['mode'] = $this->output['scripts_mode'] = 'abits_report_table';	
							$Params->setParam('specialityCode', $this->module_uri[2]);
							$SpecialityInfo = $Client->getSpecialityInfo(array(
								'idPk' => $Params->idPk, 
								'specialityCode'=> $Params->specialityCode, 
								'form' => $Params->specialityForm)
							);
							$this->output['speciality'] = new Speciality($SpecialityInfo);																						
							$Abits = new AbitCollection($Params, $Client);
							$this->output['abits'] = AbitFilter::init($Abits)->active()->allBudgetTypes()->getAbits();

							if($this->module_uri[3] == 'download') {
								$this->excelHeaders('abits_report_'.date('d.m.Y'));
								\helpers\TplHelper::beginCase('EAbit1c', '_block_simple_report_style');
								ob_start(function($buffer) {
									return (str_replace("<a", "<span", $buffer));
								});
								\helpers\TplHelper::beginCase('EAbit1c', '_abits_table_table', $this->output);
								ob_end_flush();
								exit;
							}							
						}
						break;
						default: {
							if(!empty($request['type'])) {
								$Specialities = new SpecialityCollection($Params, $Client);
								$this->output['specialities'] = $Specialities->getFullSpecialities();
							}
						} break;
					}
				} catch(Exception $e) {
					$this->output['errors'] = $e->getMessage();
				} 		
			}
			break;

			case "abits": {
				$this->output['view'] = $this->subParams[0];
				$this->output['idPk'] = $this->subParams[1];
				$this->output['specialityForm'] = H::w2u($this->subParams[2]);

				switch ($this->subParams[3]) {
					case 'magistracy':
						$this->output['abits_table_type'] = '_abits_magistracy_table_table';
						$abitsType = 'isMagistracy';
					break;	
					case 'graduate':
						$this->output['abits_table_type'] = '_abits_graduate_table_table';
						$abitsType = 'isGraduate';
					break;				
					default:
						$this->output['abits_table_type'] = '_abits_table_table';
						$abitsType = 'isBachelor';
					break;
				}

				$mode = $this->module_uri[0];
				try {
					$Params = new RequestParams();
					$Params->setParam('idPk', $this->output['idPk'])
							->setParam('specialityForm', $this->output['specialityForm']);		
					$Client = new AbitClient1c();			
					switch($mode) {
						case "people": {
							$this->output['mode'] = $this->output['scripts_mode'] = 'people';						
							$Params->setParam('peopleId', $this->module_uri[1]);
							$Abits = new AbitRequests($Params, $Client);
							$this->output['abits'] = $Abits->getAbits();
						}
						break;
						case "speciality": {
							$this->output['mode'] = $this->output['scripts_mode'] = 'abits_table';		
							$Params->setParam('specialityCode', $this->module_uri[1]);
							$SpecialityInfo = $Client->getSpecialityInfo(array(
								'idPk' => $Params->idPk, 
								'specialityCode'=> $Params->specialityCode, 
								'form' => $Params->specialityForm)
							);
							$this->output['speciality'] = new Speciality($SpecialityInfo);						
													

							if(method_exists('abit\AbitFilter', $_POST['budgetType'])
							&& method_exists('abit\AbitFilter', $_POST['listType'])) {
								$Abits = new AbitCollection($Params, $Client, $this->output['speciality']);
								$this->output['abits'] = AbitFilter::init($Abits)
														->{$_POST['listType']}()->{$_POST['budgetType']}()
														->getAbits();
							} else {
								$Abits = new AbitCollection($Params, $Client, $this->output['speciality']);
								$this->output['abits'] =  AbitFilter::init($Abits)
																->active()
																->allBudgetTypes()
																->{$abitsType}()
																->getAbits();
							}
							
							$Specialities = new SpecialityCollection($Params, $Client);
							$this->output['specialities'] = $Specialities->getSpecialities();
							$this->output['post'] = $_POST;							
						}
						break;

						case "report": {
							$Engine->main_template = "_blank";
							$this->excelHeaders('report_'.date('d.m.Y'));
							$Specialities = new SpecialityCollection($Params, $Client);
							$this->output['specialities'] = $Specialities->getSpecialities();
							\helpers\TplHelper::beginCase('EAbit1c', '_block_simple_report_style');
							ob_start(function($buffer) {
								return (str_replace("<a", "<span", $buffer));
							});
							\helpers\TplHelper::beginCase('EAbit1c', '_block_abits_specialities_table',$this->output);
							ob_end_flush();
						}
						default: {
							$this->output["show_report"] = $Engine->OperationAllowed($this->module_id, 'abits.show_report', -1, $Auth->usergroup_id);
							$this->output['mode'] = $this->output['scripts_mode'] = 'abits_specialities_' . $this->output['view'];
							$Specialities = new SpecialityCollection($Params, $Client);
							$this->output['specialities'] = $Specialities->getSpecialities();
						}					
					}
				} catch(Exception $e) {
					$this->output['errors'] = $e->getMessage();
				} 		
			}
			break;

		}
	}

	function excelHeaders($filename) 
	{
		header('Content-Type: text/html; charset=windows-1251');
		header('P3P: CP="NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM"');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0', FALSE);
		header('Pragma: no-cache');
		header('Content-transfer-encoding: binary');
		header("Content-Disposition: attachment; filename=$filename.doc");
		header('Content-Type: application/msword');	
	}

	function Output() 
	{
		return $this->output;
	}
}
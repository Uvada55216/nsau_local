<?php
class EAjax
// version: 2.1
// date: 2013-10-11
{
	var $output;

	function GetBaseUrl()
	{
		global $Engine, $DB;
		return $_SERVER['REQUEST_URI'];
	}

	function EAjax($global_params, $params, $module_id, $node_id, $module_uri)
	{
		
		global $Engine, $Auth, $DB;
		if(empty($_REQUEST)) {
			$request_body = file_get_contents('php://input');
			$data = json_decode($request_body, true);
			$_REQUEST = $data ;
			$_REQUEST['data'] = json_encode($data['data']);
		}
	  	$this->output = null;
	  	if(!empty($_REQUEST['route']) && !empty($_REQUEST['callback'])) {
	  		$route = str_replace('/', '\\', $_REQUEST['route']);
	  		$ModuleConfig = new $route();
	  		$callback = new engine\modules\configElements\Callback(
	  			$ModuleConfig,
	  			$_REQUEST['callback'], 
	  			\helpers\EncodingHelper::changeArrayEncoding($_REQUEST['params'], 'u2w')
	  		);
	  		$content = $callback->execute();
	  		$ModuleConfig->__test = $content;
  			$content = \helpers\EncodingHelper::changeArrayEncoding($content, 'w2u');
  			$content = json_encode($content);
	  		echo $content;
			exit;	  		
	  	}


		if(!empty($_REQUEST['route']) && !empty($_REQUEST['module'])) {

			if(!empty($_REQUEST['context'])) {
				$node = $DB->SetTable('engine_nodes')
						->AddCondFS('id', '=', intval($_REQUEST['context']))
						->SelectArray(1);
			}

			$_SERVER['REQUEST_URI'] = $_SERVER['HTTP_REFERER'];
			$route = str_replace('/', '\\', $_REQUEST['route']);
			$className = '\\modules\\' . $_REQUEST['module'] . '\\' . $_REQUEST['module'];
			$Module = new $className(new engine\modules\RouteInit(
											$route,
											$_REQUEST['action'],
											$_REQUEST['params']), 
									 $node['params'] ? unserialize($node['params']) : null,
									 new engine\modules\ContextData($_REQUEST['context'], $this->getFullFolderPath($node['folder_id'])));
			$content = $Module->output();
			if($_REQUEST['type'] == 'html') {
				header("Content-type: text/plain; charset=windows-1251");
				header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");  
				header("Last-Modified: " . gmdate( "D, d M Y H:i:s") . " GMT"); 
				header("Cache-Control: no-cache, must-revalidate"); 
				header("Pragma: no-cache");
				echo $content;
				exit;
			}
			if($_REQUEST['type'] == 'json') {
				// $content = json_encode($content);
				echo \helpers\EncodingHelper::w2u($content);
				exit;
			}			

			
			
		}
		
		if (isset($_REQUEST) && isset($_REQUEST['module'])) {
			$DB->SetTable('engine_modules');
			$DB->AddCondFs('id', '=', $_REQUEST['module']);
			if ($res = $DB->Select()) {
				$row = $DB->FetchObject($res);
				/**/
				if (!isset($Engine->modules_privileges[$_REQUEST['module']])) {
					$Engine->modules_privileges += $Engine->GetPrivileges($_REQUEST['module'], NULL, NULL, $Auth->usergroup_id);
				}
				/**/
				include_once INCLUDES . $row->name . CLASS_EXT;
				
				if (isset($_REQUEST['mode']) && $_REQUEST['mode'] == 'json') {
					ob_start(); 
				}
					
				$Module = new $row->name(
									$row->global_params,
									isset($_REQUEST['params']) ? $_REQUEST['params'] : null,
									$_REQUEST['module'],
									isset($_REQUEST['node']) ? $_REQUEST['node'] : null,
									isset($_REQUEST['module_uri']) ? $_REQUEST['module_uri'] : null,
									isset($_REQUEST['data']) ? $_REQUEST['data'] : null
									);
		
				$data =  $Module->Output();
					
				if (isset($_REQUEST['mode']) && $_REQUEST['mode'] == 'json') {
					$content = ob_get_contents();
					ob_end_clean();
					if(isset($data["json"])) $data = $data["json"]; 
					if(!empty($content)) $data["output"] = $content;
					$data = CF::Win2Utf($data); //CF::ArrMap($data, function($str){ return CF::Win2Utf($str); }); 
					$data = json_encode($data);
				}
				
				$MODULE_OUTPUT = $this->output["data"] = $data;
				if(isset($_REQUEST['mode']) && $_REQUEST['mode'] == 'html_code') {
					include_once THEMES_DIR . $row->name . TEMPLATE_EXT;
					$this->output["mode"] = "html_code";
					exit;
				}
			}
		} 
	
	}

	function getFullFolderPath($node_id) {
		global $DB;
		$node = $DB->SetTable('engine_folders')
				->AddCondFS('id', '=', intval($node_id))
				->AddField('uri_part')
				->AddField('pid')
				->SelectArray(1);
		$path .= '/' . $node['uri_part'];
		if($node['pid'] != 0) {
			$path = $this->getFullFolderPath($node['pid']) . $path;
		}
		return $path;
	}

	function Output()
	{  
		return $this->output;
	}
	
}
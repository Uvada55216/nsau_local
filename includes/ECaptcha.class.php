<?php

class ECaptcha
// version: 2.0 alpha 2
// date: 2008-05-16
{
	var $output;

    // � $params ��������� ����� ����� � �������: ����� �������� �� 4 �� 32, ������ �������� ������������ ������ �����
	function ECaptcha($global_params, $params, $module_id, $node_id = NULL, $module_uri = NULL, $additional = NULL)
	{
		$parts = explode(";", $params);
		$length = intval($parts[0]);
		$numbers_only = isset($parts[1]) ? (bool)$parts[1] : false;

		if ($length < 6)
		{
			$length = 6;
		}

		elseif ($length > 32)
		{
			$length = 32;
		}


		$this->output["length"] = $length;


		if ($numbers_only)
		{
			$this->output["code"] = "";

			for ($i = 0; $i < $length; $i++)
			{
				$this->output["code"] .= rand(0, 9);
			}

			$_SESSION["code"] = $this->output["code"];
		}

		else
		{
			//$this->output["code"] = $_SESSION["code"] = substr(md5(uniqid($_SERVER["SERVER_NAME"])), 0, $length);
			//$test = ("���������������������������������");
			  $this->output["code"] = $_SESSION["code"] = substr($this->generatePassword(20), 0, $length);
		}
	}

	//������ ���� md5
	function generatePassword($length){
  	$chars = '�������������������0123456789';
  	$numChars = strlen($chars);
  	$string = '';
  	for ($i = 0; $i < $length; $i++)
  	{
    	$string .= substr($chars, rand(1, $numChars) - 1, 1);
  	}
  	return $string;
	}

	function Output()
	{
		return $this->output;
	}
}

?>
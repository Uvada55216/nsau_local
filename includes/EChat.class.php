<?php
include "echat/theme.class.php";
include "echat/message.class.php";

class Echat
{
	function Echat ($global_params, $params, $module_id, $node_id, $module_uri)
	{
		global $Engine, $Auth, $DB, $notice;

        $this->options = $Engine->GetModuleOptions($module_id);

        /*�����������*/
        $this->notice = array
        (
            1 => "���� �������",
            2 => "�� ������� ������� ����",
            3 => "���� ��� ���� ��� �������� ����, ���� �� ������� ������������ ���������� ���",
            4 => "�������� ���� �� ����� ���� ������",
            5 => "�� �� ������ ������� ��� ����",
            6 => "��������� ������� ����",
            7 => "���� �������",
            8 => "������� ���� ������������",
            9 => "��������� �����������",
            10=> "�� ������� ��������� ��������� (������ ��)",
            11=> "�� �� ������ ���������� ��������� � ���� ���",
            12=> "�� �� ������ ��������� ��������� ����� ����",
            13=> "������������ ��������",
            14=> "������������ ��� ��������",
            15=> "�� ������� �������� ������������",
            16=> "�� ��� ������ ��������� �������������",
            17=> "� ���� ���� ��� �������������",
            18=> "�� �� ������ ����� �� ������ ���� (��� ����� ������ �������)",
            19=> "� ��� ��� ���� �� �������� ������������� �� ����� ����",
            20=> "������������ ������", 
            21=> "�� ������� ������� ������������",
            22=> "��������� ���������",
            23=> "�� ������� ��������� ���������",
            24=> "� ��� ��� ���� �������� ���������",
            25=> "������ ���� ���������� ��� ����",
            26=> "��������� �������",
            27=> "�� ������� ������� ���������",
            28=> "�� �� ������ ������� ��������� ���� ����"
        );

        //$this->output["options"] = $options;

        switch ($params) 
        {
        	case 'education_chat':
        	{
                $this->output["scripts_mode"] = $this->output["mode"] = $params;
        	}
        	break;

            /*�������� ����� ����*/
            case 'ajax_create_theme':
            {
                $theme = new Theme (NULL, $this->options, $this->notice);
                $this->output["json"] = $theme->Create_new_theme($this->validation($_REQUEST["data"]["theme_name"]), $this->validation($_REQUEST["data"]["theme_description"]));
                unset($theme);
            }
            break;

            /*�������� ����*/
            case 'ajax_delete_theme':
            {
                $theme = new Theme ($_REQUEST["data"]["theme_id"], $this->options, $this->notice);
                $this->output["json"] = $theme->Delete_theme();
                unset($theme);
            }
            break;

            /*��������� � ���� ����*/
            case 'ajax_goto_theme':
            {
                $theme = new Theme ($_REQUEST["data"]["theme_id"], $this->options, $this->notice);
                $theme_creator = $theme->Get_theme_creator();
                $result = array( 
                    'id'            => $_REQUEST["data"]["theme_id"],
                    'description'   => $theme->Get_theme_description(), 
                    'theme_name'    => $theme->Get_theme_name(),
                    'count_useres'  => $theme->Get_theme_users("count"),
                    'theme_creator'  => $theme_creator['auth_id']
                );
                unset($theme);

                if ($result) 
                {
                    $this->output["json"] = array('status' => 'ok', 'data' => $result);
                }
                else
                {
                    $this->output["json"] = array('status' => 'error', 'message' => $this->notice[8]);
                }
            }
            break;

            /*���������� json ������������� ����*/
            case 'ajax_get_chat_users':
            {
                $theme = new Theme ($_REQUEST["data"]["theme_id"], $this->options, $this->notice);
                $result = $theme->Get_theme_users("list");
                unset($theme);
                if ($result) 
                {
                    $this->output["json"] = array('status' => 'ok', 'data' => $result);
                }
                else
                {
                    $this->output["json"] = array('status' => 'error', 'message' => $this->notice[17]);
                }
            }
            break;

            /*���������� json ������ ���*/
            case 'ajax_themes_list':
            {
                //���� ��������� �������������
                $DB->SetTable("wall_themes");
                $DB->AddCondFS("creator_id", "=", $_REQUEST["data"]["user_id"]);
                $res = $DB->Select();
                while($row = $DB->FetchAssoc($res)) 
                {
                    $theme   = new Theme ($row['id'], $this->options, $this->notice);
                    $message = new Message ($row['id'], $this->options, $this->notice);
                    $last_message = $message->Get_last_message();

                    $themes_list[$row['id']] = array( 
                        'id'            => $row['id'],
                        'description'   => $theme->Get_theme_description(), 
                        'theme_name'    => $theme->Get_theme_name(),
                        'count_useres'  => $theme->Get_theme_users("count"),
                        'count_messages'=> $message->Get_messages_count(),
                        'last_message'  => $last_message['data']['message']
                    );
                    foreach ($theme->Get_theme_creator() as $key => $value) 
                    {
                         $themes_list[$row['id']][$key] = $value;
                    }
                    unset($theme);
                }
                //���� � ������� �������� ������������
                $DB->SetTable("wall_themes_users");
                $DB->AddCondFS("user_id", "=", $_REQUEST["data"]["user_id"]);
                $DB->AddCondFS("status", "!=", 1);
                $res = $DB->Select();
                while($row = $DB->FetchAssoc($res)) 
                {
                    $theme      = new Theme ($row['theme_id'], $this->options, $this->notice);
                    $message    = new Message ($row['theme_id'], $this->options, $this->notice);
                    $last_message = $message->Get_last_message();
                    
                    $themes_list[$row['id']] = array( 
                        'id'            => $row['theme_id'],
                        'description'   => $theme->Get_theme_description(), 
                        'theme_name'    => $theme->Get_theme_name(),
                        'count_useres'  => $theme->Get_theme_users("count"),
                         'count_messages'=> $message->Get_messages_count(),
                        'last_message'  => $last_message['data']['message']
                    );
                    foreach ($theme->Get_theme_creator() as $key => $value) 
                    {
                         $themes_list[$row['id']][$key] = $value;
                    }
                    unset($theme);
                }
                $this->output["json"] = array('status' => 'ok', 'data' => $themes_list);
            }
            break;

            /*���������� ���������*/
            case 'ajax_send_message':
            {
                $theme = new Message ($_REQUEST["data"]["theme_id"], $this->options, $this->notice);
                $this->output["json"] = $theme->Send_message($this->validation($_REQUEST["data"]["message"]));
                //unset($theme);
            }
            break;

            /*������� ���������*/
            case 'ajax_delete_message':
            {
                $theme = new Message ($_REQUEST["data"]["theme_id"], $this->options, $this->notice);
                $this->output["json"] = $theme->Delete_message($_REQUEST["data"]["message_id"]);
            }
            break;

            /*���������� user_id ��������� ����*/
            case 'ajax_creator_id':
            {
                $theme = new Theme ($_REQUEST["data"]["theme_id"], $this->options, $this->notice);
                $this->output["json"] = $theme->Get_theme_creator();
            }
            break;

            /*�������� ��������� ����*/
            case 'ajax_load_chat_messages':
            {
                $theme = new Message ($_REQUEST["data"]["theme_id"], $this->options, $this->notice);
                $this->output["json"] = $theme->Get_messages($_REQUEST["data"]["last_message_id"]);
            }
            break;

            /*���������� ������������ � ���*/
            case 'ajax_invite_user':
            {
                $theme = new Theme ($_REQUEST["data"]["theme_id"], $this->options, $this->notice);
                $this->output["json"] = $theme->Invite_user($_REQUEST["data"]["user_id"]);
            }
            break;

            /*��������� ���������*/
            case 'ajax_apply_settings':
            {
                $theme = new Theme ($_REQUEST["data"]["theme_id"], $this->options, $this->notice);
                $this->output["json"] = $theme->Change_theme_name($this->validation($_REQUEST["data"]["theme_name"]), $this->validation($_REQUEST["data"]["theme_description"]));
            }
            break;

            /*������� ������������ �� ����*/
            case 'ajax_delete_user':
            {
                $theme = new Theme ($_REQUEST["data"]["theme_id"], $this->options, $this->notice);
                $this->output["json"] = $theme->Delete_user($_REQUEST["data"]["user_id"]);
            }
            break;

            /*����� ����� � people ��� ������������*/
            case "ajax_search_people": 
            {
                $request = trim($_REQUEST["data"]["s"]);
                
                $result = array();
                if($request != "")
                {
                    $request = explode(" ", $request );
                    $DB->SetTable("nsau_people");
                    foreach($request as $req)
                    {
                        $DB->AddAltFS("last_name", "LIKE", "%".iconv("utf-8", "Windows-1251", $req)."%");
                        $DB->AddAltFS("name", "LIKE", "%".iconv("utf-8", "Windows-1251", $req)."%");
                        $DB->AddAltFS("patronymic", "LIKE", "%".iconv("utf-8", "Windows-1251", $req)."%");
                        $DB->AppendAlts();
                    }
                    $res = $DB->Select();
                    $count = 0; 
                    while($row = $DB->FetchAssoc($res)) {
                        $result[$count]["name"] = $row["last_name"]." ".$row["name"]." ".$row["patronymic"];
                        $result[$count]["user_id"] = $row["user_id"];

                        switch ($row["people_cat"]) 
                        {
                            case 1:
                            $result[$count]["status"] = '�������';
                            break;
                            
                            case 2:
                            $result[$count]["status"] = '�������������';
                            break;

                            case 3:
                            $result[$count]["status"] = '���������';
                            break;
                        }

                        $result[$count]["photo"] = $row["photo"];
                        $count++; 
                        if($count >= 30) break; 
                    }
                    header("Cache-Control: no-cache, must-revalidate");
                    header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
                    header("Pragma: no-cache");  
                } else $result = "";
                $this->output["json"] = $result;    
            }
            break;  
        }
	}

    function Output() 
    {
        return $this->output;
    }

    function validation ($str)
    {
        $result = str_replace("script", "", $str);
        $result = str_replace("<?", "", $str);
        $result = str_replace("?>", "", $str);
        return $this->encode_html($result);
    }

    function encode_html($str,$type='code')
    {
        if($type=='code'){return htmlspecialchars($str,ENT_QUOTES);}
        if($type=='encode')
        {
            $trans=get_html_translation_table(HTML_ENTITIES,ENT_QUOTES);
            $trans=array_flip($trans);;
            return strtr($str, $trans);
        }
    }

}

?>


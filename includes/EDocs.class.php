<?php

use files\File;
use files\forms\FileForm;

class EDocs
{
	var $module_id;
	var $node_id;
	var $module_uri;
	var $privileges;
	var $output;

	var $mode;
	var $department_id;
	var $db_prefix;
	var $uri_params;


	var $error = false;
	
	function EDocs($global_params, $params, $module_id, $node_id = NULL, $module_uri = NULL, $additional = NULL)
	{
		global $DB, $Engine, $Auth;

		$this->module_id = $module_id;
		$this->node_id = $node_id;
		$this->module_uri = $module_uri;
		$this->additional = $additional;
		$this->output = array();
		$this->output["messages"] = array("good" => array(), "bad" => array());
		$this->output["module_id"] = $this->module_id;

		$parts = explode(";", $global_params);
		$this->db_prefix = $parts[0];
		$parts2 = explode(";", $params);

		$this->uri_params = explode("/", $module_uri);
		

		switch ($this->mode = $parts2[0])
		{
			case "upload": {
				$this->obligatory_fields = array("doc_type_id", "qualification_id", "file_id", "year", "form_1", "form_2", "form_3", "form_4");
				$this->obligatory_fields_name = array("qualification_id"=>"������������", "doc_type_id"=>"���", "file_id"=>"����", "year"=>"���");
				$this->output["info"] = $this->addictionalData(intval($this->uri_params[0]));




				$url = explode( "/", $_SERVER['REQUEST_URI']);
				if ($url[count($url)-2] != "edit") 
				{
					$_POST['profile_id'] ? $tmp = $_POST['profile_id'] : $tmp = array ("0" => 0);

					foreach ($tmp as $key => $value) 
					{
						$_POST['profile_id'] = $value;

										$this->uploadDocs( intval($this->uri_params[0]), 
				
						$this->validateForm(
						array_map(function($v){return ($v > 0);},
						$_POST = array_map("intval", $_POST))
							)
						);
					}
				}
				else
				{
					$this->uploadDocs( intval($this->uri_params[0]), 
					$this->validateForm(
					array_map(function($v){return ($v > 0);},
					$_POST = array_map("intval", $_POST))
						)
					);
							
				}



				/*������ �����*/
				$this->output['form']['folders'] = FileForm::getUserFolders();

				/*�������� �����*/
				$_POST['file'] = $_FILES[0];

				$FileForm = new FileForm();
				if($FileForm->validate($_POST)) {
					$new_id = File::create($FileForm->getData());	
					$Engine->LogAction($this->module_id, "file", $new_id, "create");							
				} else {
					$this->output['json']["errors"] = $FileForm->getErrors();
				}
			}
			break;

			case 'all_spec':
			{
				$this->output["mode"] = $this->output["scripts_mode"] = "all_spec";

				global $DB;

				$DB->SetTable("nsau_specialities", "spec");
				$DB->AddCondFS("spec.old", "=", 0);
				$DB->AddCondFS("spec.hide_accredit", "=", 0);

				$DB->AddField("spec.id", "id");
				$DB->AddField("spec.code", "code");
				$DB->AddField("spec.name", "name");
				
				$DB->AddTable("nsau_spec_type", "type");
				$DB->AddCondFF("spec.id", "=", "type.spec_id");

				$DB->AddField("type.type", "type");

				$DB->AddTable("nsau_docs", "docs");
				$DB->AddCondFS("docs.doc_type_id", "=", 1);
				$DB->AddCondFF("spec.id", "=", "docs.speciality_id");

				$DB->AddField("docs.file_id", "file_id");
				$DB->AddField("docs.form", "form");
				$DB->AddField("docs.year", "year");
				$DB->AddField("docs.doc_type_id", "doc_type_id");
				$DB->AddGroupings(array('spec.id', 'spec.code', 'docs.year'));

				$res = $DB->Select();

				while($row = $DB->FetchAssoc($res)) 
				{
					switch ($row['type'])
		    		{
		    			case 'secondary':
		    			{
		    				$row['type'] = '������� ���������������� ����������� - ���������� ������������ �������� �����';
		    			}
		    			break;
		    			case 'bachelor':
		    			{
		    				$row['type'] ='������ ����������� - �����������';
		    			}
		    			break;
		    			case 'magistracy':
		    			{
		    				$row['type'] ='������ ����������� - ������������';
		    			}
		    			break;
		    			case 'graduate':
		    			{
		    				$row['type'] ='������ ����������� - ���������� ������ ������ ������������';
		    			}
		    			break;
		    			case 'higher':
		    			{
		    					$row['type'] ='������ ����������� - �����������';
		    			}
		    			break;
		    		}

		    		switch ($row['form']) 
		    		{
		    			case 1:
		    				$row['form'] = "�����";
		    				break;
		    			
		    			case 2:
		    				$row['form'] = "�������";
		    				break;
		    			case 3:
		    				$row['form'] = "����-�������";
		    				break;
		    		}
		    		$row['file_id']="/file/".$row['file_id'];
		    		$row['link'] = "/speciality/".$row['code']."/".$row['id'];
//					$result[$row['id']] = $row;
					$result[] = $row;
				}

				foreach ($result as $k => $v) 
				{
					$result2[] = $v;
				}

				$this->output["json"]["all_spec"] =  $result2;
			}
			break;

			/*������ ���� ���������� ������������*/
			case 'ajax_get_user_docs':
			{
				global $DB, $Auth;

				$DB->SetTable("nsau_files");
				$DB->AddCondFS("user_id", "=", $Auth->user_id);
				$DB->AddCondFS("deleted", "=", 0);

				if (!empty($_REQUEST["data"]["file_name"])) 
				{
					$DB->AddCondFS("descr", "LIKE", "".iconv("utf-8", "Windows-1251", $_REQUEST['data']["file_name"])."%");
				}
				$res = $DB->Select();
				while($file = $DB->FetchAssoc($res)) {
					//$result["files"][$file["id"]] = $file["name"];
					// if ($file["name"] != "undefined") 
					// {
					// 	# code...
					// }
					$result["files"][$file["id"]] = array('name' =>  $file["name"], 'description' => $file["descr"]);
				}	
				$this->output["json"] = $result;			
			}
			break;

			case "ajax_delete_doc": {
				$this->deleteDoc($_REQUEST["data"]["id"]);
				$this->output["json"] = "deleted";
			}
			break;

			/*����� �� ������*/
			case "ajax_get_doc": {
				//$this->deleteDoc($_REQUEST["data"]["id"]);

				global $DB, $Auth;

				//files
				$DB->SetTable("nsau_files");
				$DB->AddCondFS("user_id", "=", $Auth->user_id);
				$DB->AddCondFS("deleted", "=", 0);
				$DB->AddCondFS("name", "LIKE", "".iconv("utf-8", "Windows-1251", $_REQUEST['data']["q"])."%");
				$res = $DB->Select();
				while($file = $DB->FetchAssoc($res)) {
					//$result["files"][$file["id"]] = $file["name"];

					$result[] = array('id' => $file["id"], 'file' => $file["name"]);
				}

				$this->output["json"] = $result;
			}
			break;

			case "show_table_education":
			{
				$this->output["mode"] = $this->output["scripts_mode"] = "show_table_education";
				/*������ ���� ������������*/
				// $DB->SetTable("nsau_qualifications");
				// $DB->AddField("old_type");
				// $res = $DB->Select();
				// while ($row_ids = $DB->FetchAssoc($res))
				// {
				// 	$all_qual_old_type[]=array("old_type"=>$row_ids);
				// }
				// $all_qual_old_type= array_map("unserialize", array_unique( array_map("serialize", $all_qual_old_type) )); //���������� ���������� � �������



				$all_qual_old_type = array(0 => 'bachelor', 1 => 'higher', 2 => 'magistracy', 3=> 'graduate', 4=> 'secondary');
				$this->output["all_qual_old_type"] = $all_qual_old_type;



				foreach($all_qual_old_type as $type)
				{
					$DB->SetTable("nsau_spec_type");
					$DB->AddCondFS("type", "=", $type);
//					$DB->AddCondFS("has_vacancies", "=", 1); //1 - ����� �������
					$bach = $DB->Select();

					//baca, spec, mag, asp, spo
					while ($res_bach = $DB->FetchAssoc($bach))
					{
						if ($this->check_file($res_bach['spec_id'])==1)
						{
							/*� ��������-----------------------------------------------------------------*/
						    $DB->SetTable("nsau_profiles");
							$DB->AddCondFS("spec_id", "=", $res_bach['spec_id']);
							$prof = $DB->Select();
							while ($res_prof = $DB->FetchAssoc($prof))
							{
								$DB->SetTable("nsau_docs", 'd');
								$DB->AddCondFS("d.speciality_id", "=", $res_bach['spec_id']);
								$DB->AddTable('nsau_files', 'f');
								$DB->AddCondFF("f.id", "=", "d.file_id");
								$DB->AddField('d.id');
								$DB->AddField('d.speciality_id');
								$DB->AddField('d.profile_id');
								$DB->AddField('d.qualification_id');
								$DB->AddField('d.doc_type_id');
								$DB->AddField('d.file_id');
								$DB->AddField('d.year', 'doc_year');
								$DB->AddField('d.form');
								$DB->AddField('f.name');
								$DB->AddField('f.descr');
								$DB->AddField('f.author');
								$DB->AddField('f.year', 'file_year');
								$DB->AddField('f.volume');
								$DB->AddField('f.edition');
								$DB->AddField('f.place');
								$DB->AddField('f.filename');
								$DB->AddField('f.comment');
								$DB->AddField('f.user_id');
								$DB->AddField('f.is_html');
								$DB->AddField('f.down_count');
								$DB->AddField('f.folder_id');
								$DB->AddField('f.hash');
								$DB->AddField('f.deleted');
								$DB->AddField('f.delete_time');
								$DB->AddField('f.create_time');
								$DB->AddField('f.update_time');
								$DB->AddField('f.sw_id');
								$DB->AddField('f.sig_id');
								$DB->AddField('f.with_sig');
							    $docs = $DB->Select();

							    while ($res_docs = $DB->FetchAssoc($docs))
								{
									if ($res_bach['spec_id']==$res_docs['speciality_id'])
									{
										$bach_files[]=array
										(
											"file_id"=>$res_docs['file_id'],
											"doc_type_id"=>$res_docs['doc_type_id'],
											"year"=>$res_docs['doc_year'],
											"form"=>$res_docs['form'],
											"profile"=>$res_docs['profile_id'],
											"sig"=>$res_docs['sig_id'],
											"with_sig"=>$res_docs['with_sig'],
										);

										$form=$res_docs['form'];

										//���������� ���� � �������� �� �� �������� ���� ������ � nsau_docs
										if ($res_docs['profile_id'] == $res_prof['id'])
										{
											$year=$res_docs['doc_year'];
										}
									}

									/*������������ ����������� ����� ��������������� ���������-----------------------------*/
									
									$name="<strong>".$this->name_on_code($res_bach['code'])."</a></strong>";

									if ($this->get_fuc($res_bach['spec_id'], 1) == 9)
									{
										$name .= " <b>(����)</b>";
									}elseif ($this->get_fuc($res_bach['spec_id'], 1) == 21)
									{
										$name .= " <b>(����)</b>";
									}

									$name .= "<br> ������� <strong> ".$res_prof['name']."</strong>";

									// $name="<b itemprop='EduCode'>".$res_bach['code']."</b><br><b>".$this->name_on_code($res_bach['code'])."</a></b>";
									// $name .= "<br> ������� <strong> ".$res_prof['name']."</strong>";

									switch ($res_docs['form'])
									{
										case '1':
											$name .= "[����� ��������: �����]";
											break;

										case '2':
											$name .= "[����� ��������: �������]";
											break;

										case '3':
											$name .= "[����� ��������: ����-�������]";
											break;
									}

									if ($year)
									{
										$name .= "<br>��� ������: ".$year;
									}
									/*--------------------------------------------------------------------------------------*/

									/*������ ������*/
									$bach_data[$name]=array
									(
										"spec_id"=>$res_bach['spec_id'],
										"code"=>$res_bach['code'],
										"year"=>$year,
										"form"=>$form,
										"profile"=>$res_prof['id'],
										"facultet"=>$this->get_fuc($res_bach['spec_id'], 0),
										"hide_accredit"=>$this->get_fuc($res_bach['spec_id'], 2),
										"files"=>$bach_files,
									);
								}
								unset($year);
								unset($form);
								unset($bach_files);
							}
							/*---------------------------------------------------------------------------*/
							/*��� �������----------------------------------------------------------------*/
							$DB->SetTable("nsau_spec_type");
							$DB->AddCondFS("spec_id", "=", $res_bach['spec_id']);
							$prof = $DB->Select();
							while ($res_prof = $DB->FetchAssoc($prof))
							{

								$DB->SetTable("nsau_docs");
								$DB->AddCondFS("speciality_id", "=", $res_bach['spec_id']);
								$DB->AddTable('nsau_files', 'f');
								$DB->AddCondFF("f.id", "=", "d.file_id");
								$DB->AddField('d.id');
								$DB->AddField('d.speciality_id');
								$DB->AddField('d.profile_id');
								$DB->AddField('d.qualification_id');
								$DB->AddField('d.doc_type_id');
								$DB->AddField('d.file_id');
								$DB->AddField('d.year', 'doc_year');
								$DB->AddField('d.form');
								$DB->AddField('f.name');
								$DB->AddField('f.descr');
								$DB->AddField('f.author');
								$DB->AddField('f.year', 'file_year');
								$DB->AddField('f.volume');
								$DB->AddField('f.edition');
								$DB->AddField('f.place');
								$DB->AddField('f.filename');
								$DB->AddField('f.comment');
								$DB->AddField('f.user_id');
								$DB->AddField('f.is_html');
								$DB->AddField('f.down_count');
								$DB->AddField('f.folder_id');
								$DB->AddField('f.hash');
								$DB->AddField('f.deleted');
								$DB->AddField('f.delete_time');
								$DB->AddField('f.create_time');
								$DB->AddField('f.update_time');
								$DB->AddField('f.sw_id');
								$DB->AddField('f.sig_id');
								$DB->AddField('f.with_sig');
							    $docs = $DB->Select();
							    while ($res_docs = $DB->FetchAssoc($docs))
								{
										$bach_files[]=array
										(
											"file_id"=>$res_docs['file_id'],
											"doc_type_id"=>$res_docs['doc_type_id'],
											"year"=>$res_docs['doc_year'],
											"form"=>$res_docs['form'],
											"profile"=>$res_docs['profile_id'],
											"sig"=>$res_docs['sig_id'],
											"with_sig"=>$res_docs['with_sig'],
										);

										//���������� ���� � �������� �� �� �������� ���� ������ � nsau_docs
										if (!$res_docs['profile_id'])
										{
											$year_np=$res_docs['doc_year'];
										}
										$form_np=$res_docs['form'];

										/*������������ ����������� ����� ��������������� ���������-----------------------------*/
										$name= "<b>".$this->name_on_code($res_bach['code'])."</a></b>";

										if ($this->get_fuc($res_bach['spec_id'], 1) == 9)
										{
											$name .= " <b>(����)</b>";
										}elseif ($this->get_fuc($res_bach['spec_id'], 1) == 21)
										{
											$name .= " <b>(����)</b>";
										}

										if ($year_np)
										{
											switch ($res_docs['form'])
											{
												case '1':
													$name .= "[����� ��������: �����]";
													break;

												case '2':
													$name .= "[����� ��������: �������]";
													break;

												case '3':
													$name .= "[����� ��������: ����-�������]";
													break;
											}
										}

										if ($year_np)
										{
											$name .= "<br>��� ������: ".$year_np;
										}

										/*--------------------------------------------------------------------------------------*/
										/*������ ������*/
										$bach_data[$name]=array
										(
											"spec_id"=>$res_bach['spec_id'],
											"code"=>$res_bach['code'],
											"year"=>$year_np,
											"form"=>$form_np,
											"profile"=>0,
											"facultet"=>$this->get_fuc($res_bach['spec_id'], 0),
											"hide_accredit"=>$this->get_fuc($res_bach['spec_id'], 2),
											"files"=>$bach_files,
										);
								}
								unset($year_np);
								unset($form_np);
								unset($bach_files);
							}
						}
						else
						{
							/*������������ ����������� ����� ��������������� ���������-----------------------------*/
							$name="<b itemprop='EduCode'>".$res_bach['code']."</b><br><b>".$this->name_on_code($res_bach['code'])."</a></b>";
							/*--------------------------------------------------------------------------------------*/
							/*������ ������*/
							$bach_data[$name]=array
							(
								"spec_id"=>$res_bach['spec_id'],
								"code"=>$res_bach['code'],
								"profile"=>0,
								"facultet"=>$this->get_fuc($res_bach['spec_id'], 0),
								"hide_accredit"=>$this->get_fuc($res_bach['spec_id'], 2),
								"form"=>0,
								"year"=>0
							);
							unset($name);
						}
						/*----------------------------------------------------------------------------*/
					}
					uasort($bach_data, function($a, $b){
					    return (str_replace(".", "", $a['code']) - str_replace(".", "", $b['code']));
					});
					$this->output[$type] = $bach_data;
					unset($bach_data);
				}
			}
			break;

			case 'fucult_education':
		    {
		    	$this->output["mode"] = $this->output["scripts_mode"] = "fucult_education";
		    	//echo $parts2[1]; //id �����������

				/*������ ���� ������������*/
				$DB->SetTable("nsau_qualifications");
				$DB->AddField("old_type");
				$res = $DB->Select();
				while ($row_ids = $DB->FetchAssoc($res))
				{
					$all_qual_old_type[]=array("old_type"=>$row_ids);
				}
				$all_qual_old_type= array_map("unserialize", array_unique( array_map("serialize", $all_qual_old_type) )); //���������� ���������� � �������
				$this->output["all_qual_old_type"] = $all_qual_old_type;

				foreach($all_qual_old_type as $type)
				{
					$DB->SetTable("nsau_spec_type");
					$DB->AddCondFS("type", "=", $type['old_type']['old_type']);
//					$DB->AddCondFS("has_vacancies", "=", 1); //1 - ����� �������
					$bach = $DB->Select();

					while ($res_bach = $DB->FetchAssoc($bach))
					{
						if ($this->get_fuc($res_bach['spec_id'], 1) == $parts2[1])
						{
							if ($this->check_file($res_bach['spec_id'])==1)
							{
								/*� ��������-----------------------------------------------------------------*/
							    $DB->SetTable("nsau_profiles");
								$DB->AddCondFS("spec_id", "=", $res_bach['spec_id']);
								$prof = $DB->Select();
								while ($res_prof = $DB->FetchAssoc($prof))
								{
									$DB->SetTable("nsau_docs");
									$DB->AddCondFS("speciality_id", "=", $res_bach['spec_id']);



									//$DB->AddCondFS("form", "=", 1);





								    $docs = $DB->Select();
								    while ($res_docs = $DB->FetchAssoc($docs))
									{
										if ($res_bach['spec_id']==$res_docs['speciality_id'])
										{
											$bach_files[]=array
											(
												"file_id"=>$res_docs['file_id'],
												"doc_type_id"=>$res_docs['doc_type_id'],
												"year"=>$res_docs['year'],
												"form"=>$res_docs['form'],
												"profile"=>$res_docs['profile_id'],
											);

											$form=$res_docs['form'];

											//���������� ���� � �������� �� �� �������� ���� ������ � nsau_docs
											if ($res_docs['profile_id'] == $res_prof['id'])
											{
												$year=$res_docs['year'];
											}
										}

										/*������������ ����������� ����� ��������������� ���������-----------------------------*/
										$name="<strong itemprop='EduCode'>".$res_bach['code']."</strong> ".$this->name_on_code($res_bach['code'])."</a>";

										$name .= ", ������� <strong> ".$res_prof['name']."</strong>";

										switch ($res_docs['form'])
										{
											case '1':
												$name .= ", ����� ��������: �����";
												break;

											case '2':
												$name .= ", ����� ��������: �������";
												break;

											case '3':
												$name .= ", ����� ��������: ����-�������";
												break;
										}

										if ($year)
										{
											$name .= ", ��� ������: ".$year;
										}
										/*--------------------------------------------------------------------------------------*/

										/*������ ������*/
										$bach_data[$name]=array
										(
											"spec_id"=>$res_bach['spec_id'],
											"code"=>$res_bach['code'],
											"year"=>$year,
											"form"=>$form,
											"profile"=>$res_prof['id'],
											"facultet"=>$this->get_fuc($res_bach['spec_id'], 0),
											"hide_accredit"=>$this->get_fuc($res_bach['spec_id'], 2),
											"files"=>$bach_files,
										);
									}
									unset($year);
									unset($form);
									unset($bach_files);
								}
								/*---------------------------------------------------------------------------*/
								/*��� �������----------------------------------------------------------------*/
								$DB->SetTable("nsau_spec_type");
								$DB->AddCondFS("spec_id", "=", $res_bach['spec_id']);
								$prof = $DB->Select();
								while ($res_prof = $DB->FetchAssoc($prof))
								{

									$DB->SetTable("nsau_docs");
									$DB->AddCondFS("speciality_id", "=", $res_bach['spec_id']);







									//$DB->AddCondFS("form", "=", 1);







								    $docs = $DB->Select();
								    while ($res_docs = $DB->FetchAssoc($docs))
									{
											$bach_files[]=array
											(
												"file_id"=>$res_docs['file_id'],
												"doc_type_id"=>$res_docs['doc_type_id'],
												"year"=>$res_docs['year'],
												"form"=>$res_docs['form'],
												"profile"=>$res_docs['profile_id']
											);

											//���������� ���� � �������� �� �� �������� ���� ������ � nsau_docs
											if (!$res_docs['profile_id'])
											{
												$year_np=$res_docs['year'];
											}
											$form_np=$res_docs['form'];

											/*������������ ����������� ����� ��������������� ���������-----------------------------*/
											$name="<strong itemprop='EduCode'>".$res_bach['code']."</strong> ".$this->name_on_code($res_bach['code'])."</a>";

											if ($year_np)
											{
												switch ($res_docs['form'])
												{
													case '1':
														$name .= ", ����� ��������: �����";
														break;

													case '2':
														$name .= ", ����� ��������: �������";
														break;

													case '3':
														$name .= ", ����� ��������: ����-�������";
														break;
												}
											}

											if ($year_np)
											{
												$name .= ", ��� ������: ".$year_np;
											}
											/*--------------------------------------------------------------------------------------*/
											/*������ ������*/
											$bach_data[$name]=array
											(
												"spec_id"=>$res_bach['spec_id'],
												"code"=>$res_bach['code'],
												"year"=>$year_np,
												"form"=>$form_np,
												"profile"=>0,
												"facultet"=>$this->get_fuc($res_bach['spec_id'], 0),
												"hide_accredit"=>$this->get_fuc($res_bach['spec_id'], 2),
												"files"=>$bach_files,
											);
									}
									unset($year_np);
									unset($form_np);
									unset($bach_files);
								}
							}
							else
							{
								/*������������ ����������� ����� ��������������� ���������-----------------------------*/
								$name=$res_bach['code']." <b>".$this->name_on_code($res_bach['code'])."</a></b>";
								/*--------------------------------------------------------------------------------------*/
								/*������ ������*/
								$bach_data[$name]=array
								(
									"spec_id"=>$res_bach['spec_id'],
									"code"=>$res_bach['code'],
									"profile"=>0,
									"facultet"=>$this->get_fuc($res_bach['spec_id'], 0),
									"hide_accredit"=>$this->get_fuc($res_bach['spec_id'], 2),
									"form"=>0,
									"year"=>0
								);
								unset($name);
							}
						}
						/*����*/
						elseif ( 
							($parts2[1]==10 && $res_bach['type']=="bachelor" && $this->get_fuc($res_bach['spec_id'], 1) != 7 && $this->get_fuc($res_bach['spec_id'], 1) != 9)	|| ($parts2[1]==10 && $res_bach['type']=="higher" && $this->get_fuc($res_bach['spec_id'], 1) == 2)
						) 
						{
							/*� ��������-----------------------------------------------------------------*/
						    $DB->SetTable("nsau_profiles");
							$DB->AddCondFS("spec_id", "=", $res_bach['spec_id']);
							$prof = $DB->Select();
							while ($res_prof = $DB->FetchAssoc($prof))
							{
								$DB->SetTable("nsau_docs");
								$DB->AddCondFS("speciality_id", "=", $res_bach['spec_id']);
								$DB->AddCondFS("form", "=", 2);
							    $docs = $DB->Select();
							    while ($res_docs = $DB->FetchAssoc($docs))
								{
									if ($res_bach['spec_id']==$res_docs['speciality_id'])
									{
										$bach_files[]=array
										(
											"file_id"=>$res_docs['file_id'],
											"doc_type_id"=>$res_docs['doc_type_id'],
											"year"=>$res_docs['year'],
											"form"=>$res_docs['form'],
											"profile"=>$res_docs['profile_id'],
											"facultet_name"=>$this->get_fuc($res_bach['spec_id'], 3)
										);

										$form=$res_docs['form'];

										//���������� ���� � �������� �� �� �������� ���� ������ � nsau_docs
										if ($res_docs['profile_id'] == $res_prof['id'])
										{
											$year=$res_docs['year'];
										}
									}

									/*������������ ����������� ����� ��������������� ���������-----------------------------*/
									$name="<strong itemprop='EduCode'>".$res_bach['code']."</strong> ".$this->name_on_code($res_bach['code'])."</a>";

									$name .= ", ������� <strong> ".$res_prof['name']."</strong>";

									switch ($res_docs['form'])
									{
										case '1':
											$name .= ", ����� ��������: �����";
											break;

										case '2':
											$name .= ", ����� ��������: �������";
											break;

										case '3':
											$name .= ", ����� ��������: ����-�������";
											break;
									}

									if ($year)
									{
										$name .= ", ��� ������: ".$year;
									}
									/*--------------------------------------------------------------------------------------*/

									/*������ ������*/
									$bach_data[$name]=array
									(
										"spec_id"=>$res_bach['spec_id'],
										"code"=>$res_bach['code'],
										"year"=>$year,
										"form"=>$form,
										"profile"=>$res_prof['id'],
										"facultet"=>$this->get_fuc($res_bach['spec_id'], 0),
										"hide_accredit"=>$this->get_fuc($res_bach['spec_id'], 2),
										"files"=>$bach_files,
										'facultet_name'=>$this->get_fuc($res_bach['spec_id'], 3)

									);
								}
								unset($year);
								unset($form);
								unset($bach_files);

								//usort($bach_data, $this->build_sorter('facultet_name'));
							}
							/*---------------------------------------------------------------------------*/
							/*��� �������----------------------------------------------------------------*/
							$DB->SetTable("nsau_spec_type");
							$DB->AddCondFS("spec_id", "=", $res_bach['spec_id']);
							$prof = $DB->Select();
							while ($res_prof = $DB->FetchAssoc($prof))
							{

								$DB->SetTable("nsau_docs");
								$DB->AddCondFS("speciality_id", "=", $res_bach['spec_id']);
								$DB->AddCondFS("form", "=", 2);
							    $docs = $DB->Select();
							    while ($res_docs = $DB->FetchAssoc($docs))
								{
										$bach_files[]=array
										(
											"file_id"=>$res_docs['file_id'],
											"doc_type_id"=>$res_docs['doc_type_id'],
											"year"=>$res_docs['year'],
											"form"=>$res_docs['form'],
											"profile"=>$res_docs['profile_id'],
											"facultet_name"=>$this->get_fuc($res_bach['spec_id'], 3)
										);

										//���������� ���� � �������� �� �� �������� ���� ������ � nsau_docs
										if (!$res_docs['profile_id'])
										{
											$year_np=$res_docs['year'];
										}
										$form_np=$res_docs['form'];

										/*������������ ����������� ����� ��������������� ���������-----------------------------*/
										$name="<strong itemprop='EduCode'>".$res_bach['code']."</strong> ".$this->name_on_code($res_bach['code'])."</a>";

										if ($year_np)
										{
											switch ($res_docs['form'])
											{
												case '1':
													$name .= ", ����� ��������: �����";
													break;

												case '2':
													$name .= ", ����� ��������: �������";
													break;

												case '3':
													$name .= ", ����� ��������: ����-�������";
													break;
											}
										}

										if ($year_np)
										{
											$name .= ", ��� ������: ".$year_np;
										}
										/*--------------------------------------------------------------------------------------*/
										/*������ ������*/
										$bach_data[$name]=array
										(
											"spec_id"=>$res_bach['spec_id'],
											"code"=>$res_bach['code'],
											"year"=>$year_np,
											"form"=>$form_np,
											"profile"=>0,
											"facultet"=>$this->get_fuc($res_bach['spec_id'], 0),
											"hide_accredit"=>$this->get_fuc($res_bach['spec_id'], 2),
											"files"=>$bach_files,
											"facultet_name"=>$this->get_fuc($res_bach['spec_id'], 3)
										);
								}
								unset($year_np);
								unset($form_np);
								unset($bach_files);

								//usort($bach_data, $this->build_sorter('facultet_name'));
							}
						}
						/*----------------------------------------------------------------------------*/
					}
					$this->output[$type['old_type']['old_type']] = $bach_data;
					unset($bach_data);
				}
			}
		    break;

		    case 'import_panel':
		    {
		    	$this->output["mode"] = $this->output["scripts_mode"] = "import_panel";
		   	//  if(isset($_POST['import']))
    		// 	{
    		// 		$DB->SetTable("nsau_spec_files");
					// $old_files = $DB->Select();
					// while ($res_old_files = $DB->FetchAssoc($old_files))
					// {
					// 	$DB->SetTable("nsau_docs");
					// 	$DB->AddValue("speciality_id", $res_old_files['spec_id']);
					// 	$DB->AddValue("qualification_id", $res_old_files['qual_id']);

					// 	switch ($res_old_files['file_type'])
					// 	{
					// 		case '������� ����':
					// 			$DB->AddValue("doc_type_id", 2);
					// 		break;

					// 		case '������ �������� ��������':
					// 			$DB->AddValue("doc_type_id", 3);
					// 		break;

					// 		case '����':
					// 			$DB->AddValue("doc_type_id", 1);
					// 		break;
					// 	}
					// 	$DB->AddValue("file_id", $res_old_files['file_id']);
					// 	$DB->AddValue("form", 1);
					// 	$DB->Insert();
					// }
    		// 		//$this->output['import_message'] = "ok";
    		//	}
		    }
		    break;
		}
	}

	function build_sorter($key) 
	{
	    return function ($a, $b) use ($key) 
	    {
	        return strnatcmp($a[$key], $b[$key]);
	    };
	}

	function check_file($id)
	{
		global $DB;
		$DB->SetTable("nsau_docs");
		$DB->AddCondFS("speciality_id", "=", $id);
		$DB->AddField("file_id");
		$files = $DB->FetchAssoc($DB->Select(1));
		if ($files['file_id'])
		{
			$result=1;
		}
		else
		{
			$result=0;
		}
		//echo $id."=".$result."<br>";
		return $result;
	}


	function get_fuc($id, $mode)
	{
		/*mode
		0 - return link to methodic docs on facultet
		1 - return id facultet*/
		global $DB;
		$DB->SetTable("nsau_specialities");
		$DB->AddCondFS("id", "=", $id);
		$DB->AddField("id_faculty");
		$DB->AddField("hide_accredit");
		$facultet = $DB->FetchAssoc($DB->Select(1));

		switch ($facultet['id_faculty'])
		{
			/*��������������*/
			case '1':
				$result='/agro/library/?list_type=spec';
				break;
			/*������*/
			case '2':
				$result='/vetfac/lib/?list_type=spec';
				break;
			/*������� ���������������*/
			case '3':
				$result='/biotech/library/?list_type=spec';
				break;

			/*���������� ��������*/
			case '4':
				$result='/mechfac/lib/?list_type=spec';
				break;

			/*������*/
			case '5':
				$result='/econ/library/?list_type=spec';
				break;
			
			/*���������������� � �������. ����������*/
			case '6':
				$result='/smm/library/?list_type=spec';
				break;

			/*�����*/
			case '7':
				$result='/law/library/?list_type=spec';
				break;
		}

		// if ($mode == 0)
		// {
		// 	return $result;
		// }
		// elseif ($mode ==1)
		// {
		// 	return $facultet['id_faculty'];
		// }
		// if ($mode==2) 
		// {
		// 	return $facultet['hide_accredit'];
		// }

		switch ($mode) 
		{
			case 1:
			return $facultet['id_faculty'];
			break;
			
			case 2:
			return $facultet['hide_accredit'];
			break;

			case 3:
			{
				$DB->SetTable("nsau_faculties");
				$DB->AddCondFS("id", "=", $facultet['id_faculty']);
				$facultet = $DB->FetchAssoc($DB->Select(1));
				return $facultet['name'];
			}
			break;

			case 0:
			return $result;
			break;
		}
	}

	function name_on_code($code)
	{
		global $DB;
		$DB->SetTable("nsau_specialities");
		$DB->AddCondFS("code", "=", $code);
		$DB->AddField("name");
		$code_name = $DB->FetchAssoc($DB->Select(1));
		$result = $code_name["name"];
		return $result;
	}

	function uploadDocs($id, $validate_form) {

		global $DB, $Engine, $Auth;
		if(!empty($this->uri_params[1]) && !empty($this->uri_params[2])) {
			switch($this->uri_params[1]) {
				case "edit":

					$this->output["mode"] = $this->output["scripts_mode"] = "edit";
					$this->formBack($this->loadDocs($id, intval($this->uri_params[2])));
					$this->output["info"]["files"][$this->output["form_back"]["file_id"]] = $this->output["form_back"]["file_name"];
					$url = explode( "/", $_SERVER['REQUEST_URI']);
					if(!empty($this->post_data) && $validate_form) 
					{
						if ($url[count($url)-2] != "edit") 
						{
							$this->updateDoc($this->post_data);
							if(!$this->error) {
								CF::Redirect("../");
							}
						}
						elseif (isset($_POST["update_doc"]))
						{
							$this->updateDoc($this->post_data);
							if(!$this->error) {
								CF::Redirect("../");
							}
						}
						elseif (isset($_POST["copy_doc"])) 
						{
							$this->copyDoc($this->post_data);
						}
					}
					return 1;
				break;
			}

		}
		
		$this->output["mode"] = $this->output["scripts_mode"] = "upload";
		//isert
		if($validate_form && !empty($this->post_data)) {
			$this->insertDoc($this->post_data);
		}
		else {
			$this->formBack($this->post_data);
		}
		$this->output["docs"] = $this->loadDocs($id);
	}

	function insertDoc($data) {
		global $DB, $Engine;
		//$fields = array("profile_id", "qualification_id", "doc_type_id", "year", "form");//"speciality_id", 
		//$input_fields = array("profile_id", "qualification_id", "doc_type_id", "file_id", "year", "form");

		$fields = array("profile_id", "qualification_id", "doc_type_id", "year");//"speciality_id", 
		$input_fields = array("profile_id", "qualification_id", "doc_type_id", "file_id", "year");

		//����
		if ($data['form_1']==1) 
		{
			$DB->SetTable("nsau_docs");
			$DB->AddCondFS("speciality_id", "=", intval($this->uri_params[0]));
			foreach($fields as $key) {
				if(!empty($data[$key])) {
					$DB->AddCondFS($key, "=", $data[$key]);
				} else {
					$DB->AddCondFX($key, "IS NULL");
				}
			}
			$DB->AddCondFS("form", "=", 1);
			$row = $DB->FetchAssoc($DB->Select(1));


			if(!empty($row) && $row['doc_type_id'] != 6) //&& $row['doc_type_id'] != 6 - �.� ����� ���� ��������� ���������� ��� �������
			{

				$this->moduleMsg("bad", 302);
				$this->formBack($this->post_data);
			} 
			else {
				$DB->SetTable("nsau_docs");
				$DB->AddValue("speciality_id", intval($this->uri_params[0]));
				foreach($input_fields as $key) {
					$DB->AddValue($key, $data[$key] ? $data[$key] : NULL);
				}
				$DB->AddValue("form", 1);
				$DB->Insert();
				$Engine->LogAction($this->module_id, "doc", $DB->LastInsertID(), "insert");
				$this->moduleMsg("good", 201);
			}
		}

		//������
		if ($data['form_2']==1) 
		{
			$DB->SetTable("nsau_docs");
			$DB->AddCondFS("speciality_id", "=", intval($this->uri_params[0]));
			foreach($fields as $key) {
				if(!empty($data[$key])) {
					$DB->AddCondFS($key, "=", $data[$key]);
				} else {
					$DB->AddCondFX($key, "IS NULL");
				}
			}
			$DB->AddCondFS("form", "=", 2);
			$row = $DB->FetchAssoc($DB->Select(1));
			if(!empty($row) && $row['doc_type_id'] != 6) {
				$this->moduleMsg("bad", 302);
				$this->formBack($this->post_data);
			} 
			else {
				$DB->SetTable("nsau_docs");
				$DB->AddValue("speciality_id", intval($this->uri_params[0]));
				foreach($input_fields as $key) {
					$DB->AddValue($key, $data[$key] ? $data[$key] : NULL);
				}
				$DB->AddValue("form", 2);
				$DB->Insert();
				$Engine->LogAction($this->module_id, "doc", $DB->LastInsertID(), "insert");
				$this->moduleMsg("good", 201);
			}
		}

		//����-������
		if ($data['form_3']==1) 
		{
			$DB->SetTable("nsau_docs");
			$DB->AddCondFS("speciality_id", "=", intval($this->uri_params[0]));
			foreach($fields as $key) {
				if(!empty($data[$key])) {
					$DB->AddCondFS($key, "=", $data[$key]);
				} else {
					$DB->AddCondFX($key, "IS NULL");
				}
			}
			$DB->AddCondFS("form", "=", 3);
			$row = $DB->FetchAssoc($DB->Select(1));
			if(!empty($row) && $row['doc_type_id'] != 6) {
				$this->moduleMsg("bad", 302);
				$this->formBack($this->post_data);
			} 
			else {
				$DB->SetTable("nsau_docs");
				$DB->AddValue("speciality_id", intval($this->uri_params[0]));
				foreach($input_fields as $key) {
					$DB->AddValue($key, $data[$key] ? $data[$key] : NULL);
				}
				$DB->AddValue("form", 3);
				$DB->Insert();
				$Engine->LogAction($this->module_id, "doc", $DB->LastInsertID(), "insert");
				$this->moduleMsg("good", 201);
			}
		}

		//������-��������
		// if ($data['form_4']==1) 
		// {
		// 	$DB->SetTable("nsau_docs");
		// 	$DB->AddCondFS("speciality_id", "=", intval($this->uri_params[0]));
		// 	foreach($fields as $key) {
		// 		if(!empty($data[$key])) {
		// 			$DB->AddCondFS($key, "=", $data[$key]);
		// 		} else {
		// 			$DB->AddCondFX($key, "IS NULL");
		// 		}
		// 	}
		// 	$DB->AddCondFS("form", "=", 4);
		// 	$row = $DB->FetchAssoc($DB->Select(1));
		// 	if(!empty($row) && $row['doc_type_id'] != 6) {
		// 		$this->moduleMsg("bad", 302);
		// 		$this->formBack($this->post_data);
		// 	} 
		// 	else {
		// 		$DB->SetTable("nsau_docs");
		// 		$DB->AddValue("speciality_id", intval($this->uri_params[0]));
		// 		foreach($input_fields as $key) {
		// 			$DB->AddValue($key, $data[$key] ? $data[$key] : NULL);
		// 		}
		// 		$DB->AddValue("form", 4);
		// 		$DB->Insert();
		// 		$Engine->LogAction($this->module_id, "doc", $DB->LastInsertID(), "insert");
		// 		$this->moduleMsg("good", 201);
		// 	}
		// }
	}

	function updateDoc($data) 
	{
		global $DB, $Engine;
		$fields = array("profile_id", "qualification_id", "doc_type_id", "file_id", "year", "form");//"speciality_id", 
		$input_fields = array("profile_id", "qualification_id", "doc_type_id", "file_id", "year", "form");//"speciality_id", 
		$DB->SetTable("nsau_docs");
		$DB->AddCondFS("speciality_id", "=", intval($this->uri_params[0]));
		foreach($fields as $key) {
			if(!empty($data[$key])) {
				$DB->AddCondFS($key, "=", $data[$key]);
			} else {
				$DB->AddCondFX($key, "IS NULL");
			}
		}
		$row = $DB->FetchAssoc($DB->Select(1));
		if(!empty($row) && ($row["id"]!=$data["id"])) {
			$this->moduleMsg("bad", 302);
			$this->formBack($this->post_data);

		} 
		else {
			$DB->SetTable("nsau_docs");
			$DB->AddCondFS("id", "=", $data["id"]);
			foreach($input_fields as $key) {
				$DB->AddValue($key, $data[$key] ? $data[$key] : NULL);
			}
			$DB->Update();
			$Engine->LogAction($this->module_id, "doc", $this->post_data["id"], "edit");
		}
	}

	function copyDoc($data) 
	{
		global $DB, $Engine;
		$fields = array("profile_id", "qualification_id", "doc_type_id", "file_id", "year", "form");//"speciality_id", 
		$input_fields = array("profile_id", "qualification_id", "doc_type_id", "file_id", "year", "form");//"speciality_id", 

		$DB->SetTable("nsau_docs");
		$DB->AddValue("speciality_id", intval($this->uri_params[0]));
		foreach($input_fields as $key) {
			$DB->AddValue($key, $data[$key] ? $data[$key] : NULL);
		}
		if($DB->Insert())
		{
			$this->output["copy_result"] = 1;
			$Engine->LogAction($this->module_id, "doc", $this->post_data["id"], "copy");
		}
		else
		{
			$this->output["copy_result"] = 2;
		}
	}
	
	function deleteDoc($id) {
			global $DB, $Engine;
		$DB->SetTable("nsau_docs");
		$DB->AddCondFS("id", "=", $id);
		$DB->Delete();
		$Engine->LogAction($this->module_id, "doc", $id, "delete");
	}
	
	function loadDocs($id, $doc_id) {
		global $DB;
		$DB->SetTable("nsau_docs");
		$DB->AddCondFS("speciality_id", "=", $id);
		$DB->AddOrder("form");
		if(!empty($doc_id)) {
			$DB->AddCondFS("id", "=", $doc_id);
		}
		$res = $DB->Select();
		while($row = $DB->FetchAssoc($res)) {
			$DB->SetTable("nsau_files");
			$DB->AddCondFS("id", "=", $row["file_id"]);
			$DB->AddField("name");
			$file = $DB->FetchAssoc($DB->Select(1));
			$row["file_name"] = $file["name"];
			if(!empty($doc_id)) {
				return $row;
			}
			$result[$row["profile_id"] ? $row["profile_id"] : 0][] = $row;
		}
		ksort($result);
		return $result;
	}

	
	
	function validateForm($form_data) {
		foreach($form_data as $key => $val) {

			      
			
			$this->post_data[$key] = $_POST[$key];
			if(in_array($key, $this->obligatory_fields)) {
				if(empty($val)) {
					$this->moduleMsg("bad", 301, $this->obligatory_fields_name[$key]);
				}
				elseif (!$form_data['form_1'] && !$form_data['form_2'] && !$form_data['form_3'] && !$form_data['form_4'] && !$form_data['form'])
				{
					$form_error++;
				}
			}
		}
		if ($form_error>0) 
		{
			$this->moduleMsg("bad", 303, "");
		}
		return !$this->error;
	}
	
	function addictionalData($id) {
		global $DB, $Auth;
		//files
		$DB->SetTable("nsau_files");
		$DB->AddCondFS("user_id", "=", $Auth->user_id);
		$DB->AddCondFS("deleted", "=", 0);
		$res = $DB->Select();
		while($file = $DB->FetchAssoc($res)) {
			$result["files"][$file["id"]] = $file["name"];
		}
		//profiles
		$DB->SetTable("nsau_profiles");
		$DB->AddCondFS("spec_id", "=", $id);
		$res = $DB->Select();
		while($profile = $DB->FetchAssoc($res)) {
			$result["profiles"][$profile["id"]] = $profile["name"];
		}
		//qualifications
		$DB->SetTable("nsau_qualifications");
		$res = $DB->Select();
		while($qualification = $DB->FetchAssoc($res)) {
			$result["qualifications"][$qualification["id"]] = $qualification["name"];
		}
		//doc_types
		$DB->SetTable("nsau_docs_types");
		$DB->AddOrder("pos");
		$res = $DB->Select();
		while($type = $DB->FetchAssoc($res)) {
			$result["types"][$type["id"]] = $type["name"];
		}
		//doc_types
		$DB->SetTable("nsau_specialities");
		$DB->AddCondFS("id", "=", $id);
		$res = $DB->Select();
		while($type = $DB->FetchAssoc($res)) {
			$result["speciality"] = array("name"=>$type["name"], "code"=>$type["code"]);
		}
		
		return $result;
	}
	
	function formBack($form_data) {		
		$this->output["form_back"] = $form_data;
	}
	
	function moduleMsg($type, $code, $data = null) {
		$this->error = ($type=="bad") ? true : false;
		$this->output["messages"][$type][] = array($code=>$data);
	}
	
	function Output()	{
			return $this->output;
	}	
}
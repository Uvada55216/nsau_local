<?php
require_once INCLUDES . "/effect/" . "Formula" . CLASS_EXT ;
require_once INCLUDES . "/effect/" . "Contract" . CLASS_EXT ;
require_once INCLUDES . "/effect/" . "Reports" . CLASS_EXT ;
class EEffect
{
	var $module_id;
	var $node_id;
	var $module_uri;
	var $privileges;
	var $output;
	var $settings;
	
	var $mode;
	var $department_id;
	var $db_prefix;
	
	var $contract_id;
	var $report_type;
	var $Contract;
	var $Reports;
	
	var $indicator_progress;
	/*
	� ������ ������� ����� ������ faculties, exi
people, people, search_group, groups ���� timetable � ��� � ��� =)
	*/
	function EEffect($global_params, $params, $module_id, $node_id = NULL, $module_uri = NULL, $additional = NULL)
	{
		global $DB, $Engine, $Auth, $EFF;
		
		$this->module_id = $module_id;
		$this->node_id = $node_id;
		$this->module_uri = $module_uri;
		$this->additional = $additional;
		$this->output = array();        
		$this->output["messages"] = array("good" => array(), "bad" => array());
		$this->output["module_id"] = $this->module_id;
		
		$parts = explode(";", $global_params);
		$this->db_prefix = $parts[0];
		$parts2 = explode(";", $params);
			
		$mode = explode("/", $module_uri);
		$this->contract_id = intval($mode[1]);
		$this->department_id = intval($mode[0]);
		$this->period_id = intval($mode[1]);
		$this->report_type = $mode[2];
		$this->settings = @parse_ini_file(INCLUDES . "EEffect.ini", true);
		$this->output["work_type"] = $this->settings["work_type"];
		$this->output["pps"] = $this->settings["pps"];
		
		$this->Reports = new Reports; 
		$this->Contract = new Contract($this->contract_id); 
		
		switch ($this->mode = $parts2[0]) {

			
			case "ajax_delete_indicator": {
				if($this->Contract->checkEditAccess($this->Contract->getIdByIndicator(intval($_REQUEST["data"]["id"]))))
					$this->deleteFIndicator(intval($_REQUEST["data"]["id"]));	
				else
					$this->AccessError();
			}
			break;
			case "ajax_change_rate": {
				if($this->Contract->checkEditAccess(intval($_REQUEST["data"]["id"]))) {
					$this->Contract->changeRate(intval($_REQUEST["data"]["id"]), floatval($_REQUEST["data"]["val"]));	
					$this->contractMsg();
				}
				else
					$this->AccessError();				
			}
			break;		
			case "ajax_change_pps": {
				if($this->Contract->checkEditAccess(intval($_REQUEST["data"]["id"]))) {
					$this->Contract->changePps(intval($_REQUEST["data"]["id"]), floatval($_REQUEST["data"]["val"]));
					$this->contractMsg();
				}					
				else
					$this->AccessError();				
			}
			break;	
			case "ajax_delete_indicators": {
				if($this->Contract->checkEditAccess($this->Contract->getIdByPlan(intval($_REQUEST["data"]["id"]))))
					$this->deleteFIndicators(intval($_REQUEST["data"]["id"]));
				else
					$this->AccessError();				
			}
			break;	
			
			case "ajax_show_indicator": {
				if($this->Contract->checkEditAccess($this->Contract->getIdByIndicator(intval($_REQUEST["data"]["id"])))) {
					$this->output["mode"] = $this->output["scripts_mode"] = "show";
					$this->showIndicator($_REQUEST["data"]["id"]);		
				} else
					$this->output["messages"]["bad"][] = 404;					
			}
			break;

			case "ajax_show_full_indicator": {
				if($this->Contract->checkViewAccess($this->Contract->getIdByPlan(intval($_REQUEST["data"]["id"])))) {
					$this->output["mode"] = "show_full";
					$this->showFullIndicator($_REQUEST["data"]["id"]);
				}	else 
					$this->output["messages"]["bad"][] = 402;
			}
			break;			 
			
			case "ajax_add_indicator_fields": {	
				if($this->Contract->checkEditAccess($this->Contract->getIdByPlan(intval($_REQUEST["data"]["id"])))) 
					$this->addIndicatorFields(intval($_REQUEST["data"]["id"]), $_REQUEST["data"]["text"], $_REQUEST["data"]["file"]);
				else
					$this->AccessError();	
			}
			break;

			case "ajax_edit_indicator_fields": {	
				if($this->Contract->checkEditAccess($this->Contract->getIdByIndicator(intval($_REQUEST["data"]["id"])))) 
					$this->editIndicatorFields(intval($_REQUEST["data"]["id"]), $_REQUEST["data"]["text"], $_REQUEST["data"]["file"]);
				else
					$this->AccessError();	
			}
			break;
			
			case "ajax_add_indicator": {
				if($this->Contract->checkEditAccess(intval($_REQUEST["data"]["contract_id"]))) 
					$this->addPlanIndicator(intval($_REQUEST["data"]["id"]), intval($_REQUEST["data"]["contract_id"]));	
				else
					$this->AccessError();					
			}
			break;
			
			case "ajax_add_indicator_form": {
				if($this->Contract->checkEditAccess($this->Contract->getIdByPlan(intval($_REQUEST["data"]["id"])))) 
					$this->addIndicatorForm(intval($_REQUEST["data"]["id"]));	
			}
			break;
			
			case "plan": {
				if(!empty($_REQUEST["data"]["contract_id"])) {
					$this->contract_id = intval($_REQUEST["data"]["contract_id"]);
					$this->output["mode"] = $_REQUEST["data"]["mode"]=="plan_report" ? "plan_report" : "plan_ind";
				}
				if(empty($this->contract_id)) 
					$this->output["messages"]["bad"][] = 401;
				$this->output["contract_id"] = $this->contract_id;

				$this->output["contract_info"]["complete"] = true;
				$EFF->SetTable("effect_plan", "ep");
				$EFF->AddCondFS("ep.contract_id", "=", $this->contract_id);
				$EFF->AddTable("indicators", "i");
				$EFF->AddCondFF("i.id", "=", "ep.indicator_id");
				$EFF->AddField("ep.indicator_id", "ind_id");
				$EFF->AddField("ep.id", "id");
				$EFF->AddField("i.ind_type", "ind_type");
				$res = $EFF->Select();
				while($row = $EFF->FetchAssoc($res)) {
					$this->output["plan_indicators"][$row["ind_type"]][$row["id"]]["ind"] = $row; 
					$EFF->SetTable("effect_indicators");
					$EFF->AddCondFS("plan_id", "=", $row["id"]);
					$res2 = $EFF->Select();
					while($row2 = $EFF->FetchAssoc($res2))
						$this->output["plan_indicators"][$row["ind_type"]][$row["id"]]["vals"][$row2["id"]] = $row2["text0"];
				}
				
				if(!$this->Contract->checkViewAccess($this->contract_id))
					$this->output["messages"]["bad"][] = 402;
				
				
				$EFF->SetTable("effect_contracts");
				$EFF->AddCondFS("id", "=", $this->contract_id);
				$contr = $EFF->FetchAssoc($EFF->Select(1));

				
				$this->indicator_progress = $this->checkIndicatorProgress($this->contract_id);
				if($_REQUEST["data"]["mode"]=="plan_ind") {
					$this->output["contract_info"] = $this->checkObligatoryIndicators($this->indicator_progress);
				}
				
				
				$year_id = $this->Contract->getYearId($contr["period_id"]);//8664
				
				
				if($contr["post_id"]==2) {
					$stpd_post_id = array("self"=>2, "view" => 3, "department"=>$contr["department_id"]);
				} elseif($contr["post_id"]==1) {
					$DB->SetTable("nsau_departments");
					$DB->AddCondFS("id", "=", $contr["department_id"]);
					$DB->AddField("faculty_id");
					$_dep = $DB->FetchAssoc($DB->Select(1));		
					
					$DB->SetTable("nsau_departments");
					$DB->AddCondFS("faculty_id", "=", $_dep["faculty_id"]);
					$DB->AddCondFS("is_active", "=", 1);
					$DB->AddField("id");
					$_res = $DB->Select();
					while($depsr = $DB->FetchAssoc($_res)) {
						$de[] = $depsr["id"];
					}
					$deps = implode(",", $de);				
					$stpd_post_id = array("self"=>1, "view" => 2, "department"=>$deps);
				}
				
				
				$EFF->SetTable("effect_contracts", "ec");
				$EFF->AddCondFX("ec.department_id", "IN(".$stpd_post_id["department"].")");
				// $EFF->AddCondFS("ec.department_id", "=", $contr["department_id"]);
				$EFF->AddCondFS("ec.id", "!=", $contr["id"]);
				if($contr["post_id"]==2) {
					$EFF->AddAltFS("ec.post_id", "LIKE", "%3%");
					$EFF->AddAltFS("ec.post_id", "LIKE", "%4%");
					$EFF->AddAltFS("ec.post_id", "LIKE", "%5%");
					$EFF->AddAltFS("ec.post_id", "LIKE", "%6%");
					$EFF->AppendAlts();
				} 
				elseif($contr["post_id"]==1) {
					$EFF->AddAltFS("ec.post_id", "LIKE", "%2%");
					$EFF->AppendAlts();
				}
				$EFF->AddTable("effect_periods", "epe");
				$EFF->AddCondFS("epe.year_id", "=", $year_id);
				$EFF->AddCondFF("epe.id", "=", "ec.period_id");
				
				$EFF->AddField("ec.id", "c_id");
				$EFF->AddField("ec.user_id", "u_id");
				$EFF->AddField("epe.year_id", "y_id");
				$cr = $EFF->Select();
				while($crow = $EFF->FetchAssoc($cr)) {
					$DB->SetTable("auth_users");
					$DB->AddCondFS("id", "=", $crow["u_id"]);
					$DB->AddField("displayed_name");
					$u_r = $DB->FetchAssoc($DB->Select(1));
					$d_people[$crow["u_id"]]["c_id"][$crow["c_id"]] = $crow["c_id"];
					$d_people[$crow["u_id"]]["name"] = $u_r["displayed_name"];
				}
				
				 // CF::Debug($d_people);
				$EFF->SetTable("indicators");
				$EFF->AddAltFS("post_id", "LIKE", "%".$stpd_post_id["self"]."%");
				$EFF->AddField("name");
				$EFF->AddField("id");
				$EFF->AddField("ind_type");
				$EFF->AddField("post_id");
				$EFF->AddField("auto");
				$EFF->AddField("deny_form");
				
				$res = $EFF->Select();
				while($row = $EFF->FetchAssoc($res)) {
					
					if(!empty($row["deny_form"])) {
						unset($df);
						$deny_form = explode(";", $row["deny_form"]);
						foreach($deny_form as $iii=>$df_val) {
							$df[$df_val] = 1;
						}
						$row["deny_form"] = $df[$contr["post_id"]];
					} else $row["deny_form"] = 0;
					
					$posts = explode(";", $row["post_id"]);
					if(in_array($contr["post_id"], $posts)) {
						$row["complete"] = $this->indicator_progress[$row["id"]]["complete"];
						$row["cond"] = $this->indicator_progress[$row["id"]]["cond"];
						$row["val"] = $this->indicator_progress[$row["id"]]["val"];
						if($row["ind_type"]==1) {
							if($contr["post_id"]==$stpd_post_id["self"]) {
								$psts = explode(";", $row["post_id"]);
								if(in_array($stpd_post_id["view"], $psts)){
									
								
									foreach($d_people as $u_id => $c_id) {
										if(!$this->isDecree($u_id))	{
											foreach($c_id["c_id"] as $con_id => $cc) {
												$p = $this->checkIndicatorProgress($con_id, $row["id"], $year_id);
												$row["personal"][$u_id]["val"] += $p[$row["id"]]["val"];
												$row["personal"][$u_id]["cond"] = !empty($p[$row["id"]]["cond"]) ? $p[$row["id"]]["cond"] : $row["personal"][$u_id]["cond"];
												$row["personal"][$u_id]["name"] = $c_id["name"];
												$row["personal"][$u_id]["complete"] += $p[$row["id"]]["complete"];
											}
										}
									}
								}
							}
						}						
						$this->output["indicators"][$row["id"]] = $row; 
					}
				}



			
				$EFF->SetTable("effect_contracts");
				$EFF->AddCondFS("id", "=", $this->contract_id);
				$contract = $EFF->FetchAssoc($EFF->Select(1));
				$user_id = $contract["user_id"];
				$DB->SetTable("auth_users");
				$DB->AddCondFS("id", "=", $contract["user_id"]);
				$user  = $DB->FetchAssoc($DB->Select());
				$DB->SetTable("nsau_departments");
				$DB->AddCondFS("id", "=", $contract["department_id"]);
				$department  = $DB->FetchAssoc($DB->Select());
				$EFF->SetTable("posts");
				$EFF->AddCondFS("id", "=", $contract["post_id"]);
				$post  = $EFF->FetchAssoc($EFF->Select());
				
				$this->output["pay_info"]["username"] = $user["displayed_name"];
				$this->output["pay_info"]["rate"] = $contract["rate"];
				$this->output["pay_info"]["pps"] = $contract["pps"];
				$this->output["pay_info"]["department"] = $department["name"];
				$this->output["pay_info"]["post"] = $post["name"];
					
				//stimulation report	
				if($_REQUEST["data"]["mode"]=="plan_report") {
					
					require_once INCLUDES . "/effect/reports/" . "PeopleStimulationReport" . CLASS_EXT ;
					$PeopleStimulationReport = new PeopleStimulationReport();
					$reports = $PeopleStimulationReport->createReportByPeople($this->contract_id);
					
					if(!empty($reports)) {
						$this->output += $reports;
					}
				}
				
				
			}
			break;
			case "contracts": {
				if(!empty($_GET["user_id"]) && (($Auth->usergroup_id!=1) || !$Engine->OperationAllowed(34, "contracts.handle", $Auth->user_id, $Auth->usergroup_id))) {
					$_GET["user_id"] = intval($_GET["user_id"]);
					
					$DB->SetTable("nsau_people", "np");
					$DB->AddCondFS("np.user_id", "=", $Auth->user_id);
					$DB->AddTable("nsau_teachers", "nt");
					$DB->AddCondFF("np.id", "=", "nt.people_id");
					$DB->AddField("nt.department_id", "d_id");
					$DB->AddField("np.status_id", "status_id");
					$this_teacher = $DB->FetchAssoc($DB->Select(1));
					
					$this_teacher_deps = explode(";", $this_teacher["d_id"]);

					$DB->SetTable("nsau_people", "np");
					$DB->AddCondFS("np.user_id", "=", $_GET["user_id"]);
					$DB->AddTable("nsau_teachers", "nt");
					$DB->AddCondFF("np.id", "=", "nt.people_id");
					$DB->AddField("nt.department_id", "d_id");
					$DB->AddField("np.status_id", "status_id");
					$d_r = $DB->FetchAssoc($DB->Select(1));
					if(!empty($d_r["d_id"]) && ($d_r["status_id"]==9)) {
						$d = explode(";", $d_r["d_id"]);
						foreach($d as $val) {
							$dismissed_handle += $Engine->OperationAllowed(34, "contracts.dismissed.handle", $val, $Auth->usergroup_id);
							if($Engine->OperationAllowed(34, "contracts.dismissed.owndep.handle", -1, $Auth->usergroup_id) && (in_array($val, $this_teacher_deps))) {
								$dismissed_handle = 1;
							}
						}
					}
					
				}
				
				if((($Auth->usergroup_id==1) || $Engine->OperationAllowed(34, "contracts.handle", $Auth->user_id, $Auth->usergroup_id) || $dismissed_handle) && !empty($_GET["user_id"]))
					$user_id = intval($_GET["user_id"]);
				else $user_id = $Auth->user_id;
				if(empty($this->module_uri)) {					
					$this->output["mode"] = $this->output["scripts_mode"] = "teacher_indicators";
					
					$this->output["departments"] = $this->loadTeacherDepartments($user_id);
					if(empty($this->output["departments"])) {
						$this->output["departments"] = $this->loadAllDepartments();
					}
					
					$this->output["dates"]["fact_to"] = $this->Contract->getFactToDate($this->Contract->getPeriodId());
					$this->output["dates"]["from"] = $this->Contract->getFromDate($this->Contract->getPeriodId());
					$this->output["dates"]["to"] = $this->Contract->getToDate($this->Contract->getPeriodId());
					
					$this->output["contracts"] = $this->Contract->loadContracts($user_id);
					$this->loadPosts();
					if(!empty($_POST)) {
						if(!empty($_POST["department_id"]) && !empty($_POST["post_id"]) && !empty($_POST["rate"]) && !empty($_POST["pps"])) {
							if($this->Contract->addContract($_POST, $user_id)) 
								CF::Redirect($Engine->module_uri);
							else { $this->output["form_back"] = $_POST;	$this->contractMsg();}
						 } else {
							 $this->output["messages"]["bad"][] = 101;
							 $this->output["form_back"] = $_POST;
						 }
					} else {
						$this->output["form_back"] = $_POST;
					}						
				} else {					
					$parts = explode("/", $this->module_uri);
					switch($parts[0]) {
						case "post_info": {
							$this->output["mode"] = "post_info";
							$this->postIndicatorList($parts[1]);
						}
						break;
						case "delete":{
							if($this->Contract->checkEditAccess($this->contract_id))
								$this->Contract->deleteContract($this->contract_id);							
						}
						break;
						
						case "plan": {	
							
							if(empty($this->contract_id)) 
								$this->output["messages"]["bad"][] = 401;
							$this->output["mode"] = $this->output["scripts_mode"] = "plan";
							$this->output["contract_id"] = $this->contract_id;
							$this->output["contract_info"] = $this->checkObligatoryIndicators($this->contract_id);
							$EFF->SetTable("effect_plan", "ep");
							$EFF->AddCondFS("ep.contract_id", "=", $this->contract_id);
							$EFF->AddTable("indicators", "i");
							$EFF->AddCondFF("i.id", "=", "ep.indicator_id");
							$EFF->AddField("ep.indicator_id", "ind_id");
							$EFF->AddField("ep.id", "id");
							$EFF->AddField("i.ind_type", "ind_type");
							$res = $EFF->Select();
							while($row = $EFF->FetchAssoc($res)) {
								$this->output["plan_indicators"][$row["ind_type"]][$row["id"]]["ind"] = $row; 
								$EFF->SetTable("effect_indicators");
								$EFF->AddCondFS("plan_id", "=", $row["id"]);
								$res2 = $EFF->Select();
								while($row2 = $EFF->FetchAssoc($res2))
									$this->output["plan_indicators"][$row["ind_type"]][$row["id"]]["vals"][$row2["id"]] = $row2["text0"];
							}
							
							if(!$this->Contract->checkViewAccess($this->contract_id))
								$this->output["messages"]["bad"][] = 402;
							
							$this->indicator_progress = $this->checkIndicatorProgress($this->contract_id);
							
							$EFF->SetTable("effect_contracts");
							$EFF->AddCondFS("id", "=", $this->contract_id);
							$contr = $EFF->FetchAssoc($EFF->Select(1));
							$EFF->SetTable("indicators");
							$EFF->AddField("name");
							$EFF->AddField("id");
							$EFF->AddField("ind_type");
							$EFF->AddField("post_id");
							$EFF->AddField("auto");
							$EFF->AddField("deny_form");
							$res = $EFF->Select();
							while($row = $EFF->FetchAssoc($res)) {
								if(!empty($row["deny_form"])) {
									unset($df);
									$deny_form = explode(";", $row["deny_form"]);
									foreach($deny_form as $iii=>$df_val) {
										$df[$df_val] = 1;
									}
									$row["deny_form"] = $df[$contr["post_id"]];
								} else $row["deny_form"] = 0;
								$posts = explode(";", $row["post_id"]);
								$row["complete"] = $this->indicator_progress[$row["id"]]["complete"];
								if(in_array($contr["post_id"], $posts))
									$this->output["indicators"][$row["id"]] = $row; 
							}




						
							$EFF->SetTable("effect_contracts");
							$EFF->AddCondFS("id", "=", $this->contract_id);
							$contract = $EFF->FetchAssoc($EFF->Select(1));
							$user_id = $contract["user_id"];
							$DB->SetTable("auth_users");
							$DB->AddCondFS("id", "=", $contract["user_id"]);
							$user  = $DB->FetchAssoc($DB->Select());
							$DB->SetTable("nsau_departments");
							$DB->AddCondFS("id", "=", $contract["department_id"]);
							$department  = $DB->FetchAssoc($DB->Select());
							$EFF->SetTable("posts");
							$EFF->AddCondFS("id", "=", $contract["post_id"]);
							$post  = $EFF->FetchAssoc($EFF->Select());
							
							$this->output["pay_info"]["username"] = $user["displayed_name"];
							$this->output["pay_info"]["rate"] = $contract["rate"];
							$this->output["pay_info"]["pps"] = $contract["pps"];
							$this->output["pay_info"]["department"] = $department["name"];
							$this->output["pay_info"]["post"] = $post["name"];
			
						}
						break;

					}	
				}
			}
			break;

			case "indicators": {
				if(($Auth->usergroup_id==1) || $Engine->OperationAllowed(34, "indicators.handle", $Auth->user_id, $Auth->usergroup_id)) {
					if(empty($this->module_uri)) {
							$this->output["mode"] = $this->output["scripts_mode"] = "indicators";

							$this->indicatorsList();
							$this->loadPosts();
							if(!empty($_POST))
								if($this->checkForm($_POST)) {
									$this->addIndicator($_POST);
									CF::Redirect($Engine->module_uri);
								} else
									$this->output["form_back"] = $_POST;
					}
					else {					
						$parts = explode("/", $this->module_uri);
						switch($parts[0]) {
							case "edit": {
								if($this->indicatorsList($parts[1])) {
									$this->output["mode"] = "indicators_edit";
									$this->output["scripts_mode"] = "indicators";
									$this->loadPosts();
									if(!empty($_POST))
										if($this->checkForm($_POST)) {
											$this->editIndicator($parts[1], $_POST);
											CF::Redirect(str_replace($Engine->module_uri, "", $Engine->unqueried_uri));
										} else 
											$this->output["form_back"] = $_POST;
								}
								else
									$Engine->HTTP404();
							}
							break;
							case "delete": {
								$this->output["mode"]  = $this->output["scripts_mode"] = "indicators";
								$this->deleteIndicator($parts[1]);
								CF::Redirect(str_replace($Engine->module_uri, "", $Engine->unqueried_uri));
							}
							break;
							case "form": {
								$this->output["mode"] = "form";
								$e = explode("/", $this->module_uri);
								$id = intval($e[1]);
								$EFF->SetTable("indicators");
								$EFF->AddCondFS("id", "=", $id);
								$this->output["name"]  = $EFF->FetchAssoc($EFF->Select(1));
								$this->createForm($parts[1]);
							}
							break;

						}
					}
				}				
			}
			break;
			
			case "reports": {
				$this->output["mode"] = $this->output["scripts_mode"] = "reports";
				if(!empty($_POST["department_id"]) && !empty($_POST["period_id"])) {
					if($_POST["department_id"]!="all") {

						require_once INCLUDES . "/effect/reports/" . "DepartmentStimulationReport" . CLASS_EXT ;
						$DepartmentStimulationReport = new DepartmentStimulationReport();
						$reports = $DepartmentStimulationReport->createReportByDepartment(intval($_POST["department_id"]), intval($_POST["period_id"]));
						$this->output += array("stimulations"=>$reports);		
						
						require_once INCLUDES . "/effect/reports/" . "DepartmentObligatoryReport" . CLASS_EXT ;
						$DepartmentObligatoryReport = new DepartmentObligatoryReport();
						$reports = $DepartmentObligatoryReport->createReportByDepartment(intval($_POST["department_id"]), intval($_POST["period_id"]));
						$this->output += array("obligatory"=>$reports);	
						
						// require_once INCLUDES . "/effect/reports/" . "ObligatoryShortReport" . CLASS_EXT ;
						// $DepartmentObligatoryShortReport = new ObligatoryShortReport();
						// $reports = $DepartmentObligatoryShortReport->createReportByDepartment(intval($_POST["department_id"]), intval($_POST["period_id"]), 2);
						// $this->output += array("obligatory_short"=>$reports);	
						
						// require_once INCLUDES . "/effect/reports/" . "ObligatoryShortReport" . CLASS_EXT ;
						// $DepartmentObligatoryShortReport = new ObligatoryShortReport();
						// $reports = $DepartmentObligatoryShortReport->createReportByDepartment(intval($_POST["department_id"]), intval($_POST["period_id"]), 1);
						// $this->output += array("obligatory_short_fak"=>$reports);	
						
						
					} else { 
						CF::Redirect("/office/effect/reports/download/0/".intval($_POST["period_id"])."/");
					}
				}
				
				
				$full_department_report = $Engine->OperationAllowed(34, "contracts.fulldepartment.report", -1, $Auth->usergroup_id);
				if(!$full_department_report) {
					$DB->SetTable("nsau_teachers");
					$DB->AddCondFS("people_id", "=", $Auth->people_id);
					$DB->AddField("department_id");
					$t = $DB->FetchAssoc($DB->Select(1));
					
					if(!empty($t["department_id"])){ 
						$DB->SetTable("nsau_departments");
						$DB->AddCondFS("id", "=", $t["department_id"]);
						$DB->AddField("faculty_id");
						$f = $DB->FetchAssoc($DB->Select(1));
					}
				
				}
				$DB->SetTable("nsau_departments");
				$DB->AddCondFS("is_active", "=", 1);
				if(!empty($f["faculty_id"]) && ($Auth->usergroup_id!=1) && !$full_department_report) {
					$DB->AddCondFS("faculty_id", "=", $f["faculty_id"]);
				}
				$res = $DB->Select();
				while($row = $DB->FetchAssoc($res))
					$this->output["departments"][$row["id"]] = $row["name"];
		

				$this->output["periods"] = $this->Contract->getPeriods();
			
			}
			break;



			case "creports": {
				$this->output["mode"] = "creports";
				$this->output["scripts_mode"] = "reports";

						
						require_once INCLUDES . "/effect/reports/" . "ObligatoryShortReport" . CLASS_EXT ;
						$DepartmentObligatoryShortReport = new ObligatoryShortReport();
						$reports = $DepartmentObligatoryShortReport->createReportByDepartment(0, intval($_POST["period_id"]), 2);
						$this->output += array("obligatory_short"=>$reports);	
						
						require_once INCLUDES . "/effect/reports/" . "ObligatoryShortReportFaculties" . CLASS_EXT ;
						$FObligatoryShortReport = new ObligatoryShortReportFaculties();
						$reports = $FObligatoryShortReport->createReportByDepartment(0, intval($_POST["period_id"]), 1);
						$this->output += array("obligatory_short_faculty"=>$reports);	


				$DB->SetTable("nsau_departments");
				$DB->AddCondFS("is_active", "=", 1);
				$res = $DB->Select();
				while($row = $DB->FetchAssoc($res))
					$this->output["departments"][$row["id"]] = $row["name"];
		

				$this->output["periods"] = $this->Contract->getPeriods();
			
			}
			break;






			
			case "download": {
				$this->output["mode"] = $this->output["scripts_mode"] = "reports";
				$this->output["html"] = true;
				if($this->department_id===0) {
					$this->output["all"] = true;
					$DB->SetTable("nsau_departments");
					$DB->AddCondFS("is_active", "=", 1);
					$res = $DB->Select();
					require_once INCLUDES . "/effect/reports/" . "DepartmentStimulationReport" . CLASS_EXT ;
					$DepartmentStimulationReport = new DepartmentStimulationReport();
					require_once INCLUDES . "/effect/reports/" . "DepartmentObligatoryReport" . CLASS_EXT ;
					$DepartmentObligatoryReport = new DepartmentObligatoryReport();
					while($row = $DB->FetchAssoc($res)) {
						$s = $DepartmentStimulationReport->createReportByDepartment($row["id"], $this->period_id);	
						$o = $DepartmentObligatoryReport->createReportByDepartment($row["id"], $this->period_id);	
					}
					$this->output += array("obligatory" => $o);
					$this->output += array("stimulations" => $s);		
				}
				else {					
					switch($this->report_type) {
						case "s": 
							require_once INCLUDES . "/effect/reports/" . "DepartmentStimulationReport" . CLASS_EXT ;
							$DepartmentStimulationReport = new DepartmentStimulationReport();
							$reports = $DepartmentStimulationReport->createReportByDepartment($this->department_id, $this->period_id);
							$this->output += array("stimulations"=>$reports);		
						break;
						
						case "o": 
							
							require_once INCLUDES . "/effect/reports/" . "DepartmentObligatoryReport" . CLASS_EXT ;
							$DepartmentObligatoryReport = new DepartmentObligatoryReport();
							$reports = $DepartmentObligatoryReport->createReportByDepartment($this->department_id, $this->period_id);
							$this->output += array("obligatory"=>$reports);	
						break;
						
						case "os":
							$this->output["mode"] = "creports";
							$this->output["smode"] = "os";
							require_once INCLUDES . "/effect/reports/" . "ObligatoryShortReport" . CLASS_EXT ;
							$DepartmentObligatoryShortReport = new ObligatoryShortReport();
							$reports = $DepartmentObligatoryShortReport->createReportByDepartment(0, $this->department_id, 2);// $this->department_id = period_id
							$this->output += array("obligatory_short"=>$reports);	
						break;
						
						case "osf":
							$this->output["mode"] = "creports";
							$this->output["smode"] = "osf";
							require_once INCLUDES . "/effect/reports/" . "ObligatoryShortReportFaculties" . CLASS_EXT ;
							$FObligatoryShortReport = new ObligatoryShortReportFaculties();
							$reports = $FObligatoryShortReport->createReportByDepartment(0, $this->department_id, 1); // $this->department_id = period_id
							$this->output += array("obligatory_short_faculty"=>$reports);	
						break;
					}	
				}
				$this->downloadXlsHeaders('report'.$this->Contract->getFromDate($this->period_id).'_'.$this->Contract->getToDate($this->period_id).'.xls');			
			}
			break;
			
			case "print": {
				$this->output["mode"] = $this->output["scripts_mode"] = "reports";
				$this->output["html"] = true;
				switch($this->report_type) {
					case "s": 
							require_once INCLUDES . "/effect/reports/" . "DepartmentStimulationReport" . CLASS_EXT ;
							$DepartmentStimulationReport = new DepartmentStimulationReport();
							$reports = $DepartmentStimulationReport->createReportByDepartment($this->department_id, $this->period_id);
							$this->output += array("stimulations"=>$reports);			
					break;
					
					case "o": 
							require_once INCLUDES . "/effect/reports/" . "DepartmentObligatoryReport" . CLASS_EXT ;
							$DepartmentObligatoryReport = new DepartmentObligatoryReport();
							$reports = $DepartmentObligatoryReport->createReportByDepartment($this->department_id, $this->period_id);
							$this->output += array("obligatory"=>$reports);	
					break;
					
					case "os":
						$this->output["mode"] = "creports";
						$this->output["smode"] = "os";
						require_once INCLUDES . "/effect/reports/" . "ObligatoryShortReport" . CLASS_EXT ;
						$DepartmentObligatoryShortReport = new ObligatoryShortReport();
						$reports = $DepartmentObligatoryShortReport->createReportByDepartment(0, $this->department_id, 2);// $this->department_id = period_id
						$this->output += array("obligatory_short"=>$reports);	
					break;
					
					case "osf":
						$this->output["mode"] = "creports";
						$this->output["smode"] = "osf";
						require_once INCLUDES . "/effect/reports/" . "ObligatoryShortReportFaculties" . CLASS_EXT ;
						$FObligatoryShortReport = new ObligatoryShortReportFaculties();
						$reports = $FObligatoryShortReport->createReportByDepartment(0, $this->department_id, 1); // $this->department_id = period_id
						$this->output += array("obligatory_short_faculty"=>$reports);	
					break;
				}
															
			}
			break;
			
		}
		
	}
	
	
	private function countFields($arr) {
		for($i=0;$i<=9;$i++) {
			if(!empty($arr["text".$i]))
				$result++;
		}
		for($i=0;$i<=1;$i++) {
			if(!empty($arr["file".$i]))
				$result++;
		}
		return $result;
	}
	
	function checkIndicatorProgress($contract_id, $indicator_id = null, $year_id) {
			global $EFF, $Auth;
			// echo $contract_id."-<br>";
	$EFF->SetTable("effect_plan", "ep");
		$EFF->AddCondFS("ep.contract_id", "=", $contract_id);
		if(!empty($year_id)) {
			$EFF->AddTable("effect_periods", "epe");
			$EFF->AddCondFF("epe.id", "=", "ec.period_id");
			$EFF->AddCondFS("epe.year_id", "=", $year_id);
		}
		
		$EFF->AddTable("indicators", "i");
		
		if(empty($indicator_id)){
			$EFF->AddCondFF("i.id", "=", "ep.indicator_id");
		}
		else {
			$EFF->AddCondFS("i.id", "=", $indicator_id);
			$EFF->AddCondFF("ep.indicator_id", "=", "i.id");
				// echo $indicator_id."<br>";
		}
		
		$EFF->AddCondFS("i.ind_type", "=", 1);
		$EFF->AddTable("effect_contracts", "ec");
		$EFF->AddCondFF("ec.id", "=", "ep.contract_id");
		$EFF->AddField("i.id", "indicator_id");
		$EFF->AddField("i.auto", "auto");
		$EFF->AddField("i.post_id", "posts");
		$EFF->AddField("i.conditions", "conditions");
		$EFF->AddField("i.formula", "formula");
		$EFF->AddField("i.macros", "macros");
		$EFF->AddField("ep.id", "plan_id");
		$EFF->AddField("ec.post_id", "post_id");
		// $EFF->AddField("ec.contract_id", "contract_id");
		
		$res = $EFF->Select();
		while($row = $EFF->FetchAssoc($res)) {
			
					// CF::Debug($row["plan_id"]);
			$posts = explode(";", $row["posts"]);
			if(in_array($row["post_id"], $posts)) {
				if($row["auto"]!=1) {
					$EFF->SetTable("effect_indicators");
					$EFF->AddCondFS("plan_id", "=", $row["plan_id"]);
					$EFF->AddField("text0");
					$val = $EFF->FetchAssoc($EFF->Select(1));
					$row["val"] = $val["text0"];
				} else {
					require_once INCLUDES . "/effect/macro/" . $row["macros"] . CLASS_EXT ;
					$Macro = new $row["macros"]($contract_id, $row["conditions"]);
					$row["val"] = $Macro->getVal();
					$row["conditions"] = $Macro->getCond();
				}
					// echo $row["val"]."-".$row["plan_id"]."-".$row["contract_id"]."<br>";
				

				require_once INCLUDES . "/effect/" . "Condition" . CLASS_EXT ;
				$Condition = new Condition($row["formula"], $row["conditions"], array(0=>$row["val"]), $row["post_id"]);
				if(!empty($row["val"])) {
					$row["result"] = $Condition->getResult();
				} 
				else {
					$row["result"] = false;
				}
				$row["conditions"] = $Condition->getCond();
				$row["val"] = $Condition->getVal();
				

				
				$result[$row["indicator_id"]]["complete"] = $row["result"];
				$result[$row["indicator_id"]]["cond"] = 
					(is_array($row["conditions"]) 
					? $row["conditions"][$row["post_id"]] 
					: $row["conditions"]);
				$result[$row["indicator_id"]]["val"] = empty($row["val"][0])
					? 0 : $row["val"][0];
						

			}
		}
		return $result;
	}
	
	
	function downloadXlsHeaders($filename) {
		header('Content-Type: text/html; charset=windows-1251');
		header('P3P: CP="NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM"');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0', FALSE);
		header('Pragma: no-cache');
		header('Content-transfer-encoding: binary');
		header('Content-Disposition: attachment; filename='.$filename.'');
		header('Content-Type: application/x-unknown');		
	}
	
	function addIndicatorForm($id) {
			global $EFF, $DB;
		$this->output["mode"] = "form";
		$id = intval($id);			
		
		$EFF->SetTable("effect_plan");
		$EFF->AddCondFS("id", "=", $id);
		$res = $EFF->FetchAssoc($EFF->Select());
		
		$EFF->SetTable("indicators");
		$EFF->AddCondFS("id", "=", $res["indicator_id"]);
		$this->output["iname"]  = $EFF->FetchAssoc($EFF->Select(1));
		
		$DB->SetTable("nsau_files");
		$DB->AddCondFS("user_id", "=", $res["user_id"]);
		$DB->AddCondFS("deleted", "=", 0);
		$fr = $DB->Select();
		while($f = $DB->FetchAssoc($fr))
			$this->output["files"][$f["id"]] = $f["name"];
		
		$this->createForm($res["indicator_id"]);
	}
	

	

	
	function contractMsg() {
		$this->output["messages"] = $this->Contract->output["messages"];
		$this->output["json"] = $this->Contract->output["json"];		
	}
	
	function loadPosts() {
		global $EFF;
		$EFF->SetTable("posts");
		$res = $EFF->Select();
		while($row = $EFF->FetchAssoc($res))
			$this->output["posts"][$row["id"]] = $row;		
	}
	
	
	
	function postIndicatorList($var) {
		global $EFF;
		$EFF->SetTable("posts");
		$EFF->AddCondFS("id", "=", $var);
		$p = $EFF->FetchAssoc($EFF->Select(1));
		$this->output["post_name"] = $p["name"];
		$EFF->SetTable("indicators");
		$res = $EFF->Select();   
		while($row = $EFF->FetchAssoc($res)) {
			$row["posts"] = explode(";", $row["post_id"]);
			$EFF->SetTable("forms");
			$EFF->AddCondFS("indicator_id", "=", $row["id"]);
			$EFF->AddField("type");
			$EFF->AddField("id");
			$EFF->AddField("desc");
			$EFF->AddField("help");
			$EFF->AddOrder("id");
			$form_res  = $EFF->Select();
			while($form_row = $EFF->FetchAssoc($form_res)) {
				$row["fields"]["desc"][$form_row["id"]] = $form_row["desc"];	
				$row["fields"]["type"][$form_row["id"]] = $form_row["type"];				
			}
			if(in_array($var, $row["posts"]))
			$this->output["indicators"][$row["ind_type"]][$row["type"]][] = $row;		
		}
		asort($this->output["indicators"]); 
	}
	
	function loadTeacherDepartments($user_id = null) {
		global $DB, $EFF, $Auth;
		if(empty($user_id))
			$people_id = $Auth->people_id;
		else
			$people_id = $this->peopleIdByUserId($user_id);
		
		$DB->SetTable("nsau_teachers");
		$DB->AddCondFS("people_id", "=", $people_id);
		$DB->AddField("department_id");
		$row = $DB->FetchAssoc($DB->Select(1));
		$dep_ids = explode(";", $row["department_id"]);
		foreach($dep_ids as $dep_id) {
			$DB->SetTable("nsau_departments");
			$DB->AddCondFS("id", "=", $dep_id);
			$dep = $DB->FetchAssoc($DB->Select(1));
			if(!empty($dep["name"]))
				$departments[$dep_id] = $dep["name"];
		}
		
		$EFF->SetTable("effect_contracts");
		$EFF->AddCondFS("user_id", "=", $user_id);
		$EFF->AddField("department_id");
		$res = $EFF->Select();
		while($row = $EFF->FetchAssoc($res)) {
			if(!empty($row)) {
				$DB->SetTable("nsau_departments");
				$DB->AddCondFS("id", "=", $row["department_id"]);
				$dep = $DB->FetchAssoc($DB->Select(1));
				$departments[$dep["id"]] = $dep["name"];
			}
		}
		
		return $departments;
	}

	function peopleIdByUserId($user_id) {
		global $DB;
		$DB->SetTable("nsau_people");
		$DB->AddCondFS("user_id", "=", $user_id);
		$row = $DB->FetchAssoc($DB->Select(1));
		return $row["id"];
	}
	
	function loadAllDepartments() {
		global $DB;
		$DB->SetTable("nsau_departments");
		$res = $DB->Select();		
		while($dep = $DB->FetchAssoc($res)) {
			$departments[$dep["id"]] = $dep["name"];
		}
		return $departments;
	}
	
	function deleteFIndicator($id) {
		global $EFF, $Engine;
		$EFF->SetTable("effect_indicators");
		$EFF->AddCondFS("id", "=", $id);
		$EFF->Delete();
		$Engine->LogAction($this->module_id, "indicator", $id, "delete");	
		$this->output["json"][] = "�������."; 
	}
	
	function deleteFIndicators($id) {
		global $EFF, $Engine;
		$EFF->SetTable("effect_indicators");
		$EFF->AddCondFS("plan_id", "=", $id);
		$EFF->Delete();
		$EFF->SetTable("effect_plan");
		$EFF->AddCondFS("id", "=", $id);
		$EFF->Delete();
		$Engine->LogAction($this->module_id, "plan", $id, "delete");	
		$this->output["json"][] = "�������."; 
	}
	
	function showFullIndicator($id) {
		global $EFF, $DB;
		
		
		$EFF->SetTable("effect_plan");
		$EFF->AddCondFS("id", "=", $id);
		$ind = $EFF->FetchAssoc($EFF->Select(1));
		
		$EFF->SetTable("indicators");
		$EFF->AddCondFS("id", "=", $ind["indicator_id"]);
		$this->output["iname"]  = $EFF->FetchAssoc($EFF->Select(1));	
		
		$EFF->SetTable("forms");
		$EFF->AddCondFS("indicator_id", "=", $ind["indicator_id"]);
		$EFF->AddOrder("id");
		$res = $EFF->Select();
		while($row = $EFF->FetchAssoc($res))
			$this->output["fields"][] = $row;
		
		
		if($this->output["iname"]["auto"]==1) {
			
			require_once INCLUDES . "/effect/macro/" . $this->output["iname"]["macros"] . CLASS_EXT ;
			$Macro = new $this->output["iname"]["macros"]($ind["contract_id"], $this->output["iname"]["conditions"]);
			$values = $Macro->getFields();
			if(!empty($values)) {
				$this->output["indicators"] = $values;
			}
			else {
				$this->output["indicators"][0] = array("text0"=>$Macro->getVal());
			}
			
		} else {
		
			$EFF->SetTable("effect_indicators");
			$EFF->AddCondFS("plan_id", "=", $id);
			$res = $EFF->Select();
			while($row = $EFF->FetchAssoc($res)) {
				$this->output["indicators"][] = $row;
			}

		}
		
		$DB->SetTable("nsau_files");
		$DB->AddCondFS("user_id", "=", $ind["user_id"]);
		$DB->AddCondFS("deleted", "=", 0);
		$fr = $DB->Select();
		while($f = $DB->FetchAssoc($fr)) {
			$this->output["files"][$f["id"]] = $f["name"];
		}
	}
	
	function showIndicator($id) {
		global $EFF, $DB;

		$EFF->SetTable("effect_indicators");
		$EFF->AddCondFS("id", "=", $id);
		$row = $EFF->FetchAssoc($EFF->Select(1));
		$this->output["indicator"] = $row;
		$EFF->SetTable("effect_plan");
		$EFF->AddCondFS("id", "=", $row["plan_id"]);
		$row_p = $EFF->FetchAssoc($EFF->Select(1));
		$DB->SetTable("nsau_files");
		$DB->AddCondFS("user_id", "=", $row_p["user_id"]);
		$DB->AddCondFS("deleted", "=", 0);
		$fr = $DB->Select();
		while($f = $DB->FetchAssoc($fr))
			$this->output["files"][$f["id"]] = $f["name"];
		$this->createForm($row_p["indicator_id"]);
	}
	
	function createForm($id) {
		global $EFF;
		$EFF->SetTable("forms");
		$EFF->AddCondFS("indicator_id", "=", $id);
		$EFF->AddField("type");
		$EFF->AddField("desc");
		$EFF->AddField("help");
		$EFF->AddOrder("id");
		$form_res  = $EFF->Select();
		while($form_row = $EFF->FetchAssoc($form_res))
			$this->output["forms"][] = $form_row;			
	}
	
	function addPlanIndicator($id, $contract_id) {
		global $Auth, $EFF, $Engine;
		$EFF->SetTable("effect_plan");
		$EFF->AddCondFS("indicator_id", "=", $id);
		$EFF->AddCondFS("user_id", "=", $Auth->user_id);
		$EFF->AddCondFS("contract_id", "=", $contract_id);
		$test = $EFF->FetchAssoc($EFF->Select());
		if(!empty($test))
			$this->output["json"]["error"][] = "�������� ��� ��������";
		elseif(!empty($id)) {
			$EFF->SetTable("effect_plan");
			$EFF->AddValue("indicator_id", $id);
			$EFF->AddValue("user_id", $Auth->user_id);
			$EFF->AddValue("contract_id", $contract_id);
			$EFF->Insert();
			$Engine->LogAction($this->module_id, "plan", $EFF->LastInsertID(), "add");	
			$this->output["json"] = $EFF->LastInsertID();
		}
	}
	
	function editIndicator($id, $array) {
		global $Auth, $EFF;
		$EFF->SetTable("indicators");
		$EFF->AddValue("name", $array["name"]);
		$EFF->AddValue("indicator", $array["indicator"]);
		$EFF->AddValue("period", $array["period"]);
		$EFF->AddValue("size", $array["size"]);
		$EFF->AddValue("type", $array["type"]);	
		$EFF->AddValue("conditions", $array["conditions"]);	
		$EFF->AddValue("ind_type", $array["ind_type"]);	
		$EFF->AddValue("formula", $array["formula"]);	
		$EFF->AddValue("auto", $array["auto"]);	
		$EFF->AddValue("deny_form", $array["deny_form"]);	
		$EFF->AddValue("macros", $array["macros"]);	
		$EFF->AddValue("post_id", implode(";", $array["posts"]));	
		$EFF->AddCondFS("id", "=", $id);
		$EFF->Update();
		$EFF->SetTable("forms");
		$EFF->AddCondFS("indicator_id", "=", $id);
		$EFF->Delete();
		foreach($array["fields"]["type"] as $fid => $type) {
			$EFF->SetTable("forms");
			$EFF->AddValue("type", $type);
			$EFF->AddValue("desc", $array["fields"]["desc"][$fid]);
			$EFF->AddValue("help", $array["fields"]["help"][$fid]);
			$EFF->AddValue("indicator_id", $id);
			$EFF->Insert();
		}	
	}


	function editIndicatorFields($fid, $ftext, $ffile) {    
			global $Engine, $EFF;
		$EFF->SetTable("effect_indicators");
		$EFF->AddCondFS("id", "=", $fid);
		foreach($ftext as $id => $val) 
			$EFF->AddValue("text".$id, iconv("utf-8", "cp1251", htmlspecialchars($val)));
		foreach($ffile as $id => $val) 
			if(!empty($val))
				$EFF->AddValue("file".$id, iconv("utf-8", "cp1251", htmlspecialchars($val)));				
			else 
				$EFF->AddValue("file".$id, null);	
		$EFF->Update();
		$Engine->LogAction($this->module_id, "indicator", $fid, "edit"); 
		$this->output["json"]  = $ftext[0];	
	}
				
	
	function addIndicatorFields($fid, $ftext, $ffile) {    
			global $Engine, $EFF;
		$EFF->SetTable("effect_indicators");
		$EFF->AddValue("plan_id", $fid);
		foreach($ftext as $id => $val) 
			$EFF->AddValue("text".$id, iconv("utf-8", "cp1251", htmlspecialchars($val)));
		foreach($ffile as $id => $val) 
			$EFF->AddValue("file".$id, iconv("utf-8", "cp1251", htmlspecialchars($val)));				
		$EFF->Insert();
		$this->output["json"]["name"]  = $ftext[0];
		$this->output["json"]["id"]  = $EFF->LastInsertID();
		$Engine->LogAction($this->module_id, "indicator", $EFF->LastInsertID(), "add");
	}
	
	function indicatorsList($id = null) {
		global $EFF, $Engine;
		$EFF->SetTable("indicators");
		if(!empty($id))
			$EFF->AddCondFS("id", "=", $id);
		$res = $EFF->Select();
		while($row = $EFF->FetchAssoc($res)) {
			$row["posts"] = explode(";", $row["post_id"]);
			$EFF->SetTable("forms");
			$EFF->AddCondFS("indicator_id", "=", $row["id"]);
			$EFF->AddField("type");
			$EFF->AddField("id");
			$EFF->AddField("desc");
			$EFF->AddField("help");
			$EFF->AddOrder("id");
			$form_res  = $EFF->Select();
			while($form_row = $EFF->FetchAssoc($form_res)) {
				$row["fields"]["desc"][$form_row["id"]] = $form_row["desc"];	
				$row["fields"]["help"][$form_row["id"]] = $form_row["help"];	
				$row["fields"]["type"][$form_row["id"]] = $form_row["type"];				
			}
			if(empty($id))			
				$this->output["indicators"][$row["ind_type"]][$row["type"]][] = $row;
			else 
				$this->output["indicators"] = $row;			
		}
		arsort($this->output["indicators"]);
		if(empty($this->output["indicators"])) 
			return false;
		else return true;
	}
	
	function addIndicator($array) {
		global $Auth, $EFF;
		$EFF->SetTable("indicators");
		$EFF->AddValue("name", $array["name"]);
		$EFF->AddValue("indicator", $array["indicator"]);
		$EFF->AddValue("period", $array["period"]);
		$EFF->AddValue("size", $array["size"]);
		$EFF->AddValue("type", $array["type"]);		
		$EFF->AddValue("ind_type", $array["ind_type"]);		
		$EFF->AddValue("conditions", $array["conditions"]);		
		$EFF->AddValue("formula", $array["formula"]);		
		$EFF->AddValue("post_id", implode(";", $array["posts"]));		
		$EFF->AddValue("macros", $array["macros"]);		
		$EFF->AddValue("auto", $array["auto"]);		
		$EFF->AddValue("deny_form", $array["deny_form"]);		
		$EFF->Insert();
		$ind_id = $EFF->LastInsertID();
		foreach($array["fields"]["type"] as $id => $type) {
			$EFF->SetTable("forms");
			$EFF->AddValue("type", $type);
			$EFF->AddValue("desc", $array["fields"]["desc"][$id]);
			$EFF->AddValue("help", $array["fields"]["help"][$id]);
			$EFF->AddValue("indicator_id", $ind_id);
			$EFF->Insert();
		}
	}
	
	function checkObligatoryIndicators($oi) {
		global $EFF, $DB;
		$progress = $oi;
		foreach($progress as $val) {
			if($val["complete"])
				$t++;
		}
		$complete = ($t == count($progress)) ? true : false;
		$prog = round($t/count($progress)*100, 2);
		return array("progress" => $prog, "complete" => $complete);
	}
	
	function deleteIndicator($id) {
		global $EFF;
		$EFF->SetTable("forms");
		$EFF->AddCondFS("indicator_id", "=", $id);
		$EFF->Delete();
		$EFF->SetTable("indicators");
		$EFF->AddCondFS("id", "=", $id);
		$EFF->Delete();
	}	
		
	function checkForm($array) {
		if(empty($array["name"]) || empty($array["indicator"]) || empty($array["period"]) || empty($array["size"]) || empty($array["type"]) || (count($array["fields"]["desc"])<1)) {
			$this->output["messages"]["bad"][] = 101;
			return false;
		}
		else
			return true;
	}

	function AccessError($s = null) {
		$this->output["json"]["error"] = (empty($s) ? "Access error." : $s);
		$this->output["messages"]["bad"][] = 403;
	}
	/**
	* ���������� � ��� ��������� �� ������� � �������
	* @param int $user_id
	* @return bool
	*/
	protected function isDecree($user_id) {
		global $DB;
		$DB->SetTable("nsau_people");
		$DB->AddCondFS("user_id", "=", $user_id);
		$DB->AddField("status_id");
		$status = $DB->FetchAssoc($DB->Select(1));
		if($status["status_id"]!=10) {
			return false;
		} 
		else {
			return true;
		}
	}
	function Output()	{
			return $this->output;
	}	
}
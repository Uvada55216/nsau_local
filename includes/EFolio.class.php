<?php
class EFolio
/*
version: 1.0
date: 21.09.2016

Notes:
��� ������������ (kind_act) ��� ��:
1 - ������������ ������������
2 - ���������� ����������
3 - ���������� ������������

��� ������������ (kind act) ��� ���:
4 - ������� � ������-������������ ������������
5 - ������� � ��������� ��������
6 - ������� � ������ ��� �������� �� ���������� ���
7 - ����������
8 - ������

��� ������ � ���� �������� (people menu) ������������ �������:
� ����� ensau.ptpl "if(mode=="show_folio_nir") moduleId = 43;" � "if(mode=="show_folio_vr") moduleId = 43;"
*/
{
	private $output;
	private $extra_params;

	function EFolio($global_params, $params, $module_id, $node_id, $module_uri)
	{
		global $Engine, $Auth, $DB;
		$this->output["user_id"] = $this->get_user_id($Auth->user_id); //ID ������������
		$this->output['mode'] = $params; //��������� ������

		$this->extra_params = explode("/", $module_uri);
		switch ($params)
		{
			/*������ ���������-����������� �� ����������� � �������*/
			case "report_unfilled": {
				$exception_fac = array(51,41,21,20,19,10); //id ����������� �� ������ �����������
				$DB->SetTable("nsau_faculties");
				$DB->AddCondFS("is_active", "=", "1");
				$DB->AddCondXF("id NOT IN(".implode(",", $exception_fac).")");
				$fres = $DB->Select();
				while($fac = $DB->FetchAssoc($fres)) {
					$DB->SetTable("nsau_groups");
					$DB->AddCondFS("id_faculty", "=", $fac["id"]);
					$DB->AddCondFS("hidden", "=", 0);
					$gres = $DB->Select();
					while($group = $DB->FetchAssoc($gres)) {
						$DB->SetTable("nsau_people");
						$DB->AddCondFS("id_group", "=", $group["id"]);
						$pres = $DB->Select();
						while($people = $DB->FetchAssoc($pres)) {							
							$nir = $vr = $vkr = $info = 0;
							
							//����� �� � ���------------------------------------
							$DB->SetTable("nsau_folio");
							$DB->AddCondFS("user_id", "=", $people['id']);
							$res = $DB->Select();
							while ($row = $DB->FetchAssoc($res))
							{
								switch ($row['cat_folio'])
								{
									case '0':
										$nir++;
										break;

									case '1':
										$vr++;
										break;
								}
							}
							//--------------------------------------------------

							//����� ��� � ������ �����--------------------------
							$DB->SetTable("nsau_students_works");
							$DB->AddCondFS("student_id", "=", $people['id']);
							$ress = $DB->Select();
							while ($roww = $DB->FetchAssoc($ress))
							{
								if ($roww)
								{
									$vkr++;
								}
							}
							//--------------------------------------------------

							
							$auth_id = $Auth->GetUserData(NULL,NULL,$people['id']);
							$DB->SetTable("auth_users");
							$DB->AddCondFS("id", "=", $auth_id);
							$ress = $DB->Select();
							while ($roww = $DB->FetchAssoc($ress))
							{
								if (!$roww['email']==NULL) //����
								{
									$email=1;
								} else {
									$email = 0;
								}
							}
							
							
							if(!empty($people['photo']) && !empty($people['year']) && !empty($email)) {
								$info = 1;
							}

							if(empty($info) && empty($vkr) && empty($vr) && empty($nir)) {
								$this->output["result"][$fac["name"]][$group["name"]][] = $people["last_name"] ." ". $people["name"] ." ". $people["patronymic"] ." ";//$info."-".$vkr."-".$vr."-".$nir."-". 
							}
						}
					}
				}
				header('Content-Type: text/html; charset=windows-1251');
				header('P3P: CP="NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM"');
				header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
				header('Cache-Control: no-store, no-cache, must-revalidate');
				header('Cache-Control: post-check=0, pre-check=0', FALSE);
				header('Pragma: no-cache');
				header('Content-transfer-encoding: binary');
				header('Content-Disposition: attachment; filename=report.xls');
				header('Content-Type: application/x-unknown');		
			}
			break;
			
			/*���������� ������������ (������ � ��)*/
			case 'vr':
			{
				$this->output["scripts_mode"] = $this->output["mode"] = "vr"; //����������� ptpl
				$user_id=$this->get_user_id($Auth->user_id);

				//����������� ��� ������������ �� id
				function get_user_fio($id)
				{
					global $DB;
					$DB->SetTable("nsau_people");
					$DB->AddCondFS("id", "=", $id);
					$user = $DB->FetchAssoc($DB->Select(1));
					$row["user_fio"] = $user["last_name"]." ".$user["name"]." ".$user["patronymic"];
					return $row["user_fio"];
				}
				$this->output['user_fio'] = get_user_fio($user_id);

				$DB->SetTable("nsau_folio");

				if(isset($_POST['upload']))
    			{
    				if ($_FILES['vr']['name'])
    				{
							$filename = $_FILES['vr']['name']; //��� ����������������� �����
							$this->output['bad_message'] =  filesize($filename);
							$type = strtolower(substr($filename, 1+strrpos($filename,"."))); //���������� �����
							$path = 'files/folio/'; //���� ��� ��������
							$fname = "folio_". "vr_". rand() . "_" . $this->get_user_id($Auth->user_id). ".". $type;
							$fname = str_replace(" ","",$fname); //������� ������ ������� � �����

							if (!@copy($_FILES['vr']['tmp_name'], $path . $fname))
							{
								$this->output['bad_message'] = "������ �������� �����";
							}
					}
					else
					{
						//$fname=""; //���� ����� ���, �� ���������� ������������� � ��
						$fname="empty_". rand() . "_" . $this->get_user_id($Auth->user_id);
					}

					//���������� ��
					$DB->SetTable("nsau_folio");
					$DB->AddValue("cat_folio", "1"); 					//���������, � ������ ������ �� - 1
					$DB->AddValue("user_id", $user_id); 				//ID ������������
					$DB->AddValue("kind_act", $_POST['kind_act']);		//��� ������������
					$DB->AddValue("date", $_POST['date']);				//����
					$DB->AddValue("name", $_POST['event_name']); 		//�������� �����������
					$DB->AddValue("place", $_POST['place']); 			//����� ����������
					$DB->AddValue("doc_type", $_POST['doc_type']); 		//����� ����������
					//$DB->AddValue("comment", $_POST['comment']); 		//������� ��������
					$DB->AddValue("file", $fname);						//��� �����
					$DB->Insert();
					$DB->Update();
					$Engine->LogAction(43, "VR", $user_id, "add"); 		//������ � ����
					header("Location: ".$_SERVER["REQUEST_URI"]); 		//�� ��� ��������� �������� ������� �����
				}
			}
			break;

			/*������-����������������� � ��������� ������ (������ � ��)*/
			case 'nir':
			{
				$this->output["scripts_mode"] = $this->output["mode"] = "nir"; //����������� ptpl
				$user_id=$this->get_user_id($Auth->user_id);

				//����������� ��� ������������ �� id
				function get_user_fio($id)
				{
					global $DB;
					$DB->SetTable("nsau_people");
					$DB->AddCondFS("id", "=", $id);
					$user = $DB->FetchAssoc($DB->Select(1));
					$row["user_fio"] = $user["last_name"]." ".$user["name"]." ".$user["patronymic"];
					return $row["user_fio"];
				}
				$this->output['user_fio'] = get_user_fio($user_id);

				if(isset($_POST['upload']))
    			{
    				if ($_FILES['nir']['name'])
    				{
						$filename = $_FILES['nir']['name']; //��� ����������������� �����
						$type = strtolower(substr($filename, 1+strrpos($filename,"."))); //���������� �����
						$path = 'files/folio/';//���� ��� ��������
						$fname = "folio_". "nir_". rand() .  "_" . $this->get_user_id($Auth->user_id). ".". $type;
						$fname = str_replace(" ","",$fname);//������� ������ ������� � �����

						if (!@copy($_FILES['nir']['tmp_name'], $path . $fname))
						{
							$this->output['bad_message'] = "������ �������� �����";
						}
					}
					else
					{
						//$fname=""; //���� ����� ���, �� ���������� ������������� � ��
						$fname="empty_". rand() . "_" . $this->get_user_id($Auth->user_id);
					}

					//���������� ��
					$DB->SetTable("nsau_folio");
					$DB->AddValue("cat_folio", "0"); 					//���������, � ������ ������ ��� - 0
					$DB->AddValue("user_id", $user_id); 				//ID ������������
					$DB->AddValue("kind_act", $_POST['kind_act']);		//��� ������������
					$DB->AddValue("date", $_POST['date']);				//����
					$DB->AddValue("name", $_POST['name']); 				//��������
					$DB->AddValue("author", $_POST['author']); 			//�����
					$DB->AddValue("sub_author", $_POST['sub_author']); 	//�������
					//$DB->AddValue("comment", $_POST['comment']); 		//������� ��������
					$DB->AddValue("file", $fname);						//��� �����
					$DB->Insert();
					$DB->Update();
					$Engine->LogAction(43, "NIR", $user_id, "add"); 	//������ � ����
					header("Location: ".$_SERVER["REQUEST_URI"]); 		//�� ��� ��������� �������� ������� �����
				}
			}
			break;

			/*����� ��������� ��� ���������� ������������*/
			case 'show_folio_vr':
				{
					$this->output["scripts_mode"] = $this->output["mode"] = "show_folio_vr"; //����������� ptpl
					$user_id=$this->get_user_id($Auth->user_id);
					$folio_data = array();
					$DB->SetTable("nsau_folio");
					$DB->AddCondFS("user_id", "=", $user_id);
					$DB->AddCondFS("cat_folio", "=", "1");
					$res = $DB->Select();
					$i=0; //???
					while ($row = $DB->FetchAssoc($res))
					{
						$folio_data[$i]=$row; //???
						$i++; //???
						//$this->output['folio'] = $row;
					}
					$this->output["folio"] = $folio_data;
					//�������� ������ ���������
					if(isset($_POST['delete_vr']))
					{
						$DB->SetTable("nsau_folio");
						$DB->AddCondFS("file", "=", $_POST['del_vr_file']);
						$DB->AddCondFS("user_id", "=", $user_id);
						$DB->Delete();
						$DB->Update();
						$Engine->LogAction(43, "VR", $user_id, "delete"); 
						header("Location: ".$_SERVER["REQUEST_URI"]);
					}
					/*�������������� ������ ���������*/
					if(isset($_POST["edit_vr"]))
					{
						$DB->SetTable("nsau_folio");
						$DB->AddCondFS("file", "=", $_POST['vr_file']);
						$DB->AddCondFS("user_id", "=", $user_id);
						$DB->AddValue("kind_act", $_POST['edit_vr_kind_act']);
						$DB->AddValue("name", $_POST['edit_vr_event_name']);
						$DB->AddValue("date", $_POST['edit_vr_data']);
						$DB->AddValue("place", $_POST['edit_vr_place']);
						$DB->AddValue("doc_type", $_POST['edit_vr_doc_type']);

						if ($_FILES['vr_edit']['name'])
	    				{
								$filename = $_FILES['vr_edit']['name']; //��� ����������������� �����
								$this->output['bad_message'] =  filesize($filename);
								$type = strtolower(substr($filename, 1+strrpos($filename,"."))); //���������� �����
								$path = 'files/folio/'; //���� ��� ��������
								//$fname = "folio_". "vr_". rand() .  "_" . $user_id. ".". $type;
								$fn = explode(".",$_POST['vr_file']);
								$fname = $fn[0] . "." . $type;
								$fname = str_replace(" ","",$fname); //������� ������ ������� � �����

								if (!@copy($_FILES['vr_edit']['tmp_name'], $path . $fname))
								{
									$this->output['bad_message'] = "������ �������� �����";
								}
								else
								{
									$DB->AddValue("file", $fname);//��� �����
								}
						}
						$DB->Update();
						$Engine->LogAction(43, "VR", $user_id, "edit");//������ � ����*/
						header("Location: ".$_SERVER["REQUEST_URI"]);
					}
				}
			break;

			/*����� ��������� ��� ������-����������������� � ��������� ������*/
			case 'show_folio_nir':
				{
					$this->output["scripts_mode"] = $this->output["mode"] = "show_folio_nir"; //����������� ptpl
					$user_id=$this->get_user_id($Auth->user_id);
					$folio_data = array();
					$DB->SetTable("nsau_folio");
					$DB->AddCondFS("user_id", "=", $user_id);
					$DB->AddCondFS("cat_folio", "=", "0");
					$res = $DB->Select();
					$i=0; //???
					while ($row = $DB->FetchAssoc($res))
					{
						$folio_data[$i]=$row; //???
						$i++; //???
						//$this->output['folio'] = $row;
					}
					$this->output['folio'] = $folio_data;

					//�������� ������ ���������
					if(isset($_POST['delete_nir']))
					{
						$DB->SetTable("nsau_folio");
						$DB->AddCondFS("file", "=", $_POST['del_nir_file']);
						$DB->AddCondFS("user_id", "=", $user_id);
						$DB->Delete();
						$DB->Update();
						$Engine->LogAction(43, "NIR", $user_id, "delete"); 		//������ � ����
						header("Location: ".$_SERVER["REQUEST_URI"]);
					}
					/*�������������� ������ ���������*/
					if(isset($_POST['edit_nir']))
					{
						$DB->SetTable("nsau_folio");
						$DB->AddCondFS("file", "=", $_POST['nir_file']);
						$DB->AddCondFS("user_id", "=", $user_id);
						$DB->AddValue("kind_act", $_POST['edit_nir_kind_act']);
						$DB->AddValue("name", $_POST['edit_nir_name']);
						$DB->AddValue("date", $_POST['edit_nir_data']);
						$DB->AddValue("author", $_POST['edit_nir_author']);
						$DB->AddValue("sub_author", $_POST['edit_nir_sub_author']);

						if ($_FILES['nir_edit']['name'])
	    				{
								$filename = $_FILES['nir_edit']['name']; //��� ����������������� �����
								$this->output['bad_message'] =  filesize($filename);
								$type = strtolower(substr($filename, 1+strrpos($filename,"."))); //���������� �����
								$path = 'files/folio/'; //���� ��� ��������
								//$fname = "folio_". "nir_". rand() .  "_" . $user_id. ".". $type;
								$fn = explode(".",$_POST['nir_file']);
								$fname = $fn[0] . "." . $type;
								$fname = str_replace(" ","",$fname); //������� ������ ������� � �����

								if (!@copy($_FILES['nir_edit']['tmp_name'], $path . $fname))
								{
									$this->output['bad_message'] = "������ �������� �����";
								}
								else
								{
									$DB->AddValue("file", $fname);//��� �����
								}
						}

						$DB->Update();
						$Engine->LogAction(43, "NIR", $user_id, "edit");//������ � ����*/
						header("Location: ".$_SERVER["REQUEST_URI"]);
					}
				}
			break;

			/*����� ��������� � ���� ������������ ��� ���������� ������������*/
			case 'ajax_people_menu_show_folio_vr':
				{
					$user_check=$this->get_user_id($Auth->user_id); //�������� �� ������������ ��� �����
					if (isset($user_check))
					{
						$this->output['user_check'] = "1"; //������������
					}
					else
					{
						$this->output['user_check'] = "0"; //�����
					}

					$buffer=explode('/', $_SERVER['HTTP_REFERER']);
					$user_id=$buffer[4];

					$DB->SetTable("nsau_folio");
					$DB->AddCondFS("user_id", "=", $user_id);
					$DB->AddCondFS("cat_folio", "=", "1");
					$res = $DB->Select();
					$i=0; //???
					while ($row = $DB->FetchAssoc($res))
					{
						$folio_data[$i]=$row; //???
						$i++; //???
						//$this->output['folio'] = $row;
					}
					$this->output['folio'] = $folio_data;
					//�������� ������ ���������
					if(isset($_POST['delete_blank_folio']))
					{
						$DB->SetTable("nsau_folio");
						$DB->AddCondFS("file", "=", $_POST['delete_name']);
						$DB->Delete();
						$DB->Update();
						$Engine->LogAction(43, "VR", $user_id, "delete"); 		//������ � ����
						header("Location: ".$_SERVER["REQUEST_URI"]);
					}
				}
			break;

			/*����� ��������� � ���� ������������ ��� ������-����������������� � ��������� ������*/
			case 'ajax_people_menu_show_folio_nir':
				{
					$user_check=$this->get_user_id($Auth->user_id); //�������� �� ������������ ��� �����
					if (isset($user_check))
					{
						$this->output['user_check'] = "1"; //������������
					}
					else
					{
						$this->output['user_check'] = "0";
					}

					$buffer=explode('/', $_SERVER['HTTP_REFERER']);
					$user_id=$buffer[4];

					$folio_data = array();
					$DB->SetTable("nsau_folio");
					$DB->AddCondFS("user_id", "=", $user_id);
					$DB->AddCondFS("cat_folio", "=", "0");
					$res = $DB->Select();
					$i=0; //???
					while ($row = $DB->FetchAssoc($res))
					{
						$folio_data[$i]=$row; //???
						$i++; //???
						//$this->output['folio'] = $row;
					}
					$this->output['folio'] = $folio_data;



					//�������� ������ ���������
					if(isset($_POST['delete_nir_work']))
					{
						$DB->SetTable("nsau_folio");
						$DB->AddCondFS("file", "=", $_POST['delete_name']);
						$DB->AddCondFS("user_id", "=", $user_id);
						$DB->Delete();
						$DB->Update();
						$Engine->LogAction(43, "NIR", $user_id, "delete"); 		//������ � ����
						header("Location: ".$_SERVER["REQUEST_URI"]); 
					}
				}
			break;

			//������������
			case "study": {
				header("Content-type: text/html; charset=windows-1251");	

				if(empty($_REQUEST["data"]["select_group"])) {
					$group = intval($this->extra_params[0]);
				}
				if(empty($_REQUEST["data"]["semester"])) {
					$semester = intval($this->extra_params[1]);
				}
				if(!empty($group) && !empty($semester)) {
					$this->output["mode"] = "study_report";
				} else {
					$semester = $this->output["current_semester"] = $_REQUEST["data"]["semester"] ? $_REQUEST["data"]["semester"] : 1;
					$group = $_REQUEST["data"]["select_group"];
					$this->output["mode"] = "study";
				}



				$DB->SetTable("nsau_people");
				$DB->AddCondFS("id_group", "=", $group);
				$DB->AddField("id");
				$res = $DB->Select();
				while($row = $DB->FetchAssoc($res)) {
					$DB->SetTable("nsau_students_progress");
					$DB->AddCondFS("people_id", "=", $row["id"]);
					$DB->AddCondFS("semester", "=", $semester);
					$DB->AddField("subject");
					$DB->AddField("type");
					$DB->AddOrder("type");
					$s_res = $DB->Select();
					while($s_row = $DB->FetchAssoc($s_res)) {
						$subjects[$s_row["type"]][$s_row["subject"]] = $s_row["subject"];
					}
				}


				require_once INCLUDES . "/folio/" . "Subject" . CLASS_EXT ;
				require_once INCLUDES . "/folio/" . "People" . CLASS_EXT ;
				require_once INCLUDES . "/folio/" . "Group" . CLASS_EXT ;

				foreach($subjects as $type => $arr) {
					foreach($arr as $sub) {
						$group_params[] = mb_strtolower($sub."_".$type);
					}
				}

				$Group = new Group($group_params);

				$DB->SetTable("nsau_people");
				$DB->AddCondFS("id_group", "=", $group);
				$DB->AddFields(array("id", "last_name", "name", "patronymic"));
				$DB->AddOrder("last_name");
				$res = $DB->Select();
				while($row = $DB->FetchAssoc($res)) {
					$DB->SetTable("nsau_students_progress");
					$DB->AddCondFS("people_id", "=", $row["id"]);
					$DB->AddCondFS("semester", "=", $semester);
					$DB->AddFields(array("subject", "value", "type"));
					$DB->AddOrder("type");
					$s_res = $DB->Select();
					while($s_row = $DB->FetchAssoc($s_res)) {
						$study[$row["id"]][$s_row["type"]][$s_row["subject"]] = $s_row["value"];
					}
					foreach($subjects as $type => $arr) {
						foreach($arr as $sub) {
							$final_study[] = array("name" => $sub, "type" => $type, "val" => $study[$row["id"]][$type][$sub]);
						}
					}

					$Group->addPeople(new People($final_study, $row["last_name"] ." ". $row["name"] ." ". $row["patronymic"]));
					unset($final_study);

				}


				$DB->SetTable("nsau_groups");
				$DB->AddCondFS("id", "=", $group);
				$gr = $DB->FetchAssoc($DB->Select(1));
				$this->output["group_name"] = $gr["name"];
				$this->output["result"] = $Group;
				$this->output["group"] = $group;
				$this->output["semester"] = $semester;
				$this->output["subjects"] = $subjects;
				$this->output["semesters"] = $_REQUEST["data"]['select_course']*2;
				$this->output["qual"] = $Group->getQuaGroup();
				$this->output["absolut"] = $Group->getAbsGroup();


			}
			break;
			
			case "lc": {
					header("Content-type: text/html; charset=windows-1251");
					$this->output["mode"] = "lc";
					
					//if($Engine->OperationAllowed(43, "folio_report.handle", $Auth->user_id, $Auth->usergroup_id)) {
					/*����� ����� � ���������------------------------------------------------------------*/
						//�������� ����������
						$exception_fac = array(51,41,21,20,19,10); //id ����������� �� ������ �����������
						$DB->SetTable("nsau_faculties");
						$DB->AddCondFS("is_active", "=", "1");
						$res = $DB->Select();
						$i=0;
						while ($row = $DB->FetchAssoc($res))
						{
							if (in_array($row['id'], $exception_fac))
							{
								//dratyti
							}
							else
							{
								$data[$i]=$row;
								$i++;
							}
						}
						$this->output['nsau_fac'] = $data;

						if(isset($_POST['show']))
						{
							//������������ �������� ��� ��������������
							$this->output['selected_fac'] = $_REQUEST["data"]['select_fac'];
							$this->output['selected_form'] = $_REQUEST["data"]['select_form'];
							$this->output['selected_course'] = $_REQUEST["data"]['select_course'];

							$DB->SetTable("nsau_groups");
							$DB->AddCondFS("hidden", "=", "0"); //�� �������
							$DB->AddCondFS("id_faculty", "=", $_REQUEST["data"]['select_fac']); //���������
							$DB->AddCondFS("form_education", "=", $_REQUEST["data"]['select_form']); //����� ��������
							$DB->AddCondFS("year", "=", $_REQUEST["data"]['select_course']); //����
							$DB->AddOrder('name');
							$res = $DB->Select();
							$i=0;
							while ($row = $DB->FetchAssoc($res))
							{
								$groups[$i]=$row;
								$i++;
							}
							if ($groups)
							{
								$this->output['nsau_groups'] = $groups;
							}
							else
							{
								$this->output['message'] = "<strong>������ �� ��������.</strong>";
							}
						}
					/*------------------------------------------------------------------------------------*/

					/*����� ��������� �� �������� ������--------------------------------------------------*/
					
							//������������ �������� ��� ��������������
							$this->output['selected_fac'] = $_REQUEST["data"]['select_fac'];
							$this->output['selected_form'] = $_REQUEST["data"]['select_form'];
							$this->output['selected_course'] = $_REQUEST["data"]['select_course'];

							$DB->SetTable("nsau_groups");
							$DB->AddCondFS("hidden", "=", "0"); //�� �������
							$DB->AddCondFS("id_faculty", "=", $_REQUEST["data"]['select_fac']); //���������
							$DB->AddCondFS("form_education", "=", $_REQUEST["data"]['select_form']); //����� ��������
							$DB->AddCondFS("year", "=", $_REQUEST["data"]['select_course']); //����
							$DB->AddOrder('name');
							$res = $DB->Select();
							$i=0;
							while ($row = $DB->FetchAssoc($res))
							{
								$groups[$i]=$row;
								$i++;
							}
							if ($groups)
							{
								$this->output['nsau_groups'] = $groups;
							}

							
								$DB->SetTable("nsau_groups");
								$DB->AddCondFS("id", "=", $_REQUEST["data"]['select_group']);
								$res = $DB->Select();
								$res_gr = $DB->FetchAssoc($res);
								$gr_id = $res_gr['id'];
								$gr_name = $res_gr['name'];
							

							//�������� ���������� ������
							if ($gr_name && $gr_id)
							{
								$this->output['selected_group'] = $gr_name; //�������� ������
								$this->output['selected_group_id'] = $gr_id; //�������� ������

								$courator = $DB->SetTable('nsau_groups_curators')
									->AddCondFS('group_id', '=', $gr_id)
									->SelectArray(1);
								$teacher = $DB->SetTable('nsau_teachers')
									->AddCondFS('id', '=', $courator['curator_id'])
									->SelectArray(1);
								$people = $DB->SetTable('nsau_people')
									->AddCondFS('id', '=', $teacher['people_id'])
									->SelectArray(1);	

								if(!empty($people)) {
									$this->output['courator']['id'] = $people['id'];
									$this->output['courator']['name'] = "{$people['last_name']} {$people['name']} {$people['patronymic']}";									
								} 

								$DB->SetTable("nsau_people");
								$DB->AddCondFS("id_group", "=", $gr_id);								
								$DB->AddOrder('last_name');
								$res = $DB->Select();
								$i=0;
								while ($row = $DB->FetchAssoc($res))
								{
									$st[$i]=$row;
									$i++;
								}
								$i=0;

								if (isset($st))
								{
									foreach ($st as $key)
									{
										$result=" ";
										$nir=0;
										$vr=0;
										$vkr=0;
										$photo=0;
										$year=0;
										$email=0;

										//����� �� � ���------------------------------------
										$DB->SetTable("nsau_folio");
										$DB->AddCondFS("user_id", "=", $key['id']);
										$res = $DB->Select();
										while ($row = $DB->FetchAssoc($res))
										{
											switch ($row['cat_folio'])
											{
												case '0':
													$nir++;
													break;

												case '1':
													$vr++;
													break;
											}
										}
										//--------------------------------------------------

										//����� ��� � ������ �����--------------------------
										$auth_id=$Auth->GetUserData(NULL,NULL,$key['id']);
										$DB->SetTable("nsau_students_works");
										$DB->AddCondFS("student_id", "=", $auth_id);
										//$DB->AddCondFS("work_type", "=", '32'); //���
										$ress = $DB->Select();
										while ($roww = $DB->FetchAssoc($ress))
										{
											if ($roww)
											{
												$vkr++;
											}
										}
										//--------------------------------------------------



										//����� ����������� --------------------------
										$comp = 0;
										$all_info = $this->get_all_info($Auth->GetUserData(NULL,NULL,$key['id']));
						                $DB->SetTable("nsau_students_progress");
						                $DB->AddCondFS("people_id", "=", $all_info['people_id']);
                						$res = $DB->Select();
                						while($row = $DB->FetchAssoc($res)) 
                						{
                    						$DB->SetTable("nsau_students_comp");
						                    $DB->AddCondFS("spec_id", "=", $all_info['spec_id']);

						                    if ($all_info['profile_group']) 
						                    {
						                        $DB->AddCondFS("profile_id", "=", $all_info['profile_group']);
						                    }



						                    if ($all_info['id_faculty']=="9") 
						                    {
						                        $DB->AddCondFS("fac", "=", $all_info['id_faculty']);
						                    }



						                    $DB->AddCondFS("year", "=", $all_info['year']);
						                    $res2 = $DB->Select(1);
						                    $in_comp = $DB->FetchAssoc($res2);

						                    if ($in_comp['comp'])
						                    {
						                        // $fail = array("-", "--", "�� ������", "�� ���� �", "���", "�� �������", "�� ������", "�� �������");
						                        // if (in_array($row['value'], $fail)) 
						                        // {
						                        //     $status = "0";
						                        // }
						                        // else
						                        // {
						                        //     $status = "1";
						                        // }
						                        $comp++;
						                        
						                        //$result[ $row['subject'] ] = array('comp' => $in_comp['comp'],
						                        	//'status' => $status
						                        //);
						                    }
						                }
										//--------------------------------------------------
										//
										//
										//����� ���������� �������--------------------------
										$DB->SetTable("nsau_people");	
										$DB->AddCondFS("id", "=", $key['id']);
										$ress = $DB->Select();
										while ($roww = $DB->FetchAssoc($ress))
										{
											if (!$roww['photo']==NULL) //����
											{
												$photo=1;
											}
											if ($roww['year']>0000) //��� �����������
											{
												$year=1;
											}
										}

										$DB->SetTable("auth_users");
										$DB->AddCondFS("id", "=", $auth_id);
										$ress = $DB->Select();
										while ($roww = $DB->FetchAssoc($ress))
										{
											if (!$roww['email']==NULL) //����
											{
												$email=1;
											}
										}
										//--------------------------------------------------

										//������� ��� �����
										if ($nir>0)
										{
											$result .= "<img src='/themes/images/ok.png'/>��� (".$nir.") &nbsp &nbsp";
										}
										else
										{
											$result .= "<img width='14' height='14' src='/themes/images/delete_icon.png'/>��� &nbsp &nbsp";
										}
										//������� �� �����
										if ($vr>0)
										{
											$result .= "<img src='/themes/images/ok.png'/>�� (".$vr.") &nbsp &nbsp";
										}
										else
										{
											$result .= "<img width='14' height='14' src='/themes/images/delete_icon.png'/>�� &nbsp &nbsp";
										}
										//������� ��� �����
										if ($vkr>0)
										{
											$result .= "<img src='/themes/images/ok.png'/>�� (".$vkr.") &nbsp &nbsp";
										}
										else
										{
											$result .= "<img width='14' height='14' src='/themes/images/delete_icon.png'/>�� &nbsp &nbsp";
										}



										//������� ������ �������
										if ($photo>0 && $year>0 && $email>0)
										{
											$result .= "<img src='/themes/images/ok.png'/>�� &nbsp &nbsp"; //�� ���������
										}
										else
										{
											$result .= "<img width='14' height='14' src='/themes/images/delete_icon.png'  style='cursor: pointer;' title=' ";
											if ($photo==0)
											{
												$result .= "[����������� ���� �������] ";
											}
											if ($year==0)
											{
												$result .= "[�� ������ ��� �����������] ";
											}
											if ($email==0)
											{
												$result .= "[�� ������ Email]";
											}
											$result .= "'>��";
										}
																				//������� �����������
																				//if ($Auth->usergroup_id==1)
										{						
										if ($comp>0)
										{
											$result .= "<img src='/themes/images/ok.png'/>�� (".$comp.")&nbsp &nbsp";
										}
										else
										{
											$result .= "<img width='14' height='14' src='/themes/images/delete_icon.png'/>�� &nbsp &nbsp";
										}
									}

										//���������� ������
										$out[$i] = "<td><a href='/people/".$key['id']."'target='_blank'>".$key['last_name']." ".$key['name']." ".$key['patronymic']."</a></td><td> ".$result."</td>";
										$i++;

									}
								}
								else
								{
									$this->output['message'] = "<strong>������ ������ ".$gr_name." �� ��������.</strong>";
								}
							}
							else
							{
								$this->output['message'] = "<strong>������ ".$_REQUEST['group_for_search']." �� ��������.</strong>";
							}

							$this->output['nsau_st'] = $out;

						
					//}				
			}
			break;
			/*����� ������ ��������� � ����������� �� ����������*/
			case 'folio_report':
				{
					$this->output["scripts_mode"] = "folio_report";
					//if($Engine->OperationAllowed(43, "folio_report.handle", $Auth->user_id, $Auth->usergroup_id)) {
					/*����� ����� � ���������------------------------------------------------------------*/
						//�������� ����������
						$exception_fac = array(51,41,21,20,19,10); //id ����������� �� ������ �����������
						$DB->SetTable("nsau_faculties");
						$DB->AddCondFS("is_active", "=", "1");
						$res = $DB->Select();
						$i=0;
						while ($row = $DB->FetchAssoc($res))
						{
							if (in_array($row['id'], $exception_fac))
							{
								//dratyti    dratyi

							}
							else
							{
								$data[$i]=$row;
								$i++;
							}
						}
						$this->output['nsau_fac'] = $data;

						if(isset($_POST['show']))
						{
							//������������ �������� ��� ��������������
							$this->output['selected_fac'] = $_POST['select_fac'];
							$this->output['selected_form'] = $_POST['select_form'];
							$this->output['selected_course'] = $_POST['select_course'];

							$DB->SetTable("nsau_groups");
							$DB->AddCondFS("hidden", "=", "0"); //�� �������
							$DB->AddCondFS("id_faculty", "=", $_POST['select_fac']); //���������
							$DB->AddCondFS("form_education", "=", $_POST['select_form']); //����� ��������
							$DB->AddCondFS("year", "=", $_POST['select_course']); //����
							$DB->AddOrder('name');
							$res = $DB->Select();
							$i=0;
							while ($row = $DB->FetchAssoc($res))
							{
								$groups[$i]=$row;
								$i++;
							}
							if ($groups)
							{
								$this->output['nsau_groups'] = $groups;
							}
							else
							{
								$this->output['message'] = "<strong>������ �� ��������.</strong>";
							}
						}
					/*------------------------------------------------------------------------------------*/

					/*����� ��������� �� �������� ������--------------------------------------------------*/
						if(isset($_POST['show_st']) || isset($_POST['looking_at_group']))
						{
							//������������ �������� ��� ��������������
							$this->output['selected_fac'] = $_POST['select_fac'];
							$this->output['selected_form'] = $_POST['select_form'];
							$this->output['selected_course'] = $_POST['select_course'];

							$DB->SetTable("nsau_groups");
							$DB->AddCondFS("hidden", "=", "0"); //�� �������
							$DB->AddCondFS("id_faculty", "=", $_POST['select_fac']); //���������
							$DB->AddCondFS("form_education", "=", $_POST['select_form']); //����� ��������
							$DB->AddCondFS("year", "=", $_POST['select_course']); //����
							$DB->AddOrder('name');
							$res = $DB->Select();
							$i=0;
							while ($row = $DB->FetchAssoc($res))
							{
								$groups[$i]=$row;
								$i++;
							}
							if ($groups)
							{
								$this->output['nsau_groups'] = $groups;
							}

							if (empty($_POST['group_for_search'])) //����� �� ��������� ������
							{

								$DB->SetTable("nsau_groups");
								$DB->AddCondFS("id", "=", $_POST['select_group']);


								$res = $DB->Select();
								$res_gr = $DB->FetchAssoc($res);
								$gr_id = $res_gr['id'];
								$gr_name = $res_gr['name'];
							}
							else //����� �� ������ ��������� �������
							{
								$DB->SetTable("nsau_groups");
								$DB->AddCondFS("name", "=", $_POST['group_for_search']); //�� �������
								$DB->AddCondFS("hidden", "=", "0"); //�� �������
								$res = $DB->Select();
								$res_gr = $DB->FetchAssoc($res);
								$gr_id=$res_gr['id'];
								$gr_name=$res_gr['name'];






								$this->output['selected_fac'] = $res_gr['id_faculty'];
								$this->output['selected_form'] = $res_gr['form_education'];
								$this->output['selected_course'] = $res_gr['year'];
								$this->output['selected_group_id'] = $res_gr['id'];

								$DB->SetTable("nsau_groups");
								$DB->AddCondFS("year", "=", $this->output['selected_course']);
								$DB->AddCondFS("id_faculty", "=", $this->output['selected_fac']);
								$DB->AddCondFS("form_education", "=", $this->output['selected_form']);
								$DB->AddOrder('name');
								$r = $DB->Select();
								while($row_gr = $DB->FetchAssoc($r)) {
									$this->output['nsau_groups'][] = $row_gr;
								}

							}

							//�������� ���������� ������
							if ($gr_name && $gr_id)
							{
								$this->output['selected_group'] = $gr_name; //�������� ������
								$this->output['selected_group_id'] = $gr_id; //�������� ������

								$courator = $DB->SetTable('nsau_groups_curators')
									->AddCondFS('group_id', '=', $gr_id)
									->SelectArray(1);
								$teacher = $DB->SetTable('nsau_teachers')
									->AddCondFS('id', '=', $courator['curator_id'])
									->SelectArray(1);
								$people = $DB->SetTable('nsau_people')
									->AddCondFS('id', '=', $teacher['people_id'])
									->SelectArray(1);	

								if(!empty($people)) {
									$this->output['courator']['id'] = $people['id'];
									$this->output['courator']['name'] = "{$people['last_name']} {$people['name']} {$people['patronymic']}";									
								} 
								
								$DB->SetTable("nsau_people");
								$DB->AddCondFS("id_group", "=", $gr_id);
								$DB->AddOrder('last_name');
								$res = $DB->Select();
								$i=0;
								while ($row = $DB->FetchAssoc($res))
								{
									$st[$i]=$row;
									$i++;
								}
								$i=0;

								if (isset($st))
								{
									foreach ($st as $key)
									{
										$result=" ";
										$nir=0;
										$vr=0;
										$vkr=0;
										$photo=0;
										$year=0;
										$email=0;

										//����� �� � ���------------------------------------
										$DB->SetTable("nsau_folio");
										$DB->AddCondFS("user_id", "=", $key['id']);
										$res = $DB->Select();
										while ($row = $DB->FetchAssoc($res))
										{
											switch ($row['cat_folio'])
											{
												case '0':
													$nir++;
													break;

												case '1':
													$vr++;
													break;
											}
										}
										//--------------------------------------------------

										//����� ��� � ������ �����--------------------------
										$auth_id=$Auth->GetUserData(NULL,NULL,$key['id']);
										$DB->SetTable("nsau_students_works");
										$DB->AddCondFS("student_id", "=", $auth_id);
										//$DB->AddCondFS("work_type", "=", '32'); //���
										$ress = $DB->Select();
										while ($roww = $DB->FetchAssoc($ress))
										{
											if ($roww)
											{
												$vkr++;
											}
										}
										//--------------------------------------------------




										//����� ����������� --------------------------
										$comp = 0;
										$all_info = $this->get_all_info($Auth->GetUserData(NULL,NULL,$key['id']));
						                $DB->SetTable("nsau_students_progress");
						                $DB->AddCondFS("people_id", "=", $all_info['people_id']);
                						$res = $DB->Select();
                						while($row = $DB->FetchAssoc($res)) 
                						{
                    						$DB->SetTable("nsau_students_comp");
						                    $DB->AddCondFS("spec_id", "=", $all_info['spec_id']);

						                    if ($all_info['profile_group']) 
						                    {
						                        $DB->AddCondFS("profile_id", "=", $all_info['profile_group']);
						                    }


											if ($all_info['id_faculty']=="9") 
						                    {
						                        $DB->AddCondFS("fac", "=", $all_info['id_faculty']);
						                    }
                    
						                    $DB->AddCondFS("year", "=", $all_info['year']);
						                    $res2 = $DB->Select(1);
						                    $in_comp = $DB->FetchAssoc($res2);

						                    if ($in_comp['comp'])
						                    {
						                        // $fail = array("-", "--", "�� ������", "�� ���� �", "���", "�� �������", "�� ������", "�� �������");
						                        // if (in_array($row['value'], $fail)) 
						                        // {
						                        //     $status = "0";
						                        // }
						                        // else
						                        // {
						                        //     $status = "1";
						                        // }
						                        $comp++;
						                        
						                        //$result[ $row['subject'] ] = array('comp' => $in_comp['comp'],
						                        	//'status' => $status
						                        //);
						                    }
						                }
										//--------------------------------------------------




										//����� ���������� �������--------------------------
										$DB->SetTable("nsau_people");
										$DB->AddCondFS("id", "=", $key['id']);
										$ress = $DB->Select();
										while ($roww = $DB->FetchAssoc($ress))
										{
											if (!$roww['photo']==NULL) //����
											{
												$photo=1;
											}
											if ($roww['year']>0000) //��� �����������
											{
												$year=1;
											}
										}

										$DB->SetTable("auth_users");
										$DB->AddCondFS("id", "=", $auth_id);
										$ress = $DB->Select();
										while ($roww = $DB->FetchAssoc($ress))
										{
											if (!$roww['email']==NULL) //����
											{
												$email=1;
												$mail=$roww['email'];
											}
										}
										//--------------------------------------------------

										//������� ��� �����
										if ($nir>0)
										{
											$result .= "<img src='/themes/images/ok.png'/>��� (".$nir.") &nbsp &nbsp";
											$nir_count++;
										}
										else
										{
											$result .= "<img width='14' height='14' src='/themes/images/delete_icon.png'/>��� &nbsp &nbsp";
										}
										//������� �� �����
										if ($vr>0)
										{
											$result .= "<img src='/themes/images/ok.png'/>�� (".$vr.") &nbsp &nbsp";
											$vr_count++;
										}
										else
										{
											$result .= "<img width='14' height='14' src='/themes/images/delete_icon.png'/>�� &nbsp &nbsp";
										}
										//������� ��� �����
										if ($vkr>0)
										{
											$result .= "<img src='/themes/images/ok.png'/>�� (".$vkr.") &nbsp &nbsp";
											$rs_count++;
										}
										else
										{
											$result .= "<img width='14' height='14' src='/themes/images/delete_icon.png'/>�� &nbsp &nbsp";
										}
										//������� ������ �������
										if ($photo>0 && $year>0 && $email>0)
										{
											$result .= "<img src='/themes/images/ok.png'/>�� &nbsp &nbsp"; //�� ���������
											$ld_count++;
										}
										else
										{
											$result .= "<img width='14' height='14' src='/themes/images/delete_icon.png'  style='cursor: pointer;' title=' ";
											if ($photo==0)
											{
												$result .= "[����������� ���� �������] ";
											}
											if ($year==0)
											{
												$result .= "[�� ������ ��� �����������] ";
											}
											if ($email==0)
											{
												$result .= "[�� ������ Email]";
											}
											$result .= "'>�� &nbsp &nbsp";
										}
										//������� �������� ����
										if ($email>0)
										{
											$result .= "Email: &nbsp".$mail." &nbsp &nbsp"; 
											$ld_count++;
										}

										//������� �����������
										//if ($Auth->usergroup_id==1)
										{
											if ($comp>0)
											{
												$result .= "<img src='/themes/images/ok.png'/>�� (".$comp.")&nbsp &nbsp";
											}
											else
											{
												$result .= "<img width='14' height='14' src='/themes/images/delete_icon.png'/>�� &nbsp &nbsp";
											}
										}
										//���������� ������
										$out[$i] = "<td><a href='/people/".$key['id']."'target='_blank'>".$key['last_name']." ".$key['name']." ".$key['patronymic']."</a></td><td> ".$result."</td>";
										$i++;

									}
								}
								else
								{
									$this->output['message'] = "<strong>������ ������ ".$gr_name." �� ��������.</strong>";
								}
							}
							else
							{
								$this->output['message'] = "<strong>������ ".$_POST['group_for_search']." �� ��������.</strong>";
							}
							$this->output['nsau_st'] = $out;

							$this->output['nir_count'] 	= !empty($nir_count) ? $nir_count : 0;
							$this->output['vr_count'] 	= !empty($vr_count) ? $vr_count : 0;
							$this->output['rs_count'] 	= !empty($rs_count) ? $rs_count : 0;
							$this->output['ld_count'] 	= !empty($ld_count) ? $ld_count : 0;
							;
						}
					//}
				}
			break;
		}
	}

	function get_all_info ($auth_id)
    {
        global $Engine, $Auth, $DB;

        $DB->SetTable("nsau_people");
        $DB->AddCondFS("user_id", "=", $auth_id);
        $res = $DB->Select(1);
        $in_people = $DB->FetchAssoc($res);
        

        $DB->SetTable("nsau_groups");
        $DB->AddCondFS("id", "=", $in_people['id_group']);
        $res = $DB->Select(1);
        $in_groups = $DB->FetchAssoc($res); 

        $DB->SetTable("nsau_faculties");
        $DB->AddCondFS("id", "=", $in_groups['id_faculty']);
        $res = $DB->Select(1);
        $in_faculty = $DB->FetchAssoc($res);      

        $DB->SetTable("nsau_specialities");
        $DB->AddCondFS("code", "=", $in_groups['specialities_code']);
        $DB->AddCondFS("id_faculty", "=", $in_groups['id_faculty']);
        $res = $DB->Select(1);
        $in_special = $DB->FetchAssoc($res);  


        $current_user = array(
            'name' => $in_people['name'], 
            'last_name' => $in_people['last_name'],
            'patronymic' => $in_people['patronymic'],

            'year' => $in_people['year'],

            'id_group' => $in_people['id_group'],
            'name_group' => $in_groups['name'],
            'profile_group' => $in_groups['profile'],

            'course' => $in_groups['year'],

            'spec_code' => $in_groups['specialities_code'],
            'spec_id' => $in_special['id'],

            'id_faculty' => $in_groups['id_faculty'],
            'name_faculty' => $in_faculty['name'],
            'link_faculty' => $in_faculty['link'],

            'people_id' => $in_people['id'],

        );
        return $current_user;
    }


	/*���������� ID ������������*/
	function get_user_id($id)
	{
		global $DB;
		if(isset($id))
		{
			$DB->SetTable("nsau_people");
			$DB->AddCondFS("user_id", "=", $id);
			$DB->AddField("id", "user");
			$DB->Select(1);
			$row = $DB->FetchAssoc();
			return $row["user"];
		}
		else
		{
			return false;
		}
	}

	function Output()
	{
		return $this->output;
	}
}
?>
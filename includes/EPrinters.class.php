<?php
require_once INCLUDES . "/printers/" . "Item" . CLASS_EXT ;
require_once INCLUDES . "/printers/" . "Request" . CLASS_EXT ;
require_once INCLUDES . "/printers/" . "Comments" . CLASS_EXT ;
class EPrinters
{
	var $module_id;
	var $node_id;
	var $module_uri;
	var $privileges;
	var $output;

	var $mode;
	var $department_id;
	var $db_prefix;
	var $uri_params;
	
	
	var $printers_requests;
	var $printers_managment;


	var $error = false;
	
	function EPrinters($global_params, $params, $module_id, $node_id = NULL, $module_uri = NULL, $additional = NULL)
	{
		global $DB, $Engine, $Auth;
// sleep(1);
		$this->module_id = $module_id;
		$this->node_id = $node_id;
		$this->module_uri = $module_uri;
		$this->additional = $additional;
		$this->output = array();
		$this->output["messages"] = array("good" => array(), "bad" => array());
		$this->output["module_id"] = $this->module_id;

		$parts = explode(";", $global_params);
		$this->db_prefix = $parts[0];
		$parts2 = explode(";", $params);
		$this->uri_params = explode("/", $module_uri);
		$this->output["printers_managment"] = $this->printers_managment = $Engine->OperationAllowed($this->module_id, "printers.managment", -1, $Auth->usergroup_id);
		$this->printers_requests = $Engine->OperationAllowed($this->module_id, "printers.requests", -1, $Auth->usergroup_id);
		
		if(!$this->printers_requests) break;
		
		switch ($this->mode = $parts2[0])
		{
			case "main": {
				$this->output["mode"] = "main";
				$this->output["scripts_mode"] = "main";				
			}
			break;
			case "request_new": {
				$this->output["mode"] = "show_requests";
				$this->output["scripts_mode"] = "show_requests";
				$Request = new Request($this->module_id);
				$this->output["requests"] = $Request->getRequests("�����");
				$this->output["messages"] = $Request->getResponse();
			}
			break;
			case "request_open": {
				$this->output["mode"] = "show_requests";
				$this->output["scripts_mode"] = "show_requests";
				$Request = new Request($this->module_id);
				$this->output["requests"] = $Request->getRequests("� ������");
				$this->output["messages"] = $Request->getResponse();
			}
			break;
			case "request_close": {
				$this->output["mode"] = "show_requests";
				$this->output["scripts_mode"] = "show_requests";
				$Request = new Request($this->module_id);
				$this->output["requests"] = $Request->getRequests("���������", null, true);
				$this->output["messages"] = $Request->getResponse();
			}
			break;
			case "ajax_change_page": {
				$this->output["mode"] = "show_requests";
				$this->output["scripts_mode"] = "show_requests";
				$Request = new Request($this->module_id);
				
				
				$this->output["requests"] = $Request->getRequests(iconv("utf-8", "Windows-1251", $_REQUEST['data']["status"]),
					$_REQUEST['data']["page"],
					(iconv("utf-8", "Windows-1251", $_REQUEST['data']["status"])=="���������" ? true : false));
				$this->output["messages"] = $Request->getResponse();
				if(($_REQUEST['data']["page"]>1) && !empty($this->output["messages"]["bad"][0])) {
					$this->output["requests"] = $Request->getRequests(iconv("utf-8", "Windows-1251", $_REQUEST['data']["status"]), --$_REQUEST['data']["page"]);
					unset($this->output["messages"]["bad"][0]);
				}	
			}
			break;
			case "register": {
				$this->output["mode"] = "register";
				$this->output["scripts_mode"] = "register";
				$this->output["register_data"] = $this->loadRegisterData();
			}
			break;
			case "settings": {
				if(!$this->printers_managment) break;
				$this->output["mode"] = "settings";
				$this->output["scripts_mode"] = "settings";
				$this->output["edit_units"] = $this->loadUnits(0);	
				$this->output["register_data"] = $this->loadRegisterData();				
			}
			break;
			
			case "settings_ajax_load_edit_device_form": {
				if(!$this->printers_managment) break;
				$this->output["mode"] = "settings_ajax_load_edit_device_form";
				$this->output["register_data"] = $this->loadRegisterData();				
				$this->output["device"] = $this->loadDeviceData(intval($_REQUEST["id"]));				
			}
			break;
			case "settings_ajax_load_add_device_form": {
				if(!$this->printers_managment) break;
				$this->output["mode"] = "settings_ajax_load_add_device_form";
				$this->output["register_data"] = $this->loadRegisterData();				
			}
			break;
			case "ajax_search_unit": {
				$this->output["mode"] = "ajax_search_unit";
				$json = array();
				$DB->SetTable("printers_units");
				$DB->AddCondFS("name", "LIKE", "%".iconv("utf-8", "Windows-1251", $_REQUEST['data']["s"])."%");
				$res = $DB->Select();
				while($row = $DB->FetchAssoc($res)) {
					$DB->SetTable("printers_units");
					$DB->AddCondFS("pid", "=", $row["id"]);
					$s = $DB->FetchAssoc($DB->Select(1));
					if(empty($s)) {
						$row["full_path"] = $this->getUnitParents($row["pid"], true) . " / ".$row["name"];
						if($row["id"]) $row["name"] = $row["name"];
						$json[] = $row;
					}
				}
				header("Cache-Control: no-cache, must-revalidate");
				header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
				header("Pragma: no-cache");  
				$this->output["json"] = $json;	
			}
			break;
			case "ajax_register_device": {
				$this->output["mode"] = "ajax_register_device";
				$json = array();
				
				$item  = new Item($this->module_id);
				$item->addItem($_REQUEST['data']);
				$json[] = $item->getResponse();
				
				header("Cache-Control: no-cache, must-revalidate");
				header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
				header("Pragma: no-cache");  
				$this->output["json"] = $json;	
			}
			break;
			case "ajax_select_device": {
				$this->output["mode"] = "ajax_select_device";
				$DB->SetTable("printers_devices");
				$DB->AddCondFS("manufacturer_id", "=", intval($_REQUEST["data"]["id"]));
				$DB->AddOrder("name");
				$res = $DB->Select();
				while($row = $DB->FetchAssoc($res)) {
					$this->output["devices"][$row["id"]] = $row["name"];	
				}			
			}
			break;
			case "ajax_view_request": {
				$this->output["mode"] = "view_form";
				$Request = new Request($this->module_id);
				$this->output["request"] = $Request->getRequest($_REQUEST['data']["id"]);
				$this->output["current_page"] = $_REQUEST['data']["current_page"];
			}
			break;
			case "ajax_load_register_data": {
				if(!$this->printers_managment) break;
				$this->output["mode"] = "ajax_load_register_data";
				$Request = new Request($this->module_id);
				$this->output["request"] = $Request->getRequest($_REQUEST['data']["request_id"]);
				$this->output["devices"] = $this->loadDevices();
				$this->output["register_data"] = $this->loadRegisterData();
			}
			break;
			case "ajax_add_comment": {				
				if(!$this->printers_managment) break;
				$this->output["mode"] = "ajax_add_comment";
				$Comments = new Comments($this->module_id);
				$Comments->addComment($_REQUEST['data']);
				$this->output["comments"] = $Comments->getComments($_REQUEST['data']["request_id"]);
			}
			break;
			case "ajax_save_request": {
				if(!$this->printers_managment) break;
				$json = array();
				$Request = new Request($this->module_id);
				$Request->editRequest($_REQUEST["data"]);
				if($_REQUEST["register_data"]) {
					$Item = new Item($this->module_id);
					$Item->editItem($_REQUEST["register_data"]);
					$json[] = $Item->getResponse();
				}			
				header("Cache-Control: no-cache, must-revalidate");
				header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
				header("Pragma: no-cache");  
				$this->output["json"] = $json;	
			}
			break;
			case "ajax_delete_requests": {
				if(!$this->printers_managment) break;
				$this->output["mode"] = "show_requests";
				$Request = new Request($this->module_id);
				foreach($_REQUEST["data"]["id"] as $id) {
					$Request->deleteRequest($id);
				}				
				$this->output["requests"] = $Request->getRequests(iconv("utf-8", "Windows-1251", $_REQUEST['position']["status"]), $_REQUEST['position']["page"]);
				$this->output["messages"] = $Request->getResponse();
			}
			break;
			case "ajax_requests_change_status": {
				if(!$this->printers_managment) break;
				$this->output["mode"] = "show_requests";
				$Request = new Request($this->module_id);
				foreach($_REQUEST["data"]["id"] as $id) {
					$req["id"] = $id;
					$req["status"] = $_REQUEST["data"]["status"];
					$Request->editRequest($req);
				}				
				$this->output["requests"] = $Request->getRequests(iconv("utf-8", "Windows-1251", $_REQUEST['position']["status"]), $_REQUEST['position']["page"]);
				$this->output["messages"] = $Request->getResponse();
				if(($_REQUEST['position']["page"]>1) && !empty($this->output["messages"]["bad"][0])) {
					$this->output["requests"] = $Request->getRequests(iconv("utf-8", "Windows-1251", $_REQUEST['position']["status"]), --$_REQUEST['position']["page"]);
					unset($this->output["messages"]["bad"][0]);
				}
			}
			break;
			case "ajax_search_device": {
				$this->output["mode"] = "ajax_search_device";
				$json = array();
				$Item = new Item($this->module_id);
				$json = $Item->searchItem($_REQUEST['data']["s"]);
				header("Cache-Control: no-cache, must-revalidate");
				header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
				header("Pragma: no-cache");  
				$this->output["json"] = $json;
			}
			break;
			
			case "ajax_get_device_info": {
				$this->output["mode"] = "ajax_get_device_info";
				$Item = new Item($this->module_id);
				$this->output["info"] = $Item->getItemInfo($_REQUEST['data']["id"]);
			}
			break;
			
			case "add_request": {				
				$this->output["mode"] = "add_request";
			}
			break;		
			
			case "reports": {				
				$this->output["mode"] = "reports";
			}
			break;	
			
			case "ajax_add_request": {				
				$this->output["mode"] = "ajax_add_request";
				$json = array();
				
				$Request = new Request($this->module_id);
				$Request->addRequest($_REQUEST['data']);
				$json[] = $Request->getResponse();
				
				header("Cache-Control: no-cache, must-revalidate");
				header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
				header("Pragma: no-cache");  
				$this->output["json"] = $json;	
			}
			break;
			
			case "settings_ajax_add_unit": {
				if(!$this->printers_managment) break;				
				$this->output["mode"] = "settings_ajax_add_unit";	
			}
			break;	
			
			case "settings_ajax_add_unit_save": {		
				if(!$this->printers_managment) break;			
				$this->output["mode"] = "settings";	
				$this->addUnit(intval($_REQUEST["data"]["pid"]),
														iconv("utf-8", "Windows-1251", $_REQUEST["data"]["name"]),
														iconv("utf-8", "Windows-1251", $_REQUEST["data"]["contacts"]));
				$this->output["edit_units"] = $this->loadUnits(0);		
			}
			break;	
			
			case "settings_ajax_edit_unit": {	
				if(!$this->printers_managment) break;
				$this->output["mode"] = "settings_ajax_edit_unit";
				$this->output["edit_info"] = $this->loadUnitInfo(intval($_REQUEST["id"]));
			}
			break;				
			
			case "settings_ajax_edit_unit_save": {
				if(!$this->printers_managment) break;				
				$this->output["mode"] = "settings";
				$this->editUnitInfo(intval($_REQUEST["data"]["id"]),
														iconv("utf-8", "Windows-1251", $_REQUEST["data"]["name"]),
														iconv("utf-8", "Windows-1251", $_REQUEST["data"]["contacts"]));
				$this->output["edit_units"] = $this->loadUnits(0);		
			}
			break;	
			
			case "settings_ajax_delete_unit": {		
				if(!$this->printers_managment) break;			
				$this->output["mode"] = "settings_ajax_delete_unit";
				$this->output["edit_info"] = $this->recDeleteUnit(intval($_REQUEST["id"]));
			}
			break;	
			case "settings_ajax_edit_device_save": {
				if(!$this->printers_managment) break;				
				$this->output["mode"] = "ajax_add_request";
				$json = array();		
				$json[] = $this->editDevice($_REQUEST["data"]);			
				header("Cache-Control: no-cache, must-revalidate");
				header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
				header("Pragma: no-cache");  
				$this->output["json"] = $json;	
			}
			break;
			case "settings_ajax_add_device_save": {		
				if(!$this->printers_managment) break;			
				$this->output["mode"] = "ajax_add_request";
				$json = array();		
				$json[] = $this->addDevice($_REQUEST["data"]);			
				header("Cache-Control: no-cache, must-revalidate");
				header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
				header("Pragma: no-cache");  
				$this->output["json"] = $json;	
			}
			break;
			case "settings_ajax_delete_device": {		
				if(!$this->printers_managment) break;			
				$this->output["mode"] = "ajax_add_request";
				$json = array();		
				$json[] = $this->deleteDevice($_REQUEST["id"]);			
				header("Cache-Control: no-cache, must-revalidate");
				header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
				header("Pragma: no-cache");  
				$this->output["json"] = $json;	
			}
			break;
			case "report": {
				if(!$this->printers_managment) break;
				$this->output["mode"] = "report";
				$this->output["report"] = $this->getReport($_POST["date_begin"], $_POST["date_end"]);
				$this->downloadXlsHeaders("printers_report_".$_POST["date_begin"]."_".$_POST["date_end"]);
			}
			break;
			
		}
	}
	
	function deleteDevice($id) {
		global $DB;
		$DB->SetTable("printers_register");
		$DB->AddCondFS("device_id", "=", $id);
		$r = $DB->FetchAssoc($DB->Select(1));
		if(!empty($r)) {
			$msg["bad"][] = "������� ���������������. �������� ����������.";
			return $msg;
		}
		$DB->SetTable("printers_devices");
		$DB->AddCondFS("id", "=", intval($id));
		$DB->Delete();
		$msg["good"][] = "deleted";
		return $msg;
	}
	
	function editDevice($arr) {
		global $DB;
		$DB->SetTable("printers_devices");
		$DB->AddCondFS("name", "=", $arr["name"]);
		$r = $DB->FetchAssoc($DB->Select(1));
		if(!empty($r) && $r["id"]!=$arr["id"]) {
			$msg["bad"][] = "����� ������� ��� ���� � ����.";
			return $msg;
		}
		$DB->SetTable("printers_devices");
		$DB->AddCondFS("id", "=", $arr["id"]);
		$DB->AddValue("name", iconv("utf-8", "Windows-1251", $arr["name"]));
		$DB->AddValue("manufacturer_id", $arr["manufacturer_id"]);
		$DB->Update();
		$msg["good"][] = "updated";
		return $msg;
	}

	function addDevice($arr) {
		global $DB;
		$DB->SetTable("printers_devices");
		$DB->AddCondFS("name", "=", $arr["name"]);
		$r = $DB->FetchAssoc($DB->Select(1));
		if(!empty($r)) {
			$msg["bad"][] = "����� ������� ��� ���� � ����.";
			return $msg;
		}
		$DB->SetTable("printers_devices");
		$DB->AddValue("name", iconv("utf-8", "Windows-1251", $arr["name"]));
		$DB->AddValue("manufacturer_id", $arr["manufacturer_id"]);
		$DB->Insert();
		$msg["good"][] = "updated";
		return $msg;
	}
	
	function loadDeviceData($id) {
		global $DB;
		$DB->SetTable("printers_devices");
		$DB->AddCondFS("id", "=", $id);
		$r = $DB->FetchAssoc($DB->Select(1));
		return $r;
	}
	
	function loadDevices() {
		global $DB;
		$DB->SetTable("cartrige_manufacturers");
		$mres = $DB->Select();
		while($m = $DB->FetchAssoc($mres)) { 
			$man[$m["id"]] = $m["name"];
		}
		$DB->SetTable("printers_devices");
		$res = $DB->Select();
		while($row = $DB->FetchAssoc($res)) {
			$result[$row["id"]] = $man[$row["manufacturer_id"]]." ".$row["name"];
		}
		return $result;
	}
	
	function downloadXlsHeaders($filename) {
		header('Content-Type: text/html; charset=windows-1251');
		header('P3P: CP="NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM"');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0', FALSE);
		header('Pragma: no-cache');
		header('Content-transfer-encoding: binary');
		header('Content-Disposition: attachment; filename='.$filename.'.xls');
		header('Content-Type: application/x-unknown');		
	}	
	
	function getReport($begin, $end) {
		global $DB;
		$Item = new Item($this->module_id);
		$Comments = new Comments($this->module_id);
		$DB->SetTable("printers_requests");
		$DB->AddCondFS("date_open", ">=", $begin);
		$DB->AddCondFS("date_open", "<=", $end);
		$res = $DB->Select();
		while($row = $DB->FetchAssoc($res)) {
			$row["comments"] = $Comments->getComments($row["id"]);
			$row["device_info"] = $Item->getItemInfo($row["register_id"]);
			$result[$row["id"]] = $row;
		}
		return $result;
	}
	
	function recDeleteUnit($id) {
		global $DB;
		$DB->SetTable("printers_units");
		$DB->AddCondFS("pid", "=", $id);
		$pu = $DB->FetchAssoc($DB->Select(1));
		if(empty($pu)) {
			$unit = $this->loadUnitInfo($id);
			$DB->SetTable("printers_register");
			$DB->AddCondFS("unit_id", "=", $id);
			$ures = $DB->Select();
			while($urow = $DB->FetchAssoc($ures)) {
				$DB->SetTable("printers_register");
				$DB->AddCondFS("unit_id", "=", $id);
				$DB->AddValue("unit_id", $unit["pid"]);
				$DB->Update();
			}
			
			$DB->SetTable("printers_units");
			$DB->AddCondFS("id", "=", $id);
			$DB->Delete();
		}
		// $DB->SetTable("printers_units");
		// $DB->AddCondFS("pid", "=", $id);
		// $res = $DB->Select();
		// while($su = $DB->FetchAssoc($res)) {
			// if($su) {
				// $this->recDeleteUnit($su["id"]);
			// }	
		// }
		return true;
	}
	
	function addUnit($pid, $name, $contacts) {
		global $DB;
		$DB->SetTable("printers_units");
		$DB->AddValue("name", $name);
		$DB->AddValue("contacts", $contacts);
		$DB->AddValue("pid", $pid);
		$DB->Insert();
		return true;
	}
	
	function editUnitInfo($id, $name, $contacts) {
		global $DB;
		$DB->SetTable("printers_units");
		$DB->AddCondFS("id", "=", $id);
		$DB->AddValue("name", $name);
		$DB->AddValue("contacts", $contacts);
		$DB->Update();
		return true;
	}
	
	function loadUnitInfo($id) {
		global $DB;
		$DB->SetTable("printers_units");
		$DB->AddCondFS("id", "=", $id);
		$su = $DB->FetchAssoc($DB->Select(1));
		if($su) {
			return $su;
		}
		else return false;
	}
	
	function loadUnits($pid) {
		global $DB;
		$DB->SetTable("printers_units");
		$DB->AddCondFS("pid", "=", $pid);
		$res = $DB->Select();
		while($row = $DB->FetchAssoc($res))
		{
			$DB->SetTable("printers_units");
			$DB->AddCondFS("pid", "=", $row["id"]);
			$su = $DB->FetchAssoc($DB->Select(1));
			if($su) {
				$row["subunits"] = $this->loadUnits($row["id"]);
			}
			$result[$row["id"]] = $row;
		}
		return $result;
	}
	
	function getUnitParents($id, $level = null)
	{
		global $DB;
		$DB->SetTable("printers_units");
		$DB->AddCondFS("id", "=", $id);
		$row = $DB->FetchAssoc($DB->Select(1));
		if($level) return $row["name"];
		if(!empty($row["pid"]) && ($row["pid"] != $row["id"])) 
		{
			$result = $this->getUnitParents($row["pid"]).">".$row["name"];		
		}
		else
		{
			$result = $result.$row["name"];
		}
		return $result;
	}
	
	function loadRegisterData() {
		global $DB;
		$DB->SetTable("cartrige_manufacturers");
		$mres = $DB->Select();
		while($mrow = $DB->FetchAssoc($mres)) {
			$result["manufacturers"][$mrow["id"]] = $mrow["name"];
		}
		
		$DB->SetTable("nsau_buildings");
		$mres = $DB->Select();
		while($mrow = $DB->FetchAssoc($mres)) {
			$result["buildings"][$mrow["id"]] = $mrow["name"]."(".$mrow["label"].")";
		}

		return $result;
	}
	
	function Output()	{
			return $this->output;
	}	
}


//�������� ���������
				// $DB->SetTable("cartrige_manufacturers");
				// $res = $DB->Select();
				// while($row = $DB->FetchAssoc($res)) {
					// $man[$row["name"]] = $row["id"];
				// }
				// set_time_limit(0);
				// $handle = @fopen(INCLUDES . "printers.csv", "r");
				// if ($handle) { 
					// while (($buffer = fgets($handle)) !== false) {
						// $parts = explode(";", $buffer);
						// $manuf = trim($parts[0]);
						// $name = trim($parts[1]);
						// $DB->SetTable("printers_devices");
						// $DB->AddValue("name", $name);
						// $DB->AddValue("manufacturer_id", $man[$manuf]);
						// $DB->Insert();
						// if(empty($man[$manuf])) {
							// echo $name."<br>";
						// }

					// }
				// }


//�������� �������������

				// set_time_limit(0);
				// $handle = @fopen(INCLUDES . "units.csv", "r");
				// if ($handle) { 
					// while (($buffer = fgets($handle)) !== false) {
						
						
						// $parts = explode(";", $buffer);
						
						// $u1 = trim($parts[0]);
						// $u2 = trim($parts[1]);
						// $u3 = trim($parts[2]);
						// $u4 = trim($parts[3]);
						
						// $DB->SetTable("printers_units");
						// $DB->AddCondFS("pid", "=", 0);
						// $DB->AddCondFS("name", "=", $u1);
						// $pui1 = $DB->FetchAssoc($DB->Select(1));
						
						// $DB->SetTable("printers_units");
						// $DB->AddCondFS("pid", "=", $pui1["id"]);
						// $DB->AddCondFS("name", "=", $u2);
						// $pui2 = $DB->FetchAssoc($DB->Select(1));		
						
						// $DB->SetTable("printers_units");
						// $DB->AddCondFS("pid", "=", $pui2["id"]);
						// $DB->AddCondFS("name", "=", $u3);
						// $pui3 = $DB->FetchAssoc($DB->Select(1));						

						
						
						// $DB->SetTable("printers_units");
						// $DB->AddCondFS("name", "=", $u4);
						// $DB->AddCondFS("pid", "=", $pui3["id"]);
						// $ur = $DB->FetchAssoc($DB->Select(1));
						// if(empty($ur["id"])) {
							// $DB->SetTable("printers_units");
							// $DB->AddValue("name", $u4);
							// $DB->AddValue("pid", $pui3["id"]);
							// $DB->Insert();
						// }
					// }
				// }
				
// �����������
				// $Item  = new Item($this->module_id);
				// set_time_limit(0);
				// $handle = @fopen(INCLUDES . "printers_register.csv", "r");
				// if ($handle) { 
					// while (($buffer = fgets($handle)) !== false) {
						// $parts = explode(";", $buffer);
						
				
						// $arr["auditorium"] = $parts[3]!="��� ������" ? trim($parts[3]) : null;
						
						// $people = explode(" ", trim($parts[4]));
						// $DB->SetTable("nsau_people");
						// $DB->AddCondFS("last_name", "=", $people[0]);
						// $DB->AddCondFS("name", "=", $people[1]);
						// $DB->AddCondFS("patronymic", "=", $people[2]);
						// $pres = $DB->Select();
						// if($DB->NumRows($pres) == 1) {
							// $prow = $DB->FetchAssoc($pres);
						// }						
						// $arr["responsible_person_id"] = $prow["id"];
						// $arr["responsible_person"] = $prow["id"] ? "" : trim($parts[4]);
						
						// $DB->SetTable("printers_devices");
						// $DB->AddCondFS("name", "=", trim($parts[6]));
						// $drow = $DB->FetchAssoc($DB->Select(1));
						
						// $DB->SetTable("cartrige_manufacturers");
						// $DB->AddCondFS("name", "=", trim($parts[5]));
						// $man = $DB->FetchAssoc($DB->Select(1));
						
						// if(empty($drow["id"])) {
							// $DB->SetTable("printers_devices");
							// $DB->AddValue("name", trim($parts[6]));
							// $DB->AddValue("manufacturer_id", $man["id"]);
							// $DB->Insert();
							// $drow["id"] = $DB->LastInsertId();
						// }
						
						// $arr["device_id"] = $drow["id"] ? $drow["id"] : "ERROR";
						
						// $arr["serial"] = $parts[7]!="��� ������" ? trim($parts[7]) : null;
						
						// $arr["inventory"] = trim($parts[8]);
						
						// $arr["date_start"] = trim($parts[9])=="��� ������" ? null : implode("-", array_reverse(explode(".", trim($parts[9]))));
						
						// $arr["start_price"] = trim($parts[10])=="��� ������" ? null : str_replace(" ", "", str_replace(",", ".", trim($parts[10])));
						
						// $DB->SetTable("printers_units");
						// $DB->AddCondFS("pid", "=", 0);
						// $DB->AddCondFS("name", "=", trim($parts[0]));
						// $u1 = $DB->FetchAssoc($DB->Select(1));
						// $u_id = $u1["id"];
						// if(trim($parts[1])) {
							// $DB->SetTable("printers_units");
							// $DB->AddCondFS("pid", "=", $u1["id"]);
							// $DB->AddCondFS("name", "=", trim($parts[1]));
							// $u2 = $DB->FetchAssoc($DB->Select(1));
							// $u_id = $u2["id"] ? $u2["id"] : $u_id;
						// }
						// if(trim($parts[2])) {
							// $DB->SetTable("printers_units");
							// $DB->AddCondFS("pid", "=", $u2["id"]);
							// $DB->AddCondFS("name", "=", trim($parts[2]));
							// $u3 = $DB->FetchAssoc($DB->Select(1));
							// $u_id = $u3["id"] ? $u3["id"] : $u_id;
						// }
						// $arr["unit_id"] = $u_id;
						// $Item->addItem($arr);
						// CF::Debug($arr);
						// break;
					// }					
				// }














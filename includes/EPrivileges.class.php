<?php

class EPrivileges
// version: 1.2
// date: 2014-01-28
{
	var $output;
	var $node_id;
	var $module_id;
	var $mode;
	var $db_prefix;


	function EPrivileges($global_params, $params, $module_id, $node_id, $module_uri)
	{
		global $Engine, $Auth, $DB;
		$this->node_id = $node_id;
		$this->module_id = $module_id;
		
		$this->output["mode"] = $this->mode = $params;
		$this->output["user_displayed_name"] = $Auth->user_displayed_name;
		$this->output["messages"]["good"] = $this->output["messages"]["bad"] = array();
		
		if (isset($_SESSION["messages"])) {
			$this->output["messages"] = $_SESSION["messages"];
			unset($_SESSION["messages"]);
		}
		
		/*if (!$global_params)
		{
			die("EPrivileges module error #1001: config file not specified.");
		}

		elseif (!$array = @parse_ini_file(INCLUDES . $global_params, true))
		{
			die("EPrivileges module error #1002: cannot process config file '$global_params'.");
		}*/
		
		if (ALLOW_AUTH && $Engine->OperationAllowed(0, "engine.privileges.handle", -1, $Auth->usergroup_id))
		{
			switch ($this->mode)
			{

				case "give_privileges":
					$this->output["scripts_mode"] = $this->output["mode"] = "give_privileges";
          $this->output["plugins"][] = "jquery.ui.autocomplete.min";
					if (!$module_uri) {
						
						$this->output["mode"] = "show_modules";
						$this->show_modules();
					}
					elseif (isset($_GET["delete_name"]) && isset($_GET["delete_module"])) {
						$this->delete_privilege();
					}
					else {
						$module_uri = explode("/", $module_uri);
						$this->output["scripts_mode"] = $this->output["mode"] = "privileges_form";
            
            $this->output['current_module_id'] = $this->module_id;
						
						
						if (isset($_POST[$this->node_id]["give_privileges"]["module_id"]))
							$this->insert_privilege();
						
						$this->show_form($module_uri[0]);
						//$this->show_element($module_uri[0]);
					
						$this->show_privileges($module_uri[0]);


						
						/*echo "<pre>111";
		print_r($this->output["scripts_mode"]);
		echo "</pre>";*/
					}
					
					break;
				case "group_privileges":
					$module_uri = explode("/", $module_uri);
if (isset($_GET["delete_name"]) && isset($_GET["delete_module"])) {
						$this->delete_privilege();
					}
					if($module_uri[0] == "edit") {
						 $this->output["scripts_mode"] = $this->output["mode"] = "privileges_form";
						$this->show_privileges(NULL, $module_uri[1]);
					}
					break;
		        case "ajax_autocomplete_element":
				case "ajax_get_elements":
					$this->output["scripts_mode"] = $this->output["mode"] = "ajax_get_elements"; 
          
          $this->output["elements"] = array();
          if($this->mode != "ajax_autocomplete_element") $this->output["elements"][] = array('id' => -1, 'name'  => !AT_HOME ? '��� ��������' : CF::Win2Utf('��� ��������'));
              
					$DB->SetTable("engine_operations");
					//$DB->AddField("operation_name");
					//$DB->AddField("comment");
					$DB->AddField("table_name");
			        $DB->AddField("name_field");
					$DB->AddCondFS("module_id", "=", $_REQUEST["data"]["module"]);
					$DB->AddCondFS("operation_name", "=", $_REQUEST["data"]["operation"]);
          
					$res = $DB->Select();
					while ($row = $DB->FetchAssoc($res)) {
						if($row["table_name"]) {
							$DB->SetTable($row["table_name"]);
							$DB->AddField("id");
							$DB->AddField("pid");
							if(empty($row["name_field"]))
				              $DB->AddField("name");
				              else 
				              $DB->AddExp($row["name_field"], "name");
				              
				              if(isset($_REQUEST["data"]["q"])) {
				                if(empty($row["name_field"])) $DB->AddCondFS("name", "LIKE", "".iconv("utf-8", "Windows-1251", $_REQUEST['data']["q"])."%");
		                        else $DB->AddCondXS($row["name_field"], "LIKE", "". iconv("utf-8", "Windows-1251", $_REQUEST['data']["q"])."%");
				              }
				              
				              $DB->AddOrder("name");
							$res_elem = $DB->Select(); 
							
							while($row_elem = $DB->FetchAssoc($res_elem)) {
								if($row["table_name"] != "news_cats")
								$row_elem['name'] = !AT_HOME ? $this->get_parents_list($row_elem["pid"], $row["table_name"],true)." > ".$row_elem['name'] : CF::Win2Utf($this->get_parents_list($row_elem["pid"],true))." > ".CF::Win2Utf($row_elem['name']);
				                elseif($row_elem["pid"]!=0)
								$row_elem['name'] = !AT_HOME ? $this->get_parents_list($row_elem["pid"], $row["table_name"],true)." > ".$row_elem['name'] : CF::Win2Utf($this->get_parents_list($row_elem["pid"],true))." > ".CF::Win2Utf($row_elem['name']);
								else
								$row_elem['name'] = !AT_HOME ? $this->get_parents_list($row_elem["pid"], $row["table_name"], false,$row_elem["id"])." > ".$row_elem['name'] : CF::Win2Utf($this->get_parents_list($row_elem["pid"],true))." > ".CF::Win2Utf($row_elem['name']);
								$this->output["elements"][] = $row_elem; 
							}//print_r($this->output["elements"]); 
						}
					}
          
		          if($this->mode == "ajax_autocomplete_element") {
		            $this->output["json"] = $this->output["elements"];
		          } 


		          break; 	

	          	case "single_rules":
				{
					$this->output["scripts_mode"] = $this->output["mode"] = "single_rules";

					$DB->SetTable("engine_privileges");

					$res = $DB->Select();
	                while ($row = $DB->FetchAssoc($res))
	                {
	                	$result[] = array('module_id' => $row['module_id'],
                                           'operation_name' => $row['operation_name'],
                                           'entry_id' => $row['entry_id'],
                                           'usergroup_id' => $row['usergroup_id'],
                                           'is_allowed' => $row['is_allowed'],
                                           'user_id' => $row['user_id']
                                        );
	                }
	                $this->output["priv"] = $result;


	                $DB->SetTable("engine_modules");
					$res2 = $DB->Select();
	                while ($row2 = $DB->FetchAssoc($res2))
	                {
	                	$result2[] = array('id' => $row2['id'],
                                           'name' => $row2['name'],
                                           'comment' => $row2['comment']
                                        );
	                }
	                $this->output["modules"] = $result2;


	                /*!!!����� �������� ������ �� ������*/
					// $res3 = $DB->Exec("SELECT * FROM nsau_people WHERE (library_card) IN (SELECT library_card FROM nsau_people GROUP BY library_card HAVING COUNT(id)>1)");
					// while ($row3 = $DB->FetchAssoc($res3))
	    //             {
	    //             	$tmp[] = array('last_name' => $row3['last_name'], 
	    //             					'name' => $row3['name'],
	    //             					'patronymic' => $row3['patronymic'],
	    //             					'library_card' => $row3['library_card'],
	    //             					'id' => $row3['id'],
	    //             					'group' => $this->get_fucking_group($row3['id_group']),
	    //             					'fac' => $this->get_fucking_fak($row3['id_group']),
	    //             		);
	    //             }
	    //             $this->output["testt"] = $tmp;

				}
				break;


				case "ajax_check_user":
	            {
	                global $DB, $Engine;
	               	$element 	= $_REQUEST['data']["element"];
	                $module 	= $_REQUEST['data']["module"];
	                $operation 	= $_REQUEST['data']["operation"];
	                $user 		= $_REQUEST['data']["user"];
	                $action 	= $_REQUEST['data']["action"];

	                if (!empty($element) && isset($module) && !empty($operation) && !empty($user)) 
	                {
	                	$DB->SetTable("engine_privileges");
	                	$DB->AddCondFS("module_id", "=", $module);
	                	$DB->AddCondFS("operation_name", "=", $operation);
	                	$DB->AddCondFS("entry_id", "=", $element);
	                	$DB->AddCondFS("is_allowed", "=", $action);
	                	$DB->AddCondFS("usergroup_id", "=", "-1");

	                	$res = $DB->Select(1);
                    	$row = $DB->FetchAssoc($res);
                    	/*���� ��� ���������� ����� ��������*/
                    	if (!empty($row))
                    	{
                    		$pos = strpos($row['user_id'], $this->get_auth_id($user));
                    		if ($pos === false) 
                    		{
							    $this->output['bad_message'] = "����� ��� �� ������";
							} else {
							    $this->output['good_message'] = "����� ��� ������";
							}
                    	}
                    	else
                    	{
                    		$this->output['bad_message'] = "������� �� ��������";
                    	}
                    }
                    else
                    {
                    	$this->output['bad_message'] = "���������� �� ��� ����";
                    }
	            }
	            break;

				case "ajax_give_rules":
	            {
	                global $DB, $Engine, $Auth;

	                $element 	= $_REQUEST['data']["element"];
	                $module 	= $_REQUEST['data']["module"];
	                $operation 	= $_REQUEST['data']["operation"];
	                $user 		= $_REQUEST['data']["user"];
	                $action 	= $_REQUEST['data']["action"];

	                if (!empty($element) && isset($module) && !empty($operation) && !empty($user) ) 
	                {
	                	$DB->SetTable("engine_privileges");
	                	$DB->AddCondFS("module_id", "=", $module);
	                	$DB->AddCondFS("operation_name", "=", $operation);
	                	$DB->AddCondFS("entry_id", "=", $element);
	                	$DB->AddCondFS("is_allowed", "=", $action);
	                	$DB->AddCondFS("usergroup_id", "=", "-1");

	                	$res = $DB->Select(1);
                    	$row = $DB->FetchAssoc($res);
                    	/*���� ��� ���������� ����� ��������*/
                    	if (!empty($row))
                    	{
                    		//��������� ��������
                    		$auth_id = $this->get_auth_id($user);
                    		if (empty($row['user_id']) ) 
                    		{
                    			$new_users = $auth_id;
                    		}
                    		else
                    		{
                    			$new_users = $row['user_id'].";".$auth_id;
                    		}
                    		$DB->SetTable("engine_privileges");
	                    	$DB->AddCondFS("module_id", "=", $module);
		                	$DB->AddCondFS("operation_name", "=", $operation);
		                	$DB->AddCondFS("entry_id", "=", $element);
		                	$DB->AddCondFS("is_allowed", "=", $action);
		                	$DB->AddCondFS("usergroup_id", "=", "-1");
		                	$DB->AddValue("user_id", $new_users);
		                	$DB->Update();
		                	$this->output['good_message'] = "����� ������";
                    	}
                    	else
                    	{
                    		$auth_id = $this->get_auth_id($user);
                    		$new_users = $auth_id;
                    		$DB->SetTable("engine_privileges");
	                    	$DB->AddValue("module_id", $module);
		                	$DB->AddValue("operation_name", $operation);
		                	$DB->AddValue("entry_id", $element);
		                	$DB->AddValue("is_allowed", $action);
		                	$DB->AddValue("usergroup_id", "-1");
		                	$DB->AddValue("user_id", $new_users);
		                	$DB->Insert();
		                	$DB->Update();
		                	$this->output['good_message'] = "������� ����� �������, ����� ������";
                    	}	
	                }
	                else
	                {
	                	$this->output['bad_message'] = "���������� �� ��� ����";
	                }
	                //$DB->SetTable("nsau_struct");
	            }
	            break;

		        case 'get_leader':
	            {
	                global $DB, $Engine;
	                $DB->SetTable("nsau_people");
	                $DB->AddCondFS("id", "=", $_REQUEST['data']["id"]);
	                $res = $DB->Select(1);
	                $child1 = $DB->FetchAssoc($res);
	                $result = array('name' => $child1["name"],
	                                'last_name' => $child1["last_name"],
	                                'patronymic' => $child1["patronymic"],
	                                'photo' => $child1["photo"],
	                                'id' => $child1["id"],
	                                'auth_id' => $child1['user_id'],
	                                'people_cat' => $this->who_is($child1['people_cat']),
	                                'prad' => $child1['prad']
	                                );
	                $this->output["json"] = $result;
	            }
	            break;

	           	case 'get_operations':
	            {
	                global $DB, $Engine;
					$DB->SetTable("engine_operations");
					$DB->AddCondFS("module_id", "=", $_REQUEST['data']["id"]);

					$res = $DB->Select();
	                while ($row = $DB->FetchAssoc($res))
	                {
	                	$result[] = array( 'operation_name' => $row['operation_name'],
                                           'comment' => $row['comment'],
                                        );
	                }
	                $this->output["json"] = $result;
	            }
	            break;

	            case 'get_rules':
	           	{
	           		$this->show_privileges($_REQUEST['data']["module"], NULL, $_REQUEST['data']["operation"]);
	           	}
	            break;
	           	
			}
				
		}

		else
		{
			$this->output["messages"]["bad"][] = "������ ��������.";
		}
	}

	function who_is($cat)
	{
		switch ($cat) {
			case 1:
				return "�������";
				break;
			
			case 2:
				return "�������������";
				break;

			case 1:
				return "����";
				break;
		}
	}

	function get_fucking_group ($id)
	{
		global $DB, $Engine;
		$DB->SetTable("nsau_groups");
		$DB->AddCondFS("id", "=", $id);
		$res = $DB->Select(1);
        $row = $DB->FetchAssoc($res);
    	return $row['name'];
	}

	function get_fucking_fak ($id)
	{
		global $DB, $Engine;
		$DB->SetTable("nsau_groups");
		$DB->AddCondFS("id", "=", $id);
		$res = $DB->Select(1);
        $row = $DB->FetchAssoc($res);

        $DB->SetTable("nsau_faculties");
		$DB->AddCondFS("id", "=", $row['id_faculty']);
		$res2 = $DB->Select(1);
        $row2 = $DB->FetchAssoc($res2);

    	return $row2['name'];
	}

	function get_auth_id($id)
	{
		global $DB, $Engine;
		$DB->SetTable("nsau_people");
		$DB->AddCondFS("id", "=", $id);
		$res = $DB->Select(1);
        $row = $DB->FetchAssoc($res);
    	return $row['user_id'];
	}
	
	function get_parents_list($id, $table, $level = null, $pid)
	{
		global $DB;
		//if(($table == "news_cats") && ($id==0)) return $result."������� ��������";
		$DB->SetTable($table, "f");
		if(($table == "news_cats") && ($id==0)) {
			$DB->AddCondFS("f.id", "=", $pid);
			$DB->AddField("f.descr", "descr");
		} else
			$DB->AddCondFS("f.id", "=", $id);
		$DB->AddField("f.id", "id");
		$DB->AddField("f.title", "name");
		$DB->AddField("f.pid", "pid");
		$res = $DB->Select();	
		$row = $DB->FetchAssoc($res);
		if($level) return $row["name"];
		if(!empty($row["pid"]) && ($row["pid"] != $row["id"]) && !empty($pid)) 
		{
			$result = $this->get_parents_list($row["pid"]).">".$row["name"];		
		}
		else
		{
			if(!empty($pid))
				$result = $result.(isset($row["descr"]) ? $row["descr"] : "������� ��������");
			else
				$result = $result.$row["name"];
		}
		return $result;
	}
	
	function show_modules()
	{
		global $DB, $Engine;
		
		$DB->SetTable("engine_modules");
		$DB->AddField("id");
		$DB->AddField("comment");
		$res = $DB->Select();
		
		while ($row = $DB->FetchAssoc($res)) {
			if($row['id']) {
				$DB->SetTable("engine_operations");
				$DB->AddField("operation_name");
				$DB->AddCondFS("module_id", "=", $row["id"]);
				$res_ops = $DB->Select();
				
				if ($DB->FetchAssoc($res_ops))
					$this->output["modules"][] = $row;	
			}			
		}
	}
	
	function show_form($module_id) {
		global $DB;
		
		$DB->SetTable("engine_modules");
		$DB->AddField("comment");
		$DB->AddCondFS("id", "=", $module_id);
		$res = $DB->Select();
		
		if (!$module_id || $row = $DB->FetchAssoc($res)) {
			if ($module_id)
				$this->output["module_name"] = $row["comment"];
			else
				$this->output["module_name"] = "��������� ��������";
				
			$this->output["module_id"] = $module_id;
			
			$DB->SetTable("engine_operations");
			$DB->AddField("operation_name");
			$DB->AddField("comment");
			$DB->AddField("table_name");
			$DB->AddCondFS("module_id", "=", $module_id);
			$res = $DB->Select();
			
			while ($row = $DB->FetchAssoc($res)) {
				$this->output["operations"][] = $row;
			}
			
				/*echo "<pre>111";
		print_r($this->output["operations"]);
		echo "</pre>";	*/
			$DB->SetTable("auth_usergroups");
			$DB->AddField("id");
			$DB->AddField("comment");
			$DB->AddOrder("comment");
			
			$res = $DB->Select();
			
			while ($row = $DB->FetchAssoc($res)) {
				$this->output["usergroups"][] = $row;
			}
			
		}
		else {
			$this->show_form(0);
		}
		
	}
	
	function show_element($module_id) {
		global $DB;
		
		$DB->SetTable("engine_modules");
		$DB->AddField("comment");
		$DB->AddCondFS("id", "=", $module_id);
		$res = $DB->Select();
		
			while ($row = $DB->FetchAssoc($res)) {
				
			$this->output["module_id"] = $module_id;
			}
			$DB->SetTable("engine_operations");
			//$DB->AddField("operation_name");
			//$DB->AddField("comment");
			$DB->AddField("table_name"); 
      $DB->AddField("name_field");
			$DB->AddCondFS("module_id", "=", $module_id);
			$res = $DB->Select();
			 //$this->output["elements"] = array();
			while ($row = $DB->FetchAssoc($res)) {
				$this->output["elements"][] = $row;
			
			}
				
				
		
	foreach($this->output["elements"] as $element){
if($element["table_name"]!=NULL){
		/*echo "<pre>222";
		print_r($this->output["elements"]);
		echo "</pre>";	*/
		
			
			
			
			$DB->SetTable($element["table_name"]);
			
			$DB->AddField("id");
      if(empty($element["name_field"]))
			$DB->AddField("name");
      else 
      $DB->AddExp($element["name_field"], "name");
      
      
      $DB->AddOrder("name");
			
			$res = $DB->Select();
			 //$this->output["elements"] = array();
			while ($row = $DB->FetchAssoc($res)) {
				$this->output["name_elements"][] = $row;
			
			}
			/*$result = count($this->output["name_elements"]);
			echo $result;*/
			/*echo "<pre>888";
		print_r($this->output["name_elements"]);
		echo "</pre>";*/
		} 
	}
	
	
	
		}
	
	function insert_privilege()
	{
		global $DB, $Engine, $Auth;
		
		if (isset ($_POST[$this->node_id]["give_privileges"]["all_entries"]) && $_POST[$this->node_id]["give_privileges"]["all_entries"] == "on")
			$entry_id = -1;
		else
			$entry_id = $_POST[$this->node_id]["give_privileges"]["entry_id"];
		
/*		$DB->SetTable("engine_privileges");
		$DB->AddValue("module_id", $_POST[$this->node_id]["give_privileges"]["module_id"]);
		$DB->AddValue("operation_name", $_POST[$this->node_id]["give_privileges"]["operation_name"]);
		
		if (isset ($_POST[$this->node_id]["give_privileges"]["all_entries"]) && $_POST[$this->node_id]["give_privileges"]["all_entries"] == "on")
			$DB->AddValue("entry_id", "-1");
		else
			$DB->AddValue("entry_id", $_POST[$this->node_id]["give_privileges"]["entry_id"]);
			
		$DB->AddValue("usergroup_id", $_POST[$this->node_id]["give_privileges"]["usergroup_id"]);
		$DB->AddValue("is_allowed", $_POST[$this->node_id]["give_privileges"]["is_allowed"]);*/
		
		//if ($DB->Insert())
		
		$do = $_POST[$this->node_id]["give_privileges"]["is_allowed"] ? "����������" : "������";
		
		$DB->SetTable("auth_usergroups");
		$DB->AddCondFS("id", "=", $_POST[$this->node_id]["give_privileges"]["usergroup_id"]);
		$res = $DB->Select();
		
		$row = $DB->FetchAssoc($res);
		
		if ($Engine->AddPrivilege($_POST[$this->node_id]["give_privileges"]["module_id"], $_POST[$this->node_id]["give_privileges"]["operation_name"], $entry_id, $_POST[$this->node_id]["give_privileges"]["usergroup_id"], $_POST[$this->node_id]["give_privileges"]["is_allowed"], $_POST[$this->node_id]["give_privileges"]["operation_name"].";".$_POST[$this->node_id]["give_privileges"]["usergroup_id"]))
			$this->output["messages"]["good"][] = "�������� ������ �������";
		else
			$this->output["messages"]["bad"][] = "������";
		
		$_SESSION["messages"] = $this->output["messages"]; 
		CF::redirect($Engine->unqueried_uri);
	}
	
	function show_privileges($module_id = NULL, $usergroup_id = NULL, $operation=NULL)
	{
		global $DB;
    
    $this->output['usergroup_id'] = $usergroup_id; 
		
		$DB->SetTable("engine_privileges");
		$DB->AddField("operation_name");
		$DB->AddField("entry_id");
        $DB->AddField("module_id");
		$DB->AddField("usergroup_id");
		$DB->AddField("is_allowed");
		$DB->AddField("user_id");
		if($module_id !== NULL) $DB->AddCondFS("module_id", "=", $module_id);
		if($usergroup_id !== NULL) $DB->AddCondFS("usergroup_id", "=", $usergroup_id);
		if($operation !== NULL) $DB->AddCondFS("operation_name", "=", $operation);
		$res = $DB->Select();
		
		$i = 0;

		while ($row = $DB->FetchAssoc($res)) {
			$DB->SetTable("engine_operations");
			$DB->AddField("comment");
			$DB->AddFields(array("table_name", "name_field"));
			$DB->AddCondFS("operation_name", "=", $row["operation_name"]);
			$DB->AddCondFS("module_id", "=", $row["module_id"]);
			$res_op = $DB->Select();
			$row_op = $DB->FetchAssoc($res_op);
			
			if ($row_op["comment"])
				$this->output["privileges"][$i]["operation"] = $row_op["comment"];
			else
				$this->output["privileges"][$i]["operation"] = $row["operation_name"];
				
			$this->output["privileges"][$i]["operation_code"] = $row["operation_name"];
			$this->output["privileges"][$i]["module_id"] = $row["module_id"];
			
			if ($row["usergroup_id"] != "-1" && $row["usergroup_id"]) {
				$DB->SetTable("auth_usergroups");
				$DB->AddField("comment");
				$DB->AddCondFS("id", "=", $row["usergroup_id"]);
				$res_ug = $DB->Select();
				$row_ug = $DB->FetchAssoc($res_ug);
				
				$this->output["privileges"][$i]["usergroup"] = $row_ug["comment"];
			}
			else {
				$this->output["privileges"][$i]["usergroup"] = $row["usergroup_id"];
			}
			
			if(!empty($row_op["table_name"]) && $row["entry_id"] != -1) {
				$DB->SetTable($row_op["table_name"]);
				$DB->AddCondFS("id", "=", $row["entry_id"]);
				if(!empty($row_op["name_field"])) 
					$DB->AddExp($row_op["name_field"], "name");
				else 
					$DB->AddField("name");
				
				//echo $DB->SelectQuery(1);
				$res_entry = $DB->Select(1);
				
				while($row_entry = $DB->FetchAssoc($res_entry)) {
					$this->output["privileges"][$i]["entry"] = $row_entry;
				}
			}
			
			$this->output["privileges"][$i]["entry_id"] = $row["entry_id"];
			$this->output["privileges"][$i]["is_allowed"] = $row["is_allowed"];
			$this->output["privileges"][$i]["usergroup_id"] = $row["usergroup_id"];

			/*get info about users*/
			if ($row["user_id"] != 0) 
			{
				$users = explode(",", $row['user_id']);

				foreach ($users as $key) 
				{
					$DB->SetTable("nsau_people");
					$DB->AddCondFS("user_id", "=", $key);
					$DB->AddField("last_name");
					$DB->AddField("name");
					$DB->AddField("patronymic");
					$DB->AddField("id");
					$DB->AddField("photo");
					$res_info = $DB->Select();
                    while($user = $DB->FetchAssoc($res_info))
                    {
                        $user_info[] = array('id' => $user["id"],
                        		'name' => $user["name"],
                        		'last_name' => $user['last_name'],
                        		'patronymic' => $user['patronymic'],
                        		'photo' => $user['photo']
                        	);

                    }
				}
				$this->output["privileges"][$i]["users"] = $user_info;
			}
			$i++;
		}
		
	}
	
	function delete_privilege ()
	{
		global $DB, $Engine;
		
		/*$DB->SetTable("engine_privileges");
		$DB->AddCondFS("operation_name", "=", $_GET["delete_name"]);
		$DB->AddCondFS("module_id", "=", $_GET["delete_module"]);
		$DB->AddCondFS("usergroup_id", "=", $_GET["delete_usergroup"]);
		$DB->AddCondFS("entry_id", "=", $_GET["delete_entry"]);
		$DB->Delete();*/
		
		$Engine->DeletePrivilege($_GET["delete_module"], $_GET["delete_name"], $_GET["delete_entry"], $_GET["delete_usergroup"], $_GET["delete_allowed"]);
		
		CF::Redirect($Engine->unqueried_uri);
	}
	

	function ManageHTTPdata()
	{
		return;
	}
	
	
	function Output()
	{
		return $this->output;
	}
}

?>
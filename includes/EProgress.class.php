<?php
/**
 * ������ ������������
 */
class EProgress
{
	function EProgress ($global_params, $params, $module_id, $node_id, $module_uri)
	{
		global $Engine, $Auth, $DB;
        /**
         * ������ ����� ������
         * @var [array]
         */
		$options = $Engine->GetModuleOptions($module_id);
        $this->output["options"] = $options;
        switch ($params) 
        {
        	case 'matrix':
        	{
                $this->output["scripts_mode"] = $this->output["mode"] = $params;
                $this->output["options"] = $options;

                $all_info = $this->get_all_info();

                $DB->SetTable("nsau_students_progress");
                $DB->AddCondFS("people_id", "=", $all_info['people_id']);
                $res = $DB->Select();
                while($row = $DB->FetchAssoc($res)) 
                {


                    $DB->SetTable("nsau_students_comp");


                    /*���� �����*/
                    if (  $row['subject'] == "���������� �������� � ����� (���������� ����������)") 
                    {
                        $DB->AddCondFS("disc", "LIKE", "%���������� �������� � �����%");
                    }
                    else 
                    {
                        $DB->AddCondFS("disc", "=", $row['subject']);
                    }


                    $DB->AddCondFS("spec_id", "=", $all_info['spec_id']);

                    if ($all_info['profile_group']) 
                    {
                        $DB->AddCondFS("profile_id", "=", $all_info['profile_group']);
                    }

                    if ($all_info['id_faculty']==9) 
                    {
                        $DB->AddCondFS("fac", "=", $all_info['id_faculty']);
                    }


                    $DB->AddCondFS("year", "=", $all_info['year']);
                    $res2 = $DB->Select(1);
                    $in_comp = $DB->FetchAssoc($res2);

                    if ($in_comp['comp'])
                    {
                        $fail = array("-", "--", "�� ������", "�� ���� �", "���", "�� �������", "�� ������", "�� �������");
                        if (in_array($row['value'], $fail)) 
                        {
                            $status = "0";
                        }
                        else
                        {
                            $status = "1";
                        }
                        
                        $result[ $row['subject'] ] = array('comp' => $in_comp['comp'], 'status' => $status);
                    }
                }

                $this->output["all_info"] = $all_info;
                $this->output["all_discs"] = $result;

        	}
        	break;

            case 'upload_form':
            {
                $this->output["scripts_mode"] = $this->output["mode"] = $params;
                $this->output["options"] = $options;

                /*������ ����������*/
                $DB->SetTable("nsau_specialities");
                $DB->AddCondFS("old", "=", 0);
                $res = $DB->Select();
                while($row = $DB->FetchAssoc($res)) 
                {
                    if ($row['id_faculty']==9) 
                    {
                        $code = $row['code']."(����)";
                    }
                    else
                    {
                        $code = $row['code'];
                    }
                    $specs[$code] = array('id' => $row['id'], 'faculty' => $row['id_faculty'], 'name' => $row['name'], 'year_open' => $row['year_open']);
                }
                $this->output["specs"] = $specs;


                /*������ �����������*/
                $DB->SetTable("nsau_faculties");
                $DB->AddCondFS("is_active", "=", 1);
                $res = $DB->Select();
                while($row = $DB->FetchAssoc($res)) 
                {
                    $fac[$row['name']] = array('id' => $row['id'], 'name' => $row['name'],);
                }
                $this->output["fac"] = $fac;






                if (isset($_POST['upload'])) 
                {
                    $spec = $_POST['specs'];
                    $form_edu = $_POST['form_edu'];
                    //$year = $_POST['year'];
                    
                    $fac = $_POST['fac'];

                    $tmp_year = explode(";", $_POST['year']);

                    $profile = $_POST['profile'];

                    foreach ($tmp_year as $key => $year) 
                    {


                        if (isset($_FILES["file"])) 
                        {
                            if ($_FILES["file"]["error"] > 0) 
                            {
                                echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
                            }
                            else 
                            {
                                $fname = $_FILES["file"]["tmp_name"]; 

                                $handle = @fopen($fname, "r");
                                if ($handle) 
                                {
                                    $skip = "";
                                    $error = false;

                                    if (!empty($spec) && !empty($year)) 
                                    {
                                        while (($buffer = fgets($handle)) !== false) 
                                        {
                                            $parts = explode(";", $buffer);

                                            if (trim($parts[0])) 
                                            {
                                                $disc = trim($parts[0]);
                                            }
                                            else
                                            {
                                                $error = true;
                                            }
                                            
                                            if (trim($parts[1]))
                                            {
                                                $tmp_comp = trim($parts[1]);

                                                if (strpos($tmp_comp, '"') === 0) 
                                                {
                                                    $tmp = explode('"', $buffer);
                                                    $comp = $tmp[1];
                                                }
                                                else
                                                {
                                                    $comp = $tmp_comp;
                                                }

                                            }
                                            else
                                            {
                                                $error = true;
                                            }

                                            if ($error == false) 
                                            {
                                                $result[] = array('disc' => $disc, 'comp' => $comp, 'spec' => $spec, 'profile' => $profile ? $profile : NULL, 'year' => $year, "profile_id" => $profile ? $profile : NULL);
                                               
                                                $DB->SetTable("nsau_students_comp");
                                                $DB->AddCondFS("disc", "=", $disc);
                                                $DB->AddCondFS("comp", "=", $comp);
                                                $DB->AddCondFS("spec_id", "=", $spec);
                                                $DB->AddCondFS("profile_id", "=", $profile ? $profile : NULL);
                                                $DB->AddCondFS("form", "=", $form_edu ? $form_edu : NULL);
                                                $DB->AddCondFS("year", "=", $year);
                                                $DB->AddCondFS("fac", "=", $fac ? $fac : NULL);
                                                $res = $DB->Select(1);
                                                $rrr = $DB->FetchAssoc($res);
                                                if( empty($rrr) )
                                                {
                                                    $DB->SetTable("nsau_students_comp");
                                                    $DB->AddValue("disc", $disc);
                                                    $DB->AddValue("comp", $comp);
                                                    $DB->AddValue("spec_id", $spec);
                                                    $DB->AddValue("profile_id", $profile ? $profile : NULL);
                                                    $DB->AddValue("form", $form_edu);
                                                    $DB->AddValue("year", $year);
                                                    $DB->AddValue("fac", $fac ? $fac : NULL);
                                                    
                                                    if($DB->Insert())
                                                    {
                                                        $this->output["success"] = '���� ������� ��������';
                                                    }
                                                    else
                                                    {
                                                        $this->output["error"] = '������ �������� �����';
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                $this->output["error"] = '���� �����������';
                                            }
                                        }
                                    }
                                    else
                                    {
                                        $this->output["error"] = '�� ������� ���������� ��� ��� ������';
                                    }
                                }
                                else
                                {
                                    $this->output["error"] = '�� ������ ����';
                                }
                            }

                            //CF::Debug($result);
                        } 
                        else 
                        {
                                   $this->output["error"] = '�� ������ ����';
                        }
                        $this->output["out_text_year"] .= $year." ";
                    }
                }
            }
            break;

            case 'get_profiles':
            {
                global $DB;
                $DB->SetTable("nsau_profiles");
                $DB->AddCondFS("spec_id", "=", $_REQUEST['data']['id']);
                $res = $DB->Select();
                while($row = $DB->FetchAssoc($res)) 
                {
                    if (!empty($row['id'])) 
                    {
                        $result[] = array('id'=> $row['id'], 'name' => $row['name']);
                    }
                }
                $this->output["json"] = $result;
            }
            break;

            case 'get_non_year':
            {
                $this->output["scripts_mode"] = $this->output["mode"] = $params;
                $this->output["options"] = $options;

                global $DB;
                $DB->SetTable("nsau_people");
                $DB->AddCondFS("people_cat", "=", "1");
                $DB->AddCondFS("year", "=", "0000");
                $DB->AddCondFS("id_group", "!=", 0);
                $res = $DB->Select();
                while($row = $DB->FetchAssoc($res)) 
                {
                    $DB->SetTable("nsau_groups");
                    $DB->AddCondFS("id", "=", $row['id_group']);
                    $res2 = $DB->Select(1);
                    $in_groups = $DB->FetchAssoc($res2);


                    $result[$in_groups['name']][] = array('id'=> $row['id'], 'last_name'=> $row['last_name'], 'name' => $row['name'], 'patronymic' => $row['patronymic']);
                }
                ksort($result);
                $this->output["result"] = $result;
            }
            break;
        }
	}
    

    function get_all_info ()
    {
        global $Engine, $Auth, $DB;

        $DB->SetTable("nsau_people");
        $DB->AddCondFS("user_id", "=", $Auth->user_id);
        $res = $DB->Select(1);
        $in_people = $DB->FetchAssoc($res);
        

        $DB->SetTable("nsau_groups");
        $DB->AddCondFS("id", "=", $in_people['id_group']);
        $res = $DB->Select(1);
        $in_groups = $DB->FetchAssoc($res); 

        $DB->SetTable("nsau_faculties");
        $DB->AddCondFS("id", "=", $in_groups['id_faculty']);
        $res = $DB->Select(1);
        $in_faculty = $DB->FetchAssoc($res);      

        $DB->SetTable("nsau_specialities");
        $DB->AddCondFS("code", "=", $in_groups['specialities_code']);
        $DB->AddCondFS("id_faculty", "=", $in_groups['id_faculty']);
        $res = $DB->Select(1);
        $in_special = $DB->FetchAssoc($res);  


        $current_user = array(
            'name' => $in_people['name'], 
            'last_name' => $in_people['last_name'],
            'patronymic' => $in_people['patronymic'],

            'year' => $in_people['year'],

            'id_group' => $in_people['id_group'],
            'name_group' => $in_groups['name'],
            'profile_group' => $in_groups['profile'],

            'course' => $in_groups['year'],

            'spec_code' => $in_groups['specialities_code'],
            'spec_id' => $in_special['id'],

            'id_faculty' => $in_groups['id_faculty'],
            'name_faculty' => $in_faculty['name'],
            'link_faculty' => $in_faculty['link'],

            'people_id' => $in_people['id'],

        );
        return $current_user;
    }


    function Output() 
    {
        return $this->output;
    }
}
?>
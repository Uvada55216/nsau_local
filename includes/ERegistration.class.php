<?php
/**
 * Модуль регистрации пользователей
 */
class ERegistration
{
	function ERegistration ($global_params, $params, $module_id, $node_id, $module_uri)
	{
		global $Engine, $Auth, $DB;
        /**
         * Массив опций модуля
         * @var [array]
         */
		$options = $Engine->GetModuleOptions($module_id);
        $this->output["options"] = $options;
        switch ($params) 
        {
            //---------------------------------------------------------------------------------------------------------------
        	/**
             * Регистрация преподавателя
             */
        	case 'teachers_registration':
        	{
                $this->output["scripts_mode"] = $this->output["mode"] = $params;
                $this->output["options"] = $options;

                if ($options['check_registration_enable']) 
                {
                    $this->get_registration_blank_data("teachers");
                    if (isset($_POST['send_register'])) 
                    {
                        $important_data = array('name_f' => "fam", 
                                                'name_n' => "name",
                                                'name_o' => "otc",
                                                'post'   => "dolj",
                                                'department' => "kaf",
                                                'email'  => "email",
                                                'password'  => "pass",
                                                'repassword' => "repass",
                                                'gender'    => "pol",
                        );

                        $all_data = array('name_f','name_n','name_o','post', 'department', 'degree','rank','email','password','kurator', 'gender', 'create_new_mail');
                        /**
                         * Проверка заполнения ключевых полей
                         */
                        foreach ($important_data as $key => $value) 
                        {
                            if (!isset($_POST[$key]) || empty($_POST[$key]) || $_POST[$key] == -1) 
                            {
                                $this->output["messages"]["bad"][] = $value;
                                $error = true;
                            }
                        }
                        /**
                         * Проверка ФИО на длину
                         */
                        if (strlen($_POST['name_f'])<=1 || strlen($_POST['name_n'])<=1 || strlen($_POST['name_o'])<=1) 
                        {
                            $this->output["messages"]["bad"][] = "fio";
                            $error = true;
                        }
                        /**
                         * Проверка длины и подтверждения пароля
                         */
                        if (!($_POST['password'] == $_POST['repassword'])) 
                        {
                            $this->output["messages"]["bad"][] = "repass";
                            $error = true;
                        }elseif (strlen($_POST['password'])<3) 
                        {
                            $this->output["messages"]["bad"][] = "pass4";
                            $error = true;
                        }
                        /**
                         * Проверка почты на корректность (при выборе создания ящика и нет)
                         */
                        if (isset($_POST["create_new_mail"]))
                        {
                            $if_stuped_user_mail = explode ("@", $_POST['email']);
                            if (!empty($if_stuped_user_mail)) 
                            {
                                $new_mail = $if_stuped_user_mail[0]."@nsau.edu.ru";
                            }
                            else
                            {
                                $new_mail = $_POST['email']."@nsau.edu.ru";
                            }
                        }
                        else
                        {
                            $domain = strstr($_POST['email'], '@');
                            if (empty($domain))
                            {
                                $this->output["messages"]["bad"][] = "emailsobaka";
                                $error = true;
                            }
                            else
                            {
                                $new_mail = $_POST['email'];
                            }
                        }
                        /**
                         * Проверка кирилицы в названии почты
                         */
                        if(!preg_match('@[A-z]@u',$_POST['email']))
                        {
                            $this->output["messages"]["bad"][] = "emailsp";
                            $error = true;              
                        }
                        /**
                         * Проверка почты на пробелы в названии
                         */
                        $string = trim($_POST['email']);
                        if(strpos($string, 0x20) !== false) 
                        {
                            $this->output["messages"]["bad"][] = "emailsp";
                            $error = true;
                        }
                        /**
                         * Проверка на существование почты в базе данных
                         */
                        $DB->SetTable("auth_users");
                        $DB->AddCondFS("email", "=", $new_mail);
                        $check_mail = $DB->Select();
                        $row = $DB->FetchAssoc($check_mail);
                        if (!empty($row)) 
                        {
                            $this->output["messages"]["bad"][] = "emailallready";
                            $error = true;
                        }
                        /**
                         * Обработка Google Recaptcha
                         */
                        if ($options['check_captcha']) 
                        {
                            $recaptcha=$_POST['g-recaptcha-response'];
                            if(!empty($recaptcha))
                            {
                                $google_url ="https://www.google.com/recaptcha/api/siteverify";
                                $secret     ="6LevLxMUAAAAAHeI4DXLX2YNZIu3R0b7zF5w8l33";
                                $ip         =$_SERVER['REMOTE_ADDR'];
                                $url        =$google_url."?secret=".$secret."&response=".$recaptcha."&remoteip=".$ip;
                                $res=$this->getCurlData($url);
                                $res= json_decode($res, true);
                                //reCaptcha введена
                                if(!$res['success'])
                                {
                                    $this->output["messages"]["bad"][] = "capth";
                                    $error = true;
                                }
                            }
                            else
                            {
                                $this->output["messages"]["bad"][] = "capth";
                                $error = true;
                            }
                        }

                        /**
                         * Запись данных в БД
                         */
                        if (!$error) 
                        {
                            //auth
                            $DB->SetTable("auth_users");
                            $DB->AddValue("is_active", "0");
                            $DB->AddValue("username", $new_mail);
                            $DB->AddValue("password",md5($_POST["password"]));
                            $DB->AddValue("displayed_name", $_POST['name_f']." ".$_POST['name_n']." ".$_POST['name_o']);
                            $DB->AddValue("email", $new_mail);
                            $DB->AddValue("usergroup_id", "2");
                            $DB->AddValue("create_user_id", "987654321");
                            $DB->AddValue("create_time", "NOW()", "x");
                            $DB->Insert();

                            $DB->SetTable("auth_users");
                            $DB->AddCondFS("email", "=", $new_mail);
                            $res = $DB->Select(1);
                            $in_auth = $DB->FetchAssoc($res);
                            $post_name = $this->post_name($_POST['post']);

                            //people
                            $DB->SetTable("nsau_people");
                            $DB->AddValue("user_id", $in_auth['id']);
                            $DB->AddValue("last_name", $_POST['name_f']);
                            $DB->AddValue("name", $_POST['name_n']);
                            $DB->AddValue("patronymic", $_POST['name_o']);
                            $DB->AddValue("people_cat", "2");
                            $DB->AddValue("post", $post_name);
                            $DB->AddValue("email", $new_mail);
                            if ($_POST['gender'] = 1) 
                            {
                                $DB->AddValue("male", "1");
                            }
                            elseif ($_POST['gender'] = 2) 
                            {
                                $DB->AddValue("male", "0");
                            }
                            
                            if (isset($_POST['create_new_mail'])) 
                            {
                                $DB->AddValue("need_mail", "1");
                            }
                            $DB->Insert();

                            $DB->SetTable("nsau_people");
                            $DB->AddCondFS("email", "=", $new_mail);
                            $res = $DB->Select(1);
                            $in_people = $DB->FetchAssoc($res);

                            //into teachers
                            $DB->SetTable("nsau_teachers_post");
                            $DB->AddCondFS("id", "=", $_POST['post']);
                            $asset = $DB->FetchAssoc($DB->Select(1));

                            switch ($asset['asset']) 
                            {
                                case 1:
                                    $post="Заведующий";
                                    break;
                                case 2:
                                    $post="Преподаватель";
                                    break;
                                case 3:
                                    $post="Заведующий лабораторией";
                                    break;
                                case 4:
                                    $post="Лаборант";
                                    break;
                            }

                            $DB->SetTable("nsau_teachers");
                            $DB->AddValue("people_id", $in_people['id']);
                            $DB->AddValue("department_id", $_POST['department']);
                            $DB->AddValue("rate", "1");
                            $DB->AddValue("post", iconv('utf-8','windows-1251',$post)); 
                            $DB->AddValue("post_itemprop", $_POST['post']); 

                            $DB->AddValue("academ_stat", $_POST['rank']);
                            if (isset($_POST['kurator']) && !empty($_POST['kurator'])) 
                            {
                                $DB->AddValue("is_curator", "1");
                            }
                            else
                            {
                                $DB->AddValue("is_curator", "0");
                            }
                            $DB->AddValue("degree", $_POST['degree']);
                            $DB->Insert();

                            $DB->SetTable("nsau_teachers_department_posts");
                            $DB->AddValue("people_id", $in_people['id']);
                            $DB->AddValue("department_id", $_POST['department']);
                            $DB->AddValue("post_id", $_POST['post']); 
                            $DB->Insert();

                            if (isset($_POST['kurator']) && !empty($_POST['kurator'])) 
                            {
                                foreach ($_POST['kurator'] as $k => $id_kur) 
                                {
                                    $this->insert_to_curators ($in_people['id'], $_POST['department'], $id_kur);
                                }

                                $DB->SetTable("engine_privileges");
                                $DB->AddCondFS("module_id", "=", 0);
                                $DB->AddCondFS("operation_name", "=", "folder.view");
                                $DB->AddCondFS("entry_id", "=", 22511);
                                $DB->AddCondFS("is_allowed", "=", 1);
                                $DB->AddCondFS("usergroup_id", "=", "-1");
                                $res4 = $DB->Select(1);
                                $row4 = $DB->FetchAssoc($res4);

                                $new = $row4['user_id'].";".$in_auth['id'];
                                $DB->SetTable("engine_privileges");
                                $DB->AddCondFS("module_id", "=", 0);
                                $DB->AddCondFS("operation_name", "=", "folder.view");
                                $DB->AddCondFS("entry_id", "=", 22511);
                                $DB->AddCondFS("is_allowed", "=", 1);
                                $DB->AddCondFS("usergroup_id", "=", "-1");
                                $DB->AddValue("user_id", $new);
                                $DB->Update();
                            }


                            $summary_info = array('fio' => $_POST['name_f']." ".$_POST['name_n']." ".$_POST['name_o'],
                                                  'email' => $new_mail,
                                                  'username' => $new_mail,
                                                  'password' => $_POST['password']

                             );
                            $this->output["summary_info"] = $summary_info;

                            $DB->SetTable("nsau_tmp_register");
                            $DB->AddValue("user_id", $in_auth['id']);
                            $DB->AddValue("login", $new_mail);
                            $DB->AddValue("password", $_POST['password']); 
                            $DB->Insert();

                           // $Engine->LogAction($this->module_id, "teacher_register", NULL, "request");
                        }
                        else
                        {
                            /*error, forward data*/
                            foreach ($all_data as $key) 
                            {
                                if (isset($_POST[$key]) || !empty($_POST[$key])) 
                                {
                                    $this->output["reenter"][$key] = $_POST[$key];
                                }
                            }
                        }   
                    }
                }

        	}
        	break;

            /**
             * Модерация заявок регигстрации преподавателей
             */
            case 'teachers_registration_moderate':
            {
                $this->output["scripts_mode"] = $this->output["mode"] = $params;
                if ($options['check_moderation_enable']) 
                {
                    global $DB, $Auth;
                    $DB->SetTable("auth_users");
                    $DB->AddCondFS("create_user_id", "=", 987654321);
                    $DB->AddCondFS("is_active", "=", 0);
                    $res = $DB->Select();
                    while($row = $DB->FetchAssoc($res)) 
                    {
                        $DB->SetTable("nsau_people");
                        $DB->AddCondFS("user_id", "=", $row['id']);
                        $res2 = $DB->Select(1);
                        $in_people = $DB->FetchAssoc($res2);

                        $DB->SetTable("nsau_teachers");
                        $DB->AddCondFS("people_id", "=", $in_people['id']);
                        $in_teachers = $DB->FetchAssoc($DB->Select(1));

                        $DB->SetTable("nsau_teachers");
                        $DB->AddCondFS("people_id", "=", $Auth->people_id);
                        $current_moderator = $DB->FetchAssoc($DB->Select(1));

                        $DB->SetTable("nsau_departments");
                        $DB->AddCondFS("id", "=", $in_teachers['department_id']);
                        $is_tshi = $DB->FetchAssoc($DB->Select(1));

                        $test = explode(";", $current_moderator['department_id']);
                        foreach ($test as $current_moderator_id) 
                        {
                            if ($current_moderator_id==$in_teachers['department_id'] || $Auth->usergroup_id==1 || $Auth->user_id==598)
                            {
                                /*Если ОксанаЧеремных:D(из тсхи)*/
                                if ($Auth->user_id==598 && $is_tshi['faculty_id']==9 || $Auth->usergroup_id==1) 
                                {
                                    $result = array('id' => $row['id'], 'displayed_name'=> $row['displayed_name'], 'create_time' => $row['create_time'], 'email'=> $row['email'], "kaf"=> $this->kaf_on_id($in_teachers['department_id']));
                                    $this->output["waitings_teachers"][$in_people['id']] = $result;
                                }
                                elseif ($Auth->user_id!=598 && $is_tshi['faculty_id']!=9 || $Auth->usergroup_id==1) 
                                {
                                    $result = array('id' => $row['id'], 'displayed_name'=> $row['displayed_name'], 'create_time' => $row['create_time'], 'email'=> $row['email'], "kaf"=> $this->kaf_on_id($in_teachers['department_id']));
                                    $this->output["waitings_teachers"][$in_people['id']] = $result;
                                }
                            }
                        }
                    }
                }
            }
            break;

            /**
             * Одобрение заявки преподавателя
             */
            case 'ajax_teachers_accept':
            {
                global $DB, $DBMail, $Engine;
                $DB->SetTable("auth_users");
                $DB->AddCondFS("id", "=", $_REQUEST['data']["id"]);
                $DB->AddValue("is_active", 1);
                
                if ($DB->Update())
                {
                    $json['accept_user'] = 1;
                }
                else
                {
                    $json['accept_user'] = 0;
                }

                $DB->SetTable("nsau_people");
                $DB->AddCondFS("user_id", "=", $_REQUEST['data']["id"]);
                $res = $DB->Select(1);
                $in_people = $DB->FetchAssoc($res);

                $DB->SetTable("auth_users");
                $DB->AddCondFS("id", "=", $_REQUEST['data']["id"]);
                $res = $DB->Select(1);
                $in_auth = $DB->FetchAssoc($res);


                $DB->SetTable("nsau_tmp_register");
                $DB->AddCondFS("user_id", "=", $in_auth['id']);
                $res = $DB->Select(1);
                $tmp_register = $DB->FetchAssoc($res);

                /*try to create new mail*/
                if ($in_people['need_mail']==1) 
                {
                    if (!AT_HOME) 
                    {
                        $DBMAIL_HOST = "192.168.30.21";
                        $DBMAIL_PORT = "3306";   
                        $DBMAIL_USER = "mailadm";
                        $DBMAIL_PASS = "lf92mv73";
                        $DBMAIL_NAME = "postfix";

                        $DBMail = new MySQLhandle($DBMAIL_HOST, $DBMAIL_USER, $DBMAIL_PASS, $DBMAIL_NAME, $DBMAIL_PORT, 0);
                        $DBMail->Exec("SET time_zone = '" . TIMEZONE_OFFSET_FORMATTED . "'");
                        $DBMail->Exec("SET NAMES '" . CODEPAGE_DB . "'");
                    }

                    $user_mail_data = explode ("@", $in_auth['email']);
                    $DBMail->SetTable("mailbox");
                    $DBMail->AddValue("username", $user_mail_data[0]."@".$user_mail_data[1]);
                    $DBMail->AddValue("domain", $user_mail_data[1]);
                    $DBMail->AddValue("active", 1);
                    $DBMail->AddValue("password", $in_auth['password'] );
                    $DBMail->AddValue("quota", 300*1024*1024);
                    $DBMail->AddValue("maildir", $user_mail_data[1]."/".$user_mail_data[0]."/");                    
                    if ($DBMail->Insert()) 
                    {
                        $json['create_mail'] = 1;
                    }
                    else
                    {
                        $json['create_mail'] = 0;
                    }

                    $DBMail->SetTable("alias");
                    $DBMail->AddValue("address", $user_mail_data[0]);
                    $DBMail->AddValue("goto", $user_mail_data[0]);
                    $DBMail->AddValue("domain", $user_mail_data[1]);
                    $DBMail->AddValue("created", date("Y-m-d H:i:s"));
                    $DBMail->AddValue("modified", date("Y-m-d H:i:s"));
                    $DBMail->AddValue("active", 1);
                    $DBMail->Insert();
                    if ($DBMail->Insert()) 
                    {
                        $json['create_alias'] = 1;
                    }
                    else
                    {
                        $json['create_alias'] = 0;
                    }

                    $DB->SetTable("nsau_people");
                    $DB->AddCondFS("user_id", "=", $in_auth['id']);
                    $DB->AddValue("need_mail", "0");
                    $DB->Update();
                }
                else
                {
                    $json['create_mail'] = 2;
                    $json['create_alias'] = 2;
                }

                $this->output["json"] = $json;


                //сообщение на том, что заявка одобрена
                $array = parse_ini_file(INCLUDES . "Registration.ini", true);
                $notify_settings = $array["notify_settings"];
                if ($options["check_teachers_accept_notify"])
                {
                    $array = array(
                        "%server_name%" => $_SERVER["SERVER_NAME"],
                        "%site_name%" => SITE_NAME,
                        "%site_short_name%" => SITE_SHORT_NAME,
                        "%mail_login%" => $tmp_register['login'],
                        "%mail_pass%" =>  $tmp_register['password'],
                        "%post_time%" => date($notify_settings["time_format"]),
                        "\\r\\n" => "\r\n"
                        );
                    $array['message'] = $options['mail:teachers_accept_notify_message'];
                    $post = str_replace(array_keys($array), array_values($array), $notify_settings);

                    mail($in_auth['email'], '=?koi8-r?B?'.base64_encode(iconv('windows-1251', 'koi8-r', $post["subject"])).'?=', $post['message'], $post['headers']);
                }
                $Engine->LogAction($this->module_id, "teacher_register", $in_people['id'], "accept");

                $DB->SetTable("nsau_tmp_register");
                $DB->AddCondFS("user_id", "=", $in_auth['id']);
                $DB->Delete();
            }
            break;

            /**
             * Удаление заявки преподавателя
             */
            case 'ajax_teachers_delete':
            {
                global $DB;

                $DB->SetTable("nsau_people");
                $DB->AddCondFS("user_id", "=", $_REQUEST['data']["id"]);
                $res = $DB->Select(1);
                $in_people = $DB->FetchAssoc($res);


                $DB->SetTable("auth_users");
                $DB->AddCondFS("id", "=", $_REQUEST['data']["id"]);
                $DB->AddCondFS("is_active", "=", 0);
                $res = $DB->Select(1);
                $in_auth = $DB->FetchAssoc($res);

                if (isset($in_auth) && !empty($in_auth))  
                {
                    //delete from people
                    $DB->SetTable("nsau_people");
                    $DB->AddCondFS("user_id", "=", $_REQUEST['data']["id"]);
                    $DB->Delete();

                    //delete from auth
                    $DB->SetTable("auth_users");
                    $DB->AddCondFS("id", "=", $in_auth['id']);
                    $DB->Delete();

                    
                    $DB->SetTable("nsau_teachers");
                    $DB->AddCondFS("people_id", "=", $in_people['id']);
                    $res = $DB->Select(1);
                    $in_teachers = $DB->FetchAssoc($res);
                    //delete from teachers
                    $DB->SetTable("nsau_teachers");
                    $DB->AddCondFS("people_id", "=", $in_people['id']);
                    $DB->Delete();

                    //delete from curators
                    $DB->SetTable("nsau_groups_curators");
                    $DB->AddCondFS("curator_id", "=", $in_teachers['id']);
                    $DB->Delete();

                    //сообщение на том, что заявка отклонена
                    $array = parse_ini_file(INCLUDES . "Registration.ini", true);
                    $notify_settings = $array["notify_settings"];
                    if ($options["check_teachers_delete_notify"])
                    {
                        $array = array(
                            "%server_name%" => $_SERVER["SERVER_NAME"],
                            "%site_name%" => SITE_NAME,
                            "%site_short_name%" => SITE_SHORT_NAME,
                            "%mail_login%" => $tmp_register['login'],
                            "%mail_pass%" =>  $tmp_register['password'],
                            "%post_time%" => date($notify_settings["time_format"]),
                            "\\r\\n" => "\r\n"
                            );
                        $array['message2'] = $options['mail:teachers_delete_notify_message'];
                        $post = str_replace(array_keys($array), array_values($array), $notify_settings);

                        mail($in_auth['email'], '=?koi8-r?B?'.base64_encode(iconv('windows-1251', 'koi8-r', $post["subject2"])).'?=',   $post['message2'], $post['headers']);
                    }

                    $DB->SetTable("nsau_tmp_register");
                    $DB->AddCondFS("user_id", "=", $in_auth['id']);
                    $DB->Delete();
                }
            }
            break;
            //---------------------------------------------------------------------------------------------------------------


        	/** Регистрация сотрудников */
        	case 'associaters_registration':
        	{
        		echo "Регистрация сотрудников";
        	}
        	break;

        	/** Регисрация студентов */
        	case 'students_registration':
        	{
        		echo "Регистрация студентов";
        	}
        	break;

        	/** Модерация заявок */
        	case 'moderation':
        	{
        		echo "Модерация заявок на регистрацию";
        	}
        	break;
        }
	}
    /**
     * Возвращает данные для формы регистрации
     * @param  [char] $blank_type [тип бланка]
     */
    function get_registration_blank_data ($blank_type)
    {
        global $DB;
        switch ($blank_type) 
        {
            /**
             * Данные для регистрации преподавателя
             */
            case 'teachers':
            {
                /*должность*/
                $DB->SetTable("nsau_teachers_post");
                $DB->AddOrder ("pos");
                $res = $DB->Select();
                while($row = $DB->FetchAssoc($res)) 
                {
                    $this->output["teachers_post"][$row['id']] = $row;
                }

                /*звание*/
                $DB->SetTable("nsau_teachers_rank");
                $res = $DB->Select();
                while($row = $DB->FetchAssoc($res)) 
                {
                    $this->output["teachers_rank"][$row['id']] = $row;
                }

                /*ученая степень*/
                $DB->SetTable("nsau_teachers_degree");
                $res = $DB->Select();
                while($row = $DB->FetchAssoc($res)) 
                {
                    $this->output["teachers_degree"][$row['id']] = $row;
                }

                /*список групп*/
                $DB->SetTable("nsau_groups");
                $DB->AddCondFS("hidden", "=", 0);
                $DB->AddOrder("name");
                $res = $DB->Select();
                while($row = $DB->FetchAssoc($res)) 
                {
                    $this->output["groups"][$row['id']] = $row;
                }

                /*список кафедр*/
                $DB->SetTable("nsau_departments");
                $DB->AddCondFS("is_active", "=", 1);
                $DB->AddOrder ("faculty_id");
                $res = $DB->Select();
                while($row = $DB->FetchAssoc($res)) 
                {
                   $this->output["departments"][  $this->fac_on_id($row['faculty_id'])     ][   $row['id'] ] = $row;
                }
            }
            break;
        }
    }
    /**
     * Возвращает название факультета
     * @param  [int] $id [id факультета]
     * @return [text]    [название факультета]
     */
    function fac_on_id ($id)
    {
        global $DB;
        $DB->SetTable("nsau_faculties");
        $DB->AddCondFS("id", "=", $id);
        $row = $DB->FetchAssoc($DB->Select(1));
        return $row['name'];
    }
    /**
     * Возвращает название кафедры
     * @param  [int] $id [id кафедры]
     * @return [type]    [название кафедры]
     */
    function kaf_on_id ($id)
    {
        global $DB;
        $DB->SetTable("nsau_departments");
        $DB->AddCondFS("id", "=", $id);
        $row = $DB->FetchAssoc($DB->Select(1));
        return $row['name'];
    }
    /**
     * Запись преподавателя в таблицу кураторов
     * @param  [int] $teacher_id    [people id преподавателя]
     * @param  [int] $department_id [id кафедры]
     * @param  [int] $group_id      [id группы]
     */
    function insert_to_curators ($teacher_id, $department_id, $group_id)
    {
        global $DB;
        $DB->SetTable("nsau_teachers");
        $DB->AddCondFS("people_id", "=", $teacher_id);
        $res = $DB->Select(1);
        $row = $DB->FetchAssoc($res);
        $DB->SetTable("nsau_groups_curators");
        $DB->AddValue("curator_id", $row['id']);
        $DB->AddValue("group_id", $group_id);
        $DB->AddValue("department_id", $department_id);
        $DB->Insert();
    }
    /**
     * [Возвращает название должности преподавателя]
     * @param  [int] $id [id должности]
     * @return [char     [название должности]
     */
    function post_name ($id)
    {
        global $DB;
        $DB->SetTable("nsau_teachers_post");
        $DB->AddCondFS("id", "=", $id);
        $res = $DB->Select(1);
        $row = $DB->FetchAssoc($res);
        return $row['name'];
    }

    function getCurlData($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16");
        $curlData = curl_exec($curl);
        curl_close($curl);
        return $curlData;
    }
    
    function Output() 
    {
        return $this->output;
    }
}
?>
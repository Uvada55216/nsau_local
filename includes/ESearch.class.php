<?php
class ESearch
// version: 2.6
// date: 2014-05-16
{
	var $output;
	var $node_id;
	//var $module_uri;
	//var $db_prefix;
	var $mode;

	var $min_input_length;
	var $max_input_length;
	var $strip_tags;
	var $substitute;
	var $max_output_length;
	var $attach;
	var $Stem;
	var $status;
	//var $display_variant;
	var $form_data;
	var $search_modules_params;

    /* � $params ������� ����� ������ ������ ���� "form" ���� "results", � ������ "form" ������� ����� ����� � ������� id ����� ����������� � id ����
    	� � $global_params ������� ��� ini-�����, ��������, "ESearch.ini"
     */    
	function ESearch($global_params, $params, $module_id, $node_id = NULL, $module_uri = NULL, $additional = NULL)
	{
		global $Engine, $DB, $Auth;
    include_once INCLUDES . "Porter" . CLASS_EXT;
		$this->Stem = new Soundex_Stem_Ru();
		$this->output = array();
		$this->output["messages"] = array("good" => array(), "bad" => array());
		$this->node_id = $node_id;


		$this->output["display_access"] = $this->display_access = 1;
		$this->output["full_access"] = $this->output["manage_access"] = $this->manage_access = $this->full_access = $Auth->logged_in;
		$parts = explode(";", $params);
		$this->output["mode"] = $this->mode = $parts[0];


		switch ($this->mode)
		{
			case "form":
				if (isset($parts[1]) && CF::IsNaturalNumeric($parts[1]))
				{
					$this->output["results_folder"] = $Engine->FolderURIbyID($parts[1]);
				}

				else
				{
					die("ESearch module error #1001 at node $this->node_id: illegal RESULTS FOLDER ID.");
				}


				/*if (isset($parts[2]) && CF::IsNaturalNumeric($parts[2]))
				{
					$this->output["results_node_id"] = intval($parts[2]);
				}

				else
				{
					die("ESearch module error #1002 at node $this->node_id: illegal RESULTS NODE ID.");
				}*/
				break;


			case "results":
				$this->search_modules_params = isset($parts[1]) ? $parts[1] : false;
				$this->ProcessINI($global_params);
				$this->Results();
				break;


			default:
				die("ESearch module error #1100 at node $this->node_id: unknown mode &mdash; $this->mode.");
				break;
		}


		$this->output["status"] = $this->status;
		//$this->output["display_variant"] = $this->display_variant;
		$this->output["form_data"] = $this->form_data;
	}



	function ProcessINI($config_file)
	{
		if (!$config_file)
		{
			die("ESearch module error #1001: config file not specified.");
		}

		elseif (!$result = @parse_ini_file(INCLUDES . $config_file, true))
		{
			die("ESearch module error #1002: cannot process config file '$config_file'.");
		}


		$this->output["min_input_length"] = $this->min_input_length = (int) $result["general_settings"]["min_input_length"];
		$this->output["max_input_length"] = $this->max_input_length = (int) $result["general_settings"]["max_input_length"];
//		$this->strip_tags = (bool) $result["general_settings"]["strip_tags"];
//		$this->substitute = (bool) $result["general_settings"]["substitute"];
		$this->output["max_output_length"] = $this->max_output_length = (int) $result["general_settings"]["max_output_length"];
		$this->output["method"] = $this->method = $result["general_settings"]["method"];
		$this->attach = array();
		if ($this->search_modules_params) 
		{ 
			$this->attach = explode(',', $this->search_modules_params);
			foreach ($this->attach as $ind=>$value)
				$this->attach[$ind] = str_replace('-', ';', $value);
		}
		else
		foreach ($result["attach"] as $key => $elem)
		{
			$this->attach[$key] = $elem;
		}
	}
	
	function Results()
	{
		global $DB, $Auth, $Engine;

		//��� ��� news_items
		// if($Auth->usergroup_id==1) {
			// set_time_limit(0);
			// $DB->SetTable("news_items");
			// $DB->AddField("cached_text");
			// $DB->AddField("id");
			// $DB->AddField("title");
			// $res = $DB->Select();
			// while($row = $DB->FetchAssoc($res)) {
				// $cached_text = $this->Stem->encodeText($row["title"]." ".$row["title"]." ".$row["cached_text"]);
				// $DB->SetTable("news_items");
				// $DB->AddCondFS("id", "=", $row["id"]);
				// $DB->AddValue("cached_text", $cached_text);
				// $DB->Update();
			// }
			// return;
		// }
			//��� ��� text_items
			// if($Auth->user_id==31251) {
			// set_time_limit(0);
			// $DB->SetTable("text_items");
			// $DB->AddField("cache");
			// $DB->AddField("id");
			// $res = $DB->Select();
			// while($row = $DB->FetchAssoc($res)) {
				// $DB->SetTable("engine_nodes", "en");
				// $DB->AddCondFS("en.is_active", "=", 1);
				// $DB->AddCondFS("en.module_id", "=", 1);
				// $DB->AddCondFS("en.params", "=", $row["id"]);
				// $DB->AddTable("engine_folders", "ef");
				// $DB->AddCondFF("ef.id", "=", "en.folder_id");
				// $DB->AddField("ef.title", "title");
				// $DB->AddField("ef.id", "folder_id");
				// $t_res = $DB->Select(1);
				// if($t_row = $DB->FetchAssoc($t_res)) 
					// $title = $t_row["title"]." ".$t_row["title"];
				// else
					// $title = "";
				// $cached_text = $this->Stem->encodeText($title." ".$row["cache"]);
				// $DB->SetTable("text_items");
				// $DB->AddCondFS("id", "=", $row["id"]);
				// $DB->AddValue("search_cache", $cached_text);
				// $DB->Update();
			// }
			// return;	
			// }			
			//�������������� ����
			// $handle = @fopen(INCLUDES . "news.txt", "r");
			// if ($handle) {
			// while (($buffer = fgets($handle)) !== false) {
					// $DB->Exec($buffer);
			// }
			// if (!feof($handle)) {
					// echo "Error: unexpected fgets() fail\n";
			// }
			// fclose($handle);
			// }
			// $DB->SetTable("news_items");
			// $DB->AddAltFS("id", "=", "890");
			// $res = $DB->Select();
			// while($row = $DB->FetchAssoc($res)) {
				// print_r($row);
			// }			
		$utfString = iconv("utf-8", "cp1251", $_GET["search"]);
		$_GET["search"] = $utfString ? $utfString : urldecode(iconv("utf-8", "cp1251", urlencode($_GET["search"])));
		
		if (isset($_POST["search"]) || $_POST["search"]) {
				$search_string = trim($_POST["search"]);
		}
		else {
			if (isset($_GET["search"]) || $_GET["search"])
				$search_string = trim($_GET["search"]);
		}		
		$search = $search_string;
		if(!$this->validateSearchString($search_string))
			return false;
		else
			$search_string = $this->validateSearchString($search_string);
		include_once INCLUDES . "Ensau" . CLASS_EXT;
		$ENsau = new ENsau(null,"search_peoples;".$search_string);
		$this->output["results"][1]["results"] = $ENsau->output["results"];
		$this->output["results"][1]["count"] = count($this->output["results"][1]["results"]);
		$rel = 0.6;
		$search_string = $this->Stem->getSoundexString($this->Stem->getStemString($this->Stem->getText($search_string)))." ".$search_string." ".$this->Stem->getStemString($this->Stem->getText($search_string));//`is_active`!=NULL AND
		$sql = "SELECT `id`, `cat_id`, `short_text`, `title`, `full_text`, MATCH (title, short_text, cached_text) AGAINST ('$search_string' IN NATURAL LANGUAGE MODE) AS REL FROM  `news_items`";
		$sql .= "WHERE `is_active`!=0 AND  MATCH (title, short_text, cached_text) AGAINST ('$search_string' IN NATURAL LANGUAGE MODE)>".$rel."";// WHERE MATCH (title, short_text, cached_text) AGAINST ('$search_string' IN NATURAL LANGUAGE MODE)
		$sql .= " UNION ALL SELECT `id`, `id`, `id`, `id`, `cache`, MATCH (cache, search_cache) AGAINST ('$search_string' IN NATURAL LANGUAGE MODE) AS REL FROM `text_items` WHERE  MATCH (cache, search_cache) AGAINST ('$search_string' IN NATURAL LANGUAGE MODE)>".$rel." ORDER BY REL DESC";
		$res = $DB->Exec($sql);
		$count_items = $DB->NumRows($res);
		$this->output["count_items"] = $count_items+$this->output["results"][1]["count"];
		require_once INCLUDES . "Pager" . CLASS_EXT;
		$Pager = new Pager($count_items, 10, 0);
		$this->output["pager_output"]  = $Pager->Act();
		$from = $this->output["pager_output"]["db_from"];
		$limit = $this->output["pager_output"]["db_limit"];
		if($_GET["page"])
		{
			$from=$limit*$_GET["page"]-$limit;
		}
		$sql = "SELECT `id`, `cat_id`, `short_text`, `title`, `full_text`, MATCH (title, short_text, cached_text) AGAINST ('$search_string' IN NATURAL LANGUAGE MODE) AS REL FROM  `news_items`";
		$sql .= "WHERE  `is_active`!=0 AND MATCH (title, short_text, cached_text) AGAINST ('$search_string' IN NATURAL LANGUAGE MODE)>".$rel."";// WHERE MATCH (title, short_text, cached_text) AGAINST ('$search_string' IN NATURAL LANGUAGE MODE)
		$sql .= " UNION ALL SELECT `id`, `id`, `id`, `id`, `cache`, MATCH (cache, search_cache) AGAINST ('$search_string' IN NATURAL LANGUAGE MODE) AS REL FROM `text_items` WHERE MATCH (cache, search_cache) AGAINST ('$search_string' IN NATURAL LANGUAGE MODE)>".$rel." ORDER BY REL DESC LIMIT ".$from.",".$limit."";
		$res = $DB->Exec($sql);		
		while($row = $DB->FetchAssoc($res)) {
			if($row["id"]==$row["title"]) {
				$DB->SetTable("engine_nodes", "en");
				$DB->AddCondFS("en.is_active", "=", 1);
				$DB->AddCondFS("en.module_id", "=", 1);
				$DB->AddCondFS("en.params", "=", $row["id"]);
				$DB->AddTable("engine_folders", "ef");
				$DB->AddCondFF("ef.id", "=", "en.folder_id");
				$DB->AddField("ef.title", "title");
				$DB->AddField("ef.id", "folder_id");
				$t_res = $DB->Select(1);
				if($t_row = $DB->FetchAssoc($t_res)) {
					$result["title"] = $t_row["title"];
					$folder = $Engine->FolderDataByID($t_row["folder_id"]);		
					$result["uri"] = $folder["uri"];
					$result["text"] = strip_tags($row["full_text"]);
					$result["rel"] = $row["REL"];
				}
				else continue;	
			}
			else {
				$DB->SetTable("news_cats");
				$DB->AddCondFS("id", "=", $row["cat_id"]);
				$n_res = $DB->Select(1);
				$n_row = $DB->FetchAssoc($n_res);
				$folder_id = $n_row["folder_id"];
				$result["uri"] = ''.($folder_id ?  $Engine->FolderURIbyID($folder_id) : '/news/').$row["id"].'.html';
				$result["title"] = $row["title"];
				$result["text"] = strip_tags($row["full_text"]);
				$result["rel"] = $row["REL"];
			}
			$results[] = $result;
		}
		if(count($results)>0) {	
			$output["results"] = $results;
					$mb_str = $this->Stem->getTrueString($results, trim($search)); 
		$search = implode(" ", $mb_str);
		$this->output["results"][0]["results"] = $output["results"];
		$this->output["results"][0]["count"] = count($output["results"]);
		$this->output["results"][0]["results"] = $this->Highlight($search, $this->output["results"][0]["results"]);			
		}
		else	{

		}
	}

	function Light($text, $string) {
		$s = explode(" ", $string);
		foreach($s as $word){
			if(mb_strlen($word)>=3)
				$text = str_replace($word, "<b>".$word."</b>", $text);
		}
		return $text;
	}
	function Highlight($search_string, $results)
	{
		$add_strings_len = 200;
		foreach($results as $ind=>$result)
		{	
			$text = $result['text'];
			$occur_results = array();
			while ($pos = stripos($text, $search_string)) {
				if ($pos - $add_strings_len < 0)
				{
					$before_string = substr($text, 0, $pos);
				}
				else
				{
					$before_string = '...'.substr($text, $pos-$add_strings_len, $add_strings_len);
				}
				$after_string = substr($text, $pos + strlen($search_string), $add_strings_len);
				$occur_results[] = $before_string.'<strong>'.substr($text, $pos, strlen($search_string)).'</strong>'.$after_string.'...';
				$text = substr($text, $pos + strlen($search_string) + 1);
				
			}
			if(empty($occur_results)) $occur_results[] = $this->Light(substr($text, 0, $add_strings_len),$search_string);
			$results[$ind]['text'] = $occur_results;
		}
		return $results;
	}
//��������� ��������� ������, false - �� ������ ��������, (string)$search_string - ������� ������
	function validateSearchString($string) 
	{
		if (empty($string)) {
			$this->output["messages"]["bad"][] = 301; // �� ������ ��������� ������
			return false;
		}
		$search_string = trim($string);
		$s = explode(" ", $search_string);
		foreach($s as $w)
			if(strlen($w)>2)
				$r[] = $w;
		$search_string = implode(" ", $r);
		$search_string_length = strlen($search_string);
		if ($search_string_length < $this->min_input_length) {
			$this->output["messages"]["bad"][] = 302; // ��������� ������ ������ ���������� ����������
			return false;
		}
		if ($search_string_length > $this->max_input_length+64) {
			$this->output["messages"]["bad"][] = 303; // ��������� ������ ������ ����������� ����������
			return false;
		}
		return 	$search_string;
	}
//	
	
	
	function Output()
	{
		return $this->output;
	}
}

?>
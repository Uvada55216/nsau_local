<?php

class ETasks
// version: 1.3.5
// date: 2013-11-21
{
    var $module_id;
    var $node_id;
    var $module_uri;
    var $privileges;
    var $output;
		var $messages;

		
    var $mode;
    var $db_prefix;
    var $maintain_cache;
    var $item_id;
    var $items_num;
    
		
    var $users;
	var $settings;
	
		

	function ETasks($global_params, $params, $module_id, $node_id, $module_uri) {
		global $Engine, $Auth, $DB;
		$this->output["messages"] = array("good" => array(), "bad" => array());
		//if (is_null($global_params))
		
		if ((!($Auth->usergroup_id && $Engine->OperationAllowed($module_id, 'tasks.handle', -1, $Auth->usergroup_id)) && ( $params != 'apply_support' && $params != 'apply_support_link' || !($Auth->usergroup_id && $Engine->OperationAllowed($module_id, 'tasks.apply_support', -1, $Auth->usergroup_id) )) ) && !$Engine->OperationAllowed("21", "tasks.add.request", -1, $Auth->usergroup_id)) {
			$this->output = "acc_denied";
		} else {
			$this->settings = @parse_ini_file('ETasks.ini', true);
			$this->module_id = $module_id;	
			$this->module_uri = $module_uri;
			$this->users = array();
			$tasks_system_usergroups = array();
			$DB->SetTable("auth_usergroups");
			if ($res_groups = $DB->Select()) {
				while ($row = $DB->FetchAssoc($res_groups)) {
					if ($Engine->OperationAllowed($module_id, 'tasks.handle', -1, $row['id']))
						$tasks_system_usergroups[] = $row['id'];
				}
				$DB->FreeRes($res_groups);
			}
			
			
			if (!empty($tasks_system_usergroups)) {
				$DB->SetTable("auth_users");
				foreach ($tasks_system_usergroups as $usergroup) 
					$DB->AddAltFS('usergroup_id', '=', $usergroup);
				$DB->AppendAlts();
				$DB->AddCondFS('is_active', '=', 1);
				$res_users = $DB->Select();
				while ($row = $DB->FetchAssoc($res_users))
					$this->users[] = array('id' => $row['id'], 'name'=> $row['displayed_name']);
					
				$DB->FreeRes($res_users);
			}
			$this->output['scripts_mode'] = $this->output['mode'] = 'tasks';

			$this->output['full_task_statuses'] =  array("new", "current", "tobechecked", "done");
			$this->output['module_id'] = $module_id;
		
			if ($params) {  
				$parts = explode(';', $params);
				switch($parts[0]) { 
					case 'executor_reports':
					
						$this->output = array();
						$this->output['id'] = '0';
						$this->output['module_id'] = $module_id;
						$this->output['show_mode'] = 'executor_reports';
						$this->output['full_task_statuses'] = $this->output['task_statuses'] = array("new", "current", "tobechecked", "done");
						if (isset($_REQUEST['reload_data']) && !empty($_REQUEST['reload_data'])) $this->output['tasks']['executor_reports'] = $this->GetTasks($_REQUEST['reload_data']);
						
						if ($this->module_uri == 'print/')
							$this->output['to_print'] = true;
						$this->output['users'] = $this->users;
						$this->output['scripts_mode'][] = $this->output['mode'] = 'tasks';
						$this->output["scripts_mode"][] = 'input_calendar';
						 
						break;
					case 'add_task':
						$this->output = (isset($_REQUEST['add_data']) && $this->AddTask($_REQUEST['add_data']) ) ? '������ ���������' : '������ ��� ���������� ������. ���������� ��������� �������� ��� ���.';
						break;
					case 'update_executors':
						$this->output = array();
						$this->output[] = array('msg' => (isset($_REQUEST['update_data'], $_REQUEST['update_data']['task_id'], $_REQUEST['update_data']['executor_id'], $_REQUEST['update_data']['old_executor_id']) && $updateRes = $this->UpdateExecutors($_REQUEST['update_data']['executor_id'], $_REQUEST['update_data']['task_id'], $_REQUEST['update_data']['old_executor_id']) )  ? iconv('windows-1251', 'utf-8', '����������� ��������') : iconv('windows-1251', 'utf-8', '������ ��� ���������� �����������. ���������� ��������� �������� ��� ���.'));
						$this->output[] = array('responseData'=> $updateRes);
						
						//$this->output = (isset($_REQUEST['add_data'], $_REQUEST['add_data']['task_id'], $_REQUEST['add_data']['executor_id'], $_REQUEST['add_data']['is_new_executor']) && $this->UpdateExecutors($_REQUEST['add_data']['executor_id'], $_REQUEST['add_data']['task_id'], $_REQUEST['add_data']['is_new_executor']) ) ? '����������� ��������' : '������ ��� ���������� �����������. ���������� ��������� �������� ��� ���.';
						break;
					case 'unassign_task':
						$this->output = (isset($_REQUEST['unassign_data']) && $this->UnassignTask($_REQUEST['unassign_data']) ) ? '������ �����' : '������ ��� ������ ������. ���������� ��������� �������� ��� ���.';;
						break;
					case 'update_task':
						$this->output = array();
						$this->output[] = array('msg' => (isset($_REQUEST['update_data']) && $updateRes = $this->UpdateTask($_REQUEST['update_data']) ) ? '������ ���������' : '������ ��� ���������� ������. ���������� ��������� �������� ��� ���.');
						$this->output[] = array('responseData'=> $updateRes);
						break;
					case 'update_pager':
						break;
					case 'reload_tasks':
						
						//header("Content-type: application/json; charset=windows-1251");
						header('Content-Type: text/html,  charset=windows-1251');
						header("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
						header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
						//header("Expires: " .  date("r", time() - 24*7*3600));
						Header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
						header("Pragma: no-cache");
						$this->output = array();
						//echo "<h1>",  date("H:i:s"), "</h1>";//exit;		
						$filters = isset($_REQUEST['reload_data']['filters']['statuses'])? explode(';', $_REQUEST['reload_data']['filters']['statuses']) : false;
						if ($filters) $_SESSION['tasks_user_filters'] = serialize($filters);
						$tasks = $this->GetTasks($_REQUEST['reload_data']);
						$this->output = (isset($_REQUEST['reload_data']) ? $tasks : false);
						
						if ($this->module_uri != 'print/' && $tasks) {
							list($this->items_num) = $this->GetTasksCount($_REQUEST['reload_data']['filters']);
							require_once INCLUDES . "Pager.class";
							$Pager = new Pager($this->items_num, $this->settings['pager']['items_per_page'], $this->settings['pager']['page_limit'], null, null, array('page'=>intval($_REQUEST['reload_data']['page'])));
							$this->output[] =  array('pager_output'=>$Pager->Act());
						} else {
							$this->output[] =  array('pager_output'=> array());
						}//echo "<h1>",  date("H:i:s"), "</h1>";//exit;							
					break;
					case 'user_task_count': {
						if($Auth->usergroup_id && ($Engine->OperationAllowed($module_id, 'tasks.handle', -1, $Auth->usergroup_id) || $Engine->OperationAllowed($module_id, 'tasks.edit_all', -1, $Auth->usergroup_id))) {
							$this->GetTaskCount($Auth->user_id);
							$this->output['scripts_mode'] = $this->output['mode'] = 'user_task_count';
						}
					}
					break;
					case 'task_count': {
						if (!is_array(unserialize($_SESSION['tasks_user_filters']))) $_SESSION['tasks_user_filters'] = serialize(array("new", "current", "tobechecked", "done"));
						$filters = isset($_SESSION['tasks_user_filters']) ? unserialize($_SESSION['tasks_user_filters']) : false;
						$this->output = array();
						$this->output['users'] = $this->users;
						$this->output['tasks'] = array();
						$this->output['tasks']['my'] = $this->GetUserTasks($Auth->user_id, array('statuses'=>implode(';', $filters)));
						$this->output['show_mode'] = 'my';
						$this->output['id'] = $Auth->user_id;
						$this->output['module_id'] = $module_id;
						$this->output['full_task_statuses'] =  array("new", "current", "tobechecked", "done");
						$this->output['task_statuses'] = $filters  ?: $this->output['full_task_statuses']; 
						$this->output['statuses_titles'] = array("new" => '�����', "current"=> '�����������', "tobechecked"=> '�� ��������', "done"=> '���������');
						$this->output['edit_all'] = $Engine->OperationAllowed($module_id, "tasks.edit_all", -1, $Auth->usergroup_id);
						
						
						// ������������� pager-�
						list($this->items_num) = $this->GetTasksCount(array('author' => $Auth->user_id, 'statuses'=>implode(';',$filters)));
						require_once INCLUDES . "Pager.class";
						$Pager = new Pager($this->items_num, $this->settings['pager']['items_per_page'], $this->settings['pager']['page_limit']); 
						$this->output["pager_output"] = $Pager->Act();
				
						if($Auth->usergroup_id && ($Engine->OperationAllowed($module_id, 'tasks.handle', -1, $Auth->usergroup_id) || $Engine->OperationAllowed($module_id, 'tasks.edit_all', -1, $Auth->usergroup_id))) {
							$this->GetTaskCount($Auth->user_id);
							$this->output['scripts_mode'] = $this->output['mode'] = 'task_count';
						}
					}
					break;
					case "apply_support_link": {
						$this->output['scripts_mode'] = $this->output['mode'] = 'apply_support_link';
						$DB->AddTable("engine_nodes");
						$DB->AddCondFS("params", " LIKE ", "%apply_support%");
						$DB->AddCondFS("module_id", "=", $this->module_id);
						$DB->AddField("folder_id");
						$res = $DB->Select(1);
						while($row = $DB->FetchAssoc($res)) {
							$folder_id = $row["folder_id"];
							$this->output["apply_uri"] = $Engine->FolderURIbyID($folder_id);
						}
						$this->output["allow_handle"] = $Engine->OperationAllowed($module_id, 'tasks.handle', -1, $Auth->usergroup_id);
					}
					break;
					case "apply_support": {
						$this->output['scripts_mode'] = $this->output['mode'] = 'apply_support';
						$this->output['module_id'] = $module_id;
						$this->ApplySupport(isset($_REQUEST["add_data"]) ? $_REQUEST["add_data"] : null);
					}
					break;
					case "reports" : {
						// $handle = @fopen("includes/" . "req.csv", "r");
						// if ($handle) {
							// while (($buffer = fgets($handle)) !== false) {
								// $parts = explode(";", $buffer);
								// $this->AddRequestCSV($parts[0], trim($parts[1]));
							// }
						// }	
						if(!empty($_POST["actual_from"]) && !empty($_POST["actual_to"])) {
							if ($Engine->OperationAllowed($this->module_id, "tasks.cart.edit", -1, $Auth->usergroup_id) && (($Auth->usergroup_id==10) || ($Auth->usergroup_id==1)))	
							$this->output["allow_edit"]=true;			
							if($this->output["allow_edit"]) {
									$f = explode(".", $_POST["actual_from"]); 
									$f = array_reverse($f);
									$from  = implode("-", $f);
									$t = explode(".", $_POST["actual_to"]); 
									$t = array_reverse($t);
									$to  = implode("-", $t);
									$date_t = new DateTime($to);
									$date_f = new DateTime($from);
									$date_to = $date_t->format('Y-m-d');
									$date_from = $date_f->format('Y-m-d');
									switch($_POST["report_type"]) {
										case "stats": {								
											$this->output['mode'] = 'reports';									
											$DB->SetTable("cartrige_requests");
											$DB->AddCondFS("date_refilled", ">=", $date_from);
											$DB->AddCondFS("date_refilled", "<=", $date_to);
											$DB->AddOrder("date_refilled");
											$res = $DB->Select();
											$requests = array();
											while($row = $DB->FetchAssoc($res)) {
												$DB->SetTable("cartrige_register", "cr");
												$DB->AddCondFS("cr.id", "=", $row["register_id"]);
												$DB->AddTable("cartrige_units", "cu");
												$DB->AddCondFF("cr.unit_id", "=", "cu.id");
												$DB->AddTable("cartrige_units", "csu");
												$DB->AddCondFF("cr.subunit_id", "=", "csu.id");
												$DB->AddTable("cartrige_list", "cl");
												$DB->AddCondFF("cr.cartrige_id", "=", "cl.id");
												$DB->AddTable("cartrige_manufacturers", "cm");
												$DB->AddCondFF("cl.manufacturer_id", "=", "cm.id");
												$DB->AddField("cu.id", "unit_id");
												$DB->AddField("cu.name", "unit_name");
												$DB->AddField("csu.id", "subunit_id");
												$DB->AddField("csu.name", "subunit_name");
												$DB->AddField("cr.cartrige_uid", "cartrige_uid");
												$DB->AddField("cl.id", "cartrige_id");
												$DB->AddField("cl.name", "cartrige_name");
												$DB->AddField("cl.manufacturer_id", "manufacturer_id");
												$DB->AddField("cl.restore_price", "restore_price");
												$DB->AddField("cl.refill_price", "refill_price");
												$DB->AddField("cm.name", "manufacturer");									
												$req = $DB->Select();
												$request = $DB->FetchAssoc($req);
												$request["date_refilled"] = CF::reverseDate($row["date_refilled"]);
												$requests[$request["manufacturer_id"]][$request["cartrige_uid"]] = $request;
											}				
											$this->output["requests"] = $requests;																																																						
										}
										break;
										case "requests": {
											$this->output['mode'] = 'requests_report';
											$DB->SetTable("cartrige_requests","creq");
											$DB->AddCondFS("creq.date", ">=", $date_from);
											$DB->AddCondFS("creq.date", "<=", $date_to);
											// $DB->AddTable("cartrige_register", "cr");
											// $DB->AddCondFF("cr.id", "=", "creq.register_id");
											// $DB->AddTable("cartrige_units", "cu");
											// $DB->AddCondFF("cr.unit_id", "=", "cu.id");
											// $DB->AddOrder("cu.name");
											// $DB->AddField("creq.register_id", "register_id");
											$res = $DB->Select();
											$requests = array();
											while($row = $DB->FetchAssoc($res)) {
												$DB->SetTable("cartrige_register", "cr");
												$DB->AddCondFS("cr.id", "=", $row["register_id"]);
												$DB->AddTable("cartrige_units", "cu");
												$DB->AddCondFF("cr.unit_id", "=", "cu.id");
												$DB->AddTable("cartrige_units", "csu");
												$DB->AddCondFF("cr.subunit_id", "=", "csu.id");
												$DB->AddTable("cartrige_list", "cl");
												$DB->AddCondFF("cr.cartrige_id", "=", "cl.id");
												$DB->AddTable("auth_users", "au");
												$DB->AddCondFS("au.id", "=", $row["people_id"]);
												$DB->AddTable("cartrige_manufacturers", "cm");
												$DB->AddCondFF("cl.manufacturer_id", "=", "cm.id");
												$DB->AddField("cu.id", "unit_id");
												$DB->AddField("cu.name", "unit_name");
												$DB->AddField("csu.id", "subunit_id");
												$DB->AddField("csu.name", "subunit_name");
												$DB->AddField("cr.cartrige_uid", "cartrige_uid");
												$DB->AddField("cl.id", "cartrige_id");
												$DB->AddField("cl.name", "cartrige_name");
												$DB->AddField("cl.manufacturer_id", "manufacturer_id");
												$DB->AddField("cl.restore_price", "restore_price");
												$DB->AddField("cl.refill_price", "refill_price");
												$DB->AddField("cm.name", "manufacturer");								
												$DB->AddField("au.displayed_name", "username");								
												$req = $DB->Select();
												$request = $DB->FetchAssoc($req);
												$requests[$row["id"]] = $request;
												$requests[$row["id"]]["date_send"] = CF::reverseDate($row["date_send"]);
												$requests[$row["id"]]["date_refilled"] = CF::reverseDate($row["date_refilled"]);
												$requests[$row["id"]]["status"] = $row["status"];
												$requests[$row["id"]]["id"] = $row["id"];
											}


											foreach ($requests as $key => $r) {
											    $unit[$key]  = $r['unit_name'];
											    $subunit[$key]  = $r['subunit_name'];
											}
											array_multisort($unit, SORT_ASC, $subunit, SORT_ASC, $requests);


											// $requests = sort($requests);
											$this->output["requests"] = $requests;																													
										}
										break;
										case "cartriges": {
											$this->output['mode'] = 'cartriges_report';
											$DB->SetTable("cartrige_register", "cr");
											$DB->AddTable("cartrige_list", "cl");
											$DB->AddCondFF("cr.cartrige_id", "=", "cl.id");
											$DB->AddTable("cartrige_manufacturers", "cm");
											$DB->AddCondFF("cl.manufacturer_id", "=", "cm.id");
											$DB->AddField("cm.name", "manufacturer");	
											$DB->AddField("cl.name", "cartrige_name");
											$DB->AddField("cl.id", "cartrige_id");
											$DB->AddField("cl.manufacturer_id", "manufacturer_id");
											$res = $DB->Select();
											while($row = $DB->FetchAssoc($res)) {
												if(!empty($requests[$row["manufacturer_id"]][$row["cartrige_id"]]["total"]))
													$tmp = $requests[$row["manufacturer_id"]][$row["cartrige_id"]]["total"];
												else unset($tmp);	
												$requests[$row["manufacturer_id"]][$row["cartrige_id"]] = $row;		
												$requests[$row["manufacturer_id"]][$row["cartrige_id"]]["total"] = $tmp+1;												
											}
											$this->output["requests"] = $requests;	
										}
										break;							
									}		
									$filename = str_replace(".", "_", $_POST["actual_from"]."-".$_POST["actual_to"]);		

									header('Content-Type: text/html; charset=windows-1251');
									header('P3P: CP="NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM"');
									header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
									header('Cache-Control: no-store, no-cache, must-revalidate');
									header('Cache-Control: post-check=0, pre-check=0', FALSE);
									header('Pragma: no-cache');
									header('Content-transfer-encoding: binary');
									header('Content-Disposition: attachment; filename='.$_POST["report_type"].'_'.$filename.'.xls');
									header('Content-Type: application/x-unknown');	
									// if($Auth->user_id==31251)
// echo 1;	
							}
						}						
					}
					break;
					case "refill_cart": {
						//���������
						// $trimmed = file(INCLUDES . 'cartriges.txt', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
						// $this->output['carts']=$trimmed; 
						// foreach($trimmed as $cartrige) {
							// $cartrige = trim($cartrige);
							// $DB->SetTable("cartrige_list");
							// $DB->AddCondFS("name", "=", $cartrige);
							// $res = $DB->Select();
							// $count = $DB->NumRows($res);
							// if($count==0) {
								// $DB->SetTable("cartrige_list");
								// $DB->AddValue("name", $cartrige);
								// $DB->Insert();
							// }
						// }
						//���� �� ���������
						// $trimmed = file(INCLUDES . 'price.csv', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
						// foreach($trimmed as $cartrige) {
							// $cartrige = trim($cartrige);
							// $cartrige = explode(";", $cartrige);
							// $DB->SetTable("cartrige_list");
							// $DB->AddCondFS("name", "LIKE", "%".$cartrige[0]."%");
							// $DB->AddValue("restore_price", str_replace(",", ".", $cartrige[2]));
							// $DB->AddValue("refill_price", str_replace(",", ".", $cartrige[1]));
							// $DB->Update();
							
							// $DB->SetTable("cartrige_list");
							// $DB->AddCondFS("name", "LIKE", "%".$cartrige[0]."%");
							// $res = $DB->Select();
							// $row = $DB->FetchAssoc($res);
							// if(empty($row))
								// if($Auth->usergroup_id==1)
									// echo $cartrige[0]."<br>";
						// }
						//�������������
						// $lines = file(INCLUDES . 'cartrige_register.csv', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
						// $j=0;
						// for($i=0; $i<=count($lines); $i++) {
							// $line = explode(";", $lines[$i]);
							
							// if($line[0]=="unit") {
								// $j++;
								// $DB->SetTable("cartrige_units");
								// $DB->AddValue("name", $line[1]);
								// $DB->AddValue("pid", "0");
								// $DB->Insert();								
								// $pid = $j;
								// $this->output['carts'][$pid]["name"] = $line[1];
							// }
							// if($line[0]=="sunit") {
								// $j++;
								// $DB->SetTable("cartrige_units");
								// $DB->AddValue("name", $line[1]);
								// $DB->AddValue("pid", $pid);
								// $DB->Insert();
								// $this->output['carts'][$pid]["sss"][] = $line[1];
							// }							
						// }
						// ����������� ����������
						// $lines = file(INCLUDES . 'cartrige_register.csv', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
						// $unit_id = 0;
						// $subunit_id = 0;
						// for($i=0; $i<=count($lines); $i++) {
							// $line = explode(";", $lines[$i]);
							// if($line[0]=="sunit") {
								// $DB->SetTable("cartrige_units");
								// $DB->AddCondFS("name", "=", $line[1]);
								// $un = $DB->Select(1);
								// $row = $DB->FetchAssoc($un);
								// $subunit_id = $row["id"];
								// $unit_id = $row["pid"];
							// }
							// elseif($line[0]!="unit") {
								// $DB->SetTable("cartrige_list");
								// $DB->AddCondFS("name", "=", $line[0]);
								// $res = $DB->Select(1);
								// if($DB->NumRows($res)==0) {
									// $DB->SetTable("cartrige_list");
									// $DB->AddValue("name", $line[0]);
									// $DB->Insert();
									// $DB->SetTable("cartrige_list");
									// $DB->AddCondFS("name", "=", $line[0]);
									// $res = $DB->Select(1);
								// }
								// $row = $DB->FetchAssoc($res);
								// $cart_id = $row["id"];
								// $DB->SetTable("cartrige_register");
								// $DB->AddValue("cartrige_id", $cart_id);
								// $DB->AddValue("cartrige_uid", ($line[1] ? $line[1] : 0));
								// $DB->AddValue("unit_id", $unit_id);
								// $DB->AddValue("subunit_id", $subunit_id);
								// $DB->AddValue("user_id", 31251);
								// $DB->Insert();
								
								// $this->output['carts'][$unit_id][$subunit_id][] = $line[0]; 
							// }
					
							
						// }										
						$this->output['params']=$parts;
						$this->output['scripts_mode'] = $this->output['mode'] = 'refill_cart';
						if ($Engine->OperationAllowed($this->module_id, "tasks.add.request", -1, $Auth->usergroup_id))
							$this->output["allow_request"]=true;
						if ($Engine->OperationAllowed($this->module_id, "tasks.cart.edit", -1, $Auth->usergroup_id) && (($Auth->usergroup_id==10) || ($Auth->usergroup_id==1)))	
							$this->output["allow_edit"]=true;		
						
						$DB->SetTable("cartrige_manufacturers");
						$res_m = $DB->Select();
						while($row_m = $DB->FetchAssoc($res_m))
							$this->output["manufacturers"][$row_m["id"]] = $row_m["name"];
						
						if(!empty($parts[1]))
						{
							header("Cache-Control: no-cache, must-revalidate");
							header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
							header("Pragma: no-cache");
							$json=array();
							switch($parts[1])
							{
								case "subunit": {
									$sub_id = intval($_REQUEST['data']["id"]);
									$json = $this->RefillCart("units", $sub_id);
									$this->output["json"]=$json;
								}
								break;
								case "cartrige_list": {
									$json = $this->RefillCart("list");
									$this->output["json"]=$json;
								}
								break;
								case "search_user": {
									$request = trim($_REQUEST["data"]["s"]);
									$result = array();
									if($request != "")
									{
										$request = trim($request);
										$sql = "SELECT * FROM  `auth_users` WHERE  `displayed_name` LIKE  '%".iconv("utf-8", "Windows-1251", $request)."%' AND `is_active`='1'";
										$res = $DB->Exec($sql);
										$count = 0; 
										while($row = $DB->FetchAssoc($res)) {
											$result[$count]["name"] = $row["displayed_name"];
											$result[$count]["id"] = $row["id"];
											$count++; 
											if($count >= 30) break; 
										}
										header("Cache-Control: no-cache, must-revalidate");
										header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
										header("Pragma: no-cache");  
									} else $result = "";
									$this->output["json"] = $result;	
								}
								break;
								case "search_cartrige": {
									$request = trim($_REQUEST["data"]["s"]);
									$result = array();
									if($request != "" || !empty($_REQUEST["data"]["id"]))
									{
										$request = trim($request);
										if(empty($_REQUEST["data"]["id"]))
											$sql = "SELECT * FROM  `cartrige_register` WHERE  `cartrige_uid` LIKE  '%".iconv("utf-8", "Windows-1251", $request)."%'";
										else 
											$sql = "SELECT * FROM  `cartrige_register` WHERE  `id` = ".$_REQUEST["data"]["id"]."";
										// $this->output["json"]["sql"] = $sql;
										$res = $DB->Exec($sql);  
										$count = 0; 
										while($row = $DB->FetchAssoc($res)) {
											$DB->SetTable("cartrige_list");
											$DB->AddCondFS("id", "=", $row["cartrige_id"]);
											$c = $DB->Select(1);
											$cart = $DB->FetchAssoc($c);
											$DB->SetTable("cartrige_units");
											$DB->AddCondFS("id", "=", $row["unit_id"]);
											$u = $DB->Select(1);
											$unit = $DB->FetchAssoc($u);
											$DB->SetTable("cartrige_units");
											$DB->AddCondFS("id", "=", $row["subunit_id"]);
											$su = $DB->Select(1);
											$subunit = $DB->FetchAssoc($su);
											$result[$count]["name"] = $cart["name"]." (".$row["cartrige_uid"].")";
											$result[$count]["cartrige_name"] = $cart["name"];
											$result[$count]["unit"] = $unit["name"];
											$result[$count]["subunit"] = $subunit["name"];
											$result[$count]["uid"] = $row["cartrige_uid"];
											$result[$count]["id"] = $row["id"];
											$count++; 
											if($count >= 300) break; 
										}
										header("Cache-Control: no-cache, must-revalidate");
										header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
										header("Pragma: no-cache");  
									} else $result = "";
									$this->output["json"] = $result;	
								}
								break;
								// case "add_request": {
									// $data = array(
									// "unit" => $_REQUEST["data"]["unit"],
									// "subunit" => $_REQUEST["data"]["subunit"],
									// "cartrige" => $_REQUEST["data"]["cartrige"],
									// "priority" => $_REQUEST["data"]["priority"],
									// "another_user" => $_REQUEST["data"]["another_user"],
									// "cartrige_uid" => $_REQUEST["data"]["cartrige_uid"]);
									// $data = array_map("intval",$data);
									// if ((mb_strlen($data["cartrige_uid"])>4)) $data["error"] .= "����������� ������ ����� ���������.(�� 0 �� 4 ����)<br>";
									// if (($data["unit"]==0) || empty($data["unit"])) $data["error"] .= "�������� �������������<br>";
									// if (($data["subunit"]==0) || empty($data["subunit"])) $data["error"] .= "�������� �����<br>";
									// if (($data["cartrige"]==0) || empty($data["cartrige"])) $data["error"] .= "�������� ��������<br>";
									// if(empty($data["error"])) 
									// {
										// $DB->SetTable("cartrige_requests");
										// $DB->AddCondFS("uid", "=", $data["cartrige_uid"]);
										// $DB->AddCondFS("status", "!=", 2);
										// $res = $DB->Select();
										// if(($DB->NumRows($res)>=1) && !empty($data["cartrige_uid"]) )
										// {
											// $data["error"] .= "������ �� �������� � ������ ������� ���  ������.<br>";
											// $this->output["json"]["add_data"]["error"]=$data["error"]; 
										// }
										// else
										// {
											// $DB->SetTable("cartrige_requests");
											// $DB->AddValue("unit", $data["unit"]);
											// $DB->AddValue("subunit", $data["subunit"]);
											// $DB->AddValue("type", $data["cartrige"]);
											// $DB->AddValue("uid", $data["cartrige_uid"]);
											// $DB->AddValue("status", "0");
											// $DB->AddValue("priority", $data["priority"]);
											// $DB->AddValue("people_id", (empty($data["another_user"]) ? $Auth->user_id : $data["another_user"]));
											// $DB->AddValue("date", date('Y-m-d'));
											// $res=$DB->Insert();											
											// $this->output["json"]["add_data"]=$data;

											// $DB->SetTable("cartrige_requests", "cr");
											// $DB->AddCondFS("cr.uid", "=", $data["cartrige_uid"]);
											// $DB->AddTable("cartrige_list", "cl");
											// $DB->AddCondFF("cl.id", "=", "cr.type");
											// $DB->AddTable("auth_users", "au");
											// $DB->AddCondFF("au.id", "=", "cr.people_id");
											// $DB->AddField("au.displayed_name", "username");
											// $DB->AddField("cl.name", "cart_name");
											// $DB->AddField("cr.uid", "cart_uid");
											// $DB->AddField("cr.people_id", "user_id");
											// $DB->AddField("au.id", "id");
											// $DB->AddOrder("cr.id", true);
											// $res = $DB->Select(1);
											// if($row = $DB->FetchAssoc($res)) {$Engine->LogAction($this->module_id, 'refill_cart', $Auth->user_id);
												// $this->MailTask($row["id"], "to_refill", array('username' => $row["username"], 'cart_uid' => $row["cart_uid"], 'cart_name' => trim($row["cart_name"])), "refilled");
											// }
										// }
									// }
									// else
										// $this->output["json"]["add_data"]["error"]=$data["error"];	
								// }
								// break;
								case "change_status": {
									if($this->output["allow_edit"]) 
									{
										$data = array(
											"id" => $_REQUEST["data"]["id"],
											"status" => $_REQUEST["data"]["status"]);
										$data = array_map("intval",$data);
										$DB->SetTable("cartrige_requests");
										$DB->AddCondFS("id", "=", $data["id"]);
										if($data["status"] == 1)
											$DB->AddValue("date_send", date('Y-m-d'));
										if($data["status"] == 2)
											$DB->AddValue("date_refilled", date('Y-m-d'));
										$DB->AddValue("status", $data["status"]);
										$DB->Update();
										if($data["status"] == 2) 
										{
											$DB->SetTable("cartrige_requests", "cr");
											$DB->AddCondFS("cr.id", "=", $data["id"]);
											$DB->AddTable("cartrige_register", "cart");
											$DB->AddCondFF("cr.register_id", "=", "cart.id");
											$DB->AddTable("cartrige_list", "cl");
											$DB->AddCondFF("cl.id", "=", "cart.cartrige_id");
											$DB->AddTable("auth_users", "au");
											$DB->AddCondFF("au.id", "=", "cr.people_id");
											$DB->AddField("au.displayed_name", "username");
											$DB->AddField("cl.name", "cart_name");
											$DB->AddField("cart.cartrige_uid", "cart_uid");
											$DB->AddField("au.id", "user_id");
											$res = $DB->Select(1);
											if($row = $DB->FetchAssoc($res))
												$this->MailTask($row["user_id"], "refilled", array('username' => $row["username"], 'cart_uid' => $row["cart_uid"], 'cart_name' => trim($row["cart_name"])), "refilled");
										}										
										$this->output["json"]["change_status"]=1;
									}
									else
										$this->output["json"]["change_status"]["error"]="� ��� ��� ���� �� ������ ��������";
								}
								break;
								case "delete": {
									if($this->output["allow_edit"]) 
									{
										$data = array("id" => $_REQUEST["data"]["id"]);
										$data = array_map("intval",$data);
										$DB->SetTable("cartrige_requests");
										$DB->AddCondFS("id", "=", $data["id"]);
										$DB->Delete();
										$this->output["json"]["delete"]=1;
										$Engine->LogAction($this->module_id, 'refill_cart', $Auth->user_id, "delete_request_".$_REQUEST["data"]["id"]);
									}
									else
										$this->output["json"]["delete"]["error"]="� ��� ��� ���� �� ������ ��������";
								}
								break;
								case "sort": {
									if($_REQUEST["data"]["mode"])
									{
										$this->output["json"] = $this->RefillCart("requests", null, array(0=>$_REQUEST["data"]["mode"],1=>$_REQUEST["data"]["sort"]));
									}
								}
								break;
								case "edit_unit": {
									if($this->output["allow_edit"]) 
									{
										$id = intval($_REQUEST["data"]["id"]);
										$name = iconv("utf-8", "cp1251", $_REQUEST["data"]["subunit"]);
										$contacts = iconv("utf-8", "cp1251", $_REQUEST["data"]["contacts"]);
										$DB->SetTable("cartrige_units");
										$DB->AddCondFS("id", "=", $id);
										$DB->AddValue("name", $name);
										$DB->AddValue("contacts", $contacts);
										$DB->Update();
										$this->output["json"] = 1;
									}
									else
										$this->output["json"]["error"]="� ��� ��� ���� �� ������ ��������";
								}
								break;
								case "edit_unit_info": {
									$id = intval($_REQUEST["data"]["id"]);
									$DB->SetTable("cartrige_units");
									$DB->AddCondFS("id", "=", $id);
									$res = $DB->Select(1);
									$row = $DB->FetchAssoc($res);
									$this->output["json"]["contacts"] = $row["contacts"];
								}
								break;
								case "add_unit": {
									if($this->output["allow_edit"]) 
									{
										$error = false;
										$pid = intval($_REQUEST["data"]["pid"]);
										$name = $_REQUEST["data"]["name"];
										if(mb_strlen($name)==0)
										{
											$this->output["json"]["error"] .= "<br>������� �������� ������."; 
											$error = true;
										}
										if((mb_strlen($name)<3) || (mb_strlen($name)>256)) 
										{
											$this->output["json"]["error"] .= "<br>�������� ������ ������ ��������� �� 4 �� 256 ��������."; 
											$error = true;
										}
										if(!$error)
										{
											$name = iconv("utf-8", "cp1251", $name);
											$DB->SetTable("cartrige_units");
											$DB->AddValue("name", $name);
											$DB->AddValue("pid", $pid);
											$DB->Insert();
											$DB->SetTable("cartrige_units");
											$DB->AddCondFS("pid", "=", $pid);
											$DB->AddCondFS("name", "=", $name);
											$DB->AddField("id");
											$res = $DB->Select();
											$row = $DB->FetchAssoc($res);
											$this->output["json"]["id"] = $row["id"];
										}
									}
									else
										$this->output["json"]["error"] .= "� ��� ��� ���� �� ������ ��������";
								}
								break;
								case "delete_unit": {
									if($this->output["allow_edit"]) 
									{
										$data = array("id" => $_REQUEST["data"]["id"], "pid" => $_REQUEST["data"]["pid"]);
										$data = array_map("intval",$data);
										$unit = "subunit";
										if($data["pid"] == 0)
										{
											$DB->SetTable("cartrige_units");
											$DB->AddCondFS("pid", "=", $data["id"]);
											$res = $DB->Select();
											$total = $DB->NumRows($res);
											if($total != 0) 
												$this->output["json"]["error"] .= "<br>� ������ ������������� ���� ������.";
											$unit = "unit";
										}
										if(empty($this->output["json"]["error"]))
										{
											$DB->SetTable("cartrige_units");
											$DB->AddCondFS("id", "=", $data["id"]);
											$res = $DB->Delete();
											$DB->SetTable("cartrige_requests");
											$DB->AddCondFS($unit, "=", $data["id"]);
											$DB->Delete();
										}
									}
									else
										$this->output["json"]["error"] .= "� ��� ��� ���� �� ������ ��������";
								}
								break;
								case "edit_cart_info": {
									$id = intval($_REQUEST["data"]["id"]);
									$DB->SetTable("cartrige_list");
									$DB->AddCondFS("id", "=", $id);
									$res = $DB->Select(1);
									$row = $DB->FetchAssoc($res);
									$this->output["json"]["refill"] = $row["refill_price"];
									$this->output["json"]["repair"] = $row["restore_price"];
									$this->output["json"]["device_id"] = $row["device_id"];
									$this->output["json"]["comment"] = $row["devices"];
									$this->output["json"]["manufacturer_id"] = $row["manufacturer_id"];
								}
								break;
								case "edit_cart": {
									if($this->output["allow_edit"]) 
									{
										$id = intval($_REQUEST["data"]["id"]);
										$manufacturer_id = intval($_REQUEST["data"]["manufacturer_id"]);
										$name = $_REQUEST["data"]["name"];
										$comment = $_REQUEST["data"]["comment"];
										$DB->SetTable("cartrige_list");
										$DB->AddCondFS("id", "=", $id);
										$DB->AddValue("name", iconv("utf-8", "cp1251", $name));
										$DB->AddValue("devices", iconv("utf-8", "cp1251", $comment));
										$DB->AddValue("manufacturer_id", $manufacturer_id);
										$DB->AddValue("refill_price", (double)$_REQUEST["data"]["refill_price"]);
										$DB->AddValue("restore_price", (double)$_REQUEST["data"]["repair_price"]);
										$DB->AddValue("device_id", (int)$_REQUEST["data"]["device_id"]);
										$DB->Update();
										
									}
									else
										$this->output["json"]["error"] .= "� ��� ��� ���� �� ������ ��������";
								}
								break;
								case "add_cart": {
									if($this->output["allow_edit"]) 
									{
										$name = htmlentities($_REQUEST["data"]["name"]);
										$manufacturer_id = intval($_REQUEST["data"]["manufacturer_id"]);
										$comment = $_REQUEST["data"]["comment"];
										$DB->SetTable("cartrige_list");
										$DB->AddValue("name", iconv("utf-8", "cp1251", $name));
										$DB->AddValue("refill_price", (double)$_REQUEST["data"]["refill_price"]);
										$DB->AddValue("restore_price", (double)$_REQUEST["data"]["repair_price"]);
										$DB->AddValue("device_id", (int)$_REQUEST["data"]["device_id"]);
										$DB->AddValue("devices", iconv("utf-8", "cp1251", $comment));
										$DB->AddValue("manufacturer_id", $manufacturer_id);
										$DB->Insert();
										$this->output["json"]["ok"] .= "<br>�������� �������� � ����.";
									}
									else
										$this->output["json"]["error"] .= "� ��� ��� ���� �� ������ ��������";
								}
								break;
								case "delete_cart": {
									if($this->output["allow_edit"]) 
									{
										$id = intval($_REQUEST["data"]["id"]);
										$DB->SetTable("cartrige_list");
										$DB->AddCondFS("id", "=", $id);
										$DB->Delete();
										$DB->SetTable("cartrige_requests");
										$DB->AddCondFS("type", "=", $id);
										$DB->Delete();
										$this->output["json"]["ok"] .= "<br>�������� ������ �� ����.";
									}
									else
										$this->output["json"]["error"] .= "� ��� ��� ���� �� ������ ��������";
								}
								break;
								case "edit_register": {
									if($this->output["allow_edit"]) 
									{
										$mode = $_REQUEST["data"]["mode"];
										$modes = array("cartrige_uid" => "cartrige_uid",
																	 "limit" => "limit",
																	 "list" => "cartrige_id",
																	 "units" => "unit_id",
																	 "user_id" => "user_id",
																	 "subunit" => "subunit_id");
										$id = intval($_REQUEST["data"]["id"]);
										$val = $_REQUEST["data"]["value"];										
										$DB->SetTable("cartrige_register");
										$DB->AddCondFS("id", "=", $id);
										if($modes[$mode]=="user_id")
											$DB->AddValue($modes[$mode], $_REQUEST["data"]["user_id"]);
										else
											$DB->AddValue($modes[$mode], $val);
										$DB->Update();
										$this->output["json"]["val"] = $val;
									}
									else
										$this->output["json"]["error"] .= "� ��� ��� ���� �� ������ ��������";
								}
								break;
								case "change_user" : {
									$data = array("id" => $_REQUEST["data"]["id"], "user_id" => $_REQUEST["data"]["user_id"]);
									$data = array_map("intval",$data);
									$DB->SetTable("cartrige_requests");
									$DB->AddCondFS("id", "=", $data["id"]);
									$DB->AddValue("people_id", $data["user_id"]);
									$DB->Update();
								}
								break;
								case "edit_register_info": {
									if($_REQUEST["data"]["mode"] == "subunit") {$pid = true; $_REQUEST["data"]["mode"] = "units";}
									if($_REQUEST["data"]["mode"] == "contacts") {$cont = true; $_REQUEST["data"]["mode"] = "units";}
									$DB->SetTable("cartrige_".$_REQUEST["data"]["mode"]);
									if($pid)
										$DB->AddCondFS("pid", "=", $_REQUEST["data"]["pid"]);
									elseif($cont)
										$DB->AddCondFS("id", "=", $_REQUEST["data"]["id"]);
									elseif($_REQUEST["data"]["mode"] == "units")
										$DB->AddCondFS("pid", "=", "0");
									$res = $DB->Select();
									while($row = $DB->FetchAssoc($res)) {
										$this->output["json"]["values"][] = $row;
									}
								}
								break;
								case "edit_register_delete": {
									$id = intval($_REQUEST["data"]["id"]);
									$DB->SetTable("cartrige_register");
									$DB->AddCondFS("id", "=", $id);
									$DB->Delete();
									$this->output["json"]["deleted"] = 1;									
								}
								break;
								case "edit_register_unactive": {
									$id = intval($_REQUEST["data"]["id"]);
									$date = date("Y-m-d", time());
									$DB->SetTable("cartrige_register");
									$DB->AddCondFS("id", "=", $id);
									$DB->AddValue("deleted", 1);
									$DB->AddValue("date", $date);
									$DB->Update();
									$this->output["json"]["date"] = $date;									
								}
								break;								
								case "register_cartrige": {
									$data = array_map("intval",$_REQUEST["data"]);
									$DB->SetTable("cartrige_register");
									$DB->AddOrder("cartrige_uid", true);
									$res = $DB->Select(1);
									$row = $DB->FetchAssoc($res);
									$uid = $row["cartrige_uid"];
									$new_uid = $uid+1;
									
									if(!empty($data["cartrige_id"]) && !empty($data["subunit_id"])  && !empty($data["unit_id"])) {
										$DB->SetTable("cartrige_register");
										$DB->AddValue("cartrige_id", $data["cartrige_id"]);
										$DB->AddValue("unit_id", $data["unit_id"]);
										$DB->AddValue("subunit_id", $data["subunit_id"]);
										$DB->AddValue("user_id", $Auth->user_id);
										$DB->AddValue("cartrige_uid", $new_uid);
										$DB->Insert();
										$this->output["json"] = $new_uid;
										$DB->SetTable("cartrige_register", "cr");
										$DB->AddCondFS("cr.uid", "=", $new_uid);
										$DB->AddTable("cartrige_list", "cl");
										$DB->AddCondFF("cl.id", "=", "cr.cartrige_id");
										$DB->AddField("cr.cartrige_uid", "cartrige_uid");
										$DB->AddField("cr.id", "id");
										$DB->AddField("cl.name", "name");
										$res2 = $DB->Select(1);
										if($row2 = $DB->FetchAssoc($res2)) {
											$Engine->LogAction($this->module_id, 'refill_cart', $Auth->user_id, 'add_request_register_'.$row2["id"]);
										}												
									}
									else $this->output["json"]["error"] = "�� ��� ���� ���������";
														
								
								


									
								}
								break;	
							}
						}
						if($this->output["allow_edit"])
						{
							///�������������
							$DB->SetTable("cartrige_units");
							$DB->AddCondFS("pid", "=", "0");
							$res = $DB->Select();
							while($row = $DB->FetchAssoc($res))
							{
								$this->output["edit_units"][$row["id"]]["name"] = $row["name"];
								$DB->SetTable("cartrige_units");
								$DB->AddCondFS("pid", "=", $row["id"]);
								$resu = $DB->Select();
									while($rowu = $DB->FetchAssoc($resu))
									{
										$this->output["edit_units"][$row["id"]]["subunits"][] = array("id" => $rowu["id"],
																								"pid" => $row["id"],
																								"name" => $rowu["name"]);
									}
								
							}
							///�����������
							if(!empty($_POST["unit_id"]) && !empty($_POST["subunit_id"]) && !empty($_POST["cartrige_id"]) && !empty($_POST["uid"])) {
								$data = array_map("intval",$_POST);
								$DB->SetTable("cartrige_register");
								$DB->AddValues(array("cartrige_id" => $data["cartrige_id"], 
																		 "unit_id" => $data["unit_id"], 
																		 "subunit_id" => $data["subunit_id"], 
																		 "cartrige_uid" => $data["uid"], 
																		 "user_id" => $Auth->user_id, 
																		 "limit" => $data["limit"]));
								$DB->Insert();
								$this->output["messages"]["good"][] = 100;
							}
							elseif(!empty($_POST) && empty($_POST["register_id"]) && empty($_POST["user_reg"]))
								$this->output["messages"]["bad"][] = 103;
							///������ ������������������
							$DB->SetTable("cartrige_register", "cr");
							$DB->AddTable("cartrige_list", "cl");
							$DB->AddCondFF("cr.cartrige_id", "=", "cl.id");
							$DB->AddTable("auth_users", "au");
							$DB->AddCondFF("cr.user_id", "=", "au.id");							
							$DB->AddField("cl.name", "cartrige_name");
							$DB->AddField("cl.id", "cartrige_id");
							$DB->AddField("cr.unit_id", "unit_id");
							$DB->AddField("cr.subunit_id", "subunit_id");
							$DB->AddField("cr.limit", "limit");
							$DB->AddField("cr.cartrige_uid", "uid");
							$DB->AddField("cr.id", "id");
							$DB->AddField("au.id", "user_id");
							$DB->AddField("au.displayed_name", "user_name");
							$DB->AddField("cr.deleted", "deleted");
							$DB->AddField("cr.date", "date");
							$DB->AddOrder("cr.cartrige_uid", true);
							$res = $DB->Select();
							$i=0;
							while($row = $DB->FetchAssoc($res)) {
								$DB->SetTable("cartrige_units");
								$DB->AddCondFS("id", "=", $row["subunit_id"]);
								$sun = $DB->Select(1);
								$subunit = $DB->FetchAssoc($sun);
								$DB->SetTable("cartrige_units");
								$DB->AddCondFS("id", "=", $row["unit_id"]);
								$un = $DB->Select(1);
								$unit = $DB->FetchAssoc($un);
								$this->output["registered"][$row["id"]] = $row;
								$this->output["registered"][$row["id"]]["subunit"] = $subunit;
								$this->output["registered"][$row["id"]]["unit"] = $unit;
								$i++;
							}							
						}
						//user_reg

						
						//add request
						if(!empty($_POST["register_id"])) {
							$id = intval($_POST["register_id"]);
							$DB->SetTable("cartrige_requests");
							$DB->AddCondFS("register_id", "=", $id);
							$DB->AddCondFS("status", "!=", "2");
							$res = $DB->Select();
							$count = $DB->NumRows($res);
							if($count>0)
								$this->output["messages"]["bad"][] = 105;
							else
							{
								$DB->SetTable("cartrige_requests");
								$DB->AddValue("people_id", $Auth->user_id);
								$DB->AddValue("date", date("Y-m-d", time()));
								$DB->AddValue("status", "0");
								$DB->AddValue("priority", ($_POST["priority"] ? "1" : "0"));
								$DB->AddValue("register_id", $id);
								$DB->Insert();
								$this->output["messages"]["good"][] = 104;
								
								$DB->SetTable("cartrige_register", "cr");
								$DB->AddCondFS("cr.id", "=", $id);
								$DB->AddTable("cartrige_list", "cl");
								$DB->AddCondFF("cl.id", "=", "cr.cartrige_id");
								$DB->AddField("cr.cartrige_uid", "cartrige_uid");
								$DB->AddField("cl.name", "name");
								$res = $DB->Select(1);
								if($row = $DB->FetchAssoc($res)) {
									$Engine->LogAction($this->module_id, 'refill_cart', $Auth->user_id, 'add_request');
								  $this->MailTask($Auth->user_id, "to_refill", array('username' => $Auth->user_displayed_name, 'cart_uid' => $row["cartrige_uid"], 'cart_name' => trim($row["name"])), "refilled");
								}								
							}
						}
						
						 
						
						
						
						
						
						
						
						$this->output['module_id'] = $module_id;
					  $this->RefillCart("units");
						$this->RefillCart("requests");
						
					}
					break;

					case "cart_1c": {//����� ����� ������ � 1�
						try {
							$cartrClient = new \modules\Cartrige\core\CartrigeClient1c();
							$cartrClient->setCacheLifetime(0);
							$cartrige = new \modules\Cartrige\core\CartrigeCollection($cartrClient);
							$this->output["cart_1c"] = $cartrige->getCartBySubdivision();
						}catch (\Exception $e){
							$this->output["cart_1c"]['error'] = $e->getMessage();
						}

						$this->output['scripts_mode'] = $this->output['mode'] = 'cart_1c';
					}
					break;

					default:
					
					break;
				}
				
			} else {
				if (!is_array(unserialize($_SESSION['tasks_user_filters']))) $_SESSION['tasks_user_filters'] = serialize(array("new", "current", "tobechecked", "done"));
				$filters = isset($_SESSION['tasks_user_filters']) ? unserialize($_SESSION['tasks_user_filters']) : false;
				$this->output = array();
				$this->output['users'] = $this->users;
				$this->output['tasks'] = array();
				$this->output['tasks']['my'] = $this->GetUserTasks($Auth->user_id, array('statuses'=>implode(';', $filters)));
				$this->output['show_mode'] = 'my';
				$this->output['id'] = $Auth->user_id;
				$this->output['module_id'] = $module_id;
				$this->output['full_task_statuses'] =  array("new", "current", "tobechecked", "done");
				$this->output['task_statuses'] = $filters  ?: $this->output['full_task_statuses']; 
				$this->output['statuses_titles'] = array("new" => '�����', "current"=> '�����������', "tobechecked"=> '�� ��������', "done"=> '���������');
				$this->output['edit_all'] = $Engine->OperationAllowed($module_id, "tasks.edit_all", -1, $Auth->usergroup_id);
				$this->output['assigners']  = $this->GetAssigners();
				
				// ������������� pager-�
				list($this->items_num) = $this->GetTasksCount(array('author' => $Auth->user_id, 'statuses'=>implode(';',$filters)));
				require_once INCLUDES . "Pager.class";
				$Pager = new Pager($this->items_num, $this->settings['pager']['items_per_page'], $this->settings['pager']['page_limit']); 
				$this->output["pager_output"] = $Pager->Act();
                $this->output["scripts_mode"][] = 'input_calendar';
				$this->output['scripts_mode'][] = $this->output['mode'] = 'tasks';
				$this->output['plugins'][] = 'jquery.ui.datepicker.min';
				$this->GetTaskCount($Auth->user_id);
			} 
		}
	}
	
	function AddRequestCSV($uid, $date) {
		global $DB;		
		$uid = intval($uid);
		$DB->SetTable("cartrige_register");
		$DB->AddCondFS("cartrige_uid", "=", $uid);
		$cc = $DB->FetchAssoc($DB->Select(1));
		$id = $cc["id"];
		
		$date = implode("-" ,array_reverse(explode(".", $date)));
		$DB->SetTable("cartrige_requests");
		$DB->AddValue("people_id", 15381);
		$DB->AddValue("date", $date);
		$DB->AddValue("date_refilled", $date);
		$DB->AddValue("date_send", $date);
		$DB->AddValue("status", "2");
		$DB->AddValue("priority", 0);
		$DB->AddValue("register_id", $id);
		$DB->Insert();	
	}
	function RefillCart($table, $id=NULL, $sort=NULL)
	{
		global $DB, $Engine, $Auth;
		
		if($table != "requests")
		{
			if($table=="list") {
				$DB->SetTable("cartrige_manufacturers");
				$res2 = $DB->Select();
				while($row2 = $DB->FetchAssoc($res2)) 
				{	
					$this->output["units"]["manufacturers"][$row2["id"]]	= $row2["name"];		
					$DB->SetTable("cartrige_$table", "cart");
					$DB->AddCondFS("cart.manufacturer_id", "=", $row2["id"]);
					if(isset($id)) $DB->AddCondFS("cart.pid", "=", $id);
					$DB->AddField("cart.id");
					$DB->AddField("cart.name");
					$DB->AddField("cart.devices");
					$res = $DB->Select();				
					while($row = $DB->FetchAssoc($res)) 
					{			
						$this->output["units"]["cart"][$row2["id"]][$row["id"]]["name"]=$row["name"]; 
						$this->output["units"]["cart"][$row2["id"]][$row["id"]]["title"]=$row["devices"]; 
					}
				}
			} 
			else {
				$DB->SetTable("cartrige_$table", "cart");
				if(isset($id)) $DB->AddCondFS("cart.pid", "=", $id);
				elseif($table != "list") {
					$DB->AddCondFS("cart.pid", "=", "0");
					$DB->AddField("cart.pid");
				}
				$DB->AddField("cart.id");
				$DB->AddField("cart.name");
				$res = $DB->Select();	
				while($row = $DB->FetchAssoc($res)) 
				{			
					$this->output["units"][$row["id"]]=$row["name"]; 
				}
			}
			return $this->output["units"];
		}
		else
		{
			$DB->SetTable("cartrige_requests");
			if(!$this->output["allow_edit"]) 
				$DB->AddCondFS("people_id", "=", $Auth->user_id);
			$res_count =  $DB->Select();
			$count_items = $DB->NumRows($res_count);
			require_once INCLUDES . "Pager" . CLASS_EXT;
			$Pager = new Pager($count_items, 30, 0);  
			$this->output["pager_output"]  = $Pager->Act();
			$from = $this->output["pager_output"]["db_from"];
			$limit = $this->output["pager_output"]["db_limit"];
			$DB->SetTable("cartrige_$table", "cart");
			$DB->AddTable("cartrige_list", "cl");
			$DB->AddTable("cartrige_units", "un");
			$DB->AddTable("cartrige_units", "unn");
			$DB->AddTable("cartrige_register", "cr");
			$DB->AddCondFF("cr.id", "=", "cart.register_id");
			$DB->AddCondFF("cr.cartrige_id", "=", "cl.id");
			$DB->AddCondFF("cr.unit_id", "=", "un.id");			
			$DB->AddCondFF("unn.id", "=", "cr.subunit_id");
			if(!$this->output["allow_edit"]) 
				$DB->AddCondFS("cart.people_id", "=", $Auth->user_id);

			$DB->AddField("cl.name", "name");
			$DB->AddField("un.name", "unit");
			$DB->AddField("un.contacts", "unit_contacts");
			$DB->AddField("unn.name", "subunit");
			$DB->AddField("unn.contacts", "subunit_contacts");
			$DB->AddField("cart.id", "id");
			$DB->AddField("cart.date", "date");
			$DB->AddField("cr.cartrige_uid", "uid");
			$DB->AddField("cr.cartrige_id", "cartrige_id");
			$DB->AddField("cr.unit_id", "unit_id");
			$DB->AddField("cr.subunit_id", "subunit_id");
			$DB->AddField("cart.status", "status");	
			$DB->AddField("cart.priority", "priority");	
			$DB->AddField("cart.date_send", "date_send");
			$DB->AddField("cart.date_refilled", "date_refilled");
			$DB->AddField("cart.people_id", "people_id");
			$DB->AddField("cart.register_id", "register_id");
			if(empty($_GET["sort_by"]))
			{				
				if($this->output["allow_edit"] && !$sort) {$DB->AddOrder("cart.status");$DB->AddOrder("cart.priority", true);}
				elseif(!$this->output["allow_edit"] && !$sort){ $DB->AddOrder("cart.date", true);$DB->AddOrder("cart.priority", true);}
			}
			else {
				$mode = array("status_desc" => array("status", true), 
											"status" => array("status", false),
											"date" => array("date", false), 
											"date_desc" => array("date", true)); 
				
				
				
				$DB->AddOrder("cart.".$mode[$_GET['sort_by']][0], $mode[$_GET['sort_by']][1]);
			}
			$DB->AddOrder("cart.date",true);
			if($_REQUEST["data"]["page"])
			{
				$from=$limit*$_REQUEST["data"]["page"]-$limit;
			}
			$res = $DB->Select($limit, $from);
			$count=$DB->NumRows($res);
			while($row = $DB->FetchAssoc($res)) 
			{
				if($row["people_id"]==0)	
					$row["last_name"] = "-";
				else
				{
					$DB->SetTable("auth_users");
					$DB->AddCondFS("id", "=", $row["people_id"]);
					$r = $DB->Select(1);
					$ro = $DB->FetchAssoc($r);
					$row["last_name"]= $ro["displayed_name"];
				}
				
				$this->output["requests"][$row["id"]]=$row; 
				$result[]=$row;
			}
			if($count==0)
				return 0;
			else
				return $result;
		}
	}	
	function GetAssigners() {
		global $DB;
		$assigners = array();
		$res = $DB->Exec("SELECT DISTINCT author_name FROM tasks WHERE cancelled=0 ORDER BY author_name");
		while($row = $DB->FetchAssoc($res)) {
			$assigners[] = $row['author_name'];
		}
		return $assigners;
	}

	function GetTaskCount($user_id) {
		global $DB, $Auth, $Engine;
		$res = $DB->Exec("SELECT count(`tasks`.`id`) as `count` FROM `tasks_executors`, `tasks` WHERE `tasks_executors`.`executor_id` = ".$user_id." and `tasks_executors`.`task_id`=`tasks`.`id` and `tasks`.`status` = 'new' and `tasks`.`cancelled` = 0");
		$row = $DB->FetchAssoc($res);
		$this->output['tasks_count']['new'] = $row['count'];
		$res = $DB->Exec("SELECT count(`tasks`.`id`) as `count` FROM `tasks_executors`, `tasks` WHERE `tasks_executors`.`executor_id` = ".$user_id." and `tasks_executors`.`task_id`=`tasks`.`id` and `tasks`.`status` = 'current' and `tasks`.`cancelled` = 0");
		$row = $DB->FetchAssoc($res);
		$this->output['tasks_count']['current'] = $row['count'];
		$res = $DB->Exec("SELECT count(`tasks`.`id`) as `count` FROM `tasks_executors`, `tasks` WHERE `tasks_executors`.`executor_id` = ".$user_id." and `tasks_executors`.`task_id`=`tasks`.`id` and `tasks`.`status` = 'tobechecked' and `tasks`.`cancelled` = 0");
		$row = $DB->FetchAssoc($res);
		$this->output['tasks_count']['tobechecked'] = $row['count'];
	}
	
	function AddTask($data)
	{
		global $DB, $Auth, $Engine;
		
		$DB->SetTable('tasks');
		$DB->AddValues(array(
            "author_id" => $Auth->user_id,
            "author_name" => ( CODEPAGE == 'cp1251' ? iconv('utf-8', 'cp1251' , $data['task_author']) : $data['task_author']),
			"executor_id" => null, //$data['task_executor'],
			"status" => "new",
			"description" => ( CODEPAGE == 'cp1251' ? iconv('utf-8', 'cp1251' , $data['task_descr']) : $data['task_descr']),  
			));
		if ($DB->Insert())
		{
			//$this->MailTask($data['task_executor'], 'new');
			$Engine->LogAction($this->module_id, 'task', $DB->LastInsertId(), 'create');
			return true;
		}
		else return false;
	}
	
	
	function MakeDBConds(&$DB, $filters)
	{
		if (isset($filters['author']) && $filters['author'])
				$DB->AddCondFS('author_id', '=', $filters['author']);
		if (isset($filters['assigner']) && $filters['assigner'])
				$DB->AddCondFS('author_name', '=', iconv('utf-8', 'windows-1251', $filters['assigner']));
		if (isset($filters['tasks_ids']) && $filters['tasks_ids'])
		{
			foreach($filters['tasks_ids'] as $taskId)
				$DB->AddAltFS('id', '=', $taskId);
			$DB->AppendAlts();
		}
		if (isset($filters['assign_date_from']) && $filters['assign_date_from'])
			$DB->AddCondFS('creation_date', '>=', $filters['assign_date_from']);
		if (isset($filters['assign_date_to']) && $filters['assign_date_to'])
			$DB->AddCondFS('creation_date', '<=', $filters['assign_date_to']);
		if (isset($filters['statuses']) && $filters['statuses'])
		{	
			$statuses = explode(';', $filters['statuses']);
			foreach($statuses as $status)
				$DB->AddAltFS('status', '=', $status);
			$DB->AppendAlts();
		}
		else return false;
		return true;
	}
	
	
	
	function UpdateExecutors($executorId, $taskId, $oldExecutorId = false)
	{
		global $DB, $Engine;
		$DB->SetTable('tasks_executors');
		$DB->AddValues(array(
            "task_id" => $taskId,
            "executor_id" => $executorId
			));
		if($oldExecutorId) 
		{
			$DB->AddCondFS("executor_id", "=", $oldExecutorId);
			$DB->AddCondFS("task_id", "=", $taskId);
			if ($DB->Update())
			{
				$DB->SetTable('tasks');
				$DB->AddCondFS('id', '=', $taskId);
				if ($res = $DB->Select()) 
				{
					$row = $DB->FetchAssoc($res);
					if ($oldExecutorId != $row['author_id'])
						$this->MailTask($oldExecutorId, 'remove');
				}
				$DB->FreeRes($res);
				$this->updateTask(array('id' => $taskId, 'executor_id' => $executorId, 'status' => $row['status']), true);
				
				$Engine->LogAction($this->module_id, 'task', $oldExecutorId.'->'.$executorId, 'update executor');
				return true;
			}	
			else return false;
			
		}
		else if (!$oldExecutorId && $DB->Insert())
		{
			$DB->FreeRes();
			$DB->SetTable('tasks');
			$DB->AddCondFS('id', '=', $taskId);
			if ($res = $DB->Select()) 
				$row = $DB->FetchAssoc($res);
			$res = $this->updateTask(array('id' => $taskId, 'assignment_date' => (is_null($row['executor_id'])  ? 1 : 0), 'executor_id' => $executorId, 'status' => $row['status']));
			$Engine->LogAction($this->module_id, 'task', $taskId.'-'.$executorId.'_'.$row['status'], 'create executor');
			return $res;
		}
		else return false;
	}
	
	
	function GetExecutorsByTask($taskId, $encode = false)
	{
		global $DB;
		$executors = array();
		$DB->SetTable("tasks_executors", "te");
		$DB->AddTable("auth_users","au");
        $DB->AddCondFF("te.executor_id","=","au.id");
        $DB->AddCondFS('te.task_id', '=', $taskId);
        $DB->AddField("au.id","executor_id");
        $DB->AddField("au.displayed_name","executor_name");
		if ($res  = $DB->Select())
		{
			while ($row = $DB->FetchAssoc($res)) 
			{
				$executors[] =  array('name' => ($encode ? iconv('windows-1251', 'utf-8', $row['executor_name']) : $row['executor_name']), 'id' => $row['executor_id']);
			}
			$DB->FreeRes($res);
		}
		return $executors;
	}
	
	function GetTasksCount($filters = false)
	{	
		global $DB;
			
		if ($filters) 
		{
			if (isset($filters['executor']) && $filters['executor']) 
			{
				$DB->SetTable('tasks_executors');
				$DB->AddField('task_id');
				$DB->AddCondFS('executor_id', '=', $filters['executor']);
				if ($res = $DB->Select())
				{
					$filters['tasks_ids'] = array();
					while ($row = $DB->FetchAssoc($res)) 
						$filters['tasks_ids'][] = $row['task_id'];
					$DB->FreeRes();
				} 
			}
		}
		
		$DB->SetTable("tasks");
		$DB->AddExp("COUNT(*)");
		if ($filters && !empty($filters))
			$this->MakeDBConds($DB, $filters);
		$res  = $DB->Select();
		return $DB->FetchRow($res);
	}
	
	function GetTasks($data)
	{
		global $DB;
		
		if (isset($data['filters'])) 
		{
			if (isset($data['filters']['executor']) && $data['filters']['executor']) 
			{
				$DB->SetTable('tasks_executors');
				$DB->AddField('task_id');
				$DB->AddCondFS('executor_id', '=', $data['filters']['executor']);
				if ($res = $DB->Select())
				{
					$data['filters']['tasks_ids'] = array();
					while ($row = $DB->FetchAssoc($res)) 
						$data['filters']['tasks_ids'][] = $row['task_id'];
					$DB->FreeRes();
				} 
				if (!isset($data['filters']['tasks_ids']) || empty($data['filters']['tasks_ids'])) 
					return false;
			}
		}
		$DB->SetTable('tasks');
		if (isset($data['filters']) && !$this->MakeDBConds($DB, $data['filters']))
			return false;
		$DB->AddCondFS('cancelled', '=', 0);
		$DB->AddOrder('creation_date', true);
		if (!isset($data['from'])) $data['from'] = 1;
		if ($this->module_uri != 'print/')
			$res = $DB->Select($this->settings['pager']['items_per_page'], (intval($data['from'])-1)*$this->settings['pager']['items_per_page']);
		else $res = $DB->Select();
		if ($res) {
			$tasks = array();
			while ($row = $DB->FetchAssoc($res)) {
				$row['description'] = ( CODEPAGE == 'cp1251' ? iconv('windows-1251', 'utf-8', $row['description']) : $row['description']);
				$row['executor_comment'] = ( CODEPAGE == 'cp1251' ? iconv('windows-1251', 'utf-8', $row['executor_comment']) : $row['executor_comment']);
				$row['executors'] = $this->GetExecutorsByTask($row['id'], CODEPAGE == 'cp1251');
				$author_name = is_null($row['author_name']) ? $this->GetUserNameById($row['author_id']) : $row['author_name'] ;
		 		$row['author_name'] = ( CODEPAGE == 'cp1251' ? iconv('windows-1251', 'utf-8', $author_name) : $author_name);
		 		$row['assigner_name'] = ( CODEPAGE == 'cp1251' ? iconv('windows-1251', 'utf-8', $this->GetUserNameById($row['author_id'])) : $this->GetUserNameById($row['author_id']));
		 		$tasks[] = $row;
		 	}
		 	
		 	return $tasks;		
		 }
		return false;
	}
	
	
	function GetUserNameById($user_id)
	{  
		foreach ($this->users as $user) {
			if ($user['id'] == $user_id)
				return $user['name'];
		}
		return false;
	}

	
	function GetUserTasks($user_id, $filters = false)
	{
		global $DB, $Auth;
		
		$author_name = $this->GetUserNameById($user_id);
		$DB->SetTable("tasks");
		$DB->AddCondFS('author_id', '=', $user_id);
		$DB->AddCondFS('cancelled', '=', 0);
		if ($filters) 
		{	
			if (!$this->MakeDBConds($DB, $filters))
				return false;
		}
		$DB->AddOrder('creation_date', true);
		if ($res = $DB->Select($this->settings['pager']['items_per_page'])) {
			$tasks = array();
			while ($row = $DB->FetchAssoc($res)) {
				$row['executors'] = $this->GetExecutorsByTask($row['id']);
				$row['executor_name'] = $this->GetUserNameById($row['executor_id']);
				$row['author_name'] = is_null($row['author_name']) ? $author_name : $row['author_name'];
				$tasks[] = $row;
		 	}
		 	return $tasks;		
		 }
		return false;
	}
	
	
	function GetAgoDate($cur_date, $interval)
	{
		$data_parts = explode('-', $cur_date);
		switch ($interval) 
		{
			case 'month':
				$last_month = intval($data_parts[1])-1;
				if (!$last_month) {
					return (intval($data_parts[0])-1).'-12-'.$data_parts[2];
				} else return $data_parts[0].'-'.( $last_month < 10 ? '0'.$last_month : $last_month ).'-'.$data_parts[2];
				break;
			case 'day':
				$last_day = intval($data_parts[2])-1;
				if (!$last_day) {
					return $data_parts[0].'-'.(intval($data_parts[1])-1).'-01';
				} else return $data_parts[0].'-'.$data_parts[1].'-'.( $last_day < 10 ? '0'.$last_day : $last_day );
		}
		 
	}
	
	function MailTask($user_id, $message_type, $additional = array(), $notify_type = null)
	{
		global $Engine, $DB;
		//$this->MailTask($row['author_id'], $data['status'], array('executor_name' => $executor['name']));
		$DB->SetTable("auth_users");
		$DB->AddCondFS('id', '=', $user_id);
		$res = $DB->Select();
		while ($row = $DB->FetchObject($res))
		{
			if ($row->email)
			{	
				$mail_message_patterns = array(
					'headers' => array('%site_short_name%' =>  '=?koi8-r?B?'.base64_encode(iconv('windows-1251', 'koi8-r', '����')).'?= '."<admin@".$_SERVER['HTTP_HOST'].">", '%server_name%' => $_SERVER['HTTP_HOST'], '%email%' => $row->email),
					'subject' => array('%site_short_name%' => SITE_SHORT_NAME, '%content%' => &$this->settings['subject'][$message_type]),
					'content' => array('%server_name%' => $_SERVER['HTTP_HOST'],
															'%cart_name%' => $additional['cart_name'],
															'%cart_uid%' => $additional['cart_uid'],
															'%username%' => $additional['username'],
															'%executor_name%' => isset($additional['executor_name']) ? $additional['executor_name'] : '', 
															'%tasks_system_folder%' => isset($_REQUEST['page_uri']) ? $_REQUEST['page_uri'] : ''),
					'message' => array('%site_short_name%' => SITE_SHORT_NAME, '%author_name%' => $row->displayed_name, '%time%' => date('Y-m-d H:i:s'), '%content%' => &$this->settings['content'][$message_type])
				);
				
				foreach ($this->settings['subject'] as $ind=>$setting)
					$this->settings['subject'][$ind] = str_replace( array_keys($mail_message_patterns['content']), $mail_message_patterns['content'], $setting);
				

				foreach ($this->settings['content'] as $ind=>$setting)
					$this->settings['content'][$ind] = str_replace( array_keys($mail_message_patterns['content']), $mail_message_patterns['content'], $setting);
				
				if(empty($notify_type))
				{
					foreach ($this->settings['notify_settings'] as $ind=>$setting)
					{
						 $this->settings['notify_settings'][$ind] = str_replace( array_keys($mail_message_patterns[$ind]), $mail_message_patterns[$ind], $setting);
					}
					$mailRes = mail($row->email, '=?koi8-r?B?'.base64_encode(iconv('windows-1251', 'koi8-r', $this->settings['notify_settings']['subject'])).'?=', $this->settings['notify_settings']['message'], $this->settings['notify_settings']['headers']);
					$Engine->LogAction($this->module_id, 'task', $user_id, 'mail to executor'.' '.$row->email.'_'.strlen($this->settings['notify_settings']['message']).'_'.$mailRes);
				}
				else
				{
					foreach ($this->settings[$notify_type] as $ind=>$setting)
					{
						$this->settings[$notify_type][$ind] = str_replace( array_keys($mail_message_patterns[$ind]), $mail_message_patterns[$ind], $setting);
					}	
					$mailRes = mail($row->email, '=?koi8-r?B?'.base64_encode(iconv('windows-1251', 'koi8-r', $this->settings[$notify_type]['subject'])).'?=', $this->settings[$notify_type]['message'], $this->settings[$notify_type]['headers']);
					$Engine->LogAction($this->module_id, 'task', $user_id, 'mail to executor'.' '.$row->email.'_'.strlen($this->settings[$notify_type]['message']).'_'.$mailRes);
				}
			}
		}
	}
	
	function ManageHTTPdata()
	{
		return;
	}
	
	
	function UnassignTask($data)
	{
		global $DB;
		if (isset($data['id']))
		{ 
			$DB->SetTable('tasks');
			$DB->AddCondFS('id', '=', $data['id']);
			$DB->AddValue('cancelled', 1);
			return $DB->Update();
		}
		return false;
	}
	
	
	function updateTask($data, $noStatusUpdate = false)
	{
		global $DB, $Engine;
		$response = array('id' => isset($data['id']) ? $data['id'] : null);
		
		$DB->SetTable('tasks');
		foreach ($data as $key=>$value)
			if ($key != 'id') 
			{
				if ($key == 'assignment_date' && $value==1) {
					$assignment_date = date('Y-m-d H:i:s');
					$DB->AddValue('assignment_date', $assignment_date);
					$response['assignment_date'] = $assignment_date;
				} 
				else if ($key != 'assignment_date')
				{				
					$DB->AddValue($key, ($key == 'executor_comment' || $key == 'author_name' || $key == 'description'? ( CODEPAGE == 'cp1251' ? iconv('utf-8', 'cp1251' , urldecode($value)) : urldecode($value)) : $value), "S");
					if ($key == 'status' && !$noStatusUpdate && ($value == 'current' || $value == 'done'))
					{
						if ($value == 'current')
							$DB->AddValue('execution_start_date', date('Y-m-d H:i:s'));
						else 
							$DB->AddValue('execution_finish_date', date('Y-m-d H:i:s'));
					}
				}
			}
		$DB->AddCondFS("id", "=", $data['id']);
		$res = $DB->Update();
			
			if (isset($data['status'])) 
			{
				if ($res) $DB->FreeRes($res);
				if (!isset($data['executor_id'])) 
				{
					$executors = $this->GetExecutorsByTask($data['id']);
				} else 
				{
					$executors = array(array('id' => $data['executor_id'], 'name' => $this->GetUserNameById($data['executor_id'])));
				}  
					
				$DB->SetTable('tasks');
				$DB->AddCondFS('id', '=', $data['id']);
				if ($res = $DB->Select())
				{
					$row = $DB->FetchAssoc($res);
					
					foreach ($executors as $executor)
					{
						if ($executor['id'] != $row['author_id'])
						{
							
							if ($data['status'] == 'current' || $data['status'] == 'tobechecked')
								$this->MailTask($row['author_id'], $data['status'], array('executor_name' => $executor['name']));
							else $this->MailTask($executor['id'], $data['status']);
						}
					}
				}
			}	
			return $response;
		
	}
	
	function ApplySupport($add_data = array()) {
		global $DB, $Engine, $Auth;
		
		$DB->SetTable("nsau_people");
		$DB->AddFields(array("id", "comment", "name", "last_name", "patronymic"));
		$DB->AddCondFS("user_id", "=", $Auth->user_id);
		//$this->output["people"] = null; 
		$res = $DB->Select();
		while($row = $DB->FetchAssoc($res)) {
			$people = $row;
			$people["full_name"] = $people["last_name"]." ".$people["name"]." ". $people["patronymic"];
			$this->output["people"] = $people;
		}
		
		$this->output["examples"] = array("� ��� �� �������� �������", "�� ����������� Word", "������ �� ���������");
		
		if(count($add_data) > 0) {
			$app_subject = $add_data["app_subject"];
			$DB->SetTable('tasks');
			$DB->AddValues(array(
					"author_id" => $Auth->user_id,
					"author_name" => $Auth->usergroup_comment, 
					"status" => "new",
					//"executor_id" => (int)$this->settings['apply']['executor_id_'.$app_subject], 
					"description" => ( CODEPAGE == 'cp1251' ? iconv('utf-8', 'cp1251' , $add_data['app_descr']) : $data['app_descr']).($people ? " (<a href=\"http://".$_SERVER["HTTP_HOST"]."/people/".$people["id"]."/\">".$people["full_name"]."</a>) " : " (".$Auth->user_displayed_name.") "),
					));//echo $DB->InsertQuery(); exit;
			//if($DB->Insert()) 
			{
				$this->output = "������ ������� ���������";
				/* $taskId = $DB->LastInsertId();
				//$this->MailTask((int)$this->settings['apply']['executor_id_'.$app_subject], "new"); 
				$Engine->LogAction($this->module_id, 'task', $taskId, 'create');
				if((int)$this->settings['apply_'.$app_subject]['executor_id'] != 0) {
					$executorId = (int)$this->settings['apply_'.$app_subject]['executor_id'];
					$this->UpdateExecutors($executorId, $taskId);
				} */
				
				$this->settings['apply'] = array_merge($this->settings['apply'], $this->settings['apply_'.$app_subject]);
				$subject = explode(" ", $add_data["app_descr"]);
				$subject = array_slice($subject, 0, 6); 
				$subject = implode(" ", $subject);
				
				$mail_message_patterns = array(
							'headers' => array(
								'%site_short_name%' => '=?koi8-r?B?'.base64_encode(iconv('windows-1251', 'koi8-r', '����')).'?= '."<admin@".$_SERVER['HTTP_HOST'].">",
								'%server_name%' => $_SERVER['HTTP_HOST'],
								'%email%' => $this->settings['apply_'.$app_subject]["extra_email"],
								'%subject%' => $subject
							),
							'subject' => array(
								'%site_short_name%' => SITE_SHORT_NAME
							),
							'message' => array(
								'%time%' => date('Y-m-d H:i:s'),
								'%site_short_name%' => SITE_SHORT_NAME,
								'%author%' => ($people ? $people["full_name"]." (http://".$_SERVER["HTTP_HOST"]."/people/".$people["id"]."/)" : $Auth->user_displayed_name),
								'%assign_to%' => $this->settings['apply_'.$app_subject]["redmine_assign"],
								'%project%' => $this->settings['apply_'.$app_subject]["redmine_project"],
								'%content%' => ( CODEPAGE == 'cp1251' ? iconv('utf-8', 'cp1251' , $add_data['app_descr']) : $data['app_descr']),
							)
						);
				
						foreach ($this->settings['apply'] as $ind=>$setting) {
							$this->settings['apply'][$ind] = str_replace( array_keys($mail_message_patterns[$ind]), $mail_message_patterns[$ind], $setting);
						}
						
						mail($this->settings['apply_'.$app_subject]["extra_email"], '=?koi8-r?B?'.base64_encode(iconv('utf-8', 'koi8-r', $subject)).'?=', $this->settings['apply']['message'], $this->settings['apply']['headers'],/* $this->settings['student_registr_ok']['mess'],  */" -f admin@".$_SERVER['HTTP_HOST']."");
			}
			return ;
		}
	}

	
	function Output()
	{
		return $this->output;
	}
	
	
	
}
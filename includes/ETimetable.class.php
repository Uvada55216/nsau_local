<?php
class ETimetable
// version: 1.00
// date: 2015-06-25
{
    var $module_id;
    var $node_id;
    var $module_uri;
    var $privileges;
    var $output;
    var $mode;
    var $db_prefix;
    
    /*
    � ������ ������� ����� ������ faculties, exi
	people, people, search_group, groups ���� timetable � ��� � ��� =)
    */
    function ETimetable($global_params, $params, $module_id, $node_id = NULL, $module_uri = NULL, $additional = NULL)
    {
			global $DB, $Engine, $Auth;		
			$this->module_id = $module_id;
			$this->node_id = $node_id;
			$this->module_uri = $module_uri;
			$this->additional = $additional;
			$this->output = array();        
			$this->output["messages"] = array("good" => array(), "bad" => array());
			$this->output["module_id"] = $this->module_id;
			
			$parts = explode(";", $global_params);
			$this->db_prefix = $parts[0];
			$parts2 = explode(";", $params);
			
			$mode = explode("/", $module_uri);
			
			switch ($this->mode = $parts2[0]) {
				case "teachers_timetable": {
					$this->output["scripts_mode"][] = $this->output["mode"] = "teachers_timetable";
					$this->teachers_timetable($this->module_uri);
				}
				break;
				case "ajax_people_menu_timetable": {
					$this->output["mode"] = "people_menu_timetable";
					$this->teachers_timetable($_REQUEST["data"]);
				}
				break;	
				case "ajax_people_menu_stimetable": {
					$this->output["mode"] = "people_menu_stimetable";
					$DB->SetTable("nsau_people");
					$DB->AddCondFS("id", "=", $_REQUEST["data"]);
					$row = $DB->FetchAssoc($DB->Select(1));
					$this->output["timetable"] = $this->timetable_show($row["id_group"]);
					// $this->output["mode"] = "timetable_show";

				}
				break;
				// case "timetable": {
					// $this->timetable();
					// $this->output["scripts_mode"] = $this->output["mode"] = "timetable";
				// }
				// break;
				case "timetable_print": {
					$this->timetable_print();
					$this->output["scripts_mode"] = $this->output["mode"] = "timetable_print";
				}
				break;
				case "timetable_make": {
					$this->output["scripts_mode"] = $this->output["mode"] = "timetable_make";
					$this->timetable_make();
				}
				break;
				// case "timetable_subjects_by_departments": {
					// $this->output["scripts_mode"][] = $this->output["mode"] = "timetable_subjects_by_departments";
					// $this->output["scripts_mode"][] = 'input_calendar';
					// $this->output['plugins'][] = 'jquery.ui.datepicker.min';
					// $this->timetable_subjects_by_departments($mode);
				// }
				// break;
				// case "timetable_graphics": {
					// $this->output["scripts_mode"] = $this->output["mode"] = "timetable_graphics";
					// $this->timetable_graphics($mode);
				// }
				// break;
				// case "timetable_groups": {
					// $this->output["scripts_mode"] = $this->output["mode"] = "timetable_groups";
					// $this->timetable_groups($mode);
				// }
				// break;
				// case "timetable_faculties": {
					// $this->output["scripts_mode"] = $this->output["mode"] = "timetable_faculties";
					// $this->timetable_faculties($mode);
				// }
				// break;
				// case "timetable_departmens": {
					// $this->output["scripts_mode"] = $this->output["mode"] = "timetable_departmens";
					// $this->timetable_departmens($this->module_uri);
				// }
				// break;
				// case "timetable_teachers": {
					// $this->output["scripts_mode"] = $this->output["mode"] = "timetable_teachers";
					// if (isset($parts2[1])) {
						// $this->output["scripts_mode"] = $this->output["mode"] = "select_teachers";
						// $this->select_teachers($parts2[1]);
					// }
					// else
						// $this->timetable_teachers($mode);
				// }
				// break;
				// case "timetable_subjects": {
					// $this->output["scripts_mode"] = $this->output["mode"] = "timetable_subjects";
					// $this->timetable_subjects($mode);
				// }
				// break;
				// case "timetable_auditorium": {
					// $this->output["scripts_mode"] = $this->output["mode"] = "timetable_auditorium";
					// $this->timetable_auditorium($mode);
				// }
				// break;
				case "timetable_show": {
					$this->output["mode"] = "timetable_show";
					$this->output["scripts_mode"] = "timetable_make";
					$this->output["show_mode"] = $parts2[1];
					$this->timetable_show();
				}
				break;			
				case "ajax_edit_timetable": {
					$this->ajax_edit_timetable($_REQUEST["data"]["mode"], $_REQUEST["data"]["week"], $_REQUEST["data"]["day"], $_REQUEST["data"]["pair"], $_REQUEST["data"]["group"], $_REQUEST["data"]["data"], $_REQUEST["data"]["izop_week"]);
				}
        break;
				default:
          die("Ensau module error #1100 at node $this->node_id: unknown mode &mdash; $this->mode.");			
			
			}
		}
	function ajax_edit_timetable($mode, $week, $day, $pair, $group, $data = array(),$izop_week)
	{
		global $DB, $Engine;
		
		$DB->SetTable($this->db_prefix."timetable_new");
		$DB->AddCondFS("week", "=", $week);
		$DB->AddCondFS("day", "=", $day);
		$DB->AddCondFS("pair", "=", $pair);
		$DB->AddCondFS("group_id", "=", $group);
		if($izop_week!="")
			$DB->AddCondFS("izop_week", "=", $izop_week);
		$DB->AddOrder("id");		
		//die($DB->SelectQuery());
		$res = $DB->Select();
		$offset = $data["n"]+1;
		if($DB->NumRows($res)>=$offset)
		{
			for($i=0;$i<=$data["n"];$i++)
				$row = $DB->FetchAssoc($res);
				$edit_id = $row["id"];
		}
		else
			$edit_id = false;
		
		
		
		switch($mode){
			case "subj": {
				if($edit_id)
				{
					$DB->SetTable($this->db_prefix."timetable_new");
					$DB->AddCondFS("id", "=", $edit_id);										
					$DB->AddValue("subject_id", $data["id"]);	
					$DB->Update();
					$Engine->LogAction($this->module_id, "subject", $edit_id, "edit");
					$this->output["json"] = $row["id"].">".$i;
				}
				else
				{
					$DB->SetTable($this->db_prefix."timetable_new");					
					$DB->AddValue("subject_id", $data["id"]);					
					$DB->AddValue("week", $week);					
					$DB->AddValue("pair", $pair);					
					$DB->AddValue("day", $day);					
					$DB->AddValue("type", 0);					
					$DB->AddValue("group_id", $group);
					if($izop_week!="")
						$DB->AddValue("izop_week", $izop_week);			
					$DB->Insert();
					$new_id = $DB->LastInsertID();
					$Engine->LogAction($this->module_id, "subject", $new_id, "add");
						$this->output["json"]="add";
				}
			}
			break;
			case "type": {
				if($edit_id)
				{
					$DB->SetTable($this->db_prefix."timetable_new");
					$DB->AddCondFS("id", "=", $edit_id);										
					$DB->AddValue("type", $data["id"]);	
					$DB->Update();
					$Engine->LogAction($this->module_id, "type", $edit_id, "edit");
				}
				else
				{
					$this->output["json"]["error"] = "�������� ����������.";
				}	
			}
			break;
			case "subgr": {
				if($edit_id)
				{
					$DB->SetTable($this->db_prefix."timetable_new");
					$DB->AddCondFS("id", "=", $edit_id);										
					$DB->AddValue("subgroup", $data["id"]);
					$DB->Update();
					$Engine->LogAction($this->module_id, "subgroup", $edit_id, "edit");
				}
				else
				{
					$this->output["json"]["error"] = "�������� ����������.";
				}	
			}
			break;
			case "aud_add": {
				if($edit_id)
				{
					$DB->SetTable($this->db_prefix."timetable_new");
					$DB->AddCondFS("id", "=", $edit_id);
					$DB->AddField("auditorium_id");
					$res = $DB->Select(1);
					$row = $DB->FetchAssoc($res);
					$DB->SetTable($this->db_prefix."timetable_new");
					$DB->AddCondFS("id", "=", $edit_id);	
					if($row["auditorium_id"] == NULL)
					{									
						$DB->AddValue("auditorium_id", $data["id"].";");
					}
					else
					{
						$auds = explode(";", $row["auditorium_id"]);
						array_pop($auds);
						if(!in_array($data["id"], $auds))
						{
							$auds[] = $data["id"].";";
							$auditorium = implode(";", $auds);
							$DB->AddValue("auditorium_id", $auditorium);
						}
						else {
							$this->output["json"]["error"] = "������ ��������� ��� ���������.";
							return;
						}
					}
					$DB->Update();
					$Engine->LogAction($this->module_id, "auditorium", $edit_id, "edit");
					$this->output["json"] = $auditorium;
				}
				else
				{
					$this->output["json"]["error"] = "�������� ����������.";
				}	
			}
			break;
			case "aud_del": {
				if($edit_id)
				{
					$DB->SetTable($this->db_prefix."timetable_new");
					$DB->AddCondFS("id", "=", $edit_id);
					$DB->AddField("auditorium_id");
					$res = $DB->Select(1);
					$row = $DB->FetchAssoc($res);
					$DB->SetTable($this->db_prefix."timetable_new");
					$DB->AddCondFS("id", "=", $edit_id);	
					$auds = explode(";", $row["auditorium_id"]);
					array_pop($auds);
					foreach($auds as $aud)
						if($aud != $data["id"])
							$result[]=$aud;
							$auditorium = implode(";", $result);
							$DB->AddValue("auditorium_id", $auditorium.";");
							$DB->Update();
					$Engine->LogAction($this->module_id, "auditorium", $edit_id, "delete");
				}
				else
				{
					$this->output["json"]["error"] = "�������� ����������.";
				}	
			}
			break;
			case "teach_add": {
				if($edit_id)
				{
					$DB->SetTable($this->db_prefix."timetable_new");
					$DB->AddCondFS("id", "=", $edit_id);
					$DB->AddField("teacher_id");
					$res = $DB->Select(1);
					$row = $DB->FetchAssoc($res);
					$DB->SetTable($this->db_prefix."timetable_new");
					$DB->AddCondFS("id", "=", $edit_id);	
					if($row["teacher_id"] == NULL)
					{									
						$DB->AddValue("teacher_id", $data["id"].";");
					}
					else
					{
						$auds = explode(";", $row["teacher_id"]);
						array_pop($auds);
						if(!in_array($data["id"], $auds))
						{
							$auds[] = $data["id"].";";
							$auditorium = implode(";", $auds);
							$DB->AddValue("teacher_id", $auditorium);
						}
						else {
							$this->output["json"]["error"] = "������ ������������� ��� ��������.";
							return;
						}
					}
					$DB->Update();
					$Engine->LogAction($this->module_id, "teacher", $edit_id, "edit");
					$this->output["json"] = $auditorium;
				}
				else
				{
					$this->output["json"]["error"] = "�������� ����������.";
				}	
			}
			break;
			case "teach_del": {
				if($edit_id)
				{
					$DB->SetTable($this->db_prefix."timetable_new");
					$DB->AddCondFS("id", "=", $edit_id);
					$DB->AddField("teacher_id");
					$res = $DB->Select(1);
					$row = $DB->FetchAssoc($res);
					$DB->SetTable($this->db_prefix."timetable_new");
					$DB->AddCondFS("id", "=", $edit_id);	
					$auds = explode(";", $row["teacher_id"]);
					array_pop($auds);
					foreach($auds as $aud)
						if($aud != $data["id"])
							$result[]=$aud;
							$auditorium = implode(";", $result);
							$DB->AddValue("teacher_id", $auditorium.";");
					$DB->Update();
					$Engine->LogAction($this->module_id, "teacher", $edit_id, "delete");
				}
				else
				{
					$this->output["json"]["error"] = "�������� ����������.";
				}	
			}
			break;	
			case "add_time": {
				if($edit_id)
				{
					$DB->SetTable($this->db_prefix."timetable_new");
					$DB->AddCondFS("id", "=", $edit_id);										
					$DB->AddValue("comment_".$data["is"], implode("-",array_reverse(explode(".",$data["date"]))));
					$DB->Update();
					$Engine->LogAction($this->module_id, "time", $edit_id, "edit");
				}
				else
				{
					$this->output["json"]["error"] = "�������� ����������.";
				}	
			}
			break;
			case "comm_add": {
				if($edit_id)
				{
					$DB->SetTable($this->db_prefix."timetable_new");
					$DB->AddCondFS("id", "=", $edit_id);										
					$DB->AddValue("comment", iconv("utf-8", "cp1251", $data["text"]));
					$DB->Update();
					$Engine->LogAction($this->module_id, "comment", $edit_id, "edit");
				}
				else
				{
					$this->output["json"]["error"] = "�������� ����������.";
				}	
			}
			break;
			case "copy": {
				if($edit_id)
				{
					$DB->SetTable($this->db_prefix."timetable_new");
					$DB->AddCondFS("id", "=", $edit_id);										
					$res = $DB->Select();
					$row = $DB->FetchAssoc($res);
					foreach($data["id"] as $to_id)
					{
						$DB->SetTable($this->db_prefix."timetable_new");
						$DB->AddCondFS("week", "=", $week);
						$DB->AddCondFS("day", "=", $day);
						$DB->AddCondFS("pair", "=", $pair);
						$DB->AddCondFS("group_id", "=", $to_id);
						if($izop_week!="")
							$DB->AddCondFS("izop_week", "=", $izop_week);
						$DB->AddOrder("id");		
						$res2 = $DB->Select();
						$offset = $data["n"]+1;
						if($DB->NumRows($res2)>=$offset)
						{
							for($i=0;$i<=$data["n"];$i++)
								$row2 = $DB->FetchAssoc($res2);
								$edit_id = $row2["id"];
						}
						else
							$edit_id = false;					
						
						$DB->SetTable($this->db_prefix."timetable_new");
						if($edit_id) {
							$DB->AddCondFS("id", "=", $edit_id);
							$Engine->LogAction($this->module_id, "copy", $edit_id, "edit");
						}
						else
							$DB->AddValue("group_id", $to_id);
						$DB->AddValue("subgroup", $row["subgroup"]);
						$DB->AddValue("pair", $row["pair"]);  
						$DB->AddValue("day", $row["day"]);
						$DB->AddValue("week", $row["week"]);
						$DB->AddValue("auditorium_id", $row["auditorium_id"]);
						$DB->AddValue("teacher_id", $row["teacher_id"]);
						$DB->AddValue("subject_id", $row["subject_id"]);
						$DB->AddValue("type", $row["type"]);
						$DB->AddValue("comment", $row["comment"]);
						$DB->AddValue("comment_from", $row["comment_from"]);
						$DB->AddValue("comment_to", $row["comment_to"]);
						$DB->AddValue("ctime", $row["ctime"]);
						$DB->AddValue("izop_week",  $row["izop_week"]);
						if($edit_id)
							$DB->Update();						
						else {
							$DB->Insert();
							$new_id = $DB->LastInsertID();
							$Engine->LogAction($this->module_id, "copy", $new_id, "add");
						}
						$this->output["json"]["good"] = "�����������.";
					}
				}
				else
				{
					$this->output["json"]["error"] = "�������� ����������.";
				}	
			}
			break;
			case "clear": {
				if($edit_id)
				{
					$DB->SetTable($this->db_prefix."timetable_new");
					$DB->AddCondFS("id", "=", $edit_id);										
					$DB->Delete();
					$Engine->LogAction($this->module_id, "row", $edit_id, "clear");
				}
				else
				{
					$this->output["json"]["error"] = "�������� ����������.";
				}	
			}	
			break;
			case "actual_date": {
				$DB->SetTable($this->db_prefix."timetable_dates");
				$DB->AddCondFS("group_id", "=", $group);	
				if($data["izop_week"]!="")
					$DB->AddCondFS("week", "=", $data["izop_week"]);	
				$res = $DB->Select(1);
				$DB->SetTable($this->db_prefix."timetable_dates");				
				$DB->AddValue("actual_".$data["is"], implode("-",array_reverse(explode(".",$data["date"]))));				
				if($DB->NumRows($res)>0)
				{
					$DB->AddCondFS("group_id", "=", $group);
					if($data["izop_week"]!="")
						$DB->AddCondFS("week", "=", $data["izop_week"]);
					$DB->Update();
				}
				else
				{
					$DB->AddValue("group_id", $group);
					$DB->AddValue("week", $data["izop_week"]);
					$DB->Insert();
				}

			}	
			break;
		}
	}		
	function teachers_timetable($module_uri, $submode = null) {
		global $DB, $Engine, $Auth;
		if(!empty($module_uri)) {		
      $parts = explode ("/", $module_uri);
			$DB->SetTable("nsau_people");
			$DB->AddCondFS("id", "=", $parts[0]);
			$DB->AddFields(array("name", "last_name", "patronymic"));
			$p = $DB->Select(1);
			$people = $DB->FetchAssoc($p);
			$this->output["tt_people_name"] = $people["last_name"]." ".$people["name"]." ".$people["patronymic"];
			$DB->SetTable("nsau_teachers");
			$DB->AddCondFS("people_id", "=", $parts[0]);
			$res = $DB->Select(1);
			$this->output["tt_people_id"] = $parts[0];
			if($row=$DB->FetchAssoc($res)) {
				$DB->SetTable("nsau_timetable_new");
				$DB->AddCondFS("teacher_id", "LIKE", "%".$row["id"]."%");
				$f = $DB->Select();
				while($line = $DB->FetchAssoc($f)) {
					//auditorium
					$auditorium_id = str_replace(";", "", $line["auditorium_id"]);
					$DB->SetTable("nsau_auditorium", "a");
					$DB->AddCondFS("a.id", "=", $auditorium_id);
					$DB->AddTable("nsau_buildings", "b");
					$DB->AddCondFF("a.building_id", "=", "b.id");
					$DB->AddField("a.name", "auditorium_name");
					$DB->AddField("b.label", "auditorium_label");
					$res = $DB->Select(1);
					$row = $DB->FetchAssoc($res);
					$auditorium_name = $row["auditorium_label"]."-".$row["auditorium_name"];
					//group
					$group_id = $line["group_id"];
					$DB->SetTable("nsau_groups");
					$DB->AddCondFS("id", "=", $group_id);
					$DB->AddField("name");
					$res = $DB->Select(1);
					$row = $DB->FetchAssoc($res);
					$group_name = $row["name"];
					//subject
					$subject_id = $line["subject_id"];
					$DB->SetTable("nsau_subjects");
					$DB->AddCondFS("id", "=", $subject_id);
					$DB->AddField("name");
					$res = $DB->Select(1);
					$row = $DB->FetchAssoc($res);
					$subject_name = $row["name"];
					
					if($this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["one"]["from"]==$line["comment_from"] 
					|| !$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["one"]["groups"]) {
						$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["one"]["groups"] .= (!$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["one"]["groups"]) ? "".$group_name :", ".$group_name;
						$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["one"]["auditorium"] = $auditorium_name;
						$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["one"]["subject"] = $subject_name;
						$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["one"]["type"] = ($line["type"]==0) ? "�" : (($line["type"]==1) ? "��" : "�/�");
						$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["one"]["from"] = $line["comment_from"];
						$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["one"]["to"] = $line["comment_to"];
					}
					elseif($this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["two"]["from"]==$line["comment_from"] 
					|| !$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["two"]["groups"]) {
						$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["two"]["groups"] .= (!$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["two"]["groups"]) ? "".$group_name :", ".$group_name;
						$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["two"]["auditorium"] = $auditorium_name;
						$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["two"]["subject"] = $subject_name;
						$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["two"]["type"] = ($line["type"]==0) ? "�" : (($line["type"]==1) ? "��" : "�/�");
						$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["two"]["from"] = $line["comment_from"];
						$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["two"]["to"] = $line["comment_to"];												
					}
					else {
						$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["three"]["groups"] .= (!$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["three"]["groups"]) ? "".$group_name :", ".$group_name;
						$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["three"]["auditorium"] = $auditorium_name;
						$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["three"]["subject"] = $subject_name;
						$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["three"]["type"] = ($line["type"]==0) ? "�" : (($line["type"]==1) ? "��" : "�/�");
						$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["three"]["from"] = $line["comment_from"];
						$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["three"]["to"] = $line["comment_to"];												
					}
				}
				$this->output["pairs"] = $this->isset_pairs($this->output["tt"], "teachers");
			}
		}
	}		
	function timetable_print($students = null) {
		global $DB;
		$DB->SetTable("nsau_buildings");
		$DB->AddOrder("id");
		$res = $DB->Select();
		while ($row = $DB->FetchAssoc($res)){
			$this->output["buildings"][$row["id"]] = $row;
		}
		$gr = explode("z", $_GET["gr"]);
		array_shift($gr);
		$gr_weeks = explode("z", $_GET["weeks"]);
		array_shift($gr_weeks);
		$DB->SetTable("nsau_groups");
		foreach($gr as $g_id)
		$DB->AddAltFS("id", "=", $g_id);
		$DB->AddCondFS("hidden", "=", '0');
		$res = $DB->Select();
		while ($row = $DB->FetchAssoc($res)) {
			$groups[$row["id"]] = $row["name"];
			$facId = $row["id_faculty"];
			$specCode = $row["specialities_code"];
		}			
		$days = $x = $d =array();
		$i=0;
		if(empty($gr_weeks)) {
			foreach ($groups as $key => $val) {
				$a = $this->timetable_show($key);
				if(!empty($a)){
					$fileName .= $val.($i<(count($groups)-1) ? "_" : "");
					$i++;
					$this->output["groups"][$key] = $this->timetable_show($key);
					$this->output["groups"][$key]["group_name"] = $val;
				}
			}
		}
		else {
			$sql = "SELECT * from nsau_timetable_dates where group_id=".$gr[0]."";
			$res = $DB->Exec($sql);
			while ($row = $DB->FetchAssoc($res))
				$dates[$row["week"]] = implode(".", array_reverse(explode("-",$row["actual_from"])))."-".implode(".", array_reverse(explode("-",$row["actual_to"])));
			asort($gr_weeks);
			foreach ($gr_weeks as $w) {
				foreach ($groups as $key => $val) {
					// $a = $this->timetable_show($key, $w);
					// if(!empty($a)){
						$fileName .= (($i==0) ? $val."_" : "").$w.($i<(count($gr_weeks)-1) ? "_" : "");
						$i++;
						$this->output["groups"][$w] = $this->timetable_show($key, $w);
						$this->output["groups"][$w]["group_name"] = $val;
						$this->output["groups"][$w]["group_subname"] = " ".($w+1)." ���.".(!empty($dates[$w]) ?  " (".$dates[$w].")" : "");
					// }
				}			
			}					
		}
		$this->output["pairs"] = $this->isset_pairs($this->output["groups"]);
		$sql = "SELECT x.id as id, x.name as name, b.label as build
			FROM nsau_auditorium x
			LEFT JOIN nsau_buildings b on b.id = x.building_id";
		$res = $DB->Exec($sql);
		while ($row = $DB->FetchAssoc($res))
			$auds[$row["id"]] = $row;
		$this->output["auds"] = $auds;
		$sql = "SELECT x.id as id, p.id as people_id, p.name as p0, p.patronymic as p1, p.last_name as p2
			FROM nsau_teachers x
			LEFT JOIN nsau_people p on p.id=x.people_id";
		$res = $DB->Exec($sql);
		while ($row = $DB->FetchAssoc($res))
			$teachers[$row["id"]] = $row;				
		$this->output["teachers"] = $teachers;
		$DB->SetTable("nsau_faculties");
		$DB->AddCondFS("id", "=", $facId);
		$DB->AddField("name");
		$res = $DB->Select(1);
		$row = $DB->FetchAssoc($res);
		$this->output["info"]["facName"] = $row["name"];
		$DB->SetTable("nsau_specialities");
		$DB->AddCondFS("code", "=", $specCode);
		$DB->AddField("name");
		$res = $DB->Select(1);
		$row = $DB->FetchAssoc($res);
		$this->output["info"]["specName"] = $row["name"];		
		if(!empty($this->output["groups"])) {	
			header('Content-Type: text/html; charset=windows-1251');
			header('P3P: CP="NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM"');
			header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
			header('Cache-Control: no-store, no-cache, must-revalidate');
			header('Cache-Control: post-check=0, pre-check=0', FALSE);
			header('Pragma: no-cache');
			header('Content-transfer-encoding: binary');
			header('Content-Disposition: attachment; filename='.$fileName.'.xls');
			header('Content-Type: application/x-unknown');
		}
	} 
			
	function isset_pairs($arr, $mode=null) {
		if(!$mode)	{
			$pair = array();
			for($d=0;$d<7;$d++) {
				for($i=0;$i<7;$i++) {
					$isset_odd = 0;
					$isset_even = 0;
					foreach ($arr as $id=>$group) {
						$isset_odd = count($group["odd"][$d][$i]);
						$isset_even = count($group["even"][$d][$i]);
						if($isset_odd>0) {
							$pair[$id][$i]["odd"] = $isset_odd;
							$this->output['days'][$d] = 1;
						}
						if($isset_even>0) {
							$pair[$id][$i]["even"] = $isset_even;
							$this->output['days'][$d] = 1;
						}
					}
				}
			}
			foreach($arr as $id=>$g) {
				if(!isset($pair[$id]))
					$pair[$id] = 1;
			}
			ksort($pair);
			return $pair;
		}
		else {
			$pair = array();
			for($d=0;$d<7;$d++) {
				for($i=0;$i<7;$i++) {
					$isset_odd = 0;
					$isset_even = 0;
					$isset_odd = count($arr[0][$d][$i]);
					$isset_even = count($arr[1][$d][$i]);
					if($isset_odd>0) {
						$pair["days"][$d][0] = ($isset_odd>$pair["days"][$d][0]) ? $isset_odd : $pair["days"][$d][0];
						$pair["pairs"][$i] =  1;
					}
					if($isset_even>0) {
						$pair["pairs"][$i] =  1;
						$pair["days"][$d][1] = ($isset_even>$pair["days"][$d][1]) ? $isset_even : $pair["days"][$d][1];
					}
				}
			}
			return $pair;				
		}
	}					
	function timetable_make() {
		global $DB, $DB2;

		if (count($_POST) && isset($_GET["gr"])) {
			$DB->SetTable("nsau_timetable_new");
			$DB->AddCondFS("group_id", "=", $_GET["gr"]);
			$DB->Delete();

			for ($week=0; $week<=1; $week++) {
				for($day=0; $day<=6; $day++) {
					for($pair=0; $pair<=6; $pair++) {
						$wdps = array($week."_".$day."_".$pair, $week."_".$day."_".$pair."_d", $week."_".$day."_".$pair."_t");
						foreach ($wdps as $wdp) {
							if ($_POST["subj_".$wdp]) {
								if (isset($_POST["copy_".$wdp]) && is_array($_POST["copy_".$wdp])) {
									$grs = $_POST["copy_".$wdp];
									$grs[] = $_GET["gr"];

									foreach($grs as $group_id) {
										$DB->SetTable("nsau_timetable_new");
										$DB->AddCondFS("day","=",$day);
										$DB->AddCondFS("week","=",$week);
										$DB->AddCondFS("pair","=",$pair);
										$DB->AddCondFS("group_id","=",$group_id);
										if (isset($_POST["subgr_".$wdp]) && $_POST["subgr_".$wdp])
											$DB->AddCondFS("subgroup","=",$_POST["subgr_".$wdp]);
										$DB->Delete();


										$DB->SetTable("nsau_timetable_new");
										$DB->AddValue("group_id", $group_id);
										if (isset($_POST["subgr_".$wdp]) && ($_POST["type_".$wdp] == 1 || $_POST["type_".$wdp] == 2))
											$DB->AddValue("subgroup", $_POST["subgr_".$wdp]);
										else
											$DB->AddValue("subgroup", 0);
										$DB->AddValue("subject_id", $_POST["subj_".$wdp]);
										$DB->AddValue("auditorium_id", $_POST["aud_ids_".$wdp]);
										$DB->AddValue("teacher_id", $_POST["teach_ids_".$wdp]);
										$DB->AddValue("day", $day);
										$DB->AddValue("week", $week);
										$DB->AddValue("pair", $pair);
										$DB->AddValue("type", $_POST["type_".$wdp]);
										$DB->AddValue("comment_from", implode("-",array_reverse(explode(".",$_POST["comment_from_".$wdp]))));
										$DB->AddValue("comment_to", implode("-",array_reverse(explode(".",$_POST["comment_to_".$wdp]))));
										$DB->Insert();
									}
								}
								else {
									//echo $_POST["subj_".$wdp].">";
									$DB->SetTable("nsau_timetable_new");
									$DB->AddValue("group_id", $_GET["gr"]);
									if (isset($_POST["subgr_".$wdp]) && ($_POST["type_".$wdp] == 1 || $_POST["type_".$wdp] == 2))
										$DB->AddValue("subgroup", $_POST["subgr_".$wdp]);
									else
										$DB->AddValue("subgroup", 0);
									$DB->AddValue("subject_id", $_POST["subj_".$wdp]);
									$DB->AddValue("auditorium_id", $_POST["aud_ids_".$wdp]);
									$DB->AddValue("teacher_id", $_POST["teach_ids_".$wdp]);
									$DB->AddValue("day", $day);
									$DB->AddValue("week", $week);
									$DB->AddValue("pair", $pair);
									$DB->AddValue("type", $_POST["type_".$wdp]);
									$DB->AddValue("comment_from", implode("-",array_reverse(explode(".",$_POST["comment_from_".$wdp]))));
									$DB->AddValue("comment_to", implode("-",array_reverse(explode(".",$_POST["comment_to_".$wdp]))));
									$DB->Insert();
								}
							}
						}
						//die();
					}
				}
			}

			$DB->SetTable("nsau_timetable_dates");
			$DB->AddCondFS("group_id", "=", $_GET["gr"]);
			$DB->Delete();

			$DB->SetTable("nsau_timetable_dates");
			$DB->AddValue("group_id", $_GET["gr"]);
			$DB->AddValue("created", date("Y-m-d"));
			$DB->AddValue("actual_from", implode("-",array_reverse(explode(".",$_POST["actual_from"]))));
			$DB->AddValue("actual_to", implode("-",array_reverse(explode(".",$_POST["actual_to"]))));
			$DB->Insert();
		}

		$DB->SetTable("nsau_groups");
		$DB->AddCondFS("id", "=", $_GET["gr"]);
		$res = $DB->Select();
		$row = $DB->FetchAssoc($res);

		$DB->SetTable("nsau_groups");
		$DB->AddCondFS("form_education", "=", $row["form_education"]);
		$DB->AddCondFS("year", "=", $row["year"]);
		$DB->AddCondFS("id_faculty", "=", $row["id_faculty"]);
		$DB->AddCondFS("hidden", "=", 0);
		$res = $DB->Select();

		while ($row = $DB->FetchAssoc($res)){
			$this->output["potok"][$row["id"]] = $row["name"];
		}
		
		$grid = intval($_GET["gr"]);
		$res = $DB2->Exec("SELECT `nsau_log`.`ETimetable`.`time` FROM `nsau_log`.`ETimetable`, `nsau`.`nsau_timetable_new` 
											 WHERE `nsau`.`nsau_timetable_new`.`group_id`='".$grid."' AND `nsau_log`.`ETimetable`.`entry_id`=`nsau`.`nsau_timetable_new`.`id` 
											 order by `nsau_log`.`ETimetable`.`time` desc limit 0,1");
		$row = $DB2->FetchAssoc($res);
		if(!empty($row["time"])) {
			$t_part = explode(" ", $row["time"]);
			$date = implode(".", array_reverse(explode("-", $t_part[0])));			
			$this->output["edit_time"] = $date." ".$t_part[1];
		}
		
		$DB->SetTable("nsau_groups");
		$DB->AddCondFS("hidden", "=", 0);
                $res = $DB->Select();
                while ($row = $DB->FetchAssoc($res)){
                        $this->output["groups"][$row["id"]] = $row;
			if (isset($_GET["gr"]) && ($row["id"] == $_GET["gr"]))
				$fac_id = $row["id_faculty"];
                }
		
		if (isset($fac_id)) {
		$sql = "SELECT x.id as id, x.name as name, y.name as kaf, z.name as fac FROM nsau_subjects x LEFT JOIN nsau_departments y ON y.id = x.department_id LEFT JOIN nsau_faculties z ON z.id = y.faculty_id ORDER BY fac DESC, kaf, name";
			$res = $DB->Exec($sql);
		}
		else {
			$DB->SetTable("nsau_subjects");
        	        $res = $DB->Select();
		}
                while ($row = $DB->FetchAssoc($res)){
                        $this->output["subjects"][$row["id"]] = $row;
                }

		$sql = "SELECT * FROM nsau_faculties WHERE id in (SELECT id_faculty FROM nsau_groups)";
                $res = $DB->Exec($sql);
                while ($row = $DB->FetchAssoc($res)){
                        $this->output["faculties"][$row["id"]] = $row;
                }

                $sql = "SELECT x.id as id, people_id, name, last_name, patronymic FROM nsau_teachers x LEFT JOIN nsau_people y ON y.id = x.people_id WHERE name IS NOT NULL OR last_name IS NOT NULL ORDER BY last_name, name";
                $res = $DB->Exec($sql);
                while ($row = $DB->FetchAssoc($res)){
                        $this->output["teachers"][$row["id"]] = $row;
                }

                $sql = "SELECT x.id as aud_id, x.name as aud_name, y.label as label FROM nsau_auditorium x LEFT JOIN nsau_buildings y ON y.id = x.building_id ORDER BY label, aud_name";
                $res = $DB->Exec($sql);
                while ($row = $DB->FetchAssoc($res)){
                        $this->output["auds"][$row["aud_id"]] = $row;
                }

		if (isset($_GET["gr"])) {
		        $DB->SetTable("nsau_timetable_new");
			$DB->AddCondFS("group_id", "=", $_GET["gr"]);
			if($_GET["week"]!="") 
				$DB->AddCondFS("izop_week", "=", $_GET["week"]);
			$DB->AddOrder("id");
			$res = $DB->Select();
			while ($row = $DB->FetchAssoc($res)) {
				if ($row["week"]) {
					if (!isset($this->output["odd"][$row["day"]][$row["pair"]]))
				    $this->output["odd"][$row["day"]][$row["pair"]] = $row;
					elseif(!isset($this->output["doubles"][$this->output["odd"][$row["day"]][$row["pair"]]["id"]]))
						$this->output["doubles"][$this->output["odd"][$row["day"]][$row["pair"]]["id"]] = $row;
					else
						$this->output["triples"][$this->output["odd"][$row["day"]][$row["pair"]]["id"]] = $row;	
						
				}
				else {
					if (!isset($this->output["even"][$row["day"]][$row["pair"]]))
			      $this->output["even"][$row["day"]][$row["pair"]] = $row;
					elseif(!isset($this->output["doubles"][$this->output["even"][$row["day"]][$row["pair"]]["id"]]))
						$this->output["doubles"][$this->output["even"][$row["day"]][$row["pair"]]["id"]] = $row;
					else
						$this->output["triples"][$this->output["even"][$row["day"]][$row["pair"]]["id"]] = $row;	
				}
		        }

			$DB->SetTable("nsau_timetable_dates");
			$DB->AddCondFS("group_id", "=", $_GET["gr"]);
			$DB->AddCondFS("week", "=", $_GET["week"]);
			$res = $DB->Select();
			if ($row = $DB->FetchAssoc($res)) {
				$this->output["dates"] = $row;
			}
		}
	}		
  function timetable_show($gr_id = NULL, $weeks = null) {
    global $DB, $DB2;
		$sl = $gr_id;
		$grid = intval($_GET["gr"]);
		$res = $DB2->Exec("SELECT `nsau_log`.`ETimetable`.`time` FROM `nsau_log`.`ETimetable`, `nsau`.`nsau_timetable_new` 
											 WHERE `nsau`.`nsau_timetable_new`.`group_id`='".$grid."' AND `nsau_log`.`ETimetable`.`entry_id`=`nsau`.`nsau_timetable_new`.`id` 
											 order by `nsau_log`.`ETimetable`.`time` desc limit 0,1");
		$row = $DB2->FetchAssoc($res);
		if(!empty($row["time"])) {
			$t_part = explode(" ", $row["time"]);
			$date = implode(".", array_reverse(explode("-", $t_part[0])));			
			$this->output["edit_time"] = $date." ".$t_part[1];
		}
		
		$gr_id = $gr_id ? $gr_id : (isset($_GET["gr"]) ? htmlspecialchars($_GET["gr"]) : NULL);

		if (!$sl) {
			$sql = "SELECT * FROM nsau_faculties WHERE id in (SELECT id_faculty FROM nsau_groups)";
			$res = $DB->Exec($sql);
			while ($row = $DB->FetchAssoc($res)){
				   $this->output["faculties"][$row["id"]] = $row;
			}

			$DB->SetTable("nsau_groups");
			$DB->AddCondFS("hidden", "=", 0);
			$res = $DB->Select();
			while ($row = $DB->FetchAssoc($res)){
				    $this->output["groups"][$row["id"]] = $row;
			}

			$DB->SetTable("nsau_buildings");
			$DB->AddOrder("id");
			$res = $DB->Select();
			while ($row = $DB->FetchAssoc($res)){
				    $this->output["buildings"][$row["id"]] = $row;
			}

			$this->output["now"] = getdate();

			$this->output["parity"] = $this->timetable_weekparity();
		}

		if ($gr_id) {
			$sql = "SELECT x.*, y.name as spec, z.name as fac
			FROM nsau_groups x
			LEFT JOIN nsau_specialities y ON y.code = x.specialities_code
			LEFT JOIN nsau_faculties z ON z.id = x.id_faculty
			WHERE x.id=".$gr_id." AND x.hidden=0";

			$res = $DB->Exec($sql);
			if ($row = $DB->FetchAssoc($res)) {
				$this->output["group"] = $row;
			}

			$sql = "SELECT x.id as id, p.id as people_id, p.name as p0, p.patronymic as p1, p.last_name as p2
			FROM nsau_teachers x
			LEFT JOIN nsau_people p on p.id=x.people_id";
			$res = $DB->Exec($sql);

			while ($row = $DB->FetchAssoc($res))
				$teachers[$row["id"]] = $row;

			$sql = "SELECT x.id as id, x.name as name, b.label as build
			FROM nsau_auditorium x
			LEFT JOIN nsau_buildings b on b.id = x.building_id";
			$res = $DB->Exec($sql);

			while ($row = $DB->FetchAssoc($res))
				$auds[$row["id"]] = $row;

			$sql = "SELECT x.*, s.name as subj
			FROM nsau_timetable_new x
			LEFT JOIN nsau_groups g ON g.id = x.group_id
			LEFT JOIN nsau_subjects s ON s.id = x.subject_id
			WHERE x.group_id=".$gr_id."";
			if($_GET["week"]!="") 
				$sql .= " AND x.izop_week=".$_GET["week"];
			if(isset($weeks)) 
				$sql .= " AND x.izop_week=".$weeks;
			if (isset($_GET["subgr"]) && is_numeric($_GET["subgr"]) && ($_GET["subgr"] == 1 || $_GET["subgr"] == 2))
				$sql .= " AND (x.subgroup=0 OR x.subgroup=".$_GET["subgr"].")";
			$sql .= " ORDER BY x.id";
			$res = $DB->Exec($sql);

			$retarr = array();
			while ($row = $DB->FetchAssoc($res)) {
				$row["comment_to"] = implode(".",array_reverse(explode("-",$row["comment_to"])));
				$row["comment_from"] = implode(".",array_reverse(explode("-",$row["comment_from"])));
				$wname = $row["week"] ? "odd" : "even";
				$rowauds = explode(";", $row["auditorium_id"]);
				$rowteachers = explode(";", $row["teacher_id"]);
				foreach ($rowauds as $val)
					if ($val)
						$row["auds"][] = $auds[$val];
				foreach ($rowteachers as $val)
					if ($val)
						$row["teachs"][] = $teachers[$val];

				if (!$sl) {
					if (!isset($this->output[$wname][$row["day"]][$row["pair"]]))
						$this->output[$wname][$row["day"]][$row["pair"]] = $row;
					elseif(!isset($this->output["doubles"][$this->output[$wname][$row["day"]][$row["pair"]]["id"]]))
						$this->output["doubles"][$this->output[$wname][$row["day"]][$row["pair"]]["id"]] = $row;
					else
						$this->output["triples"][$this->output[$wname][$row["day"]][$row["pair"]]["id"]] = $row;
				}
				else {
					if (!isset($retarr[$wname][$row["day"]][$row["pair"]]))
						$retarr[$wname][$row["day"]][$row["pair"]] = $row;
					elseif(!isset($retarr["doubles"][$retarr[$wname][$row["day"]][$row["pair"]]["id"]]))
						$retarr["doubles"][$retarr[$wname][$row["day"]][$row["pair"]]["id"]] = $row;
					else
						$retarr["triples"][$retarr[$wname][$row["day"]][$row["pair"]]["id"]] = $row;
				}
			}

			if ($gr_id) {
				$DB->SetTable("nsau_timetable_dates");
				$DB->AddCondFS("group_id", "=", $gr_id);
				$DB->AddCondFS("week", "=", $_GET["week"]);
				$res = $DB->Select();
				if ($row = $DB->FetchAssoc($res))
					$this->output["dates"] = $row;
			}
		}
		if(!empty($retarr))
		return $retarr;
  }		
	function timetable_weekparity() {			//�������� ������� ������
		$today = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
		$first = mktime(0, 0, 0, 9, 3, (date("m") < 9 ? date("Y") - 1 : date("Y")));
		$first_week = date("w", mktime(0, 0, 0, 9, 3, (date("m") < 9 ? date("Y") - 1 : date("Y"))));
		$day = ($today - $first)/(24*60*60);
		$day += $first_week == 0 ? 6 : $first_week - 1;
		$j = (($day - ($day % 7)) / 7) + 1;
		if($j%2 == 1) return "odd"; 					//��������
		else return "even";						//������
	}	
	function Output() {
			return $this->output;
	}		
}?>		
﻿<?
class ETtAPI
// version: 0.0.1
// date: 2015-07-08
{
	var $output;
	var $data;
	function ETtAPI($global_params, $params, $module_id, $node_id, $module_uri) {
		global $Engine, $Auth, $DB;
		// header('Content-Type: application/json');
		$data = array(
			"faculty_id" => $_GET["fid"],
			"course" => $_GET["course"],
			"group_id" => $_GET["gid"],
			"day_id" => $_GET["did"]			
		);
		$data = array_map("intval",$data);
		$output = array();
		if(isset($_GET)) {
			switch ($_GET["mode"]) {
				case "faculty": {
					echo $this->getFaculty();
				}
				break;
				case "group": {
					echo $this->getGroups($data);
				}
				break;
				case "course": {
					echo $this->getCourse();
				}
				break;
				case "timetable": {
					echo $this->getTimetable($data);
					// print_r($this->getTimetable($data));
				}
				break;
				// case "ppc": {
					// $date = date("Y.m.d H:i:s");
					// echo $date;
					// if($date<"2015.09.23 14:20:01") {
						// $h1 = mysql_connect("localhost", "nsau", "EbAhRwRcLvpxHATe", 3306, true) or die('Не удалось соединиться: ' . mysql_error());
											// echo 'Соединение успешно установлено<br>';
						// $sql = "SELECT * FROM `nsau_log`.`EFiles` WHERE `entry_type` = 'file' AND `action` = 'delete' order by `time` ASC";
						// $res = mysql_query($sql);
						// while($row = mysql_fetch_assoc($res)) {
							// $isql = "UPDATE `nsau`.`nsau_files` SET `nsau`.`nsau_files`.`delete_time` = '".$row["time"]."' WHERE `nsau`.`nsau_files`.`id` = '".$row["entry_id"]."' AND `nsau`.`nsau_files`.`deleted`='1'";							
							// $ires = mysql_query($isql);
							// echo $isql."<br>";
						// }
					// }
				// }
				// break;
				default:
				break;
			}				
		}
	}	
	function getGroups($data) {
		global $DB;
		if(!empty($data["faculty_id"]) && !empty($data["course"])) {
			$DB->SetTable("nsau_groups");
			$DB->AddCondFS("hidden", "=", "0");
			$DB->AddCondFS("id_faculty", "=", $data["faculty_id"]);
			$DB->AddCondFS("year", "=", $data["course"]);
			$res = $DB->Select();
			while ($row = $DB->FetchAssoc($res)) {
				 $pre["id"] = $row["id"]; 
				 $pre["name"] = iconv("cp1251", "utf-8", $row["name"]); 
				 $output["groups"][] = $pre;
					
				}
			return json_encode($output);
		}
	}
	function getFaculty() {
		global $DB;
		$DB->SetTable("nsau_faculties");
		$DB->AddCondFS("is_active", "=", "1");
		$DB->AddField("id");
		$DB->AddField("name");		
		$res = $DB->Select();
		while ($row = $DB->FetchAssoc($res)) {
			 $out["id"] = $row["id"]; 
			 $out["name"] = iconv("cp1251", "utf-8", $row["name"]); 
			 $output["faculties"][] = $out;							
		}
		return json_encode($output);
	}
	function getCourse() {
		$out["courses"] = array();
		for($i=1;$i<7;$i++){
			$out["courses"][$i-1]["id"] = $i;
			$out["courses"][$i-1]["name"] = $i;			
		} 	
		return json_encode($out);
	}
	function getTimetable($data) {
		global $DB;
		$week_type = array(0=>"odd", 1=>"even");
		//group name
		$DB->SetTable("nsau_groups");
		$DB->AddCondFS("id", "=", $data["group_id"]);
		$DB->AddField("name");
		$res = $DB->Select();
		$row = $DB->FetchAssoc($res);
		$out["name"] = iconv("cp1251", "utf-8", $row["name"]);
		//timetable
		$DB->SetTable("nsau_timetable_new");
		$DB->AddCondFS("group_id", "=", $data["group_id"]);
		$res_tt = $DB->Select(); 
		while($tt = $DB->FetchAssoc($res_tt)) {
			//auditorium
			if(!empty($tt["auditorium_id"])) {
				$auds = array();
				foreach($this->parseStr($tt["auditorium_id"]) as $val) {
					$DB->SetTable("nsau_auditorium", "na");
					$DB->AddCondFS("na.id", "=", $val);
					$DB->AddTable("nsau_buildings", "nb");
					$DB->AddCondFF("na.building_id", "=", "nb.id");
					$DB->AddField("na.name", "name");
					$DB->AddField("nb.label", "label");
					$res = $DB->Select();
					$row = $DB->FetchAssoc($res);
					$auds[] = iconv("cp1251", "utf-8", $row["label"]."-".$row["name"]);
				}
				$pre["auditorium"] = "".implode(", ", $auds);
			} else 
				$pre["auditorium"] = "";
			//teacher
			if(!empty($tt["teacher_id"])) {
				$teachs = array();
				foreach($this->parseStr($tt["teacher_id"]) as $val) { 
					$DB->SetTable("nsau_teachers", "nt");
					$DB->AddCondFS("nt.id", "=", $val);
					$DB->AddTable("nsau_people", "np");
					$DB->AddCondFF("nt.people_id", "=", "np.id");
					$DB->AddField("np.name", "name");
					$DB->AddField("np.last_name", "last_name");
					$DB->AddField("np.patronymic", "patronymic");
					$res = $DB->Select();
					$row = $DB->FetchAssoc($res);
					$teachs[] = trim(iconv("cp1251", "utf-8", trim($row["last_name"])." ".trim($row["name"][0]).". ".trim($row["patronymic"][0])));
				}
				$pre["teachers"] = ", ".implode(", ", $teachs);
			} else
				$pre["teachers"] = "";
			//subject
			if(!empty($tt["subject_id"])) {			
				$DB->SetTable("nsau_subjects");
				$DB->AddCondFS("id", "=", $tt["subject_id"]);
				$DB->AddField("name");
				$res = $DB->Select();
				$row = $DB->FetchAssoc($res);				
				$pre["subject"] = iconv("cp1251", "utf-8", $row["name"]);
			}
			//pair
				$pre["pair"] = $tt["pair"];
			//week
				$pre["week"] = $tt["week"];
			//type
				$pre["type"] = (($tt["type"]==0) ? "(Л)\n " : (($tt["type"]==1) ? "(Пр)\n " : "(Лаб)\n "));				
			//comment
				$pre["comment"] = iconv("cp1251", "utf-8", $tt["comment"]);		
			//from
				$pre["comment_from"] = (!empty($pre["comment_from"])) ? "\n с ".iconv("cp1251", "utf-8", $tt["comment_from"])."\n" : "";	
			//from
				$pre["comment_to"] =  (!empty($pre["comment_to"])) ? " по ".iconv("cp1251", "utf-8", $tt["comment_to"]) : "";		
				if(!isset($out["data"][$tt["day"]]["pairs"][$week_type[$tt["week"]]][$tt["pair"]])) {
					$out["data"][$tt["day"]]["pairs"][$week_type[$tt["week"]]][$tt["pair"]] = $pre;
				}
				elseif(!isset($out["data"][$tt["day"]]["double"]["pairs"][$week_type[$tt["week"]]][$tt["pair"]])){
					$out["data"][$tt["day"]]["double"]["pairs"][$week_type[$tt["week"]]][$tt["pair"]] = $pre;
				} else {
					$out["data"][$tt["day"]]["triple"]["pairs"][$week_type[$tt["week"]]][$tt["pair"]] = $pre;
				}
		}	
		$empty_row = array("subject" => "", "teachers" => "", "auditorium" => "", "type" => "", "comment_from" => "", "comment_to" => "", "comment" => "");
		foreach($week_type as $id => $val) {
			for($d=0;$d<7;$d++) {
				for($p=0;$p<7;$p++) {
					if(!isset($out["data"][$d]["pairs"][$val][$p]))
						$out["data"][$d]["pairs"][$val][$p] = $empty_row;
					if(!isset($out["data"][$d]["double"]["pairs"][$val][$p]))
						$out["data"][$d]["double"]["pairs"][$val][$p] = $empty_row;
					if(!isset($out["data"][$d]["triple"]["pairs"][$val][$p]))
						$out["data"][$d]["triple"]["pairs"][$val][$p] = $empty_row;
					ksort($out["data"][$d]["pairs"]["odd"]);
					ksort($out["data"][$d]["double"]["pairs"]["odd"]);
					ksort($out["data"][$d]["triple"]["pairs"]["odd"]);
					ksort($out["data"][$d]["pairs"]["even"]);
					ksort($out["data"][$d]["double"]["pairs"]["even"]);
					ksort($out["data"][$d]["triple"]["pairs"]["even"]);
				}			
			}
		}

				


		
		ksort($out["data"]);
		
		return json_encode($out);		   
		// return $out;		   
	}
	function parseStr($str) {
		$arr = explode(";", $str);
		foreach($arr as $val) {
			if(!empty($val))
				$out[] = $val;
		}
		return $out;
	}
	function Output() {
		return $this->output;
	}
}	
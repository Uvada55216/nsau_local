<?php
class EVacancies
	// version: 1.12
	// date: 2014-04-09
{
	var $module_id;
	var $node_id;
	var $module_uri;
	var $privileges;
	var $output;

	var $mode;
	var $db_prefix;
	var $maintain_cache;
	var $item_id;

	var $config; // ��������� � ����� ������������ EVacancies.ini

	function EVacancies($global_params, $params, $module_id, $node_id, $module_uri) {
		global $Engine, $Auth, $DB;
		$this->node_id = $node_id;
		$this->module_id = $module_id;
		$this->module_uri = $module_uri;

		// ������������ ���� ������������
		$this->processConfigFile($global_params);

		$this->output["user_displayed_name"] = $Auth->user_displayed_name;
		$this->output["messages"]["good"] = $this->output["messages"]["bad"] = array();

		$parts = explode("/", $this->module_uri);

		if (!empty($parts[0]) && $parts[0]=="edit" && !empty($parts[1]))
			$this->mode = "vacancy_edit";
		elseif (!empty($parts[0]) && $parts[0]=="delete" && !empty($parts[1]))
			$this->mode = "vacancy_delete";
		elseif (!empty($parts[0]) && $parts[0] == "add")
			$this->mode = "vacancy_add";
		elseif (!empty($parts[0]) && $parts[0] == "add_csv")
			$this->mode = "vacancy_add_csv";
		elseif (!empty($parts[0]))
			$this->mode = "vacancy_view";
		else
			$this->mode = "vacancies_list";

		switch ($this->mode) {
			case "vacancies_list":
				$this->output["mode"] = "vacancies_list";
				$this->vacancies_list($params);
				break;

			case "vacancy_edit":
				if ($Engine->OperationAllowed(10, "vacancies.handle", 0, $Auth->usergroup_id)) {
					$this->output["mode"] = $this->output["scripts_mode"] = "vacancy_edit";
					$this->output['plugins'][] = 'jquery.ui.datepicker.min';
					$this->vacancy_edit($parts[1]);
				}
				break;

			case "vacancy_delete":
				if ($Engine->OperationAllowed(10, "vacancies.handle", 0, $Auth->usergroup_id)) {
					$this->output["mode"] = "vacancies_list";
					$this->vacancy_delete($parts[1]);
				}
				break;

			case "vacancy_add":
				if ($Engine->OperationAllowed(10, "vacancies.handle", 0, $Auth->usergroup_id)) {
					$this->output["mode"] = $this->output["scripts_mode"] = "vacancy_add";
					$this->output['plugins'][] = 'jquery.ui.datepicker.min';
					$this->vacancy_add();
				}
				break;

			case "vacancy_add_csv":
				if ($Engine->OperationAllowed(10, "vacancies.handle", 0, $Auth->usergroup_id)) {
					$this->output["mode"] = $this->output["scripts_mode"] = "vacancy_add_csv";
					if (!empty($_FILES['vac_file'])) {
						$fname = explode(".", $_FILES['vac_file']['name']);
						$fname = array_reverse($fname);
						if ($fname[0] != "csv") {
							$this->output["messages"]["bad"][] = "��������� ���� � ������� csv";
						} else {
							$f = fopen($_FILES['vac_file']['tmp_name'], "r");
							$content = fread($f, filesize($_FILES['vac_file']['tmp_name']));
							fclose($f);
							$parts = explode("\r\n", $content);
							array_shift($parts);
							$error = false;

							for($i=0;$i<count($parts);$i++) {
								$vac = explode(";", $parts[$i]);
								if(!empty($vac[0])) {
									$DB->SetTable("nsau_vacancies_education");
									$DB->AddCondFS("education", "=", trim($vac[4]));
									$edu = $DB->FetchAssoc($DB->Select(1));
									if(empty($edu)) {
										$error = true;
										$this->output["messages"]["bad"][] = "������� ������� �����������. ������: ".($i+2)."";
										return;
									}
									$DB->SetTable("nsau_vacancies_lodging");
									$DB->AddCondFS("lodging", "=", trim($vac[6]));
									$log = $DB->FetchAssoc($DB->Select(1));
									if(empty($log)) {
										$error = true;
										$this->output["messages"]["bad"][] = "������� ������� �����. ������: ".($i+2)."";
										return;
									}

									$DB->SetTable("nsau_faculties");
									$DB->AddCondFS("name", "=", trim($vac[8]));
									$fac = $DB->FetchAssoc($DB->Select(1));
									if(empty($fac)) {
										$error = true;
										$this->output["messages"]["bad"][] = "������� ������� �����������. ������: ".($i+2)."";
										return;
									}

									$DB->SetTable("nsau_specialities");
									$DB->AddCondFS("name", "=", trim($vac[9]));
									$spec = $DB->FetchAssoc($DB->Select(1));
									if(empty($spec)) {
										$error = true;
										$this->output["messages"]["bad"][] = "������� ������� �������������. ������: ".($i+2)."";
										return;
									}

									$results[] = array("vacancy" => $vac[0], "company" => $vac[1], "district" => $vac[2], "salary" => $vac[3],
										"education_id" => $edu["id"], "requirements" => $vac[5], "lodging_id" => $log["id"],
										"contacts" => $vac[7], "faculty_id" => $fac["id"], "speciality_id" => $spec["code"]);


								}
							}

							if(!$error) {
								foreach($results as $result) {
									$DB->SetTable("nsau_vacancies");
									foreach($result as $field => $val) {
										$DB->AddValue($field, $val);
									}
									$DB->AddValue("pub_date", date("Y-m-d"));
									$DB->Insert();
								}
								$this->output["messages"]["good"][] = "�������� ����������.";
								CF::Redirect($Engine->engine_uri);

							}

						}
					}
				}
				break;

			case "vacancy_view":
				$this->output["mode"] = "vacancy_view";
				$this->vacancy_view($parts[0]);
				break;
		}

	}

	function vacancies_list($params) {
		global $DB, $Engine, $Auth;

		require_once INCLUDES . "Pager" . CLASS_EXT;

		$DB->SetTable("nsau_vacancies_lodging");
		$res = $DB->Select();

		while ($row = $DB->FetchAssoc($res)) {
			$lodging[$row["id"]] = $row["lodging"];
			$this->output["lodgings"][] = $row;
		}

		$res = $DB->Exec("select distinct district from `nsau_vacancies` order by district");
		while ($row = $DB->FetchAssoc($res)) {
			$this->output["districts"][] = $row;
		}

		$res = $DB->Exec("select distinct vacancy from `nsau_vacancies` order by vacancy");
		while ($row = $DB->FetchAssoc($res)) {
			$this->output["titles"][] = $row;
		}

		$DB->SetTable("nsau_specialities", "s");
		$DB->AddTable("nsau_vacancies", "v");
		$DB->AddField("s.code");
		$DB->AddField("s.name");
		$DB->AddCondFF("v.speciality_id", "=", "s.code");

		if(isset($this->config['id_faculty']) && count($this->config['id_faculty'])){
			$DB->AddCondFO('s.id_faculty', "NOT IN(".implode(",", $this->config['id_faculty']).")");
		}
		$DB->AddGrouping('s.name');
		$res_spec = $DB->Select();

		while ($row = $DB->FetchAssoc($res_spec)) {
			$this->output["specialities"][$row["code"]] = $row;
		}

		$DB->SetTable("nsau_faculties", "f");
		$DB->AddTable("nsau_vacancies", "v");
		$DB->AddField("f.id");
		$DB->AddField("f.name");
		$DB->AddCondFF("v.faculty_id", "=", "f.id");
		$res_lodg = $DB->Select();

		while ($row = $DB->FetchAssoc($res_lodg)) {
			$this->output["faculties"][$row["id"]] = $row;
		}

		/*$DB->SetTable("nsau_vacancies");
		$res = $DB->Select();
		$num = 0;

		while ($row = $DB->FetchAssoc($res)) {
			$num++;
		}*/

		$parts = explode(";", $params);
		$per_page_input = (isset($parts[0]) && $parts[0]) ? $parts[0] : "50"; // ������ ��������� ��� Pager

		$DB->SetTable("nsau_vacancies");
		if (isset($_POST[$this->node_id]["filter"]["district"]) && $_POST[$this->node_id]["filter"]["district"])
			$_SESSION["filter_district"] = $_POST[$this->node_id]["filter"]["district"];
		elseif (isset($_POST[$this->node_id]["filter"]["district"]))
			unset($_SESSION["filter_district"]);

		if (isset($_POST[$this->node_id]["filter"]["salary_from"]) && $_POST[$this->node_id]["filter"]["salary_from"])
			$_SESSION["filter_salary_from"] = $_POST[$this->node_id]["filter"]["salary_from"];
		elseif (isset($_POST[$this->node_id]["filter"]["salary_from"]))
			unset($_SESSION["filter_salary_from"]);

		if (isset($_POST[$this->node_id]["filter"]["salary_to"]) && $_POST[$this->node_id]["filter"]["salary_to"])
			$_SESSION["filter_salary_to"] = $_POST[$this->node_id]["filter"]["salary_to"];
		elseif (isset($_POST[$this->node_id]["filter"]["salary_to"]))
			unset($_SESSION["filter_salary_to"]);

		if (isset($_POST[$this->node_id]["filter"]["lodging"]) && $_POST[$this->node_id]["filter"]["lodging"])
			$_SESSION["filter_lodging"] = $_POST[$this->node_id]["filter"]["lodging"];
		elseif (isset($_POST[$this->node_id]["filter"]["lodging"]))
			unset($_SESSION["filter_lodging"]);

		if (isset($_POST[$this->node_id]["filter"]["vacancy"]) && $_POST[$this->node_id]["filter"]["vacancy"])
			$_SESSION["filter_vacancy"] = $_POST[$this->node_id]["filter"]["vacancy"];
		elseif (isset($_POST[$this->node_id]["filter"]["vacancy"]))
			unset($_SESSION["filter_vacancy"]);

		if (isset($_POST[$this->node_id]["filter"]["speciality_id"]) && $_POST[$this->node_id]["filter"]["speciality_id"])
			$_SESSION["filter_speciality_id"] = $_POST[$this->node_id]["filter"]["speciality_id"];
		elseif (isset($_POST[$this->node_id]["filter"]["speciality_id"]))
			unset($_SESSION["filter_speciality_id"]);

		if (isset($_POST[$this->node_id]["filter"]["faculty_id"]) && $_POST[$this->node_id]["filter"]["faculty_id"])
			$_SESSION["filter_faculty_id"] = $_POST[$this->node_id]["filter"]["faculty_id"];
		elseif (isset($_POST[$this->node_id]["filter"]["faculty_id"]))
			unset($_SESSION["filter_faculty_id"]);

		if (isset($_GET[$this->node_id]["filter"]["company"]) && $_GET[$this->node_id]["filter"]["company"])
			$_SESSION["filter_company"] = $_GET[$this->node_id]["filter"]["company"];
		elseif (isset($_GET[$this->node_id]["filter"]["company"]) || $_SERVER["REQUEST_METHOD"] == "POST" )
			unset($_SESSION["filter_company"]);

		if (isset($_SESSION["filter_district"]))
			$DB->AddCondFS("district", "=", $_SESSION["filter_district"]);
		if (isset($_SESSION["filter_lodging"]))
			$DB->AddCondFS("lodging_id", "=", $_SESSION["filter_lodging"]);
		if (isset($_SESSION["filter_vacancy"]))
			$DB->AddCondFS("vacancy", "=", $_SESSION["filter_vacancy"]);
		if (isset($_SESSION["filter_speciality_id"]))
			$DB->AddCondFS("speciality_id", "=", $_SESSION["filter_speciality_id"]);
		if (isset($_SESSION["filter_faculty_id"]))
			$DB->AddCondFS("faculty_id", "=", $_SESSION["filter_faculty_id"]);
		if (isset($_SESSION["filter_salary_from"]))
			$DB->AddCondFS("salary", ">=", $_SESSION["filter_salary_from"]);
		if (isset($_SESSION["filter_salary_to"]))
			$DB->AddCondFS("salary", "<=", $_SESSION["filter_salary_to"]);
		if (isset($_SESSION["filter_company"]))
			$DB->AddCondFS("company", "=", $_SESSION["filter_company"]);

		if(!isset($_GET["sortby"])) $_GET["sortby"] = "pub_date";
		if(!isset($_GET["desc"])) { $_GET["desc"] = 1; }
		if (isset($_GET["sortby"]))
			$DB->AddOrder($_GET["sortby"], $_GET["desc"]);

		$res_count = $DB->Select();
		$num = 0;
		while ($row = $DB->FetchAssoc($res_count))
			$num++;

		$Pager = new Pager($num, $per_page_input);
		$this->output["pager_output"] = $result = $Pager->Act();

		$DB->SetTable("nsau_vacancies");
		$DB->AddExp('*');
		$DB->AddExp("DATE_FORMAT(pub_date, '%d.%m.%Y')", "vac_date");
		//$DB->AddExp('*');
		if (isset($_SESSION["filter_district"]))
			$DB->AddCondFS("district", "=", $_SESSION["filter_district"]);
		if (isset($_SESSION["filter_lodging"]))
			$DB->AddCondFS("lodging_id", "=", $_SESSION["filter_lodging"]);
		if (isset($_SESSION["filter_vacancy"]))
			$DB->AddCondFS("vacancy", "=", $_SESSION["filter_vacancy"]);
		if (isset($_SESSION["filter_speciality_id"]))
			$DB->AddCondFS("speciality_id", "=", $_SESSION["filter_speciality_id"]);
		if (isset($_SESSION["filter_faculty_id"]))
			$DB->AddCondFS("faculty_id", "=", $_SESSION["filter_faculty_id"]);
		if (isset($_SESSION["filter_salary_from"]))
			$DB->AddCondFS("salary", ">=", $_SESSION["filter_salary_from"]);
		if (isset($_SESSION["filter_salary_to"]))
			$DB->AddCondFS("salary", "<=", $_SESSION["filter_salary_to"]);
		if (isset($_SESSION["filter_company"]))
			$DB->AddCondFS("company", "=", $_SESSION["filter_company"]);

		if (isset($_GET["sortby"]))
			$DB->AddOrder($_GET["sortby"], $_GET["desc"]);

		$res = $DB->Select($result["db_limit"], $result["db_from"]);

		$num = 0;
		while ($row = $DB->FetchAssoc($res)) {
			$row["lodging"] = $lodging[$row["lodging_id"]];
			$this->output["vacancies_list"][] = $row;
			$num++;
		}

		if ($Engine->OperationAllowed(10, "vacancies.handle", 0, $Auth->usergroup_id))
			$this->output["show_manage"] = 1;
		else
			$this->output["show_manage"] = 0;
	}

	function vacancy_edit($id) {
		global $DB, $Engine;

		if (isset($_POST[$this->node_id]["edit_vacancy"])) {
			$DB->SetTable("nsau_vacancies");
			$DB->AddValue("district", $_POST[$this->node_id]["edit_vacancy"]["district"]);
			$DB->AddValue("vacancy", $_POST[$this->node_id]["edit_vacancy"]["vacancy"]);
			$DB->AddValue("company", $_POST[$this->node_id]["edit_vacancy"]["company"]);
			$DB->AddValue("education_id", $_POST[$this->node_id]["edit_vacancy"]["education_id"]);
			$DB->AddValue("requirements", $_POST[$this->node_id]["edit_vacancy"]["requirements"]);
			$DB->AddValue("salary", $_POST[$this->node_id]["edit_vacancy"]["salary"]);
			$DB->AddValue("lodging_id", $_POST[$this->node_id]["edit_vacancy"]["lodging_id"]);
			$DB->AddValue("contacts", $_POST[$this->node_id]["edit_vacancy"]["contacts"]);
			$DB->AddValue("faculty_id", implode(";", $_POST[$this->node_id]["edit_vacancy"]["faculty_id"]));
			$DB->AddValue("speciality_id", implode(";", $_POST[$this->node_id]["edit_vacancy"]["speciality_id"]));
			$DB->AddValue("pub_date", $_POST[$this->node_id]["edit_vacancy"]["pub_date"]);
			$DB->AddValue("working", $_POST[$this->node_id]["edit_vacancy"]["working"]);
			$DB->AddCondFS("id", "=", $id);

			/*���������� ��������*/
			if (filesize($_FILES["logo"]["tmp_name"])<1000000)
			{
				if (!empty($_FILES["logo"]["name"]))
				{
					$filename = $_FILES["logo"]["name"];
					$type = strtolower(substr($filename, 1+strrpos($filename,".")));
					$path = 'files/vac_logo/';
					$fname = "logo_". rand() .  "_" . $filename;
					$fname = str_replace(" ","",$fname);
					$DB->AddValue("logo", $fname);
					/*�������� �����*/
					if (!@copy($_FILES["logo"]["tmp_name"], $path . $fname))
					{
						$this->output["messages"]["bad"][] = "������ �������� ����� �� ������.";
					}
				}
			}
			else
			{
				$this->output["messages"]["bad"][] = "������ ����� ��������� 1��.";
			}


			/*�������� ��������*/
			if (isset($_POST['delete_logo']))
			{
				$DB->AddValue("logo", "");
				unlink('files/vac_logo/'.$_POST["current_logo"]);
			}
			if ($DB->Update())
				$this->output["messages"]["good"][] = "�������� ���������";
		}

		$DB->SetTable("nsau_vacancies");
		$DB->AddCondFS("id", "=", $id);
		$res = $DB->Select();

		if ($row = $DB->FetchAssoc($res)) {
			$row["faculty_id"] = explode(";", $row["faculty_id"]);
			$row["speciality_id"] = explode(";", $row["speciality_id"]);
			$row["company"] = str_replace('"', "&quot;", $row["company"]);
			$this->output["vacancy"] = $row;

			$Engine->AddFootstep($Engine->engine_uri.$Engine->module_uri, "�������������� �������� \"".$row['vacancy']."\"", '', false);

			$DB->SetTable("nsau_vacancies_education");
			$res_edu = $DB->Select();

			while ($row = $DB->FetchAssoc($res_edu)) {
				$this->output["educations"][] = $row;
			}

			$DB->SetTable("nsau_vacancies_lodging");
			$res_lodg = $DB->Select();

			while ($row = $DB->FetchAssoc($res_lodg)) {
				$this->output["lodgings"][] = $row;
			}

			$DB->SetTable("nsau_faculties");
			$DB->AddField("id");
			$DB->AddField("name");

			if(isset($this->config['id_faculty']) && count($this->config['id_faculty'])){
				$DB->AddCondFO('id', "NOT IN(".implode(",", $this->config['id_faculty']).")");
			}

			$res_lodg = $DB->Select();

			while ($row = $DB->FetchAssoc($res_lodg)) {
				$this->output["faculties"][] = $row;
			}

			$DB->SetTable("nsau_specialities");
			$DB->AddField("code");
			$DB->AddField("name");

			if(isset($this->config['id_faculty']) && count($this->config['id_faculty'])){
				$DB->AddCondFO('id_faculty', "NOT IN(".implode(",", $this->config['id_faculty']).")");
			}
			$DB->AddGrouping('name');

			$res_lodg = $DB->Select();

			while ($row = $DB->FetchAssoc($res_lodg)) {
				$this->output["specialities"][] = $row;
			}
		}
		else
			$this->output["messages"]["bad"][] = "�������� ����� ��������";
	}

	function vacancy_add() {
		global $DB, $Engine;

		if (isset($_POST[$this->node_id]["add_vacancy"]))
		{
			$DB->SetTable("nsau_vacancies");
			$DB->AddValue("district", $_POST[$this->node_id]["add_vacancy"]["district"]);
			$DB->AddValue("vacancy", $_POST[$this->node_id]["add_vacancy"]["vacancy"]);
			$DB->AddValue("company", $_POST[$this->node_id]["add_vacancy"]["company"]);
			$DB->AddValue("education_id", $_POST[$this->node_id]["add_vacancy"]["education_id"]);
			$DB->AddValue("requirements", $_POST[$this->node_id]["add_vacancy"]["requirements"]);
			$DB->AddValue("salary", $_POST[$this->node_id]["add_vacancy"]["salary"]);
			$DB->AddValue("lodging_id", $_POST[$this->node_id]["add_vacancy"]["lodging_id"]);
			$DB->AddValue("contacts", $_POST[$this->node_id]["add_vacancy"]["contacts"]);
			$DB->AddValue("faculty_id", implode(";", $_POST[$this->node_id]["add_vacancy"]["faculty_id"]));
			$DB->AddValue("speciality_id", implode(";", $_POST[$this->node_id]["add_vacancy"]["speciality_id"]));
			$DB->AddValue("working", $_POST[$this->node_id]["add_vacancy"]["working"]);


			/*���������� ��������*/
			if (filesize($_FILES["logo"]["tmp_name"])<1000000)
			{
				if (isset($_FILES["logo"]["name"]))
				{
					$filename = $_FILES["logo"]["name"]; //��� ����������������� �����
					$type = strtolower(substr($filename, 1+strrpos($filename,"."))); //���������� �����
					$path = 'files/vac_logo/';//���� ��� ��������
					$fname = "logo_". rand() .  "_" . $filename;
					$fname = str_replace(" ","",$fname);//������� ������ ������� � �����

					$DB->AddValue("logo", $fname);

					if (!@copy($_FILES["logo"]["tmp_name"], $path . $fname))
					{
						$this->output['bad_message'] = "������ �������� �����";
					}
				}
			}
			else
			{
				$this->output["messages"]["bad"][] = "������ ����� ��������� 1��.";
			}


			$DB->AddValue("pub_date", $_POST[$this->node_id]["add_vacancy"]["pub_date"]);
			if ($DB->Insert()) {
				$this->output["messages"]["good"][] = "�������� ���������";
				CF::Redirect($Engine->engine_uri);
			}
		}

		$DB->SetTable("nsau_vacancies_education");
		$res_edu = $DB->Select();

		while ($row = $DB->FetchAssoc($res_edu)) {
			$this->output["educations"][] = $row;
		}

		$DB->SetTable("nsau_vacancies_lodging");
		$res_lodg = $DB->Select();

		while ($row = $DB->FetchAssoc($res_lodg)) {
			$this->output["lodgings"][] = $row;
		}

		$DB->SetTable("nsau_faculties");
		$DB->AddField("id");
		$DB->AddField("name");

		if(isset($this->config['id_faculty']) && count($this->config['id_faculty'])){
			$DB->AddCondFO('id', "NOT IN(".implode(",", $this->config['id_faculty']).")");
		}
		$res_lodg = $DB->Select();

		while ($row = $DB->FetchAssoc($res_lodg)) {
			$this->output["faculties"][] = $row;
		}

		$DB->SetTable("nsau_specialities");
		$DB->AddField("code");
		$DB->AddField("name");

		if(isset($this->config['id_faculty']) && count($this->config['id_faculty'])){
			$DB->AddCondFO('id_faculty', "NOT IN(".implode(",", $this->config['id_faculty']).")");
		}
		$DB->AddGrouping('name');

		$res_lodg = $DB->Select();

		while ($row = $DB->FetchAssoc($res_lodg)) {
			$this->output["specialities"][] = $row;
		}
	}

	function vacancy_view ($id) {
		global $DB, $Engine;

		$DB->SetTable("nsau_vacancies");
		$DB->AddCondFS("id", "=", $id);
		$res = $DB->Select();

		if ($row = $DB->FetchAssoc($res)) {
			$DB->SetTable("nsau_vacancies_lodging");
			$DB->AddCondFS("id", "=", $row["lodging_id"]);
			$res = $DB->Select();
			$row_lodg = $DB->FetchAssoc($res);
			$row["lodging"] = $row_lodg["lodging"];

			$DB->SetTable("nsau_vacancies_education");
			$DB->AddCondFS("id", "=", $row["education_id"]);
			$res = $DB->Select();
			$row_ed = $DB->FetchAssoc($res);
			$row["education"] = $row_ed["education"];

			$fac_ids = explode(";", $row["faculty_id"]);
			$DB->SetTable("nsau_faculties");

			foreach($fac_ids as $f_id)
				$DB->AddAltFS("id", "=", $f_id);
			$res = $DB->Select();
			while($row_fac = $DB->FetchAssoc($res))
				$row["faculty"][] = $row_fac["name"];

			$spec_ids = explode(";", $row["speciality_id"]);
			$DB->SetTable("nsau_specialities");

			foreach($spec_ids as $s_id)
				$DB->AddAltFS("code", "=", $s_id);

			if(isset($this->config['id_faculty']) && count($this->config['id_faculty'])){
				$DB->AddCondFO('id_faculty', "NOT IN(".implode(",", $this->config['id_faculty']).")");
			}
			$DB->AddGrouping('name');

			$res = $DB->Select();
			while($row_spec = $DB->FetchAssoc($res))
				$row["speciality"][] = $row_spec["name"];

			$Engine->AddFootstep($Engine->engine_uri.$Engine->module_uri, $row['vacancy'], '', false);

			$this->output["vacancy"] = $row;

			if ($Engine->OperationAllowed(10, "vacancies.handle", 0, $Auth->usergroup_id))
				$this->output["show_manage"] = 1;
			else
				$this->output["show_manage"] = 0;
		}
		else
		{
			//$this->output["messages"]["bad"][] = "�������� ����� ��������";
		}
	}

	function vacancy_delete ($id) {
		global $DB, $Engine;

		$DB->SetTable("nsau_vacancies");
		$DB->AddCondFS("id", "=", $id);

		$DB->Delete();
		CF::Redirect($_SERVER["HTTP_REFERER"]);
	}

	function processConfigFile($config_file)
	{

		if (!CF::IsNonEmptyStr($config_file)) {
			$this->FatalError(1001);
		}
		elseif (!file_exists(INCLUDES . $config_file)) {
			$this->FatalError(1002, $config_file);
		}
		elseif (!$result = parse_ini_file(INCLUDES . $config_file, true)) {
			$this->FatalError(1003, $config_file);
		}
		$this->config = $result;

	}

	function Output()
	{
		return $this->output;
	}

}
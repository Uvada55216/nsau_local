<?php
class Ensau
// version: 3.251 2 3 1
// date: 2014-08-19 
{
    var $module_id;
    var $node_id;
    var $module_uri;
    var $privileges;
    var $output;

    var $mode;
    var $db_prefix;
    var $maintain_cache;
    var $item_id;
	var $Imager;
	var $sort;
	var $ppc_flag = false;//����������� ������ �� ������ �� �������� ���������� ����������

    /*
    � ������ ������� ����� ������ faculties, exi
	people, people, search_group, groups ���� timetable � ��� � ��� =)
    */
    function Ensau($global_params, $params, $module_id, $node_id = NULL, $module_uri = NULL, $additional = NULL)
    {
        global $DB, $Engine, $Auth;

        $this->module_id = $module_id;
        $this->node_id = $node_id;
        $this->module_uri = $module_uri;
        $this->additional = $additional;
        $this->output = array();
        $this->output["messages"] = array("good" => array(), "bad" => array());
		$this->output["module_id"] = $this->module_id;

        $parts = explode(";", $global_params);
        $this->db_prefix = $parts[0];
        $parts2 = explode(";", $params);

        $mode = explode("/", $module_uri);

		$array = @parse_ini_file(INCLUDES . "ENsau.ini", true);
		$this->settings = @parse_ini_file('ENsau_mail.ini', true);

		$this->images_dir = (isset($array["general_settings"]["images_dir"])) && CF::IsNonEmptyStr($array["general_settings"]["images_dir"]) ? $array["general_settings"]["images_dir"] : false;

		if ($this->images_dir && (isset($array["imager_settings"]["ini_file"]) && $array["imager_settings"]["ini_file"]))
		{
			require_once INCLUDES . "Imager" . CLASS_EXT;
			$this->Imager = new Imager($array["imager_settings"]["ini_file"], COMMON_IMAGES . $this->images_dir . "/", HTTP_COMMON_IMAGES . $this->images_dir . "/");
		}

		else
		{
			$this->Imager = false;
		}

        switch ($this->mode = $parts2[0])
        {

						case "table_generator":
							$this->output["scripts_mode"] = $this->output["mode"] = "table_generator";
							$this->output['data'] = $this->table_generator();
						break;
						
						case "countdown":
							$this->output["scripts_mode"] = $this->output["mode"] = "countdown";
						break;

						case "wmuslider":

							$this->output["scripts_mode"] = $this->output["mode"] = "wmuslider";

						break;

						case "sw_delete_file":
							$DB->SetTable("nsau_files");
							$DB->AddCondFS("id", "=", $_REQUEST["data"]["id"]);
							$DB->AddValue("deleted", 1);
							$DB->Update();
							$Engine->LogAction($this->module_id, "sw", $_REQUEST["data"]["id"], "delete_file");


							$DB->SetTable("nsau_students_works");
							$DB->AddCondFS("feedback_id", "=", $_REQUEST["data"]["id"]);
							$DB->AddValue("feedback_id", NULL);
							$DB->Update();
						break;

						case "sw_delete_work":
							$DB->SetTable("nsau_files");
							$DB->AddCondFS("sw_id", "=", $_REQUEST["data"]["id"]);
							$DB->AddValue("deleted", 1);
							$DB->Update();
							$DB->SetTable("nsau_students_works");
							$DB->AddCondFS("id", "=", $_REQUEST["data"]["id"]);
							$DB->Delete();
							$Engine->LogAction($this->module_id, "sw", $_REQUEST["data"]["id"], "delete_work");
						break;

						case "sw_edit_form":
						header("Content-type: text/html; charset=windows-1251");
							$this->output["scripts_mode"] = $this->output["mode"] = "sw_edit_form";
							$DB->SetTable("nsau_students_works");
							$DB->AddCondFS("id", "=", $_REQUEST["data"]["id"]);
							$row = $DB->FetchAssoc($DB->Select(1));

							$row["sw_teacher_name"] = $row["teacher_name"];

							$this->sw_group_select($row["group_id"]);
							$DB->SetTable("nsau_groups");
							$DB->AddCondFS("id", "=", $row["group_id"]);
							$group = $DB->FetchAssoc($DB->Select(1));
							$row["group_name"] = $group["name"];
							$this->sw_group_search($row["group_name"]);

							$DB->SetTable("nsau_subjects");
							$DB->AddCondFS("id", "=", $row["subject"]);
							$subject = $DB->FetchAssoc($DB->Select(1));
							$row["subject_name"] = $subject["name"];

							// if(!empty($row["teacher_id"])) {
								$DB->SetTable("nsau_people");
								$DB->AddCondFS("id", "=", $row["teacher_id"]);
								$teacher = $DB->FetchAssoc($DB->Select(1));
								$row["teacher_name"] = $teacher["last_name"]." ".$teacher["name"]." ".$teacher["patronymic"];
							// } else {
								// $row["teacher_name"] = $Auth->user_displayed_name;
								// $row["teacher_id"] = $Auth->user_id;
							// }

							$DB->SetTable("nsau_files");
							$DB->AddCondFS("sw_id", "=", $row["id"]);
							$DB->AddCondFS("deleted", "=", 0);
							$fres = $DB->Select();
							while($frow = $DB->FetchAssoc($fres))
							{
								$DB->SetTable("nsau_students_works");
								$DB->AddCondFS("id", "=", $frow['sw_id']);
								$DB->AddCondFS("feedback_id", "=", $frow['id']);
								$tmp1 = $DB->FetchAssoc($DB->Select(1));
								if ($tmp1) 
								{
									$oO = array("report" => 1);
									$frow = $frow + $oO;
								}
								else
								{
									$oO = array("report" => 0);
									$frow = $frow + $oO;
								}


								$this->output["files"][$frow["id"]] = $frow;
							}

							$this->output["sw"] = $row;

						break;
						case "sw_add_form":
							header("Content-type: text/html; charset=windows-1251");
							$this->output["scripts_mode"] = $this->output["mode"] = "sw_add_form";

						break;
						case "sw_check_work_exist":
							$DB->SetTable("nsau_students_works");
							$DB->AddCondFS("student_id", "=", $_REQUEST["data"]["student_id"]);
							$DB->AddCondFS("work_type", "=", $_REQUEST["data"]["work_type"]);
							$DB->AddCondFS("work_name", "=", iconv("utf-8", "cp1251", $_REQUEST["data"]["work_name"]));
							$res = $DB->Select(1);
							$row = $DB->FetchAssoc($res);

							if(empty($row))
								$this->output["json"] = 0;
							else
								$this->output["json"] = 1;

						break;
				case "department_subjects": {
					$this->output["mode"] = "department_subjects";
					$this->output["subjects"] = $this->department_subjects(intval($parts2[1]));
				}
				break;

				case "css_reasset": {
					echo " <link rel='stylesheet' href='/themes/styles/reassets/".$parts2[1]."' type='text/css'/>";
				}
				break;

				case "bootstrap": {

					echo "<link rel='stylesheet' href='/themes/styles/bootstrap/css/bootstrap.css' type='text/css'/>";
					echo "<script type='text/javascript' src='//cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js'></script>";
					echo "<script type='text/javascript' src='/themes/styles/bootstrap/js/bootstrap.min.js'></script>";
				}
				break;

				case "bootstrap4": {

					echo "<link rel='stylesheet' href='/themes/styles/bootstrap-4/dist/css/bootstrap.css' type='text/css'/>";
					echo "<script type='text/javascript' src='//cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js'></script>";
					echo "<script type='text/javascript' src='/themes/styles/bootstrap-4/dist/js/bootstrap.min.js'></script>";
				}
				break;

				case "bootstrap2": {   
					$this->output["mode"] = "bootstrap";  
				}
				break;

				case "bootstrap_clear": {   
					$this->output["mode"] = "bootstrap_clear";  
				}
				break;

				case "bootstrap_less": {   
					$this->output["mode"] = "bootstrap_less";  
				}
				break;

				case "nsau_wifi": 
				{   
					global $DB, $Auth;
					$this->output["mode"] = $this->output["scripts_mode"] = "nsau_wifi";  
				}
				break;

				case 'ajax_nsau_wifi_get_password':
				{
	            	global $DB, $Auth;
					$DB->MySQLhandle('192.168.32.2', 'nsau_wifi', 'wifi1nsau2pass3', 'nsau_wifi');
					$DB->SetTable('accounts');
					$DB->AddCondFS("siteID", "=", $Auth->user_id);
					$user_data = $DB->FetchAssoc($DB->Select(1));
					$this->output["json"] = $user_data;
				}
				break;

				case 'ajax_nsau_wifi_set_password':
				{
	            	global $DB, $Auth;
					$DB->MySQLhandle('192.168.32.2', 'nsau_wifi', 'wifi1nsau2pass3', 'nsau_wifi');
					$DB->SetTable('accounts');
					$DB->AddCondFS("siteID", "=", $Auth->user_id);
					$user_data = $DB->FetchAssoc($DB->Select(1));
					if ($user_data) 
					{
						//�������� ������
						$new_password = dechex(crc32($this->password_generate(8)));
						$DB->SetTable('accounts');
						$DB->AddCondFS("siteID", "=", $Auth->user_id);
						$DB->AddValue('pass', $new_password);
						$DB->AddValue('login', dechex(crc32($Auth->username)));
						if($DB->Update())
						{
							$user_data = array('login' => dechex(crc32($Auth->username)), 'pass' => $new_password);
							$this->output["json"] = $user_data;
						}
						else
						{
							$this->output["json"] = "������ �� ��������";
						}
					}
					else
					{
						//������� ������
						$new_password = $this->password_generate(8);
						$DB->MySQLhandle('192.168.32.2', 'nsau_wifi', 'wifi1nsau2pass3', 'nsau_wifi');
						$DB->SetTable('accounts');
						$DB->AddValue('login', dechex(crc32($Auth->username)));
						$DB->AddValue('pass', $new_password);
						$DB->AddValue('siteID', $Auth->user_id);
						if ($DB->Insert())
						{
							$user_data = array('login' => dechex(crc32($Auth->username)), 'pass' => $new_password);
							$this->output["json"] = $user_data;
						}
						else
						{
							$this->output["json"] = "������ �� ������";
						}
					}

				}
				break;

				case "students_upload":{
					/*�������� ������������*/
					if(isset($_POST['upload_progress']))
						{
							$filename = $_FILES['progress_file']['name']; 						//��� ����������������� �����
							$type = strtolower(substr($filename, 1+strrpos($filename,"."))); 	//���������� �����

							if ($type=="csv")
							{
								$path = 'files/st_progress/'; 	//���� ��� ��������
								$full_name = explode(".", $filename);
								$fname = $full_name[0]."(".date("d")."_".date("m")."_".date("y")."_".date("G").":".date("i").").".$type; //���+���� �������� �����
								$fname = str_replace(" ","",$fname); 			//������� ������ ������� � �����
								if (!@copy($_FILES['progress_file']['tmp_name'], $path . $fname))
								{
									$this->output['error'] = "������ �������� ����� <br>";
								}
								else
								{
									$handle = @fopen($path . $fname, "r");
									if ($handle) {
										while (($buffer = fgets($handle)) !== false) {
											$parts = explode(";", $buffer);
											$full_arr[] = $parts;
											$code = str_replace(" ", "", trim($parts[1]));
											$arr[$code] = $code;
										}
									}
									$tt = trim($full_arr[2][8]);
									$is_tomsk  =  ($tt=="1") ? true : false;
									
									$DB->SetTable("nsau_faculties");
									$DB->AddCondFS("name", "=", trim($full_arr[2][0]));
									$f = $DB->FetchAssoc($DB->Select(1));
									
									foreach($arr as $code) {
										$DB->SetTable("nsau_students_progress");
										$DB->AddCondFS("code", "=", $code);
										if(!$is_tomsk) {
											$DB->AddCondFS("faculty_id", "=", $f["id"]);
											$DB->AddCondFS("is_tomsk", "!=", 1);		
										}
										else {
											$DB->AddCondFS("is_tomsk", "=", 1);		
										}
										$DB->Delete();
										$del++;
									}
									
									

									
									$DB->SetTable("nsau_faculties");
									$res_f = $DB->Select();
									while($row_f = $DB->FetchAssoc($res_f)) {
										$fac_arr[$row_f["name"]] = $row_f["id"];
									}
									
									$handle = @fopen($path . $fname, "r");
									if ($handle) {
										$skip = "";
										while (($buffer = fgets($handle)) !== false) {
												$parts = explode(";", $buffer);
												
												$code = 	str_replace(" ", "", trim($parts[1]));	
												$semester = trim($parts[3]);
												$subj = 	trim($parts[4]);
												$hours = 	trim($parts[5]);
												$type = 	trim($parts[6]);
												$val = 		trim($parts[7]);
												$faculty  = trim($parts[0]);
												$faculty_id = $fac_arr[$faculty];
												$tom = trim($parts[8]);
												$tomsk = ($tom=="1") ? 1 : 0;
												
												
												if($skip!=$code) { //�� ��������� ������ ���� � ������� �������� ���������� ����� ��
													$skip=$code;
													$DB->SetTable("nsau_people", "p");
													$DB->AddTable("nsau_groups", "g");
													$DB->AddCondFS("p.library_card", "=", $code);
													$DB->AddCondFF("p.id_group", "=", "g.id");
													$DB->AddCondFS("g.id_faculty", "=", $faculty_id);
													$DB->AddField("p.id", "id");
													$p = $DB->FetchAssoc($DB->Select(1));
												}
												

												/*�������� �� ���������*/
												if($this->check_encode($buffer)) {
													$asql = "INSERT INTO `nsau`.`nsau_students_progress` (`id`, `people_id`, `code`, `semester`, `subject`, `hours`, `type`, `value`, `faculty_id`, `is_tomsk`)
																		VALUES (NULL, '".$p["id"]."', '".$code."', '".$semester."', '".$subj."', '".$hours."', '".$type."', '".$val."', '".$faculty_id."', '".$tomsk."');";
													$DB->Exec($asql);
													if(!empty($p["id"])) {
														$ins++;
													} else {
														$no_people[$code] = trim($parts[2]);
													}
												}
												else {
													$encode_error=1;
													break 1;
												}
											}
											$this->output["no_people"] = $no_people;
										}
									if (!feof($handle) || $encode_error==1)
									{
										if ($encode_error==1)
										{
											$this->output['error'] = "�������� ��������� ����� <br>";
										}
										else
										{
											$this->output['error'] = "������ �������� �����<br>";
										}
									}
									else
									{
																		$this->output['ins'] = "���������/�������� ������� ".$ins;
									// $this->output['del'] = "������� ������� ".$del;
									}
									fclose($handle);
									$Engine->LogAction(5, "studets progress", "zagruzka-studentov", "upload");//������ � ����*/
								}
							}
							else
							{
								$this->output['error'] = "�������� ������ ����� <br>";
							}
						}
							$this->output["mode"] = $this->output["scripts_mode"] = $this->output["display_variant"] = "students_upload";
							$error = false;
							$fname = explode(".", $_FILES['students']['name']);
							if(!empty($_FILES['students']['name'])) {
								if ($fname[count($fname)-1] != "csv") {
									$this->output["messages"]["bad"][] = 401;
								} else {
									$f = fopen($_FILES['students']['tmp_name'], "r");
									$content = fread($f, filesize($_FILES['students']['tmp_name']));
									/*����� ��������� �� win-1251*/
									//include "charset_convert.php";
									//$content = charset_x_win($content);
									if(!$this->check_encode($content)) {
										$error = true;
										$this->output["messages"]["bad"][0] = 407;
										break;										
									}
									$parts = explode("\r\n", $content);
									// array_shift($parts);
									$tshi = false;
									$asp = false;
									foreach($parts as $part) {
															
										$p = explode(";", $part);

										$student = array("group" => trim($p[0]), "code" => str_replace(" ", "", trim($p[1])), "fio" => trim(mb_convert_case($p[2], MB_CASE_TITLE, "cp1251")));
										if((count($p) != count($student)) && !empty($student["group"])) {
											$this->output["messages"]["bad"][1] = 402;
											$error = true;
										}
										if(!empty($student["group"])) {
											if((strpos($student["group"], "5") === 0) || (strpos($student["group"], "9") === 0)) {
												$tshi = true;
											}
											if(strpos($student["group"], "�9") === 0) {
												$asp = true;
											}
											if($asp) {
												if(strpos($student["group"], "�9") !== 0) {												
													$error = true;
													$this->output["messages"]["bad"][1] = 404;
												}
											}
											if($tshi) {
												if((strpos($student["group"], "5") !== 0) && (strpos($student["group"], "9") !== 0)) {
													$error = true;
													$this->output["messages"]["bad"][1] = 404;
												}
											}
											$DB->SetTable("nsau_groups");
											$DB->AddCondFS("name", "=", $student["group"]);
											$gr = $DB->FetchAssoc($DB->Select(1));
											$student["group_id"] = $gr["id"];
											if(empty($faculty_id)) {
												$faculty_id = $gr["id_faculty"];
											} elseif(($faculty_id != $gr["id_faculty"]) && !empty($student["group_id"]) && (!$tshi)) {
												$error = true;
												$this->output["messages"]["bad"][1] = 404;
												// echo $part."<br>";
											}
											if(empty($student["group_id"])) {
												$error = true;
												$this->output["messages"]["bad"][2] = array(403, array(1 => $student["group"]));
												// echo $part."<br>";
											}
											$student["passwd"] = str_replace(" ", "", str_replace("/", "", str_replace("-", "", CF::translit($student["code"]))));
											$user = explode(" ", $student["fio"]);
											$student["lastname"] = trim($user[0]);
											$student["name"] = trim($user[1]);
											$student["patronymic"] = trim($user[2]);
											$student["login"] = strtolower(CF::translit(trim($student["lastname"]))).mt_rand(1000,9999);
											$student["md5_passwd"] = md5($student["passwd"]);
											$buffer[$gr["form_education"]][$student["code"]] = $student;
										}
									}
									foreach($buffer as $form => $stud) {
										// if(count($buffer[$form])<10) {
											// $error = true;
											// $this->output["messages"]["bad"][3] = 405;
										// }
									}

									if(!$error) {
										$DB->SetTable("nsau_faculties");
										$DB->AddCondFS("id", "=", $faculty_id);
										$faculty = $DB->FetchAssoc($DB->Select(1));

										$filename_1 = '/files/students/'.CF::translit($faculty["name"])."_".($tshi ? "tshi" : "").date('j-m-y-h-i-s').'_original.csv';
										$dfv = fopen($_SERVER['DOCUMENT_ROOT'].$filename_1, "w+");
										fwrite($dfv, $content);
										fclose($dfv);
										$Engine->LogAction($this->module_id, "students", CF::translit($faculty["name"])."_".date('j-m-y-h-i-s').'_original.csv', "upload_file");


										$DB->SetTable("nsau_people", "p");
										$DB->AddTable("nsau_groups", "g");
										$DB->AddCondFF("p.id_group", "=", "g.id");
										if(!$asp && !$tshi) {
											$DB->AddCondFS("g.id_faculty", "=", $faculty["id"]);
										}
										if($asp) {
											$DB->AddCondFS("g.name", "REGEXP", "^�9"); //������ ������������ �9 ���������
										} else {
											$DB->AddCondFS("g.name", "NOT REGEXP", "^�9"); //�� ������ ������������ �9 ���������
										}
										if($tshi) {
											$DB->AddAltFS("g.name", "REGEXP", "^5"); //������ ������������ � 5 ����
											$DB->AddAltFS("g.name", "REGEXP", "^9"); //������ ������������ � 9 ����
											$DB->AppendAlts();
										} else {
											$DB->AddCondFS("g.name", "NOT REGEXP", "^5"); //�� ������ ������������ � 5 ����
											$DB->AddCondFS("g.name", "NOT REGEXP", "^9"); //�� ������ ������������ � 9 ����
										}
										$DB->AddCondFS("p.people_cat", "=", 1);
										$DB->AddField("p.id", "people_id");
										$DB->AddField("g.form_education", "form_education");
										$DB->AddField("p.last_name", "last_name");
										$DB->AddField("p.name", "name");
										$DB->AddField("p.patronymic", "patronymic");
										$DB->AddField("p.library_card", "library_card");
										$DB->AddField("g.id", "group_id");
										$DB->AddField("g.name", "group_name");
										
										// die($DB->SelectQuery());
										$res = $DB->Select();
										while($row = $DB->FetchAssoc($res))
											if(!empty($row["library_card"])) {
												$students[$row["form_education"]][$row["library_card"]] = $row;
											} else {
												$this->deletePeople($row["people_id"]);
											}

										for($form=1;$form<4;$form++) {
											if(count($buffer[$form])>0) {
												//+new
												$newstudents = "";
												foreach($buffer[$form] as $code => $people) {
													if(!$students[$form][$code]) {
														$this->addStudent($people["login"], $people["md5_passwd"], $people["fio"], $people["lastname"], $people["name"], $people["patronymic"], $people["group_id"], $people["code"]);
													$newstudents .= $people["group"].";".$people["fio"].";".$people["login"].";".$people["passwd"].";".$people["code"]."\r\n";
													$new[$form][] = array("group" => $people["group"], "fio" => $people["fio"], "login" => $people["login"], "passwd" => $people["passwd"]);
													}
												}
												if(!empty($newstudents)) {
													$filename_1 = '/files/students/'.$form."_".CF::translit($faculty["name"])."_".($tshi ? "tshi" : "").date('j-m-y-h-i-s').'.csv';
													$date = date('d.m.Y')."\r\n";
													$newstudents = "��������� ".$date.$newstudents;
													$df = fopen($_SERVER['DOCUMENT_ROOT'].$filename_1, "w+");
													fwrite($df, $newstudents);
													fclose($df);
													$this->output["download_files"][$form] = $filename_1;
												}
												//-old
												$deleted_students = "";
												foreach($students[$form] as $code => $people) {
													if(!$buffer[$form][$code]) {
														$this->deletePeople($people["people_id"]);
														$deleted_students .= $people["people_id"].";";
														$deleted[$form][] = array("group" => $people["group_name"], "fio" => $people["last_name"]." ".$people["name"]." ".$people["patronymic"], "code" => $people["library_card"]);
													}
												}
												if(!empty($deleted_students)) 
												{
													$filename_deleted = '/files/students/'."Deleted_".$form."_".CF::translit($faculty["name"])."_".($tshi ? "tshi" : "").date('j-m-y-h-i-s').'.csv';
													$date = date('d.m.Y')."\r\n";
													$del = fopen($_SERVER['DOCUMENT_ROOT'].$filename_deleted, "w+");
													fwrite($del, $deleted_students);
													fclose($del);
												}
												//edit
												foreach($students[$form] as $code => $people) {
													if((($buffer[$form][$code]["group_id"]!=$people["group_id"])
														|| ($buffer[$form][$code]["name"]!=$people["name"])
														|| ($buffer[$form][$code]["lastname"]!=$people["last_name"])
														|| ($buffer[$form][$code]["patronymic"]!=$people["patronymic"])
														 )
														&& !empty($buffer[$form][$code]["group_id"])) {
															$this->editStudent($people["people_id"], $code, $buffer[$form][$code]["group_id"], $buffer[$form][$code]["lastname"], $buffer[$form][$code]["name"], $buffer[$form][$code]["patronymic"]);
															$updated[$form][$code]["before"] = array("group" => $people["group_name"], "fio" => $people["last_name"]." ".$people["name"]." ".$people["patronymic"], "code" => $people["library_card"]);
															$updated[$form][$code]["after"] = array("group" => $buffer[$form][$code]["group"], "fio" => $buffer[$form][$code]["fio"], "code" => $buffer[$form][$code]["code"]);
														}

												}
											}
										}

										$this->output["messages"]["good"][] = 406;
										$this->output["new_students"] = $new;
										$this->output["deleted_students"] = $deleted;
										$this->output["updated_students"] = $updated;
									}

								}
							}
						}
						break;
						case "student_work":
						if ($Engine->OperationAllowed($this->module_id, "students.works.handle", -1, $Auth->usergroup_id)) 
						{
							$this->output["scripts_mode"] = $this->output["mode"] = "student_work";
							if(!empty($_POST))
							{

								if(!empty($_POST["sw_group_select"]) && !empty($_POST["sw_department"]) && (!empty($_POST["sw_student_fio"]) || !empty($_POST["sw_student_name"])) && !empty($_POST["sw_name"]) && (!empty($_POST["sw_teacher"]) || !empty($_POST["sw_teacher_name"]))) {

									$DB->SetTable("auth_users");
									$DB->AddCondFS("id", "=", $_POST["sw_student_fio"]);
									$student_user = $DB->FetchAssoc($DB->Select(1));

									$DB->SetTable("nsau_students_works");
									$DB->AddValue("year", $_POST["sw_year_end"]);
									$DB->AddValue("group_id", $_POST["sw_group_select"]);
									$DB->AddValue("subject", $_POST["sw_subj"]);
									$DB->AddValue("work_type", $_POST["sw_type"]);
									$DB->AddValue("work_name", $_POST["sw_name"]);

									if(!empty($_POST["sw_teacher_name"])) {
										$DB->AddValue("teacher_name", $_POST["sw_teacher_name"]);
										$DB->AddValue("teacher_id", $_POST["sw_teacher"]);
									}
									else {
										$DB->AddValue("teacher_id", $_POST["sw_teacher"]);
										$DB->AddValue("teacher_name", NULL);
									}

									if(!empty($_POST["sw_student_name"])) {
										$DB->AddValue("student_name", $_POST["sw_student_name"]);
										$DB->AddValue("student_id", NULL);
									}
									else {
										$DB->AddValue("student_id", $_POST["sw_student_fio"]);
										$DB->AddValue("student_name", $student_user["displayed_name"]);
									}


									$DB->AddValue("department_id", $_POST["sw_department"]);
									$DB->AddValue("user_id", $Auth->user_id);


									if(!empty($_POST["edit"]))
									{
										// if(!empty($_POST["sw_teacher_name"]))
										// 	$DB->AddValue("teacher_id", NULL);
										$DB->AddCondFS("id", "=", $_POST["edit"]);
										$DB->Update();

										$DB->SetTable("nsau_files");
										if (!empty($student_user)) 
										{
											$DB->AddValue("author", $student_user["displayed_name"]);
										}
										else
										{
											$DB->AddValue("author", $_POST["sw_student_fio"]);
										}
										$DB->AddCondFS("sw_id", "=", $_POST["edit"]);
										$DB->Update();

										$id = $_POST["edit"];
										$Engine->LogAction($this->module_id, "sw", $id, "edit_work");

										foreach($_POST["files_for_del"] as $dfid) {
											$DB->SetTable("nsau_files");
											$DB->AddCondFS("id", "=", $dfid);
											$DB->AddValue("deleted", 1);
											$DB->AddValue("delete_time", "NOW()", "X");
											$DB->Update();
										}

									} else {
										$DB->Insert();
										$id = $DB->LastInsertID();
										$Engine->LogAction($this->module_id, "sw", $id, "add_work");

									}

									

									if(!empty($_FILES)) 
									{
										//35.03.06_�_���_3401_��������_160616.doc
										
										$work_types = array(32=>"���", 33=>"��", 34=>"��", 35=>"��", 36=>"�");

										$DB->SetTable("nsau_groups");
										$DB->AddCondFS("id", "=", $_POST["sw_group_select"]);
										$group = $DB->FetchAssoc($DB->Select(1));


										if(isset($_POST["sw_student_fio"])) {
											$DB->SetTable("nsau_people");
											$DB->AddCondFS("user_id", "=", $_POST["sw_student_fio"]);
											$people = $DB->FetchAssoc($DB->Select(1));
										}
										if(!isset($people)) {
											$r = explode(" ", $_POST["sw_student_name"]);
											$people["last_name"] = $r[0];
											$people["name"] = $r[1];
											$people["patronymic"] = $r[2];
										}

										$date = date("dmy");

										$form = ($group["form_education"]==1 ? "�" : ($group["form_education"]==2 ? "�" : "��"));

										//$filename = $group["specialities_code"]."_".$form."_".$work_types[$_POST["sw_type"]]."_".$group["name"]."_".$people["last_name"].$people["name"][0].$people["patronymic"][0]."_".$date;

										$filename = $group["specialities_code"]."_".$form."_".$work_types[$_POST["sw_type"]]."_".$group["name"]."_".$people["last_name"].$people["name"][0].$people["patronymic"][0];


										/*�������� ������ ��������*/
										if (isset($_FILES["main_file"]) && !empty($_FILES["main_file"]))
										{
											if($_FILES["main_file"]["type"]=="application/pdf")
											{
												/*����� � �������� ���������� ������*/
												$DB->SetTable("nsau_files");
												$DB->AddCondFS("sw_id", "=", $id);
												$DB->Delete();

												$parts = array_reverse(explode(".", $_FILES["main_file"]["name"]));

												$DB->SetTable("nsau_files");
												$DB->AddValue("user_id", $Auth->user_id);
												$DB->AddValue("name", $filename);
												$DB->AddValue("author", $student_user["displayed_name"]);
												$DB->AddValue("filename", $parts[0]);
												$DB->AddValue("descr", $filename);
												$DB->AddValue("year", $_POST["sw_year_end"]);
												$DB->AddValue("sw_id", $id);
												$DB->AddValue("create_time", "NOW()", "X");
												$DB->Insert();
												$fid = $DB->LastInsertID();
												$new_name = $fid.'.'.$parts[0];
												$path = $_SERVER['DOCUMENT_ROOT'].'files/'.$new_name;

												if (!move_uploaded_file($_FILES['main_file']['tmp_name'], $path)) 
												{
													$DB->SetTable("nsau_files");
													$DB->AddCondFS("id", "=", $fid);
													$DB->Delete();
													$this->output["messages"]["bad"][] = "�� ������� �������� ����";
												}
												// else
											}
											else
											{
												//���������� � �������� �������
											}
										}

										/*�������� ������ ��������*/
										if (isset($_FILES["feedback_file"]) && !empty($_FILES["feedback_file"]))
										{
											if ($_FILES["feedback_file"]["type"]=="application/pdf") 
											{
												$parts = array_reverse(explode(".", $_FILES["feedback_file"]["name"]));

												$DB->SetTable("nsau_files");
												$DB->AddValue("user_id", $Auth->user_id);
												$DB->AddValue("name", $filename."_���");
												$DB->AddValue("author", $student_user["displayed_name"]);
												$DB->AddValue("filename", $parts[0]);
												$DB->AddValue("descr", $filename."_���");
												$DB->AddValue("year", $_POST["sw_year_end"]);
												$DB->AddValue("sw_id", $id);
												$DB->AddValue("create_time", "NOW()", "X");
												$DB->Insert();
												$report = $DB->LastInsertID();

												$fid = $DB->LastInsertID();
												$new_name = $fid.'.'.$parts[0];
												$path = $_SERVER['DOCUMENT_ROOT'].'files/'.$new_name;

												if (!move_uploaded_file($_FILES['feedback_file']['tmp_name'], $path)) 
												{
													$DB->SetTable("nsau_files");
													$DB->AddCondFS("id", "=", $report);
													$DB->Delete();
													$this->output["messages"]["bad"][] = "�� ������� �������� ����";
												}
												else
												{
													/*����� � �������� ����������� ������*/
													$DB->SetTable("nsau_students_works");
													$DB->AddCondFS("id", "=", $id);
													$check1 = $DB->FetchAssoc($DB->Select(1));
													if ($check1["feedback_id"]) 
													{
														$DB->SetTable("nsau_files");
														$DB->AddCondFS("id", "=", $check1["feedback_id"]);
														$DB->Delete();
													}
													/*���������� ������ � ������*/
													$DB->SetTable("nsau_students_works");
													$DB->AddCondFS("id", "=", $id);
													$DB->AddValue("feedback_id", $report);
													$DB->Update();
												}
											}
											else
											{
												//���������� � �������� �������
											}
										}

										// foreach($_FILES["files"]["name"] as $f_n => $fname) 
										// {

										// 	$parts = array_reverse(explode(".", $fname));
										// 	if(in_array($parts[0], array("jpg", "jpeg", "pdf", "doc", "docx"))) 
										// 	{
										// 		/*
										// 		�������� ������.-------
										// 		��
										// 		��
										// 		��
										// 		���
										// 		����
										// 		�����
										// 		������
										// 		*/



										// 		if (isset($main_file_ok) && $main_file_ok == 1 && isset($id)) 
										// 		{
										// 			$DB->SetTable("nsau_files");
										// 			$DB->AddValue("user_id", $Auth->user_id);
										// 			$DB->AddValue("name", $filename);
										// 			$DB->AddValue("author", $student_user["displayed_name"]);
										// 			$DB->AddValue("filename", $parts[0]);
										// 			$DB->AddValue("descr", $filename);
										// 			$DB->AddValue("year", $_POST["sw_year_end"]);
										// 			$DB->AddValue("sw_id", $id);
										// 			$DB->AddValue("create_time", "NOW()", "X");
										// 			$DB->Insert();
										// 			$report = $DB->LastInsertID();

										// 			/*---------------------------------������� ��� ������---------------------------------------------------------------*/
										// 			// if (!move_uploaded_file($_FILES['files']['tmp_name'][$f_n], $path)) 
										// 			// {
										// 			// 	$DB->SetTable("nsau_files");
										// 			// 	$DB->AddCondFS("id", "=", $fid);
										// 			// 	$DB->Delete();
										// 			// 	$this->output["messages"]["bad"][] = "�� ������� �������� ����";
										// 			// }
										// 			// else
										// 			// {
										// 			/*���������� ������ � ������*/
										// 			$DB->SetTable("nsau_students_works");
										// 			$DB->AddCondFS("id", "=", $id);
										// 			$DB->AddValue("feedback_id", $report);
										// 			$DB->Update();
										// 			// }

										// 		}
										// 		else
										// 		{
										// 			/*�������� ������*/
										// 			$DB->SetTable("nsau_files");
										// 			$DB->AddValue("user_id", $Auth->user_id);
										// 			$DB->AddValue("name", $filename);
										// 			$DB->AddValue("author", $student_user["displayed_name"]);
										// 			$DB->AddValue("filename", $parts[0]);
										// 			$DB->AddValue("descr", $filename);
										// 			$DB->AddValue("year", $_POST["sw_year_end"]);
										// 			$DB->AddValue("sw_id", $id);
										// 			$DB->AddValue("create_time", "NOW()", "X");
										// 			$DB->Insert();
										// 			$fid = $DB->LastInsertID();
										// 			$new_name = $fid.'.'.$parts[0];
										// 			$path = $_SERVER['DOCUMENT_ROOT'].'files/'.$new_name;

										// 			/*---------------------------------������� ��� ������---------------------------------------------------------------*/
										// 			// if (!move_uploaded_file($_FILES['files']['tmp_name'][$f_n], $path)) 
										// 			// {
										// 			// 	$DB->SetTable("nsau_files");
										// 			// 	$DB->AddCondFS("id", "=", $fid);
										// 			// 	$DB->Delete();
										// 			// 	$this->output["messages"]["bad"][] = "�� ������� �������� ����";
										// 			// }

										// 			$main_file_ok = 1;
										// 		}
										// 	}
										// }
									}
								} else


								if(empty($_FILES["main_file"]['name'])) {
									$this->output["messages"]["good"][] = 433;
								}
								if(empty($_POST["sw_group_select"])) {
									$this->output["messages"]["good"][] = 429;
								}
								if(empty($_POST["sw_department"])) {
									$this->output["messages"]["good"][] = 430;
								}
								if((empty($_POST["sw_student_fio"]) && empty($_POST["sw_student_name"]))) {
									$this->output["messages"]["good"][] = 431;
								}								
								if((empty($_POST["sw_teacher"]) && empty($_POST["sw_teacher_name"]))) {
									$this->output["messages"]["good"][] = 432;
								}		

							}

							$DB->SetTable("nsau_students_works");

							$DB->AddAltFS("user_id", "LIKE", $Auth->user_id); 
							$DB->AddAltFS("student_id", "LIKE", $Auth->user_id);
							$DB->AddAltFS("teacher_id", "LIKE", $Auth->people_id);
							$DB->AppendAlts();

							$res = $DB->Select();
							while($row = $DB->FetchAssoc($res)) {
								$DB->SetTable("nsau_subjects");
								$DB->AddCondFS("id", "=", $row["subject"]);
								$subj = $DB->FetchAssoc($DB->Select(1));
								$row["subject"] = $subj["name"];

								$DB->SetTable("nsau_groups");
								$DB->AddCondFS("id", "=", $row["group_id"]);
								$group = $DB->FetchAssoc($DB->Select(1));
								$row["group_name"] = $group["name"];

								$DB->SetTable("nsau_faculties");
								$DB->AddCondFS("id", "=", $group["id_faculty"]);
								$fac = $DB->FetchAssoc($DB->Select(1));
								$row["faculty"] = $fac["name"];

								$DB->SetTable("nsau_specialities");
								$DB->AddCondFS("code", "=", $group["specialities_code"]);
								$spec = $DB->FetchAssoc($DB->Select(1));
								$row["speciality"] = $group["specialities_code"] . "-" . $spec["name"];

								$row["form"] = ($group["form_education"]==1 ? "�����" : ($group["form_education"]==2 ? "�������" : "����-�������"));

								$DB->SetTable("nsau_departments");
								$DB->AddCondFS("id", "=", $row["department_id"]);
								$dep = $DB->FetchAssoc($DB->Select(1));
								$row["department"] = $dep["name"];


								$DB->SetTable("nsau_people");
								$DB->AddCondFS("user_id", "=", $row["student_id"]);
								$people = $DB->FetchAssoc($DB->Select());
								if(!empty($people)) {
									$row["student"] = $people["last_name"]." ".$people["name"]." ".$people["patronymic"];
								}
								else {
									$row["student"] = $row["student_name"];
								}
								$row["student_people_id"] = $people["id"];


								$DB->SetTable("nsau_files");
								$DB->AddCondFS("sw_id", "=", $row["id"]);
								$DB->AddCondFS("deleted", "=", 0);
								$fres = $DB->Select();
								while($files = $DB->FetchAssoc($fres))
									$row["files"][$files["id"]] = $files["name"];






								$this->output["sw"][$row["work_type"]][$row["id"]] = $row;



							}

							$pnfv = array("��������� ���������������� ������"=>"��������� ������������������ ������", "��������� ������"=>"��������� ������", "�������� ������"=>"�������� ������", "�������� ������"=>"�������� �������", "�������"=>"��������");

							$DB->SetTable("nsau_file_view");
							$nfvr = $DB->Select();
							while($nfv = $DB->FetchAssoc($nfvr))
								$this->output["nfv"][$nfv["id"]] = $nfv["view_name"];


						}
						break;
						case "ajax_move_subject":
								$this->output["json"]=$this->ajax_move_subjects($_REQUEST["data"]["from_id"],$_REQUEST["data"]["to_id"],$_REQUEST["data"]["mode"],$_REQUEST["data"]["current_id"]);
						break;
						case "ajax_change_people_cat":
								$this->output["json"]=$this->ajax_change_people_cat($_REQUEST["data"]["people_id"],$_REQUEST["data"]["cat"]);
						break;
						case "speciality":
                $this->output["scripts_mode"] = $this->output["mode"] = "speciality";
                $this->speciality($this->module_uri);
                break;
            case "specialities":
                $this->output["scripts_mode"] = $this->output["mode"] = "specialities";
                $this->specialities($parts2[1]);
                break;
            case "specialities_all":
                $this->output["scripts_mode"] = $this->output["mode"] = "specialities_all";
                $this->specialities_all();
                break;

            case 'ajax_get_disc':
            {
            	
            	global $DB;
            	$DB->SetTable("nsau_subjects");
            	$DB->AddCondFS("department_id", "=", $_REQUEST["data"]["id"]);
            	$res = $DB->Select();
            	while($row = $DB->FetchAssoc($res)) 
				{
					$result[$row['id']] = $row['name'];
				}
				$this->output["json"] = $result;

            }
            break;

            case "exam_ege":
            	{
            	global $DB;

            	$DB->SetTable("nsau_specialities");
            	$DB->AddCondFS("old", "=", "0");
				$res = $DB->Select();
				while($row = $DB->FetchAssoc($res)) 
				{
					$spec[$row['id']] = $row['code']." - ".$row['name'];
				}
				$this->output["spec"] = $spec;

				$DB->SetTable("nsau_abit_edu_level");
				$res = $DB->Select();
		        while($row = $DB->FetchAssoc($res))
		        {
			        $this->output["abitEduLevel"][$row['id']] = $row;
		        }

				$DB->SetTable("nsau_exams");
				$DB->AddOrder("n_pp");
				$res = $DB->Select();
				while($row = $DB->FetchAssoc($res)) 
				{
					$exams[$row['id']] = $row;
				}
				$this->output["exams"] = $exams;


            	if (isset($_POST['add_exam'])) 
            	{
            		if (!empty($_POST['nomer']) && !empty($_POST['predmet']) && !empty($_POST['ball_vyz']) && !empty($_POST['ball_mins']) && !empty($_POST['prikaz'])) 
            		{
            			$DB->SetTable("nsau_exams");
	            		$DB->AddValue("n_pp", $_POST['nomer']);
	            		$DB->AddValue("name", $_POST['predmet']);
	            		$DB->AddValue("spec", $_POST['spec']);
	            		$DB->AddValue("min_vyz", $_POST['ball_vyz']);
	            		$DB->AddValue("min_mins", $_POST['ball_mins']);
	            		$DB->AddValue("prikaz", $_POST['prikaz']);
	            		$DB->Insert();

//	            		$DB->SetTable("nsau_exams");
//	            		$DB->AddValue("name", $_POST['predmet']);
//	            		$DB->AddValue("min_points", $_POST['ball_vyz']);
//	            		$DB->Insert();
	            	}
	            	else
	            	{
	            		$this->output["exam_error"] = "����������� �� ��� ����";
	            	}
	            }

                	$this->output["scripts_mode"] = $this->output["mode"] = "exam_ege";
                }
                break;

            case 'exam_ege_add':
            {
                global $DB, $Engine;
                if ($Engine->OperationAllowed(5, "exam_ege.edit", -1, $Auth->usergroup_id))
                {
            		if (!empty($_REQUEST['data']['nomer']) && !empty($_REQUEST['data']['predmet']) && !empty($_REQUEST['data']['ball_vyz']) && !empty($_REQUEST['data']['ball_mins']) && !empty($_REQUEST['data']['prikaz'])) 
            		{
            			$DB->SetTable("nsau_exams");
	            		$DB->AddValue("n_pp", 		iconv('utf-8','windows-1251',$_REQUEST['data']['nomer'])   		);
	            		$DB->AddValue("name", 		iconv('utf-8','windows-1251',$_REQUEST['data']['predmet'])    	);
	            		$DB->AddValue("spec", 		iconv('utf-8','windows-1251',$_REQUEST['data']['spec'])    		);
	            		$DB->AddValue("min_vyz", 	iconv('utf-8','windows-1251',$_REQUEST['data']['ball_vyz'])     );
	            		$DB->AddValue("max_vyz", 	iconv('utf-8','windows-1251',$_REQUEST['data']['ball_max_vyz'])     );
	            		$DB->AddValue("min_mins", 	iconv('utf-8','windows-1251',$_REQUEST['data']['ball_mins'])    );
	            		$DB->AddValue("prikaz", 	iconv('utf-8','windows-1251',$_REQUEST['data']['prikaz'])    	);
			            $DB->AddValue("edu_level_id", 	(int)$_REQUEST['data']['eduLevel']);
//			            $DB->Insert();

//	            		$DB->SetTable("nsau_exams");
//	            		$DB->AddValue("name", 		iconv('utf-8','windows-1251',$_REQUEST['data']['predmet'])		);
//	            		$DB->AddValue("min_points", iconv('utf-8','windows-1251',$_REQUEST['data']['ball_vyz'])		);

	            		if($DB->Insert())
		                {
		                	$this->output["json"] = $DB->LastInsertId();
		                }
		                else
		                {
		                	$this->output["json"] = "error2";
		                }
	            	}
	            	else
	            	{
	            		$this->output["exam_error"] = "error4";
	            	}
            	}
            	else
            	{
            		$this->output["json"] = "error3";
            	}
            }
            break;

            case 'exam_ege_delete':
            {
                global $DB, $Engine;
                if ($Engine->OperationAllowed(5, "exam_ege.edit", -1, $Auth->usergroup_id))
                {
	                $DB->SetTable("nsau_exams");
	                $DB->AddCondFS("id", "=", $_REQUEST['data']["id"]);
//	               	$result = $DB->FetchAssoc($DB->Select(1));

//	               	$DB->SetTable("nsau_exams");
//	                $DB->AddCondFS("id", "=", $_REQUEST['data']["id"]);
	                if ($DB->Delete()) 
	                {
	                	$this->output["json"] = 1;
	                }
	                else
	                {
	                	$this->output["json"] = 2;
	                }

//	                $DB->SetTable("nsau_exams");
//	                $DB->AddCondFS("name", "=", $result['name']);
//	                if ($DB->Delete())
//	                {
//	                	$this->output["json"] = 1;
//	                }
//	                else
//	                {
//	                	$this->output["json"] = 2;
//	                }
            	}
            	else
            	{
            		$this->output["json"] = 3;
            	}
            }
            break;

            case 'exam_get':
            {
                global $DB;
	            $DB->SetTable("nsau_exams");
	            $DB->AddCondFS("id", "=", $_REQUEST['data']["id"]);
	            $result = $DB->FetchAssoc($DB->Select(1));
            	$this->output["json"] = $result;
            }
            break;

            case 'exam_ege_edit':
            {
                global $DB, $Engine;
                if ($Engine->OperationAllowed(5, "exam_ege.edit", -1, $Auth->usergroup_id))
                {
            		if (!empty($_REQUEST['data']['id']) && !empty($_REQUEST['data']['nomer']) && !empty($_REQUEST['data']['predmet']) && !empty($_REQUEST['data']['ball_vyz']) && !empty($_REQUEST['data']['ball_mins']) && !empty($_REQUEST['data']['prikaz']) && !empty($_REQUEST['data']['eduLevel']))
            		{

            			$DB->SetTable("nsau_exams");
            			$DB->AddCondFS("id", "=", $_REQUEST['data']["id"]);
//	               		$result = $DB->FetchAssoc($DB->Select(1));

	               		$DB->SetTable("nsau_exams");
            			$DB->AddCondFS("id", "=", $_REQUEST['data']["id"]);
	            		$DB->AddValue("n_pp", 		iconv('utf-8','windows-1251',$_REQUEST['data']['nomer'])   		);
	            		$DB->AddValue("name", 		iconv('utf-8','windows-1251',$_REQUEST['data']['predmet'])    	);
	            		$DB->AddValue("spec", 		iconv('utf-8','windows-1251',$_REQUEST['data']['spec'])    		);
	            		$DB->AddValue("min_vyz", 	iconv('utf-8','windows-1251',$_REQUEST['data']['ball_vyz'])     );
	            		$DB->AddValue("max_vyz", 	iconv('utf-8','windows-1251',$_REQUEST['data']['ball_max_vyz'])     );
	            		$DB->AddValue("min_mins", 	iconv('utf-8','windows-1251',$_REQUEST['data']['ball_mins'])    );
	            		$DB->AddValue("prikaz", 	iconv('utf-8','windows-1251',$_REQUEST['data']['prikaz'])    	);
	            		$DB->AddValue("edu_level_id", 	(int)$_REQUEST['data']['eduLevel']);
	            		if($DB->Update())
		                {
		                	$this->output["json"] = "ok";
		                }
		                else
		                {
		                	$this->output["json"] = "error2";
		                }

//		                $DB->SetTable("nsau_exams");
//		                $DB->AddCondFS("name", "=", $result['name']);
//		                $DB->AddValue("name", 		iconv('utf-8','windows-1251',$_REQUEST['data']['predmet'])    	);
//		                $DB->AddValue("min_points", 	iconv('utf-8','windows-1251',$_REQUEST['data']['ball_vyz'])     );
//	            		if($DB->Update())
//		                {
//		                	$this->output["json"] = "ok";
//		                }
//		                else
//		                {
//		                	$this->output["json"] = "error2";
//		                }
	            	}
	            	else
	            	{
	            		$this->output["exam_error"] = "error4";
	            	}
            	}
            	else
            	{
            		$this->output["json"] = "error3";
            	}
            }
            break;




            case "library_standard":
                $this->output["scripts_mode"] = $this->output["mode"] = "library_standard";
                $this->library_standard($parts2[1]);
                break;
            case "speciality_directory":
                $this->output["scripts_mode"] = $this->output["mode"] = "speciality_directory";
                $this->speciality_directory($this->module_uri);
                break;
            case "search_spec":
                $this->output["scripts_mode"] = $this->output["mode"] = "search_spec";
				break;
						case "search_peoples": {
							 $this->output["mode"] = "ajax_load_subjects";
							$this->search_people($parts2[1]);
						}
						break;
            case "faculties":
                $this->output["scripts_mode"] = $this->output["mode"] = "faculties";
                if (isset($parts2[1]) && !$parts2[1])
                	$this->output["hide_deps_href"] = true;
                $this->faculties();
                break;
            case "search":
                $this->output["scripts_mode"] = $this->output["mode"] = "search";
                $this->search_people($additional);
                break;
			case "global_people":
                $this->output["scripts_mode"][] = $this->output["mode"] = "global_people";
                $this->output["scripts_mode"][] = 'ajax_edit_photo';
				$this->output['plugins'][] = 'jquery.colorbox';
				$this->output['plugins'][] = 'jquery.Jcrop';
				$this->people_add_edit($this->module_uri, $parts2[1]);
				break;
						case "teachers_timetable":
                $this->output["scripts_mode"][] = $this->output["mode"] = "teachers_timetable";
								$this->teachers_timetable($this->module_uri);
								break;
            case "search_people":
                $this->output["scripts_mode"] = $this->output["mode"] = "search_people";
                break;
            case "employees":
                    $this->output["scripts_mode"] = $this->output["mode"] = "employees";
                    $this->employeesDataFilter();
					$this->employees();
                break;
            case "employees_ajax_search":
					$this->output["mode"] = "employees_ajax_search";
	                $this->output["scripts_mode"] = "employees";

	                $postID = (!empty($_REQUEST["data"]["filter"]["teacher_post"])) ? (int)$_REQUEST["data"]["filter"]["teacher_post"] : null;
	                $specID = (!empty($_REQUEST["data"]["filter"]["teachers_program"])) ? (int)$_REQUEST["data"]["filter"]["teachers_program"] : null;
	                $subjectID = (!empty($_REQUEST["data"]["filter"]["teachers_subject"])) ? (int)$_REQUEST["data"]["filter"]["teachers_subject"] : null;
	                $levelID = (!empty($_REQUEST["data"]["filter"]["teachers_level"])) ? (int)$_REQUEST["data"]["filter"]["teachers_level"] : null;
	                $degreeID = (!empty($_REQUEST["data"]["filter"]["teachers_degree"])) ? (int)$_REQUEST["data"]["filter"]["teachers_degree"] : null;
	                $rankID = (!empty($_REQUEST["data"]["filter"]["teachers_rank"])) ? (int)$_REQUEST["data"]["filter"]["teachers_rank"] : null;

					$needle = (isset($_REQUEST["data"]["needle"])) ? $_REQUEST["data"]["needle"] : null;
					$this->employees($needle, $postID, $specID, $subjectID, $levelID, $degreeID, $rankID);

                break;
            case "sw_group_search":
								$this->sw_group_search($_REQUEST["data"]["needle"]);
								if(count($this->output["group2"]) > 0)
									$this->output["mode"] = "sw_group_search";
                break;
            case "sw_group_select":
								$this->output["mode"] = "sw_group_select";
								$this->sw_group_select($_REQUEST["data"]["id"]);
                break;
            case "people_info":
				if($Auth->user_id != 0) {
					$this->output["scripts_mode"] = $this->output["mode"] = "people_info";
					$this->people_info($parts2);
				}
                break;

            case "people":
				$this->output["mode"] = "people";
				if ($Engine->OperationAllowed($this->module_id, "people.edit.auth", $Auth->user_id, $Auth->usergroup_id)) {
					$this->output["allow_edit_auth"] = true;
				}
                $this->output["scripts_mode"] = 'ajax_edit_photo';
                $this->output["scripts_mode"] = 'ajax_people_restore_password';
                $this->people($this->module_uri);
                break;

            case 'ajax_get_level':
            {
            	global $DB;
            	$DB->SetTable("nsau_groups");
	           	$DB->AddCondFS("specialities_code", "=", $_REQUEST['data']["spec"]);
	            $result = $DB->FetchAssoc($DB->Select(1));
	            $this->output["json"] = $result['qualification'];
            }
            break;

            case 'ajax_get_isset_group':
           	{
            	global $DB;
            	$DB->SetTable("nsau_groups");

            	$name = iconv('utf-8','windows-1251',$_REQUEST['data']["group"]);
	           	$DB->AddCondFS("name", "=", $name);
	            $result = $DB->FetchAssoc($DB->Select(1));
	            $this->output["json"] = $result;		
           	}
            break;
            /*�������������� ������*/
            case 'people_restore_password':
            {
                global $DB, $Engine;
                if ($Engine->OperationAllowed($this->module_id, "people.restore_password", 1234554321, $Auth->usergroup_id)) 
                {
	                $DB->SetTable("nsau_people");
	                $DB->AddCondFS("id", "=", $_REQUEST['data']["id"]);
	                $res = $DB->Select(1);
	                $in_people = $DB->FetchAssoc($res);

					$chars="qazxswedcvfrtgbnhyujmkiolp1234567890QAZXSWEDCVFRTGBNHYUJMKIOLP"; 
					$max=6; 
					$size=StrLen($chars)-1; 
					$new_password=null; 
					while($max--) 
	    			$new_password.=$chars[rand(0,$size)]; 


	                $DB->SetTable("auth_users");
	                $DB->AddCondFS("id", "=", $in_people["user_id"]);
	                $DB->AddValue("password",md5($new_password));
	                $DB->Update();

	                $DB->SetTable("auth_users");
	                $DB->AddCondFS("id", "=", $in_people["user_id"]);
	                $res = $DB->Select(1);
	                $in_auth = $DB->FetchAssoc($res);

	                $result = array('name' => $in_auth["displayed_name"],
	                				'email' => $in_auth["email"],
	                                'login' => $in_auth["username"],
	                                'password' => $new_password
	                                );
	                $this->output["json"] = $result;
	                $Engine->LogAction($this->module_id, "people", $in_people['id'], "restore password");

	                /* email-notification */
					$array = parse_ini_file(INCLUDES . "Restoration.ini", true);
					$notify_settings = $array["notify_settings"];
					if ($notify_settings["enable"])
					{
						$array = array(
							"%server_name%" => $_SERVER["SERVER_NAME"],
							"%site_name%" => SITE_NAME,
							"%site_short_name%" => SITE_SHORT_NAME,
							"%mail_login%" => $in_auth["username"],
							"%mail_pass%" =>  $new_password,
							"%post_time%" => date($notify_settings["time_format"]),
							"\\r\\n" => "\r\n"
							);
						$post = str_replace(array_keys($array), array_values($array), $notify_settings);

						mail($in_auth['email'], '=?koi8-r?B?'.base64_encode(iconv('windows-1251', 'koi8-r', $post["subject"])).'?=',	$post['message'], $post['headers']);
					}
            	}
            }
            break;
			////////////////////////////people menu//
			/////////////////////////////////////////
						case "ajax_people_menu_files": {
							header("Content-type: text/html; charset=windows-1251");
							$this->output["mode"] = "people_menu_files";
               $this->files_list($_REQUEST["data"]);
            }
						break;

						case "ajax_people_menu_student_work": {
								$this->output["mode"] = "people_menu_student_work";
	               				$this->studentWork();
            }
						break;

						case "ajax_people_menu_student_work_public": {
							$this->output["mode"] = "people_menu_student_work";
               $this->studentWork_public();
            }
						break;

						case "ajax_people_menu_students_work": {
							$this->output["mode"] = "people_menu_students_work";
              $this->studentsWork($_REQUEST["data"]);
            }
						break;

						case "ajax_people_menu_prof_development": {
							$this->output["mode"] = "people_menu_prof_development";
              $DB->SetTable("nsau_prof_development");
							$DB->AddCondFS("people_id", "=", $_REQUEST["data"]);
							$DB->AddOrder("date_end");
							$DB->AddOrder("date_begin");
							$res2 = $DB->Select();
							while($row_pd = $DB->FetchAssoc($res2)) {
								$row_pd["date_begin"] = implode(".", array_reverse(explode("-", $row_pd["date_begin"])));
								$row_pd["date_end"] = implode(".", array_reverse(explode("-", $row_pd["date_end"])));
								$this->output["prof_development"][$row_pd["id"]] = $row_pd;
							}
            }
						break;

						case "ajax_people_menu_education": {
							$this->output["mode"] = "people_menu_education";
							$this->showEducation($_REQUEST["data"]);
            }
						break;

						case "ajax_people_menu_main": {
							$this->output["mode"] = "people_menu_main";
              $this->people($_REQUEST["data"]);
            }
						break;

						case "ajax_people_menu_in_development": {
							$this->output["mode"] = "people_menu_in_development";
              $this->people($_REQUEST["data"]);
            }
						break;
/////////////////////////////////////////
/////////////////////////////////////////
			case "edit_people":
				$mu = explode ("/", $this->module_uri);
				// if($Auth->usergroup_id==1) echo $this->module_uri;
				if ($Engine->OperationAllowed($this->module_id, "people.handle", -1, $Auth->usergroup_id) || $Engine->OperationAllowed($this->module_id, "people.handle", $mu[1], $Auth->usergroup_id)) {
					$this->output["scripts_mode"] = $this->output["mode"] = "edit_people";
					$this->edit_people($this->module_uri);
				}
				break;
			case "ajax_load_subjects": {
				$this->output["mode"] = "ajax_load_subjects";
                $this->ajax_load_subjects($_REQUEST['data']['dep_id']);

			}
                break;
			case "prof_choice":
				$this->output["scripts_mode"] = $this->output["mode"] = "prof_choice";
				$this->prof_choice();
				break;
            case "search_group":
                $this->output["scripts_mode"] = $this->output["mode"] = "search_group";
                break;
            case "groups":
							$this->output["mode"] = "groups";
							$this->output["scripts_mode"] = 'input_calendar';
							$this->output['plugins'][] = 'jquery.ui.datepicker.min';
							$this->groups($this->module_uri);

             break;
			case "search_subjects":
                $this->output["scripts_mode"] = $this->output["mode"] = "search_subjects";
                break;
			case "students":
				$this->output["scripts_mode"] = $this->output["mode"] = "students";
				$this->students($this->module_uri);
				break;
			case "subjects":
                $this->output["scripts_mode"] = $this->output["mode"] = "subjects";
                $this->subjects($this->module_uri);
                break;
			case "dep_subjects":
				$this->dep_subjects($parts2[1], $mode, $parts2);




				break;

			case "izop_ekonom":




			$DB->SetTable("nsau_subjects","p");

			$DB->AddField("p.id","p_id");
			$DB->AddField("p.name","p_n");
			$DB->AddTable("nsau_files", "ptr");
			$DB->AddFields(array("ptr.id","ptr.name", "ptr.descr"));
			$DB->AddTable("nsau_files_subj","s");
			$DB->AddFields(array("s.file_id","s.spec_code","s.spec_type","s.subject_id", "s.education", "s.spec_id","s.approved"));
			$DB->AddCondFS("s.approved", "=", "1");
			//$DB->AddCondFS("s.spec_code", "=", "080109");
			//$DB->AddCondFS("s.education", "=", "�������");
			$DB->AddCondFF("s.file_id", "=", "ptr.id");
			$DB->AddCondFF("s.subject_id", "=", "p.id");
			//echo $DB->SelectQuery();


			$res = $DB->Select();
			while ($row = $DB->FetchAssoc($res)){
			$code1[]=$row;
			$this->output["code1"][] = $row;



			}
			$DB->SetTable("nsau_specialities");
			$DB->AddField("code");
			$DB->AddField("name");



		$res = $DB->Select();
		while ($row = $DB->FetchAssoc($res))
			 $this->output["specialities"][] = $row;

			$this->output["mode"] = "izop_ekonom";


				break;
				case "izop_engineer":




			$DB->SetTable("nsau_subjects","p");

			$DB->AddField("p.id","p_id");
			$DB->AddField("p.name","p_n");
			$DB->AddTable("nsau_files", "ptr");
			$DB->AddFields(array("ptr.id","ptr.name", "ptr.descr"));
			$DB->AddTable("nsau_files_subj","s");
			$DB->AddFields(array("s.file_id","s.spec_code","s.spec_type","s.subject_id", "s.education", "s.spec_id","s.approved"));
			$DB->AddCondFS("s.approved", "=", "1");
			//$DB->AddCondFS("s.spec_code", "=", "080109");
			//$DB->AddCondFS("s.education", "=", "�������");
			$DB->AddCondFF("s.file_id", "=", "ptr.id");
			$DB->AddCondFF("s.subject_id", "=", "p.id");
			//echo $DB->SelectQuery();


			$res = $DB->Select();
			while ($row = $DB->FetchAssoc($res)){
			$code1[]=$row;
			$this->output["code1"][] = $row;



			}
			$DB->SetTable("nsau_specialities");
			$DB->AddField("code");
			$DB->AddField("name");



		$res = $DB->Select();
		while ($row = $DB->FetchAssoc($res))
			 $this->output["specialities"][] = $row;

			$this->output["mode"] = "izop_engineer";


				break;
			case "izop_bio":



			$DB->SetTable("nsau_subjects","p");

			$DB->AddField("p.id","p_id");
			$DB->AddField("p.name","p_n");
			$DB->AddTable("nsau_files", "ptr");
			$DB->AddFields(array("ptr.id","ptr.name", "ptr.descr"));
			$DB->AddTable("nsau_files_subj","s");
			$DB->AddFields(array("s.file_id","s.spec_code","s.spec_type","s.subject_id", "s.education", "s.spec_id","s.approved"));
			$DB->AddCondFS("s.approved", "=", "1");
			//$DB->AddCondFS("s.spec_code", "=", "080109");
			//$DB->AddCondFS("s.education", "=", "�������");
			$DB->AddCondFF("s.file_id", "=", "ptr.id");
			$DB->AddCondFF("s.subject_id", "=", "p.id");
			//echo $DB->SelectQuery();


			$res = $DB->Select();
			while ($row = $DB->FetchAssoc($res)){
			$code1[]=$row;
			$this->output["code1"][] = $row;



			}
			$DB->SetTable("nsau_specialities");
			$DB->AddField("code");
			$DB->AddField("name");



		$res = $DB->Select();
		while ($row = $DB->FetchAssoc($res))
			 $this->output["specialities"][] = $row;

			$this->output["mode"] = "izop_bio";

				break;
				case "izop_government":



			$DB->SetTable("nsau_subjects","p");

			$DB->AddField("p.id","p_id");
			$DB->AddField("p.name","p_n");
			$DB->AddTable("nsau_files", "ptr");
			$DB->AddFields(array("ptr.id","ptr.name", "ptr.descr"));
			$DB->AddTable("nsau_files_subj","s");
			$DB->AddFields(array("s.file_id","s.spec_code","s.spec_type","s.subject_id", "s.education", "s.spec_id","s.approved"));
			$DB->AddCondFS("s.approved", "=", "1");
			//$DB->AddCondFS("s.spec_code", "=", "080109");
			//$DB->AddCondFS("s.education", "=", "�������");
			$DB->AddCondFF("s.file_id", "=", "ptr.id");
			$DB->AddCondFF("s.subject_id", "=", "p.id");
			//echo $DB->SelectQuery();


			$res = $DB->Select();
			while ($row = $DB->FetchAssoc($res)){
			$code1[]=$row;
			$this->output["code1"][] = $row;



			}
			$DB->SetTable("nsau_specialities");
			$DB->AddField("code");
			$DB->AddField("name");



		$res = $DB->Select();
		while ($row = $DB->FetchAssoc($res))
			 $this->output["specialities"][] = $row;

			$this->output["mode"] = "izop_government";

				break;

		case "students_progress":
				$this->output["scripts_mode"] = $this->output["mode"] = "students_progress";
				$this->students_progress();
				break;
			case "ajax_students_progress":
				$this->output["mode"] = "ajax_students_progress";
				$this->students_progress($_REQUEST["data"]["s"]);
				break;
			case "files_list":
				$this->output["scripts_mode"] = $this->output["mode"] = "show_attach_file";
				$this->files_list($this->module_uri);
				break;
            case "timetable":
                $this->timetable();
                $this->output["scripts_mode"] = $this->output["mode"] = "timetable";
                break;
						case "timetable_print":

                $this->timetable_print();
								$this->output["scripts_mode"] = $this->output["mode"] = "timetable_print";
                break;
            case "timetable_make":
                $this->output["scripts_mode"] = $this->output["mode"] = "timetable_make";
                $this->timetable_make();
                break;
            case "timetable_subjects_by_departments":
                $this->output["scripts_mode"][] = $this->output["mode"] = "timetable_subjects_by_departments";
                $this->output["scripts_mode"][] = 'input_calendar';
				$this->output['plugins'][] = 'jquery.ui.datepicker.min';
                $this->timetable_subjects_by_departments($mode);
                break;
            case "timetable_graphics":
                $this->output["scripts_mode"] = $this->output["mode"] = "timetable_graphics";
                $this->timetable_graphics($mode);
                break;
            case "timetable_groups":
                $this->output["scripts_mode"] = $this->output["mode"] = "timetable_groups";
                $this->timetable_groups($mode);
                break;
            case "timetable_faculties":
                $this->output["scripts_mode"] = $this->output["mode"] = "timetable_faculties";
                $this->timetable_faculties($mode);
                break;
            case "timetable_departmens":
                $this->output["scripts_mode"] = $this->output["mode"] = "timetable_departmens";
                $this->timetable_departmens($this->module_uri);
                break;
            case "timetable_teachers":
                $this->output["scripts_mode"] = $this->output["mode"] = "timetable_teachers";
                if (isset($parts2[1])) {
					$this->output["scripts_mode"] = $this->output["mode"] = "select_teachers";
					$this->select_teachers($parts2[1]);
				}
				else
					$this->timetable_teachers($mode);
                break;
            case "timetable_subjects":
                $this->output["scripts_mode"] = $this->output["mode"] = "timetable_subjects";
                $this->timetable_subjects($mode);
                break;
            case "timetable_auditorium":
                $this->output["scripts_mode"] = $this->output["mode"] = "timetable_auditorium";
                $this->timetable_auditorium($mode);
                break;
            case "timetable_show":
                $this->output["mode"] = "timetable_show";
								$this->output["scripts_mode"] = "timetable_make";
								$this->output["show_mode"] = $parts2[1];
                $this->timetable_show();
                break;
			case "rekom":
				$this->output["scripts_mode"] = $this->output["mode"] = "rekom";
				$this->rekom();
				break;
						case "search_peoples": {
							$this->output["mode"] = "ajax_load_subjects";
							$this->search_people($parts2[1]);
						}
						break;
			case "department_files":
				if(isset($parts2[1])) {
					$this->output["scripts_mode"] = $this->output["mode"] = "department_files";
					$this->department_files($parts2[1]);
				}
				break;
			case "department_curators":
				$this->output["mode"] = "department_curators";
				if(isset($parts2[1])) {
					$this->department_curators($parts2[1]);
				}
				break;
			case "faculties_files":
				if(isset($parts2[1])) {
					$this->output["scripts_mode"] = $this->output["mode"] = "faculties_files";
					$this->faculties_files($parts2[1]);
				}
				break;
			case "pulpit_list":
				$this->output["scripts_mode"] = $this->output["mode"] = "pulpit_list";
				if(isset($parts2[1]))
					$this->pulpit_list($parts2[1]);
				break;
			case "faculty_spec":
				$this->output["scripts_mode"] = $this->output["mode"] = "faculty_specialities";
				if(isset($parts2[1]))
					$this->faculty_specialities($parts2[1]);
				break;
            case "groups_list":
                $this->output["scripts_mode"] = $this->output["mode"] = "groups_list";
				//die(print_r($mode));
                if($mode[0])
                {
                    if($mode[0] == "add_group")
                    {
                        $this->add_group();
                    }
                    else
                    {
                        if(isset($mode[1]) && ( $mode[1] == "delete_group" || $mode[1] == "edit_group"))
                        {
                            $this->edit_group($mode);
                        }
                        else
                        {
                            if($mode[1])
                                $this->edit_student($mode);
                            else
                                $this->students_list($mode);
                        }
                    }
                }
                else
                {
                    $this->timetable_show();
                }
            break;

            case "student_registr": {
                $this->output["scripts_mode"] = $this->output["mode"] = "student_registr";
                $this->studentRegistration();
			}
            break;

			case 'modules_options':
		    {
		    	 $this->output["scripts_mode"] = $this->output["mode"] = "modules_options";
		        if ($Auth->usergroup_id==1) 
		        {
		        	global $DB, $DB_LOGS;
					$DB->SetTable("engine_modules_options");
					$DB->AddOrder("option");
					$res = $DB->Select();
	                while($row = $DB->FetchAssoc($res)) 
	                {
	                    $this->output["modules_options"][$row['module_id']][$row['option']] = $row;
	                    $save_data[$row['option']] = 1;
	                }

	                if (isset($_POST['save_options'])) 
	                {
	                	foreach ($save_data as $key => $value) 
	                	{
	                		$DB->SetTable("engine_modules_options");
							$DB->AddCondFS("option", "=", $key);
							if (empty($_POST[$key])) 
							{
								$DB->AddValue("value", 0);
							}
							elseif ($_POST[$key]=="on") 
							{
								$DB->AddValue("value", 1);
							}
							else
							{
								$DB->AddValue("value", $_POST[$key]);
							}
							$DB->Update();
	                	}	

	                	$DB->SetTable("engine_modules_options");
						$DB->AddOrder("option");
						$res = $DB->Select();
		                while($row = $DB->FetchAssoc($res)) 
		                {
		                    $this->output["modules_options"][$row['module_id']][$row['option']] = $row;
		                    $save_data[$row['option']] = 1;
		                }
	                }

	                $files = scandir("files/students");

	                foreach ($files as $key => $value) 
	                {
	                	$file_name = $value;
		                $value = explode("_", $value);
	                	if ($value[0] == 1 || $value[0] == 2 || $value[0] == 3) 
	                	{
                			$csv = array_map('str_getcsv', file("files/students/".$file_name));
                			$deleted = explode(";", $csv[0][0]);
                			while ($line = fgetcsv($file));
                			{
							  $numcols = count($line);
							  if ($numcols != $allowedColNum) 
							  {
							     break;
							  }
							  $col = $line[0]; 
							  $batchcount++;
							}


		                	$tshi = explode("tshi", $value[2]);

		                	if ($tshi[1]) 
		                	{
		                		$date = explode("-", $tshi[1]);
		                	}
		                	else
		                	{
		                		$date = explode("-", $value[2]);
		                	}
		                	$date[5] = str_replace(".csv", "", $date[5]);
	                		$students_upload_files[$value[0]][$value[1]][] = array (
		                		'date' => $date[0].".".$date[1].".".$date[2], 
		                		'time' => $date[3].":".$date[4].":".$date[5],
		                		'tshi' => $tshi[1] ? 1 : 0,
		                		'add_id' => $csv,
		                		'add_count' => count($csv),
		                		'file' => $file_name
		                	);
                		}
                		elseif ($value[0] == "Deleted") 
                		{
                			$csv = array_map('str_getcsv', file("files/students/".$file_name));
                			$deleted = explode(";", $csv[0][0]);
                			while ($line = fgetcsv($file));
                			{
							  $numcols = count($line);
							  if ($numcols != $allowedColNum) 
							  {
							     break;
							  }
							  $col = $line[0]; 
							  $batchcount++;
							}

                			$tshi = explode("tshi", $value[3]);
		                	if ($tshi[1]) 
		                	{
		                		$date = explode("-", $tshi[1]);
		                	}
		                	else
		                	{
		                		$date = explode("-", $value[3]);
		                	}
                			$date[5] = str_replace(".csv", "", $date[5]);
	                		$students_upload_deleted[$date[0].".".$date[1].".".$date[2]."+".$date[3].":".$date[4].":".$date[5]] = array (
		                		'date' => $date[0].".".$date[1].".".$date[2], 
		                		'time' => $date[3].":".$date[4].":".$date[5],
		                		'fac'  => $value[2],
		                		'deleted_id' => $csv[0][0],
		                		'deleted_count' => count($deleted),
		                		'file' => $file_name
		                	);
                		}
                		elseif($value[2] == "original.csv")
                		{
			                $csv = array_map('str_getcsv', file("files/students/".$file_name));
							$tshi = explode("tshi", $value[1]);
		                	if ($tshi[1]) 
		                	{
		                		$date = explode("-", $tshi[1]);
		                	}
		                	else
		                	{
		                		$date = explode("-", $value[1]);
		                	}
                			$date[5] = str_replace(".csv", "", $date[5]);
							
							$DB_LOGS->SetTable("Ensau");
							//$DB_LOGS->AddCondFS("entry_id", "LIKE", mb_strimwidth($file_name, 0, 45)."%");
							
							if (strlen($date[0])==1) 
							{
								$fuck_fix = "0".$date[0];
							}
							else
							{
								$fuck_fix = $date[0];
							}

							// $DB_LOGS->AddCondFS("entry_id", "=", mb_strimwidth($file_name, 0, 50));
							// $DB_LOGS->AddCondFF("time", "LIKE", "%%");
							// $res_logs = $DB_LOGS->Select(1);
							// while($row_logs = $DB_LOGS->FetchAssoc($res_logs)) 
							// {
							// 	$ress = $row_logs;
							// }

	                		$students_upload_original[$date[0].".".$date[1].".".$date[2]."+".$date[3].":".$date[4].":".$date[5]] = array (
		                		'date' => $date[0].".".$date[1].".".$date[2], 
		                		'time' => $date[3].":".$date[4].":".$date[5],
		                		'fac'  => $value[0],
		                		'file' => $file_name,
		                		// 'id' => $csv,
		                		'count' => count($csv),
		                		'file' => $file_name,
		                		//'ress' => $ress,
		                	);
                		}
	                }

	                $this->output["students_upload_files"] = $students_upload_files;
	                $this->output["students_upload_deleted"] = $students_upload_deleted;
	                $this->output["students_upload_original"] = $students_upload_original;



		        }
		    }
		    break;

            case "student_moderation": {
                $this->output["mode"] = "student_moderation";
                $this->studentModeration();
			}
            break;

            case "ajax_delete_subj": {
                $this->output["mode"] = "ajax_delete_subj";
                $this->ajax_delete_subj($_REQUEST['data']['subj_id']);
			}
			break;
						case "ajax_hide_subj": {
                $this->output["mode"] = "ajax_hide_subj";
                $this->ajax_hide_subj($_REQUEST['data']['subj_id'], $_REQUEST['data']['mode']);
			}
            break;
					case "ajax_delete_prof_development": {
                $this->output["mode"] = "ajax_delete_prof_development";
                $this->ajax_delete_prof_development($_REQUEST['data']['id']);
			}
            break;
						case "ajax_edit_prof_development": {
                $this->output["mode"] = "ajax_edit_prof_development";
                $this->ajax_edit_prof_development($_REQUEST['data']['id'], $_REQUEST['data']['fild'], $_REQUEST['data']['val']);
			}
            break;
						case "ajax_add_prof_development": {
                $this->output["mode"] = "ajax_add_prof_development";
                $this->ajax_add_prof_development($_REQUEST['data']);
			}
            break;
            case "ajax_search_file": {
							$json = array();
							$DB->SetTable("nsau_files");
							$DB->AddCondFS("name", "LIKE", "".iconv("utf-8", "Windows-1251", $_REQUEST['data']["q"])."%");
							$DB->AddCondFS("user_id", "=", $Auth->user_id);
							$DB->AddCondFS("deleted", "=", 0);
							$DB->AddField("id");
							$DB->AddField("name");
							$DB->AddField("filename");
							$DB->AddField("descr");
							$DB->AddOrder("name");
							$res = $DB->Select();
							$count = 0;
							while($row = $DB->FetchAssoc($res)) {
								$row["name"] = $row["name"].".".$row["filename"];
								$json[] = $row;

								$count++;
								if($count >= 10) break;
							}

							header("Cache-Control: no-cache, must-revalidate");
							header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
							header("Pragma: no-cache");
							$this->output["json"] = $json;
						}
            break;
            case "ajax_edit_photo": {
                $this->output["mode"] = "ajax_edit_photo";
                $this->ajax_edit_photo();
			}
            break;
						case "ajax_edit_timetable": {
                //$this->output["mode"] = "ajax_edit_photo";
 $this->ajax_edit_timetable($_REQUEST["data"]["mode"], $_REQUEST["data"]["week"], $_REQUEST["data"]["day"], $_REQUEST["data"]["pair"], $_REQUEST["data"]["group"], $_REQUEST["data"]["data"], $_REQUEST["data"]["izop_week"]);
			}
            break;

	        case "ajax_edit_teacher_educational_program": {
		        $this->ajax_edit_teacher_educational_program((int)$_REQUEST["data"]["id"], (int)$_REQUEST["data"]["people_id"], $_REQUEST["data"]["mode"]);
	        }
	        break;

            default:
                die("Ensau module error #1100 at node $this->node_id: unknown mode &mdash; $this->mode.");

        }
    }

	function ajax_add_prof_development($data) {
		global $DB;
		$DB->SetTable("nsau_prof_development");
		$DB->AddValue("course", iconv("utf-8", "cp1251", $data["course"]));
		$DB->AddValue("location", iconv("utf-8", "cp1251", $data["location"]));
		$DB->AddValue("hours", $data["hours"]);
		$DB->AddValue("edu_type", $data["edu_type"]);
		$DB->AddValue("doc_type", $data["doc_type"]);

		$DB->AddValue("date_end", implode("-", array_reverse(explode(".", $data["date_end"]))));
		$DB->AddValue("date_begin", implode("-", array_reverse(explode(".", $data["date_begin"]))));
		$DB->AddValue("people_id", $data["people_id"]);
		$DB->Insert();
		$id = $DB->LastInsertId();
		$this->output["json"]=$id;
	}

	function ajax_delete_prof_development($id) {
		global $DB;
		$id = intval($id);
		$DB->SetTable("nsau_prof_development");
		$DB->AddCondFS("id", "=", $id);
		$DB->Delete();
		$this->output["json"]="deleted";
	}

	function ajax_edit_prof_development($id, $fild, $val) {
		global $DB;
		if(!empty($id) && !empty($fild) && !empty($val)) {
			$id = intval($id);
			if(($fild == "date_begin") ||  ($fild == "date_end"))
				$val = implode("-", array_reverse(explode(".", $val)));
			$DB->SetTable("nsau_prof_development");
			$DB->AddCondFS("id", "=", $id);
			$DB->AddValue($fild, iconv("utf-8", "cp1251", $val));
			$DB->Update();
			$this->output["json"]="upd";
		}
		else {
			$this->output["json"]="err";
		}
	}

	function ajax_edit_timetable($mode, $week, $day, $pair, $group, $data = array(),$izop_week)
	{
		global $DB;

		$DB->SetTable($this->db_prefix."timetable_new");
		$DB->AddCondFS("week", "=", $week);
		$DB->AddCondFS("day", "=", $day);
		$DB->AddCondFS("pair", "=", $pair);
		$DB->AddCondFS("group_id", "=", $group);
		if($izop_week!="")
			$DB->AddCondFS("izop_week", "=", $izop_week);
		$DB->AddOrder("id");
		//die($DB->SelectQuery());
		$res = $DB->Select();
		$offset = $data["n"]+1;
		if($DB->NumRows($res)>=$offset)
		{
			for($i=0;$i<=$data["n"];$i++)
				$row = $DB->FetchAssoc($res);
				$edit_id = $row["id"];
		}
		else
			$edit_id = false;



		switch($mode){
			case "subj": {
				if($edit_id)
				{
					$DB->SetTable($this->db_prefix."timetable_new");
					$DB->AddCondFS("id", "=", $edit_id);
					$DB->AddValue("subject_id", $data["id"]);
					$DB->Update();
					$this->output["json"] = $row["id"].">".$i;
				}
				else
				{
					$DB->SetTable($this->db_prefix."timetable_new");
					$DB->AddValue("subject_id", $data["id"]);
					$DB->AddValue("week", $week);
					$DB->AddValue("pair", $pair);
					$DB->AddValue("day", $day);
					$DB->AddValue("type", 0);
					$DB->AddValue("group_id", $group);
					if($izop_week!="")
						$DB->AddValue("izop_week", $izop_week);
					$DB->Insert();
						$this->output["json"]="add";
				}
			}
			break;
			case "type": {
				if($edit_id)
				{
					$DB->SetTable($this->db_prefix."timetable_new");
					$DB->AddCondFS("id", "=", $edit_id);
					$DB->AddValue("type", $data["id"]);
					$DB->Update();
				}
				else
				{
					$this->output["json"]["error"] = "�������� ����������.";
				}
			}
			break;
			case "subgr": {
				if($edit_id)
				{
					$DB->SetTable($this->db_prefix."timetable_new");
					$DB->AddCondFS("id", "=", $edit_id);
					$DB->AddValue("subgroup", $data["id"]);
					$DB->Update();
				}
				else
				{
					$this->output["json"]["error"] = "�������� ����������.";
				}
			}
			break;
			case "aud_add": {
				if($edit_id)
				{
					$DB->SetTable($this->db_prefix."timetable_new");
					$DB->AddCondFS("id", "=", $edit_id);
					$DB->AddField("auditorium_id");
					$res = $DB->Select(1);
					$row = $DB->FetchAssoc($res);
					$DB->SetTable($this->db_prefix."timetable_new");
					$DB->AddCondFS("id", "=", $edit_id);
					if($row["auditorium_id"] == NULL)
					{
						$DB->AddValue("auditorium_id", $data["id"].";");
					}
					else
					{
						$auds = explode(";", $row["auditorium_id"]);
						array_pop($auds);
						if(!in_array($data["id"], $auds))
						{
							$auds[] = $data["id"].";";
							$auditorium = implode(";", $auds);
							$DB->AddValue("auditorium_id", $auditorium);
						}
						else {
							$this->output["json"]["error"] = "������ ��������� ��� ���������.";
							return;
						}
					}
					$DB->Update();
					$this->output["json"] = $auditorium;
				}
				else
				{
					$this->output["json"]["error"] = "�������� ����������.";
				}
			}
			break;
			case "aud_del": {
				if($edit_id)
				{
					$DB->SetTable($this->db_prefix."timetable_new");
					$DB->AddCondFS("id", "=", $edit_id);
					$DB->AddField("auditorium_id");
					$res = $DB->Select(1);
					$row = $DB->FetchAssoc($res);
					$DB->SetTable($this->db_prefix."timetable_new");
					$DB->AddCondFS("id", "=", $edit_id);
					$auds = explode(";", $row["auditorium_id"]);
					array_pop($auds);
					foreach($auds as $aud)
						if($aud != $data["id"])
							$result[]=$aud;
							$auditorium = implode(";", $result);
							$DB->AddValue("auditorium_id", $auditorium.";");
							$DB->Update();
				}
				else
				{
					$this->output["json"]["error"] = "�������� ����������.";
				}
			}
			break;
			case "teach_add": {
				if($edit_id)
				{
					$DB->SetTable($this->db_prefix."timetable_new");
					$DB->AddCondFS("id", "=", $edit_id);
					$DB->AddField("teacher_id");
					$res = $DB->Select(1);
					$row = $DB->FetchAssoc($res);
					$DB->SetTable($this->db_prefix."timetable_new");
					$DB->AddCondFS("id", "=", $edit_id);
					if($row["teacher_id"] == NULL)
					{
						$DB->AddValue("teacher_id", $data["id"].";");
					}
					else
					{
						$auds = explode(";", $row["teacher_id"]);
						array_pop($auds);
						if(!in_array($data["id"], $auds))
						{
							$auds[] = $data["id"].";";
							$auditorium = implode(";", $auds);
							$DB->AddValue("teacher_id", $auditorium);
						}
						else {
							$this->output["json"]["error"] = "������ ������������� ��� ��������.";
							return;
						}
					}
					$DB->Update();
					$this->output["json"] = $auditorium;
				}
				else
				{
					$this->output["json"]["error"] = "�������� ����������.";
				}
			}
			break;
			case "teach_del": {
				if($edit_id)
				{
					$DB->SetTable($this->db_prefix."timetable_new");
					$DB->AddCondFS("id", "=", $edit_id);
					$DB->AddField("teacher_id");
					$res = $DB->Select(1);
					$row = $DB->FetchAssoc($res);
					$DB->SetTable($this->db_prefix."timetable_new");
					$DB->AddCondFS("id", "=", $edit_id);
					$auds = explode(";", $row["teacher_id"]);
					array_pop($auds);
					foreach($auds as $aud)
						if($aud != $data["id"])
							$result[]=$aud;
							$auditorium = implode(";", $result);
							$DB->AddValue("teacher_id", $auditorium.";");
					$DB->Update();
				}
				else
				{
					$this->output["json"]["error"] = "�������� ����������.";
				}
			}
			break;

			case "add_time": {
				if($edit_id)
				{
					$DB->SetTable($this->db_prefix."timetable_new");
					$DB->AddCondFS("id", "=", $edit_id);
					$DB->AddValue("comment_".$data["is"], implode("-",array_reverse(explode(".",$data["date"]))));
					$DB->Update();
				}
				else
				{
					$this->output["json"]["error"] = "�������� ����������.";
				}
			}
			break;
			case "comm_add": {
				if($edit_id)
				{
					$DB->SetTable($this->db_prefix."timetable_new");
					$DB->AddCondFS("id", "=", $edit_id);
					$DB->AddValue("comment", iconv("utf-8", "cp1251", $data["text"]));
					$DB->Update();
				}
				else
				{
					$this->output["json"]["error"] = "�������� ����������.";
				}
			}
			break;
			case "copy": {
				if($edit_id)
				{
					$DB->SetTable($this->db_prefix."timetable_new");
					$DB->AddCondFS("id", "=", $edit_id);
					$res = $DB->Select();
					$row = $DB->FetchAssoc($res);
					foreach($data["id"] as $to_id)
					{
						$DB->SetTable($this->db_prefix."timetable_new");
						$DB->AddCondFS("week", "=", $week);
						$DB->AddCondFS("day", "=", $day);
						$DB->AddCondFS("pair", "=", $pair);
						$DB->AddCondFS("group_id", "=", $to_id);
						if($izop_week!="")
							$DB->AddCondFS("izop_week", "=", $izop_week);
						$DB->AddOrder("id");
						$res2 = $DB->Select();
						$offset = $data["n"]+1;
						if($DB->NumRows($res2)>=$offset)
						{
							for($i=0;$i<=$data["n"];$i++)
								$row2 = $DB->FetchAssoc($res2);
								$edit_id = $row2["id"];
						}
						else
							$edit_id = false;

						$DB->SetTable($this->db_prefix."timetable_new");
						if($edit_id)
							$DB->AddCondFS("id", "=", $edit_id);
						else
							$DB->AddValue("group_id", $to_id);
						$DB->AddValue("subgroup", $row["subgroup"]);
						$DB->AddValue("pair", $row["pair"]);
						$DB->AddValue("day", $row["day"]);
						$DB->AddValue("week", $row["week"]);
						$DB->AddValue("auditorium_id", $row["auditorium_id"]);
						$DB->AddValue("teacher_id", $row["teacher_id"]);
						$DB->AddValue("subject_id", $row["subject_id"]);
						$DB->AddValue("type", $row["type"]);
						$DB->AddValue("comment", $row["comment"]);
						$DB->AddValue("comment_from", $row["comment_from"]);
						$DB->AddValue("comment_to", $row["comment_to"]);
						$DB->AddValue("ctime", $row["ctime"]);
						$DB->AddValue("izop_week",  $row["izop_week"]);
						if($edit_id)
							$DB->Update();
						else
							$DB->Insert();

						$this->output["json"]["good"] = "�����������.";
					}
				}
				else
				{
					$this->output["json"]["error"] = "�������� ����������.";
				}
			}
			break;
			case "clear": {
				if($edit_id)
				{
					$DB->SetTable($this->db_prefix."timetable_new");
					$DB->AddCondFS("id", "=", $edit_id);
					$DB->Delete();
				}
				else
				{
					$this->output["json"]["error"] = "�������� ����������.";
				}
			}
			break;
			case "actual_date": {
				$DB->SetTable($this->db_prefix."timetable_dates");
				$DB->AddCondFS("group_id", "=", $group);
				if($data["izop_week"]!="")
					$DB->AddCondFS("week", "=", $data["izop_week"]);
				$res = $DB->Select(1);
				$DB->SetTable($this->db_prefix."timetable_dates");
				$DB->AddValue("actual_".$data["is"], implode("-",array_reverse(explode(".",$data["date"]))));
				if($DB->NumRows($res)>0)
				{
					$DB->AddCondFS("group_id", "=", $group);
					if($data["izop_week"]!="")
						$DB->AddCondFS("week", "=", $data["izop_week"]);
					$DB->Update();
				}
				else
				{
					$DB->AddValue("group_id", $group);
					$DB->AddValue("week", $data["izop_week"]);
					$DB->Insert();
				}

			}
			break;
		}




	}















	function ajax_hide_subj($subj_id, $mode)
	{
		global $DB;
		$DB->SetTable("nsau_subjects");
		$DB->AddCondFS("id", "=", $subj_id);
		if($mode=="hide")
			$DB->AddValue("is_hidden", 1);
		elseif($mode=="show")
			$DB->AddValue("is_hidden", 0);
		$DB->Update();
		$this->output["json"]=1;
	}



	function ajax_move_subjects($from, $to, $mode, $current_id = NULL)
	{
		global $DB, $Engine, $Auth;
		switch($mode)
		{
 			case "edit": {
				$DB->SetTable("nsau_subjects");
				$DB->AddCondFS("id", "=", $to);
				$res = $DB->Select();
				$row = $DB->FetchAssoc($res);
				$to_dep_id = $row["allow_move"];
				if(!$Engine->OperationAllowed($this->module_id, "subject.files.transfer", 0, $Auth->usergroup_id)) {
					$DB->SetTable("nsau_subjects");
					$DB->AddCondFS("id", "=", $from);
					$res = $DB->Select();
					$row = $DB->FetchAssoc($res);
					$from_dep_id = $row["allow_move"];
				} else {
					$from_dep_id = $to_dep_id;
				}
				if($from_dep_id != $to_dep_id)
				{
					$DB->SetTable("nsau_departments");
					$DB->AddCondFS("id", "=", $from_dep_id);
					$res = $DB->Select();
					$row = $DB->FetchAssoc($res);
					$from_name = $row["name"];
					$DB->SetTable("nsau_departments");
					$DB->AddCondFS("id", "=", $to_dep_id);
					$res = $DB->Select();
					$row = $DB->FetchAssoc($res);
					$to_name = $row["name"];
					if($current_id==$from_dep_id)
						$response["error"][] = "���������� ��������� �������� �� ��������� ����������. ��� ��������� ���������� ����������� �������� ��: ".$to_name."";
					else
						$response["error"][] = "���������� ��������� ��������. � ������ ���������� ����������� �������� ��: ".$from_name."";
				}
				else
				{
					$result = $this->ajax_move_subjects($from, $from, "show");
					$DB->SetTable("nsau_files_subj");
					$DB->AddCondFS("subject_id", "=", $from);
					$DB->AddValue("subject_id", $to);
					if($DB->Update() && isset($result["from_files"])){
						$response["response"][] =  "�������� ������ � ���������� ����������.";
					}

					$DB->SetTable("nsau_curriculum");
					$DB->AddCondFS("subject_id", "=", $from);
					$DB->AddValue("subject_id", $to);
					if($DB->Update() && isset($result["from_curr"])){
						$response["response"][] =  "�������� ���������� � ������� ������ ����������.";
					}

					$DB->SetTable("izop_curriculum");
					$DB->AddCondFS("subject_id", "=", $from);
					$DB->AddValue("subject_id", $to);
					if($DB->Update()  && isset($result["from_curr_izop"])){
						$response["response"][] =  "�������� ���������� � ������� ������ ����������(����).";
					}

				}

				return $response;
			}
			break;
			case "show": {
//�����
				$DB->SetTable("nsau_files_subj", "nfs");
				$DB->AddCondFS("nfs.subject_id", "=", $to);
				$DB->AddTable("nsau_files", "nf");
				$DB->AddCondFF("nfs.file_id", "=", "nf.id");
				$DB->AddField("nf.name", "name");
				$DB->AddField("nf.filename", "fname");
				$DB->AddTable("nsau_subjects", "ns");
				$DB->AddCondFF("nfs.subject_id", "=", "ns.id");
				$DB->AddField("ns.name", "subj_name");
				$DB->AddTable("nsau_specialities", "nspec");
				$DB->AddCondFF("nfs.spec_id", "=", "nspec.id");
				$DB->AddTable("auth_users", "au");
				$DB->AddCondFF("nf.user_id", "=", "au.id");
				$DB->AddField("au.displayed_name", "username");
				$DB->AddField("nspec.name", "spec_name");
				$DB->AddField("nspec.code", "spec_code");
				$DB->AddField("nf.id", "f_id");
				$DB->AddField("nfs.spec_type", "spec_type");
				$res = $DB->Select();
				while($row = $DB->FetchAssoc($res))
				{
					$result["to_files"][$row["f_id"]]["name"] = $row["name"].".".$row["fname"];
					$result["to_files"][$row["f_id"]]["subj_name"] = $row["subj_name"];
					$result["to_files"][$row["f_id"]]["spec_code"] = $row["spec_code"];
					$result["to_files"][$row["f_id"]]["spec_name"] = $row["spec_name"];
					$result["to_files"][$row["f_id"]]["username"] = $row["username"];
				}
				$DB->SetTable("nsau_files_subj", "nfs");
				$DB->AddCondFS("nfs.subject_id", "=", $from);
				$DB->AddTable("nsau_files", "nf");
				$DB->AddCondFF("nfs.file_id", "=", "nf.id");
				$DB->AddField("nf.name", "name");
				$DB->AddField("nf.filename", "fname");
				$DB->AddTable("nsau_subjects", "ns");
				$DB->AddCondFF("nfs.subject_id", "=", "ns.id");
				$DB->AddField("ns.name", "subj_name");
				$DB->AddTable("nsau_specialities", "nspec");
				$DB->AddCondFF("nfs.spec_id", "=", "nspec.id");
				$DB->AddTable("auth_users", "au");
				$DB->AddCondFF("nf.user_id", "=", "au.id");
				$DB->AddField("nspec.name", "spec_name");
				$DB->AddField("au.displayed_name", "username");
				$DB->AddField("nspec.code", "spec_code");
				$DB->AddField("nf.id", "f_id");
				$DB->AddField("nfs.spec_type", "spec_type");
				$res = $DB->Select();
				while($row = $DB->FetchAssoc($res))
				{
					$result["from_files"][$row["f_id"]]["name"] = $row["name"].".".$row["fname"];
					$result["from_files"][$row["f_id"]]["subj_name"] = $row["subj_name"];
					$result["from_files"][$row["f_id"]]["spec_code"] = $row["spec_code"];
					$result["from_files"][$row["f_id"]]["spec_name"] = $row["spec_name"];
					$result["from_files"][$row["f_id"]]["username"] = $row["username"];
				}
//�� �����
				$DB->SetTable("nsau_curriculum", "nc");
				$DB->AddCondFS("nc.subject_id", "=", $to);
				$DB->AddTable("nsau_subject_group", "nsg");
				$DB->AddCondFF("nc.subject_group_id", "=", "nsg.id");
				$DB->AddTable("nsau_subjects", "ns");
				$DB->AddCondFF("nc.subject_id", "=", "ns.id");
				$DB->AddTable("nsau_spec_type", "nst");
				$DB->AddCondFF("nc.spec_type_id", "=", "nst.id");
				$DB->AddTable("nsau_specialities", "nspec");
				$DB->AddCondFF("nst.spec_id", "=", "nspec.id");
				$DB->AddField("nspec.direction", "spec_direction");
				$DB->AddField("nspec.code", "spec_code");
				$DB->AddField("nspec.name", "spec_name");
				$DB->AddField("ns.name", "subj_name");
				$DB->AddField("nsg.name", "sgroup_name");
				$DB->AddField("nc.id", "id");
				$res = $DB->Select();
				while($row = $DB->FetchAssoc($res))
				{
					$result["to_curr"][$row["id"]]["group_name"] = $row["sgroup_name"];
					$result["to_curr"][$row["id"]]["subj_name"] = $row["subj_name"];
					$result["to_curr"][$row["id"]]["spec_name"] = $row["spec_name"];
					$result["to_curr"][$row["id"]]["spec_code"] = $row["spec_code"];
					$result["to_curr"][$row["id"]]["spec_direction"] = $row["spec_direction"];
				}
				$DB->SetTable("nsau_curriculum", "nc");
				$DB->AddCondFS("nc.subject_id", "=", $from);
				$DB->AddTable("nsau_subject_group", "nsg");
				$DB->AddCondFF("nc.subject_group_id", "=", "nsg.id");
				$DB->AddTable("nsau_subjects", "ns");
				$DB->AddCondFF("nc.subject_id", "=", "ns.id");
				$DB->AddTable("nsau_spec_type", "nst");
				$DB->AddCondFF("nc.spec_type_id", "=", "nst.id");
				$DB->AddTable("nsau_specialities", "nspec");
				$DB->AddCondFF("nst.spec_id", "=", "nspec.id");
				$DB->AddField("nspec.direction", "spec_direction");
				$DB->AddField("nspec.code", "spec_code");
				$DB->AddField("nspec.name", "spec_name");
				$DB->AddField("ns.name", "subj_name");
				$DB->AddField("nsg.name", "sgroup_name");
				$DB->AddField("nc.id", "id");
				$res2 = $DB->Select();
				while($row2 = $DB->FetchAssoc($res2))
				{
					$result["from_curr"][$row2["id"]]["group_name"] = $row2["sgroup_name"];
					$result["from_curr"][$row2["id"]]["subj_name"] = $row2["subj_name"];
					$result["from_curr"][$row2["id"]]["spec_name"] = $row2["spec_name"];
					$result["from_curr"][$row2["id"]]["spec_code"] = $row2["spec_code"];
					$result["from_curr"][$row2["id"]]["spec_direction"] = $row2["spec_direction"];
				}
// �� ����� ����
				$DB->SetTable("izop_curriculum", "nc");
				$DB->AddCondFS("nc.subject_id", "=", $to);
				$DB->AddTable("nsau_subjects", "ns");
				$DB->AddCondFF("nc.subject_id", "=", "ns.id");
				$DB->AddTable("nsau_specialities", "nspec");
				$DB->AddCondFF("nc.spec_id", "=", "nspec.id");
				$DB->AddField("nspec.direction", "spec_direction");
				$DB->AddField("nspec.code", "spec_code");
				$DB->AddField("nspec.name", "spec_name");
				$DB->AddField("ns.name", "subj_name");
				$DB->AddField("nc.semester", "semester");
				$DB->AddField("nc.id", "id");
				$res = $DB->Select();
				while($row = $DB->FetchAssoc($res))
				{
					$result["to_curr_izop"][$row["id"]]["semester"] = $row["semester"];
					$result["to_curr_izop"][$row["id"]]["subj_name"] = $row["subj_name"];
					$result["to_curr_izop"][$row["id"]]["spec_name"] = $row["spec_name"];
					$result["to_curr_izop"][$row["id"]]["spec_code"] = $row["spec_code"];
					$result["to_curr_izop"][$row["id"]]["spec_direction"] = $row["spec_direction"];
				}
				$DB->SetTable("izop_curriculum", "nc");
				$DB->AddCondFS("nc.subject_id", "=", $from);
				$DB->AddTable("nsau_subjects", "ns");
				$DB->AddCondFF("nc.subject_id", "=", "ns.id");
				$DB->AddTable("nsau_specialities", "nspec");
				$DB->AddCondFF("nc.spec_id", "=", "nspec.id");
				$DB->AddField("nspec.direction", "spec_direction");
				$DB->AddField("nspec.code", "spec_code");
				$DB->AddField("nspec.name", "spec_name");
				$DB->AddField("ns.name", "subj_name");
				$DB->AddField("nc.semester", "semester");
				$DB->AddField("nc.id", "id");
				$res = $DB->Select();
				while($row = $DB->FetchAssoc($res))
				{
					$result["from_curr_izop"][$row["id"]]["semester"] = $row["semester"];
					$result["from_curr_izop"][$row["id"]]["subj_name"] = $row["subj_name"];
					$result["from_curr_izop"][$row["id"]]["spec_name"] = $row["spec_name"];
					$result["from_curr_izop"][$row["id"]]["spec_code"] = $row["spec_code"];
					$result["from_curr_izop"][$row["id"]]["spec_direction"] = $row["spec_direction"];
				}
				return $result;
			}
			break;
		}


	}

	function ajax_load_subjects($var)
	{
		global $DB;
		$DB->SetTable($this->db_prefix."departments");
		$DB->AddAltFS("id", "=", $var);
		$DB->AppendAlts();
		$res =  $DB->Select();
		$dep_name = $DB->FetchAssoc($res);
		$DB->SetTable($this->db_prefix."subjects");
		$DB->AddCondFS("department_id", "=", $var);
		$DB->AddCondFS("is_hidden", "=", 0);
		$res =  $DB->Select();
		$qw["department"] = $dep_name["name"];
		while($row = $DB->FetchAssoc($res))
			{
				$qw["subjects"][$row["id"]] = $row["name"]; //��������� ���������� �� ��������� ��������
			}
		$this->output["data"]=$qw;
	}


	function studentModeration() {
		global $DB, $Engine, $Auth;
		if($Engine->OperationAllowed($this->module_id, "nsau.student.moderation", -1, $Auth->usergroup_id)) {
			//$this->studentDeleteOld();
			$DB->SetTable($this->db_prefix."student_auth_temp", "sat");
			$DB->AddTable("auth_users", "au");
			$DB->AddFields(array("sat.id", "sat.last_name", "sat.name", "sat.patronymic", "sat.male", "sat.id_group", "sat.subgroup", "sat.year", "sat.username", "sat.email", "sat.password", "sat.library_card", "sat.user_id", "au.create_time"));
			$DB->AddCondFF("sat.user_id", "=", "au.id");
			$DB->AddOrder("au.create_time", true);
			$DB->AddOrder("sat.last_name");
			$DB->AddCondFP("sat.is_active");
			$res = $DB->Select();
			while($row = $DB->FetchAssoc($res)) {
				$this->output["student_list"][$row['id']]['last_name'] = $row['last_name'];
				$this->output["student_list"][$row['id']]['name'] = $row['name'];
				$this->output["student_list"][$row['id']]['patronymic'] = $row['patronymic'];
				$this->output["student_list"][$row['id']]['male'] = $row['male'];
				$this->output["student_list"][$row['id']]["id_group"] = $row['id_group'];
				$DB->SetTable($this->db_prefix."groups");
				$DB->AddCondFS("id", "=", $row['id_group']);
				$res_group = $DB->Select();
				if($row_group = $DB->FetchAssoc($res_group)) {
					$this->output["student_list"][$row['id']]["group"] = $row_group['name'];
				}
				$this->output["student_list"][$row['id']]['subgroup'] = is_null($row['subgroup']) ? '' : $row['subgroup'];
				$this->output["student_list"][$row['id']]['year'] = $row['year'];
				$this->output["student_list"][$row['id']]['username'] = $row['username'];
				$this->output["student_list"][$row['id']]['email'] = $row['email'];
				$this->output["student_list"][$row['id']]['library_card'] = $row['library_card'];
				$this->output["student_list"][$row['id']]['user_id'] = $row['user_id'];
				$this->output["student_list"][$row['id']]['password'] = $row['password'];
				$this->output["student_list"][$row['id']]['create_time'] = $row['create_time'];
			}
			if(isset($_POST['register'])) {
				if(isset($this->output["student_list"][$_POST["student_temp_id"]])/*$row = $DB->FetchAssoc($res)*/) {
					$DB->SetTable($this->db_prefix."people");
					$DB->AddValue("user_id", $this->output["student_list"][$_POST["student_temp_id"]]['user_id']);
					$DB->AddValue("last_name", $this->output["student_list"][$_POST["student_temp_id"]]["last_name"]);
					$DB->AddValue("name", $this->output["student_list"][$_POST["student_temp_id"]]["name"]);
					$DB->AddValue("patronymic", $this->output["student_list"][$_POST["student_temp_id"]]["patronymic"]);
					$DB->AddValue("male", $this->output["student_list"][$_POST["student_temp_id"]]["male"]);
					$DB->AddValue("library_card", $this->output["student_list"][$_POST["student_temp_id"]]["library_card"]);
					$DB->AddValue("status_id", 8);
					$DB->AddValue("people_cat", 1);
					$DB->AddValue("id_group", $this->output["student_list"][$_POST["student_temp_id"]]["id_group"]);
					if(!empty($this->output["student_list"][$_POST["student_temp_id"]]["subgroup"])) {
						$DB->AddValue("subgroup", $this->output["student_list"][$_POST["student_temp_id"]]["subgroup"]);
					}
					if(!empty($this->output["student_list"][$_POST["student_temp_id"]]["year"])) {
						$DB->AddValue("year", $this->output["student_list"][$_POST["student_temp_id"]]["year"]);
					}
					if($DB->Insert()) {
						$this->output["messages"]["good"][] = 101;
						$DB->SetTable($this->db_prefix."student_auth_temp");
						$DB->AddCondFS("id", "=", $_POST["student_temp_id"]);
						$DB->Delete();

						$DB->SetTable("auth_users");
						$DB->AddCondFS("id", "=", $_POST["user_id"]);
						$DB->AddValue("is_active", 1);
						$DB->AddValue("create_user_id", $Auth->user_id);
						$DB->Update();


						$mail_message_patterns = array(
							'headers' => array(
								'%site_short_name%' => "����<admin@".$_SERVER['HTTP_HOST'].">",
								'%server_name%' => $_SERVER['HTTP_HOST'],
								'%email%' => $this->output["student_list"][$_POST["student_temp_id"]]["email"],
								'%subject%' => SITE_SHORT_NAME
							),
							'subject' => array(
								'%site_short_name%' => SITE_SHORT_NAME
							),
							'content' => array(
								'%last_name%' => $this->output["student_list"][$_POST["student_temp_id"]]["last_name"],
								'%first_name%' => ' '.$this->output["student_list"][$_POST["student_temp_id"]]["name"],
								'%patronymic%' => ' '.$this->output["student_list"][$_POST["student_temp_id"]]["patronymic"],
								'%server_name%' => $_SERVER['HTTP_HOST'],
									'%login%' =>' '.$this->output["student_list"][$_POST["student_temp_id"]]["username"],
								'%password%' => $this->output["student_list"][$_POST["student_temp_id"]]["password"],
								'%group%' => $this->output["student_list"][$_POST["student_temp_id"]]["group"].$this->output["student_list"][$_POST["student_temp_id"]]["subgroup"],
								'%year%' => $this->output["student_list"][$_POST["student_temp_id"]]["year"],
								'%library_card%' => $this->output["student_list"][$_POST["student_temp_id"]]["library_card"]
							),
							'message_student_registr' => array(
								'%site_short_name%' => SITE_SHORT_NAME,
								'%time%' => date('Y-m-d H:i:s'),
								'%content%' => &$this->settings['student_registr_ok']['mess']
							)
						);

						$this->settings['student_registr_ok']['mess'] = str_replace( array_keys($mail_message_patterns['content']), $mail_message_patterns['content'], $this->settings['student_registr_ok']['mess']);

						foreach ($this->settings['notify_settings'] as $ind=>$setting) {
							$this->settings['notify_settings'][$ind] = str_replace( array_keys($mail_message_patterns[$ind]), $mail_message_patterns[$ind], $setting);
						}

						mail($this->output["student_list"][$_POST["student_temp_id"]]["email"], $this->settings['notify_settings']['subject'], $this->settings['notify_settings']['message_student_registr'], $this->settings['notify_settings']['headers'],/* $this->settings['student_registr_ok']['mess'],  */" -f admin@".$_SERVER['HTTP_HOST']."");

						unset($this->output["student_list"][$_POST["student_temp_id"]]);
						CF::Redirect();
					} else {

					}
				}
			} elseif(isset($_POST['delete'])) {
				$DB->SetTable($this->db_prefix."student_auth_temp");
				$DB->AddCondFS("id", "=", $_POST["student_temp_id"]);
				$DB->Delete();

				$DB->SetTable("auth_users");
				$DB->AddCondFS("id", "=", $_POST["user_id"]);
				$DB->Delete();

				$mail_message_patterns = array(
					'headers' => array(
						'%site_short_name%' => "����<admin@".$_SERVER['HTTP_HOST'].">",
						'%server_name%' => $_SERVER['HTTP_HOST'],
						'%email%' => $this->output["student_list"][$_POST["student_temp_id"]]["email"],
						'%subject%' => SITE_SHORT_NAME
					),
					'subject' => array(
						'%site_short_name%' => SITE_SHORT_NAME
					),
					'content' => array(
						'%last_name%' => $this->output["student_list"][$_POST["student_temp_id"]]["last_name"],
						'%first_name%' => ' '.$this->output["student_list"][$_POST["student_temp_id"]]["name"],
						'%patronymic%' => ' '.$this->output["student_list"][$_POST["student_temp_id"]]["patronymic"],
						'%server_name%' => $_SERVER['HTTP_HOST']
					),
					'message_student_registr' => array(
						'%site_short_name%' => SITE_SHORT_NAME,
						'%time%' => date('Y-m-d H:i:s'),
						'%content%' => &$this->settings['student_registr_cansel']['mess']
					)
				);

				$this->settings['student_registr_cansel']['mess'] = str_replace( array_keys($mail_message_patterns['content']), $mail_message_patterns['content'], $this->settings['student_registr_cansel']['mess']);

				foreach ($this->settings['notify_settings'] as $ind=>$setting) {
					$this->settings['notify_settings'][$ind] = str_replace( array_keys($mail_message_patterns[$ind]), $mail_message_patterns[$ind], $setting);
				}

				mail($this->output["student_list"][$_POST["student_temp_id"]]["email"], $this->settings['notify_settings']['subject'], $this->settings['notify_settings']['message_student_registr'], $this->settings['notify_settings']['headers'], " -f admin@".$_SERVER['HTTP_HOST']."");

				unset($this->output["student_list"][$_POST["student_temp_id"]]);
				CF::Redirect();
			}
		}
	}

	function password_generate ($len)
	{
		$chars="qazxswedcvfrtgbnhyujmkiolp1234567890"; 
		$max=$len; 
		$size=StrLen($chars)-1; 
		$new_password=null; 
		while($max--) 
	    $new_password.=$chars[rand(0,$size)]; 
		return $new_password;
	}

	function studentRegistration() 
	{
        global $DB, $Engine;
		$this->output["allowed"] = 1;
		$error = false;
		$DB->SetTable($this->db_prefix."groups");
		$DB->AddOrder("name");
		$res = $DB->Select();
		while($row = $DB->FetchAssoc($res)) {
			$this->output["groups"][] = $row;
		}
		if(isset($_POST['mode']) && $_POST['mode'] == "add_student") {
			if((!isset($_POST['last_name']) || empty($_POST['last_name'])) || (!isset($_POST['first_name']) || empty($_POST['first_name']))) {
				$error = true;
				$this->output["messages"]["bad"][] = 317;
			}
			if(!isset($_POST['group']) || empty($_POST['group'])) {
				$error = true;
				$this->output["messages"]["bad"][] = 314;
			}

			if(!isset($_POST['st_year']) || empty($_POST['st_year'])) {
				$error = true;
				$this->output["messages"]["bad"][] = 328;
			}
			if(!isset($_POST['library_card']) || empty($_POST['library_card'])) {
				$error = true;
				$this->output["messages"]["bad"][] = 329;
			}
			if(!isset($_POST['email']) || empty($_POST['email'])) {
				$error = true;
				$this->output["messages"]["bad"][] = 330;
			} elseif(!CF::ValidateEmail($_POST['email'])) {
				$error = true;
				$this->output["messages"]["bad"][] = 331;
			} else {
				$DB->SetTable("auth_users");
				$DB->AddCondFS("email", "=", $_POST["email"]);
				$res = $DB->Select();
				if($row = $DB->FetchAssoc($res)) {
					$error = true;
					$this->output["messages"]["bad"][] = 332;
				}
			}
			if((!isset($_POST['password1']) || empty($_POST['password1'])) || (!isset($_POST['password2']) || empty($_POST['password2'])) || $_POST['password1'] != $_POST['password2'] || !isset($_POST['username']) || empty($_POST['username'])) {
				$error = true;
				$this->output["messages"]["bad"][] = 319;
			}
			if(isset($_POST['username']) && !empty($_POST['username'])) {
				$DB->SetTable("auth_users");
				$DB->AddCondFS("username", "=", $_POST["username"]);
				$res2 = $DB->Select();
				if($row2 = $DB->FetchAssoc($res2)) {
					$this->output["messages"]["bad"][] = 327;
					$error = true;
				}
			}
			if(!$error) {
				$user_id = $this->insert_user(0, 3);
				if($user_id) {
					$DB->SetTable($this->db_prefix."student_auth_temp");
					$DB->AddValue("last_name", $_POST["last_name"]);
					$DB->AddValue("name", $_POST["first_name"]);
					$DB->AddValue("patronymic", $_POST["patronymic"]);
					$DB->AddValue("male", $_POST["male"]);
					$DB->AddValue("id_group", $_POST["group"]);
					if($_POST["subgroup"] != '0') {
						$DB->AddValue("subgroup", $_POST["subgroup"]);
					}
					$DB->AddValue("year", $_POST["st_year"]);
					$DB->AddValue("username", $_POST["username"]);
					$DB->AddValue("email", $_POST["email"]);
					$DB->AddValue("password", $_POST["password1"]);
					$DB->AddValue("library_card", $_POST["library_card"]);
					$DB->AddValue("user_id", $user_id);
					// die($DB->InsertQuery());
					if($DB->Insert()) {
						$this->output["messages"]["good"][] = 333;
						$group = '';
						$DB->SetTable($this->db_prefix."groups");
						$DB->AddCondFS("id", "=", $_POST["group"]);
						$res_group = $DB->Select();
						if($row_group = $DB->FetchAssoc($res_group)) {
							$group = $row_group['name'];
						}
						$mail_message_patterns = array(
							'headers' => array(
								'%site_short_name%' => "����<admin@".$_SERVER['HTTP_HOST'].">",
								'%server_name%' => $_SERVER['HTTP_HOST'],
								'%email%' => $_POST["email"],
								'%subject%' => SITE_SHORT_NAME
							),
							'subject' => array(
								'%site_short_name%' => SITE_SHORT_NAME
							),
							'content' => array(
								'%last_name%' => $_POST["last_name"],
								'%first_name%' => ' '.$_POST["first_name"],
								'%patronymic%' => ' '.$_POST["patronymic"],
								'%server_name%' => $_SERVER['HTTP_HOST'],
								'%group%' => $group.$_POST["subgroup"],
								'%year%' => $_POST["st_year"],
								'%library_card%' => $_POST["library_card"],
								'%check%' => $Engine->unqueried_uri.'?mode=active_request&id='.$user_id.'&key='.md5($_POST["username"])
							),
							'message_student_registr' => array(
								'%site_short_name%' => SITE_SHORT_NAME,
								'%time%' => date('Y-m-d H:i:s'),
								'%content%' => &$this->settings['student_registr_request']['mess']
							)
						);

						$this->settings['student_registr_request']['mess'] = str_replace( array_keys($mail_message_patterns['content']), $mail_message_patterns['content'], $this->settings['student_registr_request']['mess']);

						foreach ($this->settings['notify_settings'] as $ind=>$setting) {
							$this->settings['notify_settings'][$ind] = str_replace( array_keys($mail_message_patterns[$ind]), $mail_message_patterns[$ind], $setting);
						}

						mail($_POST["email"], $this->settings['notify_settings']['subject'], $this->settings['notify_settings']['message_student_registr'], $this->settings['notify_settings']['headers'], " -f admin@".$_SERVER['HTTP_HOST']."");
					}
					$DB->SetTable("auth_users");
					$DB->AddCondFS("id", "=", $user_id);
					$DB->AddValue("is_active", 0);
					$DB->Update();
					//CF::Redirect();
				}
			} else {
				$this->output["display_variant"]["add_item"] = array(
					'last_name' => $_POST['last_name'],
					'first_name' => $_POST['first_name'],
					'patronymic' => $_POST['patronymic'],
					'male' => $_POST['male'],
					'group' => $_POST['group'],
					'subgroup' => $_POST['subgroup'],
					'st_year' => $_POST['st_year'],
					'username' => $_POST['username'],
					'email' => $_POST['email'],
					'library_card' => $_POST['library_card']
				);
			}
		}
		if(isset($_GET['mode'], $_GET['key']) && $_GET['mode'] == 'active_request') {
			$this->output['active_request'] = 0;
			$DB->SetTable($this->db_prefix."student_auth_temp");
			$DB->AddCondFS("user_id", "=", $_GET['id']);
			$DB->AddCondFN("is_active");
			$res = $DB->Select();
			if($row = $DB->FetchAssoc($res)) {
				if(md5($row['username']) === $_GET['key']) {
					$DB->SetTable($this->db_prefix."student_auth_temp");
					$DB->AddCondFS("user_id", "=", $_GET['id']);
					$DB->AddValue("is_active", 1);
					$DB->Update();
					$this->output['active_request'] = 1;
				}
			}
		}
	}

	function getCurlData($url)
	{
	    $curl = curl_init();
	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($curl, CURLOPT_TIMEOUT, 10);
	    curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16");
	    $curlData = curl_exec($curl);
	    curl_close($curl);
	    return $curlData;
	}

	function studentDeleteOld() {
        global $DB, $Engine;

		$today = new DateTime(date());
		$today->sub(new DateInterval('P3D'));

		$date_limit = $DB->SmartTime($today->format('Y'), $today->format('m'), $today->format('d'), $today->format('H'), $today->format('i'), $today->format('s'));

		$DB->SetTable($this->db_prefix."student_auth_temp", 'st_temp');
		$DB->AddTable("auth_users", 'au');
		$DB->AddCondFF("au.id", "=", 'st_temp.user_id');
		$DB->AddCondFS("au.create_time", "<", $date_limit[0]);
		$DB->AddCondFN("au.is_active");
		//echo $DB->SelectQuery();
		$res = $DB->Select();
		while($row = $DB->FetchAssoc($res)) {
			$DB->SetTable("auth_users");
			$DB->AddCondFS("id", "=", $row['user_id']);
			$DB->Delete();

			$DB->SetTable($this->db_prefix."student_auth_temp");
			$DB->AddCondFS("user_id", "=", $row['user_id']);
			$DB->Delete();
		}
	}

		function search_people($search_query = false) {
		global $DB, $Engine;
		$output = array();
		$request = trim($search_query);
		if($request != "")
		{
			$full_request = mb_strtolower($request);
			$request = explode(" ", $request );
			$DB->SetTable("nsau_people");
            $DB->AddCondFS('status_id', '<>', '9');
			$i=0;
			foreach($request as $req)
			{
				if($i==0)
					$last_name = $req;
				$i++;
				$DB->AddAltFS("last_name", "LIKE", "%".str_replace("�", "�", $req)."%");
				$DB->AddAltFS("last_name", "LIKE", "%".str_replace("�", "�", $req)."%");
				$DB->AddAltFS("name", "LIKE", "%".str_replace("�", "�", $req)."%");
				$DB->AddAltFS("name", "LIKE", "%".str_replace("�", "�", $req)."%");
				$DB->AddAltFS("patronymic", "LIKE", "%".str_replace("�", "�", $req)."%");
				$DB->AddAltFS("patronymic", "LIKE", "%".str_replace("�", "�", $req)."%");
				$DB->AppendAlts();
			}
			$other = array();
			$last0 = array();
			$last1 = array();
			$res = $DB->Select();
			while($row = $DB->FetchAssoc($res)) {
					if($row["status_id"]==7) {
						$DB->SetTable("nsau_spec_abit");
						$DB->AddCondFS("people_id", "=", $row["id"]);
						$abrow = $DB->FetchAssoc($DB->Select());
						if(empty($abrow))
							continue;
					}
					$full_name = trim(str_replace("  ", " ", $row["last_name"]." ".$row["name"]." ".$row["patronymic"]));
					$full_name = mb_strtolower($full_name);
					if (empty($row["photo"]))
						$row["photo"] = "no_photo.jpg";
					if(strpos(strtolower($row["last_name"]), strtolower($last_name))===false)
					{
						$other[] = array(
							"uri" => "/people/".$row["id"]."/",
							"title" => $row["last_name"].' '.$row["name"].' '.$row["patronymic"],
							"pic" =>  "http://".DEFAULT_DOMAIN.HTTP_ROOT.IMAGES_DIR."people/".$row["photo"]."?".filemtime(IMAGES_DIR."people/".$row["photo"]),
							"test" => $this->count_matches($full_name, str_replace("�", "�", $full_request))+$this->count_matches($full_name, str_replace("�", "�", $full_request))
						);
					}
					elseif(strpos(strtolower($row["last_name"]), strtolower($last_name))===0)
					{
						$last0[] = array(
							"uri" => "/people/".$row["id"]."/",
							"title" => $row["last_name"].' '.$row["name"].' '.$row["patronymic"],
							"pic" =>  "http://".DEFAULT_DOMAIN.HTTP_ROOT.IMAGES_DIR."people/".$row["photo"]."?".filemtime(IMAGES_DIR."people/".$row["photo"]),
							"test" => $this->count_matches($full_name, str_replace("�", "�", $full_request))+$this->count_matches($full_name, str_replace("�", "�", $full_request))
						);
					}
					else
					{
						$last1[] = array(
							"uri" => "/people/".$row["id"]."/",
							"title" => $row["last_name"].' '.$row["name"].' '.$row["patronymic"],
							"pic" =>  "http://".DEFAULT_DOMAIN.HTTP_ROOT.IMAGES_DIR."people/".$row["photo"]."?".filemtime(IMAGES_DIR."people/".$row["photo"]),
								"test" => $this->count_matches($full_name, str_replace("�", "�", $full_request))+$this->count_matches($full_name, str_replace("�", "�", $full_request))
						);
					}

					$output = array_merge ($last0,$last1,$other);


			}





		}


		function build_sorter($key) {
			return function ($a, $b) use ($key) {
				if ($a[$key] === $b[$key]) return 0;
					return $a[$key] < $b[$key] ? 1 : -1;
			};
		}
		uasort($output, build_sorter("test"));
		//die(print_r($output));

		$this->output["results"] = $output;
		//return $output;
	}

	function faculty_specialities($faculties_id) {
		global $DB, $Engine;
		$DB->SetTable($this->db_prefix."specialities", "sp");
		$DB->AddCondFS("sp.id_faculty", "=", $faculties_id);
		$DB->AddJoin($this->db_prefix."spec_type.code", "sp.code");
		$DB->AddField("sp.name");
		$DB->AddField("sp.code");
		$DB->AddField($this->db_prefix."spec_type.type");
		$res = $DB->Select();
		while($row = $DB->FetchAssoc($res)) {
			if($row["type"])
				if($row["code"]!="080100" && $row["code"]!="080200") //�������� ����� ��-�� ������� �����. ������ ����� ������ �����.
					$this->output["faculty_spec"][$row["type"]][] = array(
							"name" => $row["name"],
							"code" => $row["code"],
						);
		}
	}

	function faculties_files($faculties_id) {
		global $DB, $Engine;
		$DB->SetTable($this->db_prefix."faculties");
		$DB->AddField("name");
		$DB->AddCondFS("id", "=", $faculties_id);
		$res_fac = $DB->Select();
		$row_fac = $DB->FetchObject($res_fac);

		$DB->SetTable($this->db_prefix."departments");
		$DB->AddField("id");
		$DB->AddField("name");
		$DB->AddCondFS("faculty_id", "=", $faculties_id);
		$res_dep = $DB->Select();
		while($row_dep = $DB->FetchAssoc($res_dep)) {

			$this->dep_subjects($row_dep["id"], null);

			/*$DB->SetTable($this->db_prefix."subjects");
			$DB->AddField("id");
			$DB->AddField("name");
			$DB->AddCondFS("department_id", "=", $row_dep["id"]);
			$res1 = $DB->Select();

			$subject_array = null;
			while($row1 = $DB->FetchAssoc($res1)) {
				$DB->SetTable($this->db_prefix."files_subj");
				$DB->AddField("file_id");
				$DB->AddCondFS("subject_id", "=", $row1["id"]);
				$res2 = $DB->Select();
				$file_array = null;
				while($row2 = $DB->FetchAssoc($res2)) {
					$DB->SetTable($this->db_prefix."files");
					$DB->AddField("id");
					$DB->AddField("name");
					$DB->AddField("descr");
					$DB->AddCondFS("id", "=", $row2["file_id"]);
					$res3 = $DB->Select();
					while($row3 = $DB->FetchAssoc($res3)) {
						$file_array[] = array(
									"file_name" => $row3["name"],
									"file_descr" => $row3["descr"],
									"file_id" => $row3["id"]
								);
					}
				}
				if($file_array) {
					$subject_array[] = array(
													"subject_name" => $row1["name"],
													"file_list" => $file_array
												);
				}
			}
			//$subject_array["department_name"] = $row_dep->name;
			if($subject_array) {
				$this->output["faculties_files"][] = array(
												"faculties_name" => $row_dep["name"],
												"subject_list" => $subject_array
											);
			}	*/
		}
		$this->output["faculties_name"] = $row_fac->name;
	}

	function 
	studentWork($people_id = null) {
		global $DB, $Auth;

		$user_id = $Auth->user_id;

		$DB->SetTable("nsau_students_works");
		$DB->AddCondFS("student_id", "=", $user_id);
		$res = $DB->Select();
		while($row = $DB->FetchAssoc($res)) {
			$DB->SetTable("nsau_subjects");
			$DB->AddCondFS("id", "=", $row["subject"]);
			$subj = $DB->FetchAssoc($DB->Select(1));
			$row["subject"] = $subj["name"];

			$DB->SetTable("nsau_groups");
			$DB->AddCondFS("id", "=", $row["group_id"]);
			$group = $DB->FetchAssoc($DB->Select(1));
			$row["group_name"] = $group["name"];

			$DB->SetTable("nsau_faculties");
			$DB->AddCondFS("id", "=", $group["id_faculty"]);
			$fac = $DB->FetchAssoc($DB->Select(1));
			$row["faculty"] = $fac["name"];

			$DB->SetTable("nsau_specialities");
			$DB->AddCondFS("code", "=", $group["specialities_code"]);
			$spec = $DB->FetchAssoc($DB->Select(1));
			$row["speciality"] = $group["specialities_code"] . "-" . $spec["name"];

			$row["form"] = ($group["form_education"]==1 ? "�����" : ($group["form_education"]==2 ? "�������" : "����-�������"));

			$DB->SetTable("nsau_departments");
			$DB->AddCondFS("id", "=", $row["department_id"]);
			$dep = $DB->FetchAssoc($DB->Select(1));
			$row["department"] = $dep["name"];

			if(!empty($row["teacher_id"])) {
				$DB->SetTable("nsau_people");
				$DB->AddCondFS("id", "=", $row["teacher_id"]);
				$people = $DB->FetchAssoc($DB->Select());
					$row["teacher"] = $people["last_name"]." ".$people["name"]." ".$people["patronymic"];
					$row["teacher_people_id"] = $people["id"];
			} else {
				$row["teacher"] = $row["teacher_name"];
			}

			$DB->SetTable("nsau_files");
			$DB->AddCondFS("sw_id", "=", $row["id"]);
			$DB->AddCondFS("deleted", "=", 0);
			$fres = $DB->Select();
			while($files = $DB->FetchAssoc($fres))
			{
				$row["files"][$files["id"]] = $files["name"];
			}





			$this->output["sw"][$row["work_type"]][$row["id"]] = $row;



		}
		$pnfv = array("��������� ���������������� ������"=>"��������� ������������������ ������", "��������� ������"=>"��������� ������", "�������� ������"=>"�������� ������", "�������� ������"=>"�������� �������", "�������"=>"��������", "����� � ��������" => "������ � ��������", "�������� �� �������� ������" => "�������� �� �������� �������", "�������� �� �������� ������" => "�������� �� �������� ������");
		$DB->SetTable("nsau_file_view");
		$nfvr = $DB->Select();
		while($nfv = $DB->FetchAssoc($nfvr))
			$this->output["nfv"][$nfv["id"]] = $pnfv[$nfv["view_name"]];
	}

	function studentWork_public($people_id = null) {
		global $DB, $Auth;

		$i=$_SERVER['HTTP_REFERER'];
		$i=explode('/', $i);
		$i=array_pop($i);

		$DB->SetTable("nsau_people");
		$DB->AddCondFS("id", "=", $i);
		$i = $DB->FetchAssoc($DB->Select(1));
		$user_id=$i["user_id"];

		$DB->SetTable("nsau_students_works");
		$DB->AddCondFS("student_id", "=", $user_id);
		$res = $DB->Select();
		while($row = $DB->FetchAssoc($res)) {
			$DB->SetTable("nsau_subjects");
			$DB->AddCondFS("id", "=", $row["subject"]);
			$subj = $DB->FetchAssoc($DB->Select(1));
			$row["subject"] = $subj["name"];

			$DB->SetTable("nsau_groups");
			$DB->AddCondFS("id", "=", $row["group_id"]);
			$group = $DB->FetchAssoc($DB->Select(1));
			$row["group_name"] = $group["name"];

			$DB->SetTable("nsau_faculties");
			$DB->AddCondFS("id", "=", $group["id_faculty"]);
			$fac = $DB->FetchAssoc($DB->Select(1));
			$row["faculty"] = $fac["name"];

			$DB->SetTable("nsau_specialities");
			$DB->AddCondFS("code", "=", $group["specialities_code"]);
			$spec = $DB->FetchAssoc($DB->Select(1));
			$row["speciality"] = $group["specialities_code"] . "-" . $spec["name"];

			$row["form"] = ($group["form_education"]==1 ? "�����" : ($group["form_education"]==2 ? "�������" : "����-�������"));

			$DB->SetTable("nsau_departments");
			$DB->AddCondFS("id", "=", $row["department_id"]);
			$dep = $DB->FetchAssoc($DB->Select(1));
			$row["department"] = $dep["name"];

			if(!empty($row["teacher_id"])) {
				$DB->SetTable("nsau_people");
				$DB->AddCondFS("id", "=", $row["teacher_id"]);
				$people = $DB->FetchAssoc($DB->Select());
					$row["teacher"] = $people["last_name"]." ".$people["name"]." ".$people["patronymic"];
					$row["teacher_people_id"] = $people["id"];
			} else {
				$row["teacher"] = $row["teacher_name"];
			}

			$DB->SetTable("nsau_files");
			$DB->AddCondFS("sw_id", "=", $row["id"]);
			$DB->AddCondFS("deleted", "=", 0);
			$fres = $DB->Select();
			while($files = $DB->FetchAssoc($fres))
				$row["files"][$files["id"]] = $files["name"];





			$this->output["sw"][$row["work_type"]][$row["id"]] = $row;



		}
		$pnfv = array("��������� ���������������� ������"=>"��������� ������������������ ������", "��������� ������"=>"��������� ������", "�������� ������"=>"�������� ������", "�������� ������"=>"�������� �������", "�������"=>"��������", "����� � ��������" => "������ � ��������", "�������� �� �������� ������" => "�������� �� �������� �������", "�������� �� �������� ������" => "�������� �� �������� ������");
		$DB->SetTable("nsau_file_view");
		$nfvr = $DB->Select();
		while($nfv = $DB->FetchAssoc($nfvr))
			$this->output["nfv"][$nfv["id"]] = $pnfv[$nfv["view_name"]];
	}

	function studentsWork($id) {
		global $DB, $Auth;

		$DB->SetTable("nsau_people");
		$DB->AddCondFS("id", "=", $id);

		$user_id = $DB->FetchAssoc($DB->Select(1));

		$DB->SetTable("nsau_students_works");
		$DB->AddCondFS("teacher_id", "=", $user_id["id"]);
		$res = $DB->Select();
		while($row = $DB->FetchAssoc($res)) {
			$DB->SetTable("nsau_subjects");
			$DB->AddCondFS("id", "=", $row["subject"]);
			$subj = $DB->FetchAssoc($DB->Select(1));
			$row["subject"] = $subj["name"];



			$DB->SetTable("nsau_people");
			$DB->AddCondFS("user_id", "=", $row["student_id"]);
			$people_id = $DB->FetchAssoc($DB->Select(1));
			$row["student_people_id"] = $people_id["id"];

			if(!empty($row["student_people_id"])) {
				$DB->SetTable("nsau_groups");
				$DB->AddCondFS("id", "=", $row["group_id"]);
				$group = $DB->FetchAssoc($DB->Select(1));
				$row["group_name"] = $group["name"];

				$DB->SetTable("nsau_faculties");
				$DB->AddCondFS("id", "=", $group["id_faculty"]);
				$fac = $DB->FetchAssoc($DB->Select(1));
				$row["faculty"] = $fac["name"];

				$DB->SetTable("nsau_specialities");
				$DB->AddCondFS("code", "=", $group["specialities_code"]);
				$spec = $DB->FetchAssoc($DB->Select(1));
				$row["speciality"] = $group["specialities_code"] . "-" . $spec["name"];

				$row["form"] = ($group["form_education"]==1 ? "�����" : ($group["form_education"]==2 ? "�������" : "����-�������"));

				$DB->SetTable("nsau_departments");
				$DB->AddCondFS("id", "=", $row["department_id"]);
				$dep = $DB->FetchAssoc($DB->Select(1));
				$row["department"] = $dep["name"];

				$DB->SetTable("nsau_people");
				$DB->AddCondFS("user_id", "=", $row["student_id"]);
				$people = $DB->FetchAssoc($DB->Select());
				$row["student"] = $people["last_name"]." ".$people["name"]." ".$people["patronymic"];

			}
			else {
				$row["student"] = $row["student_name"];
			}

			$DB->SetTable("nsau_files");
			$DB->AddCondFS("sw_id", "=", $row["id"]);
			$DB->AddCondFS("deleted", "=", 0);
			$fres = $DB->Select();
			while($files = $DB->FetchAssoc($fres))
				$row["files"][$files["id"]] = $files["name"];






			$this->output["sw"][$row["work_type"]][$row["id"]] = $row;



		}
		$pnfv = array("��������� ���������������� ������"=>"��������� ������������������ ������", "��������� ������"=>"��������� ������", "�������� ������"=>"�������� ������", "�������� ������"=>"�������� �������", "�������"=>"��������");
		$DB->SetTable("nsau_file_view");
		$nfvr = $DB->Select();
		while($nfv = $DB->FetchAssoc($nfvr))
			$this->output["nfv"][$nfv["id"]] = $pnfv[$nfv["view_name"]];
	}

	function showEducation($id) {
		global $DB;
		$DB->SetTable("nsau_people");
		$DB->AddCondFS("id", "=", $_REQUEST["data"]);
		$man = $DB->FetchAssoc($DB->Select());

		$DB->SetTable("nsau_teachers_edu_level");
		$tel = $DB->Select();
		while($tel_row = $DB->FetchAssoc($yel)) 
		{
			$this->output["edu_levels"][$tel_row['id']]= $tel_row['name'];
		}

		$edu = explode(";", $man["education"]);
		$DB->SetTable("nsau_education");
		foreach($edu as $e_id)
			$DB->AddAltFS("id", "=", $e_id);
		$DB->AppendAlts();
		$er = $DB->Select();
		while($edu_row = $DB->FetchAssoc($er)) {
			$edu_row["year_end"] = implode(".", array_reverse(explode("-", $edu_row["year_end"])));
			$this->output["education"][] = $edu_row;
		}

		$DB->SetTable("nsau_teachers_degree");
		$res_post = $DB->Select();
		while($row_post = $DB->FetchAssoc($res_post)) {
			$t[$row_post["id"]] = $row_post["name"];
		}

		$DB->SetTable("nsau_teachers_rank");
		$res_post = $DB->Select();
		while($row_post = $DB->FetchAssoc($res_post)) {
			$r[$row_post["id"]] = $row_post["name"];
		}

		$aedu = explode(";", $man["add_education"]);
		$DB->SetTable("nsau_add_education");
		foreach($aedu as $ae_id)
			$DB->AddAltFS("id", "=", $ae_id);
		$DB->AppendAlts();
		$aer = $DB->Select();
		while($aedu_row = $DB->FetchAssoc($aer)) {
			$aedu_row["date_doc"] = implode(".", array_reverse(explode("-", $aedu_row["date_doc"])));
			$aedu_row["date_rank"] = implode(".", array_reverse(explode("-", $aedu_row["date_rank"])));
			$aedu_row["certificate"] = (!empty($aedu_row["certificate"]) ? $t[$aedu_row["certificate"]] : (!empty($aedu_row["certificate_rank"]) ? $r[$aedu_row["certificate_rank"]] : ""));


			$this->output["addictional_education"][] = $aedu_row;
		}
	}

	function department_curators($department_id) {
		global $DB, $Engine;

		$DB->SetTable($this->db_prefix."groups_curators", "gc");
		$DB->AddTable($this->db_prefix."teachers", "t");
		$DB->AddTable($this->db_prefix."people", "p");
		$DB->AddTable($this->db_prefix."groups", "g");
		$DB->AddCondFF("gc.curator_id", "=", "t.id");
		$DB->AddCondFF("gc.group_id", "=", "g.id");
		$DB->AddCondFF("t.people_id", "=", "p.id");
		$DB->AddField("g.name", "group_name");
		$DB->AddField("p.last_name", "curator_last_name");
		$DB->AddField("p.name", "curator_name");
		$DB->AddField("p.id", "curator_id");
		$DB->AddField("p.patronymic", "curator_patronymic");
    $DB->AddField("t.curator_text", "curator_text");
		$DB->AddCondFS("gc.department_id", "=", $department_id);
    $DB->AddOrder("p.last_name");
    //echo $DB->SelectQuery();
		$res =  $DB->Select();
		while($row = $DB->FetchAssoc($res)) {
			if (!isset($curators_info[$row["curator_id"]])) {
				$curators_info[$row["curator_id"]]["name"] = $row["curator_last_name"]." ".$row["curator_name"]." ".$row["curator_patronymic"];
				$curators_info[$row["curator_id"]]["href"] = "/people/".$row["curator_id"];
        $curators_info[$row["curator_id"]]["text"] = $row["curator_text"];
			}
			$curators_info[$row["curator_id"]]["groups"][] = $row["group_name"];
		}
		$this->output["curators_info"] = $curators_info;

	}

    function department_files($department_id) {
		global $DB, $Engine;
		$DB->SetTable($this->db_prefix."departments");
		$DB->AddField("name");
		$DB->AddCondFS("id", "=", $department_id);
		$res_dep = $DB->Select();
		$row_dep = $DB->FetchObject($res_dep);

		$DB->SetTable($this->db_prefix."subjects");
		$DB->AddField("id");
		$DB->AddField("name");
		$DB->AddCondFS("department_id", "=", $department_id);
		$res1 = $DB->Select();
		while($row1 = $DB->FetchAssoc($res1)) {
			$DB->SetTable($this->db_prefix."files_subj");
			$DB->AddField("file_id");
			$DB->AddCondFS("subject_id", "=", $row1["id"]);
			$res2 = $DB->Select();
			$file_array = null;
			while($row2 = $DB->FetchAssoc($res2)) {
				$DB->SetTable($this->db_prefix."files");
				$DB->AddField("id");
				$DB->AddField("name");
				$DB->AddField("descr");
				$DB->AddCondFS("id", "=", $row2["file_id"]);
				$res3 = $DB->Select();
				while($row3 = $DB->FetchAssoc($res3)) {
					$file_array[] = array(
								"file_name" => $row3["name"],
								"file_descr" => $row3["descr"],
								"file_id" => $row3["id"]
							);
				}
			}
			if($file_array) {
				$this->output["department_files"][] = array(
												"subject_name" => $row1["name"],
												"file_list" => $file_array
											);
			}
		}
		$this->output["department_name"] = $row_dep->name;
	}

    function timetable_graphics($mode)
    {
        global $DB, $Engine;
        if(empty($mode[0]))
        {
            if(!isset($_POST["graphic_name"]))
            {
                $this->output["graphics_list"]=array();
                $DB->SetTable($this->db_prefix."graphics_list");
                $DB->AddOrder("type");
                $res = $DB->Select();
                while($row = $DB->FetchAssoc($res))
                {
                    $this->output["graphics_list"][] = $row;
                }
            }
            else
            {
                $DB->SetTable($this->db_prefix."graphics_list");
                $DB->AddValue("name",$_POST["graphic_name"]);
                $DB->AddValue("type",$_POST["type"]);
                if(isset($_POST["begin_day"]) && isset($_POST["begin_month"]) && isset($_POST["begin_year"]))
                {
                    $DB->AddValue("begin_session",$_POST["begin_year"]."-".$_POST["begin_month"]."-".$_POST["begin_day"]);
                }
                if(isset($_POST["end_day"]) && isset($_POST["end_month"]) && isset($_POST["end_year"]))
                {
                    $DB->AddValue("end_session",$_POST["end_year"]."-".$_POST["end_month"]."-".$_POST["end_day"]);
                }
                $DB->Insert();
                CF::Redirect($Engine->engine_uri);
            }
        }
        else
        {
            if(!empty($mode[0]) && $mode[0] == "edit")
            {
                if(!isset($_POST["graphic_name"]))
                {
                    $DB->SetTable($this->db_prefix."graphics_list");
                    $DB->AddCondFS("id","=",$mode[1]);
                    $res = $DB->Select();
                    if($row = $DB->FetchAssoc($res))
                    {
                        $this->output["graphics_list_data"]=$row;
                    }
                }
                else
                {
                    $DB->SetTable($this->db_prefix."graphics_list");
                    $DB->AddCondFS("id","=",$mode[1]);
                    $DB->AddValue("name",$_POST["graphic_name"]);
                    $DB->AddValue("type",$_POST["type"]);
                    if(isset($_POST["begin_day"]) && isset($_POST["begin_month"]) && isset($_POST["begin_year"]))
                    {
                        $DB->AddValue("begin_session",$_POST["begin_year"]."-".$_POST["begin_month"]."-".$_POST["begin_day"]);
                    }
                    if(isset($_POST["end_day"]) && isset($_POST["end_month"]) && isset($_POST["end_year"]))
                    {
                        $DB->AddValue("end_session",$_POST["end_year"]."-".$_POST["end_month"]."-".$_POST["end_day"]);
                    }
                    $DB->Update();
                    CF::Redirect($Engine->engine_uri);
                }
            }
            if(!empty($mode[0]) && $mode[0] == "delete")
            {
                $DB->Exec("delete from `nsau_groups_graphics` where `id_graphics`=(select `id` from `nsau_graphics` where `id_graphics_list`=".$mode[1].")");
                $DB->Exec("delete from `nsau_hours` where `id_graphics`=(select `id` from `nsau_graphics` where `id_graphics_list`=".$mode[1].")");
                $DB->Exec("delete from `nsau_graphics` where `id_graphics_list`=".$mode[1]);
                $DB->SetTable($this->db_prefix."graphics_list");
                $DB->AddCondFS("id","=",$mode[1]);
                $DB->Delete();
                CF::Redirect($Engine->engine_uri);
            }
            if(empty($mode[1]))
            {
                if(!empty($_POST) && !isset($_POST["subject_id"]))
                {
                    foreach($_POST as $data)
                    {
                        $i=1;
                        foreach($data["l"] as $l)
                        {
                            $DB->SetTable($this->db_prefix."hours");
                            $DB->AddCondFS("id_graphics","=",$data["name"]);
                            $DB->AddCondFS("week_num","=",$i);
                            $DB->AddCondFS("type","=",0);
                            $res = $DB->Select();
                            if($row = $DB->FetchAssoc($res))
                            {
                                if(!empty($l) && $l!=$row["hours_per_week"])
                                {

                                    $DB->SetTable($this->db_prefix."hours");
                                    $DB->AddCondFS("id","=",$row["id"]);
                                    $DB->AddValue("hours_per_week",$l);
                                    $DB->Update();
                                }
                                else
                                {
                                    if(empty($l))
                                    {
                                        $DB->SetTable($this->db_prefix."hours");
                                        $DB->AddCondFS("id","=",$row["id"]);
                                        $DB->Delete();

                                    }
                                }
                            }
                            else
                            {
                                if(!empty($l))
                                {
                                    $DB->SetTable($this->db_prefix."hours");
                                    $DB->AddValue("hours_per_week",$l);
                                    $DB->AddValue("week_num",$i);
                                    $DB->AddValue("id_graphics",$data["name"]);
                                    $DB->AddValue("type",0);
                                    $DB->Insert();
                                }
                            }
                            $i++;
                        }
                        $i=1;
                        foreach($data["p"] as $l)
                        {
                            $DB->SetTable($this->db_prefix."hours");
                            $DB->AddCondFS("id_graphics","=",$data["name"]);
                            $DB->AddCondFS("week_num","=",$i);
                            $DB->AddCondFS("type","=",1);
                            $res = $DB->Select();
                            if($row = $DB->FetchAssoc($res))
                            {
                                if(!empty($l) && $l!=$row["hours_per_week"])
                                {

                                    $DB->SetTable($this->db_prefix."hours");
                                    $DB->AddCondFS("id","=",$row["id"]);
                                    $DB->AddValue("hours_per_week",$l);
                                    $DB->Update();
                                }
                                else
                                {
                                    if(empty($l))
                                    {
                                        $DB->SetTable($this->db_prefix."hours");
                                        $DB->AddCondFS("id","=",$row["id"]);
                                        $DB->Delete();

                                    }
                                }
                            }
                            else
                            {
                                if(!empty($l))
                                {
                                    $DB->SetTable($this->db_prefix."hours");
                                    $DB->AddValue("hours_per_week",$l);
                                    $DB->AddValue("week_num",$i);
                                    $DB->AddValue("id_graphics",$data["name"]);
                                    $DB->AddValue("type",1);
                                    $DB->Insert();
                                }
                            }
                            $i++;
                        }
                    }
                }
                if(isset($_POST["subject_id"]) && isset($_POST["type"]) && isset($_POST["kol_groups"]) && isset($_POST["lecture_hours"]) && isset($_POST["practice_hours"]) && isset($_POST["attestation_type"]) && isset($_POST["groups"]))
                {
                    $DB->SetTable($this->db_prefix."subjects");
                    $DB->AddCondFS("id","=",$_POST["subject_id"]);
                    $res = $DB->Select();
                    if($row = $DB->FetchAssoc($res))
                    {
                        $department_id = $row["department_id"];
                    }
                    $DB->SetTable($this->db_prefix."graphics");
                    $DB->AddValue("id_graphics_list",$mode[0]);
                    $DB->AddValue("department_id",$department_id);
                    $DB->AddValue("subject_id",$_POST["subject_id"]);
                    $DB->AddValue("type",$_POST["type"]);
                    $DB->AddValue("kol_groups",$_POST["kol_groups"]);
                    $DB->AddValue("lecture_hours", $_POST["lecture_hours"]);
                    $DB->AddValue("practice_hours",$_POST["practice_hours"]);
                    $DB->AddValue("attestation_type",$_POST["attestation_type"]);
                    if(isset($_POST["ust_lecture"]))
                    {
                        $DB->AddValue("ust_lecture",$_POST["ust_lecture"]);
                    }
                    if(isset($_POST["ust_practice"]))
                    {
                        $DB->AddValue("ust_practice",$_POST["ust_practice"]);
                    }
                    if(isset($_POST["Contr"]))
                    {
                        if($_POST["Contr"] == "on")
                            $DB->AddValue("Contr",1);
                    }
                    if(isset($_POST["Curse"]))
                    {
                        if($_POST["Curse"]=="on")
                            $DB->AddValue("Curse",1);
                    }
                    $DB->Insert();
                    $tmp = str_replace(" ", "", $_POST["groups"]);
                    $groups = explode(";",$tmp);
                    $max_id = $DB->Exec("Select max(`id`) from `nsau_graphics`");
                    if($row3 = $DB->FetchAssoc($max_id))
                    {
                        foreach($row3 as $r)
                        {
                            for($i=0; $i<count($groups)-1; $i++)
                            {
                                $res = $DB->Exec("insert into `nsau_groups_graphics` set `id_group` = (select `id` from `nsau_groups` where `name`=".$groups[$i]."), `id_graphics` = ".$r);
                            }
                        }
                    }

                }

                $DB->SetTable($this->db_prefix."graphics_list");
                $DB->AddCondFS("id","=",$mode[0]);
                $res1 = $DB->Select();
                if($row1 = $DB->FetchAssoc($res1))
                {
                    $this->output["g_name"]=$row1["name"];
                    $this->output["begin_session"]=$row1["begin_session"];
                    $this->output["end_session"]=$row1["end_session"];
                    $this->output["g_id"]=$mode[0];
                    $this->output["uri"]=$Engine->engine_uri;
                    $this->output["g_type"]=$row1["type"];
                }
                $this->output["graphics_data"]=array();

                $DB->SetTable($this->db_prefix."dates_for_weeks");
                $dates = $DB->Select();
                while($d = $DB->FetchAssoc($dates))
                {
                    $this->output["dates"][]=$d;
                }



                $DB->SetTable($this->db_prefix."graphics","g");
                $DB->AddTable($this->db_prefix."departments","d");
                $DB->AddTable($this->db_prefix."subjects","s");
                $DB->AddTable($this->db_prefix."faculties","f");
                $DB->AddCondFS("g.id_graphics_list","=",$mode[0]);
                $DB->AddCondFF("g.department_id","=","d.id");
                $DB->AddCondFF("d.faculty_id","=","f.id");
                $DB->AddCondFF("g.subject_id","=","s.id");
                $DB->AddField("g.id","id_graphic");
                $DB->AddField("d.name","department_name");
                $DB->AddField("d.id","department_id");
                $DB->AddField("f.id","faculty_id");
                $DB->AddField("f.name","faculty_name");
                $DB->AddField("f.short_name","faculty_short_name");
                $DB->AddField("s.name","subject_name");
                $DB->AddField("s.id","subject_id");
                $DB->AddField("g.type","type");
                $DB->AddField("g.kol_groups","kol_groups");
                $DB->AddField("g.lecture_hours","lecture_hours");
                $DB->AddField("g.practice_hours","practice_hours");
                $DB->AddField("g.ust_lecture","ust_lecture");
                $DB->AddField("g.ust_practice","ust_practice");
                $DB->AddField("g.Contr","Contr");
                $DB->AddField("g.Curse","Curse");
                $DB->AddField("g.attestation_type","attestation_type");
                $res = $DB->Select();
                while($row = $DB->FetchAssoc($res))
                {
                    $row["pr"]=array();
                    $row["lec"]=array();
                    $DB->SetTable($this->db_prefix."hours");
                    $DB->AddCondFS("id_graphics","=",$row["id_graphic"]);
                    $DB->AddOrder("id_graphics");
                    $DB->AddOrder("week_num");
                    $res2 = $DB->Select();
                    while($row2 = $DB->FetchAssoc($res2))
                    {
                        if($row2["type"])
                            $row["pr"][] = $row2;
                        else
                            $row["lect"][] = $row2;
                    }
                    $res1 = $DB->Exec("SELECT count(`nsau_people`.`id`),
    `nsau_groups_graphics`.`id_group`, `nsau_groups`.`name`  FROM `nsau_people`, `nsau_groups_graphics`, `nsau_groups` WHERE `nsau_people`.`id_group` = `nsau_groups_graphics`.`id_group` and `nsau_groups`.`id`=`nsau_groups_graphics`.`id_group` and `nsau_groups_graphics`.`id_graphics` =".$row["id_graphic"]." group by `nsau_groups`.`name`");
                    $sum = 0;
                    while($row1 = $DB->FetchAssoc($res1))
                    {
                            $row["groups"][]=array("group_id" => $row1["id_group"], "group_name" => $row1["name"]);
                            $sum += $row1["count(`nsau_people`.`id`)"];
                    }
                    $row["sum_students"]=$sum;
                    $this->output["graphics_data"][]=$row;
                }
            }
            else
            {
                if($mode[1] == "calendar")
                {
                    if(isset($_POST["end_date"]) && isset($_POST["begin_date"]))
                    {
                        foreach($_POST["begin_date"] as $date)
                        {
                            $DB->SetTable($this->db_prefix."dates_for_weeks");
                            $DB->AddCondFS("num_week","=",$date["num_week"]);
                            $DB->AddValue("begin_date",$date["year"]."-".$date["month"]."-".$date["day"]);
                            $DB->Update();
                        }
                        foreach($_POST["end_date"] as $date)
                        {
                            $DB->SetTable($this->db_prefix."dates_for_weeks");
                            $DB->AddCondFS("num_week","=",$date["num_week"]);
                            $DB->AddValue("end_date",$date["year"]."-".$date["month"]."-".$date["day"]);
                            $DB->Update();
                        }
                    }
                    //CF::Debug($_POST);
                    $this->output["calendar"] = array();
                    $DB->SetTable($this->db_prefix."dates_for_weeks");
                    $DB->AddOrder("num_week");
                    $res = $DB->Select();
                    while($row = $DB->FetchAssoc($res))
                    {
                        $this->output["calendar_data"][] = $row;
                    }
                    $this->output["calendar"]["back_link"]=$Engine->engine_uri.$mode[0]."/";
                }
                if($mode[1] == "edit")
                {
                    if(!isset($_POST["subject_id"]))
                    {
                        $DB->SetTable($this->db_prefix."graphics","g");
                        $DB->AddTable($this->db_prefix."departments","d");
                        $DB->AddTable($this->db_prefix."subjects","s");
                        $DB->AddTable($this->db_prefix."faculties","f");
                        $DB->AddTable($this->db_prefix."graphics_list","gl");
                        $DB->AddCondFS("gl.id","=",$mode[0]);
                        $DB->AddCondFS("g.id","=",$mode[2]);
                        $DB->AddCondFF("g.department_id","=","d.id");
                        $DB->AddCondFF("d.faculty_id","=","f.id");
                        $DB->AddCondFF("g.subject_id","=","s.id");
                        $DB->AddField("gl.name","g_name");
                        $DB->AddField("gl.type","g_type");
                        $DB->AddField("g.id","id_graphic");
                        $DB->AddField("d.name","department_name");
                        $DB->AddField("d.id","department_id");
                        $DB->AddField("f.id","faculty_id");
                        $DB->AddField("f.name","faculty_name");
                        $DB->AddField("f.short_name","faculty_short_name");
                        $DB->AddField("s.name","subject_name");
                        $DB->AddField("s.id","subject_id");
                        $DB->AddField("g.type","type");
                        $DB->AddField("g.kol_groups","kol_groups");
                        $DB->AddField("g.lecture_hours","lecture_hours");
                        $DB->AddField("g.practice_hours","practice_hours");
                        $DB->AddField("g.ust_lecture","ust_lecture");
                        $DB->AddField("g.ust_practice","ust_practice");
                        $DB->AddField("g.Contr","Contr");
                        $DB->AddField("g.Curse","Curse");
                        $DB->AddField("g.attestation_type","attestation_type");
                        $res = $DB->Select();
                        if($row = $DB->FetchAssoc($res))
                        {
                            $res1 = $DB->Exec("SELECT count(`nsau_people`.`id`),
        `nsau_groups_graphics`.`id_group`, `nsau_groups`.`name`  FROM `nsau_people`, `nsau_groups_graphics`, `nsau_groups` WHERE `nsau_people`.`id_group` = `nsau_groups_graphics`.`id_group` and `nsau_groups`.`id`=`nsau_groups_graphics`.`id_group` and `nsau_groups_graphics`.`id_graphics` =".$mode[2]." group by `nsau_groups`.`name`");
                            $sum = 0;
                            while($row1 = $DB->FetchAssoc($res1))
                            {
                                    $row["groups"][]=array("group_id" => $row1["id_group"], "group_name" => $row1["name"]);
                                    $sum += $row1["count(`nsau_people`.`id`)"];
                            }
                            $row["sum_students"]=$sum;
                            $this->output["edit_graphic"]=$row;
                        }
                    }
                    else
                    {
                        if(isset($_POST["type"]) && isset($_POST["kol_groups"]) && isset($_POST["lecture_hours"]) && isset($_POST["practice_hours"]) && isset($_POST["attestation_type"]) && isset($_POST["groups"]))
                        {
                            $DB->SetTable($this->db_prefix."subjects");
                            $DB->AddCondFS("id","=",$_POST["subject_id"]);
                            $res = $DB->Select();
                            if($row = $DB->FetchAssoc($res))
                            {
                                $department_id = $row["department_id"];
                            }
                            $DB->SetTable($this->db_prefix."graphics");
                            $DB->AddCondFS("id","=",$mode[2]);
                            $DB->AddValue("id_graphics_list",$mode[0]);
                            $DB->AddValue("department_id",$department_id);
                            $DB->AddValue("subject_id",$_POST["subject_id"]);
                            $DB->AddValue("type",$_POST["type"]);
                            $DB->AddValue("kol_groups",$_POST["kol_groups"]);
                            $DB->AddValue("lecture_hours", $_POST["lecture_hours"]);
                            $DB->AddValue("practice_hours",$_POST["practice_hours"]);
                            $DB->AddValue("attestation_type",$_POST["attestation_type"]);
                            $DB->AddValue("ust_lecture",$_POST["ust_lecture"]);
                            $DB->AddValue("ust_practice",$_POST["ust_practice"]);
                            if(isset($_POST["Contr"]) && $_POST["Contr"]=="on")
                            {
                                $DB->AddValue("Contr",1);
                            }
                            else
                            {
                                $DB->AddValue("Contr",0);
                            }
                            if(isset($_POST["Curse"]) && $_POST["Curse"]=="on")
                            {
                                $DB->AddValue("Curse",1);
                            }
                            else
                            {
                                $DB->AddValue("Curse",0);
                            }
                            $DB->Update();
                            $tmp = str_replace(" ", "", $_POST["groups"]);
                            $groups = explode(";",$tmp);
                            $max_id = $DB->Exec("Select max(`id`) from `nsau_graphics`");
                            for($i=0; $i<count($groups)-1; $i++)
                            {
                                $res = $DB->Exec("delete from `nsau_groups_graphics` where `id_graphics` = ".$mode[2]);
                            }
                            for($i=0; $i<count($groups)-1; $i++)
                            {
                                $res = $DB->Exec("insert into `nsau_groups_graphics` set `id_group` = (select `id` from `nsau_groups` where `name`=".$groups[$i]."), `id_graphics` = ".$mode[2]);
                            }
                            CF::Redirect($Engine->engine_uri.$mode[0]."/");
                        }
                    }
                }
                else
                {
                    if($mode[1] == "delete")
                    {
                        $DB->SetTable($this->db_prefix."groups_graphics");
                        $DB->AddCondFS("id_graphics","=",$mode[2]);
                        $DB->Delete();
                        $DB->SetTable($this->db_prefix."hours");
                        $DB->AddCondFS("id_graphics","=",$mode[2]);
                        $DB->Delete();
                        $DB->SetTable($this->db_prefix."graphics");
                        $DB->AddCondFS("id","=",$mode[2]);
                        $DB->Delete();
                        CF::Redirect($Engine->engine_uri.$mode[0]."/");
                    }
                }
            }
            $DB->SetTable($this->db_prefix."subjects","s");
            $DB->AddTable($this->db_prefix."departments","d");
            $DB->AddTable($this->db_prefix."faculties","f");
            $DB->AddCondFF("s.department_id","=","d.id");
            $DB->AddCondFF("f.id","=","d.faculty_id");
            $DB->AddOrder("s.department_id");
            $DB->AddOrder("d.faculty_id");
            $DB->AddField("s.id","subject_id");
            $DB->AddField("s.name","subject_name");
            $DB->AddField("d.id","department_id");
            $DB->AddField("d.name","department_name");
            $DB->AddField("d.faculty_id","faculty_id");
            $DB->AddField("f.name","faculty_name");
            $DB->AddField("f.short_name","faculty_short_name");
            $res = $DB->Select();
            while($row = $DB->FetchAssoc($res))
            {
                $this->output["subjects"][] = $row;
            }
        }
    }

    function timetable_subjects_by_departments($mode)
    {
        global $DB, $Engine;
        if(empty($mode[0])) {
            $DB->SetTable($this->db_prefix."departments","d");
            $DB->AddTable($this->db_prefix."faculties","f");
            $DB->AddOrder("d.faculty_id");

            $DB->AddCondFF("d.faculty_id","=","f.id");
            $DB->AddField("d.name","name");
            $DB->AddField("d.id","id");
            $DB->AddField("f.id","faculty_id");
            $DB->AddField("f.name","faculty_name");
            $res =  $DB->Select();
            while($row = $DB->FetchAssoc($res)) {
                $this->output["departments_list"][$row['faculty_id']]['fname'] = $row['faculty_name'];
                $this->output["departments_list"][$row['faculty_id']]['dep_list'][$row['id']] = $row['name'];
            }
            $DB->SetTable($this->db_prefix."faculties");
            $res = $DB->Select();
            while($row = $DB->FetchAssoc($res)) {
                $this->output["faculties"][] = $row;
            }
        } else
        {
            if($mode[0]=="show" && empty($mode[2]))
            {
                if(isset($_POST["subject"]))
                {
                    $DB->SetTable($this->db_prefix."subjects_by_departments");
                    $DB->AddValue("subject_id",$_POST["subject"]);
                    $DB->AddValue("faculty_id",$_POST["faculty"]);
                    $DB->AddValue("department_id",$mode[1]);
                    $DB->AddValue("year",$_POST["year"]);
                    $DB->AddValue("type",$_POST["type"]);
                    $DB->AddValue("teacher_id",$_POST["teacher_id"]);
                    $DB->AddValue("groups",$_POST["groups"]);
                    $DB->AddValue("subgroups",$_POST["subgroups"]);
                    $DB->AddValue("auditorium",$_POST["auditorium"]);
                    if(isset($_POST["rectors"]))
                        $DB->AddValue("participation_rectors",1);
                    else
                        $DB->AddValue("participation_rectors",0);
                    if(isset($_POST["prorectors"]))
                        $DB->AddValue("participation_prorectors",1);
                    else
                        $DB->AddValue("participation_prorectors",0);
                    if(isset($_POST["decanats"]))
                        $DB->AddValue("participation_decanats",1);
                    else
                        $DB->AddValue("participation_decanats",0);
                    if(isset($_POST["academic_senate"]))
                        $DB->AddValue("participation_academic_senate",1);
                    else
                    if(isset($_POST["date_from"]))
                        $DB->AddValue("date_from", $_POST["date_from"]);
                    if(isset($_POST["date_to"]))
                        $DB->AddValue("date_to", $_POST["date_to"]);
                    $DB->AddValue("state",$_POST["state"]);
                    if(isset($_POST["rate"]))
                        $DB->AddValue("rate",$_POST["rate"]);
                    if(isset($_POST["comments"]))
                        $DB->AddValue("comments",$_POST["comments"]);
                    $DB->Insert();
                }
                $this->output["show_mode"]="show";
                $DB->SetTable($this->db_prefix."subjects");
                $DB->AddCondFS("department_id","=",$mode[1]);
                $res = $DB->Select();
                while($row = $DB->FetchAssoc($res))
                {
                    $this->output["subjects_list"][] = $row;
                }
                $DB->SetTable($this->db_prefix."teachers","t");
                $DB->AddTable($this->db_prefix."people","p");
                $DB->AddCondFS("t.department_id","=",$mode[1]);
                $DB->AddAltFF("t.people_id","=","p.id");
                $DB->AddField("p.last_name","last_name");
                $DB->AddField("p.name","name");
                $DB->AddField("p.patronymic","patronymic");
                $DB->AddField("p.id","id");
                $res = $DB->Select();
                while($row = $DB->FetchAssoc($res))
                {
                    $this->output["teachers_list"][] = $row;
                }
                $DB->SetTable($this->db_prefix."faculties");
                $res = $DB->Select();

                while($row = $DB->FetchAssoc($res))
                    $this->output["faculty_list"][] = $row;
                $DB->SetTable($this->db_prefix."subjects_by_departments","sbd");
                $DB->AddTable($this->db_prefix."subjects","s");
                $DB->AddTable($this->db_prefix."people","p");
                $DB->AddTable($this->db_prefix."faculties","f");
                $DB->AddCondFF("sbd.faculty_id","=","f.id");
                $DB->AddCondFF("sbd.teacher_id","=","p.id");
                $DB->AddCondFF("sbd.subject_id","=","s.id");
                $DB->AddCondFS("sbd.department_id","=",$mode[1]);
                $DB->AddField("sbd.id","id");
                $DB->AddField("sbd.subject_id","subject_id");
                $DB->AddField("s.name","subject");
                $DB->AddField("sbd.faculty_id","faculty_id");
                $DB->AddField("f.name","faculty");
                $DB->AddField("f.short_name","faculty_short");
                $DB->AddField("sbd.department_id","department_id");
                $DB->AddField("sbd.year","year");
                $DB->AddField("sbd.teacher_id","teacher_id");
                $DB->AddField("p.last_name","last_name");
                $DB->AddField("p.name","name");
                $DB->AddField("p.patronymic","patronymic");
                $DB->AddField("sbd.type","type");
                $DB->AddField("sbd.groups","groups");
                $DB->AddField("sbd.subgroups","subgroups");
                $DB->AddField("sbd.auditorium","auditorium");
                $DB->AddField("sbd.participation_rectors","rectors");
                $DB->AddField("sbd.participation_prorectors","prorectors");
                $DB->AddField("sbd.participation_decanats","decanats");
                $DB->AddField("sbd.participation_academic_senate","academic_senate");
                $DB->AddField("sbd.comments","comments");
                $DB->AddField("sbd.state","state");
                $DB->AddField("sbd.rate","rate");
                $DB->AddField("sbd.date_from","date_from");
                $DB->AddField("sbd.date_to","date_to");
                $res = $DB->Select();
                while($row = $DB->FetchAssoc($res))
                    $this->output["subjects_by_departments_list"][] = $row;
                $DB->SetTable($this->db_prefix."departments");
                $DB->AddCondFS("id","=",$mode[1]);
                $res = $DB->Select();
                if($row = $DB->FetchAssoc($res))
                    $this->output["department_name"]=$row["name"];
            }
            if(!empty($mode[2]) && $mode[2] == "edit")
            {
                if(!isset($_POST["id"]))
                {
                    $this->output["show_mode"]="edit";
                    $DB->SetTable($this->db_prefix."subjects_by_departments");
                    $DB->AddCondFS("id","=",$mode[3]);
                    $res = $DB->Select();
                    if($row = $DB->FetchAssoc($res))
                        $this->output["edit_row"] = $row;
                    $DB->SetTable($this->db_prefix."subjects");
                    $DB->AddCondFS("department_id","=",$mode[1]);
                    $res = $DB->Select();
                    while($row = $DB->FetchAssoc($res))
                    {
                        $this->output["subjects_list"][] = $row;
                    }
                    $DB->SetTable($this->db_prefix."teachers","t");
                    $DB->AddTable($this->db_prefix."people","p");
                    $DB->AddCondFS("t.department_id","=",$mode[1]);
                    $DB->AddAltFF("t.people_id","=","p.id");
                    $DB->AddField("p.last_name","last_name");
                    $DB->AddField("p.name","name");
                    $DB->AddField("p.patronymic","patronymic");
                    $DB->AddField("p.id","id");
                    $res = $DB->Select();
                    while($row = $DB->FetchAssoc($res))
                    {
                        $this->output["teachers_list"][] = $row;
                    }
                    $DB->SetTable($this->db_prefix."faculties");
                    $res = $DB->Select();
                    while($row = $DB->FetchAssoc($res))
                        $this->output["faculty_list"][] = $row;
                }
                else
                {
                    $DB->SetTable($this->db_prefix."subjects_by_departments");
                    $DB->AddCondFS("id","=",$_POST["id"]);
                    $DB->AddValue("subject_id",$_POST["subject"]);
                    $DB->AddValue("faculty_id",$_POST["faculty"]);
                    $DB->AddValue("department_id",$mode[1]);
                    $DB->AddValue("year",$_POST["year"]);
                    $DB->AddValue("type",$_POST["type"]);
                    $DB->AddValue("teacher_id",$_POST["teacher_id"]);
                    $DB->AddValue("groups",$_POST["groups"]);
                    $DB->AddValue("subgroups",$_POST["subgroups"]);
                    $DB->AddValue("auditorium",$_POST["auditorium"]);
                    if(isset($_POST["rectors"]))
                        $DB->AddValue("participation_rectors",1);
                    else
                        $DB->AddValue("participation_rectors",0);
                    if(isset($_POST["prorectors"]))
                        $DB->AddValue("participation_prorectors",1);
                    else
                        $DB->AddValue("participation_prorectors",0);
                    if(isset($_POST["decanats"]))
                        $DB->AddValue("participation_decanats",1);
                    else
                        $DB->AddValue("participation_decanats",0);
                    if(isset($_POST["academic_senate"]))
                        $DB->AddValue("participation_academic_senate",1);
                    else
                        $DB->AddValue("participation_academic_senate",0);
                    if(isset($_POST["date_from"]))
                        $DB->AddValue("date_from", $_POST["date_from"]);
                    if(isset($_POST["date_to"]))
                        $DB->AddValue("date_to", $_POST["date_to"]);
                    $DB->AddValue("state",$_POST["state"]);
                    if(isset($_POST["rate"]))
                        $DB->AddValue("rate",$_POST["rate"]);
                    if(isset($_POST["comments"]))
                        $DB->AddValue("comments",$_POST["comments"]);
                    $DB->Update();
                    CF::Redirect($Engine->engine_uri.$mode[0]."/".$mode[1]."/");
                }
            }
            if(!empty($mode[2]) && $mode[2] == "delete")
            {
                $DB->SetTable($this->db_prefix."subjects_by_departments");
                $DB->AddCondFS("id","=",$mode[3]);
                $DB->Delete();
                CF::Redirect($Engine->engine_uri.$mode[0]."/".$mode[1]."/");
            }
        }
    }

    function edit_group($mode)							/*++++++++++++++++++++++++++++++++++++++++++++*/
    {
        global $DB, $Engine;
        if($mode[1] == "delete_group")
        {
            $DB->SetTable($this->db_prefix."people");
            $DB->AddCondFS("id_group", "=", $mode[0]);
            $DB->AddCondFS("status_id","<>","4");
            $res = $DB->Select();
            if(!($row = $DB->FetchAssoc($res)))
            {
                $DB->SetTable($this->db_prefix."groups");
                $DB->AddCondFS("id", "=", $mode[0]);
                $DB->Delete();
                $Engine->LogAction($this->module_id, "group", $mode[0], "delete");
                CF::Redirect($Engine->engine_uri);
            }
            else
            {
                $this->output["messages"]["bad"][] = 305;
                CF::Redirect($Engine->engine_uri."/".$mode[0]."/");
            }
        }
        else
        {
            if(!isset($_POST["id"]))
                {
                    $DB->SetTable($this->db_prefix."groups");
                    $DB->AddCondFS("id", "=", $mode[0]);
                    $res = $DB->Select();
                    if($row = $DB->FetchAssoc($res))
                    {
                        $DB->SetTable($this->db_prefix."faculties");
                        $DB->AddCondFS("id", "=", $row["id_faculty"]);
                        $res1 = $DB->Select();
                        if($row1 = $DB->FetchAssoc($res1))
                            $row["faculty_name"] = $row1["name"];
                        $DB->SetTable($this->db_prefix."faculties");
                        $res1 = $DB->Select();
                        while($row2 = $DB->FetchAssoc($res1))
                        {
                            $this->output["faculties_edit_group"][]=$row2;
                        }
                        $this->output["edit_group"][]=$row;
                    }
                }
                else
                {
                    $DB->SetTable($this->db_prefix."groups");
                    $DB->AddCondFS("id", "=", $_POST["id"]);
                    $DB->AddValue("name", $_POST["group_name"]);
                    $DB->AddValue("id_faculty", $_POST["faculty"]);
                    $DB->AddValue("year", $_POST["year"]);
                    $DB->Update();
                    $Engine->LogAction($this->module_id, "group", $mode[0], "edit");
                    $mode[0]=NULL;
                    CF::Redirect($Engine->engine_uri);
                }
        }
    }


    function add_group()							/*++++++++++++++++++++++++++++++++++++++++++++++++++*/
    {
        global $DB, $Engine;
        if(isset($_POST["group_name"]))
        {
            $DB->SetTable($this->db_prefix."groups");
            $DB->AddCondFS("name","=",$_POST["group_name"]);
            $res = $DB->Select();
            if($row = $DB->FetchAssoc($res))
            {
                $this->output["messages"]["bad"][] = 304;
            }
            else
            {
                $DB->SetTable($this->db_prefix."groups");
                $DB->AddValue("name", $_POST["group_name"]);
                $DB->AddValue("id_faculty", $_POST["faculty"]);
                $DB->AddValue("year", $_POST["year"]);
                $DB->Insert();
                $Engine->LogAction($this->module_id, "group", $DB->LastInsertID(), "delete");
                CF::Redirect($Engine->engine_uri);
            }
        }
        $DB->SetTable($this->db_prefix."faculties");
        $res = $DB->Select();
        while($row = $DB->FetchAssoc($res))
        {
            $this->output["faculty_list"][] = $row;
        }
    }

    function update_user($row, $group_id = false)
    {
        global $DB;
        //CF::Debug($_POST);
        if(isset($_POST["username"], $_POST["password1"], $_POST["email"]) && $_POST["username"] && $_POST["password1"] == $_POST["password2"] ) {
								// die($_POST["password1"]);
            $DB->SetTable("auth_users");
            $DB->AddCondFS("id","=",$row["user_id"]);
            $res3 = $DB->Select();
            if($row3 = $DB->FetchAssoc($res3)) {
                $DB->SetTable("auth_users");
                $DB->AddCondFS("id","=",$row["user_id"]);
        		if ($_POST["email"]) $DB->AddValue("email", $_POST["email"]);
                $DB->AddValue("username", $_POST["username"]);
                if ($group_id) $DB->AddValue("usergroup_id",$group_id);
                $DB->AddValue("displayed_name", $_POST["last_name"]." ".$_POST["first_name"]." ".$_POST["patronymic"]);
                //$DB->AddValue("displayed_name", $_POST["first_name"]." ".$_POST["patronymic"]);
                if( isset($_POST["password2"]) && $_POST["password1"] && $_POST["password1"] == $_POST["password2"])
                {
                    $DB->AddValue("password",md5($_POST["password1"]));
                }

                $DB->Update();
								if(!empty($_POST["password1"]))
									$this->sendNewPasswd($_POST["email"], $_POST["password1"], $_POST["username"]);
            } else {
            	if (!$group_id) $group_id = 2;
				$this->insert_user($row["id"], $group_id);
			}
        }
    }

    function insert_user($id, $group_id)
    {
        global $DB, $Auth;
        if(isset($_POST["username"], $_POST["password1"], $_POST["email"]) && $_POST["username"] && $_POST["password1"] ) {
			/*$DB->SetTable("auth_users");
			$DB->AddCondFS("email", "=", $_POST["email"]);
			$res = $DB->Select();
			if($row = $DB->FetchAssoc($res)) {
				return false;
			} else */
			{
				$DB->SetTable("auth_users");
				$DB->AddValue("email", $_POST["email"]);
				$DB->AddValue("username", $_POST["username"]);
				$DB->AddValue("usergroup_id",$group_id);
				$DB->AddValue("create_user_id",$Auth->user_id);
				$DB->AddValue("displayed_name", $_POST["last_name"]." ".$_POST["first_name"]." ".$_POST["patronymic"]);
        $DB->AddValue("create_time", "NOW()", "x");
				//$DB->AddValue("displayed_name", $_POST["first_name"]." ".$_POST["patronymic"]);
				if( isset($_POST["password2"]) && $_POST["password1"] == $_POST["password2"]) {
				   $DB->AddValue("password",md5($_POST["password1"]));
				}
				if($DB->Insert()) {
					$last_id = $DB->LastInsertID();
					//$DB->SetTable("auth_users");
					//$DB->AddCondFS("email","=",$_POST["email"]);
					//$res4 = $DB->Select();
					$res4 = $DB->Exec("SELECT max(LAST_INSERT_ID(id)) as 'id' FROM auth_users limit 1");

					if(/*$row4 = $DB->FetchAssoc($res4)*/$last_id) {
						$DB->SetTable($this->db_prefix."people");
						$DB->AddCondFS("id","=",$id);
						$DB->AddValue("user_id",/*$row4["id"]*/$last_id);
						$DB->Update();
						return /*$row4["id"]*/$last_id;
					}
				}
			}
        }
		return false;
    }

    //�������������� �������� � id=$mode[1]
    function edit_student($mode)
    {
        global $DB, $Engine;
        if(isset($mode[2]) && $mode[2] == "expulsion")
        {
            $DB->SetTable($this->db_prefix."people");
            $DB->AddCondFS("id", "=", $mode[1]);
            $DB->AddValue("status_id","4");
            $DB->Update();
            CF::Redirect($Engine->engine_uri.$mode[0]."/");
        }
        if(isset($_POST["last_name"]))
        {
            $DB->SetTable($this->db_prefix."people");
            $DB->AddCondFS("id", "=", $mode[1]);
            $DB->AddValue("last_name",$_POST["last_name"]);
            $DB->AddValue("name",$_POST["first_name"]);
            $DB->AddValue("patronymic", $_POST["patronymic"]);
            $DB->AddValue("male", $_POST["male"]);
            $DB->AddValue("year", $_POST["year"]);
            $DB->AddValue("id_group",$_POST["group"]);
            $DB->AddValue("subgroup", $_POST["subgroup"]);
            //$DB->AddValue("id_speciality", $_POST["id_speciality"]);
            $DB->Update();
            $DB->SetTable($this->db_prefix."people");
            $DB->AddCondFS("id", "=", $mode[1]);
            $res = $DB->Select();
			$row = $DB->FetchAssoc($res);
            if($row["user_id"])
            {
                /*if(isset($_POST["username"]) && isset($_POST["email"]))
                {
                $DB->SetTable("auth_users");
                $DB->AddCondFS("id","=",$row["user_id"]);
                $res3 = $DB->Select();
                if($row3 = $DB->FetchAssoc($res3))
                {
                    $DB->SetTable("auth_users");
                    $DB->AddCondFS("id","=",$row["user_id"]);
                    $DB->AddValue("email", $_POST["email"]);
                    $DB->AddValue("username", $_POST["username"]);
                    $DB->AddValue("displayed_name", $_POST["last_name"]." ".$_POST["first_name"]." ".$_POST["patronymic"]);
                    $DB->Update();
                }
                }*/
                $this->update_user($row, 3);
            }
                else
                {
                    /*if(isset($_POST["username"]) && isset($_POST["email"]))
                    {
                        $DB->SetTable("auth_users");
                        $DB->AddValue("email", $_POST["email"]);
                        $DB->AddValue("username", $_POST["username"]);
                        $DB->AddValue("displayed_name", $_POST["last_name"]." ".$_POST["first_name"]." ".$_POST["patronymic"]);
                        $DB->Insert();
                        $DB->SetTable("auth_users");
                        $DB->AddCondFS("email","=",$_POST["email"]);
                        $res4 = $DB->Select();
                        if($row4 = $DB->FetchAssoc($res4))
                        {
                            $DB->SetTable($this->db_prefix."people");
                            $DB->AddCondFS("id","=",$mode[1]);
                            $DB->AddValue("user_id",$row4["id"]);
                            if(!isset($_POST["password1"]) || !isset($_POST["password2"]) || $_POST["password1"] != $_POST["password2"])
                            {
                                $this->output["messages"]["bad"][] = 301;
                            }
                            else
                            {
                                $DB->AddValue("password",$_POST["password1"]);
                            }
                            $DB->Update();
                        }
                    }*/
                    $this->insert_user($mode[1],3);
                }

            }
        if(isset($mode[2]) && $mode[2] == "marks")
        {
            $this->marks($mode);
        }
        else
        {
            $DB->SetTable($this->db_prefix."people");
            $DB->AddCondFS("id", "=", $mode[1]);
            $res = $DB->Select();
            if($row = $DB->FetchAssoc($res))
            {
                $DB->SetTable("auth_users");
                $DB->AddCondFS("id", "=", $row["user_id"]);
                $res2 = $DB->Select();
                if($row2 = $DB->FetchAssoc($res2))
                {
                    $row["username"] = $row2["username"];
                    $row["displayed_name"] = $row2["displayed_name"];
                    $row["email"] = $row2["email"];
                    $row["password"] = $row2["password"];
                }
                $DB->SetTable($this->db_prefix."groups");
                $DB->AddCondFS("id", "=", $row["id_group"]);
                $res2 = $DB->Select();
                if($row2 = $DB->FetchAssoc($res2))
                {
                    $row["group_name"] = $row2["name"];
                    $row["group_year"] = $row2["year"];
                    $row["id_faculty"] = $row2["id_faculty"];
                }
                $this->output["edit_student"] = $row;
                $DB->SetTable($this->db_prefix."specialities");
                $DB->AddCondFS("id_faculty", "=", $row["id_faculty"]);
                $res3 = $DB->Select();
                while($row3 = $DB->FetchAssoc($res3))
                {
                    $this->output["specialities"][] = $row3;
                }
            }
            $DB->SetTable($this->db_prefix."faculties");
            $res = $DB->Select();
            while($row = $DB->FetchAssoc($res))
            {
                $row["groups"] = array();
                $DB->SetTable($this->db_prefix."groups");
                $DB->AddCondFS("id_faculty","=", $row["id"]);
                $res2 = $DB->Select();
                while($row2 = $DB->FetchAssoc($res2))
                {
                    $row["groups"][] = $row2;
                }
                $this->output["faculties_list"][] = $row;
            }
        }
    }

    function marks($mode)
    {
        global $DB, $Engine;
        if(isset($_POST["student_id"]))
        {
            if(empty($_POST["day"]) || empty($_POST["month"]) || empty($_POST["year"]) || empty($_POST["subject"]) || empty($_POST["mark"]) || empty($_POST["term"]))
            {
                $this->output["messages"]["bad"][] = 306;
            }
            else
            {
                $date = $_POST["year"]."-".$_POST["month"]."-".$_POST["day"];
                $DB->SetTable($this->db_prefix."marks");
                $DB->AddValue("student_id",$_POST["student_id"]);
                $DB->AddValue("subject_id",$_POST["subject"]);
                $DB->AddValue("mark",$_POST["mark"]);
                $DB->AddValue("term", $_POST["term"]);
                $DB->AddValue("date", $date);
                $DB->Insert();
                $DB->SetTable($this->db_prefix."marks");
                $DB->AddCondFS("student_id","=",$mode[1]);
                $DB->AddOrder("term");
                $DB->AddOrder("date");
                $res = $DB->Select();
                while($row = $DB->FetchAssoc($res))
                {
                    $DB->SetTable($this->db_prefix."subjects");
                    $DB->AddCondFS("id","=",$row["subject_id"]);
                    $res2 = $DB->Select();
                    if($row2 = $DB->FetchAssoc($res2))
                    {
                        $row["subject"] = $row2["name"];
                    }
                    $this->output["marks"][]=$row;
                }
                $DB->SetTable($this->db_prefix."people");
                $DB->AddCondFS("id","=", $mode[1]);
                $res2 = $DB->Select();
                if($row2 = $DB->FetchAssoc($res2))
                {
                    $this->output["student_last_name"] = $row2["last_name"];
                    $this->output["student_name"] = $row2["name"];
                    $this->output["student_patronymic"] = $row2["patronymic"];
                    $this->output["student_id"] = $mode[1];
                }
                $res2 = $DB->Exec("select max(`term`) from `nsau_marks` where `student_id`=".$mode[1]);
                if($row2 = $DB->FetchAssoc($res2))
                {
                    $this->output["max_term"] = $row2;
                }
                $DB->SetTable($this->db_prefix."subjects");
                $res = $DB->Select();
                while($row = $DB->FetchAssoc($res))
                {
                    $this->output["subjects"][] = $row;
                }
            }
        }
        else
        if(!empty($mode[3]))
        {
            if($mode[3] == "delete")
            {
                $DB->SetTable($this->db_prefix."marks");
                $DB->AddCondFS("id","=",$mode[4]);
                $DB->Delete();
                CF::Redirect($Engine->engine_uri.$mode[0]."/".$mode[1]."/".$mode[2]."/");
            }
            if($mode[3] == "edit")
            {
                if(isset($_POST["mark_id"]))
                {
                    if(empty($_POST["day"]) || empty($_POST["month"]) || empty($_POST["year"]) || empty($_POST["subject"]) || empty($_POST["mark"]) || empty($_POST["term"]))
                    {
                        $this->output["messages"]["bad"][] = 306;
                    }
                    else
                    {
                        $date = $_POST["year"]."-".$_POST["month"]."-".$_POST["day"];
                        $DB->SetTable($this->db_prefix."marks");
                        $DB->AddCondFS("id","=",$_POST["mark_id"]);
                        $DB->AddValue("subject_id",$_POST["subject"]);
                        $DB->AddValue("mark",$_POST["mark"]);
                        $DB->AddValue("term", $_POST["term"]);
                        $DB->AddValue("date", $date);
                        $DB->Update();
                        CF::Redirect($Engine->engine_uri.$mode[0]."/".$mode[1]."/".$mode[2]."/");
                        unset($mode[3]);
                        unset($mode[4]);
                    }
                }
                else
                {
                    $DB->SetTable($this->db_prefix."marks");
                    $DB->AddCondFS("id","=",$mode[4]);
                    $res = $DB->Select();
                    if($row = $DB->FetchAssoc($res))
                    {
                        $DB->SetTable($this->db_prefix."subjects");
                        $DB->AddCondFS("id","=",$row["subject_id"]);
                        $res2 = $DB->Select();
                        if($row2 = $DB->FetchAssoc($res2))
                        {
                            $row["subject_name"] = $row2["name"];
                        }
                        $date = explode("-",$row["date"]);
                        $row["year"] = $date[0];
                        $row["month"] = $date[1];
                        $row["day"] = $date[2];
                        $this->output["edit_mark"]=$row;
                    }
                    $DB->SetTable($this->db_prefix."subjects");
                    $res = $DB->Select();
                    while($row = $DB->FetchAssoc($res))
                    {
                        $this->output["subjects"][] = $row;
                    }
                }
            }
        }
        else
        {
            $this->output["marks"]=array();
            $DB->SetTable($this->db_prefix."marks");
            $DB->AddCondFS("student_id","=",$mode[1]);
            $DB->AddOrder("term");
            $DB->AddOrder("date");
            $res = $DB->Select();
            while($row = $DB->FetchAssoc($res))
            {
                $DB->SetTable($this->db_prefix."subjects");
                $DB->AddCondFS("id","=",$row["subject_id"]);
                $res2 = $DB->Select();
                if($row2 = $DB->FetchAssoc($res2))
                {
                    $row["subject"] = $row2["name"];
                }
                $this->output["marks"][]=$row;
            }
            $DB->SetTable($this->db_prefix."people");
            $DB->AddCondFS("id","=", $mode[1]);
            $res2 = $DB->Select();
            if($row2 = $DB->FetchAssoc($res2))
            {
                $this->output["student_last_name"] = $row2["last_name"];
                $this->output["student_name"] = $row2["name"];
                $this->output["student_patronymic"] = $row2["patronymic"];
                $this->output["student_id"] = $mode[1];
            }
            $res2 = $DB->Exec("select max(`term`) from `nsau_marks` where `student_id`=".$mode[1]);
            if($row2 = $DB->FetchAssoc($res2))
            {
                $this->output["max_term"] = $row2;
            }
            $DB->SetTable($this->db_prefix."subjects");
            $res = $DB->Select();
            while($row = $DB->FetchAssoc($res))
            {
                $this->output["subjects"][] = $row;
            }
        }
    }



    //��������� ������ ���������, ���������� � ������ $mode[0]
    function students_list($mode)
    {
        global $DB, $Auth;
        if(isset($_POST["last_name"]))
        {
            if(!isset($_POST["password1"]) || !isset($_POST["password2"]) || $_POST["password1"] != $_POST["password2"])
            {
                $this->output["messages"]["bad"][] = 301;
            }
            else
            {	//die(print_r($_POST));
                $DB->SetTable("auth_users");
                $DB->AddValue("is_active", 1);
                $DB->AddValue("username", $_POST["username"]);
                $DB->AddValue("password", md5($_POST["password1"]));
                $DB->AddValue("displayed_name", $_POST["last_name"]." ".$_POST["first_name"]." ".$_POST["patronymic"]);
                $DB->AddValue("email", $_POST["email"]);
                $DB->AddValue("usergroup_id", 3);
                $DB->AddValue("create_time","NOW()", "X");
                $DB->AddValue("create_user_id", $Auth->user_id);
                $DB->Insert();
                $res = $DB->Exec("SELECT max(LAST_INSERT_ID(id)) FROM auth_users limit 1");
                if($row = $DB->FetchAssoc($res))
                {
                    foreach($row as $row2)
                    {
                        $DB->SetTable($this->db_prefix."people");
                        $DB->AddValue("user_id",$row2);
                        $DB->AddValue("last_name", $_POST["last_name"]);
                        $DB->AddValue("name", $_POST["first_name"]);
                        $DB->AddValue("patronymic", $_POST["patronymic"]);
                        $DB->AddValue("male", $_POST["male"]);
                        $DB->AddValue("status_id", 8);
                        $DB->AddValue("year", $_POST["year"]);
                        $DB->AddValue("id_group", $_POST["group_id"]);
                        if(isset($_POST["subgroup"]))
                            $DB->AddValue("subgroup", $_POST["subgroup"]);
                        //$DB->AddValue("id_speciality", $_POST["id_speciality"]);
                        if($DB->Insert())
                        {
                            $this->output["messages"]["good"][] = 101;
                        }
                        else
                        {
                            $this->output["messages"]["bad"][] = 301;
                        }

                    }
                }
            }
        }
        $DB->SetTable($this->db_prefix."people");
        $DB->AddCondFS("id_group", "=", $mode[0]);
        $DB->AddCondFS("status_id", "!=", "4");
        $DB->AddOrder("last_name");
        $DB->AddOrder("name");
        $DB->AddOrder("patronymic");
        $res = $DB->Select();
        while($row = $DB->FetchAssoc($res))
        {
            $DB->SetTable("auth_users");
            $DB->AddCondFS("id", "=", $row["user_id"]);
            $res2 = $DB->Select();
            if($row2 = $DB->FetchAssoc($res2))
            {
                $row["username"] = $row2["username"];
                $row["displayed_name"] = $row2["displayed_name"];
                $row["email"] = $row2["email"];
            }
            else
            {
                $row["username"] = 0;
                $row["displayed_name"] = 0;
                $row["email"] = 0;
            }
            $this->output["students_list"][] = $row;
        }
        $DB->SetTable($this->db_prefix."groups");
        $DB->AddCondFS("id", "=", $mode[0]);
        $res = $DB->Select();
        if($row = $DB->FetchAssoc($res))
        {
            $DB->SetTable($this->db_prefix."specialities");
            $DB->AddCondFS("id_faculty", "=", $row["id_faculty"]);
            $res3 = $DB->Select();
            while($row3 = $DB->FetchAssoc($res3))
            {
                $this->output["specialities"][] = $row3;
            }
            $this->output["group_name"] = $row["name"];
            $this->output["year"] = $row["year"];
            $this->output["id_faculty"]= $row["id_faculty"];
            $DB->SetTable($this->db_prefix."faculties");
            $DB->AddCondFS("id", "=", $row["id_faculty"]);
            $res2 = $DB->Select();
            if($row2 = $DB->FetchAssoc($res2))
            {
                $this->output["faculty_name"]= $row2["name"];
            }
        }
        $this->output["group_id"] = $mode[0];
        $this->output["students_list_mode"] = "students_list_mode";
    }

    function timetable_show($gr_id = NULL, $weeks = null)
    {
        global $DB;
		$sl = $gr_id;
		$gr_id = $gr_id ? $gr_id : (isset($_GET["gr"]) ? htmlspecialchars($_GET["gr"]) : NULL);

		if (!$sl) {
			$sql = "SELECT * FROM nsau_faculties WHERE id in (SELECT id_faculty FROM nsau_groups)";
			$res = $DB->Exec($sql);
			while ($row = $DB->FetchAssoc($res)){
				   $this->output["faculties"][$row["id"]] = $row;
			}

			$DB->SetTable("nsau_groups");
			$DB->AddCondFS("hidden", "=", 0);
			$res = $DB->Select();
			while ($row = $DB->FetchAssoc($res)){
				    $this->output["groups"][$row["id"]] = $row;
			}

			$DB->SetTable("nsau_buildings");
			$DB->AddOrder("id");
			$res = $DB->Select();
			while ($row = $DB->FetchAssoc($res)){
				    $this->output["buildings"][$row["id"]] = $row;
			}

			$this->output["now"] = getdate();

			$this->output["parity"] = $this->timetable_weekparity();
		}

		if ($gr_id) {
			$sql = "SELECT x.*, y.name as spec, z.name as fac
			FROM nsau_groups x
			LEFT JOIN nsau_specialities y ON y.code = x.specialities_code
			LEFT JOIN nsau_faculties z ON z.id = x.id_faculty
			WHERE x.id=".$gr_id." AND x.hidden=0";

			$res = $DB->Exec($sql);
			if ($row = $DB->FetchAssoc($res)) {
				$this->output["group"] = $row;
			}

			$sql = "SELECT x.id as id, p.id as people_id, p.name as p0, p.patronymic as p1, p.last_name as p2
			FROM nsau_teachers x
			LEFT JOIN nsau_people p on p.id=x.people_id";
			$res = $DB->Exec($sql);

			while ($row = $DB->FetchAssoc($res))
				$teachers[$row["id"]] = $row;

			$sql = "SELECT x.id as id, x.name as name, b.label as build
			FROM nsau_auditorium x
			LEFT JOIN nsau_buildings b on b.id = x.building_id";
			$res = $DB->Exec($sql);

			while ($row = $DB->FetchAssoc($res))
				$auds[$row["id"]] = $row;

			$sql = "SELECT x.*, s.name as subj
			FROM nsau_timetable_new x
			LEFT JOIN nsau_groups g ON g.id = x.group_id
			LEFT JOIN nsau_subjects s ON s.id = x.subject_id
			WHERE x.group_id=".$gr_id."";
			if($_GET["week"]!="")
				$sql .= " AND x.izop_week=".$_GET["week"];
			if(isset($weeks))
				$sql .= " AND x.izop_week=".$weeks;
			if (isset($_GET["subgr"]) && is_numeric($_GET["subgr"]) && ($_GET["subgr"] == 1 || $_GET["subgr"] == 2))
				$sql .= " AND (x.subgroup=0 OR x.subgroup=".$_GET["subgr"].")";
			$sql .= " ORDER BY x.id";
			$res = $DB->Exec($sql);

			$retarr = array();
			while ($row = $DB->FetchAssoc($res)) {
				$row["comment_to"] = implode(".",array_reverse(explode("-",$row["comment_to"])));
				$row["comment_from"] = implode(".",array_reverse(explode("-",$row["comment_from"])));
				$wname = $row["week"] ? "odd" : "even";
				$rowauds = explode(";", $row["auditorium_id"]);
				$rowteachers = explode(";", $row["teacher_id"]);
				foreach ($rowauds as $val)
					if ($val)
						$row["auds"][] = $auds[$val];
				foreach ($rowteachers as $val)
					if ($val)
						$row["teachs"][] = $teachers[$val];

				if (!$sl) {
					if (!isset($this->output[$wname][$row["day"]][$row["pair"]]))
						$this->output[$wname][$row["day"]][$row["pair"]] = $row;
					elseif(!isset($this->output["doubles"][$this->output[$wname][$row["day"]][$row["pair"]]["id"]]))
						$this->output["doubles"][$this->output[$wname][$row["day"]][$row["pair"]]["id"]] = $row;
					else
						$this->output["triples"][$this->output[$wname][$row["day"]][$row["pair"]]["id"]] = $row;
				}
				else {
					if (!isset($retarr[$wname][$row["day"]][$row["pair"]]))
						$retarr[$wname][$row["day"]][$row["pair"]] = $row;
					elseif(!isset($retarr["doubles"][$retarr[$wname][$row["day"]][$row["pair"]]["id"]]))
						$retarr["doubles"][$retarr[$wname][$row["day"]][$row["pair"]]["id"]] = $row;
					else
						$retarr["triples"][$retarr[$wname][$row["day"]][$row["pair"]]["id"]] = $row;
				}
			}

			if ($gr_id) {
				$DB->SetTable("nsau_timetable_dates");
				$DB->AddCondFS("group_id", "=", $gr_id);
				$DB->AddCondFS("week", "=", $_GET["week"]);
				$res = $DB->Select();
				if ($row = $DB->FetchAssoc($res))
					$this->output["dates"] = $row;
			}
		}
		if(!empty($retarr))
		return $retarr;
    }
		function timetable_print($students = null)
		{
			global $DB;
			$DB->SetTable("nsau_buildings");
			$DB->AddOrder("id");
			$res = $DB->Select();
			while ($row = $DB->FetchAssoc($res)){
				$this->output["buildings"][$row["id"]] = $row;
			}
			$gr = explode("z", $_GET["gr"]);
			array_shift($gr);
			$gr_weeks = explode("z", $_GET["weeks"]);
			array_shift($gr_weeks);
			$DB->SetTable("nsau_groups");
			foreach($gr as $g_id)
			$DB->AddAltFS("id", "=", $g_id);
			$DB->AddCondFS("hidden", "=", '0');
			$res = $DB->Select();
			while ($row = $DB->FetchAssoc($res)) {
				$groups[$row["id"]] = $row["name"];
				$facId = $row["id_faculty"];
				$specCode = $row["specialities_code"];
			}
			$days = $x = $d =array();
			$i=0;
			if(empty($gr_weeks)) {
				foreach ($groups as $key => $val) {
					$a = $this->timetable_show($key);
					if(!empty($a)){
						$fileName .= $val.($i<(count($groups)-1) ? "_" : "");
						$i++;
						$this->output["groups"][$key] = $this->timetable_show($key);
						$this->output["groups"][$key]["group_name"] = $val;
					}
				}
			}
			else {
				$sql = "SELECT * from nsau_timetable_dates where group_id=".$gr[0]."";
				$res = $DB->Exec($sql);
				while ($row = $DB->FetchAssoc($res))
					$dates[$row["week"]] = implode(".", array_reverse(explode("-",$row["actual_from"])))."-".implode(".", array_reverse(explode("-",$row["actual_to"])));
				asort($gr_weeks);
				foreach ($gr_weeks as $w) {
					foreach ($groups as $key => $val) {
						// $a = $this->timetable_show($key, $w);
						// if(!empty($a)){
							$fileName .= (($i==0) ? $val."_" : "").$w.($i<(count($gr_weeks)-1) ? "_" : "");
							$i++;
							$this->output["groups"][$w] = $this->timetable_show($key, $w);
							$this->output["groups"][$w]["group_name"] = $val;
							$this->output["groups"][$w]["group_subname"] = " ".($w+1)." ���.".(!empty($dates[$w]) ?  " (".$dates[$w].")" : "");
						// }
					}
				}
			}
			$this->output["pairs"] = $this->isset_pairs($this->output["groups"]);
			$sql = "SELECT x.id as id, x.name as name, b.label as build
				FROM nsau_auditorium x
				LEFT JOIN nsau_buildings b on b.id = x.building_id";
			$res = $DB->Exec($sql);
			while ($row = $DB->FetchAssoc($res))
				$auds[$row["id"]] = $row;
			$this->output["auds"] = $auds;
			$sql = "SELECT x.id as id, p.id as people_id, p.name as p0, p.patronymic as p1, p.last_name as p2
				FROM nsau_teachers x
				LEFT JOIN nsau_people p on p.id=x.people_id";
			$res = $DB->Exec($sql);
			while ($row = $DB->FetchAssoc($res))
				$teachers[$row["id"]] = $row;
			$this->output["teachers"] = $teachers;
			$DB->SetTable("nsau_faculties");
			$DB->AddCondFS("id", "=", $facId);
			$DB->AddField("name");
			$res = $DB->Select(1);
			$row = $DB->FetchAssoc($res);
			$this->output["info"]["facName"] = $row["name"];
			$DB->SetTable("nsau_specialities");
			$DB->AddCondFS("code", "=", $specCode);
			$DB->AddField("name");
			$res = $DB->Select(1);
			$row = $DB->FetchAssoc($res);
			$this->output["info"]["specName"] = $row["name"];
			if(!empty($this->output["groups"])) {
				header('Content-Type: text/html; charset=windows-1251');
				header('P3P: CP="NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM"');
				header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
				header('Cache-Control: no-store, no-cache, must-revalidate');
				header('Cache-Control: post-check=0, pre-check=0', FALSE);
				header('Pragma: no-cache');
				header('Content-transfer-encoding: binary');
				header('Content-Disposition: attachment; filename='.$fileName.'.xls');
				header('Content-Type: application/x-unknown');
			}
		}

		function isset_pairs($arr, $mode=null) {
			if(!$mode)	{
				$pair = array();
				for($d=0;$d<7;$d++) {
					for($i=0;$i<7;$i++) {
						$isset_odd = 0;
						$isset_even = 0;
						foreach ($arr as $id=>$group) {
							$isset_odd = count($group["odd"][$d][$i]);
							$isset_even = count($group["even"][$d][$i]);
							if($isset_odd>0) {
								$pair[$id][$i]["odd"] = $isset_odd;
								$this->output['days'][$d] = 1;
							}
							if($isset_even>0) {
								$pair[$id][$i]["even"] = $isset_even;
								$this->output['days'][$d] = 1;
							}
						}
					}
				}
				foreach($arr as $id=>$g) {
					if(!isset($pair[$id]))
						$pair[$id] = 1;
				}
				ksort($pair);
				return $pair;
			}
			else {
				$pair = array();
				for($d=0;$d<7;$d++) {
					for($i=0;$i<7;$i++) {
						$isset_odd = 0;
						$isset_even = 0;
						$isset_odd = count($arr[0][$d][$i]);
						$isset_even = count($arr[1][$d][$i]);
						if($isset_odd>0) {
							$pair["days"][$d][0] = ($isset_odd>$pair["days"][$d][0]) ? $isset_odd : $pair["days"][$d][0];
							$pair["pairs"][$i] =  1;
						}
						if($isset_even>0) {
							$pair["pairs"][$i] =  1;
							$pair["days"][$d][1] = ($isset_even>$pair["days"][$d][1]) ? $isset_even : $pair["days"][$d][1];
						}
					}
				}
				return $pair;
			}


		}


   function speciality_directory($module_uri)
    {
        global $DB, $Auth, $Engine;

        /*������ �����������*/
    	$DB->SetTable($this->db_prefix."faculties", "f");
		$DB->AddField("f.id");
		$DB->AddField("f.name");
		$res=$DB->Select();
		while ($row=$DB->FetchAssoc($res)) {
			$this->output["faculty"][]=$row;
		};

		$pars = explode('/', $module_uri);
		$this->output['curriculum_allowed'] = 1;
		$spec_type = array(
			'0' => array('51'=>'secondary','68'=>'magistracy','62'=>'bachelor','65'=>'higher'),
			'1' => array('secondary'=>'51','magistracy'=>'68','bachelor'=>'62','higher'=>'65')
		);
		$spec_type_name = array('secondary' => '���','bachelor' => '�����������', 'magistracy' => '������������', 'higher' => '���');
		$this->output["spec_type_name"] = $spec_type_name;
		if(isset($pars[0], $pars[1]) && $pars[0] == 'curriculum' && CF::IsNaturalNumeric($pars[1]) && (!isset($pars[2]) || empty($pars[2]))) {
			$this->output['mode'] = $pars[0];
			$plan_id = $pars[1];
			$spec_type_code = array('secondary' => 51, 'bachelor' => 62, 'higher' => 65, 'magistracy' => 68);
			$DB->SetTable($this->db_prefix."spec_type");
			$DB->AddCondFS("id", "=", $plan_id);
			$res = $DB->Select();
			if(!($row = $DB->FetchAssoc($res)) && $Engine->OperationAllowed($this->module_id, "specialities.handle", (int)$row["code"], $Auth->usergroup_id)) {
				$Engine->HTTP404();
				return;
			} else {
				$this->output['curriculum_allowed'] = 1;
			}
			$Engine->AddFootstep($Engine->engine_uri.$Engine->module_uri, "������� ����", '', false);
			if(isset($_POST['add_sgroup'])) {
				$error = false;
				if(!isset($_POST['sgroup_name']) || empty($_POST['sgroup_name'])) {
					$error = true;
					$this->output["mess"]['curriculum'][] = "�� ������� �������� �����.";
				}
				if(!isset($_POST['sgroup_code']) || empty($_POST['sgroup_code'])) {
					$error = true;
					$this->output["mess"]['curriculum'][] = "�� ������ ���� �����.";
				}
				if(!$error) {
					$DB->SetTable($this->db_prefix."subject_group");
					$DB->AddValue("name", $_POST['sgroup_name']);
					$DB->AddValue("code", $_POST['sgroup_code']);
					if(!$DB->Insert()) {

					}
				} else {
					$this->output["form_back"]['add_sgroup']['sgroup_name'] = $_POST['sgroup_name'];
					$this->output["form_back"]['add_sgroup']['sgroup_code'] = $_POST['sgroup_code'];
				}
			}
			elseif(isset($_POST['add_subject'])) {
				$error = false;
				$select_subject = null;
				if(!isset($_POST['sgroup_id']) || empty($_POST['sgroup_id'])) {
					$error = true;
					$this->output["mess"]['curriculum'][] = "���� �� ������.";
				}
				if(!isset($_POST['subject_id']) || count($_POST['subject_id']) == 0) {
					$error = true;
					$this->output["mess"]['curriculum'][] = "�� �������� ����������.";
				} else {
					foreach($_POST['subject_id'] as $subject_id) {
						if(isset($_POST['subject_code'][$subject_id]) && !empty($_POST['subject_code'][$subject_id])) {
							$select_subject[$subject_id] = $_POST['subject_code'][$subject_id];
						} else {
							$select_subject = null;
							continue;
						}
					}
					if(is_null($select_subject)) {
						$error = true;
						$this->output["mess"]['curriculum'][] = "� ��������� ��������� �� ������ ����.";
					}
				}
				if(!$error) {
					$pos = 1;
					$query = "INSERT INTO `".$this->db_prefix."curriculum` (pos, subject_group_id, spec_type_id, subject_group_code, subject_id) VALUES ";
						//$DB->SetTable($this->db_prefix."curriculum");
					$res_p = $DB->Exec("select max(pos) as 'max' from `".$this->db_prefix."curriculum` WHERE subject_group_id = ".$_POST['sgroup_id']." and spec_type_id = ".$plan_id.";");

					if($row_p = $DB->FetchAssoc($res_p)) {
						$pos = $row_p['max']+1;
					}
					foreach($select_subject as $subject_id => $subject_code) {
						if($pos == $row_p['max']+1) {
							$query .= "('".$pos."', '".$_POST['sgroup_id']."', '".$plan_id."', '".$subject_code."', '".$subject_id."')";
						} else {
							$query .= ", ('".$pos."', '".$_POST['sgroup_id']."', '".$plan_id."', '".$subject_code."', '".$subject_id."')";
						}
						$pos++;
							//$DB->AddValue("subject_group_id", $sgroup_id);
							//$DB->AddValue("spec_type_id", $plan_id);
							//$DB->AddValue("subject_group_code", $subject_code);
							//$DB->AddValue("subject_id", $subject_id);
					}
					$res = $DB->Exec($query);
					if(!$res) {
						$this->output["mess"]['curriculum'][] = "������� �� �������. �������� ����� ���������� ��� ������������.";
					}
					$this->output["form_back"]['add_subject']['sgroup_id'] = $_POST['sgroup_id'];
				} else {
					$this->output["form_back"]['add_subject']['sgroup_id'] = $_POST['sgroup_id'];
					foreach($_POST['subject_id'] as $subject_id) {
						$this->output["form_back"]['add_subject']['select_subject'][$subject_id] = $_POST['subject_code'][$subject_id];
					}
				}
			}
			elseif(isset($_POST['edit_subject_id'])) {
				if($_POST['action_del']) {
					$DB->SetTable($this->db_prefix."curriculum");
					$DB->AddCondFS("id", "=", $_POST['edit_cur_id']);
					if(!$DB->Delete()) {
						$this->output["mess"]['curriculum'][] = "�� ������� ������� ����������.";
					}
				} else {
					$DB->SetTable($this->db_prefix."curriculum");
					$DB->AddCondFS("id", "=", $_POST['edit_cur_id']);
					if($_POST['edit_subject_id'] != $_POST['select_subject_id']) {
						$DB->AddValue("subject_id", $_POST['select_subject_id']);
					}
					$DB->AddValue("subject_group_code", $_POST['edit_subject_code']);
					if(CF::IsNaturalNumeric($_POST['edit_subject_pos'])) {
						$DB->AddValue("pos", $_POST['edit_subject_pos']);
					}
					if(!$DB->Update()) {
						$this->output["mess"]['curriculum'][] = "�� ������� �������� ����������.";
					}
				}
			}
			elseif(isset($_POST['edit_sgroup_id'])) {
				if($_POST['action_del']) {
					$DB->SetTable($this->db_prefix."subject_group");
					$DB->AddCondFS("id", "=", $_POST['edit_sgroup_id']);
					if(!$DB->Delete()) {
						$this->output["mess"]['curriculum'][] = "�� ������� ������� ���� ���������. �������� � ����� ��� ������� ����������.";
					}
				} else {
					$DB->SetTable($this->db_prefix."subject_group");
					$DB->AddCondFS("id", "=", $_POST['edit_sgroup_id']);
					$DB->AddValue("name", $_POST['edit_sgroup_name']);
					$DB->AddValue("code", $_POST['edit_sgroup_code']);
					if(!$DB->Update()) {
						$this->output["mess"]['curriculum'][] = "�� ������� ������������� ���� ���������.";
					}
				}
			}
			$DB->SetTable($this->db_prefix."subject_group");
			$DB->AddOrder("name");
			$res = $DB->Select();
			while ($row = $DB->FetchAssoc($res)) {
				$this->output['curriculum']['sgroup_list'][] = $row;
			}

			$DB->SetTable($this->db_prefix."subjects", "s");
			$DB->AddTable($this->db_prefix."departments", "d");
			$DB->AddCondFF("s.department_id", "=", "d.id");
			$DB->AddCondFS("d.is_active", "=", 1);
			$DB->AddCondFS("s.is_hidden", "=", 0);
			$DB->AddField("s.name", "sname");
			$DB->AddField("s.id", "sid");
			$DB->AddField("d.name", "dname");
			$DB->AddField("d.id", "did");
			$DB->AddOrder("d.name");
			$DB->AddOrder("s.name");
			$res = $DB->Select();
			while ($row = $DB->FetchAssoc($res)) {
				$this->output['curriculum']['departments'][$row['did']]['name'] = $row['dname'];
				$this->output['curriculum']['departments'][$row['did']]['subjects'][$row['sid']] = $row['sname'];
			}

			$DB->SetTable($this->db_prefix."spec_type", "st");
			$DB->AddTable($this->db_prefix."specialities", "s");
			//$DB->AddTable($this->db_prefix."faculties", "f");
			$DB->AddCondFS("st.id", "=", $plan_id);
			$DB->AddCondFF("s.id", "=", "st.spec_id");
			//$DB->AddCondFF("s.id_faculty", "=", "f.id");
			$DB->AddField("s.code", "scode");
			$DB->AddField("s.name", "sname");
			$DB->AddField("s.id_faculty", "fid");
			//$DB->AddField("f.name", "fname");
			$DB->AddField("st.type", "st_type");
			$res = $DB->Select();
			if($row = $DB->FetchAssoc($res)) {
				$this->output['curriculum']['fname'] = '�� ������';
				$this->output['curriculum']['sname'] = $row['sname'];
				$this->output['curriculum']['scode'] = $row['scode'];
				$this->output['curriculum']['stcode'] = $spec_type_code[$row['st_type']];
				$DB->SetTable($this->db_prefix."faculties");
				$DB->AddCondFS("id", "=", $row['fid']);
				$DB->AddField("name");
				$res_f = $DB->Select();
				if($row_f = $DB->FetchAssoc($res_f)) {
					$this->output['curriculum']['fname'] = $row_f['name'];
				}
			}
			$DB->SetTable($this->db_prefix."curriculum", 'cur');
			$DB->AddTable($this->db_prefix."subject_group", 'subjg');
			$DB->AddTable($this->db_prefix."subjects", 'subj');
			//$DB->AddTable($this->db_prefix."departments", 'd');
			$DB->AddCondFS("cur.spec_type_id", "=", $plan_id);
			$DB->AddCondFF("cur.subject_group_id", "=", "subjg.id");
			$DB->AddCondFF("cur.subject_id", "=", "subj.id");
			//$DB->AddCondFF("d.id", "=", "subj.department_id");
			$DB->AddField("cur.id", "cur_id");
			$DB->AddField("cur.pos", "cur_pos");
			$DB->AddField("cur.subject_group_id", "sgroup_id");
			$DB->AddField("cur.subject_group_code", "subject_code");
			$DB->AddField("subjg.name", "sgroup_name");
			$DB->AddField("subjg.code", "sgroup_code");
			$DB->AddField("subj.id", "sid");
			$DB->AddField("subj.is_hidden", "is_hidden");
			$DB->AddField("subj.name", "sname");
			$DB->AddField("subj.department_id", "dep_id");
			//$DB->AddField("d.name", "dname");
			$DB->AddOrder("subjg.name");
			$DB->AddOrder("cur.pos");
			$res_plan = $DB->Select();
			while ($row_plan = $DB->FetchAssoc($res_plan)) {
				$this->output['curriculum']['subject_group'][$row_plan['sgroup_id']]['sgroup_name'] = $row_plan['sgroup_name'];
				$this->output['curriculum']['subject_group'][$row_plan['sgroup_id']]['sgroup_code'] = $row_plan['sgroup_code'];
				$this->output['curriculum']['subject_group'][$row_plan['sgroup_id']]['subjects'][$row_plan['cur_id']]['scode'] = $row_plan['subject_code'];
				$this->output['curriculum']['subject_group'][$row_plan['sgroup_id']]['subjects'][$row_plan['cur_id']]['sname'] = $row_plan['sname'];
				$this->output['curriculum']['subject_group'][$row_plan['sgroup_id']]['subjects'][$row_plan['cur_id']]['is_hidden'] = $row_plan['is_hidden'];
				$this->output['curriculum']['subject_group'][$row_plan['sgroup_id']]['subjects'][$row_plan['cur_id']]['subj_id'] = $row_plan['sid'];
				$this->output['curriculum']['subject_group'][$row_plan['sgroup_id']]['subjects'][$row_plan['cur_id']]['pos'] = $row_plan['cur_pos'];
				$this->output['curriculum']['subject_group'][$row_plan['sgroup_id']]['subjects'][$row_plan['cur_id']]['sdep'] = "";
				$DB->SetTable($this->db_prefix."departments");
				$DB->AddCondFS("id", "=", $row_plan['dep_id']);
				$DB->AddField("name");
				$res_dep = $DB->Select();
				if($row_dep = $DB->FetchAssoc($res_dep)) {
					$this->output['curriculum']['subject_group'][$row_plan['sgroup_id']]['subjects'][$row_plan['cur_id']]['sdep'] = $row_dep['name'];
				}
			}
			$this->output['scripts_mode'] = 'curriculum';
		}
		elseif(isset($pars[0]) && !empty($pars[0]) && $pars[0] == "add_speciality") {
			$this->output["sub_mode"] = "add_speciality";
			$this->output['plugins'][] = 'jquery.editable-select';
			$DB->SetTable($this->db_prefix."school");
//		    $DB->AddOrder("name");
		    $DB->AddOrder("code");
			$res = $DB->Select();
			$this->output["directory"] = array();
			while ($row = $DB->FetchAssoc($res)) 
			{
				//����� ���� ������ ������ � ����� �����������
				$old_directory = explode (".", $row['code']);
				if (!empty($old_directory[1]))
				$this->output["directory"][] = $row;
			}

			$DB->SetTable($this->db_prefix."specialities", "s");
			$DB->AddTable($this->db_prefix."school", "sch");
			$DB->AddCondFS("s.old", "=", 1);
			$DB->AddCondFF("s.direction", "=", "sch.code");
			$DB->AddFields(array("s.id", "s.name", "s.code", "s.direction"));
			$DB->AddField("sch.name", "dir_name");
			$DB->AddField("sch.code", "dir_code");

			$res_old = $DB->Select();
			while ($row_old = $DB->FetchAssoc($res_old)) {
				$this->output["old_specs"][$row_old["direction"]]["specs"][$row_old["id"]] = $row_old;
				$this->output["old_specs"][$row_old["direction"]]["title"] = $row_old["dir_name"];
				$this->output["old_specs"][$row_old["direction"]]["code"] = $row_old["dir_code"];
			}

			$DB->SetTable($this->db_prefix."qualifications");

			$res_qual = $DB->Select();
			while ($row_qual = $DB->FetchAssoc($res_qual)) {
				$this->output["qualifications"][$row_qual["id"]] = $row_qual;
			}

			if ($_POST["action_op"]=="add") { //���������� ������������� � ������� ����������
				$this->output["form_back"]["code_add"] = $_POST["code_add"];
				$this->output["form_back"]["speciality_name"] = $_POST["speciality_name"];
				$this->output["form_back"]["direction"] = $_POST["direction"];
				$this->output["form_back"]["description"] = $_POST["description"];
				$this->output["form_back"]["year_open"] = $_POST["year_open"];
				$this->output["form_back"]["type"] = $_POST["type"];
				$this->output["form_back"]["old_spec"] = $_POST["old_spec"];
				$this->output["form_back"]["has_vacancies"] = empty($_POST["has_vacancies"]) ? 0 : 1;
				$this->output["form_back"]["internal_tuition"] = empty($_POST["internal_tuition"]) ? 0 : 1;
				$this->output["form_back"]["correspondence_tuition"] = empty($_POST["correspondence_tuition"]) ? 0 : 1;

				$this->output["form_back"]["qualification"] = $_POST["qualification"];

				if(!isset($_POST["code_add"]) /*|| !CF::IsNaturalNumeric($_POST["code_add"]) */ || empty($_POST["code_add"])) {
					$this->output["messages"]["bad"][] = "�� ������ ��� �������������.";
				} elseif(!isset($_POST["speciality_name"]) || empty($_POST["speciality_name"])) {
					$this->output["messages"]["bad"][] = "�� ������� �������� �������������.";
				} elseif(!isset($_POST["direction"]) || $_POST["direction"] == "0" || empty($_POST["direction"])) {
					$this->output["messages"]["bad"][] = "�� �������� �����������.";
				} elseif((!isset($_POST["type"]) || $_POST["type"] == "0" || empty($_POST["type"])) && (!isset($_POST["qualification"]) || $_POST["qualification"] == "0" || empty($_POST["qualification"])) ) {
					$this->output["messages"]["bad"][] = "������� ���������� �� ������.";
				} else {
					$DB->SetTable($this->db_prefix."specialities");
					
					$DB->AddValue("code", $_POST["code_add"]);
					$DB->AddValue("name", $_POST["speciality_name"]);
					$DB->AddValue("direction", $_POST["direction"]);
					$DB->AddValue("description", $_POST["description"]);
					$DB->AddValue("year_open", $_POST["year_open"]);

					$DB->AddValue("old", 0);
					$DB->AddValue("old_id", (int)$_POST["old_spec"]);

					if(!empty($_POST["data_training"]) && $_POST["type"] == "secondary") {
						$DB->AddValue("data_training", $_POST["data_training"]);
					};

					if(!empty($_POST["faculty"])) 
					{
						$DB->AddValue("id_faculty", $_POST["faculty"]);
					}
					else
					{
						$DB->AddValue("id_faculty", 0);
					}


					if($DB->Insert(true)) {
						$spec_id = $DB->LastInsertID();
						$Engine->LogAction($this->module_id, "specialities", $spec_id, "add");
						$DB->SetTable($this->db_prefix."spec_type");
						$DB->AddValue("spec_id", $spec_id);
						$DB->AddValue("code", $_POST["code_add"]);
						if(!empty($_POST["type"])) {
							$DB->AddValue("type", $_POST["type"]);
							$DB->AddValue("type_code", $spec_type[1][$_POST["type"]]);
						}
						$DB->AddValue("year_open", $_POST["year_open"]);
						//$DB->AddValue("has_vacancies", $_POST["has_vacancies"]);

						if(!empty($_POST["qualification"])) {
							$DB->AddValue("qual_id", $_POST["qualification"]);
						}

						if(empty($_POST["has_vacancies"])) {
							$DB->AddValue("has_vacancies", 1);
						} else {
							$DB->AddValue("has_vacancies", 1);
						}
						if(empty($_POST["internal_tuition"])) {
							$DB->AddValue("internal_tuition", 0);
						} else {
							$DB->AddValue("internal_tuition", 1);
						}
						if(empty($_POST["correspondence_tuition"])) {
							$DB->AddValue("correspondence_tuition", 0);
						} else {
							$DB->AddValue("correspondence_tuition", 1);
						}



						if($DB->Insert()) {
							$this->output["messages"]["good"][] = "������������� ������� ���������.";
							$Engine->LogAction($this->module_id, "spec_type", $spec_id, "add");
							CF::Redirect($Engine->engine_uri);
						} else {
							$DB->SetTable($this->db_prefix."specialities");
							$DB->AddCondFS("id", "=", $spec_id);
							$res = $DB->Delete();
							$this->output["messages"]["bad"][] = "������ ��� ���������� ������ �������� �������������";
						}
					} else {
						$this->output["messages"]["bad"][] = "������������� �� ���� ���������";
						$this->output["messages"]["bad"][] = "��������, ������������� � ���������� ��������� � ����� ��� ���������� � �����������";
					}
				}
			}
		}
		elseif(isset($pars[0], $pars[1]) && !empty($pars[0]) && $pars[0] == "del_speciality" && CF::IsNaturalNumeric($pars[1])) {
			$DB->SetTable($this->db_prefix."specialities");
			$DB->AddCondFS("id","=", $pars[1]);
			if($DB->Delete())  {
				$DB->SetTable($this->db_prefix."spec_type");
				$DB->AddCondFS("spec_id","=", $pars[1]);
				if($DB->Delete()) {
					$this->output["messages"]["good"][] = "������������� � �� �������� �������� ������� �������.";
				} else {
					$this->output["messages"]["bad"][] = "������������� � �� �������� �������� �� �������.";
				};
			} else {
				$this->output["messages"]["bad"][] = "������������� � �� �������� �������� �� �������.";
			};
			$Engine->LogAction($this->module_id, "speciality", $pars[1], "delete");
			CF::Redirect($Engine->engine_uri);
		}
		elseif(isset($pars[0], $pars[1]) && !empty($pars[0]) && $pars[0] == "add_spec_type" && CF::IsNaturalNumeric($pars[1])) {
			$this->output["sub_mode"] = "add_spec_type";

			$DB->SetTable($this->db_prefix."specialities");
			$DB->AddField("id");
			$DB->AddField("code");
			$DB->AddField("name");
			$DB->AddField("direction");
			$DB->AddField("description");

			$DB->AddCondFS("id", "=", $pars[1]);
			$res = $DB->Select();

			$this->output["spec_info"] = array();

			if($row = $DB->FetchAssoc($res))
			{
				$this->output["spec_info"] = $row;

				$DB->SetTable("nsau_spec_type");
				$DB->AddField("type");
				$DB->AddCondFS("code", "=", $row['code']);
				$res2 = $DB->Select();
				if($row2= $DB->FetchAssoc($res2))
				{
					$this->output["spec_type"] = $row2;
				}
			};




		    $DB->SetTable($this->db_prefix."qualifications");

			$res_qual = $DB->Select();
			while ($row_qual = $DB->FetchAssoc($res_qual)) {
				$this->output["qualifications"][$row_qual["id"]] = $row_qual;
			}
			if (!empty($_POST["action_sub"]) && $_POST["action_sub"]=="add_spec_type") {
				$this->output["form_back"]["type_sub"] = $_POST["type_sub"];
				$this->output["form_back"]["year_open_sub"] = $_POST["year_open_sub"];
				$this->output["form_back"]["data_training"] = $_POST["data_training"];
				$this->output["form_back"]["data_training_ex"] = $_POST["data_training_ex"];
				$this->output["form_back"]["data_training_int_ex"] = $_POST["data_training_int_ex"];
				$this->output["form_back"]["has_vacancies"] = empty($_POST["has_vacancies"]) ? 0 : 1;
				$this->output["form_back"]["internal_tuition_sub"] = empty($_POST["internal_tuition_sub"]) ? 0 : 1;
				$this->output["form_back"]["correspondence_tuition_sub"] = empty($_POST["correspondence_tuition_sub"]) ? 0 : 1;
				$this->output["form_back"]["internal_correspondence_tuition_sub"] = empty($_POST["internal_correspondence_tuition_sub"]) ? 0 : 1;
		        $this->output["form_back"]["qualification"] = $_POST["qualification"];

				if(!isset($_POST["type_sub"]) || $_POST["type_sub"] == "0" || empty($_POST["type_sub"])) {
					$this->output["messages"]["bad"][] = "������� ���������� �� ������.";
				} else {
					$DB->SetTable($this->db_prefix."spec_type");
					$DB->AddValue("spec_id", $_POST["spec_id"]);
					$DB->AddValue("code", $_POST["code"]);
					$DB->AddValue("type", $_POST["type_sub"]);
					$DB->AddValue("type_code", $spec_type[1][$_POST["type_sub"]]);
					$DB->AddValue("year_open", $_POST["year_open_sub"]);
					if(isset($_POST["data_training"]) && !empty($_POST["data_training"])) {
						$DB->AddValue("data_training", $_POST["data_training"]);
					}
					if(isset($_POST["data_training_ex"]) && !empty($_POST["data_training_ex"])) {
						$DB->AddValue("data_training_ex", $_POST["data_training_ex"]);
					}
					if(isset($_POST["data_training_int_ex"]) && !empty($_POST["data_training_int_ex"])) {
						$DB->AddValue("data_training_int_ex", $_POST["data_training_int_ex"]);
					}
					if(!empty($_POST["internal_tuition_sub"]) && $_POST["internal_tuition_sub"]=="on") {
						$DB->AddValue("internal_tuition", 1);
					} else {
						$DB->AddValue("internal_tuition", 0);
					}
					if(!empty($_POST["correspondence_tuition_sub"]) && $_POST["correspondence_tuition_sub"]=="on") {
						$DB->AddValue("correspondence_tuition", 1);
					} else {
						$DB->AddValue("correspondence_tuition", 0);
					}
					if(!empty($_POST["internal_correspondence_tuition_sub"]) && $_POST["internal_correspondence_tuition_sub"]=="on") {
						$DB->AddValue("internal_correspondence_tuition", 1);
					} else {
						$DB->AddValue("internal_correspondence_tuition", 0);
					}
					if(!empty($_POST["has_vacancies"]) && $_POST["has_vacancies"]=="on") {
						$DB->AddValue("has_vacancies", 1);
					} else {
						$DB->AddValue("has_vacancies", 0);
					}
			        if(!empty($_POST["qualification"])) {
			            $DB->AddValue("qual_id", $_POST["qualification"]);
			        }

					if($DB->Insert()) {
						$this->output["messages"]["good"][] = "������� �������� ������������� ������� ��������.";
						if($_POST["type_sub"] == "secondary" && !empty($_POST["data_training"])) {
							$DB->SetTable($this->db_prefix."specialities");
							$DB->AddCondFS("id", "=", $_POST["spec_id"]);
							$DB->AddValue("data_training", $_POST["data_training"]);
							$DB->Update();
						}
						$Engine->LogAction($this->module_id, "spec_type", $_POST["spec_id"], "add");
						CF::Redirect($Engine->engine_uri."edit_speciality/".$_POST["spec_id"]."/");
					} else {
						$this->output["messages"]["bad"][] = "������� �������� ������������� �� ��������.";
					}
				}
			}
		}
		elseif(isset($pars[0], $pars[1]) && !empty($pars[0]) && $pars[0] == "del_spec_type" && CF::IsNaturalNumeric($pars[1])) {
			$res1 = $DB->Exec("SELECT count(`".$this->db_prefix."spec_type`.`id`) as 'count'  FROM `".$this->db_prefix."spec_type` WHERE `".$this->db_prefix."spec_type`.`spec_id` = (SELECT `spec_id` FROM `".$this->db_prefix."spec_type` WHERE `".$this->db_prefix."spec_type`.`id` = ".$pars[1].");");
            if($row1 = $DB->FetchAssoc($res1)) {
				if($row1['count'] > 1) {
					$DB->SetTable($this->db_prefix."spec_type");
					$DB->AddCondFS("id","=", $pars[1]);
					if($DB->Delete()) {
						$this->output["messages"]["good"][] = "������� �������� ������������� ������� ������.";
						$Engine->LogAction($this->module_id, "spec_type", $pars[1], "delete");
					} else {
						$this->output["messages"]["bad"][] = "������� �������� ������������� �� ������.";
					}
				}
			}
			CF::Redirect($Engine->engine_uri);
		}
		elseif(isset($pars[0], $pars[1]) && !empty($pars[0]) && $pars[0] == "del_profile" && CF::IsNaturalNumeric($pars[1])) {
			$DB->SetTable($this->db_prefix."profiles");
			$DB->AddCondFS("id", "=", $pars[1]);
			$res = $DB->Select();
			if($row = $DB->FetchAssoc($res)){
				$DB->SetTable($this->db_prefix."profiles");
				$DB->AddCondFS("id", "=", $pars[1]);
				if($DB->Delete()) {
					$Engine->LogAction($this->module_id, "profile", $pars[1], "delete");
					CF::Redirect($Engine->engine_uri."edit_speciality/".$row["spec_id"]);
				} else {
					$this->output["messages"]["bad"][] = "������� �������� ������������� �� ������.";
				}
			}
		}
		elseif(isset($pars[0], $pars[1]) && !empty($pars[0]) && $pars[0] == "add_profile" && CF::IsNaturalNumeric($pars[1])) {
			if (!empty($_POST["action_sub"]) && $_POST["action_sub"]=="add_profile") {
				if(!isset($_POST["profile_name"]) || $_POST["profile_name"] == ""){
					$this->output["messages"]["bad"][] = "������� �������� �������";
				}
				else {
					$DB->SetTable($this->db_prefix."profiles");
					$DB->AddValue("name", $_POST["profile_name"]);
					$DB->AddValue("spec_id", $_POST["spec_id"]);
					if($DB->Insert()) {
						$Engine->LogAction($this->module_id, "profile", $_POST["spec_id"], "add");
						CF::Redirect($Engine->engine_uri."edit_speciality/".$_POST["spec_id"]);
					} else {
						$this->output["messages"]["bad"][] = "������� �� ��������";
					}
				}
			}
			$this->output["sub_mode"] = "add_profile";
			$DB->SetTable($this->db_prefix."specialities");
			$DB->AddField("id");
			$DB->AddField("code");
			$DB->AddField("name");
			$DB->AddField("direction");
			$DB->AddField("description");

			$DB->AddCondFS("id", "=", $pars[1]);
			$res = $DB->Select();
			$this->output["spec_info"] = array();
			if($row = $DB->FetchAssoc($res)) {
				$this->output["spec_info"] = $row;
			};
		}
		elseif(isset($pars[0], $pars[1]) && !empty($pars[0]) && $pars[0] == "add_price" && CF::IsNaturalNumeric($pars[1])) {

			$id = (int)$pars[1];

			$DB->SetTable($this->db_prefix."spec_price");
			$DB->AddField("id");
			$DB->AddField("id_spec");
			$DB->AddField("id_edu_form");
			$DB->AddField("value");
			$DB->AddCondFS("id_spec", "=", $id);
			$res = $DB->SelectArray();
			$this->output["edu_price"] = $res ? $res : array();

			if (!empty($_POST["action_sub"]) && $_POST["action_sub"]=="add_price") {
				$priceList = array_slice($_POST, 1  );

				if($id && !empty($priceList)){
					$arrPrice = array();
					foreach($priceList as $k=>$v){
						$price = explode('_', $k);
						$price_id = (int)$price[1];
						$arrPrice[$price_id] = trim($v);
					}

					if(empty($this->output["edu_price"])){//insert
						foreach($arrPrice as $f=>$v){
							$DB->SetTable($this->db_prefix . "spec_price");
							$DB->AddValue("id_spec", $id);
							$DB->AddValue("id_edu_form", (int)$f);
							$DB->AddValue("value", (string)$v);
							$DB->Insert();
						}
						if($DB->LastInsertID()) {
							$this->output["messages"]["good"][] = "���� ���������.";
						} else {
							$this->output["messages"]["bad"][] = "���� �� ���������.";
						}
					}else{//update
						foreach($arrPrice as $f=>$v){
							$DB->SetTable($this->db_prefix . "spec_price");
							$DB->AddCondFS("id_spec", "=", $id);
							$DB->AddCondFS("id_edu_form", "=", (int)$f);
							$DB->AddValue("value", (string)$v);
							$DB->Update();
						}

						$this->output["messages"]["good"][] = "���� ���������.";
					}

					$DB->SetTable($this->db_prefix."spec_price");
					$DB->AddField("id");
					$DB->AddField("id_spec");
					$DB->AddField("id_edu_form");
					$DB->AddField("value");
					$DB->AddCondFS("id_spec", "=", $id);
					$this->output["edu_price"] = $DB->SelectArray();

				}else{
					$this->output["messages"]["bad"][] = "���� �� ���������.";
				}
			}

			$DB->SetTable($this->db_prefix."specialities");
			$DB->AddField("id");
			$DB->AddField("code");
			$DB->AddField("name");
			$DB->AddField("direction");
			$DB->AddField("description");

			$DB->AddCondFS("id", "=", $id);
			$res = $DB->Select();
			$this->output["spec_info"] = array();
			if($row = $DB->FetchAssoc($res)) {
				$this->output["spec_info"] = $row;
			};

			$DB->SetTable($this->db_prefix."edu_form");
			$DB->AddField("id");
			$DB->AddField("name");
			$DB->AddField("short_name");
			$res = $DB->SelectArray();
			$this->output["edu_form"] = array();
			if(!empty($res)){
				foreach($res as $row){
					$this->output["edu_form"][$row['id']] = $row;
				}
			}

			$this->output["sub_mode"] = "add_price";

		}

		elseif(isset($pars[0], $pars[1]) && !empty($pars[0]) && $pars[0] == "edit_speciality" && CF::IsNaturalNumeric($pars[1])) {
			$this->output["sub_mode"] = "edit_speciality";
			$DB->SetTable($this->db_prefix."school");
//		    $DB->AddOrder("name");
		    $DB->AddOrder("code");
			$res = $DB->Select();
			$this->output["direction"] = array();
			while ($row = $DB->FetchAssoc($res)) 
			{
				//����� ���� ������ ������ � ����� �����������
				$old_directory = explode (".", $row['code']);
				if (!empty($old_directory[1]))
			    $this->output["direction"][] = $row;
			};

			$DB->SetTable($this->db_prefix."faculties", "f");
			$DB->AddField("f.id");
			$DB->AddField("f.name");
			$res=$DB->Select();
			while ($row=$DB->FetchAssoc($res)) {
				$this->output["faculty"][]=$row;
			};

		      $file_types = array('����', '������� ����', '������ �������� ��������');
		      $this->output['file_types'] = $file_types;

		      $this->output["files"] = array();
		      $DB->SetTable("nsau_files");
		      $DB->AddCondFS("user_id", "=", $Auth->user_id);
		      $res = $DB->Select();
		      while($row = $DB->FetchAssoc($res)) {
		        // $this->output['files'][$row['id']] = $row;
		      }

			$DB->SetTable($this->db_prefix."specialities", "s");
			$DB->AddField("id");
			$DB->AddField("id_faculty");
			$DB->AddField("s.code");
			$DB->AddField("s.name");
			$DB->AddField("s.hide_accredit");
			$DB->AddField("s.direction");
			$DB->AddField("s.description");
			$DB->AddField("s.old");
		    $DB->AddField("s.old_id");
			$DB->AddCondFS("id", "=", $pars[1]);
			$res=$DB->Select();
			$this->output["speciality"]=array();
			if($row=$DB->FetchAssoc($res)) {
				$this->output["speciality"]=$row;
			};

		    /*$DB->SetTable($this->db_prefix."specialities");
			$DB->AddCondFS("old", "=", 1);

			$res_old = $DB->Select();
			while ($row_old = $DB->FetchAssoc($res_old)) {
				$this->output["old_specs"][$row_old["id"]] = $row_old;
			}
			*/

			$DB->SetTable($this->db_prefix."specialities", "s");
			$DB->AddTable($this->db_prefix."school", "sch");
			$DB->AddCondFS("s.old", "=", 1);
			$DB->AddCondFF("s.direction", "=", "sch.code");
			$DB->AddFields(array("s.id", "s.name", "s.code", "s.direction"));
			$DB->AddField("sch.name", "dir_name");
			$DB->AddField("sch.code", "dir_code");

			$res_old = $DB->Select();
			while ($row_old = $DB->FetchAssoc($res_old)) {
				$this->output["old_specs"][$row_old["direction"]]["specs"][$row_old["id"]] = $row_old;
				$this->output["old_specs"][$row_old["direction"]]["title"] = $row_old["dir_name"];
				$this->output["old_specs"][$row_old["direction"]]["code"] = $row_old["dir_code"];
			}

			$DB->SetTable($this->db_prefix."spec_type","st");
			$DB->AddTable($this->db_prefix."specialities","spec");
			$DB->AddField("st.id");
			$DB->AddField("st.spec_id");
			$DB->AddField("st.year_open");
			$DB->AddField("st.internal_tuition");
			$DB->AddField("st.correspondence_tuition");
			$DB->AddField("st.internal_correspondence_tuition");
			$DB->AddField("st.type");
			$DB->AddField("st.data_training");
			$DB->AddField("st.data_training_ex");
			$DB->AddField("st.data_training_int_ex");
			$DB->AddField("st.accreditation");
			$DB->AddField("st.qualification");
			$DB->AddField("st.description");
			$DB->AddField("st.has_vacancies");
			$DB->AddField("st.qual_id");
			$DB->AddCondFF("st.code", "=", "spec.code");
			$DB->AddCondFF("st.spec_id", "=", "spec.id");
			$DB->AddCondFS("st.spec_id", "=", $pars[1]);
			$res=$DB->Select();
			$this->output["speciality_edit"]=array();
			while ($row=$DB->FetchAssoc($res)) {
				$row["accreditation"] = implode(".", array_reverse(explode("-", $row["accreditation"])));
				$this->output["speciality_edit"][]=$row;
			};


			$DB->SetTable($this->db_prefix."exams", "ex");
			$DB->AddTable($this->db_prefix."abit_edu_level", "el");
			$DB->AddCondFF("ex.edu_level_id", "=", "el.id");
			$DB->AddField("ex.id");
			$DB->AddField("ex.name");
			$DB->AddField("ex.n_pp");
			$DB->AddField("ex.spec");
			$DB->AddField("ex.min_vyz");
			$DB->AddField("ex.max_vyz");
			$DB->AddField("ex.min_mins");
			$DB->AddField("ex.prikaz");
			$DB->AddField("ex.required");
			$DB->AddField("el.short_name");
			$res = $DB->Select();

			$need_exam = 1;
			while($row = $DB->FetchAssoc($res))	{
				$DB->SetTable($this->db_prefix."spec_exams");
				$DB->AddCondFS("spec_id", "=", $pars[1]);
				$DB->AddCondFS("exam_id", "=", $row["id"]);
				$res2 = $DB->Select();
				if($row2 = $DB->FetchAssoc($res2)) {
					$row["need"] = ($row2["pos"] ? $row2["pos"] : $need_exam++ );
					$row['type'] = $row2['type'];
				} else {
					$row["need"] = 0;
				};
				$this->output["exams"][] = $row;
			}

			//additional exams
			$DB->SetTable($this->db_prefix."spec_exams_additional");
			$DB->AddCondFS("spec_id", "=", $pars[1]);
			$DB->AddField("exam_id");
			$DB->AddField("pos");
			$res_addit = $DB->Select();
			while ($row_addit = $DB->FetchAssoc($res_addit)) {
				foreach($this->output["exams"] as $k=>$exam){
					if($exam['id'] === $row_addit['exam_id']){
						$this->output["exams"][$k]['additonal'] = 1;
					}
				}
			}
//			ksort($this->output["exams_additonal"], SORT_NUMERIC);

			$DB->SetTable($this->db_prefix."qualifications");

			$res_qual = $DB->Select();
			while ($row_qual = $DB->FetchAssoc($res_qual)) {
				$this->output["qualifications"][$row_qual["id"]] = $row_qual;
			}

			$DB->SetTable($this->db_prefix."profiles");
			$DB->AddCondFS("spec_id", "=", $pars[1]);
			$res = $DB->Select();
			while($row = $DB->FetchAssoc($res))	{
				$this->output["profiles"][] = $row;
			}

		    $this->output["spec_files"] = array();
		    $DB->SetTable($this->db_prefix."spec_files");
		    $DB->AddCondFS("spec_id", "=", $pars[1]);
		    $res = $DB->Select();
			while($row = $DB->FetchAssoc($res))	{
				$this->output["spec_files"][$row["file_type"]][$row["qual_id"]] = $row;
			    $DB->SetTable("nsau_files");
			    $DB->AddCondFS("id", "=", $row["file_id"]);
			    $res_file = $DB->Select();
			    while($row_file = $DB->FetchAssoc($res_file)) {
			      $this->output['files'][$row_file['id']] = $row_file;
			    }
			}

			//price
			$DB->SetTable($this->db_prefix."spec_price", "sp");
			$DB->AddTable($this->db_prefix."edu_form", "ef");
			$DB->AddCondFS("sp.id_spec", "=", $pars[1]);
			$DB->AddCondFF("sp.id_edu_form", "=", "ef.id");
			$DB->AddField("sp.id");
			$DB->AddField("sp.id_spec");
			$DB->AddField("sp.id_edu_form");
			$DB->AddField("sp.value");
			$DB->AddField("ef.name");
			$DB->AddField("ef.short_name");
			$res = $DB->SelectArray();
			$this->output["edu_price"] = $res ? $res : array();

			//banner
			$DB->SetTable($this->db_prefix."spec_banner", "sb");
			$DB->AddCondFS("sb.id_spec", "=", $pars[1]);
			$DB->AddField("sb.id_spec");
			$DB->AddField("sb.id_ban");
			$DB->AddField("sb.value");
			$res = $DB->SelectArray();
			$this->output["edu_banner"] = $res ? $res : array();


			if (!empty($_POST["spec_id"]))	{

				$DB->SetTable($this->db_prefix."specialities", "s");
				$DB->AddCondFS("s.id", "=", $_POST["spec_id"]);
				$DB->AddValue("s.code", $_POST["code"]);
				if(isset($_POST["faculty"])) {
					$DB->AddValue("s.id_faculty", $_POST["faculty"]);
				}
				$DB->AddValue("s.name", $_POST["speciality_name"]);
				$DB->AddValue("s.direction", $_POST["direction"]);
				$DB->AddValue("s.description", $_POST["description"]);

		        if(preg_match('/\d{2}\.\d{2}\.\d{2}/', $_POST['code'])) {
		          $DB->AddValue('old', 0);
		        }

		        $DB->AddValue("old_id", (int)$_POST["old_spec"]);

		        if ($_POST["hide_accredit"])
		        {
		        	$DB->AddValue("hide_accredit", 1);
		        }
		        else
		        {
		        	$DB->AddValue("hide_accredit", 0);
		        }

				if($DB->Update()) {
					$Engine->LogAction($this->module_id, "specialities", $_POST["spec_id"], "update");
					foreach ($_POST['spec_type'] as $type => $edit_type ) {
						$DB->SetTable($this->db_prefix."spec_type", "st");
						$DB->AddCondFS("st.spec_id", "=", $_POST["spec_id"]);
						$DB->AddCondFS("st.id", "=", $edit_type["st_type_id"]);
						$DB->AddValue("st.code", $_POST["code"]);
						$DB->AddValue("st.type", $edit_type["type"]);
						$DB->AddValue("st.type_code", $spec_type[1][$edit_type["type"]]);
						$DB->AddValue("st.data_training", $edit_type["data_training"]);
						$DB->AddValue("st.data_training_ex", $edit_type["data_training_ex"]);
						$DB->AddValue("st.data_training_int_ex", $edit_type["data_training_int_ex"]);
						$DB->AddValue("st.qualification", $edit_type["qualification"]);
						// if($this->checkDate($edit_type["accreditation"]))
							$DB->AddValue("st.accreditation", implode("-", array_reverse(explode(".", $edit_type["accreditation"]))));
			            $DB->AddValue("st.qual_id", $edit_type["qual_id"]);
						$DB->AddValue("st.description", $edit_type["description"]);
						$DB->AddValue("st.year_open", $edit_type["year_open"]);

						if(isset($edit_type["has_vacancies"]) && $edit_type["has_vacancies"]=='on') {
							$DB->AddValue("st.has_vacancies", 1);
						} else {
							$DB->AddValue("st.has_vacancies", 0);
						};
						if(isset($edit_type["internal_tuition"]) && $edit_type["internal_tuition"]=='on') {
							$DB->AddValue("st.internal_tuition", 1);
						} else {
							$DB->AddValue("st.internal_tuition", 0);
						};
						if(isset($edit_type["correspondence_tuition"]) && $edit_type["correspondence_tuition"]=='on') {
							$DB->AddValue("st.correspondence_tuition", 1);
						} else {
							$DB->AddValue("st.correspondence_tuition", 0);
						};
						if(isset($edit_type["internal_correspondence_tuition"]) && $edit_type["internal_correspondence_tuition"]=='on') {
							$DB->AddValue("st.internal_correspondence_tuition", 1);
						} else {
							$DB->AddValue("st.internal_correspondence_tuition", 0);
						};
						if($DB->Update()) {
							$Engine->LogAction($this->module_id, "spec_type", $edit_type["st_type_id"], "update");
							if(!empty($edit_type["qual_id"])) {
							$DB->SetTable($this->db_prefix."spec_exams");
							$DB->AddCondFS("spec_id", "=", $_POST["spec_id"]);
							$DB->AddCondFS("qual_id", "=", $edit_type["qual_id"]);
							$DB->Delete();
							$DB->SetTable($this->db_prefix."exams");
							$res = $DB->Select();

							// Exams
							$position = 1;
							$exam_base = array('obligatory', 'prof');
							foreach($edit_type["exams"] as $exam_type => $exam_id) {
								$DB->SetTable($this->db_prefix."qualifications");
								$DB->AddCondFS("id", "=", $edit_type["qual_id"]);
								$res_qual = $DB->Select(1);
								while ($row_qual = $DB->FetchAssoc($res_qual)) {
									$qual_name = $row_qual["name"];
								}

								$DB->SetTable($this->db_prefix . "spec_exams");
								$DB->AddValue("spec_id", $_POST["spec_id"]);
								$DB->AddValue("spec_type_id", $edit_type['st_type_id']);
								$DB->AddValue("speciality_code", $_POST["code"]);
								$DB->AddValue("qual_id", $edit_type["qual_id"]);
								$DB->AddValue("type", $edit_type['type']);
								$DB->AddValue("qualification", $qual_name);
								$DB->AddValue("pos", $position++);

								if(in_array($exam_type, $exam_base) && !is_array($exam_id)){//base exam
									$DB->AddValue("exam_id", $exam_id);
									$DB->Insert();
								}elseif($exam_type == 'addictional'){//additional exams
									if(is_array($exam_id) && count($exam_id)){
										$pos_addit = 1;
										$DB->SetTable($this->db_prefix."spec_exams_additional");
										$DB->AddCondFS("spec_id", "=", $_POST["spec_id"]);
										$DB->Delete();

										foreach($exam_id as $k=>$additional_id){
											$DB->SetTable($this->db_prefix."spec_exams_additional");
											$DB->AddValue("spec_id", $_POST["spec_id"]);
											$DB->AddValue("exam_id", $additional_id);
											$DB->AddValue("pos", $pos_addit);
											$DB->Insert();
											$pos_addit++;
										}
									}

								}

							}
							// while ($row = $DB->FetchAssoc($res)) {
								// if(isset($edit_type['exams'][$row["id"]]) && $edit_type['exams'][$row["id"]] == "on") {
								// $DB->SetTable($this->db_prefix."qualifications");
									// $DB->AddCondFS("id", "=", $edit_type["qual_id"]);
									// $res_qual = $DB->Select(1);
									// while ($row_qual = $DB->FetchAssoc($res_qual)) {
										// $qual_name = $row_qual["name"];
									// }
									// $DB->SetTable($this->db_prefix."spec_exams");
									// $DB->AddValue("spec_id", $_POST["spec_id"]);
									// $DB->AddValue("spec_type_id", $edit_type['st_type_id']);
									// $DB->AddValue("speciality_code", $_POST["code"]);
									// $DB->AddValue("exam_id", $row["id"]);
									// $DB->AddValue("qual_id", $edit_type["qual_id"]);
									// $DB->AddValue("type", $edit_type['type']);//echo $DB->InsertQuery();
									// $DB->AddValue("qualification", $qual_name);

									// if($DB->Insert()) {
										// 1;
									// }
								// }
							// }
							}
						} else {
							$this->output["messages"]["bad"][] = "������ ��� �������������� �������������.";
						};

						/*$DB->SetTable($this->db_prefix."exams");
						$res = $DB->Select();
						while ($row = $DB->FetchAssoc($res)) {
							foreach($edit_type["exams"] as $exam) {
								if(isset($edit_type['exams'][$row["id"]]) && $edit_type['exams'][$row["id"]] == "on") {
									$DB->SetTable($this->db_prefix."spec_exams");
									$DB->AddCondFS("spec_id", "=", $_POST["spec_id"]);
									$DB->AddCondFS("exam_id", "=", $row["id"]);
									$DB->AddCondFS("qual_id", "=", $type);echo $DB->SelectQuery();
									$res_se = $DB->Select();
									if (!($row_se = $DB->FetchAssoc($res_se))) {
										$DB->SetTable($this->db_prefix."spec_exams");
										$DB->AddValue("spec_id", $_POST["spec_id"]);
										$DB->AddValue("spec_type_id", $edit_type['st_type_id']);
										$DB->AddValue("speciality_code", $_POST["code"]);
										$DB->AddValue("exam_id", $row["id"]);
										$DB->AddValue("qual_id", $type);
										$DB->AddValue("type", $edit_type['type']);echo $DB->InsertQuery();
										if($DB->Insert()) {
											1;
										}
									}
								} else {
									$DB->SetTable($this->db_prefix."spec_exams");
									$DB->AddCondFS("spec_id", "=", $_POST["spec_id"]);
									$DB->AddCondFS("exam_id", "=", $row["id"]);
									$DB->AddCondFS("qual_id", "=", $type);
									//$DB->Delete();
								}
							}
						}*/
					}
					$DB->SetTable($this->db_prefix."exams");
					$res = $DB->Select();
					while ($row = $DB->FetchAssoc($res)) {
						if(isset($_POST['spec_type']["secondary"]['exams'][$row["id"]]) && $_POST['spec_type']["secondary"]['exams'][$row["id"]] == "on" && $_POST['spec_type']["secondary"]['type'] != "magistracy") {
							$DB->SetTable($this->db_prefix."spec_exams");
							$DB->AddCondFS("spec_id", "=", $_POST["spec_id"]);
							$DB->AddCondFS("exam_id", "=", $row["id"]);
							$DB->AddCondFS("type", "=", "secondary");
							$res_se = $DB->Select();
							if (!($row_se = $DB->FetchAssoc($res_se))) {
								$DB->SetTable($this->db_prefix."spec_exams");
								$DB->AddValue("spec_id", $_POST["spec_id"]);
								$DB->AddValue("spec_type_id", $_POST['spec_type']["secondary"]['st_type_id']);
								$DB->AddValue("speciality_code", $_POST["code"]);
								$DB->AddValue("exam_id", $row["id"]);
								$DB->AddValue("type", $_POST['spec_type']["secondary"]['type']);
								if($DB->Insert()) {
									1;
								}
							}
						} else {
							$DB->SetTable($this->db_prefix."spec_exams");
							$DB->AddCondFS("spec_id", "=", $_POST["spec_id"]);
							$DB->AddCondFS("exam_id", "=", $row["id"]);
							$DB->AddCondFS("type", "=", "secondary");
							$DB->AddCondFS("qual_id", "=", 0);
							$DB->Delete();
						}

						if (isset($_POST['spec_type']["bachelor"]['exams'][$row["id"]]) && $_POST['spec_type']["bachelor"]['exams'][$row["id"]] == "on" && $_POST['spec_type']["bachelor"]['type'] != "magistracy") {
							$DB->SetTable($this->db_prefix."spec_exams");
							$DB->AddCondFS("spec_id", "=", $_POST["spec_id"]);
							$DB->AddCondFS("exam_id", "=", $row["id"]);
							$DB->AddCondFS("type", "=", "bachelor");
							$res_se = $DB->Select();
							if (!($row_se = $DB->FetchAssoc($res_se))) {
								$DB->SetTable($this->db_prefix."spec_exams");
								$DB->AddValue("spec_id", $_POST["spec_id"]);
								$DB->AddValue("spec_type_id", $_POST['spec_type']["bachelor"]['st_type_id']);
								$DB->AddValue("speciality_code", $_POST["code"]);
								$DB->AddValue("exam_id", $row["id"]);
								$DB->AddValue("type", $_POST['spec_type']["bachelor"]['type']);
								$DB->Insert();
							}
						} else {
							$DB->SetTable($this->db_prefix."spec_exams");
							$DB->AddCondFS("spec_id", "=", $_POST["spec_id"]);
							$DB->AddCondFS("exam_id", "=", $row["id"]);
							$DB->AddCondFS("type", "=", "bachelor");
							$DB->AddCondFS("qual_id", "=", 0);
							$DB->Delete();
						}
						if (isset($_POST['spec_type']["magistracy"]['exams'][$row["id"]]) && $_POST['spec_type']["magistracy"]['exams'][$row["id"]] == "on" && $_POST['spec_type']["magistracy"]['type'] != "magistracy") {
							$DB->SetTable($this->db_prefix."spec_exams");
							$DB->AddCondFS("spec_id", "=", $_POST["spec_id"]);
							$DB->AddCondFS("exam_id", "=", $row["id"]);
							$DB->AddCondFS("type", "=", "magistracy");
							$res_se = $DB->Select();
							if (!($row_se = $DB->FetchAssoc($res_se))) {
								$DB->SetTable($this->db_prefix."spec_exams");
								$DB->AddValue("spec_id", $_POST["spec_id"]);
								$DB->AddValue("spec_type_id", $_POST['spec_type']["magistracy"]['st_type_id']);
								$DB->AddValue("speciality_code", $_POST["code"]);
								$DB->AddValue("exam_id", $row["id"]);
								$DB->AddValue("type", $_POST['spec_type']["magistracy"]['type']);
								$DB->Insert();
							}
						} else {
							$DB->SetTable($this->db_prefix."spec_exams");
							$DB->AddCondFS("spec_id", "=", $_POST["spec_id"]);
							$DB->AddCondFS("exam_id", "=", $row["id"]);
							$DB->AddCondFS("type", "=", "magistracy");
							$DB->AddCondFS("qual_id", "=", 0);
							$DB->Delete();
						}
						if (isset($_POST['spec_type']["higher"]['exams'][$row["id"]]) && $_POST['spec_type']["higher"]['exams'][$row["id"]] == "on" && $_POST['spec_type']["higher"]['type'] != "magistracy") {
							$DB->SetTable($this->db_prefix."spec_exams");
							$DB->AddCondFS("spec_id", "=", $_POST["spec_id"]);
							$DB->AddCondFS("exam_id", "=", $row["id"]);
							$DB->AddCondFS("type", "=", "higher");
							$res_se = $DB->Select();
							if (!($row_se = $DB->FetchAssoc($res_se))) {
								$DB->SetTable($this->db_prefix."spec_exams");
								$DB->AddValue("spec_id", $_POST["spec_id"]);
								$DB->AddValue("spec_type_id", $_POST['spec_type']["higher"]['st_type_id']);
								$DB->AddValue("speciality_code", $_POST["code"]);
								$DB->AddValue("exam_id", $row["id"]);
								$DB->AddValue("type", $_POST['spec_type']["higher"]['type']);
								$DB->Insert();
							}
						} else {
							$DB->SetTable($this->db_prefix."spec_exams");
							$DB->AddCondFS("spec_id", "=", $_POST["spec_id"]);
							$DB->AddCondFS("exam_id", "=", $row["id"]);
							$DB->AddCondFS("type", "=", "higher");
							$DB->AddCondFS("qual_id", "=", 0);
							$DB->Delete();
						}
						$DB->FreeRes();
						if(isset($_POST['profile'])){
							foreach($_POST['profile'] as $id => $profile){
								$DB->SetTable($this->db_prefix."profiles");
								$DB->AddCondFS("id", "=", $id);
								$DB->AddValue("spec_id", $_POST["spec_id"]);
								$DB->AddValue("name", $profile["name"]);
								//echo $DB->UpdateQuery();exit;
								if($DB->Update()) {
									$Engine->LogAction($this->module_id, "profile", $profile["id"], "update");
								} else {
									$this->output["messages"]["bad"][] = "������ ��� �������������� �������������.";
								};
							}
						}
					}

			        if(isset($_POST['spec_file'])) {
			          $DB->SetTable($this->db_prefix."spec_files");
			          $DB->AddCondFS("spec_id", "=", $_POST["spec_id"]);
			          $DB->Delete();
			          foreach($_POST['spec_file'] as $file_type => $quals) {
			            foreach($quals as $spec_type_id => $file_id){
			              if($file_id == 0) continue;
			              $qual_id = $_POST['spec_type'][$spec_type_id]['qual_id'];
			              $DB->SetTable($this->db_prefix."spec_files");
			              $DB->AddValue("spec_id", $_POST["spec_id"]);
			              $DB->AddValue("qual_id", $qual_id);
			              $DB->AddValue("file_type", $file_type);
			              $DB->AddValue("file_id", $file_id);
			              $DB->Insert();
			            }
			          }
			        }

			        //banner
			        if(!empty($this->output["edu_banner"])){//update
				        foreach($_POST['banner'] as $b=>$v){
					        $DB->SetTable($this->db_prefix."spec_banner");
					        $DB->AddCondFS("id_spec", "=", (int)$_POST["spec_id"]);
					        $DB->AddCondFS("id_ban", "=", (int)$b);
					        $DB->AddValue("value", trim($v));
					        $DB->Update();
						}
			        }else{//insert
						foreach($_POST["banner"] as $b=>$v){
							$DB->SetTable($this->db_prefix."spec_banner");
							$DB->AddValue("id_spec", (int)$_POST["spec_id"]);
							$DB->AddValue("id_ban", (int)$b);
							$DB->AddValue("value", trim($v));
							$DB->Insert();
						}
			        }

					$this->output["messages"]["good"][] = "�������������� ������ �������.";
					CF::Redirect($Engine->engine_uri);
				} else {
					$this->output["messages"]["bad"][] = "������ ��� �������������� �������������.";
				}
			}
		}
		elseif(isset($pars[0]) && !empty($pars[0]) && $pars[0] == "manage_qualifications") {
			$this->output["sub_mode"] = "manage_qualifications";

			if(isset($_SESSION[$pars[0]]["messages"])) {
				$this->output["messages"] = $_SESSION[$pars[0]]["messages"];
				unset($_SESSION[$pars[0]]["messages"]);
			}

			$DB->SetTable($this->db_prefix."qualifications");
			$res = $DB->Select();
			while ($row = $DB->FetchAssoc($res)) {
				$this->output["qualifications"][$row['id']] = $row;
			}

			if(isset($_POST["name"]) && isset($_POST["action"]) && $_POST["action"] == "add_qualification") {
				$DB->SetTable($this->db_prefix."qualifications");
				$DB->AddValue("name", $_POST["name"]);
				$DB->AddValue("old_type", $_POST["type"]);

				if($DB->Insert()) {
					$this->output["messages"]["good"][] = 107;
				}
				$Engine->LogAction($this->module_id, "qualification", $_POST["name"], "add");
				$_SESSION[$pars[0]]["messages"] =  $this->output["messages"];
				CF::Redirect($Engine->unqueried_uri);
			}
			if(isset($_POST["name"]) && isset($_POST["action"]) && $_POST["action"] == "save_qualifications") {
				$ids = isset($_POST["id"]) ? $_POST["id"] : array();
				foreach ($ids as $id) {
					$DB->SetTable($this->db_prefix."qualifications");
					$DB->AddValue("name", $_POST["name"][$id]);
					$DB->AddValue("old_type", $_POST["type"][$id]);
					$DB->AddCondFS("id", "=", $id);
					//echo $DB->UpdateQuery();exit;

					$DB->Update();
				}
				$Engine->LogAction($this->module_id, "qualification", $id, "update");
				CF::Redirect($Engine->unqueried_uri);
			}
			if(isset($_POST["name"]) && isset($_POST["action"]) && $_POST["action"] == "edit_qualification") {
				$DB->SetTable($this->db_prefix."qualifications");
				$DB->AddValue("name", $_POST["name"]);
				$DB->AddCondFS("id", "=", $_POST["id"]);
				if($DB->Update()) {
					$this->output["messages"]["good"][] = 108;

				}
				$Engine->LogAction($this->module_id, "qualification", $_POST["id"], "update");
				$_SESSION[$pars[0]]["messages"] =  $this->output["messages"];
				CF::Redirect($Engine->unqueried_uri);
			}

			if(isset($_POST["name"]) && isset($_POST["action"]) && $_POST["action"] == "delete_qualification") {
				$DB->SetTable($this->db_prefix."qualifications");
				$DB->AddCondFS("id", "=", $_POST["id"]);
				if($DB->Delete()) {
					$this->output["messages"]["good"][] = 109;

				}
				$Engine->LogAction($this->module_id, "qualification", $_POST["id"], "delete");
				$_SESSION[$pars[0]]["messages"] =  $this->output["messages"];
				CF::Redirect($Engine->unqueried_uri);
			}
		}
		elseif(isset($pars[0]) && !empty($pars[0])) {
			$Engine->HTTP404();
			return;
		}
		else {
			/*if(!empty($_GET["code"]) && $_GET["action"]=="delete") {
				if(!empty($_GET["section"]) && $_GET["section"]=="1")  {
					$DB->SetTable($this->db_prefix."specialities");
					$DB->AddCondFS("code","=", $_GET["code"]);
					//$DB->AddCondFS("name","=", $_GET["name"]);
					if($DB->Delete())  {
						$DB->SetTable($this->db_prefix."spec_type");
						$DB->AddCondFS("code","=", $_GET["code"]);
						if($DB->Delete())
							$this->output["messages"]["good"][] = 105;
						else
							$this->output["messages"]["bad"][] = 310;
					}
					else {
						$this->output["messages"]["bad"][] = 310;
					}
					$DB->SetTable($this->db_prefix."spec_type");
					$DB->AddCondFS("code","=", $_GET["code"]);
					$DB->Delete();
				}
				elseif(!empty($_GET["subsection"]) && $_GET["subsection"]=="1") {
					$DB->SetTable($this->db_prefix."spec_type");
					$DB->AddCondFS("code","=", $_GET["code"]);
					$DB->AddCondFS("type","=", $_GET["type"]);
					$DB->AddCondFS("year_open","=", $_GET["year_open"]);
					if($DB->Delete()) {
						$this->output["messages"]["good"][] = 104;
					}
					else {
						$this->output["messages"]["bad"][] = 309;
					}
				}
			} */

			$res = $DB->Exec("SELECT st.id, st.code, st.name, st.id_faculty, st.direction, st.description, st.old, f.name as fname FROM ".$this->db_prefix."specialities as st left join ".$this->db_prefix."faculties as f on st.id_faculty = f.id ".(isset($_GET["old"]) ? " WHERE st.old=".(int)$_GET["old"] : "")." order by st.code");
			$this->output["speciality_directory"] = array();
			while ($row = $DB->FetchAssoc($res)) {
				if ($Engine->OperationAllowed($this->module_id, "specialities.handle", (int)$row["code"], $Auth->usergroup_id))
					$row["handle"] = 1;
				else
					$row["handle"] = 0;
				if($Engine->OperationAllowed($this->module_id, "specialities.faculty.handle", (int)$row["id_faculty"], $Auth->usergroup_id))
					$row["handle"] = true;
				$DB->SetTable($this->db_prefix."spec_type");
				$DB->AddField("id");
				$DB->AddField("code");
				$DB->AddField("year_open");
				$DB->AddField("type");
				$DB->AddField("has_vacancies");
				$DB->AddField("qual_id");
				$DB->AddCondFS("spec_id","=", $row['id']);
				$res_type = $DB->Select();
				$this->output["speciality"] = array();
				$row['spec_type_count'] = 0;
				while ($row_type = $DB->FetchAssoc($res_type)) {
					//$row['spec_type'][] = $row_type;
					//$this->output["speciality"][] = $row;
					$row['spec_type_count'] += 1;

					$DB->SetTable($this->db_prefix."qualifications");
					$DB->AddCondFS("id", "=", $row_type['qual_id']);

					$res_qual = $DB->Select(1);
					while($row_qual = $DB->FetchAssoc($res_qual)) {
						$row_type['qualification'] = $row_qual['name'];
					}

					$row['spec_type'][] = $row_type;//print_r($row_type);
				}

				if($row["handle"] == 1){
					$this->output["speciality_directory"][$row["id_faculty"]]["spec"][$row["id"]] = $row;
					$this->output["speciality_directory"][$row["id_faculty"]]["name"] = $row["fname"];
				}
			}
			/*if (!count($this->output["speciality"]))
				$Engine->HTTP404();*/

			$DB->SetTable($this->db_prefix."school","s");
//		    $DB->AddOrder("s.name");
		    $DB->AddOrder("s.code");
			$res = $DB->Select();
			$this->output["directory"] = array();
			while ($row = $DB->FetchAssoc($res)) {
				$this->output["directory"][] = $row;
			}

			/*$DB->SetTable("engine_privileges");
			$DB->AddCondFS("operation_name", "=", "specialities.handle");
			$DB->AddCondFS("usergroup_id", "=", $Auth->usergroup_id);

			$res = $DB->Select();
			while ($row = $DB->FetchAssoc($res))
				$this->output["specs_to_handle"][] = $row["entry_id"];*/
		}
    }
    ///////////////////////////////////////////////

    function speciality($module_uri)
    {
        global $DB, $Engine, $Auth;

        $spec_type = array(
			'0' => array('51'=>'secondary','68'=>'magistracy','62'=>'bachelor','65'=>'higher'),
			'1' => array('secondary'=>'51','magistracy'=>'68','bachelor'=>'62','higher'=>'65')
		);

    $spec_type_name = array('secondary' => '���', 'bachelor' => '�����������', 'magistracy' => '������������', 'higher' => '�����������', 'graduate' => '�����������');
    $this->output["spec_type_name"] = $spec_type_name;


    	$module_uri =iconv("UTF-8", "windows-1251", urldecode($module_uri));
		$parts = explode ("/", $module_uri);
		if(isset($_GET['action']) && $_GET['action'] == 'add_subsection' && isset($_GET['id'])) {
			if(!empty($parts[0]) && (CF::IsNaturalNumeric($parts[0]) || preg_match('/\d{2}\.\d{2}\.\d{2}/', $parts[0]) ) && CF::IsNaturalNumeric($_GET['id'])) {
				$DB->SetTable($this->db_prefix."specialities");
				$DB->AddField("id");
				$DB->AddField("code");
				$DB->AddField("name");
				$DB->AddField("direction");
				$DB->AddField("description");

				$DB->AddCondFS("code", "=", "36.03.02(����)");
				$DB->AddCondFS("id", "=", $_GET['id']);
				$res = $DB->Select();
				$this->output["spec_info"] = array();
				if($row = $DB->FetchAssoc($res)) {
					$this->output["spec_info"] = $row;
				};
			};
		};

		if (!empty($_POST["action_sub"]) && $_POST["action_sub"]=="add_subsection") {
            /*$DB->SetTable($this->db_prefix."spec_type");
			$DB->AddCondFS("code", "=", $_POST["code"]);
			$DB->AddCondFS("type", "=", $_POST["type_sub"]);
			$res =  = $DB->Select();
			if($row = $DB->FetchAssoc($res)) {
				echo "error";
			} else {
				echo "ok";
			}*/
            $DB->SetTable($this->db_prefix."spec_type");
            $DB->AddValue("spec_id", $_POST["spec_id"]);
            $DB->AddValue("code", $_POST["code"]);
            $DB->AddValue("type", $_POST["type_sub"]);
            $DB->AddValue("year_open", $_POST["year_open_sub"]);
						if(isset($_POST["data_training"]) && !empty($_POST["data_training"])) {
							$DB->AddValue("data_training", $_POST["data_training"]);
						}
						if(isset($_POST["data_training_ex"]) && !empty($_POST["data_training_ex"])) {
							$DB->AddValue("data_training_ex", $_POST["data_training_ex"]);
						}
						if(isset($_POST["data_training_int_ex"]) && !empty($_POST["data_training_int_ex"])) {
							$DB->AddValue("data_training_int_ex", $_POST["data_training_int_ex"]);
						}
            if(!empty($_POST["internal_tuition_sub"]) && $_POST["internal_tuition_sub"]=="on") {
                $DB->AddValue("internal_tuition", 1);
            } else {
                $DB->AddValue("internal_tuition", 0);
			}
            if(!empty($_POST["correspondence_tuition_sub"]) && $_POST["correspondence_tuition_sub"]=="on") {
                $DB->AddValue("correspondence_tuition", 1);
            } else {
                $DB->AddValue("correspondence_tuition", 0);
            }
            if(!empty($_POST["internal_correspondence_tuition_sub"]) && $_POST["internal_correspondence_tuition_sub"]=="on") {
                $DB->AddValue("internal_correspondence_tuition", 1);
            } else {
                $DB->AddValue("internal_correspondence_tuition", 0);
            }
            if(!empty($_POST["has_vacancies"]) && $_POST["has_vacancies"]=="on") {
            	$DB->AddValue("has_vacancies", 1);
            } else {
				$DB->AddValue("has_vacancies", 0);
            }
            if($DB->Insert()) {
                $this->output["messages"]["good"][] = 102;
				if($_POST["type_sub"] == "secondary" && !empty($_POST["data_training"])) {
					$DB->SetTable($this->db_prefix."specialities", "s");
					$DB->AddCondFS("s.code", "=", $_POST["code"]);
					$DB->AddValue("data_training", $_POST["data_training"]);
					$DB->Update();
				}
				$Engine->LogAction($this->module_id, "spec_type", $_POST["spec_id"], "add");
            } else {
                $this->output["messages"]["bad"][] = 307;
            }
        }

		if (!empty($_POST["code_add"]) && $_POST["action_op"]=="add") { //���������� ������������� � ������� ����������
            $DB->SetTable($this->db_prefix."specialities");
            $DB->AddValue("id_faculty", 0);
            $DB->AddValue("code", $_POST["code_add"]);
            $DB->AddValue("name", $_POST["speciality_name"]);
            $DB->AddValue("direction", $_POST["direction"]);
            $DB->AddValue("description", $_POST["description"]);
            $DB->AddValue("year_open", $_POST["year_open"]);
            /*if(empty($_POST["internal_tuition"]))
                $DB->AddValue("internal_tuition", 0);
            else
                $DB->AddValue("internal_tuition", $_POST["internal_tuition"]);

            if(empty($_POST["correspondence_tuition"]))
                $DB->AddValue("correspondence_tuition", 0);
            else
				$DB->AddValue("correspondence_tuition", $_POST["correspondence_tuition"]);*/

            if(!empty($_POST["data_training"]) && $_POST["type"] == "secondary") {
                $DB->AddValue("data_training", $_POST["data_training"]);
			};

            /*$DB->AddValue("higher", 0);
            $DB->AddValue("secondary", 0);
            $DB->AddValue("bachelor", 0);
            $DB->AddValue("magistracy", 0); */

            if($DB->Insert(true, true)) {
				$spec_id = $DB->LastInsertID();

				$Engine->LogAction($this->module_id, "specialities", $spec_id, "add2");
                $DB->SetTable($this->db_prefix."spec_type");
                $DB->AddValue("spec_id", $spec_id);
                $DB->AddValue("code", $_POST["code_add"]);
                $DB->AddValue("type", $_POST["type"]);
                $DB->AddValue("year_open", $_POST["year_open"]);
				//$DB->AddValue("has_vacancies", $_POST["has_vacancies"]);

				if(!empty($_POST["has_vacancies"]) && $_POST["has_vacancies"]=="on") {
					$DB->AddValue("has_vacancies", 1);
				} else {
					$DB->AddValue("has_vacancies", 0);
				}
                if(empty($_POST["internal_tuition"])) {
                    $DB->AddValue("internal_tuition", 0);
                } else {
                    $DB->AddValue("internal_tuition", 1);
				}
                if(empty($_POST["correspondence_tuition"])) {
                    $DB->AddValue("correspondence_tuition", 0);
                } else {
                    $DB->AddValue("correspondence_tuition", 1);
				}

                if($DB->Insert()) {
                    $this->output["messages"]["good"][] = 103;
					$Engine->LogAction($this->module_id, "spec_type", $spec_id, "add3");
                } else {
                    $this->output["messages"]["bad"][] = 311;
                }
				$DB->SetTable($this->db_prefix."specialities", "s");
				$DB->AddField("s.code");
				$DB->AddField("s.name");
				$DB->AddCondFS("s.code", "=", $_POST["code_add"]);
				$res = $DB->Select();
				$this->output["specialities"] = array();
				while($row = $DB->FetchAssoc($res)) {
					$this->output["specialities"][] = $row;
				}
            } else {
                $this->output["messages"]["bad"][] = 308;
                $this->output["messages"]["bad"][] = 334;
            }
        }

        $DB->SetTable($this->db_prefix."school","s");
        $DB->AddOrder("s.name");
        $DB->AddOrder("s.code");
        $res = $DB->Select();
        $this->output["directory"] = array();
        while ($row = $DB->FetchAssoc($res)) {
            $this->output["directory"][] = $row;
        }

        if (!empty($module_uri)) {

            $parts = explode ("/", $module_uri); // ��� �������������
            if(!empty($parts[1]) && !(CF::IsNaturalNumeric($parts[0]) || preg_match('/\d{2}\.\d{2}\.\d{2}/', $parts[0]))) {


				$DB->SetTable($this->db_prefix."specialities", "s");
				$DB->AddField("s.id");
				$DB->AddField("s.code");
				$DB->AddField("s.name");
				$DB->AddField("s.direction");
				$DB->AddField("s.description");
		        $DB->AddField("s.old");
		        $DB->AddField("s.old_id");

		        //$DB->AddField("s.data_training");
				$DB->AddCondFS("s.code", "=", $parts[1]);
		        $code = $parts[1];

				if(isset($parts[2]) && !empty($parts[2]) && CF::IsNaturalNumeric($parts[2])) {
					$DB->AddCondFS("s.id", "=", $parts[2]);
			        $spec_id = $parts[2];
				}
				$res=$DB->Select();
				$this->output["speciality"]=array();
				while ($row=$DB->FetchAssoc($res)) {
					$row["id_edit"] = $row["id"];
				    if($row['old'] || $row['old_id'] == 0)
		            $this->output["speciality"][$row["id"]]=$row;

					$DB->SetTable("nsau_profiles");
					$DB->AddCondFS("spec_id", "=", $row["id"]);
					$pres = $DB->Select();
					while($prow = $DB->FetchAssoc($pres)) {
						$this->output["profiles"][] = $prow;
					}
					/*if($parts[0] == "spo") {
						if($row["data_training"] != null) {
							$this->output["type_klass"] = "����������";
							$this->output["data_training"] = $row["data_training"];
						} else {
							$this->output["type_klass"] = "����������";
							$this->output["data_training"] = "����������";
						}
					} elseif($parts[0] == "mag") {
						$this->output["type_klass"] = "�������";
						$this->output["data_training"] = "5 ���";
					} elseif($parts[0] == "bach") {
						$this->output["type_klass"] = "��������";
						$this->output["data_training"] = "4 ����";
					} elseif($parts[0] == "vpo") {
						$this->output["type_klass"] = "����������";
						$this->output["data_training"] = "5 ���";
					} else {
						$this->output["type_klass"] = "����������";
						$this->output["data_training"] = "����������";
					}*/


					$new_sid = $spec_id;









					$DB->SetTable($this->db_prefix."specialities", "s");
					$DB->AddField("id");
					$DB->AddField("id_faculty");
					$DB->AddField("s.code");
					$DB->AddField("s.name");
					$DB->AddField("s.direction");
					$DB->AddField("s.description");
					$DB->AddCondFS("s.id", "=", $spec_id);


					$res_old = $DB->Select();
					while($row_old = $DB->FetchAssoc($res_old)) {
						$special = $row_old;


						$special["id_edit"] = $spec_id;
					}













          $DB->SetTable($this->db_prefix . "spec_files", "f");
          $DB->AddTable($this->db_prefix . "spec_type", "st");
          $DB->AddCondFS("f.spec_id", "=", $spec_id);
          //$DB->AddCondFS("f.qual_id", "=", $row["qual_id"]);
          $DB->AddCondFF("f.qual_id", "=", "st.qual_id");
          $DB->AddCondFS("st.type", "=", $parts[0]);
          $DB->AddExp("f.*");
          $res_file = $DB->Select();
          while($row_file=$DB->FetchAssoc($res_file)) {
            $this->output["files"][$row_file["file_type"]] = $row_file;
            $special["files"][$row_file["file_type"]] = $row_file;
          }

          if(!count($special["files"])) {
            $DB->SetTable($this->db_prefix . "spec_files", "f");
            $DB->AddTable($this->db_prefix . "spec_type", "st");
            $DB->AddCondFS("f.spec_id", "=", $parts[2]);
            //$DB->AddCondFS("qual_id", "=", $row["qual_id"]);
            $DB->AddCondFF("f.qual_id", "=", "st.qual_id");
            $DB->AddCondFS("st.type", "=", $parts[0]);
            $DB->AddExp("f.*");
            //echo $DB->selectQuery();
            $res_file = $DB->Select();
            while($row_file=$DB->FetchAssoc($res_file)) {
              $this->output["files"][$row_file["file_type"]] = $row_file;
              $special["files"][$row_file["file_type"]] = $row_file;
            }
          }

          $this->output["speciality"][$special["id"]]=$special;
				}
				if (!count($this->output["speciality"])) {
					$Engine->HTTP404();
				};



				$DB->SetTable($this->db_prefix."spec_type","st");
				$DB->AddTable($this->db_prefix."specialities","spec");
				$DB->AddField("st.spec_id");
				$DB->AddField("st.year_open");
				$DB->AddField("st.internal_tuition");
				$DB->AddField("st.correspondence_tuition");
				$DB->AddField("st.type");
				$DB->AddField("st.data_training");
				$DB->AddField("st.qualification");
				$DB->AddField("st.has_vacancies");
        $DB->AddField("st.qual_id");
				$DB->AddField("st.description");
				$DB->AddCondFF("st.code", "=", "spec.code");
				$DB->AddCondFF("st.spec_id", "=", "spec.id");
				$DB->AddCondFS("st.code", "=", $code);
				$DB->AddCondFS("st.type", "=", $parts[0]);
				if(isset($parts[2]) && !empty($parts[2]) && CF::IsNaturalNumeric($parts[2])) {
					$DB->AddCondFS("spec.id", "=", $spec_id);
				}
				//echo $DB->selectQuery();
				$res=$DB->Select();

				$this->output["speciality_edit"]=array();

				while($row = $DB->FetchAssoc($res)) {
					/*if($row["type"] == "secondary") {
						if($row["data_training"] == null) {
							$row["data_training"] = "����������";
						}
					} elseif($row["type"] == "magistracy") {
						$row["data_training"] = "5 ���";
					} elseif($row["type"] == "bachelor") {
						$row["data_training"] = "4 ����";
					} elseif($row["type"] == "higher") {
						$row["data_training"] = "5 ���";
					} else {
						$row["data_training"] = "";
					}*/
          $exam_pos = 1;
					$DB->SetTable($this->db_prefix."exams");
					$res_exam = $DB->Select();
					while($row_exam = $DB->FetchAssoc($res_exam))	{
						$DB->SetTable($this->db_prefix."spec_exams");
						$DB->AddCondFS("spec_id", "=", $row['spec_id']);
						$DB->AddCondFS("exam_id", "=", $row_exam["id"]);
						$DB->AddCondFS("type", "=", $row['type']);
						$res2 = $DB->Select();
						if($row2 = $DB->FetchAssoc($res2)) {
							$row['exams'][($row2["pos"] ? $row2["pos"] : $exam_pos++)] = $row_exam;
						}
					}
          ksort($row['exams']);
          $DB->SetTable($this->db_prefix . "spec_files", "f");
          $DB->AddTable($this->db_prefix . "spec_type", "st");
          $DB->AddCondFS("f.spec_id", "=", $parts[2]);
          //$DB->AddCondFS("f.qual_id", "=", $row["qual_id"]);
          $DB->AddCondFF("f.qual_id", "=", "st.qual_id");
          $DB->AddCondFS("st.type", "=", $row['type']);
          $DB->AddExp("f.*");//echo $DB->SelectQuery();
          $res_file = $DB->Select();
          while($row_file=$DB->FetchAssoc($res_file)) {
            $this->output["files"][$row_file["file_type"]] = $row_file;
            $row["files"][$row_file["file_type"]] = $row_file;
          }

          if(!count($row["files"])) {
            $DB->SetTable($this->db_prefix . "spec_files", "f");
            $DB->AddTable($this->db_prefix . "spec_type", "st");
            $DB->AddCondFS("f.spec_id", "=", $parts[2]);
            //$DB->AddCondFS("f.qual_id", "=", $row["qual_id"]);
            $DB->AddCondFF("f.qual_id", "=", "st.qual_id");
            //$DB->AddCondFS("st.type", "=", $parts[0]);
            $DB->AddCondFS("st.type", "=", $row['type']);
            $DB->AddExp("f.*");
            //echo $DB->selectQuery();
            $res_file = $DB->Select();
            while($row_file=$DB->FetchAssoc($res_file)) {
              $this->output["files"][$row_file["file_type"]] = $row_file;
              $row["files"][$row_file["file_type"]] = $row_file;
            }
          }

				    $this->output["speciality_edit"][$row['spec_id']][] = $row;
				}

				$DB->SetTable($this->db_prefix."school");
		        $DB->AddOrder("s.code");
		        $DB->AddOrder("s.name");
				$res = $DB->Select();
				$this->output["direction"] = array();
				while ($row = $DB->FetchAssoc($res)) {
				    $this->output["direction"][] = $row;
				};
			} elseif(CF::IsNaturalNumeric($parts[0]) || preg_match('/\d{2}\.\d{2}\.\d{2}/', $parts[0])) {

				$DB->SetTable($this->db_prefix."faculties", "f");
				$DB->AddField("f.id");
				$DB->AddField("f.name");
				$res=$DB->Select();
				while ($row=$DB->FetchAssoc($res)) {
					$this->output["faculty"][]=$row;
				}

				$DB->SetTable($this->db_prefix."specialities", "s");
				$DB->AddField("id");
				$DB->AddField("id_faculty");
				$DB->AddField("s.code");
				$DB->AddField("s.name");
				$DB->AddField("s.direction");
				$DB->AddField("s.description");
        $DB->AddFields(array("s.old", "s.old_id"));

				$DB->AddCondFS("s.code", "=", $parts[0]);
				if(isset($parts[1]) && !empty($parts[1]) && CF::IsNaturalNumeric($parts[1])) {
					$DB->AddCondFS("s.id", "=", $parts[1]);
				}
				$res=$DB->Select();
		        $spec_id = $parts[1];
				$this->output["speciality"]=array();
		        $special = null;
				while ($row=$DB->FetchAssoc($res)) {
					//$this->output["speciality"][]=$row;

					$special = $row;
					$special["id_edit"] = $special["id"];
		          $code = $row["code"];

		         
          // if($row["old"] == 0 && $row["old_id"]) {
          	if(trim(strip_tags($row["description"])) != "") $special=$row;
          	else 
          	{
	            $spec_id = $row["id"];
	            $DB->SetTable($this->db_prefix."specialities", "s");
	            $DB->AddField("id");
	            $DB->AddField("id_faculty");
	            $DB->AddField("s.code");
	            $DB->AddField("s.name");
	            $DB->AddField("s.direction");
	            $DB->AddField("s.description");
	            $DB->AddCondFS("s.id", "=", $row["id"]);
	            $res_old = $DB->Select();
	            while($row_old = $DB->FetchAssoc($res_old)) {
	              $code = $row_old["code"];
	              $row_old["code"] = $row["code"];
	              $row_old["name"] = $row["name"];
	              //$this->output["speciality"][]=$row_old;
	              $special = $row_old;
	              $special["id_edit"] = $row["id"];
	            }
          	}
          // }
				}

		$this->output["profiles"]=$this->get_profiles($special['id']);


        if($special) {
          $DB->SetTable($this->db_prefix . "spec_files", "f");
          $DB->AddTable($this->db_prefix . "spec_type", "st");
          $DB->AddCondFS("f.spec_id", "=", $spec_id);
          //$DB->AddCondFS("f.qual_id", "=", $row["qual_id"]);
          $DB->AddCondFF("f.qual_id", "=", "st.qual_id");
          //$DB->AddCondFS("st.type", "=", $parts[0]);
          $DB->AddExp("f.*");
          $res_file = $DB->Select();
          while($row_file=$DB->FetchAssoc($res_file)) {
            $this->output["files"][$row_file["file_type"]] = $row_file;
            $special["files"][$row_file["file_type"]] = $row_file;
          }

          if(!count($special["files"])) {
            $DB->SetTable($this->db_prefix . "spec_files", "f");
            $DB->AddTable($this->db_prefix . "spec_type", "st");
            $DB->AddCondFS("f.spec_id", "=", $parts[1]);
            //$DB->AddCondFS("qual_id", "=", $row["qual_id"]);
            $DB->AddCondFF("f.qual_id", "=", "st.qual_id");
            //$DB->AddCondFS("st.type", "=", $parts[0]);
            $DB->AddExp("f.*");
            //echo $DB->selectQuery();
            $res_file = $DB->Select();
            while($row_file=$DB->FetchAssoc($res_file)) {
              $this->output["files"][$row_file["file_type"]] = $row_file;
              $special["files"][$row_file["file_type"]] = $row_file;
            }
          }
					$special["id_edit"] = $spec_id;
          $this->output["speciality"][] = $special;
        }
				if (!count($this->output["speciality"])) {
					$Engine->HTTP404();
				}

				/*get docs from nsau_docs (NEW DOCS)*/
				$DB->SetTable("nsau_docs");
				$DB->AddCondFS("speciality_id", "=", $spec_id);
				$res=$DB->Select();
				while($row=$DB->FetchAssoc($res)) {
			     
			    switch ($row['form']) 
			    {
			    	case 1:
			    		$row['form'] = "�����";
			    		break;
			     	case 2:
			    		$row['form'] = "�������";
			    		break;
			    	case 3:
			    		$row['form'] = "����-������";
			    		break;   
			    	case 4:
			    		$row['form'] = "�������-����������";
			    		break;  
			    }
			      $this->output["new_docs"][$row["form"]][$row["profile_id"]][$row["doc_type_id"]][$row["year"]][] = $row;

			      //[�������]->[��� ��������� (1,2,3)]->[���]
			    }
    



    $DB->SetTable($this->db_prefix . "spec_files");

    $DB->AddCondFS("spec_id", "=", $spec_id);
    $res = $DB->Select();
    while($row=$DB->FetchAssoc($res)) {
      $this->output["files"][$row["qual_id"]][$row["file_type"]] = $row;
    }

				$DB->SetTable($this->db_prefix."spec_type","st");
				$DB->AddTable($this->db_prefix."specialities","spec");
				$DB->AddField("st.id");
				$DB->AddField("st.spec_id");
				$DB->AddField("st.year_open");
				$DB->AddField("st.internal_tuition");
				$DB->AddField("st.correspondence_tuition");
				$DB->AddField("st.type");
				$DB->AddField("st.data_training");
				$DB->AddField("st.qualification");
				$DB->AddField("st.description");
				$DB->AddField("st.has_vacancies");
				$DB->AddField("st.qual_id");
				$DB->AddCondFF("st.code", "=", "spec.code");
				$DB->AddCondFF("st.spec_id", "=", "spec.id");
				$DB->AddCondFS("st.code", "=", $code/*$parts[0]*/);
				if(isset($parts[1]) && !empty($parts[1]) && CF::IsNaturalNumeric($parts[1])) {
					$DB->AddCondFS("st.spec_id", "=", $spec_id);
				}
				$res=$DB->Select();
				$this->output["speciality_edit"]=array();
				while ($row=$DB->FetchAssoc($res)) {
					/*if($row["type"] == "secondary") {
						if($row["data_training"] == null) {
							$row["data_training"] = "����������";
						}
					} elseif($row["type"] == "magistracy") {
						$row["data_training"] = "5 ���";
					} elseif($row["type"] == "bachelor") {
						$row["data_training"] = "4 ����";
					} elseif($row["type"] == "higher") {
						$row["data_training"] = "5 ���";
					} else {
						$row["data_training"] = "";
					}*/
					$DB->SetTable($this->db_prefix."exams", "ex");
					$DB->AddTable($this->db_prefix."abit_edu_level", "el");
					$DB->AddCondFF("ex.edu_level_id", "=", "el.id");
					$DB->AddField("ex.id");
					$DB->AddField("ex.name");
					$DB->AddField("ex.n_pp");
					$DB->AddField("ex.spec");
					$DB->AddField("ex.min_vyz");
					$DB->AddField("ex.max_vyz");
					$DB->AddField("ex.min_mins");
					$DB->AddField("ex.prikaz");
					$DB->AddField("ex.required");
					$DB->AddField("el.short_name");
					$res_exam = $DB->Select();
					while($row_exam = $DB->FetchAssoc($res_exam))	{
						$DB->SetTable($this->db_prefix."spec_exams");
						$DB->AddCondFS("spec_id", "=", $row['spec_id']);
						$DB->AddCondFS("exam_id", "=", $row_exam["id"]);
						$DB->AddCondFS("type", "=", $row['type']);


						$res2 = $DB->Select();
						if($row2 = $DB->FetchAssoc($res2)) {
							$row['exams'][$row2['pos']] = $row_exam;
						}
					}

          $DB->SetTable($this->db_prefix . "spec_files", "f");
          $DB->AddTable($this->db_prefix . "spec_type", "st");
          $DB->AddCondFS("f.spec_id", "=", $parts[1]);
          //$DB->AddCondFS("f.qual_id", "=", $row["qual_id"]);
          $DB->AddCondFF("f.qual_id", "=", "st.qual_id");
          $DB->AddCondFS("st.type", "=", $row['type']);
          $DB->AddExp("f.*");//echo $DB->SelectQuery();
          $res_file = $DB->Select();
          while($row_file=$DB->FetchAssoc($res_file)) {
            $this->output["files"][$row_file["file_type"]] = $row_file;
            $row["files"][$row_file["file_type"]] = $row_file;
          }



          if(!count($row["files"])) {
            $DB->SetTable($this->db_prefix . "spec_files", "f");
            $DB->AddTable($this->db_prefix . "spec_type", "st");

            $DB->AddCondFS("f.spec_id", "=", $parts[1]);
            //$DB->AddCondFS("f.qual_id", "=", $row["qual_id"]);
            $DB->AddCondFF("f.qual_id", "=", "st.qual_id");
            //$DB->AddCondFS("st.type", "=", $parts[0]);
            $DB->AddCondFS("st.type", "=", $row['type']);
            $DB->AddExp("f.*");

            
            //echo $DB->selectQuery();
            $res_file = $DB->Select();
            while($row_file=$DB->FetchAssoc($res_file)) 
            {
              $this->output["files"][$row_file["file_type"]] = $row_file;
              $row["files"][$row_file["file_type"]] = $row_file;
            }
          }
          			$row['fac']=$this->get_fac_name($row['spec_id']);
					$this->output["speciality_edit"][$row['spec_id']][]=$row;
				}

				$DB->SetTable($this->db_prefix."school");
		        $DB->AddOrder("s.name");
		        $DB->AddOrder("s.code");
				$res = $DB->Select();
				$this->output["direction"] = array();
				while ($row = $DB->FetchAssoc($res)) {
					$this->output["direction"][] = $row;
				}

				$DB->SetTable($this->db_prefix."exams");
				$res = $DB->Select();
				while($row = $DB->FetchAssoc($res))	{
					$DB->SetTable($this->db_prefix."spec_exams");
					$DB->AddCondFS("speciality_code", "=", $code);
					$DB->AddCondFS("exam_id", "=", $row["id"]);
					if(isset($parts[1]) && !empty($parts[1]) && CF::IsNaturalNumeric($parts[1])) {
						$DB->AddCondFS("spec_id", "=", $spec_id);
					}
					$res2 = $DB->Select();
					if ($DB->FetchAssoc($res2))
						$row["need"] = 1;
					else
						$row["need"] = 0;

					//if (!$row["required"])
						$this->output["exams"][] = $row;
				}

	            //additional exams
	            $DB->SetTable($this->db_prefix."spec_exams_additional");
	            $DB->AddCondFS("spec_id", "=", $parts[1]);
	            $DB->AddField("exam_id");
	            $DB->AddField("pos");
	            $res_addit = $DB->Select();
	            while ($row_addit = $DB->FetchAssoc($res_addit)) {
		            foreach($this->output["exams"] as $k=>$exam){
			            if($exam['id'] === $row_addit['exam_id']){
				            $this->output["exams_additonal"][] = $this->output["exams"][$k];
			            }
		            }
	            }

				//price
	            $DB->SetTable($this->db_prefix."spec_price", "sp");
	            $DB->AddTable($this->db_prefix."edu_form", "ef");
	            $DB->AddCondFS("sp.id_spec", "=", $parts[1]);
	            $DB->AddCondFF("sp.id_edu_form", "=", "ef.id");
	            $DB->AddField("sp.id");
	            $DB->AddField("sp.id_spec");
	            $DB->AddField("sp.id_edu_form");
	            $DB->AddField("sp.value");
	            $DB->AddField("ef.name");
	            $DB->AddField("ef.short_name");
	            $res = $DB->SelectArray();
	            $this->output["edu_price"] = $res ? $res : array();

	            //banners
	            $DB->SetTable($this->db_prefix."spec_banner", "sb");
	            $DB->AddCondFS("sb.id_spec", "=", $parts[1]);
	            $DB->AddField("sb.id_spec");
	            $DB->AddField("sb.id_ban");
	            $DB->AddField("sb.value");
	            $res = $DB->SelectArray();
	            $this->output["edu_banner"] = $res ? $res : array();

	            //admission plan
	            //����� �� ��������
	            //$secondary = 32491;//���
	            $secondary_budget = 6021;//��� ������
	            $secondary_comerce = 6121;//��� ���������
	            $bachelor = 24201;//�����������, ������ �����������
	            $magistracy = 24211;//������������
	            $higher =25131;//�����������
	            $graduate =24241;//�����������
	            $tomsk = 33241;//�����������, �����������

	            $code = $this->output['speciality'][0]['code'];

	            $type = ($this->output['speciality_edit'][$parts[1]][0]['fac']['link'] === '/tomsk/')
		            ? 'tomsk'
		            : $this->output['speciality_edit'][$parts[1]][0]['type'];

	            $plan = array();

	            switch ($type){
		            case 'bachelor':
			            $res = $this->getTextItems($bachelor);
			            if($res){
				            $html = iconv('windows-1251','utf-8', $res["text"]);
				            $plan = $this->getPlanBySpeciality($code, $html);
			            }
		            	break;
		            case 'higher':
			            $res = $this->getTextItems($higher);
			            if($res){
				            $html = iconv('windows-1251','utf-8', $res["text"]);
				            $plan = $this->getPlanBySpeciality($code, $html);
			            }
			            break;
		            case 'magistracy':
			            $res = $this->getTextItems($magistracy);
			            if($res){
				            $html = iconv('windows-1251', 'utf-8', $res["text"]);
				            $plan = $this->getPlanBySpeciality($code, $html);
			            }
				        break;
		            case 'graduate':
			            $res = $this->getTextItems($graduate);
			            if($res){
				            $html = iconv('windows-1251', 'utf-8', $res["text"]);
				            $plan = $this->getPlanBySpeciality($code, $html);
			            }
			            break;
		            case 'secondary':
			            $res = $this->getTextItems($secondary_budget);
			            if($res){
				            $html = iconv('windows-1251', 'utf-8', $res["text"]);
				            $budget = $this->getPlanBySpecialitySPOnew($code, $html);
			            }

			            $res = $this->getTextItems($secondary_comerce);
			            if($res){
				            $html = iconv('windows-1251', 'utf-8', $res["text"]);
				            $comerce = $this->getPlanBySpecialitySPOnew($code, $html);
			            }
			            if (!empty($budget))
				            $plan['budget'] = $budget;
			            if (!empty($comerce))
				            $plan['comerce'] = $comerce;
			            break;
			        case 'tomsk':
			            $res = $this->getTextItems($tomsk);
			            if($res){
				            $html = iconv('windows-1251','utf-8', $res["text"]);
				            $plan = $this->getPlanBySpeciality($code, $html);
			            }
			            break;
		            default:
		            	break;
	            }

	            $this->output["edu_plan"] = $plan;
			}
		} else {
			/*����� �������������*/
            if(isset($_GET["name_search"])) {
				$parts = explode(" ", trim($_GET["name_search"]));
				$DB->SetTable($this->db_prefix."specialities", "s");
				foreach($parts as $part) {
					$DB->AddAltFS("s.name", "LIKE", "%".$part."%");
					$DB->AddAltFS("s.code", "LIKE", "%".$part."%");
					$DB->AppendAlts();
				}
				$DB->AddOrder("s.code");
				$res = $DB->Select();
				$this->output["specialities"] = array();
				while($row = $DB->FetchAssoc($res)) {
					$this->output["specialities"][] = $row;
				}
			};
		}

    if((!CF::IsNaturalNumeric($parts[0]) || !preg_match('/\d{2}\.\d{2}\.\d{2}/', $parts[0]) ) /*isset($parts[2]) && !empty($parts[2]) && CF::IsNaturalNumeric($parts[2])*/) {
      //$Engine->AddFootstep($Engine->engine_uri.$parts[0].'/'/*$Engine->module_uri*/, $spec_type_name[$parts[0]], '', false);
    }

		if ($this->output['speciality'][0]['name'] != '') {
			$Engine->AddFootstep($Engine->engine_uri.$Engine->module_uri, $this->output['speciality'][0]['name'], '', false);
			//$Engine->AddFootstep("", $this->output['speciality'][0]['name']);
		}
        $this->output["privileges"]["specialities.handle"] = false;
        $this->output["privileges"]["specialities.handle"] = $Engine->OperationAllowed(5, "specialities.handle", -1, $Auth->usergroup_id);
    }

    function get_fac_name ($id)
    {
    	global $DB;
    	$DB->SetTable("nsau_specialities", "spec");
    	$DB->AddTable("nsau_faculties", "fac");
    	$DB->AddCondFS("spec.id", "=", $id);
    	$DB->AddCondFF("fac.id","=","spec.id_faculty");
    	$DB->AddField("fac.name", "name");
    	$DB->AddField("fac.link", "link");
    	$res = $DB->Select(1);
        $row = $DB->FetchAssoc($res);
        $result = array('name' => $row['name'], 'link' => $row['link']);
		return $result;
    }

    function library_standard($type) {
        global $DB, $Engine, $Auth;
        $spec_type = array(
          '0' => array('51'=>'secondary','68'=>'magistracy','62'=>'bachelor','65'=>'higher'),
          '1' => array('secondary'=>'51','magistracy'=>'68','bachelor'=>'62','higher'=>'65')
        );

        $this->output["type"] = $type;

        $DB->SetTable($this->db_prefix."spec_type","st");
        $DB->AddTable($this->db_prefix."specialities", "s");
        $DB->AddTable($this->db_prefix."school", "sch");
        $DB->AddTable($this->db_prefix."spec_files", "sf");

        $DB->AddCondFS("st.type", "=", $type);
        $DB->AddCondFS("sf.file_type", "=", "����");
        $DB->AddCondFF("s.code","=","st.code");
        $DB->AddCondFF("s.id","=","st.spec_id");
        $DB->AddCondFF("st.qual_id","=","sf.qual_id");
        $DB->AddCondFF("s.id","=","sf.spec_id");
        $DB->AddCondFF("s.direction","=","sch.code");

            $DB->AddField("st.code");
            $DB->AddField("s.id");
            $DB->AddField("s.name");
            $DB->AddField("s.direction");
            $DB->AddField("st.year_open");
            $DB->AddField("st.type");
            $DB->AddField("s.old");
            $DB->AddField("st.qual_id");
			$DB->AddField("st.type");
            $DB->AddField("st.internal_tuition");
            $DB->AddField("st.has_vacancies");
            $DB->AddField("st.correspondence_tuition");
            $DB->AddField("sch.name", "name_dir");
            $DB->AddField("sch.code", "code_dir");
            $DB->AddOrder("s.direction");
            $DB->AddCondFS("st.type", "=", $type);
            $DB->AddCondFF("s.code","=","st.code");
            $DB->AddCondFF("s.id","=","st.spec_id");
            $DB->AddCondFF("s.direction","=","sch.code");
            $DB->AddField("sf.file_id");

            //echo $DB->SelectQuery();
            $res = $DB->Select();

            while ($row = $DB->FetchAssoc($res)) {
                $this->output["specialities"][$row['direction']]['direction'] = $row['name_dir'];
				if(!isset($this->output["specialities"][$row['direction']]['has_vacancies'])) {
					$this->output["specialities"][$row['direction']]['has_vacancies'] = 0;
				}
				if($row['has_vacancies'] == 1) {
					$this->output["specialities"][$row['direction']]['has_vacancies'] = $row['has_vacancies'];
				}

        if($row["old"] == 0) {
          $DB->SetTable($this->db_prefix."qualifications");
          $DB->AddCondFS("id", "=", $row["qual_id"]);
          $res_qual = $DB->Select(1);
          while($row_qual = $DB->FetchAssoc($res_qual)) {
            $row["qualification"] = $row_qual["name"];
          }
        }

                /*if(!isset( $this->output["specialities"][$row['direction']]['spec'][$row['id']] )) */$this->output["specialities"][$row['direction']]['spec'][/*$row['id']*/] = $row;

                /*if($row["old"] == 0) {
          $DB->SetTable($this->db_prefix."qualifications");
          $DB->AddCondFS("id", "=", $row["qual_id"]);
          $res_qual = $DB->Select(1);
          while($row_qual = $DB->FetchAssoc($res_qual)) {
            $this->output["specialities"][$row['direction']]['spec'][$row['id']]["qualification"][] = $row_qual["name"];
          }
        } */
            }


            $this->output["privileges"]["specialities.handle"] = false;
            $this->output["privileges"]["specialities.handle"] = $Engine->OperationAllowed(5, "specialities.handle", -1, $Auth->usergroup_id);
    }

    function specialities($type)
    {
        global $DB, $Engine, $Auth;
        $spec_type = array(
				'0' => array('51'=>'secondary','68'=>'magistracy','62'=>'bachelor','65'=>'higher'),
				'1' => array('secondary'=>'51','magistracy'=>'68','bachelor'=>'62','higher'=>'65')
			);
            $DB->SetTable($this->db_prefix."spec_type","st");
            $DB->AddTable($this->db_prefix."specialities", "s");
            $DB->AddTable($this->db_prefix."school", "sch");
            $DB->AddField("st.code");
            $DB->AddField("s.id");
            $DB->AddField("s.name");
            $DB->AddField("s.direction");
            $DB->AddField("st.year_open");
            $DB->AddField("st.type");
            $DB->AddField("s.old");
            $DB->AddField("st.qual_id");
						$DB->AddField("st.type");
            $DB->AddField("st.internal_tuition");
            $DB->AddField("st.has_vacancies");
            $DB->AddField("st.correspondence_tuition");
            $DB->AddField("st.internal_correspondence_tuition");
            $DB->AddField("st.data_training");
            $DB->AddField("st.data_training_ex");
            $DB->AddField("st.data_training_int_ex");
            $DB->AddField("st.accreditation");
            $DB->AddField("sch.name", "name_dir");
            $DB->AddField("sch.code", "code_dir");
            $DB->AddOrder("s.direction");
            $DB->AddCondFS("st.type", "=", $type);
            $DB->AddCondFF("s.code","=","st.code");
            $DB->AddCondFF("s.id","=","st.spec_id");
            $DB->AddCondFF("s.direction","=","sch.code");
            $DB->AddCondFS("st.has_vacancies","=","1");



            if(isset($_GET["old"])) {
            	$DB->AddCondFS("s.old", "=", (int)$_GET["old"]);
            }

        if(isset($_GET["sort"]) && $_GET["sort"]=="name")
        {
            $DB->AddOrder("s.name");
            $res = $DB->Select();
            $this->output["type"] = $type;
            $this->output["specialities"] = array();
            /*while ($row = $DB->FetchAssoc($res))
            {
                $this->output["specialities"][] = $row;
            }*/
        }
        if(isset($_GET["sort"]) && $_GET["sort"]=="year")
        {
            $DB->AddOrder("st.year_open");
            $res = $DB->Select();
            $this->output["type"] = $type;
            $this->output["specialities"] = array();
           /* while ($row = $DB->FetchAssoc($res))
            {
                $this->output["specialities"][] = $row;
            }*/
        }
        if(isset($_GET["sort"]) && $_GET["sort"]=="code")
        {
            $DB->AddOrder("st.code");
            $res = $DB->Select();
            $this->output["type"] = $type;
            $this->output["specialities"] = array();
            /*while ($row = $DB->FetchAssoc($res))
            {
                $this->output["specialities"][] = $row;
            }*/
        }

        if(isset($_GET["sort"]) && $_GET["sort"]=="correspondence_tuition")
        {
            $DB->AddCondFS("st.correspondence_tuition", "=", 1);
            $DB->AddOrder("st.correspondence_tuition");
            $res = $DB->Select();
            $this->output["type"] = $type;
            $this->output["type_tuition"] = "correspondence_true";
            $this->output["specialities"] = array();
            /*while ($row = $DB->FetchAssoc($res))
            {
                $this->output["specialities"][] = $row;
            }*/
        }
        elseif(!isset($_GET["sort"]) || $_GET["sort"]=="all") {
            $DB->AddOrder("st.code");
			//echo $DB->SelectQuery();
            $res = $DB->Select();
            $this->output["type"] = $type;
            $this->output["specialities"] = array();
            /*while ($row = $DB->FetchAssoc($res))
            {
                $this->output["specialities"][] = $row;
            }*/
        }

            while ($row = $DB->FetchAssoc($res)) {
							$row['accreditation'] = implode(".", array_reverse(explode("-", $row['accreditation'])));
                $this->output["specialities"][$row['direction']]['direction'] = $row['name_dir'];
				if(!isset($this->output["specialities"][$row['direction']]['has_vacancies'])) {
					$this->output["specialities"][$row['direction']]['has_vacancies'] = 0;
				}
				if($row['has_vacancies'] == 1) {
					$this->output["specialities"][$row['direction']]['has_vacancies'] = $row['has_vacancies'];
				}

        if($row["old"] == 0) {
          $DB->SetTable($this->db_prefix."qualifications");
          $DB->AddCondFS("id", "=", $row["qual_id"]);
          $res_qual = $DB->Select(1);
          while($row_qual = $DB->FetchAssoc($res_qual)) {
            $row["qualification"] = $row_qual["name"];
          }
        }

                /*if(!isset( $this->output["specialities"][$row['direction']]['spec'][$row['id']] )) */$this->output["specialities"][$row['direction']]['spec'][/*$row['id']*/] = $row;

                /*if($row["old"] == 0) {
          $DB->SetTable($this->db_prefix."qualifications");
          $DB->AddCondFS("id", "=", $row["qual_id"]);
          $res_qual = $DB->Select(1);
          while($row_qual = $DB->FetchAssoc($res_qual)) {
            $this->output["specialities"][$row['direction']]['spec'][$row['id']]["qualification"][] = $row_qual["name"];
          }
        } */
            }


            $this->output["privileges"]["specialities.handle"] = false;
            $this->output["privileges"]["specialities.handle"] = $Engine->OperationAllowed(5, "specialities.handle", -1, $Auth->usergroup_id);
    }

    function specialities_all($type)
    {
        global $DB, $Engine, $Auth;
        $spec_type = array(
				'0' => array('51'=>'secondary','68'=>'magistracy','62'=>'bachelor','65'=>'higher'),
				'1' => array('secondary'=>'51','magistracy'=>'68','bachelor'=>'62','higher'=>'65')
			);

            $DB->SetTable($this->db_prefix."spec_type","st");
            $DB->AddTable($this->db_prefix."specialities", "s");
            $DB->AddTable($this->db_prefix."school", "sch");
            $DB->AddField("st.code");
            $DB->AddField("s.id");
            $DB->AddField("s.name");
            $DB->AddField("s.direction");
            $DB->AddField("st.year_open");
            $DB->AddField("st.type");
            $DB->AddField("s.old");
            $DB->AddField("s.hide_accredit");
            $DB->AddField("st.qual_id");
						$DB->AddField("st.type");
            $DB->AddField("st.internal_tuition");
            $DB->AddField("st.has_vacancies");
            $DB->AddField("st.correspondence_tuition");
            $DB->AddField("st.internal_correspondence_tuition");
            $DB->AddField("st.data_training");
            $DB->AddField("st.data_training_ex");
            $DB->AddField("st.data_training_int_ex");
            $DB->AddField("st.accreditation");
            $DB->AddField("sch.name", "name_dir");
            $DB->AddField("sch.code", "code_dir");
            $DB->AddOrder("s.direction");
            //$DB->AddCondFS("st.type", "=", $type);
            $DB->AddCondFF("s.code","=","st.code");
            $DB->AddCondFF("s.id","=","st.spec_id");
            $DB->AddCondFF("s.direction","=","sch.code");
            $DB->AddCondFS("st.has_vacancies","=","1");



            if(isset($_GET["old"])) {
            	$DB->AddCondFS("s.old", "=", (int)$_GET["old"]);
            }

        if(isset($_GET["sort"]) && $_GET["sort"]=="name")
        {
            $DB->AddOrder("s.name");
            $res = $DB->Select();
            $this->output["type"] = $type;
            $this->output["specialities"] = array();
            /*while ($row = $DB->FetchAssoc($res))
            {
                $this->output["specialities"][] = $row;
            }*/
        }
        if(isset($_GET["sort"]) && $_GET["sort"]=="year")
        {
            $DB->AddOrder("st.year_open");
            $res = $DB->Select();
            $this->output["type"] = $type;
            $this->output["specialities"] = array();
           /* while ($row = $DB->FetchAssoc($res))
            {
                $this->output["specialities"][] = $row;
            }*/
        }
        if(isset($_GET["sort"]) && $_GET["sort"]=="code")
        {
            $DB->AddOrder("st.code");
            $res = $DB->Select();
            $this->output["type"] = $type;
            $this->output["specialities"] = array();
            /*while ($row = $DB->FetchAssoc($res))
            {
                $this->output["specialities"][] = $row;
            }*/
        }

        if(isset($_GET["sort"]) && $_GET["sort"]=="correspondence_tuition")
        {
            $DB->AddCondFS("st.correspondence_tuition", "=", 1);
            $DB->AddOrder("st.correspondence_tuition");
            $res = $DB->Select();
            $this->output["type"] = $type;
            $this->output["type_tuition"] = "correspondence_true";
            $this->output["specialities"] = array();
            /*while ($row = $DB->FetchAssoc($res))
            {
                $this->output["specialities"][] = $row;
            }*/
        }
        elseif(!isset($_GET["sort"]) || $_GET["sort"]=="all") {
            $DB->AddOrder("st.code");
			//echo $DB->SelectQuery();
            $res = $DB->Select();
            $this->output["type"] = $type;
            $this->output["specialities"] = array();
            /*while ($row = $DB->FetchAssoc($res))
            {
                $this->output["specialities"][] = $row;
            }*/
        }

            while ($row = $DB->FetchAssoc($res)) {
							$row['accreditation'] = implode(".", array_reverse(explode("-", $row['accreditation'])));
                $this->output["specialities"][$row['direction']]['direction'] = $row['name_dir'];
				if(!isset($this->output["specialities"][$row['direction']]['has_vacancies'])) {
					$this->output["specialities"][$row['direction']]['has_vacancies'] = 0;
				}
				if($row['has_vacancies'] == 1) {
					$this->output["specialities"][$row['direction']]['has_vacancies'] = $row['has_vacancies'];
				}

        if($row["old"] == 0) {
          $DB->SetTable($this->db_prefix."qualifications");
          $DB->AddCondFS("id", "=", $row["qual_id"]);
          $res_qual = $DB->Select(1);
          while($row_qual = $DB->FetchAssoc($res_qual)) {
            $row["qualification"] = $row_qual["name"];
          }
        }

                /*if(!isset( $this->output["specialities"][$row['direction']]['spec'][$row['id']] )) */$this->output["specialities"][$row['direction']]['spec'][/*$row['id']*/] = $row;
               // CF::Debug($row);

                /*if($row["old"] == 0) {
          $DB->SetTable($this->db_prefix."qualifications");
          $DB->AddCondFS("id", "=", $row["qual_id"]);
          $res_qual = $DB->Select(1);
          while($row_qual = $DB->FetchAssoc($res_qual)) {
            $this->output["specialities"][$row['direction']]['spec'][$row['id']]["qualification"][] = $row_qual["name"];
          }
        } */
            }


            $this->output["privileges"]["specialities.handle"] = false;
            $this->output["privileges"]["specialities.handle"] = $Engine->OperationAllowed(5, "specialities.handle", -1, $Auth->usergroup_id);
    }

    function faculties()
    {
        global $DB;
        $DB->SetTable($this->db_prefix."faculties");
		$DB->AddCondFN("pid");
		$DB->AddOrder("pos");
        $res = $DB->Select();
        $this->output["faculties"] = array();
        while ($row = $DB->FetchAssoc($res))
        {
            $this->output["faculties"][] = $row;
        }

    }

	function people_info($param) {
        global $DB, $Engine, $Auth, $DB_LOGS;
		$DB->SetTable("nsau_teachers_post");
		$res_post = $DB->Select();
		while($row_post = $DB->FetchAssoc($res_post)) {
			$this->output["teacher"]["post"][$row_post["id"]] = $row_post;
		}


		// $DB_LOGS->SetTable("EFiles");
		// $DB_LOGS->AddCondFS("entry_id", "=", "96021");
		// $DB_LOGS->AddCondFS("action", "=", "download");
		// $res_logs = $DB_LOGS->Select();
		// while($row_logs = $DB_LOGS->FetchAssoc($res_logs)) 
		// {
		// 	$time = explode("-", $row_logs['time']);
		// 	$result[$time[0]."-".$time[1]] = $result[$time[0]."-".$time[1]] ? $result[$time[0]."-".$time[1]]+1 : 1;
			
		// }
		// $this->output["fuck"] = $result;





		$DB->SetTable("nsau_teachers_degree");
		$res_post = $DB->Select();
		while($row_post = $DB->FetchAssoc($res_post)) {
			$this->output["teacher"]["degree"][$row_post["id"]] = $row_post;
		}

		$DB->SetTable("nsau_teachers_rank");
		$res_post = $DB->Select();
		while($row_post = $DB->FetchAssoc($res_post)) {
			$this->output["teacher"]["rank"][$row_post["id"]] = $row_post;
		}

		$DB->SetTable("nsau_buildings");
		$res_post = $DB->Select();
		while($row_post = $DB->FetchAssoc($res_post)) {
			$this->output["buildings"][$row_post["id"]] = $row_post;
		}
        if(!empty($param[1]) && $param[1] == "general_info") {
			$this->output["info_view"] = "general_info";
		}
		$parts = explode ("/", $module_uri);
        $DB->SetTable($this->db_prefix."people", "p");
		$DB->AddTable($this->db_prefix."statuses", "s");
		$DB->AddTable($this->db_prefix."groups", "g");
		$DB->AddField("p.id");
		$DB->AddField("p.last_name");
		$DB->AddField("p.name");
		$DB->AddField("p.patronymic");
		$DB->AddField("p.email");
		$DB->AddField("p.phone");
		$DB->AddField("p.post");
		$DB->AddField("p.exp_full");
		$DB->AddField("p.exp_teach");
		$DB->AddField("p.status_id");
		$DB->AddField("p.location");
		$DB->AddField("p.location_building");
		$DB->AddField("p.id_group");
		$DB->AddField("p.photo");
		$DB->AddField("p.comment");
		$DB->AddField("p.year");
		$DB->AddField("p.prad");
		$DB->AddField("s.name", "status");
		if(CF::IsIntNumeric($parts[0]) && $Auth->user_id == 0) {
			$DB->AddCondFS("p.id", "=", $parts[0]);
		} else {
			$DB->AddCondFS("p.user_id", "=", $Auth->user_id);
		}
		$DB->AddCondFF("p.status_id", "=", "s.id");
		$DB->AddGrouping("p.id");
		$res = $DB->Select(1);

		if($row = $DB->FetchAssoc($res)) {
			if ($row["status"] == "�������") {
				$DB->SetTable($this->db_prefix."groups");
				$DB->AddCondFS("id", "=", $row["id_group"]);
				$sgr = $DB->FetchAssoc($DB->Select(1));

				$DB->SetTable($this->db_prefix."specialities");
				$DB->AddCondFS("code", "=", $sgr["specialities_code"]);
				$sspec = $DB->FetchAssoc($DB->Select(1));

				$DB->SetTable($this->db_prefix."spec_type");
				$DB->AddCondFS("spec_id", "=", $sspec["id"]);
				$sst = $DB->FetchAssoc($DB->Select(1));

				$DB->SetTable($this->db_prefix."qualifications");
				$DB->AddCondFS("id", "=", $sst["qual_id"]);
				$squal = $DB->FetchAssoc($DB->Select(1));

				$DB->SetTable($this->db_prefix."faculties");
				$DB->AddCondFS("id", "=", $sgr["id_faculty"]);
				$sfac = $DB->FetchAssoc($DB->Select(1));


				$row["spec"] = $sspec["name"];
				$row["spec_code"] = $sgr["specialities_code"];
				$row["group"] = $sgr["name"];
				$intgroup = preg_replace("~[^0-9]+~", "", $row["group"]);
				$row["course"] = $intgroup[1];
				$row["qual"] = $squal["name"];
				$row["fac"] = $sfac["name"];
				$row["fac_link"] = (strpos($sfac["link"], "http") !== false) ? $sfac["link"] : "http://nsau.edu.ru/".str_replace("/", "", $sfac["link"]);



				$part = explode("/", $row["fac_link"]);
				$uri_part = (!empty($part[count($part)-4]) ? $part[count($part)-1] : $part[count($part)-2]);



				$DB->SetTable("engine_folders");
				$DB->AddCondFS("uri_part", "=", $uri_part);
				$DB->AddCondFS("pid", "=", 0);
				$folder = $DB->FetchAssoc($DB->Select(1));
				// try {
					$this->recFolderCheck($folder["id"]);

				// } catch (Exception $e) {
					// $spec_uri = $e->getMessage();
				// }
				$row["spec_link"] = $row["fac_link"]."/".$this->ppc_flag;

			}
			if ($row["status"] == "�������������") {
				$DB->SetTable($this->db_prefix."teachers", "t");
				$DB->AddTable($this->db_prefix."departments", "d");
				$DB->AddTable($this->db_prefix."faculties", "f");
				$DB->AddCondFS("t.people_id", "=", $row["id"]);
				$DB->AddCondFF("t.department_id", "=", "d.id");
				$DB->AddCondFF("d.faculty_id", "=", "f.id");
				$DB->AddField("t.post_itemprop", "post");
				$DB->AddField("t.degree", "degree");
				$DB->AddField("t.academ_stat", "academ_stat");
				$DB->AddField("f.name","fname");
				$DB->AddField("f.link","furl");
				$DB->AddField("f.id","fid");
				$DB->AddField("d.name","name");
				$DB->AddField("d.url","url");
				$DB->AddField("d.id","id");
				$res_t = $DB->Select();
				while($row_t = $DB->FetchObject($res_t)) {
					//$row["post"] = $row_t->post;
					$row["post"] = $this->Get_post($row["id"]);
					if(isset($row_t->degree))
						$row["degree"] = explode(";", $row_t->degree);
					else
						$row["degree"] = NULL;
					if(isset($row_t->academ_stat))
						$row["academ_stat"] = explode(";", $row_t->academ_stat);
					else
						$row["academ_stat"] = NULL;
					if(isset($row_t->name)) {
						$this->output["people_fac_dep"][$row_t->fid]["name"] = $row_t->fname;
						$this->output["people_fac_dep"][$row_t->fid]["uri"] = $row_t->furl;
						$this->output["people_fac_dep"][$row_t->fid]["departments"][$row_t->id]["name"] = $row_t->name;
						$this->output["people_fac_dep"][$row_t->fid]["departments"][$row_t->id]["uri"] = $row_t->url;
					}
					$DB->SetTable($this->db_prefix."subjects");
					$DB->AddCondFS("department_id","=", $row_t->id);
					$DB->AddOrder("name");
					$res_subj = $DB->Select();
					while($row_subj = $DB->FetchObject($res_subj)) {
						$this->output["subjects"][] = $row_subj->name;
					}
				}
			}
			$this->output["man"] = $row;
			if(!empty($row["photo"])) {
				$this->output["man"]["original_photo"] = $row['photo']."?".filemtime($this->Imager->files_dir . $row['photo']);
				$photo = explode(".", $row['photo']);
				$photo[sizeof($photo) - 2] .= "_tn";
				$photo = implode(".", $photo);
				$photo .= "?".filemtime($this->Imager->files_dir . $photo);
				$this->output["man"]["photo"] = $photo;
			}
			//$this->files_list($parts[0]);
		} else {
			if($Auth->user_id) {
				$this->output["user_displayed_name"] = $Auth->user_displayed_name;
			}
			//CF::Redirect("/");
		}
				/*if ($Engine->OperationAllowed($this->module_id, "people.handle", -1, $Auth->usergroup_id)
				|| (isset($parts[0]) && intval($parts[0]) && $Engine->OperationAllowed($this->module_id, "people.handle", intval($parts[0]), $Auth->usergroup_id)))
					$this->output["allow_peopledit"] = 1;
				else
					$this->output["allow_peopledit"] = 0;

				$Engine->AddFootstep("/people/".$parts[0]."/", $row["last_name"]." ".$row["name"]." ".$row["patronymic"], '', false);*/
    }

    	function Get_post ($id)
    	{
    		global $DB;
            $DB->SetTable('nsau_teachers_department_posts');
            $DB->AddCondFS('people_id', '=', $id);
            $rnst = $DB->Select();
            while($nst = $DB->FetchAssoc($rnst)) {
                $row[  $this->Get_post_misc($nst['department_id'], 1)   ][] = $this->Get_post_misc($nst['post_id'], 2);
            }
    		return $row;
    	}

 		function Get_post_misc ($id, $option)
 		{
 			global $DB;
 			if ($option == 1) 
 			{
 				$DB->SetTable('nsau_departments');
 			}
 			elseif($option == 2)
 			{
 				$DB->SetTable('nsau_teachers_post');
 			}
 			$DB->AddCondFS('id', '=', $id);	
 			$result = $DB->FetchAssoc($DB->Select(1));
            return $result['name'];
 		}

		function recFolderCheck($id, $parent_uri = null) {
			global $DB;
			if(!$this->ppc_flag) {
				$DB->SetTable("engine_folders");
				$DB->AddCondFS("pid", "=", $id);
				$res = $DB->Select();
				while($row = $DB->FetchAssoc($res)) {
					if(empty($row["id"])) return false;
					$uri = $parent_uri.($parent_uri ? "/" : "").$row["uri_part"];

					if($row["title"] == "����������� ����������") {
						$this->ppc_flag = $uri;
						return $uri;
					}
					else
						$this->recFolderCheck($row["id"], $uri);
				}
			}
		}

		function attachMenu($menu_id) {
			global $Engine;
			include_once INCLUDES . "EMenu" . CLASS_EXT;
			if(!isset($Engine->modules_privileges[3])) {
				$Engine->modules_privileges += $Engine->GetPrivileges(3, NULL, NULL, $Auth->usergroup_id);
			}
			$Module = new EMenu("menu_", $menu_id, 3);
			$Engine->modules_data["people_menu"][] = array(
					"name" => "EMenu",
					"output" => $Module->Output()
			);
		}
    function people($module_uri)
    {
        global $DB, $Engine, $Auth;
				$DB->SetTable("nsau_teachers_post");
				$res_post = $DB->Select();


				while($row_post = $DB->FetchAssoc($res_post)) {
					$this->output["teacher"]["post"][$row_post["id"]] = $row_post;
				}

				$DB->SetTable("nsau_teachers_degree");
				$res_post = $DB->Select();
				while($row_post = $DB->FetchAssoc($res_post)) {
					$this->output["teacher"]["degree"][$row_post["id"]] = $row_post;
				}

				$DB->SetTable("nsau_teachers_rank");
				$res_post = $DB->Select();
				while($row_post = $DB->FetchAssoc($res_post)) {
					$this->output["teacher"]["rank"][$row_post["id"]] = $row_post;
				}

				$DB->SetTable("nsau_buildings");
				$res_post = $DB->Select();
				while($row_post = $DB->FetchAssoc($res_post)) {
					$this->output["buildings"][$row_post["id"]] = $row_post;
				}


        if(!empty($module_uri))
        {


            $parts = explode ("/", $module_uri);

			if ($parts[0] == "edit") {
					$this->output["scripts_mode"] = $this->output["mode"] = "";
				/*if ($Engine->OperationAllowed($this->module_id, "people.handle", -1, $Auth->usergroup_id)
				|| (isset($parts[1]) && intval($parts[1]) && $Engine->OperationAllowed($this->module_id, "people.handle", intval($parts[1]), $Auth->usergroup_id))
				) {
					$this->output["mode"] = "edit_people";
					if($this->global_people($parts[1])) {
						CF::Redirect("/people/".$parts[1]."/");
					}
					//$this->edit_people($this->module_uri);
				}
				else
					CF::Redirect("/people/".$parts[1]."/");*/
			}
			elseif ($parts[0] == "delete") {
					$this->output["scripts_mode"] = $this->output["mode"] = "";
				/*if ($Engine->OperationAllowed($this->module_id, "people.handle", -1, $Auth->usergroup_id)) {
					$this->output["scripts_mode"] = $this->output["mode"] = "delete_people";
					$this->delete_people($this->module_uri);
				}
				else
					CF::Redirect("/people/".$parts[1]."/");*/
			}
			elseif ($parts[0] == "add") {
					$this->output["scripts_mode"] = $this->output["mode"] = "";
				/*if ($Engine->OperationAllowed($this->module_id, "people.add", -1, $Auth->usergroup_id)) {
					$this->output["mode"] = "add_people";
					if($this->global_people()) {
						CF::Redirect("/people/".$parts[1]."/");
					}
					//$this->add_people();
				}
				else
					CF::Redirect("/people/".$parts[1]."/");*/
			}
			else {

				$DB->SetTable("nsau_teachers");
				$DB->AddCondFS("people_id", "=", $parts[0]);
				$teach_row = $DB->FetchAssoc($DB->Select(1));
				$DB->SetTable("nsau_groups_curators");
				$DB->AddCondFS("curator_id", "=", $teach_row["id"]);
				$cur_res = $DB->Select();
				while($cur_row = $DB->FetchAssoc($cur_res)) {
					$DB->SetTable("nsau_groups");
					$DB->AddCondFS("id", "=", $cur_row["group_id"]);
					$gr_row = $DB->FetchAssoc($DB->Select(1));
					$this->output["curator"][] = $gr_row["name"];
				}


				// if (empty($this->output["curator"]))
				// {
				// 	$DB->SetTable("nsau_people");
				// 	$DB->AddCondFS("id", "=", $parts[0]);
				// 	$in_people = $DB->FetchAssoc($DB->Select(1));

				// 	$DB->SetTable("nsau_groups_curators");
				// 	$DB->AddCondFS("curator_id", "=", $in_people["user_id"]);
				// 	$cur_res = $DB->Select();
				// 	while($cur_row = $DB->FetchAssoc($cur_res)) 
				// 	{
				// 		$DB->SetTable("nsau_groups");
				// 		$DB->AddCondFS("id", "=", $cur_row["group_id"]);
				// 		$gr_row = $DB->FetchAssoc($DB->Select(1));
				// 		$this->output["curator_2"][] = $gr_row["name"];
				// 	}
				// }
			$DB->SetTable("nsau_people");
			$DB->AddCondFS("id", "=", $parts[0]);
			$DB->AddField("people_cat");
			$DB->AddField("status_id");
			$stud = $DB->FetchAssoc($DB->Select(1));



	//


				$DB->SetTable($this->db_prefix."people", "p");
				$DB->AddTable($this->db_prefix."statuses", "s");
				$DB->AddField("p.last_name");
				$DB->AddField("p.name");
				$DB->AddField("p.email");
				$DB->AddField("p.phone");
				$DB->AddField("p.location");
				$DB->AddField("p.location_building");
				$DB->AddField("p.id");
				$DB->AddField("p.patronymic");
				$DB->AddField("p.id_group");
				$DB->AddField("p.photo");
				$DB->AddField("p.comment");
				$DB->AddField("p.year");
				$DB->AddField("p.status_id");
				$DB->AddField("p.people_cat");
				$DB->AddField("p.prad");
				$DB->AddField("p.post");
				$DB->AddField("p.exp_full");
				$DB->AddField("p.exp_teach");
				$DB->AddField("s.name", "status");
				$DB->AddCondFS("p.id", "=", $parts[0]);
                $DB->AddCondFS("p.status_id", "<>", '9');
				$DB->AddCondFF("p.status_id", "=", "s.id");
				$res = $DB->Select(1);
				$is_teacher = false;
				if($row = $DB->FetchAssoc($res)) {
					if($row["people_cat"]==1) {
						$DB->SetTable("nsau_groups");
						$DB->AddCondFS("id", "=", $row["id_group"]);
						$gr_row = $DB->FetchAssoc($DB->Select(1));
						$DB->SetTable("nsau_spec_type");
						$DB->AddCondFS("code", "=", $gr_row["specialities_code"]);
						$st_row = $DB->FetchAssoc($DB->Select(1));
						$row["specialities_code"] = $gr_row["specialities_code"];
						$DB->SetTable("nsau_specialities");
						$DB->AddCondFS("id", "=", $st_row["spec_id"]);
						$sp_row = $DB->FetchAssoc($DB->Select(1));
						$row["spec_name"] = $sp_row["name"];
						if(!empty($st_row["qual_id"])) {
							$DB->SetTable("nsau_qualifications");
							$DB->AddCondFS("id", "=", $st_row["qual_id"]);
							$q_row = $DB->FetchAssoc($DB->Select(1));
							$row["qual_name"] = $q_row["name"];
						} else
							$row["qual_name"] = "-";

					}



					 if (($row["status"] == "�������������") || ($row["people_cat"]==2)) {


					 	$DB->SetTable('nsau_departments');
					 	$dres = $DB->Select();
					 	while($drow = $DB->FetchAssoc($dres)) {
					 		$this->output['departments'][$drow['id']] = $drow['name'];
					 	}

					 	unset($row["post"]);
						$DB->SetTable('nsau_teachers_department_posts');
						$DB->AddCondFS('people_id', '=', $row['id']);
						$rnst = $DB->Select();
						while($nst = $DB->FetchAssoc($rnst)) {
							$row["post"][$nst['department_id']][] = $nst['post_id'];
						}

						$is_teacher = true;
						$DB->SetTable($this->db_prefix."teachers");
						$DB->AddCondFS("people_id", "=", $row["id"]);
						$res_t = $DB->Select();
						if($row_t = $DB->FetchAssoc($res_t)) {

							// $row["post"] = $row_t["post_itemprop"];
							if(!empty($row_t["degree"]))
								$row["degree"] = explode(";", $row_t["degree"]);
							if(!empty($row_t["academ_stat"]))
								$row["academ_stat"] = explode(";", $row_t["academ_stat"]);
						}
						$row["status"] = $row["prad"];
						$row["prad"] = "";

					}

					if(!empty($row["email"]))
						$row["email"] = explode(",", $row["email"]);
					else
						$row["email"] = NULL;
					$this->output["man"] = $row;
					//����������� ���� ��� ������ ��������� ����� 1- �������, 2 - ������, 3 - ������
					switch($this->output["man"]["people_cat"]) {
						case 1:
							{
								if ($this->output["man"]["status_id"] == 7) //���� ������� �� ���������� (lol!)
								{

								}
								else
								{
									$this->attachMenu(201);
								}
							}
						break;
						case 2:
							$this->attachMenu(132);
						break;
						case 3:
							$this->attachMenu(133);
						break;
					}

					if(isset($row["photo"]) && !empty($row["photo"])) {
						$this->output["man"]["original_photo"] = $row['photo']."?".filemtime($this->Imager->files_dir . $row['photo']);
						$photo = explode(".", $row['photo']);
						$photo[sizeof($photo) - 2] .= "_tn";
						$photo = implode(".", $photo);
						$photo .= "?".filemtime($this->Imager->files_dir . $photo);
						$this->output["man"]["photo"] = $photo;
					} else {
						$this->output["man"]["photo"] = false;
					}
					// $this->files_list($parts[0]);
				}
				else {
					CF::Redirect("/people/");
				}
				if(!empty($row["id_speciality"])) {
					$DB->SetTable($this->db_prefix."specialities");
					$DB->AddCondFS("code", "=", $row["id_speciality"]);
					if($res = $DB->Select(1))
						$this->output["man"]["speciality"] = $DB->FetchAssoc($res);

				}

				if ($row["status"] == "����������") {
					$DB->SetTable($this->db_prefix."spec_abit");
					$DB->AddCondFS("people_id", "=", $parts[0]);
					$DB->AddCondFS("took", "=", 0);
					$DB->AddCondFS("out", "=", 0);
					$res = $DB->Select();

					while ($row1 = $DB->FetchAssoc($res)) {
						$DB->SetTable($this->db_prefix."specialities", "s");
						$spec_code = explode(".", $row1["speciality_id"]);
						$DB->AddCondFS("s.code", "=", $spec_code[0]);
            $DB->AddTable($this->db_prefix."spec_type", "st");
            $DB->AddCondFF("s.id", "=", "st.spec_id");
            $DB->AddCondFS("st.qual_id", "=", $row1["qualification"]);
						$res_sp = $DB->Select();
						if ($row_sp = $DB->FetchAssoc($res_sp))
							$new_spec = $row_sp;

						else {

							$new_spec["code"] = $row1["speciality_id"];//$spec_code[0];
              $DB->SetTable($this->db_prefix."specialities", "s");
              $spec_code = explode(".", $row1["speciality_id"]);
              $DB->AddCondFS("s.code", "=", $row1["speciality_id"]);
              $DB->AddTable($this->db_prefix."spec_type", "st");
              $DB->AddCondFF("s.id", "=", "st.spec_id");
              $DB->AddCondFS("st.qual_id", "=", $row1["qualification"]);
              $res_sp = $DB->Select();
              if ($row_sp = $DB->FetchAssoc($res_sp))
                $new_spec = $row_sp;
              else {
                $new_spec["code"] = $row1["speciality_id"];//$spec_code[0];
              }
						}
						$new_spec["type_id"] = $row1["type_id"];
						$new_spec["ege"] = $row1["score"];
						$new_spec["qualification"] = $row1["qualification"];
						$new_spec["form"] = $row1["form"];

            $DB->SetTable("engine_nodes", "n");
            $DB->AddTable("engine_modules", "m");
            $DB->AddCondFF("m.id", "=", "n.module_id");
            if(($new_spec['type'] == "higher" || $new_spec['type'] == "bachelor")  && $new_spec['form'] == '�������')
            $DB->AddCondFS("m.name", "=", "EIzop");
            else
            $DB->AddCondFS("m.name", "=", "EAbit");
            $DB->AddExp("n.*");
            //$DB->AddCondFS("module_id", "=", $this->module_id);
            if($new_spec['type'] == "magistracy" && $new_spec['form'] == '�����') $DB->AddCondFS("n.params", "REGEXP", " *list;[0-9];magistracy *$");
            if($new_spec['type'] == "magistracy" && $new_spec['form'] == '�������') $DB->AddCondFS("n.params", "REGEXP", " *list;[0-9];magistracy-ex *$");
            if(($new_spec['type'] == "higher" || $new_spec['type'] == "bachelor")  && $new_spec['form'] == '�����') $DB->AddCondFS("n.params", "REGEXP", " *list;[0-9] *$");
            if(($new_spec['type'] == "higher" || $new_spec['type'] == "bachelor")  && $new_spec['form'] == '��������') $DB->AddCondFS("n.params", "REGEXP", " *list;[0-9];vpo-ev *$");
            if(($new_spec['type'] == "higher" || $new_spec['type'] == "bachelor")  && $new_spec['form'] == '�������') $DB->AddCondFS("n.params", "REGEXP", " *list *$");
            if($new_spec['type'] == "secondary" && $new_spec['form'] == '�����') $DB->AddCondFS("n.params", "REGEXP", " *list;[0-9];spo *$");
            if($new_spec['type'] == "secondary" && $new_spec['form'] == '�������') $DB->AddCondFS("n.params", "REGEXP", " *list;[0-9];spo-ex *$");


            //echo $DB->SelectQuery();
            $res2 = $DB->Select(1);


            while ($row2 = $DB->FetchAssoc($res2)) {
              $new_spec["abit_list_base_href"] = $Engine->FolderURIbyID($row2["folder_id"])."?type=";
            }//print_r($new_spec);
						$this->output["abit_specs"][$new_spec["id"]] = $new_spec;
					}
				}

				if(!empty($row["id_group"])) {
					$DB->SetTable($this->db_prefix."groups");
					$DB->AddField("name");
					$DB->AddCondFS("id", "=", $row["id_group"]);
					if($res = $DB->Select(1))
					{
						$row2 = $DB->FetchAssoc($res);
						$this->output["man"]["group_name"] = $row2["name"];

					}
				}
				else {
					$this->output["man"]["group_name"] = "";

				}
				$DB->AddTable($this->db_prefix."subj_list");//
				$DB->AddField("people_id");//
				$DB->AddCondFS("people_id", "=", $parts[0]);//
				$res = $DB->Select();//
				$subj_count = $DB->FetchAssoc($res);// ��������� ������� ���������

				$DB->SetTable($this->db_prefix."teachers");




				$DB->AddCondFS("people_id", "=", $parts[0]);
				$DB->AddField("department_id");




				$t_res = $DB->Select(1);
				$t_row = $DB->FetchAssoc($t_res);
				if($this->output["man"]["people_cat"]==2) {
					$deps = explode(";", $t_row["department_id"]);
					foreach($deps as $dep_id) {
						$DB->SetTable($this->db_prefix."departments");
						$DB->AddCondFS("id", "=", $dep_id);

						$res_dep = $DB->Select(1);
						$row_dep = $DB->FetchAssoc($res_dep);
						$this->output["displayed_departments"][$row_dep["id"]]["name"] = $row_dep["name"];
						$this->output["displayed_departments"][$row_dep["id"]]["url"] = $row_dep["url"];

						$DB->SetTable($this->db_prefix."subj_list", "sl");
						$DB->AddTable($this->db_prefix."subjects", "s");
						$DB->AddCondFS("sl.people_id", "=", $parts[0]);

						$DB->AddCondFS("sl.department_id", "=", $dep_id);
						$DB->AddCondFS("s.is_hidden", "=", 0);
						$DB->AddCondFF("sl.subject_id", "=", "s.id");
						$DB->AddField("s.name", "subject_name");
						$DB->AddField("s.id", "subject_id");

						$res_subj = $DB->Select();
						while($row_subj = $DB->FetchAssoc($res_subj))
							$this->output["displayed_subjects"][$dep_id][$row_subj["subject_id"]] = $row_subj["subject_name"];
					}
				}










				$allowed = false;
				if($is_teacher) {
					$editor = $this->peopleWork($Auth->people_id);
					$object = $this->peopleWork($parts[0]);
					if($Engine->OperationAllowed($this->module_id, "teachers.faculty.handle", -1, $Auth->usergroup_id)) {
						$allowed = true;
					} else {
						foreach($object["facs"] as $fac_id) {
							if($Engine->OperationAllowed($this->module_id, "teachers.faculty.handle", $fac_id, $Auth->usergroup_id)) {
								$allowed = true;
								break;
							}
						}
						foreach($object["deps"] as $dep_id) {
							if($Engine->OperationAllowed($this->module_id, "teachers.own.handle", -1, $Auth->usergroup_id) && (in_array($dep_id, $editor["deps"]))) {
								$allowed = true;
								break;
							}
						}
						foreach($object["deps"] as $dep_id) {
							if($Engine->OperationAllowed($this->module_id, "teachers.own.handle", $dep_id, $Auth->usergroup_id)) {
								$allowed = true;
								break;
							}
						}
					}
				}
				$tp_id = $Auth->GetUserData(null, null, intval($parts[0]));
				if(!empty($tp_id)) {
					$DB->AddTable('auth_users');
					$DB->AddField('usergroup_id');
					$DB->AddCondFS('id', '=', $tp_id);
					$tp_r = $DB->FetchAssoc($DB->Select(1));
					$target_usergroup_id = $tp_r['usergroup_id'];
					if(!empty($target_usergroup_id))
						if($Engine->OperationAllowed($this->module_id, "people.usergroup.handle", $target_usergroup_id, $Auth->usergroup_id)) {
							$usergroup_allowed = true;
						}					
				}



				if ($Engine->OperationAllowed($this->module_id, "people.handle", intval($parts[0]), $Auth->usergroup_id)
				|| ($allowed && $is_teacher)
				|| ($usergroup_allowed)
				|| ($Auth->people_id == $parts[0]))
					$this->output["allow_peopledit"] = 1;
				else
					$this->output["allow_peopledit"] = 0;

				/*���������� �� �������������� ������-----------------------------------------------------------------------*/
				$DB->SetTable($this->db_prefix."people");
            	$DB->AddCondFS("user_id", "=", $Auth->user_id);
				$in_people = $DB->FetchAssoc($DB->Select(1));
				$DB->SetTable($this->db_prefix."teachers");
            	$DB->AddCondFS("people_id", "=", $in_people['id']);
				$in_teachers = $DB->FetchAssoc($DB->Select(1));

				$in_teachers_array = explode(";", $in_teachers['department_id']);
				$teachers_departments_array = explode(";", $t_row['department_id']);
				$t_alow_restore = false;
				foreach ($in_teachers_array as $value) {
					if(in_array($value, $teachers_departments_array)) {
						$t_alow_restore = true;
					}
				}

	            $this->output["restore_option"]=$Engine->GetModuleOption("people_password_restore");

				if ($Engine->OperationAllowed($this->module_id, "people.restore_password", 1234554321, $Auth->usergroup_id) && $Engine->GetModuleOption("people_password_restore"))
				{
					if ($t_alow_restore || $Auth->usergroup_id == 1) //current user department = vieved user department || admin
					{
						$this->output["allow_people_password_restore"] = 1;
					}
				}
				else
				{
					$this->output["allow_people_password_restore"] = 0;
				}
				/*----------------------------------------------------------------------------------------------------------*/

				$Engine->AddFootstep("/people/".$parts[0]."/", $row["last_name"]." ".$row["name"]." ".$row["patronymic"], '', false);
            }
        }
        else {
            if(isset($_GET["person_name"]) && trim($_GET["person_name"]) != "") {
                $full_request = trim($_GET["person_name"]);
								$full_request = mb_strtolower($full_request);
								$parts = explode(" ", trim($_GET["person_name"]));
                $DB->SetTable($this->db_prefix."people");
                $DB->AddCondFS('status_id', '<>', '9');
                foreach($parts as $part)
                {

                    $DB->AddAltFS("last_name", "LIKE", "%".$part."%");
					$DB->AddAltFS("name", "LIKE", "%".$part."%");
					$DB->AddAltFS("patronymic", "LIKE", "%".$part."%");
					$DB->AppendAlts();
				}

				$DB->AddOrder("last_name");
				$res = $DB->Select();
				$this->output["people"] = array();
				while($row = $DB->FetchAssoc($res))
				{
					if($row["status_id"]==7) {
						$DB->SetTable("nsau_spec_abit");
						$DB->AddCondFS("people_id", "=", $row["id"]);
						$abrow = $DB->FetchAssoc($DB->Select());
						if(empty($abrow))
							continue;
					}
					$full_name = trim(str_replace("  ", " ", $row["last_name"]." ".$row["name"]." ".$row["patronymic"]));
					$full_name = mb_strtolower($full_name);
					//$lev =  levenshtein($full_request, $full_name);
					//$lev += $this->count_matches($full_name, $full_request);
					$row["test"] = $this->count_matches($full_name, $full_request);
					//$row["order"] = $lev+$row["test"];


					$this->output["people"][] = $row;

				}
				function build_sorter($key) {
					return function ($a, $b) use ($key) {
						if ($a[$key] === $b[$key]) return 0;
							return $a[$key] < $b[$key] ? 1 : -1;
					};
				}

				uasort($this->output["people"], build_sorter("test"));



				if (count($this->output["people"]) == 1)
					CF::Redirect("/people/".$this->output["people"][0]["id"]."/");
				elseif (!count($this->output["people"]))
					$this->output["messages"]["bad"][] = 315;
            } else {
				$this->output["messages"]["bad"][] = 315;
			};
			$this->output["allow_addman"] = $Engine->OperationAllowed($this->module_id, "people.add", -1, $Auth->usergroup_id);
		}
    }

	function count_matches($haystack2, $needle2) {
		$needle = explode(" ", $needle2);
		$haystack = explode(" ", $haystack2);
		foreach ($needle as $need) {
			$i = 30;
			$x = 5;
			$x+=substr_count($haystack2, $need);
			foreach ($haystack as $word) {
				//$x+=++$i;
				$i-=10;
				$pos = stripos($word, $need);
				if(($pos !== false) && ($pos==0)) {
					$result+=$x+$i;
				}
			}
			unset($i);
		}
		return $result;
	}


	function edit_people($module_uri) {
		global $DB, $Engine, $Auth;

		$parts = explode ("/", $module_uri);

		$DB->SetTable($this->db_prefix."people");
		$DB->AddCondFS("id", "=", $parts[1]);
		$res = $DB->Select();
		$man = $DB->FetchAssoc($res);
		$this->output["cur_user"] = $man['user_id'];

		$DB->SetTable("nsau_statuses");
		$res = $DB->Select();
		while ($row = $DB->FetchAssoc($res)) {
			$this->output["statuses"][] = $row;
		}

		$DB->SetTable("auth_users");
		$DB->AddOrder("displayed_name");
		$res = $DB->Select();
		while ($row = $DB->FetchAssoc($res)) {
			$this->output["users"][] = $row;
		}

		$DB->SetTable("auth_usergroups");
		$res = $DB->Select();
		while ($row = $DB->FetchAssoc($res))
			$this->output["usergroups"][] = $row;

		if($man["people_cat"] == 1) {
			$DB->SetTable("nsau_groups");
			$res = $DB->Select();
			while ($row = $DB->FetchAssoc($res))
				$this->output["groups"][] = $row;

			$DB->SetTable("nsau_types");
			$res = $DB->Select();
			while ($row = $DB->FetchAssoc($res))
				$this->output["specialities"][] = $row;

			$DB->SetTable("nsau_spec_abit");
			$DB->AddCondFS("people_id", "=", $parts[1]);
			$res = $DB->Select();
			$row = $DB->FetchAssoc($res);
			$this->output["type_id"] = $row["type_id"];
		}
		elseif($man["people_cat"] == 2) {
            if($man) {
				$DB->AddTable($this->db_prefix."teachers", "t");
                $DB->AddTable($this->db_prefix."departments", "d");
                $DB->AddCondFS("t.people_id", "=", $man['id']);
                $DB->AddCondFF("t.department_id", "=", "d.id");
                $DB->AddField("d.name", "department_name");
                $DB->AddField("t.department_id", "department_id");
                $DB->AddField("t.post", "post");
                $DB->AddField("t.post_itemprop", "post_itemprop");
                $DB->AddField("t.degree", "degree");
                $DB->AddField("t.academ_stat", "academ_stat");
                $DB->AddCondFS("d.is_active", "=", 1);
                $res = $DB->Select();
				while($row = $DB->FetchAssoc($res)) {
					$man["department"][$row["department_id"]] = $row["department_name"];
                    $man["post"] = $row["post"];
                    $man["post_itemprop"] = $row["post_itemprop"];
                    $man["degree"] = $row["degree"];
                    $man["academ_stat"] = $row["academ_stat"];
				}
                if($man["user_id"]) {
                    $DB->SetTable("auth_users");
                    $DB->AddCondFS("id", "=", $man["user_id"]);
                    $res2 = $DB->Select();
                    if($row2 = $DB->FetchAssoc($res2)) {
                        $man["username"] = $row2["username"];
                        $man["email"] = $row2["email"];
                        $man["password"] = $row2["password"];
												$man["usergroup"] = $row2["usergroup_id"];
                    }
                }
                $this->output["edit_teacher"][]=$row;
                if($Engine->OperationAllowed($this->module_id, "teachers.own.handle", -1, $Auth->usergroup_id) && !$Engine->OperationAllowed($this->module_id, "teachers.all.handle", -1, $Auth->usergroup_id)) {
					$DB->SetTable($this->db_prefix."people", "p");
                    $DB->AddTable($this->db_prefix."teachers", "t");
                    $DB->AddTable($this->db_prefix."departments", "d");
                    $DB->AddCondFS("p.user_id", "=", $Auth->user_id);
                    $DB->AddCondFF("t.people_id", "=", "p.id");
                    $DB->AddCondFF("t.department_id", "=", "d.id");
                    $DB->AddField("d.name", "department_name");
                    $DB->AddField("t.department_id", "department_id");
                    $DB->AddCondFS("d.is_active", "=", 1);
                    $res = $DB->Select();
					while($row = $DB->FetchAssoc($res)) {
						$this->output["current_department"][$row["department_id"]] = $row["department_name"];
					}
                }
            }
			$DB->SetTable($this->db_prefix."faculties");
			$DB->AddOrder("name");
			$res = $DB->Select();
			while($row = $DB->FetchAssoc($res)) {
				$DB->SetTable($this->db_prefix."departments");
				$DB->AddCondFS("faculty_id", "=", $row["id"]);
        $DB->AddCondFS("is_active", "=", 1);
				$DB->AddOrder("name");
				$res2 =  $DB->Select();
				while($row2 = $DB->FetchAssoc($res2)) {
					if ($Engine->OperationAllowed($this->module_id, "teachers.faculty.handle", $row["id"], $Auth->usergroup_id) || $Engine->OperationAllowed($this->module_id, "teachers.own.handle", $row2["id"], $Auth->usergroup_id)) {
						$this->output["faculties"][$row["id"]]["fac_name"] = $row["name"];
						$this->output["faculties"][$row["id"]]["departments"][] = $row2;
						$this->output["allow_handle"][$row2["id"]] = 1;
					}
				}
			}
		}
		else {}
		$this->output["editperson"] = $man;
		if (!isset($_POST["mode"])) {
			$Engine->AddFootstep("/people/".$parts[1]."/", $man["last_name"]." ".$man["name"]." ".$man["patronymic"]);
		}
		else {
			if($_POST["mode"] == "edit_people") {
				$is_error = false;
				$user_valid = true;
				if (!$_POST["user"]) {
					if(!empty($_POST["username"]) || !empty($_POST["password1"]) || !empty($_POST["password2"]) || !empty($_POST["email"])) {
						if(empty($_POST["username"]) || ((empty($_POST["password1"]) && empty($_POST["password2"]) || $_POST["password1"] != $_POST["password2"]))) {
							$this->output["messages"]["bad"][] = 319;
							$is_error = true;
							$user_valid = false;
						}
					}
					if(empty($_POST["username"]) || empty($_POST["password1"]) || empty($_POST["password2"])) {
						$user_valid = false;
					}
				} elseif ($_POST["user"] == 1) {
					$this->output["display_variant"]["add_item"]["user_select"] = $_POST["user_select"];
					$user_valid = false;
				}
				if($_POST["people_cat"] == 1) {

				} elseif($_POST["people_cat"] == 2) {
					if(isset($_POST["responsible"]))
					$this->output["display_variant"]["add_item"]["usergroup"] = $_POST["usergroup"];
				} else {
					$this->output["display_variant"]["add_item"]["usergroup"] = $_POST["usergroup"];
				}
				$this->output["display_variant"]["add_item"]["last_name"] = $_POST["last_name"];
				$this->output["display_variant"]["add_item"]["first_name"] = $_POST["first_name"];
				$this->output["display_variant"]["add_item"]["patronymic"] = $_POST["patronymic"];
				$this->output["display_variant"]["add_item"]["edit_photo"] = $_FILES["edit_photo"];
				$this->output["display_variant"]["add_item"]["male"] = $_POST["male"];
				$this->output["display_variant"]["add_item"]["status"] = $_POST["status"];
				$this->output["display_variant"]["add_item"]["comment"] = $_POST["comment"];

				$this->output["display_variant"]["add_item"]["prad"] = $_POST["prad"];
				$this->output["display_variant"]["add_item"]["post"] = $_POST["post"];
				$this->output["display_variant"]["add_item"]["degree"] = $_POST["degree"];
				$this->output["display_variant"]["add_item"]["academ_stat"] = $_POST["academ_stat"];
				$this->output["display_variant"]["add_item"]["post_itemprop"] = $_POST["post_itemprop"];
				$this->output["display_variant"]["add_item"]["exp_full"] = $_POST["exp_full"];
				$this->output["display_variant"]["add_item"]["exp_teach"] = $_POST["exp_teach"];
				$this->output["display_variant"]["add_item"]["department"] = $_POST["department"];

				$this->output["display_variant"]["add_item"]["group"] = $_POST["group"];
				$this->output["display_variant"]["add_item"]["subgroup"] = $_POST["subgroup"];
				$this->output["display_variant"]["add_item"]["speciality"] = $_POST["speciality"];
				$this->output["display_variant"]["add_item"]["year"] = $_POST["year"];

				$this->output["display_variant"]["add_item"]["username"] = $_POST["username"];
				$this->output["display_variant"]["add_item"]["email"] = $_POST["email"];


				if((!isset($_POST["last_name"]) || empty($_POST["last_name"])) || (!isset($_POST["first_name"]) || empty($_POST["first_name"]))) {
					$this->output["messages"]["bad"][] = 317;
					$is_error = true;
				}
				if($_POST["people_cat"] == 1) {	//���� �������
					if(!isset($_POST["group"])) {
						$this->output["messages"]["bad"][] = 314;
						$is_error = true;
					}
					if(!isset($_POST["speciality"])) {
						$this->output["messages"]["bad"][] = 323;
						$is_error = true;
					}
				} elseif($_POST["people_cat"] == 2) {
					$isset_dep = false;
					foreach($_POST["department"] as $dep) {
						if($dep) {
							$isset_dep = true;
							continue;
						}
					}
					if(!$isset_dep) {
						$this->output["messages"]["bad"][] = 318;
						$is_error = true;
					}
					if(!isset($_POST["post"])) {
						$this->output["messages"]["bad"][] = 324;
						$is_error = true;
					}
				}

				if(isset($_POST["last_name"]) && !$is_error) {
		            $peopleid = $parts[1];
		            $DB->SetTable($this->db_prefix."people");
					$DB->AddCondFS("id", "=", $peopleid);
					$DB->AddValue("last_name", $_POST["last_name"]);
					$DB->AddValue("name", $_POST["first_name"]);
					$DB->AddValue("patronymic", $_POST["patronymic"]);
                    $DB->AddValue("male", $_POST["male"]);
					$DB->AddValue("comment", $_POST["comment"]);
			        $DB->AddValue("status_id", (int)$_POST["status"]);
					$DB->AddValue("people_cat", $_POST["people_cat"]);

					if($_POST["people_cat"] == 1) {
						$DB->AddValue("id_group", $_POST["group"]);
						if($_POST["subgroup"]) {
							$DB->AddValue("subgroup", $_POST["subgroup"]);
						}
						if(!empty($_POST["st_year"])) {
							$DB->AddValue("year", $_POST["st_year"]);
						}
					} elseif($_POST["people_cat"] == 2) {
						$DB->AddValue("prad", $_POST["prad"]);
						$DB->AddValue("exp_full", $_POST["exp_full"]);
						$DB->AddValue("exp_teach", $_POST["exp_teach"]);
					}
                    $DB->Update();

					$DB->SetTable($this->db_prefix."people");
					$DB->AddCondFS("id", "=", $peopleid);
					$res = $DB->Select();
					if($row = $DB->FetchAssoc($res)) {
						if($user_valid) {
							if($_POST["people_cat"] == 1) {
								if($row["user_id"] == 0) {
									$user_id = $this->insert_user($peopleid, 3);
								} else {
									$user_id = $this->update_user($row, 3);
								}
								if(!($user_id === false))
									$Engine->LogAction($this->module_id, "student", $user_id, "edit");
							} elseif($_POST["people_cat"] == 2) {
								$user_group = 2;
								if(isset($_POST["responsible"])) {
									$user_group = $_POST["usergroup"];
								}
								if($row["user_id"] == 0) {
									$user_id = $this->insert_user($peopleid, $user_group);
								} else {
									$user_id = $this->update_user($row, $user_group);
								}
								if(!($user_id === false))
									$Engine->LogAction($this->module_id, "teacher", $user_id, "edit");
							} else {
								if($row["user_id"] == 0) {
									$user_id = $this->insert_user($peopleid, $_POST["usergroup"]);
								} else {
									$user_id = $this->update_user($row, $_POST["usergroup"]);
								}
								if(!($user_id === false))
									$Engine->LogAction($this->module_id, "user", $user_id, "edit");
							}
                        } elseif(isset($_POST["user_select"])) {
							$DB->SetTable($this->db_prefix."people");
							$DB->AddCondFS("id", "=", $peopleid);
							$DB->AddValue("user_id", $_POST["user_select"]);
							$DB->Update();
						}
						$Engine->AddPrivilege($this->module_id, "people.handle", $peopleid, $Auth->usergroup_id, 1, "add_teachers");
						if($_POST["people_cat"] == 1) {
							$DB->SetTable($this->db_prefix."spec_abit");
							$DB->AddCondFS("people_id", "=", $peopleid);
							$DB->AddValue("type_id", $_POST["speciality"]);
							$DB->Update();
						} elseif($_POST["people_cat"] == 2) {
							if(isset($_POST["department"])) {
								$DB->SetTable($this->db_prefix."teachers");
								$DB->AddCondFS("people_id", "=", $peopleid);
								$DB->Delete();
								foreach($_POST["department"] as $dep) {
									if($dep) {
										$DB->SetTable($this->db_prefix."teachers");
										$DB->AddValue("people_id", $peopleid);
										$DB->AddValue("department_id", $dep);
										$DB->AddValue("post", $_POST['post']);
										// $DB->AddValue("post_itemprop", $_POST['post_itemprop']);
										$DB->Insert();
									}
								}
							}
						}
						/**/
					}
					$this->output["display_variant"] = null;
					CF::Redirect("/people/".$parts[1]."/");
				}
			}
		}
	}

	function ajax_edit_photo() {
		global $DB, $Engine, $Auth;
		if(isset($_REQUEST['data'])) {
			$path_original = explode('?', $_REQUEST['data']['img_uri']);
			$path_original = $path_original[0];
			$photo = explode("/", $path_original);
			$photo = $photo[count($photo)-1];
			$photo_tn = explode(".", $photo);
			$photo_tn[0] = $photo_tn[0]."_tn";
			//$photo_tn[1] = $photo_th[1];
			//$photo_tn = implode(".", $photo_tn);
			$path = "/images/people/".$photo_tn[0].".".$photo_tn[1];
			/*if (file_exists($path))
				unlink ($path);*/
			$result = @getimagesize($this->Imager->files_dir . $photo);
			list($INPUT_WIDTH, $INPUT_HEIGHT, $INPUT_FORMAT) = $result;
			if (!$this->Imager->ResizeImage(
				$this->Imager->files_dir . $photo,
				$this->Imager->files_dir . $photo_tn[0] . "_new." . $photo_tn[1],
				$INPUT_FORMAT,
				$INPUT_WIDTH,
				$INPUT_HEIGHT,
				$_REQUEST['data']['width'] ? $_REQUEST['data']['width'] : $this->Imager->output_files[2]['width'],
				$_REQUEST['data']["height"] ? $_REQUEST['data']["height"] : $this->Imager->output_files[2]['height'],
				$this->Imager->output_image_formats[$INPUT_FORMAT]["use_interlacing"],
				$this->Imager->output_image_formats[$INPUT_FORMAT]["quality"],
				isset($_REQUEST['data']["pos_x"]) ? $_REQUEST['data']["pos_x"] : null,
				isset($_REQUEST['data']["pos_y"]) ? $_REQUEST['data']["pos_y"] : null
			)) {
				unlink ($this->Imager->files_dir . $photo_tn[0] . "_new." . $photo_tn[1]);
			} else {
				unlink ($this->Imager->files_dir . $photo_tn[0] . "." . $photo_tn[1]);
				rename($this->Imager->files_dir . $photo_tn[0] . "_new." . $photo_tn[1], $this->Imager->files_dir . $photo_tn[0] . "." . $photo_tn[1]);
				echo "<img src='".$path."?".filemtime($this->Imager->files_dir . $photo_tn[0] . "." . $photo_tn[1])."' alt='' />";
			}
		}
	}


	function peopleWork($people_id) {
		global $DB, $Auth;

		$DB->SetTable("nsau_teachers");
		$DB->AddCondFS("people_id", "=", $people_id);
		$DB->AddField("department_id");
		$dep_row = $DB->FetchAssoc($DB->Select(1));
		if(!empty($dep_row["department_id"]))
			$result["deps"] = explode(";", $dep_row["department_id"]);
		foreach($result["deps"] as $dep_id) {
			$DB->SetTable("nsau_departments");
			$DB->AddCondFS("id", "=", $dep_id);
			$DB->AddField("faculty_id");
			$fac_row = $DB->FetchAssoc($DB->Select(1));
			$result["facs"][] = $fac_row["faculty_id"];
		}
		return $result;
	}


	function teachers_timetable($module_uri, $submode = null) {
		global $DB, $Engine, $Auth;
		if(!empty($module_uri)) {
      $parts = explode ("/", $module_uri);
			$DB->SetTable("nsau_people");
			$DB->AddCondFS("id", "=", $parts[0]);
			$DB->AddFields(array("name", "last_name", "patronymic"));
			$p = $DB->Select(1);
			$people = $DB->FetchAssoc($p);
			$this->output["tt_people_name"] = $people["last_name"]." ".$people["name"]." ".$people["patronymic"];
			$DB->SetTable("nsau_teachers");
			$DB->AddCondFS("people_id", "=", $parts[0]);
			$res = $DB->Select(1);
			$this->output["tt_people_id"] = $parts[0];
			if($row=$DB->FetchAssoc($res)) {
				$DB->SetTable("nsau_timetable_new");
				$DB->AddCondFS("teacher_id", "LIKE", "%".$row["id"]."%");
				$f = $DB->Select();
				while($line = $DB->FetchAssoc($f)) {
					//auditorium
					$auditorium_id = str_replace(";", "", $line["auditorium_id"]);
					$DB->SetTable("nsau_auditorium", "a");
					$DB->AddCondFS("a.id", "=", $auditorium_id);
					$DB->AddTable("nsau_buildings", "b");
					$DB->AddCondFF("a.building_id", "=", "b.id");
					$DB->AddField("a.name", "auditorium_name");
					$DB->AddField("b.label", "auditorium_label");
					$res = $DB->Select(1);
					$row = $DB->FetchAssoc($res);
					$auditorium_name = $row["auditorium_label"]."-".$row["auditorium_name"];
					//group
					$group_id = $line["group_id"];
					$DB->SetTable("nsau_groups");
					$DB->AddCondFS("id", "=", $group_id);
					$DB->AddField("name");
					$res = $DB->Select(1);
					$row = $DB->FetchAssoc($res);
					$group_name = $row["name"];
					//subject
					$subject_id = $line["subject_id"];
					$DB->SetTable("nsau_subjects");
					$DB->AddCondFS("id", "=", $subject_id);
					$DB->AddField("name");
					$res = $DB->Select(1);
					$row = $DB->FetchAssoc($res);
					$subject_name = $row["name"];

					if($this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["one"]["from"]==$line["comment_from"]
					|| !$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["one"]["groups"]) {
						$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["one"]["groups"] .= (!$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["one"]["groups"]) ? "".$group_name :", ".$group_name;
						$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["one"]["auditorium"] = $auditorium_name;
						$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["one"]["subject"] = $subject_name;
						$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["one"]["type"] = ($line["type"]==0) ? "�" : (($line["type"]==1) ? "��" : "�/�");
						$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["one"]["from"] = $line["comment_from"];
						$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["one"]["to"] = $line["comment_to"];
					}
					elseif($this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["two"]["from"]==$line["comment_from"]
					|| !$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["two"]["groups"]) {
						$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["two"]["groups"] .= (!$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["two"]["groups"]) ? "".$group_name :", ".$group_name;
						$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["two"]["auditorium"] = $auditorium_name;
						$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["two"]["subject"] = $subject_name;
						$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["two"]["type"] = ($line["type"]==0) ? "�" : (($line["type"]==1) ? "��" : "�/�");
						$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["two"]["from"] = $line["comment_from"];
						$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["two"]["to"] = $line["comment_to"];
					}
					else {
						$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["three"]["groups"] .= (!$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["three"]["groups"]) ? "".$group_name :", ".$group_name;
						$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["three"]["auditorium"] = $auditorium_name;
						$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["three"]["subject"] = $subject_name;
						$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["three"]["type"] = ($line["type"]==0) ? "�" : (($line["type"]==1) ? "��" : "�/�");
						$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["three"]["from"] = $line["comment_from"];
						$this->output["tt"][$line["week"]][$line["day"]][$line["pair"]]["three"]["to"] = $line["comment_to"];
					}
				}
				$this->output["pairs"] = $this->isset_pairs($this->output["tt"], "teachers");
			}
		}
	}

	function people_add_edit($module_uri, $submode = null) {
        global $DB, $Engine, $Auth;

	if(!empty($module_uri)) {
            $parts = explode ("/", $module_uri);
			$back_uri = $Engine->engine_uri;
			if(!isset($submode) && $submode != 2) {
				//$back_uri .= $parts[1]."/";
			}
			$allowed = false;
			// $is_teacher = true;

				$editor = $this->peopleWork($Auth->people_id);
				$object = $this->peopleWork($parts[1]);
				if($Engine->OperationAllowed($this->module_id, "teachers.faculty.handle", -1, $Auth->usergroup_id)) {
					$allowed = true;
				} else {
					foreach($object["facs"] as $fac_id) {
						if($Engine->OperationAllowed($this->module_id, "teachers.faculty.handle", $fac_id, $Auth->usergroup_id)) {
							$allowed = true;
						}
					}
					foreach($object["deps"] as $dep_id) {
						if($Engine->OperationAllowed($this->module_id, "teachers.own.handle", -1, $Auth->usergroup_id) && (in_array($dep_id, $editor["deps"]))) {
							$allowed = true;
						}
					}
					foreach($object["deps"] as $dep_id) {

						if($Engine->OperationAllowed($this->module_id, "teachers.own.handle", $dep_id, $Auth->usergroup_id) && (in_array($dep_id, $editor["deps"]))) {
							$allowed = true;

						}
					}
				}



			if ($parts[0] == "edit") {
				$tp_id = $Auth->GetUserData(null, null, intval($parts[1]));
				if(!empty($tp_id)) {
					$DB->AddTable('auth_users');
					$DB->AddField('usergroup_id');
					$DB->AddCondFS('id', '=', $tp_id);
					$tp_r = $DB->FetchAssoc($DB->Select(1));
					$target_usergroup_id = $tp_r['usergroup_id'];
				}
				if(!empty($target_usergroup_id))
					if($Engine->OperationAllowed($this->module_id, "people.usergroup.handle", $target_usergroup_id, $Auth->usergroup_id)) {
						$usergroup_allowed = true;
					}
				if ($Engine->OperationAllowed($this->module_id, "people.handle", intval($parts[1]), $Auth->usergroup_id)
				 || $allowed
				 || $usergroup_allowed
				 || ($Auth->people_id == $parts[1])) {
					$this->output["mode"] = "edit_people";
					if($peopleid = $this->global_people($submode, $parts[1])) {
						if(!isset($submode) && $submode != 2) {
							CF::Redirect($back_uri.$peopleid);
						} else {
							CF::Redirect($back_uri);
						}
					}
					$this->output["allow_peopledit"] = 1;
				}
				else {
					$this->output["allow_peopledit"] = 0;
					CF::Redirect($back_uri);
				}
			}
			elseif ($parts[0] == "delete") {
				if ($Engine->OperationAllowed($this->module_id, "people.handle", -1, $Auth->usergroup_id) || $allowed) {
					$this->output["scripts_mode"] = $this->output["mode"] = "delete_people";
					$this->delete_people($this->module_uri);
					$this->output["allow_peopledit"] = 1;
				} else {
					$this->output["allow_peopledit"] = 0;
					CF::Redirect($back_uri);
					//CF::Redirect("/people/".$parts[1]."/");
				}
			}
			elseif ($parts[0] == "add") {
				if ($Engine->OperationAllowed($this->module_id, "people.add", -1, $Auth->usergroup_id) || $allowed
				|| (isset($parts[1]) && intval($parts[1]) && $Engine->OperationAllowed($this->module_id, "people.add", intval($parts[1]), $Auth->usergroup_id))) {
					/*$this->output["scripts_mode"] = */$this->output["mode"] = "add_people";
					if($peopleid = $this->global_people($submode)) {

						if(!isset($submode) && $submode != 2) {

							CF::Redirect($back_uri.$peopleid);
						} else {

							CF::Redirect($back_uri);
						}
						//CF::Redirect("/people/".$parts[1]."/");
					}
					$this->output["allow_peopledit"] = 1;
					//$this->add_people();
				}
				else {
					$this->output["allow_peopledit"] = 0;
					CF::Redirect($back_uri);
					//CF::Redirect("/people/".$parts[1]."/");
				}
			} else {
				if ($Engine->OperationAllowed($this->module_id, "people.handle", -1, $Auth->usergroup_id)|| $allowed
					|| (isset($parts[0]) && intval($parts[0]) && $Engine->OperationAllowed($this->module_id, "people.handle", intval($parts[0]), $Auth->usergroup_id)))
					$this->output["allow_peopledit"] = 1;
				else
					$this->output["allow_peopledit"] = 0;
				//CF::Redirect($Engine->engine_uri);
			}
        }
    }


		function deleteEducation($people_id) {
			global $DB;
			if(isset($people_id)) {
				$DB->SetTable("nsau_people");
				$DB->AddField("education");
				$DB->AddCondFS("id", "=", $people_id);
				$edu_r = $DB->FetchAssoc($DB->Select(1));
				$edus = explode(";", $edu_r["education"]);
				foreach($edus as $eid) {
					$DB->SetTable("nsau_education");
					$DB->AddCondFS("id", "=", $eid);
					$DB->Delete();
				}
			}
		}

		function deleteAddEducation($people_id) {
			global $DB;
			if(isset($people_id)) {
				$DB->SetTable("nsau_people");
				$DB->AddField("add_education");
				$DB->AddCondFS("id", "=", $people_id);
				$edu_r = $DB->FetchAssoc($DB->Select(1));
				$edus = explode(";", $edu_r["add_education"]);
				foreach($edus as $eid) {
					$DB->SetTable("nsau_add_education");
					$DB->AddCondFS("id", "=", $eid);
					$DB->Delete();
				}
			}
		}

		function checkYear($year) {
			if(($year<=2030) && ($year>=1901))
				return true;
			else
				return false;
		}

		function checkDate($date) {
			$part = explode(".", $date);
			if(($part[2]<1901) || ($part[2]>2030))
				return false;
			if(($part[1]<1) || ($part[1]>12))
				return false;
			if(($part[0]<1) || ($part[0]>31))
				return false;
			return true;
		}

		function addEducation($univ, $edu_level, $year, $spec, $qual) {
			global $DB;
			foreach($univ as $id => $val) {
				if(!empty($val) && !empty($year[$id]) && !empty($spec[$id]) && !empty($qual[$id])) {
					$DB->SetTable("nsau_education");
					$DB->AddValue("university", $val);
					$DB->AddValue("edu_level", $edu_level[$id]);
					$DB->AddValue("year_end", $year[$id]);
					$DB->AddValue("speciality", $spec[$id]);
					$DB->AddValue("qualification", $qual[$id]);
					$DB->Insert();
					$e_ids[] = $DB->LastInsertId();
				}
			}
			return $education = implode(";", $e_ids);
		}

		function addAddEducation($cert, $univ, $ddoc, $drank) {
			global $DB;
			foreach($univ as $id => $val) {
				if(!empty($val) && !empty($cert[$id]) && !empty($ddoc[$id]) && $this->checkDate($ddoc[$id])) {
					$exp = explode(";", $cert[$id]);
					$type = ($exp[1] == "degree") ? "" : "_".$exp[1];
					$DB->SetTable("nsau_add_education");
					$DB->AddValue("university", $val);
					if(!empty($drank[$id]) && $this->checkDate($drank[$id]))
						$DB->AddValue("date_rank", implode("-", array_reverse(explode(".", $drank[$id]))));
					$DB->AddValue("date_doc", implode("-", array_reverse(explode(".", $ddoc[$id]))));
					$DB->AddValue("certificate".$type, $exp[0]);
					$DB->Insert();
					$e_ids[] = $DB->LastInsertId();
				}
			}
			return $add_education = implode(";", $e_ids);
		}

		function checkEducation($univ, $year, $spec, $qual) {
			foreach($univ as $id => $val) {
				if(empty($val) || empty($year[$id]) || empty($spec[$id]) || empty($qual[$id]) || !$this->checkYear($year[$id]))
					continue;
				else return true;
			}
			return false;
		}


		function get_profiles ($id)
		{
			global $DB;
			$DB->SetTable($this->db_prefix."profiles");
			$DB->AddCondFS("spec_id", "=", $id);
			$res = $DB->Select();
			
			while($row = $DB->FetchAssoc($res)) 
			{
				$result[] = $row;
			}
			return $result;
		}




     function global_people($submode = null, $people_id = null) {
        global $DB, $Engine, $Auth;

		//if (!empty($_POST)) {print_r($_POST); exit;}
		$this->output["submode"] = $submode;
		if($_POST["people_cat"] == 2) {
			$_POST["status"] = $_POST["people_cat"];
		} 

		$test  = true;
		if(empty($_POST)) {
			$test = false;
		}
		if(isset($people_id)) {
			$DB->SetTable($this->db_prefix."people");
			$DB->AddCondFS("id", "=", $people_id);
			$res = $DB->Select();
			$man = $DB->FetchAssoc($res);
			$this->output["cur_user"] = $man['user_id'];
			if(isset($man["photo"]) && !empty($man["photo"])) {
				$man["original_photo"] = $man['photo'];
				$photo = explode(".", $man['photo']);
				$photo[sizeof($photo) - 2] .= "_tn";
				$man["photo"] = implode(".", $photo);
				$man["photo"] .= "?".filemtime($this->Imager->files_dir . $man["photo"]);
			} else {
				$man["photo"] = false;
			}

			$man["email_info"] = $man["email"];
			$_POST["people_cat"] = $man["people_cat"];
		}
		// if(($Auth->user_id==31251) && !empty($_POST)) { echo $_POST["people_cat"]; die;}
		//if($Engine->OperationAllowed($this->module_id, "teachers.retired.handle", -1, $Auth->usergroup_id)){

		if(!empty($_POST["retired"]) && $_POST["people_cat"] == 2) {
			$_POST["status"] = 9;
		}
		elseif(!empty($_POST["decret"]) && $_POST["people_cat"] == 2)
		{
			$_POST["status"] = 10;
		}
		elseif($_POST["status"] == 8) {
			$_POST["status"] = 8;
		}


		elseif($test)  {
			$_POST["status"] = $_POST["people_cat"];
				$anal_flag = true;	
		}


		$DB->SetTable($this->db_prefix."groups");
		$res =  $DB->Select();
		while($row = $DB->FetchAssoc($res)) {
			$this->output["all_groups"][] = $row;
		}

		$DB->SetTable($this->db_prefix."statuses");
		$DB->AddOrder("people_cat");
		$res = $DB->Select();
		while ($row = $DB->FetchAssoc($res)) {
			$this->output["statuses"][$row['id']] = $row;
		}
		unset($this->output["statuses"][1]);
		unset($this->output["statuses"][4]);
		unset($this->output["statuses"][7]);

		$DB->SetTable("auth_users");
		$DB->AddOrder("displayed_name");
		$res = $DB->Select();
		while ($row = $DB->FetchAssoc($res))
			$this->output["users"][$row['id']] = $row;

		$DB->SetTable("auth_usergroups");
		$DB->AddOrder("comment");
		$res = $DB->Select();
		while ($row = $DB->FetchAssoc($res))
			$this->output["usergroups"][] = $row;

		if(!isset($submode) || $submode == 1) {
			$DB->SetTable($this->db_prefix."groups");
			$DB->AddCondFS("hidden", "=", 0);
			$DB->AddOrder("name");
			$res = $DB->Select();
			while ($row = $DB->FetchAssoc($res))
				$this->output["groups"][] = $row;

			$DB->SetTable($this->db_prefix."types");
			$DB->AddOrder("type");
			$res = $DB->Select();
			while ($row = $DB->FetchAssoc($res))
				$this->output["specialities"][] = $row;

			if(isset($people_id)) {
				$DB->SetTable("nsau_spec_abit");
				$DB->AddCondFS("people_id", "=", $people_id);
				$res = $DB->Select();
				$row = $DB->FetchAssoc($res);
				$this->output["type_id"] = $row["type_id"];
			}
		}

		$DB->SetTable($this->db_prefix."faculties");
		$DB->AddOrder("name");
		$res = $DB->Select();
		while($row = $DB->FetchAssoc($res)) {
			$DB->SetTable($this->db_prefix."departments");
			$DB->AddCondFS("faculty_id", "=", $row["id"]);
			$DB->AddCondFS("is_active", "=", 1);
			$DB->AddOrder("name");
			$res2 =  $DB->Select();
			while($row2 = $DB->FetchAssoc($res2)) {

				if ($Engine->OperationAllowed($this->module_id, "teachers.faculty.handle", $row["id"], $Auth->usergroup_id) || $Engine->OperationAllowed($this->module_id, "teachers.own.handle", $row2["id"], $Auth->usergroup_id) || ($Auth->people_id==$man["id"])) {
					$this->output["faculties"][$row["id"]]["fac_name"] = $row["name"];
					$this->output["faculties"][$row["id"]]["departments"][] = $row2;
					$this->output["allow_handle"][$row2["id"]] = 1;
				}
			}
		}

		//��� ������������� � �����������
	    $specRepo = new \repository\SpecialityRepository();
		$arrSpec = $specRepo->getNewAssocSpecialities();

		foreach($this->output["faculties"] as $fac_id => $faculty){
			foreach($arrSpec as $spec){
				if($fac_id == $spec['id_faculty']){
					$this->output["faculties"][$fac_id]['specialities'][] = $spec;
				}
			}
		}

		//��������������� ��������� ������� ��������������
		$programList = $DB->SetTable('nsau_teacher_edu_program', 'pr')
			->addCondFs('people_id', '=', (int)$people_id)
			->AddField('pr.id', 'id')
			->AddField('pr.spec_id', 'spec_id')
			->AddOrder('pr.id')
			->SelectArray();

		if($programList){
			foreach($programList as $key => $prog){
				foreach($arrSpec as $spec){
					if($prog['spec_id'] == $spec['id']){
						$programList[$key]['code'] = $spec['code'];
						$programList[$key]['name'] = $spec['name'];
					}
				}
			}
		}

	    $this->output['educational_programs'] = $programList;


		$edu = explode(";", $man["education"]);
		$DB->SetTable("nsau_education");
		foreach($edu as $e_id)
			$DB->AddAltFS("id", "=", $e_id);
		$DB->AppendAlts();
		$er = $DB->Select();
		while($edu_row = $DB->FetchAssoc($er)) {
			$edu_row["year_end"] = implode(".", array_reverse(explode("-", $edu_row["year_end"])));
			$this->output["education"][] = $edu_row;
		}

		$DB->SetTable("nsau_teachers_edu_level");
		$tel = $DB->Select();
		while($tel_row = $DB->FetchAssoc($yel)) 
		{
			$this->output["edu_levels"][$tel_row['id']]= $tel_row['name'];
		}


		$aedu = explode(";", $man["add_education"]);
		$DB->SetTable("nsau_add_education");
		foreach($aedu as $ae_id)
			$DB->AddAltFS("id", "=", $ae_id);
		$DB->AppendAlts();
		$aer = $DB->Select();
		while($aedu_row = $DB->FetchAssoc($aer)) {
			$aedu_row["date_doc"] = implode(".", array_reverse(explode("-", $aedu_row["date_doc"])));
			$aedu_row["date_rank"] = implode(".", array_reverse(explode("-", $aedu_row["date_rank"])));
			$aedu_row["type"] = !empty($aedu_row["certificate"]) ? "degree" : (!empty($aedu_row["certificate_rank"]) ? "rank" : "");
			$this->output["addictional_education"][] = $aedu_row;
		}




		$DB->SetTable("nsau_teachers_post");
		$DB->AddOrder("pos");
		$res_post = $DB->Select();
		while($row_post = $DB->FetchAssoc($res_post)) {
			$this->output["teacher"]["post"][$row_post["id"]] = $row_post;
		}

		$DB->SetTable("nsau_teachers_degree");
		$res_post = $DB->Select();
		while($row_post = $DB->FetchAssoc($res_post)) {
			$row_post["type"] = "degree";
			$this->output["teacher"]["degree"][$row_post["id"]] = $row_post;
		}

		$DB->SetTable("nsau_teachers_rank");
		$res_post = $DB->Select();
		while($row_post = $DB->FetchAssoc($res_post)) {
			$row_post["type"] = "rank";
			$this->output["teacher"]["rank"][$row_post["id"]] = $row_post;
		}
		$this->output["teacher"]["add"]["degree"] = array_merge($this->output["teacher"]["degree"], $this->output["teacher"]["rank"]);
		$DB->SetTable("nsau_buildings");
		$res_post = $DB->Select();
		while($row_post = $DB->FetchAssoc($res_post)) {
			$this->output["buildings"][$row_post["id"]] = $row_post;
		}
     $DB->SetTable('nsau_departments');
     $DB->AddCondFS('is_active', '=', 1);
     $dres = $DB->Select();
     while($drow = $DB->FetchAssoc($dres)) {
     	$this->output['departments'][$drow['id']] = $drow['name'];
     }

		$DB->SetTable("nsau_prof_development");
		$DB->AddCondFS("people_id", "=", $people_id);
		$DB->AddOrder("date_end");
		$DB->AddOrder("date_begin");

		$res = $DB->Select();
		while($row_pd = $DB->FetchAssoc($res)) {
			$row_pd["date_begin"] = implode(".", array_reverse(explode("-", $row_pd["date_begin"])));
			$row_pd["date_end"] = implode(".", array_reverse(explode("-", $row_pd["date_end"])));
			$this->output["prof_development"][$row_pd["id"]] = $row_pd;
		}

		$DB->AddTable($this->db_prefix."subj_list");//
		$DB->AddField("people_id");//
		$DB->AddCondFS("people_id", "=", $man['id']);//
		$res = $DB->Select();//
		$subj_count = $DB->FetchAssoc($res);

		if(isset($man)) {
			$DB->AddTable($this->db_prefix."teachers", "t");
			$DB->AddTable($this->db_prefix."departments", "d");
			$DB->AddCondFS("t.people_id", "=", $man['id']);
			$DB->AddCondFF("t.department_id", "=", "d.id");
			$DB->AddField("d.name", "department_name");
			$DB->AddField("t.department_id", "department_id");
			$DB->AddField("t.id", "teacher_id");
			$DB->AddField("t.post", "post");
			$DB->AddField("t.post_itemprop", "post_itemprop");
			$DB->AddField("t.degree", "degree");
			$DB->AddField("t.academ_stat", "academ_stat");
			$DB->AddField("t.is_curator", "is_curator");
			$DB->AddField("t.curator_text", "curator_text");
			$DB->AddCondFS("d.is_active", "=", 1);
			if ($subj_count != false)
			{
				$DB->AddTable($this->db_prefix."subj_list", "sl");//
				$DB->AddTable($this->db_prefix."subjects", "s");//
				$DB->AddCondFS("sl.people_id", "=", $man['id']);//
				$DB->AddField("s.name", "subject_name");//
				$DB->AddField("s.id", "subject_id");//
				$DB->AddField("s.department_id", "dep_subject_id");//
				$DB->AddCondFF("s.id", "=", "sl.subject_id");//

      		}
			$res = $DB->Select();
			while($row = $DB->FetchAssoc($res)) {
				// $man["department"][$row["department_id"]] = $row["department_name"];
				if ($row["dep_subject_id"]==$row["department_id"])//
				 // $man["subjects"][$row["department_id"]][$row["subject_id"]] = $row["subject_name"];//
                $man["post"] = $row["post"];
                $man["post_itemprop"] = $row["post_itemprop"];
                $man["degree"] = explode(";", $row["degree"]);
                $man["academ_stat"] = explode(";", $row["academ_stat"]);
                $man["is_curator"] = $row["is_curator"];
                $man["curator_text"] = $row["curator_text"];
                $teacher_ids[] = $row["teacher_id"];
			}

				$DB->SetTable($this->db_prefix."teachers");
				$DB->AddCondFS("people_id", "=", $man['id']);
				$DB->AddField("department_id");
				$t_res = $DB->Select(1);
				$t_row = $DB->FetchAssoc($t_res);
				$deps = explode(";", $t_row["department_id"]);

				foreach($deps as $dep_id) {
					$DB->SetTable($this->db_prefix."departments");
					$DB->AddCondFS("id", "=", $dep_id);
					$res_dep = $DB->Select(1);
					$row_dep = $DB->FetchAssoc($res_dep);
					if(!empty($row_dep["name"]))
						$man["department"][$row_dep["id"]] = $row_dep["name"];


					$DB->SetTable($this->db_prefix."subj_list", "sl");
					$DB->AddTable($this->db_prefix."subjects", "s");
					$DB->AddCondFS("sl.people_id", "=", $man['id']);
					$DB->AddCondFS("sl.department_id", "=", $dep_id);
					$DB->AddCondFF("sl.subject_id", "=", "s.id");
					$DB->AddField("s.name", "subject_name");
					$DB->AddField("s.id", "subject_id");

					$res_subj = $DB->Select();
					while($row_subj = $DB->FetchAssoc($res_subj))
						$man["subjects"][$dep_id][$row_subj["subject_id"]] = $row_subj["subject_name"];
				}


				// CF::Debug(array());
				// CF::Debug(array($man));
				// CF::Debug(array($_POST["status"]));

				if($anal_flag || $_POST["status"] == 9) {
					$DB->SetTable("auth_users");
					if($_POST["status"] == 9) {
						$DB->AddValue("is_active", "0");
					} else {
						$DB->AddValue("is_active", "1");
					}
					$DB->AddCondFS("id", "=", $man["user_id"]);
					$DB->Update();
				}



			if ($man["is_curator"]) {

				$DB->AddTable($this->db_prefix."groups_curators", "gc");
				$DB->AddTable($this->db_prefix."groups", "g");
				$DB->AddCondFF("gc.group_id", "=", "g.id");
				foreach($teacher_ids as $teacher_id)
					$DB->AddAltFS("gc.curator_id", "=", $teacher_id);
				$DB->AppendAlts();
				$res =  $DB->Select();
				$curator_groups = array();
				while($row = $DB->FetchAssoc($res)) {
					//if (!in_array($row["group_id"], $curator_groups))
					$curator_ids[] = $row["group_id"];
					$curator_groups[$row["department_id"]][] = array("id"=>$row["group_id"],"name"=>$row["name"]);
				}
				$man["curator_groups"] = $curator_groups;
			}
				$DB->AddTable($this->db_prefix."groups", "g");
				$DB->AddCondFS("g.hidden", "=", 0);
				$DB->AddField('g.id');
				$DB->AddField('g.name');
				$DB->AddOrder('g.name');
				$res = $DB->Select();
				while($row = $DB->FetchAssoc($res)) {
					if (!in_array($row["id"], $curator_ids))
						$groups[] = $row;
				}
				$man["all_groups"] = $groups;

            if($man["user_id"]) {
                $DB->SetTable("auth_users");
                $DB->AddCondFS("id", "=", $man["user_id"]);
                $res2 = $DB->Select();
                if($row2 = $DB->FetchAssoc($res2)) {
                    $man["username"] = $row2["username"];
                    $man["email"] = $row2["email"];
                    $man["password"] = $row2["password"];
										$man["usergroup"] = $row2["usergroup_id"];
                } else {
					$this->output["cur_user"] = $man["user_id"] = 0;
				}
            } else {
							$man["email"] = "";
						}
            $this->output["edit_teacher"][]=$row;

            if($Engine->OperationAllowed($this->module_id, "teachers.own.handle", -1, $Auth->usergroup_id) && !$Engine->OperationAllowed($this->module_id, "teachers.all.handle", -1, $Auth->usergroup_id)) {
				$DB->SetTable($this->db_prefix."people", "p");
                $DB->AddTable($this->db_prefix."teachers", "t");
                $DB->AddTable($this->db_prefix."departments", "d");
                $DB->AddCondFS("p.user_id", "=", $Auth->user_id);
                $DB->AddCondFF("t.people_id", "=", "p.id");
                $DB->AddCondFF("t.department_id", "=", "d.id");
                $DB->AddField("d.name", "department_name");
                $DB->AddField("t.department_id", "department_id");
                $res = $DB->Select();
				while($row = $DB->FetchAssoc($res)) {
					$this->output["current_department"][$row["department_id"]] = $row["department_name"];
				}
            }

// ��� ���� global_people, ������? 



     $DB->SetTable('nsau_teachers_department_posts');
     $DB->AddCondFS('people_id', '=', $man['id']);
     $rnst = $DB->Select();
     while($nst = $DB->FetchAssoc($rnst)) {
     	$man['department_post'][$nst['department_id']][] = $nst['post_id'];
     }




// CF::Debug($man['department_post']);
// $man['department_post'] = 'xxx';

	 $this->output["editperson"] = $man;






			if (!empty($this->output["editperson"]["department"]))
			{
				foreach ($this->output["editperson"]["department"] as $dep_id => $dep_name)
				{
					$DB->SetTable($this->db_prefix."subjects");
					$DB->AddCondFS("is_hidden", "=", 0);
					$DB->AddAltFS("department_id", "=", $dep_id);
					$DB->AppendAlts();
					$DB->AddOrder("name");
					$res =  $DB->Select();
					while($row = $DB->FetchAssoc($res))
					{
						$this->output["departments_subj"][$dep_name][] = $row; //���������� �� ��������� ��������
					}
				}
				$DB->SetTable($this->db_prefix."subj_list");
				$DB->AddAltFS("people_id", "=", $this->output["editperson"]["id"]);
				$DB->AppendAlts();
				$res =  $DB->Select();
				while($row = $DB->FetchAssoc($res))
				{
					$this->output["current_subject"][$row["department_id"]] = $row["subject_id"]; //��������� ���������� �� ��������� ��������
				}
			}
			else
						$this->output["departments_subj"] = false;



        }

		if(($man["status_id"]==8) && ($Auth->usergroup_id!=1) && isset($_POST["male"])) {
			$_POST["last_name"] = $man["last_name"];
			$_POST["first_name"] = $man["name"];
			$_POST["patronymic"] = $man["patronymic"];
			$_POST["group"] = $man["id_group"];
			$_POST["status"] = $man["status_id"];
			$_POST["people_cat"] = 1;
			// CF::Debug($man);
		}
		if (!isset($_POST["last_name"])) {
			//$Engine->AddFootstep("/people/".$parts[1]."/", $man["last_name"]." ".$man["name"]." ".$man["patronymic"]);
			return false;
		}
		else {
			$is_error = false;
			$user_valid = true;
			if (!$_POST["user"]) {
				if(!empty($_POST["username"]) || !empty($_POST["password1"]) || !empty($_POST["password2"]) || !empty($_POST["email"])) {
					if(empty($_POST["username"]) || (((empty($_POST["password1"]) && empty($_POST["password2"]) && $_POST["mode"] != "edit_people") || $_POST["password1"] != $_POST["password2"]))) {
						if($Auth->usergroup_id!=3) {
							$this->output["messages"]["bad"][] = 319;
							$is_error = true;
							$user_valid = false;
						} elseif(!empty($_POST["email"])) {
							if(CF::ValidateEmail($_POST["email"])) {
								$DB->SetTable("auth_users");
								$DB->AddCondFS("email", "=", $_POST["email"]);
								$semail = $DB->FetchAssoc($DB->Select(1));
								if(empty($semail["id"]))
								{
									$DB->SetTable("auth_users");
									$DB->AddCondFS("id", "=", $Auth->user_id);
									$DB->AddValue("email", $_POST["email"]);
									$DB->Update();
								}
								else
								{
									//�������� �� ��, ��� ���� ����������� �������� ������������
									$DB->SetTable("auth_users");
									$DB->AddCondFS("email", "=", $_POST["email"]);
									$res = $DB->Select();
									$row = $DB->FetchAssoc($res);

									if (!($row["id"]==$Auth->user_id))
									{
										$this->output["messages"]["bad"][] = 332;
										$is_error = true;
										$user_valid = false;
									}
								}
							}
							 else {
								$this->output["messages"]["bad"][] = 331;
								$is_error = true;
								$user_valid = false;
							 }
						}
					}
					elseif(!empty($_POST["username"])) {
						$DB->SetTable("auth_users");
						$DB->AddAltFS("username", "=", $_POST["username"]);
						$DB->AddAltFS("email", "=", $_POST["email"]);
						$DB->AppendAlts();
						if($man["user_id"]) {
							$DB->AddCondFS("id", "!=", $man["user_id"]);
						}
						$res2 = $DB->Select();
						if($row2 = $DB->FetchAssoc($res2)) {
							if ($row2["username"] ==  $_POST["username"])
								$this->output["messages"]["bad"][] = 327;
							if ($row2["email"] ==  $_POST["email"])
								$this->output["messages"]["bad"][] = 332;
							$is_error = true;
							$user_valid = false;
						}

					}
				} elseif(empty($_POST["username"]) || empty($_POST["password1"]) || empty($_POST["password2"])) {
					//$this->output["messages"]["bad"][] = 319;
					//$is_error = true;
					$user_valid = false;
				}
				if((($_POST["people_cat"] == 2 && isset($_POST["responsible"])) || $_POST["people_cat"] == 3) && (!isset($_POST["usergroup"]) || $_POST["usergroup"] == 0) && $is_error) {
					$this->output["messages"]["bad"][] = 326;
					$is_error = true;
					$user_valid = false;
				}
				if(((empty($_POST["exp_full"]) && ($_POST["people_cat"] == 2)) && ($Auth->usergroup_id!=1)) && (empty($_POST["retired"])) && (empty($_POST["decret"]) && ($Auth->usergroup_id!=1))) {
						$this->output["messages"]["bad"][] = 339;
						$is_error = true;
						$user_valid = false;
				}
				if(((empty($_POST["new_post_itemprop"]) && empty($_POST["new_department_itemprop"]) && ($_POST["people_cat"] == 2)) && ($Auth->usergroup_id!=1)) && (empty($_POST["retired"])) && (empty($_POST["decret"]) && ($Auth->usergroup_id!=1))) {
						$this->output["messages"]["bad"][] = 324;
						$is_error = true;
						$user_valid = false;
				}
				if(($_POST["people_cat"] == 3)){
					if(!empty($_POST["post_add"]))
						$_POST["post"] = $_POST["post_add"];
				}
				// die($_POST["post"]);
				if((empty($_POST["post"]) && ($_POST["people_cat"] == 3))  && ($Auth->usergroup_id!=1)) {
						$this->output["messages"]["bad"][] = 324;
						$is_error = true;
						$user_valid = false;
				}
			}

			foreach($_POST['new_department_itemprop'] as $i => $d_id) {
				$this->output["display_variant"]["add_item"]['department_post'][$d_id] = $_POST['new_post_itemprop'][$i];
			}
			$this->output["display_variant"]["add_item"]["people_cat"] = $_POST["people_cat"];
			$this->output["display_variant"]["add_item"]["responsible"] = $_POST["responsible"];

			$this->output["display_variant"]["add_item"]["last_name"] = $_POST["last_name"];
			$this->output["display_variant"]["add_item"]["first_name"] = $_POST["first_name"];
			$this->output["display_variant"]["add_item"]["patronymic"] = $_POST["patronymic"];
			$this->output["display_variant"]["add_item"]["edit_photo"] = $_FILES["edit_photo"];
			$this->output["display_variant"]["add_item"]["male"] = $_POST["male"];
			$this->output["display_variant"]["add_item"]["status"] = $_POST["status"];
			$this->output["display_variant"]["add_item"]["comment"] = $_POST["comment"];
			$this->output["display_variant"]["add_item"]["phone"] = $_POST["phone"];
			$this->output["display_variant"]["add_item"]["email_info"] = $_POST["email_info"];
			$this->output["display_variant"]["add_item"]["location"] = $_POST["location"];

			$this->output["display_variant"]["add_item"]["prad"] = $_POST["prad"];
			$this->output["display_variant"]["add_item"]["post"] = $_POST["post"];
			$this->output["display_variant"]["add_item"]["post_itemprop"] = $_POST["post_itemprop"];
			$this->output["display_variant"]["add_item"]["degree"] = $_POST["degree"];
			$this->output["display_variant"]["add_item"]["academ_stat"] = $_POST["academ_stat"];
			$this->output["display_variant"]["add_item"]["location_building"] = $_POST["location_building"];
			$this->output["display_variant"]["add_item"]["exp_full"] = $_POST["exp_full"];
			$this->output["display_variant"]["add_item"]["exp_teach"] = $_POST["exp_teach"];

			foreach($_POST["a_university"] as $id => $val) {
				if(!empty($val) || !empty($_POST["a_certificate"][$id]) || !empty($_POST["a_date_doc"][$id]))
					$display_add_edu[] = array("university" => $val, "date_doc" => $_POST["a_date_doc"][$id], "certificate" => $_POST["a_certificate"][$id], "date_rank" => $_POST["a_date_rank"][$id]);
			}


			foreach($_POST["e_university"] as $id => $val) {
				if(!empty($val) || !empty($_POST["e_year_end"][$id]) || !empty($_POST["e_speciality"][$id]) || !empty($_POST["e_qualification"][$id]))
					$display_edu[] = array("university" => $val, "year_end" => $_POST["e_year_end"][$id], "speciality" => $_POST["e_speciality"][$id], "qualification" => $_POST["e_qualification"][$id]);
			}
			$this->output["display_variant"]["add_item"]["education"] = $display_edu;
			$this->output["display_variant"]["add_item"]["addictional_education"] = $display_add_edu;
			$this->output["display_variant"]["add_item"]["is_curator"] = $_POST["curator_selector"];

			foreach($_POST["curator_groups"] as $dep_id => $dgroups) {
				foreach($dgroups as $gr_id) {
					$DB->SetTable("nsau_groups");
					$DB->AddCondFS("id", "=", $gr_id);
					$gr_row = $DB->FetchAssoc($DB->Select(1));
					$cg[$dep_id][$gr_id]["name"] = $gr_row["name"];
					$cg[$dep_id][$gr_id]["id"] = $gr_row["id"];
				}
			}
			$this->output["display_variant"]["add_item"]["curator_groups"] = $cg;
			if(isset($man)) {
				// $this->output["display_variant"]["add_item"]["department"] = $_POST["department"];

				foreach($_POST["department"] as $dep_id) {
					$DB->SetTable($this->db_prefix."departments");
					$DB->AddCondFS("id", "=", $dep_id);
					$res2 =  $DB->Select();
					if($row2 = $DB->FetchAssoc($res2)) {
						$this->output["display_variant"]["add_item"]["department"][$row2["id"]] = $row2["name"];
						foreach($_POST["subject"][$row2["id"]] as $subj_id) {
							$DB->SetTable($this->db_prefix."subjects");
							$DB->AddCondFS("id", "=", $subj_id);
							$res3 =  $DB->Select();
							$row3 = $DB->FetchAssoc($res3);
							$this->output["display_variant"]["add_item"]["subject"][$row2["id"]][$row3["id"]] = $row3["name"];
						}
					}
				}
			} else {
				foreach($_POST["department"] as $dep_id) {
					$DB->SetTable($this->db_prefix."departments");
					$DB->AddCondFS("id", "=", $dep_id);
					$res2 =  $DB->Select();
					if($row2 = $DB->FetchAssoc($res2)) {
						$this->output["display_variant"]["add_item"]["department"][$row2["id"]] = $row2["name"];
						foreach($_POST["subject"][$row2["id"]] as $subj_id) {
							$DB->SetTable($this->db_prefix."subjects");
							$DB->AddCondFS("id", "=", $subj_id);
							$res3 =  $DB->Select();
							$row3 = $DB->FetchAssoc($res3);
							$this->output["display_variant"]["add_item"]["subject"][$row2["id"]][$row3["id"]] = $row3["name"];
						}
					}
				}
				$this->output["display_variant"]["add_item"]["is_curator"] = $_POST["curator_selector"];

			}

			if(!isset($submode) || $submode == 1) {
				$this->output["display_variant"]["add_item"]["group"] = $_POST["group"];
				$this->output["display_variant"]["add_item"]["subgroup"] = $_POST["subgroup"];
				$this->output["display_variant"]["add_item"]["speciality"] = $_POST["speciality"];
				$this->output["display_variant"]["add_item"]["st_year"] = $_POST["st_year"];
			}

			$this->output["display_variant"]["add_item"]["prof_development"] = $_POST["prof_development"];
			$this->output["display_variant"]["add_item"]["usergroup"] = $_POST["usergroup"];
			$this->output["display_variant"]["add_item"]["user_select"] = $_POST["user_select"];
			$this->output["display_variant"]["add_item"]["username"] = $_POST["username"];
			$this->output["display_variant"]["add_item"]["email"] = $_POST["email"];



			if((!isset($_POST["last_name"]) || empty($_POST["last_name"])) || (!isset($_POST["first_name"]) || empty($_POST["first_name"]))) {
				$this->output["messages"]["bad"][] = 317;
				$is_error = true;
			}
			// if(!isset($_POST["status"]) || $_POST["status"] == 0) {
				// $this->output["messages"]["bad"][] = 325;
				// $is_error = true;
			// }
			if($_POST["people_cat"] == 1) {	//���� �������
				if(!isset($_POST["group"])) {
					$this->output["messages"]["bad"][] = 314;
					$is_error = true;
				}
				/*if(!isset($_POST["speciality"])) {
					$this->output["messages"]["bad"][] = 323;
					$is_error = true;
				}*/
			} elseif($_POST["people_cat"] == 2) {
				$isset_dep = false;
				foreach($_POST["department"] as $dep) {
					if($dep) {
						$isset_dep = true;
						continue;
					}
				}
				if(!$isset_dep && ($Auth->usergroup_id!=1)) {
					$this->output["messages"]["bad"][] = 318;
					$is_error = true;
				}
				if(!isset($_POST["post"])) {
					$this->output["messages"]["bad"][] = 324;
					$is_error = true;
				}
				if(isset($_POST["curator_selector"]) && (!isset($_POST["curator_groups"]) || empty($_POST["curator_groups"])) ) {
					$this->output["messages"]["bad"][] = 335;
					$is_error = true;
				}


				if((!$this->checkEducation($_POST["e_university"], $_POST["e_year_end"], $_POST["e_speciality"], $_POST["e_qualification"]) && ($Auth->usergroup_id!=1)) && (empty($_POST["retired"])) && (empty($_POST["decret"]) && ($Auth->usergroup_id!=1))) {
					$this->output["messages"]["bad"][] = 341;
					$is_error = true;
				}










			}







			if(isset($_POST["last_name"]) && !$is_error) {
                //if(!isset($_POST["check"])) {
                $DB->SetTable($this->db_prefix."people");
                $DB->AddCondFS("last_name", "=", $_POST["last_name"]);
                if(!empty($_POST["first_name"]))
					$DB->AddCondFS("name", "=", $_POST["first_name"]);
                if(!empty($_POST["patronymic"]))
                    $DB->AddCondFS("patronymic", "=", $_POST["patronymic"]);
                $res = $DB->Select();
                if(($row = $DB->FetchAssoc($res)) && !isset($_POST["check"]) && !isset($people_id)) {
					if($_POST["people_cat"] == 2) {
						$DB->SetTable($this->db_prefix."teachers");
						$DB->AddCondFS("people_id", "=", $row["id"]);
						$res1 = $DB->Select();
						while($row1 = $DB->FetchAssoc($res1)) {
							if(!(array_search($row1["department_id"], $_POST["department"]) === false)) {
								$row["dep_identic"] = true;
								$DB->SetTable($this->db_prefix."departments");
								$DB->AddCondFS("id", "=", $row1["department_id"]);
								$res2 = $DB->Select();
								if($row2 = $DB->FetchAssoc($res2)) {
									$row["department"]["old"][$row2["id"]] = $row2["name"];
								}
							}
						}
					}
					$row["last_name"] = $_POST["last_name"];
					$row["name"] = $_POST["first_name"];
					$row["patronymic"] = $_POST["patronymic"];
					$row["male"] = $_POST["male"];

					$this->output["is_exist"]["people"] = $row;
					$this->output["is_exist"]["people_cat"] = $_POST["people_cat"];
					//$this->output["is_exist"]["user_photo"] = $_FILES["user_photo"]["tmp_name"];
					$this->output["is_exist"]["status"] = $_POST["status"];
					$this->output["is_exist"]["comment"] = $_POST["comment"];
					foreach($_POST['new_department_itemprop'] as $i => $d_id) {
						$this->output["is_exist"]['department_post'][$d_id] = $_POST['new_post_itemprop'][$i];
					}
					$this->output["is_exist"]["post"] = $_POST["post"];
					$this->output["is_exist"]["phone"] = $_POST["phone"];
					$this->output["is_exist"]["email_info"] = $_POST["email_info"];
					$this->output["is_exist"]["academ_stat"] = $_POST["academ_stat"];
					$this->output["is_exist"]["degree"] = $_POST["degree"];
					$this->output["is_exist"]["location"] = $_POST["location"];
					$this->output["is_exist"]["location_building"] = $_POST["location_building"];
					$this->output["is_exist"]["post_itemprop"] = $_POST["post_itemprop"];
					$this->output["is_exist"]["prof_development"] = $_POST["prof_development"];
					$this->output["is_exist"]["e_university"] = $_POST["e_university"];
					$this->output["is_exist"]["e_qualification"] = $_POST["e_qualification"];
					$this->output["is_exist"]["e_speciality"] = $_POST["e_speciality"];
					$this->output["is_exist"]["e_year_end"] = $_POST["e_year_end"];
					$this->output["is_exist"]["a_certificate"] = $_POST["a_certificate"];
					$this->output["is_exist"]["a_university"] = $_POST["a_university"];
					$this->output["is_exist"]["a_date_doc"] = $_POST["a_date_doc"];
					$this->output["is_exist"]["a_date_rank"] = $_POST["a_date_rank"];
					if($_POST["people_cat"] == 1) {
						$this->output["is_exist"]["group"] = $_POST["group"];
						$this->output["is_exist"]["subgroup"] = $_POST["subgroup"];
						$this->output["is_exist"]["speciality"] = $_POST["speciality"];
						$this->output["is_exist"]["st_year"] = $_POST["st_year"];
					} elseif($_POST["people_cat"] == 2) {
						//$this->output["is_exist"]["department"] = $_POST["department"];
						$this->output["is_exist"]["exp_full"] = $_POST["exp_full"];
						$this->output["is_exist"]["exp_teach"] = $_POST["exp_teach"];
						$this->output["is_exist"]["prad"] = $_POST["prad"];


					}

					$this->output["is_exist"]["curator_groups"] = $cg;
					$this->output["is_exist"]["username"] = $_POST["username"];
					$this->output["is_exist"]["email"] = $_POST["email"];
					$this->output["is_exist"]["password1"] = $_POST["password1"];
					$this->output["is_exist"]["password2"] = $_POST["password2"];
					$this->output["is_exist"]["user"] = $_POST["user"];

					if($_POST["people_cat"] == 2) {
						if(isset($_POST["responsible"])) {
							$this->output["is_exist"]["usergroup"] = $_POST["usergroup"];
						}
						foreach($_POST["department"] as $dep_id) {
							$this->output["is_exist"]["people"]["department"]["new"][] = $dep_id;
							foreach($_POST["subject"][$dep_id] as $subj_id) {
								$DB->SetTable($this->db_prefix."subjects");
								$DB->AddCondFS("id", "=", $subj_id);
								$res3 =  $DB->Select();
								$row3 = $DB->FetchAssoc($res3);
								$this->output["is_exist"]["subject"][$dep_id][$row3["id"]] = $row3["name"];
							}
						}
					} elseif($_POST["people_cat"] == 3) {
						$this->output["is_exist"]["usergroup"] = $_POST["usergroup"];
					}
					//if ($_POST["user"] == 1) {
						$this->output["is_exist"]["user_select"] = $_POST["user_select"];
					//}
                }
           elseif(!isset($_POST["check"]) || $_POST["check"] == "��") {



				///////////////////�����������//
				////////////////////////////////
				$this->deleteEducation($people_id);
				$education = $this->addEducation($_POST["e_university"], $_POST["e_edu_level"], $_POST["e_year_end"], $_POST["e_speciality"], $_POST["e_qualification"]);
				/////////////////////////////////
				/////////////////////////////////
				///////////////////�������������� �����������//
				////////////////////////////////
				$this->deleteAddEducation($people_id);
				$add_education = $this->addAddEducation($_POST["a_certificate"], $_POST["a_university"], $_POST["a_date_doc"], $_POST["a_date_rank"]);
				/////////////////////////////////
				/////////////////////////////////






          $DB->SetTable($this->db_prefix."people");
					if(isset($people_id)) {
						$DB->AddCondFS("id", "=", $people_id);
					} else {
						$DB->AddValue("user_id", null);

					}

					$DB->AddValue("last_name", $_POST["last_name"]);
					$phone = explode(" ", $_POST["phone"]);
					$DB->AddValue("phone", str_replace("-", "", $phone[2]));
					$DB->AddValue("email", $_POST["email_info"]);
					$DB->AddValue("location", $_POST["location"]);
					$DB->AddValue("location_building", $_POST["location_building"]);
					// die($_POST["post"]);
					$DB->AddValue("post", $_POST["post"]);
					$DB->AddValue("education", $education);
					$DB->AddValue("add_education", $add_education);

					$DB->AddValue("name", $_POST["first_name"]);
					$DB->AddValue("patronymic", $_POST["patronymic"]);
					$DB->AddValue("male", $_POST["male"]);
					// $DB->AddValue("prof_development", $_POST["prof_development"]);
					$DB->AddValue("comment", $_POST["comment"]);
					if(!empty($_POST["status"]))
						$DB->AddValue("status_id", (int)$_POST["status"]);
					if(!empty($_POST["people_cat"]))
						$DB->AddValue("people_cat", (int)$_POST["people_cat"]);


					if($_POST["people_cat"] == 1) {
						$DB->AddValue("id_group", $_POST["group"]);
						// if($_POST["subgroup"]) {
							$DB->AddValue("subgroup", $_POST["subgroup"]);
						// }
						if ($_POST["st_year"]) {
							$DB->AddValue("year", $_POST["st_year"]);
						}
						if ($_POST["library_card"]) {
							$DB->AddValue("library_card", $_POST["library_card"]);
						}
					} elseif($_POST["people_cat"] == 2) {
						$DB->AddValue("prad", $_POST["prad"]);
						$DB->AddValue("exp_full", $_POST["exp_full"]);
						$DB->AddValue("exp_teach", $_POST["exp_teach"]);
					}
					$peopleid = null;
					if(isset($people_id)) {
						if($DB->Update())
							$peopleid = $people_id;
					} else {

						if($DB->Insert())
							$peopleid = $DB->LastInsertID();

					}
					//$res2 = $DB->Exec("SELECT max(LAST_INSERT_ID(id)) FROM nsau_people limit 1");
					$DB->SetTable($this->db_prefix."people");
					$DB->AddCondFS("id", "=", $peopleid);
					$res = $DB->Select();
					if($row = $DB->FetchAssoc($res)) {
						if($user_valid && (!isset($_POST["user_select"]) || $_POST["user_select"] == 0)) {
							if($_POST["people_cat"] == 1) {
								if(is_null($row["user_id"])) {
									$user_id = $this->insert_user($peopleid, 3);
									if(!($user_id === false))
										$Engine->LogAction($this->module_id, "student", $user_id, "add");
								} else {
									$this->update_user($row, 3);
									$Engine->LogAction($this->module_id, "student", $row["user_id"], "edit");
								}
							} elseif($_POST["people_cat"] == 2) {
								$user_group = 2;
								if(isset($_POST["responsible"])) {
									$user_group = $_POST["usergroup"];
								}
								if(is_null($row["user_id"])) {
									$user_id = $this->insert_user($peopleid, $user_group);
									if(!($user_id === false))
										$Engine->LogAction($this->module_id, "teacher", $user_id, "add");
								} else {
									$user_id = $this->update_user($row, $user_group);
									$Engine->LogAction($this->module_id, "teacher", $row["user_id"], "edit");
								}
							} else {

								if(is_null($row["user_id"])) {
									$user_id = $this->insert_user($peopleid, $_POST["usergroup"]);
									if(!($user_id === false))
										$Engine->LogAction($this->module_id, "user", $user_id, "add");
								} else {
									$this->update_user($row, $_POST["usergroup"]);
									$Engine->LogAction($this->module_id, "user", $row["user_id"], "edit");
								}
							}
                        } elseif(!empty($_POST["user_select"])) {
							$DB->SetTable($this->db_prefix."people");
							$DB->AddCondFS("id", "=", $peopleid);
							$DB->AddValue("user_id", $_POST["user_select"]);
							$DB->Update();
							$this->update_user(array("id"=>$peopleid,"user_id"=>$_POST["user_select"]), $_POST["usergroup"]);
						}
						if(isset($people_id)) {
							$Engine->AddPrivilege($this->module_id, "people.handle", $peopleid, $Auth->usergroup_id, 1, "edit_teachers");
						} else {
							$Engine->AddPrivilege($this->module_id, "people.handle", $peopleid, $Auth->usergroup_id, 1, "add_teachers");
						}
						if($_POST["people_cat"] == 1) {
							$spec = $_POST["speciality"];
							/*if(isset($people_id)) {
								$DB->SetTable($this->db_prefix."spec_abit");
								$DB->AddCondFS("people_id", "=", $peopleid);
								$DB->AddValue("type_id", $spec);
								$DB->Update();
							} else {
								$DB->SetTable($this->db_prefix."types");
								$DB->AddCondFS("id", "=", $spec);
								$res = $DB->Select();
								$row = $DB->FetchAssoc($res);

								$spec_parts = explode(".", $row["code"]);
								$speciality_id = $spec_parts[0];
								$qual = "";
								if (isset($spec_parts[1]))
									$qual = $spec_parts[1];

								$DB->SetTable($this->db_prefix."spec_abit");
								$DB->AddValue("speciality_id", $speciality_id);
								$DB->AddValue("type_id", $spec);
								$DB->AddValue("qualification", $qual);
								$DB->AddValue("type", $row["type"]);
								$DB->AddValue("people_id", $peopleid);
								$DB->Insert();
							}*/
						} elseif($_POST["people_cat"] == 2) {
							if(isset($_POST["department"])) {
								if(isset($people_id)) {

									$DB->SetTable($this->db_prefix."teachers");
									$DB->AddCondFS("people_id", "=", $peopleid);
									$rr = $DB->Select(1);
									$rrr = $DB->FetchAssoc($rr);
									$last_id = $rrr["id"];
									// die($last_id);
									// if (isset($_POST["curator_selector"])) {
										$DB->SetTable($this->db_prefix."teachers");
										$DB->AddCondFS("people_id", "=", $peopleid);
										$res = $DB->Select();
										$teacher_old_ids = array();
										while ($row = $DB->FetchAssoc($res))
											$teacher_old_ids[] = $row["id"];
										if (!empty($teacher_old_ids)) {
											$DB->SetTable($this->db_prefix."groups_curators");
											foreach($teacher_old_ids as $teacher_old_id)
												$DB->AddAltFS("curator_id", "=", $teacher_old_id);
											$DB->AppendAlts();
											$DB->Delete();
										}

									// }
									$DB->SetTable($this->db_prefix."teachers");
									$DB->AddCondFS("people_id", "=", $peopleid);
									$DB->Delete();
								}



								//if(isset($_POST["subject"])) {
								$DB->SetTable($this->db_prefix."subj_list");
								$DB->AddCondFS("people_id", "=", $peopleid);
								$DB->Delete();
								//}

								foreach($_POST["subject"] as $dep_id => $subjects) {
									foreach($subjects as $subj_id => $subj)
									{
										$DB->SetTable($this->db_prefix."subj_list");
										$DB->AddValue("people_id", $peopleid);
										$DB->AddValue("department_id", $dep_id);
										$DB->AddValue("subject_id", $subj_id);
										$DB->Insert();
									}
								}


									if(!empty($_POST['new_post_itemprop']) && !empty($_POST['new_department_itemprop'])) {
										$DB->SetTable('nsau_teachers_department_posts');
										$DB->AddCondFS('people_id', '=', $peopleid);
										$DB->Delete();

										foreach($_POST['new_department_itemprop'] as $i => $d_id) {
											$DB->SetTable('nsau_teachers_department_posts');
											$DB->AddValue('people_id', $peopleid);
											$DB->AddValue('department_id', $d_id);
											$DB->AddValue('post_id', $_POST['new_post_itemprop'][$i]);
											$DB->Insert();
										}
									}

									if($_POST["department"]) {
										$DB->SetTable($this->db_prefix."teachers");
										$DB->AddValue("people_id", $peopleid);
										$DB->AddValue("id", $last_id);
										$DB->AddValue("department_id", implode(";", $_POST["department"]));
										$DB->AddValue("post", $_POST['post']);
										$DB->AddValue("post_itemprop", $_POST['post_itemprop']);
										if(!empty($_POST['degree'])) {
											$DB->AddValue("degree", implode(";", $_POST['degree']));
										}
										if(!empty($_POST['academ_stat'])) {
											$DB->AddValue("academ_stat", implode(";", $_POST['academ_stat']));
										}
										if (isset($_POST["curator_selector"])) {
											$DB->AddValue("is_curator", 1);
											$DB->AddValue("curator_text", $_POST["curator_text"]);
										}
										$DB->Insert();




										if (isset($_POST["curator_selector"])) {
											if (isset($_POST["curator_groups"]) && !empty($_POST["curator_groups"]) &&  $teacher_new_id = $DB->LastInsertID()) {
												foreach($_POST["curator_groups"] as $curator_dep_id=>$dep_groups)

														foreach($dep_groups as $group_id) {
															$DB->SetTable($this->db_prefix."groups_curators");
															$DB->AddValue("curator_id", $teacher_new_id);
															$DB->AddValue("department_id", $curator_dep_id);
															$DB->AddValue("group_id", $group_id);
															$DB->Insert();

														}
											}
										}


									}

								// }
							}
						}
						if($_POST["people_cat"] == 2 && isset($_POST["email"]) && !empty($_POST["email"]) && !isset($people_id)) {
							$deport_mass = null;
							if(isset($_POST["department"])) {
								foreach($_POST["department"] as $dep) {
									$DB->SetTable($this->db_prefix."departments");
									$DB->AddCondFS("id", "=", $dep);
									$res1 = $DB->Select();
									if($row1 = $DB->FetchAssoc($res1)) {
										if(!is_null($deport_mass)) {
											$deport_mass .= ", ".$row1["name"];
										} else {
											$deport_mass = $row1["name"];
										}
									}
								}
							}
							if(is_null($deport_mass)) {
								$deport_mass = "�� �������";
							}

							$mail_message_patterns = array(
								'headers' => array(
										'%site_short_name%' => "����<admin@".$_SERVER['HTTP_HOST'].">",
										'%server_name%' => $_SERVER['HTTP_HOST'],
										'%email%' => $_POST["email"],
										'%subject%' => SITE_SHORT_NAME
									),
								'subject' => array(
										'%site_short_name%' => SITE_SHORT_NAME
									),
								'content' => array(
										'%last_name%' => $_POST["last_name"],
										'%first_name%' => $_POST["first_name"],
										'%patronymic%' => $_POST["patronymic"],
										'%server_name%' => $_SERVER['HTTP_HOST'],
										'%department%' => $deport_mass,
										'%login%' => $_POST["username"],
										'%password%' => $_POST["password1"]
									),
								'message' => array(
										'%site_short_name%' => SITE_SHORT_NAME,
										'%time%' => date('Y-m-d H:i:s'),
										'%content%' => &$this->settings['content']['mess']
									)
							);

							$this->settings['content']['mess'] = str_replace( array_keys($mail_message_patterns['content']), $mail_message_patterns['content'], $this->settings['content']['mess']);

							foreach ($this->settings['notify_settings'] as $ind=>$setting) {
								$this->settings['notify_settings'][$ind] = str_replace( array_keys($mail_message_patterns[$ind]), $mail_message_patterns[$ind], $setting);
							}

							mail($_POST["email"], $this->settings['notify_settings']['subject'], $this->settings['notify_settings']['message'], $this->settings['notify_settings']['headers']);

						}
						/**/
					}
					if (($_FILES["user_photo"]["tmp_name"] && $FILE_DATA = $_FILES["user_photo"]) || (isset($_POST["delete_photo"]) && $_POST["delete_photo"])) {
						$photo_name = '';
						if(isset($man) && $man['photo']) {
							$path = $_SERVER["DOCUMENT_ROOT"]."/images/peoples/".$man["original_photo"];
							if (file_exists($path)) {
								unlink ($path);
							}
							$photo = explode(".", $man["original_photo"]);
							$photo_tn[0] = $photo[0]."_tn";
							$photo_tn[1] = $photo[1];
							$photo_tn = implode(".", $photo_tn);
							$path = $_SERVER["DOCUMENT_ROOT"]."/images/peoples/".$photo_tn;
							if (file_exists($path)) {
								unlink ($path);
							}
						}

						if($_FILES["user_photo"]["tmp_name"] && $FILE_DATA = $_FILES["user_photo"]) {
							if (is_array($result = $this->Imager->GrabUploadedFile($FILE_DATA))) {
								$this->status = false;
								$this->output["messages"] = array_merge($this->output["messages"], $result);
							}
							if (is_array($result = $this->Imager->CreateOutputFiles($peopleid))) {
								$this->status = false;
								$this->output["messages"] = array_merge($this->output["messages"], $result);
							}
							$photo_name = $this->Imager->base_filename.".".$this->Imager->ext;

						}
						$DB->SetTable($this->db_prefix."people");
						$DB->AddCondFS("id", "=", $peopleid);
						$DB->AddValue("photo", $photo_name);
						$DB->Update();

					}
					$this->output["display_variant"] = null;
					return $peopleid;
					//CF::Redirect("/people/".$peopleid."/");
				} else {
					return true;
				}
			} else {
				return false;
			}
		}
	}

	function edit_man_photo($photo_name, $man_id)
	{
		global $DB, $Auth;


		if (isset($_POST["delete_photo"]) && $_POST["delete_photo"] && $photo_name) {
					$DB_ref->AddValue("photo", "");
					$path = $_SERVER["DOCUMENT_ROOT"]."/images/peoples/".$photo_name;
					if (file_exists($path))
						unlink ($path);

					$photo = explode(".", $photo_name);
					$photo_tn[0] = $photo[0]."_tn";
					$photo_tn[1] = $photo[1];
					$photo_tn = implode(".", $photo_tn);
					$path = $_SERVER["DOCUMENT_ROOT"]."/images/peoples/".$photo_tn;
					if (file_exists($path))
						unlink ($path);
				} elseif ($_FILES["edit_photo"]["tmp_name"] && $FILE_DATA = $_FILES["edit_photo"]) {

					$path = $_SERVER["DOCUMENT_ROOT"]."/images/peoples/".$photo_name;
					if (file_exists($path))
						unlink ($path);

					$photo = explode(".", $photo_name);
					$photo_tn[0] = $photo[0]."_tn";
					$photo_tn[1] = $photo[1];

					$photo_tn = implode(".", $photo_tn);
					$path = $_SERVER["DOCUMENT_ROOT"]."/images/peoples/".$photo_tn;
					if (file_exists($path))
						unlink ($path);

					if (is_array($result = $this->Imager->GrabUploadedFile($FILE_DATA))) {
						$this->status = false;
						$this->output["messages"] = array_merge($this->output["messages"], $result);
					}

					if (is_array($result = $this->Imager->CreateOutputFiles($man_id))) {
						$this->status = false;
						$this->output["messages"] = array_merge($this->output["messages"], $result);
					}
					$DB_ref->AddValue("photo", $this->Imager->base_filename.".".$this->Imager->ext);
				}
	}

	function add_people()
	{
        global $DB, $Engine, $Auth;

		$DB->SetTable("nsau_statuses");
		$DB->AddOrder("people_cat");
		$res = $DB->Select();
		while ($row = $DB->FetchAssoc($res))
			$this->output["statuses"][] = $row;

		$DB->SetTable($this->db_prefix."faculties");
        $DB->AddOrder("name");
        $res = $DB->Select();
        while($row = $DB->FetchAssoc($res)) {
			$DB->SetTable($this->db_prefix."departments");
            $DB->AddCondFS("faculty_id", "=", $row["id"]);
			$DB->AddOrder("name");
            $res2 =  $DB->Select();
            while($row2 = $DB->FetchAssoc($res2)) {
				if ($Engine->OperationAllowed(5, "teachers.faculty.handle", $row["id"], $Auth->usergroup_id) || $Engine->OperationAllowed(5, "teachers.own.handle", $row2["id"], $Auth->usergroup_id)) {
					$this->output["faculties"][$row["id"]]["fac_name"] = $row["name"];
					$this->output["faculties"][$row["id"]]["departments"][] = $row2;
					$this->output["allow_handle"][$row2["id"]] = 1;
				}
            }
        }

		$DB->SetTable("nsau_groups");
		$DB->AddOrder("name");
		$res = $DB->Select();
		while ($row = $DB->FetchAssoc($res))
			$this->output["groups"][] = $row;

		$DB->SetTable("nsau_types");
		$DB->AddOrder("type");
		$res = $DB->Select();
		while ($row = $DB->FetchAssoc($res))
			$this->output["specialities"][] = $row;

		$DB->SetTable("auth_usergroups");
		$DB->AddOrder("comment");
		$res = $DB->Select();
		while ($row = $DB->FetchAssoc($res))
			$this->output["usergroups"][] = $row;

		$DB->SetTable("auth_users");
		$DB->AddOrder("displayed_name");
		$res = $DB->Select();
		while ($row = $DB->FetchAssoc($res))
			$this->output["users"][] = $row;
		if (!isset($_POST["last_name"])) {
			//$Engine->AddFootstep("/people/".$parts[1]."/", $man["last_name"]." ".$man["name"]." ".$man["patronymic"]);
		}
		else {
			$is_error = false;
			$user_valid = true;
			if (!$_POST["user"]) {
				if(!empty($_POST["username"]) || !empty($_POST["password1"]) || !empty($_POST["password2"]) || !empty($_POST["email"])) {
					if(empty($_POST["username"]) || ((empty($_POST["password1"]) && empty($_POST["password2"]) || $_POST["password1"] != $_POST["password2"]))) {
						$this->output["messages"]["bad"][] = 319;
						$is_error = true;
						$user_valid = false;
					}
				}
				if(empty($_POST["username"]) || empty($_POST["password1"]) || empty($_POST["password2"])) {
					$user_valid = false;
				}
			} /*elseif ($_POST["user"] == 1) {
			}
			if($_POST["people_cat"] == 1) {

			} elseif($_POST["people_cat"] == 2) {
				if(isset($_POST["responsible"]))
					$this->output["display_variant"]["add_item"]["usergroup"] = $_POST["usergroup"];
			} else {
			}*/
			$this->output["display_variant"]["add_item"]["people_cat"] = $_POST["people_cat"];
			$this->output["display_variant"]["add_item"]["responsible"] = $_POST["responsible"];

			$this->output["display_variant"]["add_item"]["last_name"] = $_POST["last_name"];
			$this->output["display_variant"]["add_item"]["first_name"] = $_POST["first_name"];
			$this->output["display_variant"]["add_item"]["patronymic"] = $_POST["patronymic"];
			$this->output["display_variant"]["add_item"]["edit_photo"] = $_FILES["edit_photo"];
			$this->output["display_variant"]["add_item"]["male"] = $_POST["male"];
			$this->output["display_variant"]["add_item"]["status"] = $_POST["status"];
			$this->output["display_variant"]["add_item"]["comment"] = $_POST["comment"];

			$this->output["display_variant"]["add_item"]["prad"] = $_POST["prad"];
			$this->output["display_variant"]["add_item"]["post"] = $_POST["post"];
			$this->output["display_variant"]["add_item"]["exp_full"] = $_POST["exp_full"];
			$this->output["display_variant"]["add_item"]["exp_teach"] = $_POST["exp_teach"];

			//$this->output["display_variant"]["add_item"]["department"] = $_POST["department"];
			foreach($_POST["department"] as $dep_id) {
				$DB->SetTable($this->db_prefix."departments");
				$DB->AddCondFS("id", "=", $dep_id);
				$res2 =  $DB->Select();
				if($row2 = $DB->FetchAssoc($res2)) {
					$this->output["display_variant"]["add_item"]["department"][$row2["id"]] = $row2["name"];
				}
			}

			$this->output["display_variant"]["add_item"]["group"] = $_POST["group"];
			$this->output["display_variant"]["add_item"]["subgroup"] = $_POST["subgroup"];
			$this->output["display_variant"]["add_item"]["speciality"] = $_POST["speciality"];
			$this->output["display_variant"]["add_item"]["st_year"] = $_POST["st_year"];

			$this->output["display_variant"]["add_item"]["usergroup"] = $_POST["usergroup"];
			$this->output["display_variant"]["add_item"]["user_select"] = $_POST["user_select"];
			$this->output["display_variant"]["add_item"]["username"] = $_POST["username"];
			$this->output["display_variant"]["add_item"]["email"] = $_POST["email"];


			if((!isset($_POST["last_name"]) || empty($_POST["last_name"])) || (!isset($_POST["first_name"]) || empty($_POST["first_name"]))) {
				$this->output["messages"]["bad"][] = 317;
				$is_error = true;
			}
			if($_POST["people_cat"] == 1) {	//���� �������
				if(!isset($_POST["group"])) {
					$this->output["messages"]["bad"][] = 314;
					$is_error = true;
				}
				if(!isset($_POST["speciality"])) {
					$this->output["messages"]["bad"][] = 323;
					$is_error = true;
				}
			} elseif($_POST["people_cat"] == 2) {
				$isset_dep = false;
				foreach($_POST["department"] as $dep) {
					if($dep) {
						$isset_dep = true;
						continue;
					}
				}
				if(!$isset_dep) {
					$this->output["messages"]["bad"][] = 318;
					$is_error = true;
				}
				if(!isset($_POST["post"])) {
					$this->output["messages"]["bad"][] = 324;
					$is_error = true;
				}
			}

			if(isset($_POST["last_name"]) && !$is_error) {

                //if(!isset($_POST["check"])) {
                $DB->SetTable($this->db_prefix."people");
                $DB->AddCondFS("last_name", "=", $_POST["last_name"]);
                if(!empty($_POST["first_name"]))
					$DB->AddCondFS("name", "=", $_POST["first_name"]);
                if(!empty($_POST["patronymic"]))
                    $DB->AddCondFS("patronymic", "=", $_POST["patronymic"]);
                $res = $DB->Select();
                if(($row = $DB->FetchAssoc($res)) && !isset($_POST["check"])) {
					if($_POST["people_cat"] == 2) {
						$DB->SetTable($this->db_prefix."teachers");
						$DB->AddCondFS("people_id", "=", $row["id"]);
						$res1 = $DB->Select();
						while($row1 = $DB->FetchAssoc($res1)) {
							if(!(array_search($row1["department_id"], $_POST["department"]) === false)) {
								$row["dep_identic"] = true;
								$DB->SetTable($this->db_prefix."departments");
								$DB->AddCondFS("id", "=", $row1["department_id"]);
								$res2 = $DB->Select();
								if($row2 = $DB->FetchAssoc($res2)) {
									$row["department"][$row2["id"]] = $row2["name"];
								}
							}
						}
					}
					$row["last_name"] = $_POST["last_name"];
					$row["name"] = $_POST["first_name"];
					$row["patronymic"] = $_POST["patronymic"];
					$row["male"] = $_POST["male"];

					$this->output["is_exist"]["people"] = $row;
					$this->output["is_exist"]["people_cat"] = $_POST["people_cat"];
					//$this->output["is_exist"]["user_photo"] = $_FILES["user_photo"]["tmp_name"];
					$this->output["is_exist"]["status"] = $_POST["status"];
					$this->output["is_exist"]["comment"] = $_POST["comment"];

					if($_POST["people_cat"] == 1) {
						$this->output["is_exist"]["group"] = $_POST["group"];
						$this->output["is_exist"]["subgroup"] = $_POST["subgroup"];
						$this->output["is_exist"]["speciality"] = $_POST["speciality"];
						$this->output["is_exist"]["st_year"] = $_POST["st_year"];
					} elseif($_POST["people_cat"] == 2) {
						//$this->output["is_exist"]["department"] = $_POST["department"];
						$this->output["is_exist"]["exp_full"] = $_POST["exp_full"];
						$this->output["is_exist"]["exp_teach"] = $_POST["exp_teach"];
						$this->output["is_exist"]["prad"] = $_POST["prad"];
						$this->output["is_exist"]["post"] = $_POST["post"];
					}

					$this->output["is_exist"]["username"] = $_POST["username"];
					$this->output["is_exist"]["email"] = $_POST["email"];
					$this->output["is_exist"]["password1"] = $_POST["password1"];
					$this->output["is_exist"]["password2"] = $_POST["password2"];
					$this->output["is_exist"]["user"] = $_POST["user"];

					if($_POST["people_cat"] == 2) {
						if(isset($_POST["responsible"]))
							$this->output["is_exist"]["usergroup"] = $_POST["usergroup"];
					} elseif($_POST["people_cat"] == 3) {
						$this->output["is_exist"]["usergroup"] = $_POST["usergroup"];
					}
					if ($_POST["user"] == 1) {
						$this->output["is_exist"]["user_select"] = $_POST["user_select"];
					}
                }
                else {
                    $DB->SetTable($this->db_prefix."people");
                    $DB->AddValue("user_id", "0");
                    $DB->AddValue("last_name", $_POST["last_name"]);
                    $DB->AddValue("name", $_POST["first_name"]);
                    $DB->AddValue("patronymic", $_POST["patronymic"]);
                    $DB->AddValue("male", $_POST["male"]);
					$DB->AddValue("comment", $_POST["comment"]);
			        $DB->AddValue("status_id", (int)$_POST["status"]);
					$DB->AddValue("people_cat", $_POST["people_cat"]);

					if($_POST["people_cat"] == 1) {
						$DB->AddValue("id_group", $_POST["group"]);
						if($_POST["subgroup"]) {
							$DB->AddValue("subgroup", $_POST["subgroup"]);
						}
						if ($_POST["st_year"]) {
							$DB->AddValue("year", $_POST["st_year"]);
						}
					} elseif($_POST["people_cat"] == 2) {
						$DB->AddValue("prad", $_POST["prad"]);
						$DB->AddValue("exp_full", $_POST["exp_full"]);
						$DB->AddValue("exp_teach", $_POST["exp_teach"]);
					}
                       $DB->Insert();
					$peopleid = $DB->LastInsertID();
					//$res2 = $DB->Exec("SELECT max(LAST_INSERT_ID(id)) FROM nsau_people limit 1");
					if($peopleid) {
                        if($user_valid) {
							if($_POST["people_cat"] == 1) {
								$user_id = $this->insert_user($peopleid, 3);
								if(!($user_id === false))
									$Engine->LogAction($this->module_id, "student", $user_id, "add");
							} elseif($_POST["people_cat"] == 2) {
								if(isset($_POST["responsible"])) {
									$user_id = $this->insert_user($peopleid, $_POST["usergroup"]);
								} else {
									$user_id = $this->insert_user($peopleid, 2);
								}
								if(!($user_id === false))
									$Engine->LogAction($this->module_id, "teacher", $user_id, "add");
							} else {
								$user_id = $this->insert_user($peopleid, $_POST["usergroup"]);
								if(!($user_id === false))
									$Engine->LogAction($this->module_id, "user", $user_id, "add");
							}
                        }
						$Engine->AddPrivilege($this->module_id, "people.handle", $peopleid, $Auth->usergroup_id, 1, "add_teachers");
						if($_POST["people_cat"] == 1) {
							$spec = $_POST["speciality"];

							$DB->SetTable($this->db_prefix."types");
							$DB->AddCondFS("id", "=", $spec);
							$res = $DB->Select();
							$row = $DB->FetchAssoc($res);

							$spec_parts = explode(".", $row["code"]);
							$speciality_id = $spec_parts[0];
							$qual = "";
							if (isset($spec_parts[1]))
								$qual = $spec_parts[1];

							$DB->SetTable($this->db_prefix."spec_abit");
							$DB->AddValue("speciality_id", $speciality_id);
							$DB->AddValue("type_id", $spec);
							$DB->AddValue("qualification", $qual);
							$DB->AddValue("type", $row["type"]);
							$DB->AddValue("people_id", $peopleid);
							$DB->Insert();
						} elseif($_POST["people_cat"] == 2) {
							if(isset($_POST["department"])) {
								foreach($_POST["department"] as $dep) {
									if($dep) {
										$DB->SetTable($this->db_prefix."teachers");
										$DB->AddValue("people_id", $peopleid);
										$DB->AddValue("department_id", $dep);
										$DB->AddValue("post", $_POST['post']);
										$DB->Insert();
									}
								}
							}
						}
						if($_POST["people_cat"] == 2 && isset($_POST["email"]) && !empty($_POST["email"])) {
							$deport_mass = null;
							if(isset($_POST["department"])) {
								foreach($_POST["department"] as $dep) {
									$DB->SetTable($this->db_prefix."departments");
									$DB->AddCondFS("id", "=", $dep);
									$res1 = $DB->Select();
									if($row1 = $DB->FetchAssoc($res1)) {
										if(!is_null($deport_mass)) {
											$deport_mass .= ", ".$row1["name"];
										} else {
											$deport_mass = $row1["name"];
										}
									}
								}
							}
							if(is_null($deport_mass)) {
								$deport_mass = "�� �������";
							}

							$mail_message_patterns = array(
								'headers' => array(
										'%site_short_name%' => "����<admin@".$_SERVER['HTTP_HOST'].">",
										'%server_name%' => $_SERVER['HTTP_HOST'],
										'%email%' => $_POST["email"],
										'%subject%' => SITE_SHORT_NAME
									),
								'subject' => array(
										'%site_short_name%' => SITE_SHORT_NAME
									),
								'content' => array(
										'%last_name%' => $_POST["last_name"],
										'%first_name%' => $_POST["first_name"],
										'%patronymic%' => $_POST["patronymic"],
										'%server_name%' => $_SERVER['HTTP_HOST'],
										'%department%' => $deport_mass,
										'%login%' => $_POST["username"],
										'%password%' => $_POST["password1"]
									),
								'message' => array(
										'%site_short_name%' => SITE_SHORT_NAME,
										'%time%' => date('Y-m-d H:i:s'),
										'%content%' => &$this->settings['content']['mess']
									)
							);

							$this->settings['content']['mess'] = str_replace( array_keys($mail_message_patterns['content']), $mail_message_patterns['content'], $this->settings['content']['mess']);

							foreach ($this->settings['notify_settings'] as $ind=>$setting) {
								$this->settings['notify_settings'][$ind] = str_replace( array_keys($mail_message_patterns[$ind]), $mail_message_patterns[$ind], $setting);
							}

							mail($_POST["email"], $this->settings['notify_settings']['subject'], $this->settings['notify_settings']['message'], $this->settings['notify_settings']['headers']);

						}
						/**/
					}
					$this->output["display_variant"] = null;
					if ($_FILES["user_photo"]["tmp_name"] && $FILE_DATA = $_FILES["user_photo"]) {
						if (is_array($result = $this->Imager->GrabUploadedFile($FILE_DATA))) {
							$this->status = false;
							$this->output["messages"] = array_merge($this->output["messages"], $result);
						}
						if (is_array($result = $this->Imager->CreateOutputFiles($peopleid))) {
							$this->status = false;
							$this->output["messages"] = array_merge($this->output["messages"], $result);
						}

						$DB->SetTable($this->db_prefix."people");
						$DB->AddCondFS("id", "=", $peopleid);
						$DB->AddValue("photo", $this->Imager->base_filename.".".$this->Imager->ext);
						$DB->Update();
					}

					CF::Redirect("/people/".$peopleid."/");
				}
			}
		}
	}

	function delete_people($module_uri)
	{
		global $DB, $Engine;

		$parts = explode ("/", $module_uri);

		$DB->SetTable($this->db_prefix."people");
		$DB->AddField("user_id");
		$DB->AddField("last_name");
		$DB->AddField("name");
		$DB->AddField("patronymic");
		$DB->AddField("photo");
		$DB->AddCondFS("id", "=", $parts[1]);
		$res = $DB->Select();
		$man = $DB->FetchAssoc($res);

		$this->output["people_id"] = $parts[1];

		if ($man["user_id"])
			$this->output["is_user"] = 1;

		if ($man["photo"]) {
			$thumb = explode(".", $man["photo"]);
			$thumb[0] .= "_tn";
			$thumb = implode(".", $thumb);
		}
		if (!isset($_GET["confirm"])) {
			if ($man) {
				$this->output["person_name"] = $man["last_name"]." ".$man["name"]." ".$man["patronymic"];
			}
		}

		elseif ($_GET["confirm"] == 1) {
			$DB->SetTable($this->db_prefix."people");
			$DB->AddCondFS("id", "=", $parts[1]);
			$DB->Delete();
			if ($man["photo"] && $thumb) {
				unlink(COMMON_IMAGES.'/'.$this->images_dir.'/'.$man["photo"]);
				unlink(COMMON_IMAGES.'/'.$this->images_dir.'/'.$thumb);
			}
			
			$DB->SetTable($this->db_prefix . "teachers");
			$DB->AddCondFS("people_id", "=", $parts[1]);
			$DB->Delete();

			$Engine->LogAction($this->module_id, "people", $parts[1], "delete");
			CF::Redirect($Engine->engine_uri);
		}
		elseif ($_GET["confirm"] == 2) {
			$DB->SetTable($this->db_prefix."people");
			$DB->AddCondFS("id", "=", $parts[1]);
			$DB->Delete();
			if ($man["photo"] && $thumb) {
				unlink(COMMON_IMAGES.'/'.$this->images_dir.'/'.$man["photo"]);
				unlink(COMMON_IMAGES.'/'.$this->images_dir.'/'.$thumb);
			}

			$DB->SetTable("auth_users");
			$DB->AddCondFS("id", "=", $man["user_id"]);
			$DB->Delete();

			$DB->SetTable($this->db_prefix . "teachers");
			$DB->AddCondFS("people_id", "=", $parts[1]);
			$DB->Delete();
			
			$Engine->LogAction($this->module_id, "people", $parts[1], "delete");
			CF::Redirect($Engine->engine_uri);
		}
		else {
			//CF::Redirect("/people/".$parts[1]."/");
			CF::Redirect($Engine->engine_uri);
		}
	}

	function files_list($user_id)
	{
		global $DB;
		$people_id = $user_id;
		$DB->SetTable($this->db_prefix."people");
		$DB->AddField("user_id");
		$DB->AddCondFS("id", "=", $user_id);
		$res = $DB->Select();
		$row = $DB->FetchAssoc($res);
		$user_id = $row["user_id"];
		if(isset($user_id) || isset($people_id))
		{
			include_once INCLUDES . "EFiles" . CLASS_EXT;
			$EFile = new EFiles(null,"view_files_list;".$user_id.";".$people_id);
			$this->output["files_list"] = $EFile->output["files_list_test"];
		}

	}

     function groups($module_uri)
    {
        global $DB, $Engine, $Auth;

        if(!empty($module_uri))
        {
            $parts = explode ("/", $module_uri);
            if($Engine->OperationAllowed($this->module_id, "timetable.handle", -1, $Auth->usergroup_id))
            {
                if(!empty($parts[1]))//������ ���� � ������ edit ��� delete => �����������  ��� ������� ������ ���������� ������
                {
                    if($parts[1] == "delete" && !empty($parts[2]))//�������� �������� � id = $parts[2]
                    {
                        $DB->SetTable($this->db_prefix."timetable");
                        $DB->AddCondFS("id","=",$parts[2]);
                        $DB->Delete();
                        CF::Redirect($Engine->engine_uri.$parts[0]."/");
                    }

                    if($parts[1] == "edit")//�������������� �������� � id = $parts[2]
                    {
                        if(!isset($_POST["id"]))
                        {
                            $DB->SetTable($this->db_prefix."timetable");
                            $DB->AddCondFS("id","=",$parts[2]);
                            $res = $DB->Select();
                            $this->output["edit_timetable"] = array();
                            if($row = $DB->FetchAssoc($res))
                            {
                                $this->output["edit_timetable"] = $row;
                            }
                        }
                        else
                        {
                            $DB->SetTable($this->db_prefix."timetable");
                            $DB->AddCondFS("id","=",$_POST["id"]);
                            $DB->AddValue("group_id", $parts[0]);
                            if($_POST["subgroup"])
                                $DB->AddValue("subgroup",$_POST["subgroup"]);
                            else
                                $DB->AddValue("subgroup","");
                            $DB->AddValue("num",$_POST["num"]);
                            $DB->AddValue("dow",$_POST["dow"]);
                            if(!$_POST["parity"])
                            {
                                $DB->AddValue("parity","both");
                            }
                            else
                            {
                                if($_POST["parity"] == 1)
                                {
                                    $DB->AddValue("parity","odd");
                                }
                                else
                                {
                                    $DB->AddValue("parity","even");
                                }
                            }
                            $DB->AddValue("subject_id",$_POST["subject"]);
                            $DB->AddValue("auditorium_id",$_POST["auditorium"]);
                            $DB->AddValue("lesson_view",$_POST["lesson_view"]);
                            $DB->Update();
                            CF::Redirect($Engine->engine_uri.$parts[0]."/");
                        }
                    }
                }
            }
            if(empty($parts[1]))
            {
								$_GET["gr"] = $parts[0];
                $DB->SetTable($this->db_prefix."groups");
                $DB->AddCondFS("id", "=", $parts[0]); // id ������
                $DB->AddCondFS("hidden", "=", 0);
                $res = $DB->Select(1);
                if($row = $DB->FetchAssoc($res))
                    $this->output["group"] = $row;
                if(!empty($row["id_faculty"]))
                {
                    $DB->SetTable($this->db_prefix."faculties");
                    $DB->AddCondFS("id", "=", $row["id_faculty"]);
                    if($res = $DB->Select(1))
                        $this->output["group"]["faculty"] = $DB->FetchAssoc($res);
                }
                if(isset($_POST["num"]))//���������� ������ � ���������� ������� ������ � id = $parts[0]
                {
                    $DB->SetTable($this->db_prefix."timetable");
                    $DB->AddValue("group_id", $parts[0]);
                    if($_POST["subgroup"])
                        $DB->AddValue("subgroup",$_POST["subgroup"]);
                    else
                        $DB->AddValue("subgroup","");
                    $DB->AddValue("num",$_POST["num"]);
                    $DB->AddValue("dow",$_POST["dow"]);
                    if(!$_POST["parity"])
                    {
                        $DB->AddValue("parity","both");
                    }
                    else
                    {
                        if($_POST["parity"] == 1)
                        {
                            $DB->AddValue("parity","odd");
                        }
                        else
                        {
                            $DB->AddValue("parity","even");
                        }
                    }
                    $DB->AddValue("subject_id",$_POST["subject"]);
                    $DB->AddValue("auditorium_id",$_POST["auditorium"]);
                    $DB->AddValue("lesson_view",$_POST["lesson_view"]);
                    $DB->AddValue("date_from",$_POST["date_from"]);
                    $DB->AddValue("date_to",$_POST["date_to"]);
					$this->output["display_variant"] = "add_timetable";
                    if($DB->Insert(true, false)) {
						$this->output["messages"]["good"][] = 321;
					} else {
						$this->output["messages"]["bad"][] = 320;
					}
                }
            }
            $this->timetable($parts[0]);
        }
        else
            if(isset($_GET["group"]) && trim($_GET["group"]) != "")
            {
                $DB->SetTable($this->db_prefix."groups", "g");
                $DB->AddTable($this->db_prefix."faculties", "f");
                $DB->AddField("g.id", "id");
                $DB->AddField("g.name", "name");
                $DB->AddField("f.name", "faculty");
                $DB->AddCondFS("g.name", "LIKE", "%".$_GET["group"]."%");
                $DB->AddCondFS("g.hidden", "=", 0);
                $DB->AddCondFF("g.id_faculty", "=", "f.id");
                $res = $DB->Select();
                $this->output["groups"] = array();
                while($row = $DB->FetchAssoc($res))
                {
                    $this->output["groups"][] = $row;
                }
            } else {
				$this->output["messages"]["bad"][] = 314;
			};
    }



    function subjects($module_uri)
    {

        global $DB, $Auth;
        if(!empty($module_uri))
        {

            $parts = explode ("/", $module_uri);
            $DB->SetTable($this->db_prefix."subjects");
            $DB->AddCondFS("id", "=", $parts[0]); // id ��������
            $res = $DB->Select(1);
            if($row = $DB->FetchAssoc($res))
                $this->output["subject"]["name"] = $row["name"];

            //$this->timetable($parts[0]); // id ������

			$DB->SetTable("nsau_files_subj", "nfs");
			$DB->AddTable("nsau_files", "nf");
			$DB->AddCondFF("nf.id", "=", "nfs.file_id");
			$DB->AddCondFS("nf.deleted", "=", 0);
			$DB->AddField("nfs.file_id");
			$DB->AddCondFS("nfs.subject_id", "=", $parts[0]);
			$DB->AddCondFS("nfs.approved", "=", 1);
			$res_att = $DB->Select(null,null,true,true,true);

			$i = 0;
			while ($row_att = $DB->FetchAssoc($res_att)) {
				$DB->SetTable("nsau_files");
				$DB->AddField("id");
				$DB->AddField("name");
				$DB->AddField("filename");
				$DB->AddField("descr");
				$DB->AddField("author");
				$DB->AddField("year");
				$DB->AddField("volume");
				$DB->AddField("edition");
				$DB->AddField("place");
				$DB->AddField("user_id");
				$DB->AddCondFS("id", "=", $row_att["file_id"]);
				$DB->AddCondFS("deleted", "=", 0);
				$res = $DB->Select();
				$row = $DB->FetchAssoc($res);
				$row["name"] .= ".".$row["filename"];


				$DB->SetTable("nsau_people");
				$DB->AddField("id");
				$DB->AddField("last_name");
				$DB->AddField("name");
				$DB->AddField("patronymic");
				$DB->AddCondFS("user_id", "=", $row["user_id"]);
				$res_user = $DB->Select();
				$row_user = $DB->FetchAssoc($res_user);

				if (sizeof($row_user) && isset($row_user["last_name"]))
					$row["user_name"] = $row_user["last_name"]." ".$row_user["name"]." ".$row_user["patronymic"];
				else {
					$DB->SetTable("auth_users");
					$DB->AddField("displayed_name");
					$DB->AddCondFS("id", "=", $row["user_id"]);
					$res_user = $DB->Select();
					$row_user = $DB->FetchAssoc($res_user);
					$row["user_name"] = $row_user["displayed_name"];
				}

				$row["user_id"] = $row_user["id"];
				$this->output["subject"]["files"][$i] = $row;
				$i++;
			}

        }
        else

            if(isset($_GET["subject_name"]))
            {
                $DB->SetTable($this->db_prefix."subjects", "s");
                $DB->AddField("s.id", "id");
                $DB->AddField("s.name", "name");
                $DB->AddCondFS("s.name", "LIKE", "%".$_GET["subject_name"]."%");
                $DB->AddCondFS("s.is_hidden", "=", 0);
                $res = $DB->Select();
                $this->output["subjects"] = array();
                while($row = $DB->FetchAssoc($res))
                {
                	$DB->SetTable("nsau_files_subj");
                	$DB->AddCondFS("subject_id", "=", $row["id"]);
                	$DB->AddCondFS("approved", "=", 1);
                	$DB->AddExp("count(*)", "num_files");
                	$res_att = $DB->Select();
                	while ($row_att = $DB->FetchAssoc($res_att)) {
                		$row["num_files"] = $row_att["num_files"];
                	}
                    $this->output["subjects"][] = $row;
                }

            };
    }


    function timetable($group_id = NULL)
    {
        global $DB, $Auth, $Engine;
        $this->output["now"] = $now = $now = getdate();
        $DB->SetTable($this->db_prefix."timetable", "t");
        $DB->AddTable($this->db_prefix."subjects", "s");
        $DB->AddTable($this->db_prefix."buildings", "b");
        $DB->AddTable($this->db_prefix."auditorium","a");
        $DB->AddField("t.num");
        $DB->AddField("t.dow");
        $DB->AddField("t.parity");
        $DB->AddField("t.id");
        $DB->AddField("t.subgroup");
        $DB->AddField("t.lesson_view");
        $DB->AddField("s.name", "subject");
        $DB->AddField("t.auditorium_id");
        $DB->AddField("a.name","auditorium");
        $DB->AddField("b.label");
        $DB->AddField("b.name", "building");
        $DB->AddCondFF("t.subject_id", "=", "s.id");
        $DB->AddCondFF("t.auditorium_id", "=", "a.id");
        $DB->AddCondFF("a.building_id","=","b.id");
        $DB->AddOrder("t.dow");
        $DB->AddOrder("t.num");
        if(!is_null($group_id))
            $DB->AddCondFS("t.group_id", "=", $group_id);
        $parity = $this->timetable_weekparity();
		$is_both = true;
		if(!isset($_GET['parity']) || $_GET['parity'] != "both") {
			$DB->AddAlt($DB->AddAltFS("t.parity", "=", "both"), $DB->AddAltFS("t.parity", "=", "$parity"));
			$is_both = false;
		}
		if(isset($_GET['subgr']) && $_GET['subgr'] != "all") {
			$DB->AppendAlts();
			$DB->AddAlt($DB->AddAltFS("t.subgroup", "=", ""), $DB->AddAltFS("t.subgroup", "=", ($_GET['subgr'] == "a") ? "�" : "�"));
		}
		$this->output["parity"] = $parity;
        $DB->AddOrder("t.num");
        $res = $DB->Select();
        $this->output["timetable"] = array();
        while($row = $DB->FetchAssoc($res)) {
			if($is_both) {
				if($row["parity"] == "odd" || $row["parity"] == "both") {
					$this->output["timetable"]["odd"][$row["dow"]][$row["num"]][] = $row;
				}
				if($row["parity"] == "even" || $row["parity"] == "both") {
					$this->output["timetable"]["even"][$row["dow"]][$row["num"]][] = $row;
				}
			} else {
				if($row["parity"] == $parity || $row["parity"] == "both") {
					$this->output["timetable"][$parity][$row["dow"]][$row["num"]][] = $row;
				}
			}
        }
        $DB->SetTable($this->db_prefix."buildings");
        $DB->AddOrder("id");
        $res = $DB->Select();
        $this->output["buildings"]=array();
        while($row = $DB->FetchAssoc($res))
        {
            $this->output["buildings"][] = $row;
        }

        if($Engine->OperationAllowed($this->module_id, "timetable.handle", -1, $Auth->usergroup_id))
        {
            $this->output["edit_acces"] = 1;
        }
        else
        {
            $this->output["edit_acces"] = 0;
        }


		$DB->SetTable($this->db_prefix."departments");
        $DB->AddOrder("name");
        $res = $DB->Select();
        while($row = $DB->FetchAssoc($res)) {
			$DB->SetTable($this->db_prefix."subjects");
			$DB->AddCondFS("department_id", "=", $row['id']);
			$DB->AddOrder("name");
			$res1 = $DB->Select();
			while($row1 = $DB->FetchAssoc($res1)) {
				$this->output["dep_subjects"][$row['id']]['dep_name'] = $row['name'];
				$this->output["dep_subjects"][$row['id']]['subjects'][] = $row1;
			}
		}
        $DB->SetTable($this->db_prefix."subjects");
		$DB->AddCondFS("department_id", "=", '0');
        $DB->AddOrder("name");
        $res = $DB->Select();
        while($row = $DB->FetchAssoc($res)) {
			$this->output["dep_subjects"][$row['department_id']]['dep_name'] = "������� �����������";
			$this->output["dep_subjects"][$row['department_id']]['subjects'][] = $row;
        }

        $DB->SetTable($this->db_prefix."auditorium","a");
        $DB->AddTable($this->db_prefix."buildings","b");
        $DB->AddField("a.id","id");
        $DB->AddField("a.name","name");
        $DB->AddField("b.name","building_name");
        $DB->AddField("b.id","building_id");
        $DB->AddCondFF("a.building_id","=","b.id");
        $DB->AddCondFS("a.is_using","=",0);
        $DB->AddOrder("a.building_id");
        $res = $DB->Select();
        while($row = $DB->FetchAssoc($res))
        {
            $this->output["auditorium"][] = $row;
        }
    }



	function students($group_id = NULL)
	{
		if(trim($group_id) == "")
			return;
		global $DB;
		$group_id = explode ("/", $group_id);
		$gid = $group_id[0];
		$DB->SetTable($this->db_prefix."people");
		$DB->AddField("id");
		$DB->AddField("last_name");
		$DB->AddField("name");
		$DB->AddField("patronymic");
        $DB->AddOrder("last_name");
        $DB->AddOrder("name");
        $DB->AddOrder("patronymic");
		$DB->AddCondFS("id_group", "=", $gid);
		$res = $DB->Select();
		while ($row = $DB->FetchAssoc($res)){
			$this->output["students"][] = $row;
		}
	}

	function editStudent($people_id, $code, $group, $lastname, $name, $patronymic) {
		global $DB;
		$DB->SetTable("nsau_people");
		$DB->AddCondFS("library_card", "=", $code);
		$DB->AddCondFS("id", "=", $people_id);
		$DB->AddValue("id_group", $group);
		$DB->AddValue("last_name", $lastname);
		$DB->AddValue("name", $name);
		$DB->AddValue("patronymic", $patronymic);
		$DB->Update();
	}

  function deletePeople($people_id) {
		global $DB;
		$DB->SetTable("nsau_people");
		$DB->AddCondFS("id", "=", $people_id);
		$p = $DB->FetchAssoc($DB->Select(1));
		$DB->SetTable("auth_users");
		$DB->AddCondFS("id", "=", $p["user_id"]);
		$DB->Delete();
		$DB->SetTable("nsau_people");
		$DB->AddCondFS("id", "=", $p["id"]);
		$DB->Delete();
	}

  function addStudent($login, $passmd5, $username, $lastname, $name, $patronymic, $group, $code) {
		global $DB;
		$asql = "INSERT INTO `nsau`.`auth_users`
		(`id`, `is_active`, `ban_reason`, `username`, `password`, `displayed_name`, `email`, `usergroup_id`,
		`create_time`, `create_user_id`, `last_login_time`, `last_ip`, `last_ua`, `prev_login_time`, `prev_ip`, `prev_ua`)
		VALUES (NULL, '1', NULL, '".$login."', '".$passmd5."', '".$username."', NULL, '3',
		'', '0', NULL, NULL, NULL, NULL, NULL, NULL);";
		$DB->Exec($asql);
		$user_id = $DB->LastInsertID();
		$psql = "INSERT INTO `nsau`.`nsau_people`
			(`id`, `user_id`, `last_name`, `name`, `patronymic`, `male`, `status_id`, `people_cat`, `year`,
			`id_group`, `subgroup`, `photo`, `comment`, `pass_num`, `pass_series`, `library_card`, `personal_fid`, `prad`,
			`exp_full`, `exp_teach`, `post`, `email`, `phone`, `location`, `location_building`, `education`, `add_education`)
			VALUES (NULL, '".$user_id."', '".$lastname."', '".$name."', '".$patronymic."', '1', '8', '1', '',
			'".$group."', NULL, NULL, NULL, NULL, NULL, '".$code."', NULL, NULL,
			NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);";
		$DB->Exec($psql);
	}

	function timetable_make()
	{
		global $DB;

		if (count($_POST) && isset($_GET["gr"])) {
			$DB->SetTable("nsau_timetable_new");
			$DB->AddCondFS("group_id", "=", $_GET["gr"]);
			$DB->Delete();

			for ($week=0; $week<=1; $week++) {
				for($day=0; $day<=6; $day++) {
					for($pair=0; $pair<=6; $pair++) {
						$wdps = array($week."_".$day."_".$pair, $week."_".$day."_".$pair."_d", $week."_".$day."_".$pair."_t");
						foreach ($wdps as $wdp) {
							if ($_POST["subj_".$wdp]) {
								if (isset($_POST["copy_".$wdp]) && is_array($_POST["copy_".$wdp])) {
									$grs = $_POST["copy_".$wdp];
									$grs[] = $_GET["gr"];

									foreach($grs as $group_id) {
										$DB->SetTable("nsau_timetable_new");
										$DB->AddCondFS("day","=",$day);
										$DB->AddCondFS("week","=",$week);
										$DB->AddCondFS("pair","=",$pair);
										$DB->AddCondFS("group_id","=",$group_id);
										if (isset($_POST["subgr_".$wdp]) && $_POST["subgr_".$wdp])
											$DB->AddCondFS("subgroup","=",$_POST["subgr_".$wdp]);
										$DB->Delete();


										$DB->SetTable("nsau_timetable_new");
										$DB->AddValue("group_id", $group_id);
										if (isset($_POST["subgr_".$wdp]) && ($_POST["type_".$wdp] == 1 || $_POST["type_".$wdp] == 2))
											$DB->AddValue("subgroup", $_POST["subgr_".$wdp]);
										else
											$DB->AddValue("subgroup", 0);
										$DB->AddValue("subject_id", $_POST["subj_".$wdp]);
										$DB->AddValue("auditorium_id", $_POST["aud_ids_".$wdp]);
										$DB->AddValue("teacher_id", $_POST["teach_ids_".$wdp]);
										$DB->AddValue("day", $day);
										$DB->AddValue("week", $week);
										$DB->AddValue("pair", $pair);
										$DB->AddValue("type", $_POST["type_".$wdp]);
										$DB->AddValue("comment_from", implode("-",array_reverse(explode(".",$_POST["comment_from_".$wdp]))));
										$DB->AddValue("comment_to", implode("-",array_reverse(explode(".",$_POST["comment_to_".$wdp]))));
										$DB->Insert();
									}
								}
								else {
									//echo $_POST["subj_".$wdp].">";
									$DB->SetTable("nsau_timetable_new");
									$DB->AddValue("group_id", $_GET["gr"]);
									if (isset($_POST["subgr_".$wdp]) && ($_POST["type_".$wdp] == 1 || $_POST["type_".$wdp] == 2))
										$DB->AddValue("subgroup", $_POST["subgr_".$wdp]);
									else
										$DB->AddValue("subgroup", 0);
									$DB->AddValue("subject_id", $_POST["subj_".$wdp]);
									$DB->AddValue("auditorium_id", $_POST["aud_ids_".$wdp]);
									$DB->AddValue("teacher_id", $_POST["teach_ids_".$wdp]);
									$DB->AddValue("day", $day);
									$DB->AddValue("week", $week);
									$DB->AddValue("pair", $pair);
									$DB->AddValue("type", $_POST["type_".$wdp]);
									$DB->AddValue("comment_from", implode("-",array_reverse(explode(".",$_POST["comment_from_".$wdp]))));
									$DB->AddValue("comment_to", implode("-",array_reverse(explode(".",$_POST["comment_to_".$wdp]))));
									$DB->Insert();
								}
							}
						}
						//die();
					}
				}
			}

			$DB->SetTable("nsau_timetable_dates");
			$DB->AddCondFS("group_id", "=", $_GET["gr"]);
			$DB->Delete();

			$DB->SetTable("nsau_timetable_dates");
			$DB->AddValue("group_id", $_GET["gr"]);
			$DB->AddValue("created", date("Y-m-d"));
			$DB->AddValue("actual_from", implode("-",array_reverse(explode(".",$_POST["actual_from"]))));
			$DB->AddValue("actual_to", implode("-",array_reverse(explode(".",$_POST["actual_to"]))));
			$DB->Insert();
		}

		$DB->SetTable("nsau_groups");
		$DB->AddCondFS("id", "=", $_GET["gr"]);
		$res = $DB->Select();
		$row = $DB->FetchAssoc($res);

		$DB->SetTable("nsau_groups");
		$DB->AddCondFS("form_education", "=", $row["form_education"]);
		$DB->AddCondFS("year", "=", $row["year"]);
		$DB->AddCondFS("id_faculty", "=", $row["id_faculty"]);
		$DB->AddCondFS("hidden", "=", 0);
		$res = $DB->Select();

		while ($row = $DB->FetchAssoc($res)){
			$this->output["potok"][$row["id"]] = $row["name"];
		}

		$DB->SetTable("nsau_groups");
		$DB->AddCondFS("hidden", "=", 0);
                $res = $DB->Select();
                while ($row = $DB->FetchAssoc($res)){
                        $this->output["groups"][$row["id"]] = $row;
			if (isset($_GET["gr"]) && ($row["id"] == $_GET["gr"]))
				$fac_id = $row["id_faculty"];
                }

		if (isset($fac_id)) {
		$sql = "SELECT x.id as id, x.name as name, y.name as kaf, z.name as fac FROM nsau_subjects x LEFT JOIN nsau_departments y ON y.id = x.department_id LEFT JOIN nsau_faculties z ON z.id = y.faculty_id ORDER BY fac DESC, kaf, name";
			$res = $DB->Exec($sql);
		}
		else {
			$DB->SetTable("nsau_subjects");
        	        $res = $DB->Select();
		}
                while ($row = $DB->FetchAssoc($res)){
                        $this->output["subjects"][$row["id"]] = $row;
                }

		$sql = "SELECT * FROM nsau_faculties WHERE id in (SELECT id_faculty FROM nsau_groups)";
                $res = $DB->Exec($sql);
                while ($row = $DB->FetchAssoc($res)){
                        $this->output["faculties"][$row["id"]] = $row;
                }

                $sql = "SELECT x.id as id, people_id, name, last_name, patronymic FROM nsau_teachers x LEFT JOIN nsau_people y ON y.id = x.people_id WHERE name IS NOT NULL OR last_name IS NOT NULL ORDER BY last_name, name";
                $res = $DB->Exec($sql);
                while ($row = $DB->FetchAssoc($res)){
                        $this->output["teachers"][$row["id"]] = $row;
                }

                $sql = "SELECT x.id as aud_id, x.name as aud_name, y.label as label FROM nsau_auditorium x LEFT JOIN nsau_buildings y ON y.id = x.building_id ORDER BY label, aud_name";
                $res = $DB->Exec($sql);
                while ($row = $DB->FetchAssoc($res)){
                        $this->output["auds"][$row["aud_id"]] = $row;
                }

		if (isset($_GET["gr"])) {
		        $DB->SetTable("nsau_timetable_new");
			$DB->AddCondFS("group_id", "=", $_GET["gr"]);
			if($_GET["week"]!="")
				$DB->AddCondFS("izop_week", "=", $_GET["week"]);
			$DB->AddOrder("id");
			$res = $DB->Select();
			while ($row = $DB->FetchAssoc($res)) {
				if ($row["week"]) {
					if (!isset($this->output["odd"][$row["day"]][$row["pair"]]))
				    $this->output["odd"][$row["day"]][$row["pair"]] = $row;
					elseif(!isset($this->output["doubles"][$this->output["odd"][$row["day"]][$row["pair"]]["id"]]))
						$this->output["doubles"][$this->output["odd"][$row["day"]][$row["pair"]]["id"]] = $row;
					else
						$this->output["triples"][$this->output["odd"][$row["day"]][$row["pair"]]["id"]] = $row;

				}
				else {
					if (!isset($this->output["even"][$row["day"]][$row["pair"]]))
			      $this->output["even"][$row["day"]][$row["pair"]] = $row;
					elseif(!isset($this->output["doubles"][$this->output["even"][$row["day"]][$row["pair"]]["id"]]))
						$this->output["doubles"][$this->output["even"][$row["day"]][$row["pair"]]["id"]] = $row;
					else
						$this->output["triples"][$this->output["even"][$row["day"]][$row["pair"]]["id"]] = $row;
				}
		        }

			$DB->SetTable("nsau_timetable_dates");
			$DB->AddCondFS("group_id", "=", $_GET["gr"]);
			$DB->AddCondFS("week", "=", $_GET["week"]);
			$res = $DB->Select();
			if ($row = $DB->FetchAssoc($res)) {
				$this->output["dates"] = $row;
			}
		}
    }
    function timetable_groups($mode)						/*+++++++++++++++++++++++++++++++++++++*/
    {
        global $DB, $Engine;
        if(isset($_POST["mode"]))
            $mode[0] = $_POST["mode"];
        switch($mode[0])
        {
					case "edit":

					{
						if(!isset($_POST["id"]))

						{
							$DB->SetTable($this->db_prefix."groups");
							$DB->AddCondFS("id", "=", $mode[1]);
							$res = $DB->Select();
							if($row = $DB->FetchAssoc($res)) {
                        /*$DB->SetTable($this->db_prefix."faculties");
                        $DB->AddCondFS("id", "=", $row["id_faculty"]);
                        $res1 = $DB->Select();
                        if($row1 = $DB->FetchAssoc($res1))
                            $row["faculty_name"] = $row1["name"];
                        $DB->SetTable($this->db_prefix."faculties");
						$DB->AddOrder("name");
                        $res1 = $DB->Select();
                        while($row2 = $DB->FetchAssoc($res1)) {
                            $this->output["faculties"][]=$row2;
                        }

                        $DB->SetTable($this->db_prefix."specialities");
                        $DB->AddCondFS("id_faculty", "=", $row["id_faculty"]);
						$DB->AddOrder("name");
                        $res1 = $DB->Select();
                        while($row2 = $DB->FetchAssoc($res1)) {
                            $this->output["spec"][]=$row2;
							if(isset($row["specialities_code"]) && $row2["code"] == $row["specialities_code"])
								$row["spec_code"] = $row["specialities_code"];
                        }*/

						$DB->SetTable($this->db_prefix."faculties");
						$DB->AddCondFP("is_active");
						$DB->AddOrder("name");
						$res1 = $DB->Select();
						while($row1 = $DB->FetchAssoc($res1)) {
							$DB->SetTable($this->db_prefix."specialities");
							$DB->AddCondFS("id_faculty", "=", $row1["id"]);
							$DB->AddOrder("name");
							$res2 = $DB->Select();
							while($row2 = $DB->FetchAssoc($res2)) {

								$this->output["faculties"][$row1["id"]]["fac_name"] = $row1["name"];
								$this->output["faculties"][$row1["id"]]["spec"][]=$row2;
								if(isset($row["specialities_code"]) && $row2["code"] == $row["specialities_code"])
									$row["spec_code"] = $row["specialities_code"];
							}
							if($row1["id"] == $row["id_faculty"])
								$row["faculty_name"] = $row1["name"];
						}

						$DB->SetTable("nsau_specialities");
                		$DB->AddCondFS("code", "=", $row['specialities_code']);
                		$DB->AddCondFS("id_faculty", "=", $row['id_faculty']);
                		$res6 = $DB->Select(1);
        				$in_spec = $DB->FetchAssoc($res6);

        				$DB->SetTable("nsau_profiles");
                		$DB->AddCondFS("spec_id", "=", $in_spec['id']);
		                $res7 = $DB->Select();
		                while($row7 = $DB->FetchAssoc($res7)) 
		                {
		                    $spec[] = array('id'=> $row7['id'], 'name' => $row7['name']);
		                }

		                $this->output["profiles"]=$spec;
                        $this->output["edit_group"][]=$row;
                        $this->output["groups_mode"]="edit";
                    }
                }
                else
                {

									$DB->SetTable($this->db_prefix."groups");
									$DB->AddCondFS("id", "=", $_POST["id"]);
									$DB->AddValue("name", $_POST["group_name"]);
									$fac = explode("|", $_POST["faculty"]);
									$DB->AddValue("id_faculty", $fac[0]);
									$DB->AddValue("year", $_POST["year"]);
									$DB->AddValue("specialities_code", $fac[1]);
									$DB->AddValue("qualification", $_POST["qualif"]);
									$DB->AddValue("form_education", $_POST["form_edu"]);
									$DB->AddValue("profile", $_POST["profile"]);

									$DB->AddValue("hidden", ($_POST["hide"]==1 ? "1" : "0"));
									$DB->Update();
									$Engine->LogAction($this->module_id, "group", $_POST["id"], "edit");
									$mode[0]=NULL;

									$this->output["groups_mode"]=NULL;
									CF::Redirect($Engine->engine_uri);
                }
                break;
            }
            case "delete":
            {
                $DB->SetTable($this->db_prefix."groups");
                $DB->AddCondFS("id", "=", $mode[1]);
                $DB->Delete();
                $mode[0]=NULL;
                $this->output["groups_mode"]=NULL;
                CF::Redirect($Engine->engine_uri);
            }
            case "edit_timetable":
            {

                //$this->output["edit_timetable"][]=$row;
                $this->output["groups_mode"]="edit_timetable";
            }
            default: {
							if(isset($_POST["group_name"]) && !empty($_POST["group_name"])) {
								$_POST["group_name"] = str_replace(" ", "", $_POST["group_name"]);


								$DB->SetTable($this->db_prefix."groups");
								$DB->AddCondFS("name","=",$_POST["group_name"]);
								$res = $DB->Select();
								if($row = $DB->FetchAssoc($res))
								{
										if($row["hidden"]) $this->output["messages"]["bad"][] = 338;
										else
										$this->output["messages"]["bad"][] = 304;
										$this->output["id"] = $row["id"];
								}
								else
								{
									$DB->SetTable($this->db_prefix."groups");
									$DB->AddCondFS("name", "=", $_POST["group_name"]);
									$fac = explode("|", $_POST["faculties"]);
									$DB->AddCondFS("id_faculty", "=", $fac[0]);
									$DB->AddCondFS("specialities_code", "=", $fac[1]);
									$DB->AddCondFS("qualification", "=", $_POST["qualif"]);
									$DB->AddValue("form_education", $_POST["form_edu"]);
									$res = $DB->Select();
									if(!($row = $DB->FetchAssoc($res))) {
										$DB->SetTable($this->db_prefix."groups");
										$DB->AddValue("name", $_POST["group_name"]);
										$DB->AddValue("id_faculty", $fac[0]);
										$DB->AddValue("specialities_code", $fac[1]);
										$DB->AddValue("qualification", $_POST["qualif"]);
										$DB->AddValue("form_education", $_POST["form_edu"]);
										$DB->AddValue("year", $_POST["year"]);

										if($DB->Insert())
										{
											$this->output["add_group_status"]="ok";
										}
										else
										{
											$this->output["add_group_status"]="fail";
										}
									}
								}
              }

                $DB->SetTable($this->db_prefix."groups");
                $DB->AddOrder("id_faculty");
                $DB->AddOrder("form_education");							
                $DB->AddOrder("name");
                $res =  $DB->Select();
                while($row = $DB->FetchAssoc($res))
                {
                    $DB->SetTable($this->db_prefix."faculties");
                    $DB->AddCondFS("id", "=", $row["id_faculty"]);
                    $res2 = $DB->Select();
                    if($row2 = $DB->FetchAssoc($res2))
                    {
                        $row["faculty_name"] = $row2["name"];
                        if($row2["pid"])
                        {
                            $DB->SetTable($this->db_prefix."faculties");
                            $DB->AddCondFS("id", "=", $row2["pid"]);
                            $res3 = $DB->Select();
                            if($row3 = $DB->FetchAssoc($res3))
                            {
                                $row["institute_name"] = $row3["name"];
                            }
                        }
                        $row["institute_name"]=NULL;
                    }
                    $this->output["timetable_groups"][$row["id_faculty"]][$row["form_education"]][] = $row;
                }
                $DB->SetTable($this->db_prefix."faculties");
				$DB->AddCondFP("is_active");
                $DB->AddOrder("name");
                $res = $DB->Select();
                /*while($row = $DB->FetchAssoc($res)) {
					$DB->SetTable($this->db_prefix."specialities");
					$DB->AddCondFS("id_faculty", "=", $row["id"]);
					$DB->AddOrder("name");
					$res1 = $DB->Select();
                    while($row2 = $DB->FetchAssoc($res1)) {
						$this->output["faculties"][$row["id"]]["fac_name"] = $row["name"];
                        $this->output["faculties"][$row["id"]]["spec"][]=$row2;
                    }
                }*/

                $DB->SetTable($this->db_prefix."specialities");
                	while($row = $DB->FetchAssoc($res)) {
						$DB->AddAltFS("id_faculty", "=", $row["id"]);
						$facNames[$row["id"]] = $row["name"];
						$facPos[$row["id"]] = $row["group_num"];
                	}
                	$facNames[0] = "��������� �� ������";
					$DB->AddAltFS("id_faculty", "=", 0);
					$DB->AppendAlts();
					$DB->AddOrder("name");
					$res1 = $DB->Select();
                    while($row2 = $DB->FetchAssoc($res1)) {
						$this->output["faculties"][$row2["id_faculty"]]["fac_name"] = $facNames[$row2["id_faculty"]];
						$this->output["faculties"][$row2["id_faculty"]]["fac_pos"] = $facPos[$row2["id_faculty"]];
                        $this->output["faculties"][$row2["id_faculty"]]["spec"][]=$row2;
                    }

                break;
            }
        }

    }

    function timetable_faculties($mode)
    {
        global $DB, $Engine;
        if(isset($_POST["mode"]))
            $mode[0] = $_POST["mode"];
        switch($mode[0])
        {
            case "edit":
            {
                if(!isset($_POST["id"]))
                {
                    $DB->SetTable($this->db_prefix."faculties");
                    $DB->AddCondFS("id", "=", $mode[1]);
                    $res = $DB->Select();
                    if($row = $DB->FetchAssoc($res))
                    {
                        if($row["pid"])
                        {
                            $DB->SetTable($this->db_prefix."faculties");
                            $DB->AddCondFS("id", "=", $row["pid"]);
							$DB->AddOrder("pos");
                            $res1 = $DB->Select();
                            if($row2 = $DB->FetchAssoc($res1))
                                $row["pid_name"] = $row2["name"];
                        }
                        $DB->SetTable($this->db_prefix."faculties");
						$DB->AddOrder("pos");
                        $res1 = $DB->Select();
                        while($row2 = $DB->FetchAssoc($res1))
                        {
                            $this->output["faculties"][]=$row2;
                        }
                        $this->output["edit_faculty"][]=$row;
                        $this->output["faculty_mode"]="edit";
                    }
                }
                else
                {
                    $DB->SetTable($this->db_prefix."faculties");
                    $DB->AddCondFS("id", "=", $_POST["id"]);
                    $DB->AddValue("name", $_POST["faculty_name"]);
                    $DB->AddValue("pid", $_POST["pid"]);
					$DB->AddValue("link", $_POST["faculty_link"]);
					$DB->AddValue("pos", $_POST["pos"]);
					$DB->AddValue("foundation_year", $_POST["foundation_year"]);
					$DB->AddValue("comment", $_POST["comment"]);
					if (isset($_POST["is_active"]) && $_POST["is_active"])
						$DB->AddValue("is_active", 1);
					else
						$DB->AddValue("is_active", 0);
                    $DB->Update();
                    $mode[0]=NULL;
                    $this->output["faculty_mode"]=NULL;
                    CF::Redirect($Engine->engine_uri);
                }
                break;
            }
            case "delete":
            {
                $DB->SetTable($this->db_prefix."departments");
                $DB->AddCondFS("faculty_id", "=", $mode[1]);
                $res = $DB->Select();
                while($row = $DB->FetchAssoc($res))
                {
                    $DB->SetTable($this->db_prefix."teachers");
                    $DB->AddCondFS("department_id", "=", $row["id"]);
                    $DB->Delete();
                }
                $DB->SetTable($this->db_prefix."departments");
                $DB->AddCondFS("faculty_id", "=", $mode[1]);
                $DB->Delete();
                $DB->SetTable($this->db_prefix."faculties");
                $DB->AddCondFS("id", "=", $mode[1]);
                $DB->Delete();
                $mode[0]=NULL;
                $this->output["faculty_mode"]=NULL;
                CF::Redirect($Engine->engine_uri);
            }
            default:
            {
                if(isset($_POST["faculty_name"]))
                {
                    $DB->SetTable($this->db_prefix."faculties");
                    $DB->AddCondFS("name", "=", $_POST["faculty_name"]);
                    $res = $DB->Select();
                    if(!($row = $DB->FetchAssoc($res)))
                    {
                        $DB->SetTable($this->db_prefix."faculties");
                        $DB->AddValue("name", $_POST["faculty_name"]);
                        $DB->AddValue("pid", $_POST["pid"]);
						$DB->AddValue("pos", $_POST["pos"]);
						$DB->AddValue("link", $_POST["faculty_link"]);
						$DB->AddValue("short_name", $_POST["faculty_shortname"]);
						$DB->AddValue("foundation_year", $_POST["foundation_year"]);
						$DB->AddValue("comment", $_POST["comment"]);
						if (isset($_POST["is_active"]) && $_POST["is_active"])
							$DB->AddValue("is_active", 1);
						else
							$DB->AddValue("is_active", 0);
                        $DB->Insert();
                    }
                }

                $DB->SetTable($this->db_prefix."faculties");
                $DB->AddCondFS("pid", "=", 0);
				$DB->AddOrder("pos");
                $res =  $DB->Select();
                while($row = $DB->FetchAssoc($res))
                {
                    $DB->SetTable($this->db_prefix."faculties");
                    $DB->AddCondFS("pid", "=", $row["id"]);
                    $res2 =  $DB->Select();
                    while($row2 = $DB->FetchAssoc($res2))
                    {
                        $row["subfaculties"][]=$row2;
                    }
                    $this->output["timetable_faculties"][] = $row;
                }
                break;
            }
        }
    }

	function pulpit_list($faculty_id ) {
		global $DB, $Engine;
		$output = null;
		$DB->SetTable($this->db_prefix."departments");
        $DB->AddCondFS("faculty_id", "=", $faculty_id);
		$DB->AddCondFS("is_active", "=", 1);
        $res = $DB->Select();
		while($row = $DB->FetchAssoc($res)) {
            $output[] = array(
				"url" => $row["url"],
				"name" => $row["name"]
			);
		}
		$this->output["pulpit"] = $output;
		$DB->SetTable($this->db_prefix."faculties");
        $DB->AddCondFS("id", "=", $faculty_id);
        $res2 = $DB->Select();
		if($row2 = $DB->FetchAssoc($res2))
            $this->output["faculty_name"] = $row2["name"];
	}

    function timetable_departmens($mode)
    {
        global $DB, $Engine, $Auth;
		$mode = explode("/", $mode);
        if(isset($_POST["mode"])) {
            $mode[0] = $_POST["mode"];
            $mode[1] = $_POST["id"];
        }
        switch($mode[0])
        {
            case "edit":
            {
                if(!isset($_POST["id"]))
                {
                    $DB->SetTable($this->db_prefix."departments");
                    $DB->AddCondFS("id", "=", $mode[1]);
                    $res = $DB->Select();
                    if($row = $DB->FetchAssoc($res))
                    {
                        $DB->SetTable($this->db_prefix."faculties");
                        $DB->AddCondFS("id", "=", $row["faculty_id"]);
                        $res2 = $DB->Select();
                        if($row2 = $DB->FetchAssoc($res2))
                            $row["faculty_name"] = $row2["name"];
                        $DB->SetTable($this->db_prefix."faculties");
                        $res2 = $DB->Select();
                        while($row2 = $DB->FetchAssoc($res2))
                            $this->output["faculties"][] = $row2;
                        $this->output["edit_department"][]=$row;
                    }
                    $this->output["departments_mode"]="edit";
                }
                else
                {
                    $DB->SetTable($this->db_prefix."departments");
                    $DB->AddCondFS("id", "=", $_POST["id"]);
                    $DB->AddValue("name", $_POST["department_name"]);
                    $DB->AddValue("url", $_POST["department_url"]);
                    $DB->AddValue("faculty_id", $_POST["faculty"]);
                    $DB->AddValue("is_active", isset($_POST["is_active"]) ? (int)$_POST["is_active"] : 0);
                    $DB->Update();
                    $mode[0]=NULL;
                    $this->output["departments_mode"]=NULL;
                    CF::Redirect($Engine->engine_uri);
                }
                break;
            }
            case "delete":
            {
                $DB->SetTable($this->db_prefix."teachers");
                $DB->AddCondFS("department_id", "=", $mode[1]);
                $DB->Delete();
                $DB->SetTable($this->db_prefix."departments");
                $DB->AddCondFS("id", "=", $mode[1]);
                $DB->Delete();
                $mode[0]=NULL;
                $this->output["departments_mode"]=NULL;
                CF::Redirect($Engine->engine_uri);
                break;
            }
            default:
            {
                if(isset($_POST["department_name"]))
                {
                    $DB->SetTable($this->db_prefix."departments");
                    $DB->AddCondFS("name", "=", $_POST["department_name"]);
                    $DB->AddCondFS("faculty_id", "=", $_POST["faculty_id"]);
                    $res = $DB->Select();
                    if(!($row = $DB->FetchAssoc($res)))
                    {
                        $DB->SetTable($this->db_prefix."departments");
                        $DB->AddValue("name", $_POST["department_name"]);
                        $DB->AddValue("url", $_POST["department_url"]);
                        $DB->AddValue("faculty_id", $_POST["faculty_id"]);
                        $DB->AddValue("is_active", isset($_POST["is_active"]) ? (int)$_POST["is_active"] : 0);
                        $DB->Insert();
                    }
                }


                $DB->SetTable($this->db_prefix."departments");
                if (isset($mode[1]))
                	$DB->AddCondFS("id", "=", $mode[1]);
                $DB->AddOrder("faculty_id");
                $res =  $DB->Select();
                while($row = $DB->FetchAssoc($res))
                {
                    $DB->SetTable($this->db_prefix."faculties");
                    $DB->AddCondFS("id", "=", $row["faculty_id"]);
                    $res2 =  $DB->Select();
                    if($row2 = $DB->FetchAssoc($res2))
                    {
                        $row["faculty_name"] = $row2["name"];
                        $row["faculty_id"] = $row2["id"];
                    }

										if($Engine->OperationAllowed($this->module_id, "department.faculty.handle", $row["faculty_id"], $Auth->usergroup_id))
											$this->output["timetable_departments"][] = $row;
                }
                $DB->SetTable($this->db_prefix."faculties");
                $res = $DB->Select();
                while($row = $DB->FetchAssoc($res))
                {
									if($Engine->OperationAllowed($this->module_id, "department.faculty.handle", $row["id"], $Auth->usergroup_id))
                    $this->output["faculties"][] = $row;
                }
                break;
            }
        }
    }

  function ajax_delete_subj($subj_id)
	{
        global $DB, $Engine, $Auth;
		if(isset($_REQUEST['data']['subj_id']) && CF::IsIntNumeric($subj_id)) {
			$DB->SetTable("nsau_files_subj");
			$DB->AddExp("count(*)", "num_files");
			$DB->AddCondFS("subject_id", "=", $subj_id);
			//$DB->AddGrouping("subject_id");
			$res = $DB->Select(1);
			if($row = $DB->FetchObject()) {
				if($row->num_files > 0) {
					$this->output["ajax_delete_subj"] = 0;
					$this->output["error_msg"] = "�� ���������� ������� ���������";
					return ;
				}
			}
			$DB->SetTable("nsau_curriculum");
			$DB->AddExp("count(*)", "num_records");
			$DB->AddCondFS("subject_id", "=", $subj_id);
			//$DB->AddGrouping("subject_id");
			$res = $DB->Select(1);
			if($row = $DB->FetchObject()) {
				if($row->num_records > 0) {
					$this->output["ajax_delete_subj"] = 0;
					$this->output["error_msg"] = "���������� ������������ � ������� ������";
					return ;
				}
			}
			$DB->SetTable("izop_curriculum");
			$DB->AddExp("count(*)", "num_records");
			$DB->AddCondFS("subject_id", "=", $subj_id);
			$res = $DB->Select(1);
			if($row = $DB->FetchObject()) {
				if($row->num_records > 0) {
					$this->output["ajax_delete_subj"] = 0;
					$this->output["error_msg"] = "���������� ������������ � ������� ������ ����";
					return ;
				}
			}
			$DB->SetTable("nsau_timetable");
			$DB->AddCondFS("subject_id", "=", $subj_id);
			$DB->Delete();

			$DB->SetTable("nsau_subjects");
            $DB->AddCondFS("id", "=", $subj_id);
			if($DB->Delete())
				$this->output["ajax_delete_subj"] = 1;
       else {
				 $this->output["ajax_delete_subj"] = 0;
				 $this->output["error_msg"] = "������ �������� �� ����.";
			 }
		}
	}

    function timetable_subjects($mode)
    {
        global $DB, $Engine, $Auth;
        if(isset($_POST["mode"]))
            $mode[0] = $_POST["mode"];
        if($Engine->OperationAllowed($this->module_id, "subject.files.transfer", 0, $Auth->usergroup_id))
          $this->output["allow_transfer"] = true;
				else
					$this->output["allow_transfer"] = false;
        switch($mode[0])
        {
            case "edit":
            {
                if(!isset($_POST["id"]))
                {
										$DB->SetTable($this->db_prefix."subjects");
                    $res = $DB->Select();
										while($row = $DB->FetchAssoc($res))
											$this->output["all_subjects"][$row["department_id"]][] = $row;



                    $DB->SetTable($this->db_prefix."subjects");
                    $DB->AddCondFS("id", "=", $mode[1]);
                    $res = $DB->Select();
                    if($row = $DB->FetchAssoc($res))
                    {
                        $DB->SetTable($this->db_prefix."departments");
                        $DB->AddCondFS("id", "=", $row["department_id"]);
                        $res2 = $DB->Select();
                        if($row2 = $DB->FetchAssoc($res2))
                            $row["department_name"] = $row2["name"];
                        $DB->SetTable($this->db_prefix."departments", "d");
                        $DB->AddTable($this->db_prefix."faculties","f");
                        $DB->AddCondFF("d.faculty_id", "=", "f.id");
                        $DB->AddField("d.name","name");
                        $DB->AddField("d.id","id");
                        $DB->AddField("f.id","faculty_id");
                        $DB->AddField("f.name","faculty_name");
                        $DB->AddOrder("d.faculty_id");
                        $DB->AddOrder("d.name");
                        $res2 = $DB->Select();
                        while($row2 = $DB->FetchAssoc($res2))
													if ($Engine->OperationAllowed($this->module_id, "subjects.faculty.handle", $row2["faculty_id"], $Auth->usergroup_id)
														|| $Engine->OperationAllowed($this->module_id, "subjects.faculty.handle", -1, $Auth->usergroup_id)
														|| $Engine->OperationAllowed($this->module_id, "subjects.department.handle", $row2["id"], $Auth->usergroup_id))
														$this->output["departments"][] = $row2;
												$this->output["edit_subject"][] = $row;
												$DB->SetTable("nsau_subjects");
												$DB->AddCondFS("department_id", "=", $row["department_id"]);
												$res3 = $DB->Select();
												while($row3 = $DB->FetchAssoc($res3))
													$this->output["subjects"][] = $row3;
                    }
                    $this->output["subjects_mode"]="edit";
                }
                else
                {
					if(!empty($_POST["subject_name"]) && isset($_POST["department"])) {
						$DB->SetTable($this->db_prefix."subjects");
						$DB->AddCondFS("id", "=", $_POST["id"]);
						$DB->AddValue("name", $_POST["subject_name"]);
						$DB->AddValue("department_id", $_POST["department"]);
						if($DB->Update()) {
							$Engine->LogAction($this->module_id, "subjects_item", $_POST["id"], "edit");
						}
						$mode[0]=NULL;
						$this->output["subjects_mode"]=NULL;
						CF::Redirect($Engine->engine_uri);
					} else {
						$this->output["error"] = 313;
						$DB->SetTable($this->db_prefix."subjects");
						$DB->AddCondFS("id", "=", $mode[1]);
						$res = $DB->Select();
						if($row = $DB->FetchAssoc($res))
						{
							$DB->SetTable($this->db_prefix."departments");
							$DB->AddCondFS("id", "=", $row["department_id"]);
							$res2 = $DB->Select();
							if($row2 = $DB->FetchAssoc($res2))
								$row["department_name"] = $row2["name"];
							$DB->SetTable($this->db_prefix."departments", "d");
              $DB->AddTable($this->db_prefix."faculties","f");
              $DB->AddCondFF("d.faculty_id", "=", "f.id");
              $DB->AddField("f.name","faculty_name");
							$DB->AddOrder("name");
							$res2 = $DB->Select();
							while($row2 = $DB->FetchAssoc($res2))
								// if ($Engine->OperationAllowed($this->module_id, "subjects.faculty.handle", $row2["faculty_id"], $Auth->usergroup_id) || $Engine->OperationAllowed($this->module_id, "subjects.department.handle", $row2["id"], $Auth->usergroup_id))
									$this->output["departments"][] = $row2;
							$this->output["edit_subject"][]=$row;
						}
						$this->output["subjects_mode"]="edit";
					}
                }
                break;
            }
            case "delete":
            {
                $DB->SetTable($this->db_prefix."subjects");
                $DB->AddCondFS("id", "=", $mode[1]);
                if(!$DB->Delete()) {
					$this->output["display_variant"] = "del_item";
					$this->output["messages"]["bad"][] = 322;
					//echo "<script>alert('������ ��������. �� ���������� ������� ���������.');</script>";
				} else {
					$Engine->LogAction($this->module_id, "subjects_item", $mode[1], "del");
				}
                $mode[0]=NULL;
                $this->output["subjects_mode"]=NULL;
                CF::Redirect($Engine->engine_uri);
            }
            default:
            {
                if(isset($_POST["subject_name"]) && isset($_POST["department_id"])) {
					if($_POST["department_id"] != 0) {
						$DB->SetTable($this->db_prefix."subjects");
						$DB->AddCondFS("name", "=", $_POST["subject_name"]);
						$DB->AddCondFS("department_id", "=", $_POST["department_id"]);
						$res = $DB->Select();
						if(mb_strlen($_POST["subject_name"])<3) $this->output["error"] = 337;
						if(!($row = $DB->FetchAssoc($res)) && empty($this->output["error"])) {
						    $DB->SetTable($this->db_prefix."subjects");
						    $DB->AddValue("name", $_POST["subject_name"]);
						    $DB->AddValue("department_id", $_POST["department_id"]);
								$DB->AddValue("allow_move", $_POST["department_id"]);
						    $DB->Insert();
						    $Engine->LogAction($this->module_id, "item", $DB->LastInsertId(), "add");
						    CF::Redirect($Engine->engine_uri);
						}
					} else {
						$this->output["error"] = 313;
					}
                }

				$DB->SetTable($this->db_prefix."subjects");
                //$DB->AddOrder("department_id");
                $DB->AddOrder("name");
                $res = $DB->Select();
				$dep_id = 0;
				$subj_array = null;
				while($row = $DB->FetchAssoc($res)) {
					$subj_array[$row["department_id"]][] = array(
														"subj_name" => $row["name"],
														"subj_id" => $row["id"],
														"is_hidden" => $row["is_hidden"]
													);
				}

				$dep_array = null;
				$DB->SetTable($this->db_prefix."subjects", "s");
                $DB->AddTable($this->db_prefix."departments","d");
                $DB->AddCondFF("d.id","=","s.department_id");
				$DB->AddOrder("d.faculty_id");

                $DB->AddField("d.id","id");
                $DB->AddField("d.name","name");
                $DB->AddField("d.faculty_id","faculty_id");
								$DB->AddField("d.is_active","is_active");

				$res = $DB->Select(null, null, true, true, true);
				while($row = $DB->FetchAssoc($res)) {
					$dep_array[$row["faculty_id"]][] = array(
													"dep_name" => $row["name"],
													"dep_id" => $row["id"],
													"subj_list" => $subj_array[$row["id"]],
													"is_active" => $row["is_active"]
												);
				}

				$DB->SetTable($this->db_prefix."subjects", "s");
                $DB->AddTable($this->db_prefix."departments","d");
                $DB->AddTable($this->db_prefix."faculties","f");
                $DB->AddCondFF("d.id","=","s.department_id");
                $DB->AddCondFF("f.id","=","d.faculty_id");
				$DB->AddOrder("f.id");

                $DB->AddField("f.id","id");
                $DB->AddField("f.name","name");
				$res = $DB->Select(null, null, true, true, true);
				while($row = $DB->FetchAssoc($res)) {
					//echo $row["id"]."<br/>";


					if ($Engine->OperationAllowed($this->module_id, "subjects.faculty.handle", $row["id"], $Auth->usergroup_id))
					$this->output["fac_dep_subj"][] = array(
									"fac_name" => $row["name"],
									"fac_id" => $row["id"],
									"dep_list" => $dep_array[$row["id"]]
								);
					else {
						$dep_list = array();
						foreach ($dep_array[$row["id"]] as $dep)
							if ($Engine->OperationAllowed($this->module_id, "subjects.department.handle", $dep["dep_id"], $Auth->usergroup_id) ||
							( $Engine->OperationAllowed($this->module_id, "subjects.own.handle", -1, $Auth->usergroup_id)  && $this->if_user_from_department($dep["dep_id"]))) {
								$dep_list[] = $dep;
							}
						$this->output["fac_dep_subj"][] = array(
									"fac_name" => $row["name"],
									"fac_id" => $row["id"],
									"dep_list" => $dep_list
						);

					}

				}
				$DB->SetTable($this->db_prefix."subjects");
                $DB->AddCondFS("department_id","=", "0");
				$DB->AddOrder("name");
                $DB->AddField("id");
                $DB->AddField("name");
				$res = $DB->Select();
				while($row = $DB->FetchAssoc($res)) {
					$this->output["nodep_subj"][] = array(
												"subj_name" => $row["name"],
												"subj_id" => $row["id"]
											);
				}


                /*$DB->SetTable($this->db_prefix."subjects");
                $DB->AddOrder("department_id");
                $res = $DB->Select();
                while($row = $DB->FetchAssoc($res))
                {
                    $DB->SetTable($this->db_prefix."departments");
                    $DB->AddCondFS("id", "=", $row["department_id"]);
                    $DB->AddOrder("name");
                    $res2 = $DB->Select();
                    if($row2 = $DB->FetchAssoc($res2))
                        $row["department_name"] = $row2["name"];
                    $this->output["subjects"][] = $row;
                } */
                $DB->SetTable($this->db_prefix."departments","d");
                $DB->AddTable($this->db_prefix."faculties","f");
                $DB->AddCondFS("d.is_active","=", 1);
                $DB->AddCondFF("f.id","=","d.faculty_id");
                $DB->AddField("d.id","id");
                $DB->AddField("d.name","name");
                $DB->AddField("d.faculty_id","fac_id");
                $DB->AddField("f.id","faculty_id");
                $DB->AddField("f.name","faculty_name");
				$DB->AddOrder("f.id");
                $res = $DB->Select();
                while($row = $DB->FetchAssoc($res))
                {
					if ($Engine->OperationAllowed($this->module_id, "subjects.faculty.handle", $row["fac_id"], $Auth->usergroup_id) || $Engine->OperationAllowed($this->module_id, "subjects.department.handle", $row["id"], $Auth->usergroup_id)
					|| ( $Engine->OperationAllowed($this->module_id, "subjects.own.handle", -1, $Auth->usergroup_id) && $this->if_user_from_department($row["id"])) || $Auth->usergroup_id==1)
						$this->output["departments"][] = $row;
                }

                break;
            }
        }
    }


    function if_user_from_department($dep_id)
    {
    	global $Engine,$DB,$Auth;
    	if ($user_id = $Auth->user_id)
    	{
    		$DB->SetTable($this->db_prefix."people", "p");
    		$DB->AddTable($this->db_prefix."teachers", "t");
    		$DB->AddCondFF("p.id","=","t.people_id");
    		$DB->AddCondFS("p.user_id","=",$user_id);
    		$DB->AddField("t.department_id","dep_id");
    		if ($res = $DB->Select())
    		{
    			$row = $DB->FetchAssoc($res);
    			return ($row["dep_id"]==$dep_id);
    		}
    	}
    	return false;
    }

    function timetable_auditorium($mode)
    {
        global $DB, $Engine, $Auth;
        if(isset($_POST["mode"])) {
            $mode[0] = $_POST["mode"];
		}
		$this->output['plugins'][] = 'jquery.editable-select';

        switch($mode[0]) {
            case "edit": {
                if(!isset($_POST["id"])) {
                    $DB->SetTable($this->db_prefix."auditorium");
                    $DB->AddCondFS("id", "=", $mode[1]);
                    $res = $DB->Select();
                    if($row = $DB->FetchAssoc($res)) {
                        $DB->SetTable($this->db_prefix."buildings");
                        $DB->AddCondFS("id", "=", $row["building_id"]);
                        $res2 = $DB->Select();
                        if($row2 = $DB->FetchAssoc($res2)) {
                            $row["building_name"] = $row2["name"];
                            $row["building_label"] = $row2["label"];
                        }
                        $DB->SetTable($this->db_prefix."buildings");
                        $DB->AddOrder("name");
                        $res2 = $DB->Select();
                        while($row2 = $DB->FetchAssoc($res2)) {
                            $this->output["buildings"][] = $row2;
                        }
                        $DB->SetTable($this->db_prefix."departments");
                        $DB->AddOrder("name");
                        $res2 = $DB->Select();
                        while($row2 = $DB->FetchAssoc($res2)) {
                            $this->output["departments"][] = $row2;
                        }
                        $this->output["edit_auditorium"][]=$row;
                    }
                    $this->output["auditorium_mode"]="edit";
                } else {
                    $DB->SetTable($this->db_prefix."auditorium");
                    $DB->AddCondFS("id", "=", $_POST["id"]);
                    $DB->AddValue("is_using",$_POST["is_using"]);
                    $DB->AddValue("name", $_POST["auditorium_name"]);
                    $DB->AddValue("building_id", $_POST["building"]);
                    $DB->AddValue("roominess",$_POST["roominess"]);
                    if(isset($_POST["department_id"])) {
                        $DB->AddValue("department_id",$_POST["department_id"]);
                    }
                    if(isset($_POST["type"])) {
                        $DB->AddValue("type",$_POST["type"]);
                    } else {
                        $DB->AddValue("type",0);
                    }
                    if(isset($_POST["multimedia"])) {
                    	$DB->AddValue("multimedia", $_POST["multimedia"]);
                    }
                    else {
                    	$DB->AddValue("multimedia", 0);
                    }
                    if(!empty($_POST["for_request"])) {
                    	$DB->AddValue("for_request", 1);
                    }
                    else {
                    	$DB->AddValue("for_request", 0);
                    }
                    $DB->Update();
                    $mode[0]=NULL;
                    $this->output["auditorium_mode"]=NULL;
                    CF::Redirect($Engine->engine_uri);
                }
            }
            break;

            case "delete": {
                $DB->SetTable($this->db_prefix."auditorium");
                $DB->AddCondFS("id", "=", $mode[1]);
                $DB->Delete();
                $mode[0]=NULL;
                $this->output["auditorium_mode"]=NULL;
                CF::Redirect($Engine->engine_uri);
            }

            default: {
                if(isset($_POST["auditorium_name"])) {
                    $DB->SetTable($this->db_prefix."auditorium");
                    $DB->AddCondFS("name", "=", $_POST["auditorium_name"]);
                    $DB->AddCondFS("building_id", "=", $_POST["building_id"]);
                    $res = $DB->Select();
                    if(!($row = $DB->FetchAssoc($res))) {
                        $DB->SetTable($this->db_prefix."auditorium");
                        $DB->AddValue("is_using",$_POST["is_using"]);
                        $DB->AddValue("name", $_POST["auditorium_name"]);
                        $DB->AddValue("building_id", $_POST["building_id"]);
                        $DB->AddValue("roominess",$_POST["roominess"]);
                        if(isset($_POST["department_id"])) {
                            $DB->AddValue("department_id",$_POST["department_id"]);
                        }
                        if(isset($_POST["type"])) {
                            $DB->AddValue("type",$_POST["type"]);
                        } else {
                            $DB->AddValue("type",0);
                        }
                        if(isset($_POST["multimedia"])) {
                        	$DB->AddValue("multimedia", $_POST["multimedia"]);
                        }
                        else {
                        	$DB->AddValue("multimedia", 0);
                        }
												if(!empty($_POST["for_request"])) {
													$DB->AddValue("for_request", 1);
												}
												else {
													$DB->AddValue("for_request", 0);
												}
                        $DB->Insert();
                        CF::Redirect($Engine->engine_uri);
                    }
                }

				$DB->SetTable($this->db_prefix."buildings");
                $DB->AddOrder("id");
                $res_build = $DB->Select();
				while($row_build = $DB->FetchAssoc($res_build)) {
					$this->output["auditorium"][$row_build['id']]['bild_name'] = $row_build['name'];
					$this->output["auditorium"][$row_build['id']]['bild_label'] = $row_build['label'];
					$DB->SetTable($this->db_prefix."auditorium");
                    $DB->AddCondFS("building_id", "=", $row_build["id"]);
					$DB->AddOrder("name");
					$res_audit = $DB->Select();
					while($row_audit = $DB->FetchAssoc($res_audit)) {
						$this->output["auditorium"][$row_build['id']]['bild_audit'][$row_audit['id']] = $row_audit;
					}
				}
                /*$DB->SetTable($this->db_prefix."auditorium");
                $DB->AddOrder("building_id");
                $DB->AddOrder("name");
                $res = $DB->Select();
                while($row = $DB->FetchAssoc($res)) {
                    $DB->SetTable($this->db_prefix."buildings");
                    $DB->AddCondFS("id", "=", $row["building_id"]);
                    $res2 = $DB->Select();
                    if($row2 = $DB->FetchAssoc($res2)) {
                        $row["building_name"] = $row2["name"];
                        $row["label"] = $row2["label"];
                    }
                    $this->output["auditorium"][] = $row;
                } */
                $DB->SetTable($this->db_prefix."buildings");
                $DB->AddOrder("name");
                $res = $DB->Select();
                while($row = $DB->FetchAssoc($res)) {
                    $this->output["buildings"][] = $row;
                }
                $DB->SetTable($this->db_prefix."departments");
                $DB->AddOrder("name");
                $res = $DB->Select();
                while($row = $DB->FetchAssoc($res)) {
					$this->output["departments"][] = $row;
                }
            }
            break;
        }
    }

    function timetable_teachers($mode, $dep_id)
    {
        global $DB, $Engine, $Auth;
        if(isset($_POST["mode"]))
            $mode[0] = $_POST["mode"];

        switch($mode[0]) {
            case "edit": {
						break;
            }
            case "delete":
            {
                $DB->SetTable($this->db_prefix."teachers");
                $DB->AddCondFS("people_id", "=", $mode[1]);
                $DB->Delete();

				$Engine->LogAction($this->module_id, "teacher", $mode[1], "delete");
                $mode[0]=NULL;
                $this->output["teachers_mode"]=NULL;
                CF::Redirect($Engine->engine_uri);
                break;
            }
            case "search_teacher":
            {
                //���� ����������� ������� �������, �� ��������� �������������� �� ������� �������
                //CF::Debug($_POST);
                if(isset($_POST["department_id"]))
                {
                    foreach($_POST["teachers_id"] as $teacher)
                    {
                        if(isset($teacher["check"]) && $teacher["check"] == "on")
                        {
                            $DB->SetTable($this->db_prefix."teachers");
                            $DB->AddValue("people_id",$teacher["id"]);
                            $DB->AddValue("department_id",$_POST["department_id"]);
                            if ($DB->Insert())
                            	$Engine->LogAction($this->module_id, "teacher", $teacher["id"], "add_teacher");
                        }
                    }
                    CF::Redirect($Engine->engine_uri);
                }

                //�� ���� �� ����-�� ����
                if(isset($_POST["search_teacher"]))
                {
                    //�������� �������� ���
                    $parts = explode(" ", trim($_POST["search_teacher"]));

                    //�������� ������� ������� (� ����������� �� �������������� ������������ � �����-�� �������)
                    //���� ��������, ��� �������, ���� ���������� �������� �� ������ �� ��� �������, ��� �� ���. !!!!!!!!!!!!!!!!!!!!!!
                    $DB->SetTable($this->db_prefix."people","p");
                    $DB->AddTable($this->db_prefix."teachers","t");
                    $DB->AddTable("auth_users","a");
                    $DB->AddCondFS("a.id","=",$Auth->user_id);
                    $DB->AddCondFF("a.id","=","p.user_id");
                    $DB->AddCondFS("a.usergroup_id","=",6);//�������, ����� ��� ��� ������ ����������
                    $DB->AddCondFF("t.people_id","=","p.id");
                    $res2 = $DB->Select();
                    if($row2 = $DB->FetchAssoc($res2))
                    {
                        //���� ������������ - ����������, ������� ������� ��� �������
                        $this->output["current_department"]=$row2["department_id"];
                        //���� ���������� ��������������
                        $DB->SetTable($this->db_prefix."people");
                        //$DB->AddTable($this->db_prefix."teachers","t");
                        foreach($parts as $part)
                        {
                            $DB->AddAltFS("name", "LIKE", "%".$part."%");
                            $DB->AddAltFS("last_name", "LIKE", "%".$part."%");
                            $DB->AddAltFS("patronymic", "LIKE", "%".$part."%");
                            $DB->AppendAlts();
                            $DB->AddCondFS("status_id","=",2);
                        }
                        $DB->AddOrder("last_name");
                        $res = $DB->Select();
                        $this->output["search_teachers"] = array();
                        while($row = $DB->FetchAssoc($res))
                        {
                            $DB->SetTable($this->db_prefix."teachers");
                            $DB->AddCondFS("people_id","=",$row["id"]);
                            $DB->AddCondFS("department_id","=",$row2["department_id"]);
                            $res3 = $DB->Select();
                            if($row3 = $DB->FetchAssoc($res3))
                            {
                                $row["in_current_department"] = 1;
                            }
                            else
                            {
                                $row["in_current_department"] = 0;
                            }
                            $this->output["search_teachers"][] = $row;
                        }
                        $this->output["auth_usergroup"] = $Auth->usergroup_id;
                        //CF::Debug($this->output["auth_usergroup"]);
                    }
                    else
                    {
                        $DB->SetTable($this->db_prefix."people");
                        foreach($parts as $part)
                        {
                            $DB->AddAltFS("name", "LIKE", "%".$part."%");
                            $DB->AddAltFS("last_name", "LIKE", "%".$part."%");
                            $DB->AddAltFS("patronymic", "LIKE", "%".$part."%");
                            $DB->AppendAlts();
                            $DB->AddCondFS("status_id","=",2);
                        }
                        $DB->AddOrder("last_name");
                        $res = $DB->Select();
                        $this->output["search_teachers"] = array();
                        while($row = $DB->FetchAssoc($res))
                        {
                            $this->output["search_teachers"][] = $row;
                        }
                    }
                    $this->output["teachers_mode"]="search_teacher";
                    $this->output["uri"]=$Engine->engine_uri;
                }
                break;
            }
            default:
            {

				if($Engine->OperationAllowed($this->module_id, "teachers.all.handle", -1, $Auth->usergroup_id))
				{
					$DB->SetTable($this->db_prefix."teachers", "t");
					$DB->AddTable($this->db_prefix."people","p");
					$DB->AddTable("auth_users","a");
          			$DB->AddCondFF("a.id","=", "p.user_id");
					$DB->AddField("a.is_active","is_active");


					$DB->AddField("t.department_id","department_id");
					$DB->AddField("p.name","name");
					$DB->AddField("p.last_name","last_name");
					$DB->AddField("p.patronymic","patronymic");
					$DB->AddField("p.exp_teach","exp_teach");
					$DB->AddField("p.exp_full","exp_full");
					$DB->AddField("p.prad","prad");
					$DB->AddField("t.post","post");
					$DB->AddField("p.male","male");
					$DB->AddField("p.user_id","user_id");
					$DB->AddField("p.id","id");
					$DB->AddField("p.status_id","status");
					$DB->AddCondFF("p.id","=", "t.people_id");
          $DB->AddOrder("p.last_name");



					$res =  $DB->Select();
					while($row = $DB->FetchAssoc($res)) {
						$did = explode(";", $row["department_id"]);
						foreach($did as $d_id) {
							if(!empty($d_id)) {
								$DB->SetTable("nsau_departments", "d");
								$DB->AddTable($this->db_prefix."faculties","f");
								$DB->AddCondFS("d.id", "=", $d_id);
								$DB->AddCondFF("d.faculty_id","=", "f.id");
								$DB->AddField("f.name","fac_name");
								$DB->AddField("d.id","d_id");
								$DB->AddField("d.name","d_name");
								$DB->AddField("f.id","fac_id");
								$DB->AddField("d.is_active","is_active");
								$row2 = $DB->FetchAssoc($DB->Select(1));

								$this->output["timetable_teachers"][$row2["d_id"]]["dep_name"] = $row2["d_name"];
								$this->output["timetable_teachers"][$row2["d_id"]]["is_active"] = $row2["is_active"];
								$this->output["timetable_teachers"][$row2["d_id"]]["teachers"][] = $row;
								$this->output["timetable_teachers2"][$row2["fac_id"]]["fac_name"] = $row["fac_name"];
								$this->output["timetable_teachers2"][$row2["fac_id"]]["departments"][$row2["d_id"]]["dep_name"] = $row2["d_name"];
								$this->output["timetable_teachers2"][$row2["fac_id"]]["departments"][$row2["d_id"]]["is_active"] = $row2["is_active"];
								$this->output["timetable_teachers2"][$row2["fac_id"]]["departments"][$row2["d_id"]]["teachers"][] = $row;
								$this->output["timetable_teachers2"][$row2["fac_id"]]["fac_name"] = $row2["fac_name"];
								$this->output["allow_handle"][$row2["d_id"]] = 1;
							}
						}
					}
					$this->output["current_department_id"] = 0;
				}
				else
				{
					if($Engine->OperationAllowed($this->module_id, "teachers.own.handle", -1, $Auth->usergroup_id))
					{

						$DB->SetTable("auth_users","a");
						$DB->SetTable($this->db_prefix."people","p");
						$DB->AddTable($this->db_prefix."teachers","t");
						$DB->AddTable($this->db_prefix."departments","d");
						$DB->AddCondFS("p.user_id","=",$Auth->user_id);
						$DB->AddCondFF("t.people_id","=","p.id");
						$DB->AddCondFF("t.department_id","=","d.id");
						$DB->AddField("d.name","department_name");
						$DB->AddField("t.department_id","department_id");
						$res = $DB->Select();
						if($row3 = $DB->FetchAssoc($res))
						{
					$DB->SetTable($this->db_prefix."teachers", "t");
					$DB->AddTable($this->db_prefix."people","p");
					$DB->AddField("t.department_id","department_id");
					$DB->AddField("p.name","name");
					$DB->AddField("p.last_name","last_name");
					$DB->AddField("p.patronymic","patronymic");
					$DB->AddField("p.exp_teach","exp_teach");
					$DB->AddField("p.exp_full","exp_full");
					$DB->AddField("p.prad","prad");
					$DB->AddField("t.post","post");
					$DB->AddField("p.male","male");
					$DB->AddField("p.id","id");

					$DB->AddField("p.user_id", "user_id");
					$DB->AddField("p.status_id","status");
					$DB->AddCondFF("p.id","=", "t.people_id");
          $DB->AddOrder("p.last_name");
					$res =  $DB->Select();
					while($row = $DB->FetchAssoc($res)) {
						$did = explode(";", $row["department_id"]);
						foreach($did as $d_id) {
							if(!empty($d_id)) {
								$DB->SetTable("nsau_departments", "d");
								$DB->AddTable($this->db_prefix."faculties","f");
								$DB->AddCondFS("d.id", "=", $d_id);
								$DB->AddCondFF("d.faculty_id","=", "f.id");
								$DB->AddField("f.name","fac_name");
								$DB->AddField("d.id","d_id");
								$DB->AddField("d.name","d_name");
								$DB->AddField("f.id","fac_id");
								$DB->AddField("d.is_active","is_active");
								$row2 = $DB->FetchAssoc($DB->Select(1));

								$this->output["timetable_teachers"][$row2["d_id"]]["dep_name"] = $row2["d_name"];
								$this->output["timetable_teachers"][$row2["d_id"]]["is_active"] = $row2["is_active"];
								$this->output["timetable_teachers"][$row2["d_id"]]["teachers"][] = $row;
								$this->output["timetable_teachers2"][$row2["fac_id"]]["fac_name"] = $row["fac_name"];
								$this->output["timetable_teachers2"][$row2["fac_id"]]["departments"][$row2["d_id"]]["dep_name"] = $row2["d_name"];
								$this->output["timetable_teachers2"][$row2["fac_id"]]["departments"][$row2["d_id"]]["is_active"] = $row2["is_active"];
								$this->output["timetable_teachers2"][$row2["fac_id"]]["departments"][$row2["d_id"]]["teachers"][] = $row;
								$this->output["timetable_teachers2"][$row2["fac_id"]]["fac_name"] = $row2["fac_name"];
								if($d_id==$row3["department_id"])
									$this->output["allow_handle"][$d_id] = 1;
								else
									$this->output["allow_handle"][$d_id] = 0;
							}
						}



					}
					$this->output["current_department_id"] = 0;

							$this->output["current_department_id"] = $row2["d_id"];
							$this->output["current_department_name"] = $row2["d_name"];
						}
					}
					else {
					$DB->SetTable($this->db_prefix."teachers", "t");
					$DB->AddTable($this->db_prefix."people","p");
					$DB->AddField("t.department_id","department_id");
					$DB->AddField("p.name","name");
					$DB->AddField("p.last_name","last_name");
					$DB->AddField("p.patronymic","patronymic");
					$DB->AddField("p.exp_teach","exp_teach");
					$DB->AddField("p.exp_full","exp_full");
					$DB->AddField("p.prad","prad");
					$DB->AddField("t.post","post");
					$DB->AddField("p.male","male");
					$DB->AddField("p.user_id","user_id");
					$DB->AddField("p.id","id");
					$DB->AddField("p.status_id","status");
					$DB->AddCondFF("p.id","=", "t.people_id");
          $DB->AddOrder("p.last_name");
					$res =  $DB->Select();
					while($row = $DB->FetchAssoc($res)) {
						$did = explode(";", $row["department_id"]);
						foreach($did as $d_id) {
							if(!empty($d_id)) {
								$DB->SetTable("nsau_departments", "d");
								$DB->AddTable($this->db_prefix."faculties","f");
								$DB->AddCondFS("d.id", "=", $d_id);
								$DB->AddCondFF("d.faculty_id","=", "f.id");
								$DB->AddField("f.name","fac_name");
								$DB->AddField("d.id","d_id");
								$DB->AddField("d.name","d_name");
								$DB->AddField("f.id","fac_id");
								$DB->AddField("d.is_active","is_active");
								$row2 = $DB->FetchAssoc($DB->Select(1));

								$this->output["timetable_teachers"][$row2["d_id"]]["dep_name"] = $row2["d_name"];
								$this->output["timetable_teachers"][$row2["d_id"]]["is_active"] = $row2["is_active"];
								$this->output["timetable_teachers"][$row2["d_id"]]["teachers"][] = $row;
								$this->output["timetable_teachers2"][$row2["fac_id"]]["fac_name"] = $row["fac_name"];
								$this->output["timetable_teachers2"][$row2["fac_id"]]["departments"][$row2["d_id"]]["dep_name"] = $row2["d_name"];
								$this->output["timetable_teachers2"][$row2["fac_id"]]["departments"][$row2["d_id"]]["is_active"] = $row2["is_active"];
								$this->output["timetable_teachers2"][$row2["fac_id"]]["departments"][$row2["d_id"]]["teachers"][] = $row;
								$this->output["timetable_teachers2"][$row2["fac_id"]]["fac_name"] = $row2["fac_name"];
								if ($Engine->OperationAllowed($this->module_id, "teachers.faculty.handle", $row2["fac_id"], $Auth->usergroup_id)) {
									$this->output["allow_handle"][$row2["d_id"]] = 1;
								}
							}
						}



					}
					$this->output["current_department_id"] = 0;

					}
				}
				$DB->SetTable($this->db_prefix."faculties");
                $DB->AddOrder("name");
                $res = $DB->Select();
                while($row = $DB->FetchAssoc($res)) {
					$DB->SetTable($this->db_prefix."departments");
                    $DB->AddCondFS("faculty_id", "=", $row["id"]);
					$DB->AddOrder("name");
                    $res2 =  $DB->Select();
                    while($row2 = $DB->FetchAssoc($res2)) {
						if ($Engine->OperationAllowed($this->module_id, "teachers.faculty.handle", $row["id"], $Auth->usergroup_id) || $Engine->OperationAllowed($this->module_id, "teachers.own.handle", $row2["id"], $Auth->usergroup_id)) {
							$this->output["faculties"][$row["id"]]["fac_name"] = $row["name"];
							$this->output["faculties"][$row["id"]]["departments"][] = $row2;
							// $this->output["allow_handle"][$row2["id"]] = 1;
						}
                    }
                }

				break;
            }
        }
    }

	function select_teachers($dep_id) {
		global $DB;

		$DB->SetTable($this->db_prefix."teachers", "t");
		$DB->AddTable($this->db_prefix."people","p");
		$DB->AddTable($this->db_prefix."departments","d");
		$DB->AddField("d.name","department_name");
		$DB->AddField("d.id","department_id");
		$DB->AddField("d.faculty_id","faculty_id");
		$DB->AddField("p.name","name");
		$DB->AddField("p.last_name","last_name");
		$DB->AddField("p.patronymic","patronymic");
		$DB->AddField("p.comment","comment");
		$DB->AddField("p.exp_teach","exp_teach");
		$DB->AddField("p.exp_full","exp_full");
		$DB->AddField("p.prad","prad");
		$DB->AddField("p.male","male");
		$DB->AddField("p.comment","comment");
		$DB->AddField("p.id","id");
		$DB->AddField("p.user_id","user_id");
		$DB->AddField("p.status_id","st_id");
		$DB->AddField("t.post","post");
		$DB->AddCondFF("p.id","=", "t.people_id");
		// $DB->AddCondFF("d.id","=","t.department_id");
		// $DB->AddCondFS("d.id","=",$dep_id);
		$DB->AddOrder("t.post");
		$DB->AddField("t.degree","degree");
		$DB->AddField("t.academ_stat","academ_stat");
		// $DB->AddField("t.post_itemprop","post_itemprop");
		$DB->AddCondFF("p.id","=", "t.people_id");
		$DB->AddCondFS("t.department_id", "REGEXP", "(^".$dep_id."$|^".$dep_id.";|;".$dep_id."$)");
		$DB->AddCondFS("d.id","=",$dep_id);
		$DB->AddOrder("t.post");
		$DB->AddOrder("p.last_name");
		$DB->AddOrder("p.name");
		$DB->AddOrder("p.patronymic");
		// die($DB->SelectQuery());
		$res = $DB->Select();


		$DB->SetTable("nsau_teachers_post");
		$res_post = $DB->Select();
		while($row_post = $DB->FetchAssoc($res_post)) {
			$posts[$row_post["id"]] = $row_post;
		}
		$DB->SetTable("nsau_teachers_degree");
		$res_post = $DB->Select();
		while($row_post = $DB->FetchAssoc($res_post)) {
			$degree[$row_post["id"]] = $row_post;
		}
		$DB->SetTable("nsau_teachers_rank");
		$res_post = $DB->Select();
		while($row_post = $DB->FetchAssoc($res_post)) {
			$rank[$row_post["id"]] = $row_post;
		}


		while ($row = $DB->FetchAssoc($res)) 
		{
			$DB->SetTable('auth_users');
			$DB->AddCondFS('id', '=', $row['user_id']);
			$in_auth = $DB->FetchAssoc($DB->Select(1));
			if (empty($row['user_id']) || ($in_auth['is_active']==1)) 
			{
				$DB->SetTable('nsau_teachers_department_posts');
				$DB->AddCondFS('department_id', '=', $dep_id);
				$DB->AddCondFS('people_id', '=', $row['id']);
				$r = $DB->FetchAssoc($DB->Select(1));



				$dgr = array();
				$row["post_itemprop"] = $posts[$r['post_id']]["name"];
				$row["pos"] = $posts[$r['post_id']]["pos"];
				$deg = explode(";", $row["degree"]);
				foreach($deg as $i => $d_id) {
					if($i==0)
						$dgr[] = $degree[$d_id]["name"];
					else
						$dgr[] = CF::LowCaseOne($degree[$d_id]["name"]);
				}
				$row["degree"] = implode(", ", $dgr);
				$dgr = array();
				$deg = explode(";", $row["academ_stat"]);
				foreach($deg as $i => $d_id) {
					if($i==0)
						$dgr[] = $rank[$d_id]["name"];
					else
						$dgr[] = CF::LowCaseOne($rank[$d_id]["name"]);
				}
				$row["academ_stat"] = implode(", ", $dgr);
				$this->output["select_teachers"][] = $row;
			}
		}
	}

	function dep_subjects($dep_id, $module_uri, $parts = null) {
		global $DB;
		$this->output["scripts_mode"] = $this->output["mode"] = "dep_subjects";


		$DB->SetTable($this->db_prefix."subjects");
		$DB->AddCondFS("department_id", "=", $dep_id);
		//$DB->AddCondFS("is_hidden", "!=", 1);
		$res_subj = $DB->Select();
		while($row_subj = $DB->FetchAssoc($res_subj)) {
			if($row_subj["is_hidden"]==1) continue;
			$this->output["subjects_file"][$row_subj['id']]['name'] = $row_subj['name'];
			$DB->SetTable($this->db_prefix."files_subj", "fs");
      $DB->AddTable($this->db_prefix."file_view", "fv");
			$DB->AddCondFS("fs.subject_id", "=", $row_subj['id']);
			$DB->AddCondFS("fs.approved", "=", 1);
      $DB->AddCondFF("fs.view_id", "=", "fv.id");
			if(isset($parts[2]) && !empty($parts[2]))
				$DB->AddCondFS("fs.education", "=", $parts[2]);

      //$DB->AddOrder("fs.view_id");
      $DB->AddOrder("fv.pos");
      //$DB->AddOrder("fs.view_id");
      //echo $DB->SelectQuery();

			$res_file_subj = $DB->Select();
			while($row_file_subj = $DB->FetchAssoc($res_file_subj)) {
				$DB->SetTable($this->db_prefix."files");
				$DB->AddCondFS("id", "=", $row_file_subj['file_id']);
				$DB->AddCondFS("deleted", "=", 0);
				$res_file = $DB->Select();
				if($row_file = $DB->FetchAssoc($res_file)) {
					$DB->SetTable("nsau_people");
					$DB->AddCondFS("user_id", "=", $row_file['user_id']);
					$p_res = $DB->Select(1);
					$p_row = $DB->FetchAssoc($p_res);
					$this->output["subjects_file"][$row_subj['id']]['files'][$row_file_subj['file_id']]['name'] = (empty($row_file['descr']) ? $row_file['name'] : $row_file['descr']);
					$this->output["subjects_file"][$row_subj['id']]['files'][$row_file_subj['file_id']]['people_id'] = $p_row['id'];
					$this->output["subjects_file"][$row_subj['id']]['files'][$row_file_subj['file_id']]['people_name'] = $p_row["last_name"]." ".$p_row["name"]." ".$p_row["patronymic"];
					$this->output["subjects_file"][$row_subj['id']]['files'][$row_file_subj['file_id']]['sig_id'] = $row_file['sig_id'];
					$this->output["subjects_file"][$row_subj['id']]['files'][$row_file_subj['file_id']]['with_sig'] = $row_file['with_sig'];
					if(!empty($row_file['descr'])) {
						$this->output["subjects_file"][$row_subj['id']]['files'][$row_file_subj['file_id']]['descr'] = $row_file['descr'];
					}
					if(!is_null($row_file_subj['view_id'])) {
						$DB->SetTable($this->db_prefix."file_view");
						$DB->AddCondFS("id", "=", $row_file_subj['view_id']);
						$DB->AddField("view_name");
						$res_file_view = $DB->Select();
						if($row_file_view = $DB->FetchAssoc($res_file_view)) {
							$this->output["subjects_file"][$row_subj['id']]['files'][$row_file_subj['file_id']]['view'] = $row_file_view['view_name'];
						}
					}
					if(!is_null($row_file_subj['education'])) {
						$this->output["subjects_file"][$row_subj['id']]['files'][$row_file_subj['file_id']]['education'] = $row_file_subj['education'];
					}
					if(!is_null($row_file_subj['spec_id'])) {
						$spec_type = array(
										'0' => array('51'=>'secondary','68'=>'magistracy','62'=>'bachelor','65'=>'higher'),
										'1' => array('secondary'=>'51','magistracy'=>'68','bachelor'=>'62','higher'=>'65')
									);
						$DB->SetTable($this->db_prefix."specialities", 'sp');
						if(!is_null($row_file_subj['spec_type'])) {
							$DB->AddTable($this->db_prefix."spec_type", 'st');
							$DB->AddCondFF("sp.id","=", "st.spec_id");
							$DB->AddCondFS("st.type", "=", $spec_type[0][$row_file_subj['spec_type']]);
							$DB->AddField("st.type", "type");
						}
						$DB->AddCondFS("sp.id", "=", $row_file_subj['spec_id']);
						$DB->AddField("sp.id", "id");
						$DB->AddField("sp.code", "code");
						$DB->AddField("sp.name", "name");
						$res_spec = $DB->Select();
						if($row_spec = $DB->FetchAssoc($res_spec)) {

							$this->output["subjects_file"][$row_subj['id']]['files'][$row_file_subj['file_id']]['spec_code'] = $row_spec['code'].((strpos($row_spec['code'], ".") === false) ? ".".$spec_type[1][$row_spec['type']] : "");
							$this->output["subjects_file"][$row_subj['id']]['files'][$row_file_subj['file_id']]['spec_name'] = $row_spec['name'];

							$this->output["spec_file"][$row_spec['id']]['name'] = $row_spec['name'];
							$this->output["spec_file"][$row_spec['id']]['code'] = $row_spec['code'];
							$this->output["spec_file"][$row_spec['id']]['files'][$row_file_subj['file_id']]['name'] = (empty($row_file['descr']) ? $row_file['name'] : $row_file['descr']);
							$this->output["spec_file"][$row_spec['id']]['files'][$row_file_subj['file_id']]['subj'] = $row_subj['name'];
							if(!empty($row_file['descr'])) {
								$this->output["spec_file"][$row_spec['id']]['files'][$row_file_subj['file_id']]['descr'] = $row_file['descr'];
							}
							if(isset($this->output["subjects_file"][$row_subj['id']]['files'][$row_file_subj['file_id']]['view'])) {
								$this->output["spec_file"][$row_spec['id']]['files'][$row_file_subj['file_id']]['view'] = $this->output["subjects_file"][$row_subj['id']]['files'][$row_file_subj['file_id']]['view'];
							}
							if(isset($this->output["subjects_file"][$row_subj['id']]['files'][$row_file_subj['file_id']]['education'])) {
								$this->output["spec_file"][$row_spec['id']]['files'][$row_file_subj['file_id']]['education'] = $this->output["subjects_file"][$row_subj['id']]['files'][$row_file_subj['file_id']]['education'];
							}
							$this->output["spec_file"][$row_spec['id']]['files'][$row_file_subj['file_id']]['sig_id'] = $this->output["subjects_file"][$row_subj['id']]['files'][$row_file_subj['file_id']]['sig_id'];
							$this->output["spec_file"][$row_spec['id']]['files'][$row_file_subj['file_id']]['with_sig'] = $this->output["subjects_file"][$row_subj['id']]['files'][$row_file_subj['file_id']]['with_sig'];
						}
					}
				}
			}
		}
		/*echo "<pre>";
		print_r($this->output["spec_file"]);
		echo "</pre>";*/

		/*$DB->SetTable($this->db_prefix."subjects");
		$DB->AddCondFS("department_id", "=", $dep_id);
		$res = $DB->Select();

		$files_by_subj = array();
		$DB->SetTable($this->db_prefix."files_subj");

		$DB->AddGrouping("subject_id");
		if ($res2 = $DB->Select()) {
			while($row = $DB->FetchAssoc($res2))
				$files_by_subj[$row['subject_id']] = $row;
		} ;


		while ($row = $DB->FetchAssoc($res)) {
			$row['has_files_attached'] = isset($files_by_subj[$row['id']]);
			$subjects[] = $row;
		}

		$specs = array();
		$res = $DB->Exec('select distinct sp.id,sp.code, sp.name from nsau_specialities as sp left join nsau_files_subj as fss on fss.spec_id = sp.id left join nsau_subjects as s on s.id = fss.subject_id where s.department_id = '.$dep_id);
		while ($row = $DB->FetchAssoc($res)) {
			$specs[] = $row;
		}

		if (count($module_uri)==1 && !$module_uri[0]) {
			$this->output["subjects"] = $subjects;
			$this->output["specs"] = $specs;
			$this->output["scripts_mode"] = $this->output["mode"] = "dep_subjects";
		}
		elseif ($module_uri[0]) {
			$DB->SetTable($this->db_prefix."file_view");
			$DB->AddOrder("pos");
			$res = $DB->Select();

			while ($row = $DB->FetchAssoc($res)) {
				$DB->SetTable($this->db_prefix."files", "f");

				if (CF::IsIntNumeric($module_uri[0])) {
					$DB->AddTable($this->db_prefix."files_subj", "fs");
					$DB->AddCondFF("f.id","=", "fs.file_id");
					$DB->AddCondFS("fs.subject_id", "=", $module_uri[0]);
					$DB->AddCondFS("fs.view_id", "=", $row["id"]);
					$DB->AddCondFS("fs.view_id", "=", $row["id"]);
				} elseif ($module_uri[1] && $module_uri[0] == 'specs') {
					$DB->AddTable($this->db_prefix."files_subj", "fss");
					$DB->AddTable($this->db_prefix."subjects", "s");
					$DB->AddCondFF("f.id","=", "fss.file_id");
					$DB->AddCondFF("s.id","=", "fss.subject_id");
					$DB->AddCondFS("s.department_id","=", $dep_id);
					$DB->AddCondFS("fss.spec_id", "=", $module_uri[1]);
					$DB->AddCondFS("fss.view_id", "=", $row["id"]);
				}


				$res_file = $DB->Select();
				while ($row_file = $DB->FetchAssoc($res_file)) {
					$row["file_id"] = $row_file["file_id"];
					$row["file_descr"] = $row_file["descr"];
					$this->output["views"][] = $row;
				}
			}

			//die(print_r($this->output["views"]));

			if (CF::IsIntNumeric($module_uri[0])) {
				$DB->SetTable($this->db_prefix."subjects");
				$DB->AddCondFS("id", "=", $module_uri[0]);
				$res = $DB->Select();
				$this->output["subject"] = $DB->FetchAssoc($res);
			} elseif ($module_uri[1] && $module_uri[0] == 'specs') {
				$DB->SetTable($this->db_prefix."specialities");
				$DB->AddCondFS("id", "=", $module_uri[1]);
				$res = $DB->Select();
				$this->output["spec"] = $DB->FetchAssoc($res);
			}


			$DB->SetTable($this->db_prefix."departments");
			$DB->AddCondFS("id", "=", $dep_id);
			$res = $DB->Select();
			$this->output["department"] = $DB->FetchAssoc($res);

			$this->output["scripts_mode"] = $this->output["mode"] = "dep_subj_views";
		}*/
	}


	function prof_choice() {
		global $DB;

		$sql = 'select * from nsau_specialities where id in (select spec_id from nsau_spec_type where type="magistracy" and has_vacancies=1)';
		$res = $DB->Exec($sql);

		while($row = $DB->FetchAssoc($res))
			$this->output["mag_spec"][] = $row;

		$DB->SetTable("nsau_school");
		$res = $DB->Select();

		while($row = $DB->FetchAssoc($res))
			$this->output["schools"][] = $row;

		$sql = 'select * from nsau_specialities where id in (select spec_id from nsau_spec_type where (type="higher" or type="bachelor") and has_vacancies=1)';
		$res = $DB->Exec($sql);

		while($row = $DB->FetchAssoc($res)) {
			$this->output["high_spec"][$row["direction"]][] = $row;
		}
		//CF::Debug($this->output["high_spec"]);
	}

	function rekom() {
		global $DB;
		error_reporting(0);
		$DB->SetTable("nsau_exams");
		$res = $DB->Select();
		$on = array();

    $this->output["rekoms"] = array();
    $spec_exams = array();

		while ($row = $DB->FetchAssoc($res))
			$exams[$row["id"]] = $row;

		$DB->SetTable("nsau_spec_exams");
		if (isset($_POST["ege_bac_on"]) && $_POST["ege_bac_on"])
			$DB->AddCondFS("type", "=", "bachelor");
		if (isset($_POST["ege_spec_on"]) && $_POST["ege_spec_on"])
			$DB->AddCondFS("type", "=", "higher");
		if (isset($_POST["ege_mag_on"]) && $_POST["ege_mag_on"])
			$DB->AddCondFS("type", "=", "magistracy");
		if (isset($_POST["ege_sec_on"]) && $_POST["ege_sec_on"])
			$DB->AddCondFS("type", "=", "secondary");

		$res = $DB->Select();
		while ($row = $DB->FetchAssoc($res))
			if ($row["exam_id"] !=7 && $row["exam_id"] !=8)
				$spec_exams[$row["speciality_code"]][] = $row["exam_id"];
			//die(print_r($_POST));
		if (isset($_POST["ege_rus_on"]) && $_POST["ege_rus_on"]=="true" && $_POST["ege_rus"] >= $exams[1]["min_points"])
			$on[] = 1;
		elseif ($exams[1]["required"])
			$this->output["decline"] = 1;
		if (isset($_POST["ege_math_on"]) && $_POST["ege_math_on"]=="true" && $_POST["ege_math"] >= $exams[2]["min_points"])
			$on[] = 2;
		elseif ($exams[2]["required"])
			$this->output["decline"] = 1;
		if (isset($_POST["ege_inf_on"]) && $_POST["ege_inf_on"]=="true" && $_POST["ege_inf"] >= $exams[3]["min_points"])
			$on[] = 3;
		elseif ($exams[3]["required"])
			$this->output["decline"] = 1;
		if (isset($_POST["ege_hist_on"]) && $_POST["ege_hist_on"]=="true" && $_POST["ege_hist"] >= $exams[4]["min_points"])
			$on[] = 4;
		elseif ($exams[4]["required"])
			$this->output["decline"] = 1;
		if (isset($_POST["ege_phys_on"]) && $_POST["ege_phys_on"]=="true" && $_POST["ege_phys"] >= $exams[5]["min_points"])
			$on[] = 5;
		elseif ($exams[5]["required"])
			$this->output["decline"] = 1;
		if (isset($_POST["ege_biol_on"]) && $_POST["ege_biol_on"]=="true" && $_POST["ege_biol"] >= $exams[6]["min_points"])
			$on[] = 6;
		elseif ($exams[6]["required"])
			$this->output["decline"] = 1;
		/*if (isset($_POST["ege_chem_on"]) && $_POST["ege_chem_on"]=="true" && $_POST["ege_chem"] > $exams[7]["min_points"])
			$on[] = 7;
		elseif ($exams[7]["required"])
			$this->output["decline"] = 1;
		if (isset($_POST["ege_geo_on"]) && $_POST["ege_geo_on"]=="true" && $_POST["ege_geo"] > $exams[8]["min_points"])
			$on[] = 8;
		elseif ($exams[8]["required"])
			$this->output["decline"] = 1;*/
		if (isset($_POST["ege_lang_on"]) && $_POST["ege_lang_on"]=="true" && $_POST["ege_lang"] >= $exams[9]["min_points"])
			$on[] = 9;
		elseif ($exams[9]["required"])
			$this->output["decline"] = 1;
		if (isset($_POST["ege_soc_on"]) && $_POST["ege_soc_on"]=="true" && $_POST["ege_soc"] >= $exams[10]["min_points"])
			$on[] = 10;
		elseif ($exams[10]["required"])
			$this->output["decline"] = 1;

		$specialities = array();
    $DB->SetTable("nsau_specialities", "s");
    $DB->AddTable("nsau_spec_type", "st");
    $DB->AddExp("s.*");
    $DB->AddCondFF("s.id", "=", "st.spec_id");
    $DB->AddCondFS("st.has_vacancies", "=", 1);
		$res = $DB->Select();
		while ($row = $DB->FetchAssoc($res))
			$specialities[$row["code"]] = $row["code"]." - ".$row["name"];

		//print_r($spec_exams);print_r($on);
    foreach ($spec_exams as $code => $exams_ids) {

			//$exams_ids[] = 1;
			//$exams_ids[] = 2;
			//if (!count(array_diff($exams_ids, $on)))
			$dec = 0;
			foreach ($exams_ids as $eid)
				if (!in_array($eid, $on)) {
					$dec = 1;
					}
			if (!$dec && isset($specialities[$code]) && !in_array($specialities[$code], $this->output["rekoms"]))
				$this->output["rekoms"][] = $specialities[$code];

		}

		if(empty($this->output["rekoms"]))
			$this->output["error"] = "� ������ ������������ ��� �� ������� ���������� ��������������.";
		/*if (in_array(1, $on) && in_array(2, $on) && in_array(6, $on)) {
			$this->output["rekoms"][] = $specialities["110204"];
			$this->output["rekoms"][] = $specialities["110102"];
			$this->output["rekoms"][] = $specialities["110200"];
			$this->output["rekoms"][] = $specialities["110203"];
			$this->output["rekoms"][] = $specialities["250100"];
		}
		elseif (in_array(1, $on) && in_array(2, $on) && in_array(8, $on))
			$this->output["rekoms"][] = $specialities["250100"];*/

			//die(print_r($this->output["rekoms"]));

		if (/*!isset($this->output["rekoms"]) || */!is_array($this->output["rekoms"]) || !count($this->output["rekoms"]))
			$this->output["decline"] = 1;
	}

	function parsexls() {

	}
	function sw_group_select($id) {
		global $DB;
		$DB->SetTable("nsau_groups");
		//$DB->AddCondFS("hidden", "=", 0);
		$DB->AddCondFS("id", "=", $id);
		$row = $DB->FetchAssoc($DB->Select(1));
		$DB->SetTable("nsau_faculties");
		$DB->AddCondFS("id", "=", $row["id_faculty"]);
		$fac = $DB->FetchAssoc($DB->Select(1));
		$row["faculty"] = $fac["name"];

		if(!empty($row["specialities_code"])) {
			$DB->SetTable("nsau_specialities");
			$DB->AddCondFS("code", "=", $row["specialities_code"]);
			$spec = $DB->FetchAssoc($DB->Select(1));
			$row["speciality"] = $row["specialities_code"] . "-" . $spec["name"];
		}

		if(!empty($row["form_education"])) {
			$row["form"] = ($row["form_education"]==1 ? "�����" : ($row["form_education"]==2 ? "�������" : "����-�������"));
		}
		$DB->SetTable("nsau_departments");

		/*������� ��� ������ ���� ������ � �����������*/
		// $DB->AddCondFS("faculty_id", "=", $row["id_faculty"]);

		$DB->AddCondFS("is_active", "=", 1);
		$dep_res = $DB->Select();

		while($dep = $DB->FetchAssoc($dep_res)) {
			//$row["departments"][$dep["id"]] = $dep["name"];

			$DB->SetTable("nsau_faculties");
			$DB->AddCondFS("id", "=", $dep["faculty_id"]);
			$fac = $DB->FetchAssoc($DB->Select(1));
			$fac = $fac["name"];
			$row["departments"][$fac][$dep["id"]] = $dep["name"];


			//$row["departments"]["lol"] = $dep["faculty_id"];
		}


		$DB->SetTable("nsau_people");
		$DB->AddCondFS("id_group", "=", $row["id"]);
		$DB->AddCondFS("status_id", "=", 8);
		$peop_res = $DB->Select();
		while($people = $DB->FetchAssoc($peop_res)) {
			$row["students"][$people["user_id"]] = $people["last_name"]." ".$people["name"]." ".$people["patronymic"];
		}


			$this->output["group"] = $row;

	}
	function sw_group_search($search) {
		global $DB;
		$DB->SetTable("nsau_groups");
		//$DB->AddCondFS("hidden", "=", 0);
		$DB->AddCondFS("name", "LIKE", "%".iconv("utf-8", "cp1251", $search)."%");
		$res = $DB->Select();
		while($row = $DB->FetchAssoc($res))
			$this->output["group2"][] = $row;
	}

	function employees($search = null, $postID = null, $specID = null, $subjectID = null, $levelID = null, $degreeID = null, $rankID = null){
		global $DB, $Auth;

			$DB->SetTable("nsau_teachers", "nt");
			$DB->AddTable("nsau_people", "np");
			$DB->AddCondFF("np.id", "=", "nt.people_id");
			$DB->AddCondFS("np.status_id", "!=", 9);
			$DB->AddCondFS("np.status_id", "!=", 10);
			$DB->AddAltFS("nt.post", "=", "�������������");
			$DB->AddAltFS("nt.post", "=", "����������");
			$DB->AddField("np.id", "id");
			$res_count =  $DB->Select();
			$count_items = $DB->NumRows($res_count);
			require_once INCLUDES . "Pager" . CLASS_EXT;
			$Pager = new Pager($count_items, 10, 15);
			$this->output["pager_output"]  = $Pager->Act();

			$from = (!empty($postID) || !empty($specID) || !empty($subjectID) || !empty($levelID) || !empty($degreeID) || !empty($rankID)) ? null : $this->output["pager_output"]["db_from"];
			$limit = (!empty($postID) || !empty($specID) || !empty($subjectID) || !empty($levelID) || !empty($degreeID) || !empty($rankID)) ? null : $this->output["pager_output"]["db_limit"];

			$DB->SetTable("nsau_teachers", "nt");
			$DB->AddTable("nsau_people", "np");
			$DB->AddCondFF("np.id", "=", "nt.people_id");
			$DB->AddCondFS("np.status_id", "!=", 9);
			$DB->AddCondFS("np.status_id", "!=", 10);
			$DB->AddAltFS("nt.post", "=", "�������������");
			$DB->AddAltFS("nt.post", "=", "����������");
			$DB->AppendAlts();
			//search
			if(!empty($search)) {
				$DB->AddAltFS("np.last_name", "LIKE", "%".iconv("utf-8", "cp1251", $search)."%");
				$DB->AddAltFS("np.name", "LIKE", "%".iconv("utf-8", "cp1251", $search)."%");
				// $DB->AddAltFS("np.patronymic", "LIKE", "%".iconv("utf-8", "cp1251", $search)."%");
				$DB->AppendAlts();
			}
			//������
			if(!empty($postID)){//���������
				$DB->AddTable("nsau_teachers_department_posts", "ntdp");
				$DB->AddCondFF("ntdp.people_id", "=","np.id");
				$DB->AddCondFS("ntdp.post_id", "=", $postID);
				$DB->AppendAlts();
			}
			if(!empty($specID)){//��� ���������
				$DB->AddTable("nsau_teacher_edu_program", "ntep");
				$DB->AddCondFF("ntep.people_id", "=","np.id");
				$DB->AddCondFS("ntep.spec_id", "=", $specID);
				$DB->AppendAlts();
			}
			if(!empty($subjectID)){//����������
				$DB->AddTable("nsau_subj_list", "nsl");
				$DB->AddCondFF("nsl.people_id", "=", "np.id");
				$DB->AddTable("nsau_subjects", "ns");
				$DB->AddCondFF("ns.id", "=","nsl.subject_id");
				$DB->AddCondFS("ns.id", "=", $subjectID);
				$DB->AddField("nsl.subject_id", "subject_id");
				$DB->AppendAlts();
			}
			if(!empty($rankID)){//������
				$DB->AddAltFS("nt.academ_stat", "REGEXP", "[[:<:]]{$rankID}[[:>:]]");
				$DB->AppendAlts();
			}
			if(!empty($degreeID)){//�������
				$DB->AddAltFS("nt.degree", "REGEXP", "[[:<:]]{$degreeID}[[:>:]]");
				$DB->AppendAlts();
			}

			$DB->AddField("np.id", "id");
			$DB->AddField("np.last_name", "last_name");
			$DB->AddField("np.name", "name");
			$DB->AddField("np.patronymic", "patronymic");
			$DB->AddField("nt.degree", "degree");
			$DB->AddField("nt.academ_stat", "academ_stat");
			$DB->AddField("np.education", "education");
			$DB->AddField("np.add_education", "add_education");
			$DB->AddField("np.exp_full", "exp_full");
			$DB->AddField("np.exp_teach", "exp_teach");
			$DB->AddOrder("np.last_name");

			if(!empty($search))
				$res = $DB->Select(10, 0, true, true, true);
			else
				$res = $DB->Select($limit, $from, true, true, true);
			while($row = $DB->FetchAssoc($res)) {

				//���������
				///////////////
			
				$posts = "";
				$DB->SetTable("nsau_teachers_department_posts");
				$DB->AddCondFS("people_id", "=", $row["id"]);
				$pres = $DB->Select();
				while($prow = $DB->FetchAssoc($pres)) {
					$DB->SetTable("nsau_teachers_post");
					$DB->AddCondFS("id", "=", $prow["post_id"]);
					$post = $DB->FetchAssoc($DB->Select(1));
					$posts[] = $post['name'];
				}
				$row["posts"] = implode(', ', $posts);

				//����������
				///////////////
				$DB->SetTable("nsau_subj_list");
				$DB->AddCondFS("people_id", "=", $row["id"]);
				$sres = $DB->Select();
				while($srow = $DB->FetchAssoc($sres))	{
					$DB->SetTable("nsau_subjects");
					$DB->AddCondFS("id", "=", $srow["subject_id"]);
					$DB->AddCondFS("is_hidden", "=", 0);
					$subj = $DB->FetchAssoc($DB->Select(1));
					if (isset($subj["name"]))
						$subjects[$row["id"]][] = $subj["name"];
				}
				//������
				///////////////
				$rank = explode(";", $row["academ_stat"]);
				foreach($rank as $rid) {
					if(!empty($rid)) {
						$DB->SetTable("nsau_teachers_rank");
						$DB->AddCondFS("id", "=", $rid);
						$rrow = $DB->FetchAssoc($DB->Select(1));
						$ranks[$row["id"]][] = $rrow["name"];
					}
				}
				//�������
				///////////////
				$degr = explode(";", $row["degree"]);
				foreach($degr as $did) {
					if(!empty($did)) {
						$DB->SetTable("nsau_teachers_degree");
						$DB->AddCondFS("id", "=", $did);
						$drow = $DB->FetchAssoc($DB->Select(1));
						$degrees[$row["id"]][] = $drow["name"];
					}
				}
				//�����������
				///////////////
				$edu = explode(";", $row["education"]);
				foreach($edu as $eid) {
					if(!empty($eid)) {
						$DB->SetTable("nsau_education");
						$DB->AddCondFS("id", "=", $eid);
						$erow = $DB->FetchAssoc($DB->Select(1));
						$educations[$row["id"]][] = array("edu" => $erow["university"], "qual" => $erow["qualification"], "spec" => $erow["speciality"], 'lvl' => $this->name_edu($erow['edu_level']));
					}
				}
				//��������� ������������
				///////////////
				$DB->SetTable("nsau_prof_development");
				$DB->AddCondFS("people_id", "=", $row["id"]);
				$DB->AddCondFS("edu_type", "=", "���������������� ��������������");
				$aeres = $DB->Select();
				while($aerow = $DB->FetchAssoc($aeres))	{
						$add_educations[$row["id"]][] = array("edu_type" => $aerow["edu_type"], "location" => $aerow["location"], "course" => $aerow["course"]);
				}

				$date_begin  = date("Y", mktime(0, 0, 0, date("m"),   date("d"),   date("Y")-3))."-01-01";
				$DB->SetTable("nsau_prof_development");
				$DB->AddCondFS("people_id", "=", $row["id"]);
				$DB->AddCondFS("edu_type", "!=", "���������������� ��������������");
				$DB->AddCondFS("date_end", ">=", $date_begin);
				$aeres = $DB->Select();
				while($aerow = $DB->FetchAssoc($aeres))	{
						$add_educations[$row["id"]][] = array("edu_type" => $aerow["edu_type"], "location" => $aerow["location"], "course" => $aerow["course"]);
				}
				$row["post"] = $post["name"];
				$row["subj"] = $subjects[$row["id"]];
				$row["rank"] = $ranks[$row["id"]];
				$row["degree"] = $degrees[$row["id"]];
				$row["edu"] = $educations[$row["id"]];
				$row["aedu"] = $add_educations[$row["id"]];

				//��������������� ��������� ������� ��������������
				$programList = $DB->SetTable('nsau_teacher_edu_program', 'pr')
					->AddTable('nsau_specialities', 'sp')
					->AddCondFF("pr.spec_id", '=', 'sp.id')
					->addCondFs('pr.people_id', '=', (int)$row['id'])
					->AddField('pr.id', 'id')
					->AddField('pr.spec_id', 'spec_id')
					->AddField('sp.code', 'code')
					->AddField('sp.name', 'name')
					->AddOrder('pr.id')
					->SelectArray();

				$row['programs'] = $programList ? $programList: array();

				$this->output["teachers"][$row["id"]] = $row;
			}

			//������ �����������
			if(!empty($levelID)){
				$nameEdu = $this->name_edu($levelID);

				foreach($this->output["teachers"] as $id=>$row) {
					$isLvl = false;
					if(!empty($row['edu'])) {
						foreach($row['edu'] as $edu){
							if($nameEdu === $edu['lvl']){
								$isLvl = true;
							}
						}
					}

					if($isLvl) continue;
					unset($this->output['teachers'][$id]);

				}
			}
	}

	function employeesDataFilter(){
		global $DB;

		$DB->SetTable("nsau_teachers", "nt");
		$DB->AddTable("nsau_people", "np");
		$DB->AddTable("nsau_subj_list", "nsl");
		$DB->AddCondFF("nsl.people_id", "=", "np.id");
		$DB->AddCondFF("np.id", "=", "nt.people_id");
		$DB->AddCondFS("np.status_id", "!=", 9);
		$DB->AddField("np.id", "id");
		$DB->AddField("np.last_name", "last_name");
		$DB->AddField("np.name", "name");
		$DB->AddField("np.patronymic", "patronymic");
		$DB->AddOrder("np.last_name");
		$res = $DB->Select(null, null, true, true, true);
		while($row = $DB->FetchAssoc($res)){
			$this->output["filter"]["teachers"][$row["id"]] = sprintf("%s %s %s", $row["last_name"], $row["name"], $row["patronymic"]);
		}

		//������ id ��������������
		$teachersID = array_keys($this->output["filter"]["teachers"]);
		unset($this->output["filter"]["teachers"]);

		//���������
		///////////////
		$posts = array();
		$DB->SetTable("nsau_teachers_department_posts");
		$DB->AddCondFO("people_id", "IN(". implode(",", $teachersID) .")");
		$DB->AddField("post_id");
		$pres = $DB->Select(null, null, true, true, true);
		while($prow = $DB->FetchAssoc($pres)) {
			$posts[] = $prow["post_id"];
		}
		$DB->SetTable("nsau_teachers_post");
		$DB->AddCondFO("id", "IN(". implode(",", $posts) .")");
		$DB->AddField("id");
		$DB->AddField("name");
		$DB->AddOrder("pos");
		$res = $DB->Select(null, null, true, true, true);
		unset($posts);
		while($row = $DB->FetchAssoc($res)) {
			$this->output["filter"]["post"][$row["id"]] = $row["name"];
		}

		//��������������� ��������� ������� ��������������
		$DB->SetTable('nsau_teacher_edu_program', 'pr');
		$DB->AddTable('nsau_specialities', 'sp');
		$DB->AddCondFF("pr.spec_id", '=', 'sp.id');
		$DB->AddCondFO('pr.people_id', "IN(". implode(",", $teachersID) .")");
		$DB->AddField('pr.spec_id', 'spec_id');
		$DB->AddField('sp.code', 'code');
		$DB->AddField('sp.name', 'name');
		$DB->AddOrder('pr.id');
		$res = $DB->Select(null, null, true, true, true);
		while($row = $DB->FetchAssoc($res)) {
			$this->output["filter"]["program"][$row["spec_id"]] = array("code" => $row["code"] , "name" => $row["name"]);
		}

		//����������
		$DB->SetTable("nsau_subj_list", "nsl");
		$DB->AddTable("nsau_subjects", "ns");
		$DB->AddCondFF("nsl.subject_id", '=', 'ns.id');
		$DB->AddCondFO('nsl.people_id', "IN(". implode(",", $teachersID) .")");
		$DB->AddField('ns.id', 'id');
		$DB->AddField('ns.name', 'name');
		$DB->AddOrder('ns.id');
		$res = $DB->Select(null, null, true, true, true);
		while($row = $DB->FetchAssoc($res)) {
			$this->output["filter"]["subjects"][$row["id"]] = $row["name"];
		}

		//������
		$DB->SetTable("nsau_teachers_rank");
		$DB->AddField('id');
		$DB->AddField('name');
		$DB->AddOrder('id');
		$res = $DB->Select(null, null, true, true, true);
		while($row = $DB->FetchAssoc($res)) {
			$this->output["filter"]["rank"][$row["id"]] = $row["name"];
		}

		//�������
		$DB->SetTable("nsau_teachers_degree");
		$DB->AddField('id');
		$DB->AddField('name');
		$DB->AddOrder('id');
		$res = $DB->Select(null, null, true, true, true);
		while($row = $DB->FetchAssoc($res)) {
			$this->output["filter"]["degree"][$row["id"]] = $row["name"];
		}

		//������� �����������
		$DB->SetTable("nsau_teachers_edu_level");
		$DB->AddField('id');
		$DB->AddField('name');
		$DB->AddOrder('id');
		$res = $DB->Select(null, null, true, true, true);
		while($row = $DB->FetchAssoc($res)) {
			$this->output["filter"]["level"][$row["id"]] = $row["name"];
		}

		unset($rs);

	}




	function name_edu ($id)
	{
		global $DB;
		$DB->SetTable("nsau_teachers_edu_level");
		$DB->AddCondFS("id", "=", $id);
		$result = $DB->FetchAssoc($DB->Select(1));
		return $result['name'];
	}

	function students_progress($semester = null) {
		global $DB, $Auth;
		$DB->SetTable("nsau_people");
		$DB->AddCondFS("id", "=", $Auth->people_id);
		$people = $DB->FetchAssoc($DB->Select(1));

		$DB->SetTable($this->db_prefix."groups");
		$DB->AddCondFS("id", "=", $people["id_group"]);
		$sgr = $DB->FetchAssoc($DB->Select(1));
		$this->output["group"] = $sgr["name"];
		$intgroup = preg_replace("~[^0-9]+~", "", $this->output["group"]);
		$this->output["course"] = $intgroup[1];

		$code = $people["library_card"];
		$this->output["test"] = $code;
		$DB->SetTable("nsau_students_progress");
		$DB->AddCondFS("people_id", "=", $Auth->people_id);
		if(!empty($semester) && ($semester!="all"))
			$DB->AddCondFS("semester", "=", intval($semester));
		elseif($semester=="all") {
			for($i=1;$i<$this->output["course"]*2;$i++) {
				$DB->AddAltFS("semester", "=", $i);
			}
			$DB->AppendAlts();
		} else
			$DB->AddCondFS("semester", "=", 1);
		$DB->AddOrder("semester");
		$DB->AddOrder("type", true);
		$DB->AddField("code");
		$DB->AddField("semester");
		$DB->AddField("subject");
		$DB->AddField("hours");
		$DB->AddField("type");
		$DB->AddField("value");
		$res = $DB->Select();
		while($row = $DB->FetchAssoc($res)) {
			$this->output["progress"][$row["semester"]][] = $row;
		}
	}

	function check_encode($str){
    $cp_list = array('windows-1251');
    foreach ($cp_list as $k=>$codepage){
        if (md5($str) === md5(iconv($codepage, $codepage, $str))){
            return $codepage;
        }
    }
    return null;
	}

	function sendNewPasswd($email, $pass, $username)
	{
		$headers = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=windows-1251\r\n";
		$headers .= "Content-Language: ru\r\n";
		$headers .= "From: ".'=?koi8-r?B?'.base64_encode(iconv('windows-1251', 'koi8-r', "����")).'?='." <"."noreply@nsau.edu.ru".">\r\n";
		$headers .= "Return-Path: ".'=?koi8-r?B?'.base64_encode(iconv('windows-1251', 'koi8-r', "����")).'?='." <"."noreply@nsau.edu.ru".">\r\n";
		$headers .= "X-Priority: 3\r\n";
		$headers .= "User-Agent: NSAU Mailer\r\n"; // �� ���������, ������, �� ������
		$headers .= "X-Mailer: NSAU Mailer\r\n";   // �� ���������, ������, �� ������
		$subject = '=?koi8-r?B?'.base64_encode(iconv('windows-1251', 'koi8-r', "������ ������� �� ������� ����")).'?=';
		$message = "����� ������ ������ �������� '".$username."' �� ����� ".SITE_SHORT_NAME." - ".$pass;
		if (!mail($email, $subject, $message, $headers, " -f noreply@nsau.edu.ru"))
			$this->output["messages"]["bad"][] = "���� �������� ���������.";
	}

	function timetable_weekparity() {			//�������� ������� ������
		$today = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
		$first = mktime(0, 0, 0, 9, 3, (date("m") < 9 ? date("Y") - 1 : date("Y")));
		$first_week = date("w", mktime(0, 0, 0, 9, 3, (date("m") < 9 ? date("Y") - 1 : date("Y"))));
		$day = ($today - $first)/(24*60*60);
		$day += $first_week == 0 ? 6 : $first_week - 1;
		$j = (($day - ($day % 7)) / 7) + 1;
		if($j%2 == 1) return "odd"; 					//��������
		else return "even";						//������
	}

	function ajax_change_people_cat($people_id, $cat) {
		global $DB, $Auth;
		if($Auth->usergroup_id==1) {
			$DB->SetTable("nsau_people");
			$DB->AddCondFS("id", "=", $people_id);
			$DB->AddValue("people_cat", $cat);
			$DB->Update();
			return "changed";
		} else return "failed";
	}

	function department_subjects($id) {
		global $DB;
		$DB->SetTable("nsau_subjects");
		$DB->AddCondFS("department_id", "=", $id);
		$DB->AddCondFS("is_hidden", "=", 0);
		$res = $DB->Select();
		while($row = $DB->FetchAssoc($res)) {
			$result[] = $row;
		}
		return $result;
	}

	function table_generator() {
		global $DB;
		####### TABLE 11
		// $types = array('graduate' => 5, 'bachelor' => 4);
		// $DB->SetTable('nsau_specialities');
		// $DB->AddCondFS("code", "LIKE", "%.%");
		// $res = $DB->Select();
		// while($row = $DB->FetchAssoc($res)) {
		// 	$DB->SetTable('nsau_spec_type');
		// 	$DB->AddCondFS('code', '=', $row['code']);
		// 	$st = $DB->FetchAssoc($DB->Select(1));

		// 	$row['years'] = $types[$st['type']];
		// 	if(empty($row['years'])) $row['years'] = 5;

		// 	$DB->SetTable('nsau_profiles');
		// 	$DB->AddCondFS("spec_id", "=", $row['id']);
		// 	$pres = $DB->Select();
		// 	while($prow = $DB->FetchAssoc($pres)) {
		// 		$row['profile'][] = $prow['name'];
		// 	}
		// 	$result[] = $row;
		// }
		// return $result;
		#################
		#
		####### TABLE12
		// $DB->SetTable('nsau_specialities');
		// $DB->AddCondFS("code", "LIKE", "%.%");
		// $res = $DB->Select();
		// while($row = $DB->FetchAssoc($res)) {
		// 	$DB->SetTable('nsau_profiles');
		// 	$DB->AddCondFS("spec_id", "=", $row['id']);
		// 	$pres = $DB->Select();
		// 	while($prow = $DB->FetchAssoc($pres)) {
		// 		$DB->SetTable('nsau_docs');
		// 		$DB->AddCondFS("speciality_id", "=", $row['id']);
		// 		$DB->AddCondFS("profile_id", "=", $prow['id']);
		// 		$fres = $DB->Select();
		// 		while($frow = $DB->FetchAssoc($fres)) {
		// 			$row['profile'][$prow['id']]['year'] = $frow['year'];
		// 			$row['profile'][$prow['id']]['form'] = $frow['form'];
		// 		}
		// 		$row['profile'][$prow['id']]['name'] = $prow['name'];
		// 	}
		// 	$result[] = $row;
		// }
		// return $result;
		######################
		#
		####TABLE 7,8,9
		#
		$types = array(
			'bachelor' => '������ ����������� � �����������',
			'higher' => '������ ����������� � �����������',
			'magistracy' => '������ ����������� � ������������',
			'graduate' => '������ ����������� � ���������� ������ ������ ������������',
			'secondary' => '������� ���������������� �����������'
		);

		$DB->SetTable('nsau_spec_type');
		$DB->AddCondFS('code', '=', $_REQUEST['data']['code']);
		$st = $DB->FetchAssoc($DB->Select(1));
		$json = $types[$st['type']];
		header("Cache-Control: no-cache, must-revalidate");
		header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
		header("Pragma: no-cache");
		$this->output["json"] = $json;	

	}

	function getPlanBySpeciality($code, $html){
		$dom = new DOMDocument();
		$dom->loadHTML(trim($html));
		$nod = $dom->getElementsByTagName('tbody');
		$planList = array();
		$budget = array(3,4,5,6,7,8);
		$comerce = array(9,10,11);

		if ($nod->length) {
			$trList = $nod->item(0)->getElementsByTagName('tr');
			if($trList->length){
				$i_tr = 0;
				foreach($trList as $tr){
					$tdList = $trList->item($i_tr)->getElementsByTagName('td');
					if($tdList->length){
						$i_td = 0;
						foreach($tdList as $td){
							if($tdList->item(0)->nodeValue == $code){
								$num = trim($tdList->item($i_td)->nodeValue);

								if(in_array($i_td, $budget, true) && is_numeric($num)){
									$planList['budget']+= $num;
								}
								if(in_array($i_td, $comerce, true) && is_numeric($num)){
									$planList['comerce']+= $num;
								}
							}
							$i_td++;
						}
					}
					$i_tr++;
				}
			}
		}

    	return $planList;
	}

	function getPlanBySpecialitySPOnew($code, $html) {
		$dom = new DOMDocument();
		$dom->loadHTML(trim($html));
		$nod = $dom->getElementsByTagName('tbody');
		if ($nod->length) {
			$trList = $nod->item(0)->getElementsByTagName('tr');
			if($trList->length){
				for ($i_tr = 0; $i_tr < $trList->length; $i_tr++){
					$tdList = $trList->item($i_tr)->getElementsByTagName('td');
					if($tdList->length){
						if ($tdList->item(0)->nodeValue == $code && is_numeric($tdList->item(2)->nodeValue)){
							return $tdList->item(2)->nodeValue;
						}
					}
				}
			}
		}
	}


	function getPlanBySpecialitySPO($code, $html){
		$dom = new DOMDocument();
		$dom->loadHTML(trim($html));
		$nod = $dom->getElementsByTagName('thead');
		$planList = array();
		$budget = array(2,4,6);
		$comerce = array(3,5,7);

		if ($nod->length) {
			$trList = $nod->item(0)->getElementsByTagName('tr');
			if($trList->length){
				$i_tr = 3;
				$nodeListLength = $trList->length;
				for ($i_tr; $i_tr < $nodeListLength; $i_tr++){
					$tdList = $trList->item($i_tr)->getElementsByTagName('td');
					if($tdList->length){
						$i_td = 0;
						foreach($tdList as $td){
							if($tdList->item(0)->nodeValue == $code){
								$num = trim($tdList->item($i_td)->nodeValue);

								if(in_array($i_td, $budget, true) && is_numeric($num)){
									$planList['budget']+= $num;
								}
								if(in_array($i_td, $comerce, true) && is_numeric($num)){
									$planList['comerce']+= $num;
								}
							}
							$i_td++;
						}
					}
				}
			}
		}

		return $planList;
	}

	function getTextItems($id){
    	global $DB;

		$DB->SetTable('text_items');
		$DB->AddCondFS('id', '=', $id);
		$DB->AddField('text');

		return $DB->SelectArray(1);
	}

	function ajax_edit_teacher_educational_program($id, $peopleId, $mode){
		global $DB;
		$DB->SetTable("nsau_teacher_edu_program");

		if($mode == 'insert'){
			$DB->AddValue("people_id", (int)$peopleId);
			$DB->AddValue("spec_id", (int)$id);
			if($DB->Insert()){
				$this->output['id'] = $DB->LastInsertID();
				return $this->output['response'] = 'insert';
			}
		}elseif($mode == 'delete'){
			$DB->AddCondFS("id", "=", (int)$id);
			if($DB->Delete()){
				return $this->output['response'] = 'delete';
			}
		}

		return $this->output['response'] = 'failed';
	}
		
    function Output()
    {
        return $this->output;
    }

}
?>
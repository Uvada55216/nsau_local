<?php
class Eui

{
	private $output;

	function Eui($global_params, $params, $module_id, $node_id, $module_uri)
	{
		global $Engine, $Auth, $DB;
		$this->output["user_id"] = $this->get_user_id($Auth->user_id); //ID пользователя
		$this->output['mode'] = $params; //параметры модуля

		switch ($params)
		{
			case 'admin_ui':
			{
				$this->output["scripts_mode"] = $this->output["mode"] = "admin_ui"; //подключение ptpl
				$this->output["mode"] = "admin_ui";

        		//$this->ajax_delete_subj($_REQUEST['data']['subj_id']);

				/*поиск по таблице успеваемости студентов------------------------------------------------------*/
				if (isset($_POST['diapazon_select'])) 
				{
					$DB->SetTable("nsau_students_progress");
					$DB->AddCondFS("semester", "=", $_POST['semester']);
					$DB->AddOrder("subject");
					$DB->AddOrder("code");
					$DB->AddOrder("type");
					$DB->AddOrder("value");
					$dublicate=0;
					if (!empty($_POST['id_min']) && !empty($_POST['id_max'])) 
					{
						$DB->AddAltFS("id", ">", $_POST['id_min']); $DB->AddAltFS("id", "=", $_POST['id_min']);
						$DB->AppendAlts();
						$DB->AddAltFS("id", "<", $_POST['id_max']); $DB->AddAltFS("id", "=", $_POST['id_max']);
						$DB->AppendAlts();
					}
					$old_mass=0;
					
					$q = $DB->Select();
					while ($res = $DB->FetchAssoc($q))
					{
						$sub_data[$res['subject']]= array
							(
								"code"=>$res['code'],
								"subject"=>$res['subject'],
								"hours"=>$res['hours'],
								"type"=>$res['type'],
								"value"=>$res['value']
							);

						if (!isset($_POST['show_dubl'])) 
						{
							if ($sub_data[$res['subject']] == $old_mass) 
							{
								$old_mass = $sub_data[$res['subject']];
								unset($sub_data[$res['subject']]);
								$dublicate++;
							}
							else
							{
								$old_mass = $sub_data[$res['subject']];
								$data[$res['id']]	=	$sub_data[$res['subject']];
							}
						}
						else
						{
							$data[$res['id']]	=	$sub_data[$res['subject']];
						}	
					}

					$this->output['data'] = $data;
					$this->output['dublicate'] = $dublicate;
				}

				/*удаление диапазона*/
				if (isset($_POST['diapazon_delete'])) 
				{
					if (!empty($_POST['id_min']) && !empty($_POST['id_max']) && !empty($_POST['semester'])) 
					{
						$DB->SetTable("nsau_students_progress");
						$DB->AddCondFS("semester", "=", $_POST['semester']);
						$DB->AddAltFS("id", ">", $_POST['id_min']); $DB->AddAltFS("id", "=", $_POST['id_min']);
						$DB->AppendAlts();
						$DB->AddAltFS("id", "<", $_POST['id_max']); $DB->AddAltFS("id", "=", $_POST['id_max']);
						$DB->AppendAlts();
						$DB->Delete();
						$DB->Update();
					}
				}

				/*удаление дубликатов---------------------------------------------------------------------------*/
				if (isset($_POST['dublicate_delete'])) 
				{
					$DB->SetTable("nsau_students_progress");
					$DB->AddCondFS("semester", "=", $_POST['semester']);
					$DB->AddOrder("subject");
					$DB->AddOrder("code");
					$DB->AddOrder("type");
					$DB->AddOrder("value");
					$dublicate=0;
					if (!empty($_POST['id_min']) && !empty($_POST['id_max'])) 
					{
						$DB->AddAltFS("id", ">", $_POST['id_min']); $DB->AddAltFS("id", "=", $_POST['id_min']);
						$DB->AppendAlts();
						$DB->AddAltFS("id", "<", $_POST['id_max']); $DB->AddAltFS("id", "=", $_POST['id_max']);
						$DB->AppendAlts();
					}
					$old_mass=0;
					
					$q = $DB->Select();
					while ($res = $DB->FetchAssoc($q))
					{
						$sub_data[$res['subject']]= array
							(
								"code"=>$res['code'],
								"subject"=>$res['subject'],
								"hours"=>$res['hours'],
								"type"=>$res['type'],
								"value"=>$res['value']
							);


						if ($sub_data[$res['subject']] == $old_mass) 
						{
							$old_mass = $sub_data[$res['subject']];
							unset($sub_data[$res['subject']]);

							$DB->SetTable("nsau_students_progress");
							$DB->AddCondFS("id", "=", $res['id']);
							$DB->Delete();
							$DB->Update();
						}
						else
						{
							$old_mass = $sub_data[$res['subject']];
						}
					}
				}
				/*----------------------------------------------------------------------------------------------*/

				/*удаление 0 семестра*/
				if (isset($_POST['null_delete'])) 
				{
						$DB->SetTable("nsau_students_progress");
						$DB->AddCondFS("semester", "=", 0);
						$DB->Delete();
						$DB->Update();
				}



				/*GIVE PRIV for curators-------------------------------------------------------------------------*/
				$DB->SetTable("nsau_groups_curators");
				$q = $DB->Select();
				$id="";
				while ($res = $DB->FetchAssoc($q))
					{
						$DB->SetTable("nsau_teachers");
						$DB->AddCondFS("id", "=", $res['curator_id']);
						$qq = $DB->Select();
						while ($ress = $DB->FetchAssoc($qq))
						{
							$DB->SetTable("nsau_people");
							$DB->AddCondFS("id", "=", $ress['people_id']);
							$DB->AddCondFS("people_cat", "=", 2);
							$qqq = $DB->Select();
							while ($resss = $DB->FetchAssoc($qqq))
							{
								$curators[$ress['people_id']]= array
								(
									"group_name"=>$this->get_name_group($res['group_id']),
									"group_id"=>$res['group_id'],
									"curator"=>$resss['last_name']." ".$resss['name']." ".$resss['patronymic'],
									"people"=>$ress['people_id'],
									"auth"=>$resss['user_id'],
									"teachers"=>$ress['id']
								);

								//$idd .= $resss['user_id'].",";
							}
						}
					}
				$this->output['curators'] = $curators;


			}
			break;
		}
	}

	function get_name_group ($id)
	{
		global $DB;
		$DB->SetTable("nsau_groups");
		$DB->AddCondFS("id", "=", $id);
		$DB->Select(1);
		$row = $DB->FetchAssoc();
		return $row["name"];
	}

	/*возвращает ID пользователя*/
	function get_user_id($id)
	{
		global $DB;
		if(isset($id))
		{
			$DB->SetTable("nsau_people");
			$DB->AddCondFS("user_id", "=", $id);
			$DB->AddField("id", "user");
			$DB->Select(1);
			$row = $DB->FetchAssoc();
			return $row["user"];
		}
		else
		{
			return false;
		}
	}

	function Output()
	{
		return $this->output;
	}
}
?>
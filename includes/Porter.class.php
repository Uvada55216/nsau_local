<?php
class Soundex_Stem_Ru 
{
    var $VERSION = "0.02";
    var $Stem_Caching = 0;
    var $Stem_Cache = array();
    var $VOWEL = '/аеиоуыэюя/';
    var $PERFECTIVEGROUND = '/((ив|ивши|ившись|ыв|ывши|ывшись)|((?<=[ая])(в|вши|вшись)))$/';
    var $REFLEXIVE = '/(с[яь])$/';
    var $ADJECTIVE = '/(ее|ие|ые|ое|ими|ыми|ей|ий|ый|ой|ем|им|ым|ом|его|ого|еых|ую|юю|ая|яя|ою|ею)$/';
    var $PARTICIPLE = '/((ивш|ывш|ующ)|((?<=[ая])(ем|нн|вш|ющ|щ)))$/';
    var $VERB = '/((ила|ыла|ена|ейте|уйте|ите|или|ыли|ей|уй|ил|ыл|им|ым|ены|ить|ыть|ишь|ую|ю)|((?<=[ая])(ла|на|ете|йте|ли|й|л|ем|н|ло|но|ет|ют|ны|ть|ешь|нно)))$/';
    var $NOUN = '/(а|ев|ов|ие|ье|е|иями|ями|ами|еи|ии|и|ией|ей|ой|ий|й|и|ы|ь|ию|ью|ю|ия|ья|я)$/';
    var $RVRE = '/^(.*?[аеиоуыэюя])(.*)$/';
    var $DERIVATIONAL = '/[^аеиоуыэюя][аеиоуыэюя]+[^аеиоуыэюя]+[аеиоуыэюя].*(?<=о)сть?$/';

    function s(&$s, $re, $to)
    {
        $orig = $s;
        $s = preg_replace($re, $to, $s);
        return $orig !== $s;
    }

    function m($s, $re)
    {
        return preg_match($re, $s);
    }

    function stem_word($word) 
    {
        $word = strtolower($word);
        $word = strtr($word, 'ё', 'е'); // замена ё на е, что бы учитывалась как одна и та же буква
        # Check against cache of stemmed words
        if ($this->Stem_Caching && isset($this->Stem_Cache[$word])) {
            return $this->Stem_Cache[$word];
        }
        $stem = $word;
        do {
          if (!preg_match(iconv("utf-8", "cp1251", $this->RVRE), $word, $p)) break;
          $start = $p[1];
          $RV = $p[2];
          if (!$RV) break;

          # Step 1
          if (!$this->s($RV, iconv("utf-8", "cp1251",$this->PERFECTIVEGROUND), '')) {
              $this->s($RV, iconv("utf-8", "cp1251",$this->REFLEXIVE), '');

              if ($this->s($RV, iconv("utf-8", "cp1251",$this->ADJECTIVE), '')) {
                  $this->s($RV, iconv("utf-8", "cp1251",$this->PARTICIPLE), '');
              } else {
                  if (!$this->s($RV, iconv("utf-8", "cp1251",$this->VERB), ''))
                      $this->s($RV, iconv("utf-8", "cp1251",$this->NOUN), '');
              }
          }

          # Step 2
          $this->s($RV, iconv("utf-8", "cp1251",'/и$/'), '');

          # Step 3
          if ($this->m($RV, iconv("utf-8", "cp1251",$this->DERIVATIONAL)))
              $this->s($RV, iconv("utf-8", "cp1251",'/ость?$/'), '');

          # Step 4
          if (!$this->s($RV, iconv("utf-8", "cp1251",'/ь$/'), '')) {
              $this->s($RV, iconv("utf-8", "cp1251",'/ейше?/'), '');
              $this->s($RV, iconv("utf-8", "cp1251",'/нн$/'), iconv("utf-8", "cp1251",'н')); 
          }

          $stem = $start.$RV;
        } while(false);
        if ($this->Stem_Caching) $this->Stem_Cache[$word] = $stem;
        return $stem;
    }
		function getStemString($string) 
    {
			$words = explode(" ", $string);
			foreach($words as $word) 
				$result[] = $this->stem_word($word);
			return implode(" ", $result);
		}
    function stem_caching($parm_ref) 
    {
        $caching_level = @$parm_ref['-level'];
        if ($caching_level) {
            if (!$this->m($caching_level, '/^[012]$/')) {
                die(__CLASS__ . "::stem_caching() - Legal values are '0','1' or '2'. '$caching_level' is not a legal value");
            }
            $this->Stem_Caching = $caching_level;
        }
        return $this->Stem_Caching;
    }

    function clear_stem_cache() 
    {
        $this->Stem_Cache = array();
    }
		function dmword($string, $is_cyrillic = true)
		{	
			static $codes = array(
				'A' =>	array(array(0, -1, -1),
					'I' =>	array(array(0, 1, -1)),
					'J' =>	array(array(0, 1, -1)),
					'Y' =>	array(array(0, 1, -1)),
					'U' =>	array(array(0, 7, -1))),
		 
				'B' =>	array(array(7, 7, 7)),
		 
				'C' =>	array(array(5, 5, 5), array(4, 4, 4),
					'Z' => 	array(array(4, 4, 4),
						'S' =>	array(array(4, 4, 4))),
					'S' =>	array(array(4, 4, 4),
						'Z' =>	array(array(4, 4, 4))),
					'K' =>	array(array(5, 5, 5), array(45, 45, 45)),
					'H' =>	array(array(5, 5, 5), array(4, 4, 4),
						'S' =>	array(array(5, 54, 54)))),
		 
				'D' =>	array(array(3, 3, 3),
					'T' =>	array(array(3, 3, 3)),
					'Z' =>	array(array(4, 4, 4),
						'H' =>	array(array(4, 4, 4)),
						'S' =>	array(array(4, 4, 4))),
					'S' =>	array(array(4, 4, 4),
						'H' =>	array(array(4, 4, 4)),
						'Z' =>	array(array(4, 4, 4))),
					'R' =>	array(
						'S' =>	array(array(4, 4, 4)),
						'Z' =>	array(array(4, 4, 4)))),
		 
				'E' =>	array(array(0, -1, -1),
					'I' =>	array(array(0, 1, -1)),
					'J' =>	array(array(0, 1, -1)),
					'Y' =>	array(array(0, 1, -1)),
					'U' =>	array(array(1, 1, -1))),
		 
				'F' =>	array(array(7, 7, 7),
					'B' =>	array(array(7, 7, 7))),
		 
				'G' =>	array(array(5, 5, 5)),
		 
				'H' =>	array(array(5, 5, -1)),
		 
				'I' =>	array(array(0, -1, -1),
					'A' =>	array(array(1, -1, -1)),
					'E' =>	array(array(1, -1, -1)),
					'O' =>	array(array(1, -1, -1)),
					'U' =>	array(array(1, -1, -1))),
		 
				'J'	=>	array(array(4, 4, 4)),
		 
				'K' =>	array(array(5, 5, 5),
					'H' =>	array(array(5, 5, 5)),
					'S' =>	array(array(5, 54, 54))),
		 
				'L' =>	array(array(8, 8, 8)),
		 
				'M' =>	array(array(6, 6, 6),
					'N' =>	array(array(66, 66, 66))),
		 
				'N' =>	array(array(6, 6, 6),
					'M' =>	array(array(66, 66, 66))),
		 
				'O' =>	array(array(0, -1, -1),
					'I' =>	array(array(0, 1, -1)),
					'J' =>	array(array(0, 1, -1)),
					'Y' =>	array(array(0, 1, -1))),
		 
				'P' =>	array(array(7, 7, 7),
					'F'	=>	array(array(7, 7, 7)),
					'H'	=>	array(array(7, 7, 7))),
		 
				'Q' =>	array(array(5, 5, 5)),
		 
				'R' =>	array(array(9, 9, 9),
					'Z'	=>	array(array(94, 94, 94), array(94, 94, 94)), // special case
					'S' =>	array(array(94, 94, 94), array(94, 94, 94))), // special case
		 
				'S' =>	array(array(4, 4, 4),
					'Z' =>	array(array(4, 4, 4),
						'T' =>	array(array(2, 43, 43)),
						'C' =>	array(
							'Z' => array(array(2, 4, 4)),
							'S' => array(array(2, 4, 4))),
						'D' =>	array(array(2, 43, 43))),
					'D' =>	array(array(2, 43, 43)),
					'T' =>	array(array(2, 43, 43),
						'R'	=>	array(
							'Z' =>	array(array(2, 4, 4)),
							'S' =>	array(array(2, 4, 4))),
						'C' =>	array(
							'H' =>	array(array(2, 4, 4))),
						'S' =>	array(
							'H'	=>	array(array(2, 4, 4)),
							'C' =>	array(
								'H' =>	array(array(2, 4, 4))))),
					'C'	=>	array(array(2, 4, 4),
						'H' =>	array(array(4, 4, 4),
							'T' => array(array(2, 43, 43),
								'S' => array(
									'C' => array(
										'H' =>	array(array(2, 4, 4))),
									'H' => array(array(2, 4, 4))),
								'C' => array(
									'H' =>	array(array(2, 4, 4)))),
							'D' =>	array(array(2, 43, 43)))),
					'H' =>	array(array(4, 4, 4),
						'T'	=>	array(array(2, 43, 43),
							'C' =>	array(
								'H' =>	array(array(2, 4, 4))),
							'S' =>	array(
								'H' =>	array(array(2, 4, 4)))),
						'C'	=>	array(
							'H' =>	array(array(2, 4, 4))),
						'D' =>	array(array(2, 43, 43)))),
		 
				'T' =>	array(array(3, 3, 3),
					'C' =>	array(array(4, 4, 4),
						'H' =>	array(array(4, 4, 4))),
					'Z'	=>	array(array(4, 4, 4),
						'S' =>	array(array(4, 4, 4))),
					'S' =>	array(array(4, 4, 4),
						'Z' =>	array(array(4, 4, 4)),
						'H' =>	array(array(4, 4, 4)),
						'C' =>	array(
							'H' =>	array(array(4, 4, 4)))),
					'T' =>	array(
						'S' =>	array(array(4, 4, 4),
							'Z' =>	array(array(4, 4, 4)),
							'C' =>	array(
								'H' =>	array(array(4, 4, 4)))),
						'C' =>	array(
							'H' =>	array(array(4, 4, 4))),
						'Z' =>	array(array(4, 4, 4))),
					'H' =>	array(array(3, 3, 3)),
					'R' =>	array(
						'Z' =>	array(array(4, 4, 4)),
						'S' =>	array(array(4, 4, 4)))),
		 
				'U' =>	array(array(0, -1, -1),
					'E' =>	array(array(0, -1, -1)),
					'I' =>	array(array(0, 1, -1)),
					'J' =>	array(array(0, 1, -1)),
					'Y' =>	array(array(0, 1, -1))),
		 
				'V' =>	array(array(7, 7, 7)),
		 
				'W' =>	array(array(7, 7, 7)),
		 
				'X' =>	array(array(5, 54, 54)),
		 
				'Y' =>	array(array(1, -1, -1)),
		 
				'Z' =>	array(array(4, 4, 4),
					'D' =>	array(array(2, 43, 43),
						'Z' =>	array(array(2, 4, 4),
							'H' =>	array(array(2, 4, 4)))),
					'H' =>	array(array(4, 4, 4),
						'D' => array(array(2, 43, 43),
							'Z' =>	array(
								'H' =>	array(array(2, 4, 4))))),
					'S' =>	array(array(4, 4, 4),
						'H' =>	array(array(4, 4, 4)),
						'C' =>	array(
							'H' =>	array(array(4, 4, 4))))));
		 
			$length = strlen($string);
			$output = '';
			$i = 0;
		 
			$previous = -1;
		 
			while ($i < $length)
			{		
				$current = $last = &$codes[$string[$i]];
		 
				for ($j = $k = 1; $k < 7; $k++)
				{			
					if (!isset($string[$i + $k]) ||
						!isset($current[$string[$i + $k]]))
						break;
		 
					$current = &$current[$string[$i + $k]];						
		 
					if (isset($current[0]))
					{
						$last = &$current;
						$j = $k + 1;
					}
				}
		 
				if ($i == 0)
					$code = $last[0][0];
				elseif (!isset($string[$i + $j]) || ($codes[$string[$i + $j]][0][0] != 0))
					$code = $is_cyrillic ? (isset($last[1]) ? $last[1][2] : $last[0][2]) : $last[0][2];
				else
					$code = $is_cyrillic ? (isset($last[1]) ? $last[1][1] : $last[0][1]) : $last[0][1];
		 
				if (($code != -1) && ($code != $previous))
					$output .= $code;
		 
				$previous = $code;
		 
				$i += $j;
		 
			}
		 
			return str_pad(substr($output, 0, 6), 6, '0');
		}
		 
		//------------------------------------------------------------------------------
		 
		function getSoundexString($string)
		{
			$is_cyrillic = false;
			$string = iconv("cp1251", "utf-8", $string);
			if (preg_match('#[А-Яа-я]#i', $string) === 1)
			{
				$string = $this->translit($string);
				$is_cyrillic = true;
			}
		 
			$string = preg_replace(array('#[^\w\s]|\d#i', '#\b[^\s]{1,3}\b#i', '#\s{2,}#i', '#^\s+|\s+$#i'),
				array('', '', ' '), strtoupper($string));
		 
			if (!isset($string[0]))
				return null;
		 
			$matches = explode(' ', trim($string));
			foreach($matches as $key => $match)
				$matches[$key] = $this->dmword($match, $is_cyrillic);			
			return implode(" ",$matches);
		}
		
		function getTrueWord($string, $needle) {
			$string = $this->getText($string);
			$words = explode(" ", $string);
			foreach($words as $ind=>$word) {
				$lev = levenshtein($word, $needle);
				$result[$lev] = $word;
			}
			if(!empty($result))
				for($i=0;$i<10;$i++) 
					if(isset($result[$i])) {
						$results[$i] = $result[$i];
						return $results;
					}				
			return false;
		}
		function getTrueString($array, $string) {
			$s_words = explode(" ", $string);
			$j=0;
			$dep = ((count($array)<70) ? count($array) : 70);
			foreach($s_words as $word) {
				for($i=0; $i<$dep; $i++){
					$w = $this->getTrueWord($array[$i]["text"], $word);
					foreach($w as $lev=>$wrd)
						$res[$j][$lev][$wrd][] = $wrd;
				}
				ksort($res[$j]);
				$j++;
			}
			for($i=0;$i<count($s_words);$i++) {
				$result[] = array_shift($res[$i]);
			}
			foreach($result as $id=>$words) {
				if(count($words>1)){
					$n=0;
					foreach($words as $w=>$word) {
						$c = count($word);
						if($n<$c) {
							$n=$c;
							$out[$id]=$w;
						}
					}
				}
			}			
			return $out;
		}
		 
		//------------------------------------------------------------------------------
		 
		function translit($string)
		{
			static $ru = array(
				'А', 'а', 'Б', 'б', 'В', 'в', 'Г', 'г', 'Д', 'д', 'Е', 'е', 'Ё', 'ё', 'Ж', 'ж', 'З', 'з',
				'И', 'и', 'Й', 'й', 'К', 'к', 'Л', 'л', 'М', 'м', 'Н', 'н', 'О', 'о', 'П', 'п', 'Р', 'р',
				'С', 'с', 'Т', 'т', 'У', 'у', 'Ф', 'ф', 'Х', 'х', 'Ц', 'ц', 'Ч', 'ч', 'Ш', 'ш', 'Щ', 'щ',
				'Ъ', 'ъ', 'Ы', 'ы', 'Ь', 'ь', 'Э', 'э', 'Ю', 'ю', 'Я', 'я'
			);
		 
			static $en = array(
				'A', 'a', 'B', 'b', 'V', 'v', 'G', 'g', 'D', 'd', 'E', 'e', 'E', 'e', 'Zh', 'zh', 'Z', 'z', 
				'I', 'i', 'J', 'j', 'K', 'k', 'L', 'l', 'M', 'm', 'N', 'n', 'O', 'o', 'P', 'p', 'R', 'r',
				'S', 's', 'T', 't', 'U', 'u', 'F', 'f', 'H', 'h', 'C', 'c', 'Ch', 'ch', 'Sh', 'sh', 'Sch', 'sch',
				'\'', '\'', 'Y', 'y',  '\'', '\'', 'E', 'e', 'Ju', 'ju', 'Ja', 'ja'
			);
		 
			$string = str_replace($ru, $en, $string);	
			return $string;
		}
		
		function getText($text)
		{
			return mb_strtolower(trim(mb_eregi_replace('![^\w\d\s]*!','',$text)));
		}
		
		function encodeText($text)
		{
			$text = $this->getText($text);
			$code = $this->getSoundexString($this->getStemString($text));
			return $text." | ".$code;
		}
		
		function decodeText($text)
		{
			$text = explode("|", $text);
			$codes = explode(" ", trim($text[1]));
			$real_text = $this->getStemString(trim($text[0]));
			$string = preg_replace(array('#[^\w\s]|\d#i', '#\b[^\s]{1,3}\b#i', '#\s{2,}#i', '#^\s+|\s+$#i'), array('', '', ' '),$real_text);
			$words = explode(" ", trim($string));
			foreach($words as $id=>$word)
				$result[$codes[$id]] = $word;
			return $result;
		}	
		
		function decodeString($string)
		{			
			return $string;
		}
}
?>
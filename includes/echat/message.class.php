<?php
/**
* Message
* ����� ��� �������� ���������
*/
class Message
{
    function __construct($theme_id, $options, $notice)
    {
        $this->theme_id             = $theme_id;
        $this->options = $options;
        $this->notice  = $notice;
    }

    /**
     * ���������� ���������
     * @param  [string]    $message     [���������]
     * @return [json]                   [��������� ��������]
    */
    public function Send_message ($message)
    {
        global $Auth;
        if ($this->Access_to_send_message($Auth->user_id)) 
        {
            global $DB;
            $DB->SetTable("wall_messages");
            $DB->AddValue("time", date("H:i:s") );
            $DB->AddValue("date", date("Y.m.d") );
            $DB->AddValue("theme_id", $this->theme_id);
            $DB->AddValue("user_id", $Auth->user_id);
            $DB->AddValue("message", mb_convert_encoding($message, "windows-1251", "utf-8"));
            if($DB->Insert())
            {
                //��������� �����������
                return array('status' => 'ok', 'message' => $this->notice[9]);
            }
            else
            {
                //�� ������� ��������� ��������� (������ insert)
                return array('status' => 'error', 'message' => $this->notice[10]);
            }
        }
        else
        {
            //��� ���� ��� �������� ���������
            return array('status' => 'error', 'message' => $this->notice[11]);
        }
    }


    /**
     * ������� ���������
     * @param  [string]    $message_id     [���������]
     * @return [json]                   [��������� ��������]
    */
    public function Delete_message ($message_id)
    {
        global $Auth;
        if ($this->Access_to_send_message($Auth->user_id)) 
        {
            global $DB;
            $DB->SetTable("wall_messages");
            $DB->AddCondFS("theme_id", "=", $this->theme_id);
            $DB->AddCondFS("id", "=", $message_id);
            if($DB->Delete())
            {
                //��������� �������
                return array('status' => 'ok', 'message' => $this->notice[26]);
            }
            else
            {
                //�� ������� ��������� ��������� (������ delete)
                return array('status' => 'error', 'message' => $this->notice[27]);
            }
        }
        else
        {
            //��� ���� ��� �������� ���������
            return array('status' => 'error', 'message' => $this->notice[28]);
        }
    }


    /**
     * ���������� ��� ��������� ����
     * @return [json]                   [��������� ��� ������]
    */
    public function Get_messages ($from_id = null)
    {
        global $Auth;
        if ($this->Access_to_get_messages($Auth->user_id)) 
        {
            global $DB;
            $DB->SetTable("wall_messages");
            $DB->AddCondFS("theme_id", "=", $this->theme_id);
            if(!empty($from_id)) {
                $DB->AddCondFS('id', '>', $from_id);
            }
            $DB->AddOrder("date");
            $DB->AddOrder("time");
            $res = $DB->Select();
            while($row = $DB->FetchAssoc($res))
            {
                $people = $this->find_in_people($row['user_id']);
                
                if (str_replace("-", ".", $row['date'])==date("Y.m.d")) 
                {
                   $date = "�������, ";
                }
                else
                {
                    $date = $row['date'];
                }

                $messages[$row['id']] = array (
                    'user_id'=>$row['user_id'], 
                    'id' => $row['id'], 
                    'message' => $row['message'], 
                    'name' => $people['name']." ".$people['last_name'], 
                    'photo' => $people['photo'], 
                    "date" => $date, 
                    "time" => $row['time']
                );

            }
            if($messages)
            {
                return array('status' => 'ok', 'data' => $messages);
            }
        }
        else
        {
            //��� ���� ��� ��������� ��������� ����� ����
            return array('status' => 'error', 'message' => $this->notice[12]);
        }
    }


    /**
     * ���������� ��� ��������� ����
     * @return [json]                   [��������� ��� ������]
    */
    public function Get_messages_count ()
    {
        global $DB;
        $DB->SetTable("wall_messages");
        $DB->AddCondFS("theme_id", "=", $this->theme_id);

        $res = $DB->Select();
        $message_count = 0;
        while($row = $DB->FetchAssoc($res))
        {
            $message_count++;
        }
        return !empty($message_count) ? $message_count : "��������� ���";
    }

    /**
     * ������� ��������� ��������� ����
     * @return [array]             [��������� ��������� � �����������]
    */
    public function Get_last_message ()
    {
        global $Auth;
        if ($this->Access_to_get_messages($Auth->user_id)) 
        {
            global $DB;
            $DB->SetTable("wall_messages");
            $DB->AddCondFS("theme_id", "=", $this->theme_id);
            $DB->AddOrder("date");
            $DB->AddOrder("time");
            $res = $DB->Select();
            while($row = $DB->FetchAssoc($res))
            {
                $message = $row['message'];
                if (str_replace("-", ".", $row['date'])==date("Y.m.d")) 
                {
                   $date = "�������, ".$row['time'];
                }
                else
                {
                    $date = $row['date'].", ".$row['time'];
                }
                $user_id = $row['user_id'];
            }

            if($message)
            {
                $people = $this->find_in_people($user_id);
                $message = array ("message" => $people['name']." ".$people['last_name']." (".$date."): ". mb_convert_encoding($message, "windows-1251", "utf-8"));
                return array('status' => 'ok', 'data' => $message);
            }
            else
            {
                $message = array ("message" => "��� ��������� � ����");
                return array('status' => 'ok', 'data' => $message);
            }
        }
        else
        {
            //��� ���� ��� ��������� ��������� ����� ����
            return array('status' => 'error', 'message' => $this->notice[12]);
        }
    }

    /**
     * ���� �������� � people
     * @param  [int]    $user_id          [auth id ������������]
     * @return [array]                    [������ ������������ �� people]
    */
    public function find_in_people ($user_id)
    {
        global $DB;
        $DB->SetTable("nsau_people");
        $DB->AddCondFS("user_id", "=", $user_id);
        $res = $DB->Select(1);
        $people = $DB->FetchAssoc($res);
        $result = array(
            'people_id'=>$people['id'],
            'last_name'=>$people['last_name'],
            'name'=>$people['name'],
            'patronymic'=>$people['patronymic'],
            'photo'=>$people['photo']
        );
        return $result;
    }

    /**
     * ��������� ����� ��� �������� ��������� � ���
     * @param  [int]    $user_id    [auth_id ������������]
     * @return [bool]               [true / false]
    */
    public function Access_to_send_message ($user_id)
    {
        global $DB;

        $DB->SetTable("wall_themes_users");
        $DB->AddCondFS("user_id",   "=", $user_id);
        $DB->AddCondFS("theme_id",  "=", $this->theme_id);
        $res = $DB->FetchAssoc($DB->Select(1));
        if (!empty($res)) 
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * ��������� ����� ��� ��������� ��������� ����
     * @param  [int]    $user_id    [auth_id ������������]
     * @return [bool]               [true / false]
    */
    public function Access_to_get_messages ($user_id)
    {
        global $DB;

        $DB->SetTable("wall_themes_users");
        $DB->AddCondFS("user_id", "=", $user_id);
        $res = $DB->FetchAssoc($DB->Select(1));
        if ($res) 
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
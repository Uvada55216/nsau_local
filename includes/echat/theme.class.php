<?php
/**
* Theme
* ����� ��� ������ � �����
*/
class Theme 
{
    function __construct($theme_id=NULL, $options, $notice)
    {
        if ($theme_id) 
        {
            global $DB;
            $DB->SetTable("wall_themes");
            $DB->AddCondFS("id", "=", $theme_id);
            $res = $DB->Select(1);
            $theme_data = $DB->FetchAssoc($res);

            $this->theme_id             = $theme_id;
            $this->theme_creator_id     = $theme_data['creator_id'];
            $this->theme_name           = $theme_data['name'];
            $this->theme_description    = !empty($theme_data['description']) ? $theme_data['description'] : "�������� �����������";
        }
            $this->options = $options;
            $this->notice  = $notice;
    }

    /**
     * ���������� ���������� � ��������� ����
     * @return [array]  [������ ���������������� ������]
    */
    public function Get_theme_creator ()
    {
        global $DB;
        $DB->SetTable("auth_users");
        $DB->AddField("id");
        $DB->AddField("is_active");
        $DB->AddField("displayed_name");
        $DB->AddField("usergroup_id");
        $DB->AddCondFS("id", "=", $this->theme_creator_id);
        $res = $DB->Select(1);
        $auth = $DB->FetchAssoc($res);
        $DB->SetTable("nsau_people");
        $DB->AddField("id");
        $DB->AddCondFS("user_id", "=", $this->theme_creator_id);
        $res = $DB->Select(1);
        $people = $DB->FetchAssoc($res);
        $creator = array(
            'auth_id' => $auth['id'], 
            'people_id'=>$people['id'],
            'is_active'=>$auth['is_active'],
            'displayed_name'=>$auth['displayed_name'],
            'usergroup_id'=>$auth['usergroup_id']
        );
        return $creator;
    }

    /**
     * ���������� �������� ����
     * @return [string] [�������� ����]
    */
    public function Get_theme_name ()
    {
        return $this->theme_name;
    }

    /**
     * ���������� �������� ����
     * @return [string] [�������� ����]
    */
    public function Get_theme_description ()
    {
        return $this->theme_description;
    }

    /**
     * ���������� ������������� ����
     * @param  [mode]    count / list      [count - ���������� ���������� �������������, list - ������ � ������� �������������]
     * @return [int]    [���������� �������������]
     * @return [array]  [������ �������������]
    */
    public function Get_theme_users ($mode)
    {
        global $DB;
        $DB->SetTable("wall_themes_users");
        $DB->AddCondFS("theme_id", "=", $this->theme_id);
        $res = $DB->Select();
        if ($mode=="count") 
        {
            $user_count = 0;
            while($row = $DB->FetchAssoc($res))
            {
                $user_count++;
            }
            return $user_count;
        }
        elseif ($mode=="list")
        {
            $user_count = 0;
            while($row = $DB->FetchAssoc($res))
            {
                $in_people = $this->find_in_people($row['user_id']);
                $users[$row['user_id']] = array('status' => $row['status'], 'displayed_name' => $in_people['last_name']." ".$in_people['name']." ".$in_people['patronymic'], 'photo' => $in_people['photo']);
            }
            return $users;
        }
    }  

    /**
     * ������� ����� ����
     * @param  [string]    $theme_name      [�������� ����� ����]
     * @return [json]                       [��������� ��������]
    */
    public function Create_new_theme ($theme_name, $theme_description=NULL)
    {
        global $DB, $Auth, $Engine;
        if ($this->Access_to_theme_create($Auth->user_id)) 
        {
            if (!empty($theme_name))
            {
                $DB->SetTable("wall_themes");
                $DB->AddValue("creator_id", $Auth->user_id);
                $DB->AddValue("name", mb_convert_encoding($theme_name, "windows-1251", "utf-8"));
                $DB->AddValue("description", mb_convert_encoding($theme_description, "windows-1251", "utf-8"));
                if($DB->Insert())
                {
                    $new_theme_id = $DB->LastInsertID();
                    $DB->SetTable("wall_themes_users");
                    $DB->AddValue("theme_id", $new_theme_id);
                    $DB->AddValue("user_id", $Auth->user_id);
                    $DB->AddValue("status", 1);
                    if($DB->Insert())
                    {
                        //���� �������
                        $Engine->LogAction(52, "theme", $DB->LastInsertID(), "create");
                        return array('status' => 'ok', 'message' => $this->notice[1]);
                        
                    }
                    else
                    {
                        //������, ��������� ������� ���� (������ inserta)
                        return array('status' => 'error', 'message' => $this->notice[2]);
                    }
                }
                else
                {
                    //������, ��������� ������� ���� (������ inserta)
                    return array('status' => 'error', 'message' => $this->notice[2]);
                }
            }
            else
            {
                //������, ������ ��������
                return array('status' => 'error', 'message' => $this->notice[4]);
            }
        }
        else
        {
            //������, �������� ���� ��������� / ������� ������������ ���������� ���
            return array('status' => 'error', 'message' => $this->notice[3]);
        }
    }

    /**
     * ������� ����
     * @return [json]                       [��������� ��������]
    */
    public function Delete_theme ()
    {
        global $DB, $Auth, $Engine;
        if ($this->Is_creator($this->theme_id)) 
        {
            //������� ����
            $DB->SetTable("wall_themes");
            $DB->AddCondFS("id", "=", $this->theme_id);
            if($DB->Delete())
            {
                //������� ������������� ����
                $DB->SetTable("wall_themes_users");
                $DB->AddCondFS("theme_id", "=", $this->theme_id);
                if($DB->Delete())
                {
                    //������� ��������� ����
                    $DB->SetTable("wall_messages");
                    $DB->AddCondFS("theme_id", "=", $this->theme_id);
                    if($DB->Delete())
                    {
                        //���� �������
                        $Engine->LogAction(52, "theme", $this->theme_id, "delete");
                        return array('status' => 'ok', 'message' => $this->notice[7]);
                    }
                    else
                    {
                        //������, ��������� ������� ���� (������ delete)
                        return array('status' => 'error', 'message' => $this->notice[6]);     
                    }
                }
                else
                {
                    //������, ��������� ������� ���� (������ delete)
                    return array('status' => 'error', 'message' => $this->notice[6]);         
                }
            }
            else
            {
                //������, ��������� ������� ���� (������ delete)
                return array('status' => 'error', 'message' => $this->notice[6]);
            }
        }
        else
        {
            //������, �������� ���� ���������
            return array('status' => 'error', 'message' => $this->notice[5]);
        }
    }

    /**
     * ��������� ������������ � ���
     * @param  [int]    $user_id    [auth_id ������������]
     * @return [string]             [��������� ����������]
    */
    public function Invite_user ($user_id)
    {
        global $DB, $Auth, $Engine;
        if ($this->Is_creator($this->theme_id)) 
        {
            if (!empty($user_id))
            {
                $DB->SetTable("wall_themes_users");
                $DB->AddCondFS("theme_id", "=", $this->theme_id);
                $DB->AddCondFS("user_id", "=", $user_id);
                $row = $DB->FetchAssoc($DB->Select(1));
                if ($row) 
                {
                    //������, ������������ ��� ��������
                    return array('status' => 'error', 'message' => $this->notice[14]);
                }
                else
                {
                    $DB->SetTable("wall_themes_users");
                    $DB->AddValue("theme_id", $this->theme_id);
                    $DB->AddValue("user_id", $user_id);
                    $DB->AddValue("status", 3);
                    if($DB->Insert())
                    {
                        //������������ ��������
                        $Engine->LogAction(52, "user", $user_id, "invite");
                        return array('status' => 'ok', 'message' => $this->notice[13]);
                    }
                    else
                    {
                        //������, �������� ������������ (������ inserta)
                        return array('status' => 'error', 'message' => $this->notice[15]);
                    }
                }
            }
        }
        else
        {
            //������, �� ��������� ���������� ������������� (�� ��������� ����)
            return array('status' => 'error', 'message' => $this->notice[16]);
        }
    }

    /**
     * ������� ������������ �� ����
     * @param  [int]    $user_id    [auth_id ������������]
     * @return [array]              [��������� ����������]
    */
    public function Delete_user ($user_id)
    {
        global $DB, $Auth, $Engine;
        if ($this->Is_creator($this->theme_id)) 
        {
            if ($Auth->user_id != $user_id) 
            {
                $DB->SetTable("wall_themes_users");
                $DB->AddCondFS("theme_id", "=", $this->theme_id);
                $DB->AddCondFS("user_id", "=", $user_id);
                if($DB->Delete())
                {
                    //������������ ������
                    $Engine->LogAction(52, "user", $user_id, "delete");
                    return array('status' => 'ok', 'message' => $this->notice[20]);
                }
                else
                {
                    //������ �������� ������������ (������ deleta)
                    return array('status' => 'error', 'message' => $this->notice[21]);
                }
            }
            else
            {
                //������, �� �� ������ ����� �� ����� ����
                return array('status' => 'error', 'message' => $this->notice[18]);
            }
        }
        else
        {
            if ($Auth->user_id != $user_id) 
            {
                //������, �� ��������� �������� ������������� (�� ��������� ����)
                return array('status' => 'error', 'message' => $this->notice[19]);
            }
            else
            {
                $DB->SetTable("wall_themes_users");
                $DB->AddCondFS("theme_id", "=", $this->theme_id);
                $DB->AddCondFS("user_id", "=", $user_id);
                if($DB->Delete())
                {
                    //������������ ������
                    $Engine->LogAction(52, "user", $user_id, "out");
                    return array('status' => 'ok', 'message' => 'leave_chat');
                }
                else
                {
                    //������ �������� ������������ (������ deleta)
                    return array('status' => 'error', 'message' => $this->notice[21]);
                }
            }
        }
    }

    /**
     * ��������������� ���� � � ��������
     * @param  [string]    $theme_name          [�������� ����]
     * @param  [string]    $theme_description   [�������� ����]
     * @return [array]                          [��������� ��������������]
    */
    public function Change_theme_name ($theme_name, $theme_description=NULL)
    {
        global $DB, $Auth, $Engine;
        if ($this->Is_creator($this->theme_id)) 
        {
            if (!empty($theme_name)) 
            {
                $DB->SetTable("wall_themes");
                $DB->AddCondFS("id", "=", $this->theme_id);
                $DB->AddValue("name", mb_convert_encoding($theme_name, "windows-1251", "utf-8"));
                $DB->AddValue("description", mb_convert_encoding($theme_description, "windows-1251", "utf-8"));
                if($DB->Update())
                {
                    //��������� ���������
                    $Engine->LogAction(52, "theme", $this->theme_id, "edit");
                    return array('status' => 'ok', 'message' => $this->notice[22]);
                }
                else
                {
                    //������, ��������� �������� ��������� (������ update)
                    return array('status' => 'error', 'message' => $this->notice[23]);
                }
            }
            else
            {
                //������, ���� �� ����� ���� �������
                return array('status' => 'error', 'message' => $this->notice[25]);
            }
        }
        else
        {
            //������, � ���� ��� ���� �������� ���������
            return array('status' => 'error', 'message' => $this->notice[24]);
        }
    }

    /**
     * ���� �������� � people
     * @param  [int]    $user_id          [auth id ������������]
     * @return [array]                    [������ ������������ �� people]
    */
    public function find_in_people ($user_id)
    {
        global $DB;
        $DB->SetTable("nsau_people");
        $DB->AddCondFS("user_id", "=", $user_id);
        $res = $DB->Select(1);
        $people = $DB->FetchAssoc($res);
        $result = array(
            'people_id'=>$people['id'],
            'last_name'=>$people['last_name'],
            'name'=>$people['name'],
            'patronymic'=>$people['patronymic'],
            'photo'=>$people['photo']
        );
        return $result;
    }

    /**
     * ��������� ����� ��� �������� ����� ����
     * @param  [int]    $user_id    [auth_id ������������]
     * @return [bool]               [true / false]
    */
    public function Access_to_theme_create ($user_id)
    {
        global $DB;
        $DB->SetTable("auth_users");
        $DB->AddCondFS("id", "=", $user_id);
        $res = $DB->Select(1);
        $in_auth = $DB->FetchAssoc($res);

        $DB->SetTable("wall_themes");
        $DB->AddCondFS("creator_id", "=", $user_id);
        $res = $DB->Select();
        while($row = $DB->FetchAssoc($res)) 
            $i++;

        switch ($in_auth['usergroup_id']) 
        {
            /*��������������� ����� ��������� ����� ���������� ���*/
            case 1:
            return true;
            break;
            /*��������*/
            case 2:
            {
                if ($i>$this->options['themes_limit_teachers']) 
                {
                    return false;
                }
            }
            break;
            /*���������*/
            case 3:
            {
                if ($i>$this->options['themes_limit_stud']) 
                {
                    return false;
                }
            }
            break;
            /*������*/
            default:
            if ($i>$this->options['themes_limit_other']) 
            {
                return false;
            }
            break;
        }
        return true;
    }

    /**
     * ��������� ����������� �� ���� �������� ������������
     * @param  [int]    $user_id    [id ����]
     * @return [bool]               [true / false]
    */
    public function Is_creator ($theme_id)
    {
        global $Auth;
        $theme = $this->Get_theme_creator();
        if ($theme['auth_id']==$Auth->user_id) 
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
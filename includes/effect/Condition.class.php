<?
/**
* ��������� ������������� ������, � ��� ����� ������ 
*	Formula->parsePay($data)
*	Formula->parseData($data)
*/
require_once INCLUDES . "/effect/" . "Formula" . CLASS_EXT ;

/**
* ��������� ��������� ���������� �������� � ���������/�������� ���������
*	��������� ����������
*	��� ��������� ������������ eval. ��� ��� ��������� ������ ������� ������ ���� ���������
* ������ ���������������. 
* ������������� ������������ ������ ����������� ��� ������������� ������ �
* ����� getResult() ��� ��������� ��������� ����������.
*/
class Condition extends Formula {
	/**
	* �������� � ������������ 
	*/
	private $cond;
	private $formula;
	private $post_id;
	private $val;
	
	/**
	* bool $result  �������� ���������
	*/
	private $result;
	
	/**
	* ��� ��������� ��� ������ �������� � ������������
	*	@param string $formula �������. c-��������� ��������(�� ��), f0-f10 - ���� �� �����, p - id ���������
	* �������� f0>c[p] -> �������� �� ������� ���� �������� ������ ��� ����������� �������� �� �� ��� ����� � ������������ ����������
	* �������� f0==c -> �������� �� ������� ���� �������� ����� ������������ �������� ��� ���� ����������
	*	@param string $cond ��������� �������� �������� (������� �� ��). ����� ��� �������.
	* ���� ������ � ���� �������, ������� ����������� � ������.
	* �������� 90 => 90
	* �������� 90% => 0,9
	* �������� 1.2 => 1.2
	* �������� 1-3:4%;4:4 => array(1=>0.4, 2=>0.4, 3=>0.4, 4=>4)
	* @param array $val ���� �������� �������������, ���� ���� ���������� �������������� ��������� �������������
	* �������� array(0=>$value, 1=>$value1)
	* ���� ������������ ������ 0-�� ������, ��������� �� ������������� �� ��������������
	* @param int $post_id �� ���������, �� �� [p] � �������
	* �� ������ ������ id ���������� �������� � ������� posts
	* @return void
	*/
	public function __construct($formula, $cond, $val, $post_id) {
		$this->cond = $this->parsePay($cond); 
		$this->val = $this->parseData($val);
		$this->post_id = $post_id;
		$this->formula = $this->parseFormula($formula);
		$this->condition();
	}
	
	/**
	* ������ �� ������� php ������
	*	@param string $f �������. �������� ������� �������� � ������������
	* @return string  ����� php ���� ��� ����������
	*/
	private function parseFormula($f) {
		for($i=0;$i<10;$i++) {
			$f = str_replace("f".$i, 'round($this->val["'.$i.'"],2)', $f); 
		}
		
		if(strpos($f, "c[p]")===false) {
			$f = str_replace("c", 'round(c,2)', $f); 
		}
		else {
			$f = str_replace("c[p]", 'round(c[p],2)', $f); 
		}
			
		$f = str_replace("c", '$this->cond', $f);
		$f = str_replace("p", '$this->post_id', $f);
		if(!empty($f)) {
			$f = 'if('.$f.') $this->result = 1; else $this->result = 0;';
		}
		return $f;
	}	
	
	/**
	* ��������� ����������� � Condition->parseFormula($f) ���.
	* @return void
	*/
	private function condition() {
			eval($this->formula);		
	}
	
	
	/**
	* ��������� ��������.
	* @return float
	*/
	public function getCond() {
		return $this->cond;
	}
	
	/**
	* ����������� ��������.
	* @return float
	*/
	public function getVal() {
		return $this->val;
	}	
	
	
	/**
	* ���������� �������� ���������.
	* @return bool
	*/
	public function getResult() {
		// var_dump($this->result);
		return $this->result;
	}

}
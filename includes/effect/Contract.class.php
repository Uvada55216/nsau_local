<?
require_once INCLUDES . "/effect/" . "Dates" . CLASS_EXT ;

class Contract extends Dates{
	
	protected $contract_id;
	public $output;
	
	
	public function __construct($contract_id) {	
		$this->contract_id = $contract_id;	
		parent::__construct();
	}
	
	public function checkEditAccessBool($contract_id) {
		global $EFF, $Auth, $Engine;
		if(($this->getPeriodId()!=$this->getContractPeriodId($contract_id)) 
			&& !$Engine->OperationAllowed(34, "contracts.without.period.handle", -1, $Auth->user_id)) {
			return false;
		}
		else return true;
	}
	
	public function checkEditAccess($contract_id) {
		if(!$this->checkEditAccessBool($contract_id)) 
			return false;
		else return true;
	}
	
	public function checkCEditAccess($contract_id) {
		global $EFF, $Auth, $Engine, $DB;
		$EFF->SetTable("effect_contracts");
		$EFF->AddCondFS("id", "=", $contract_id);
		$EFF->AddField("department_id");
		$d = $EFF->FetchAssoc($EFF->Select(1));
		

		$DB->SetTable("nsau_people", "np");
		$DB->AddCondFS("np.user_id", "=", $Auth->user_id);
		$DB->AddTable("nsau_teachers", "nt");
		$DB->AddCondFF("np.id", "=", "nt.people_id");
		$DB->AddField("nt.department_id", "d_id");
		$DB->AddField("np.status_id", "status_id");
		$this_teacher = $DB->FetchAssoc($DB->Select(1));
		
		$this_teacher_deps = explode(";", $this_teacher["d_id"]);
		$dismissed_handle = false;
		if($Engine->OperationAllowed(34, "contracts.dismissed.owndep.handle", -1, $Auth->usergroup_id) && (in_array($d["department_id"], $this_teacher_deps))) {
			$dismissed_handle = true;
		}
		
		
		$EFF->SetTable("effect_contracts");
		$EFF->AddCondFS("id", "=", $contract_id);
		if(($Auth->usergroup_id != 1) && !$dismissed_handle && !$Engine->OperationAllowed(34, "contracts.handle", $Auth->user_id, $Auth->usergroup_id) && !$Engine->OperationAllowed(34, "contracts.dismissed.handle", $d["department_id"], $Auth->usergroup_id))
			$EFF->AddCondFS("ep.user_id", "=", $Auth->user_id);
		$ct = $EFF->FetchAssoc($EFF->Select(1));
		if(empty($ct)) {
			return false;
		} else return true;
	}
	
	public function checkViewAccess($contract_id) {
		global $EFF, $Auth, $Engine, $DB;
		$EFF->SetTable("effect_contracts");
		$EFF->AddCondFS("id", "=", $contract_id);
		$EFF->AddField("department_id");		
		$d = $EFF->FetchAssoc($EFF->Select(1));
	
		$DB->SetTable("nsau_people", "np");
		$DB->AddCondFS("np.user_id", "=", $Auth->user_id);
		$DB->AddTable("nsau_teachers", "nt");
		$DB->AddCondFF("np.id", "=", "nt.people_id");
		$DB->AddField("nt.department_id", "d_id");
		$DB->AddField("np.status_id", "status_id");
		$this_teacher = $DB->FetchAssoc($DB->Select(1));
		
		$this_teacher_deps = explode(";", $this_teacher["d_id"]);
		$dismissed_handle = false;
		if($Engine->OperationAllowed(34, "contracts.dismissed.owndep.handle", -1, $Auth->usergroup_id) && (in_array($d["department_id"], $this_teacher_deps))) {
			$dismissed_handle = true;
		}

	
		$EFF->SetTable("effect_contracts");
		$EFF->AddCondFS("id", "=", $contract_id);
		if(($Auth->usergroup_id != 1) && !$dismissed_handle && !$Engine->OperationAllowed(34, "contracts.handle", $Auth->user_id, $Auth->usergroup_id) && !$Engine->OperationAllowed(34, "contracts.dismissed.handle", $d["department_id"], $Auth->usergroup_id))
			$EFF->AddCondFS("ep.user_id", "=", $Auth->user_id);
		$ct = $EFF->FetchAssoc($EFF->Select(1));
		if(empty($ct)) 
			return false;
		 else return true;
	}
	
	
	public function getContractPeriodId($id){
			global $EFF;
		$EFF->SetTable("effect_contracts");
		$EFF->AddCondFS("id", "=", intval($id));
		$EFF->AddField("period_id");
		$row = $EFF->FetchAssoc($EFF->Select(1));
		return $row["period_id"];			
	}
	
	public function getContractRate($id){
			global $EFF;
		$EFF->SetTable("effect_contracts");
		$EFF->AddCondFS("id", "=", intval($id));
		$EFF->AddField("rate");
		$row = $EFF->FetchAssoc($EFF->Select(1));
		return $row["rate"];			
	}
	
	public function getPlanPeriodId($id){
		global $EFF;
		$EFF->SetTable("effect_plan");
		$EFF->AddCondFS("id", "=", intval($id));
		$EFF->AddField("contract_id");
		$row = $EFF->FetchAssoc($EFF->Select(1));
		return $this->getContractPeriodId($row["contract_id"]);
	}
	
	public function getIndicatorPeriodId($id){
			global $EFF;
		$EFF->SetTable("effect_indicators");
		$EFF->AddCondFS("id", "=", intval($id));
		$EFF->AddField("plan_id");
		$row = $EFF->FetchAssoc($EFF->Select(1));
		return $this->getPlanPeriodId($row["plan_id"]);
	}
	
	public function getIdByPlan($id){
			global $EFF;
		$EFF->SetTable("effect_plan");
		$EFF->AddCondFS("id", "=", intval($id));
		$EFF->AddField("contract_id");
		$row = $EFF->FetchAssoc($EFF->Select(1));
		return $row["contract_id"];
	}
	
	public function getIdByIndicator($id){
			global $EFF;
		$EFF->SetTable("effect_indicators");
		$EFF->AddCondFS("id", "=", intval($id));
		$EFF->AddField("plan_id");
		$row = $EFF->FetchAssoc($EFF->Select(1));
		return $this->getIdByPlan($row["plan_id"]);
	}
	
	public function deleteContract($id) {
			global $EFF, $Engine;
		$EFF->SetTable("effect_plan");
		$EFF->AddCondFS("contract_id", "=", $id);
		$ep_res = $EFF->Select();
		
		while($ep = $EFF->FetchAssoc($ep_res)) {
			$EFF->SetTable("effect_indicators");
			$EFF->AddCondFS("plan_id", "=", $ep["id"]);
			$EFF->Delete();						
			$Engine->LogAction($this->module_id, "plan", $ep["id"], "delete_with_contract");								
		}
		
		$EFF->SetTable("effect_plan");
		$EFF->AddCondFS("contract_id", "=", $id);
		$EFF->Delete();		
		
		$EFF->SetTable("effect_contracts");
		$EFF->AddCondFS("id", "=", $id);
		$EFF->Delete();
		
		$Engine->LogAction($this->module_id, "contract", $id, "delete");
		CF::Redirect("/office/effect/");
	}
	
	public function changeRate($id, $val) {
			global $EFF;
		$EFF->SetTable("effect_contracts");
		$EFF->AddCondFS("id", "=", $id);
		$EFF->AddValue("rate", $val);
		$EFF->Update();
		$this->output["json"] = "success";
	}
	
	public function changePps($id, $val) {
			global $EFF;
		$EFF->SetTable("effect_contracts");
		$EFF->AddCondFS("id", "=", $id);
		$EFF->AddValue("pps", $val);
		$EFF->Update();
		$this->output["json"] = "success";
	}
	
	public function loadContracts($user_id = null) {
		global $Auth, $EFF;
		if(empty($user_id))
			$user_id = $Auth->user_id;
		$EFF->SetTable("effect_contracts");
		$EFF->AddCondFS("user_id", "=", $user_id);
		$res = $EFF->Select();
		while($row = $EFF->FetchAssoc($res)) {
			$row["date_from"] = $this->getFromDate($row["period_id"]);
			$row["date_to"] = $this->getToDate($row["period_id"]);
			$row["edit_access"] = $this->checkEditAccessBool($row["id"]);
			$effect[$row["id"]] = $row;
		}
		return $effect;
	}
	
	public function addContract($array, $user_id = null) {
		global $EFF, $Auth, $Engine;
		$period_id = $this->getPeriodId();
		if(empty($period_id)) {			
			$this->output["messages"]["bad"][] = 405;
			return false;
		}
		
		if(empty($user_id)) {
			$user_id = $Auth->user_id;
		}
		$EFF->SetTable("effect_contracts");
		$EFF->AddCondFS("user_id", "=", $user_id);
		$EFF->AddCondFS("post_id", "=", $array["post_id"]);
		$EFF->AddCondFS("department_id", "=", $array["department_id"]);
		$EFF->AddCondFS("period_id", "=", $this->getPeriodId());
		$row = $EFF->FetchAssoc($EFF->Select(1));
		if(!empty($row)) {
			$this->output["messages"]["bad"][] = 102;
			return false;
		}
		$EFF->AddTable("effect_contracts");
		$EFF->AddValue("user_id", $user_id);   
		$EFF->AddValue("rate", $array["rate"]);
		$EFF->AddValue("department_id", $array["department_id"]);
		$EFF->AddValue("post_id", $array["post_id"]);
		$EFF->AddValue("pps", $array["pps"]);
		$EFF->AddValue("period_id", $this->getPeriodId());
		$EFF->Insert();
		$contract_id = $EFF->LastInsertID();
		
		$EFF->SetTable("indicators");
		$EFF->AddCondFS("post_id", "LIKE", "%".$array["post_id"]."%");
		$EFF->AddCondFS("ind_type", "=", 1);
		$EFF->AddField("id");
		$indr = $EFF->Select();
		while($ind = $EFF->FetchAssoc($indr)) {
			$EFF->SetTable("effect_plan");
			$EFF->AddValue("user_id", $user_id);
			$EFF->AddValue("contract_id", $contract_id);
			$EFF->AddValue("indicator_id", $ind["id"]);
			$EFF->Insert();
		}
		
		
		$Engine->LogAction($this->module_id, "contract", $EFF->LastInsertID(), "add");
		return true;
	}	
}
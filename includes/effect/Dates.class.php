<?
class Dates {
	private $period_id;
	private $dates;
	
	public function __construct() {
			global $EFF;
		$EFF->SetTable("effect_periods");
		$EFF->AddCondFS("from", "<=", date("Y-m-d H:i:s"));
		$EFF->AddCondFS("to", ">=", date("Y-m-d H:i:s"));
		$EFF->AddField("id");
		$rowdate = $EFF->FetchAssoc($EFF->Select());
		$this->period_id = $rowdate["id"];
	}
	
	public function getPeriodId() {
		return $this->period_id;
	}
	
	public function getPeriodDates($id) {
			global $EFF;
		$EFF->SetTable("effect_periods");
		$EFF->AddCondFS("id", "=", $id);
		$this->dates = $EFF->FetchAssoc($EFF->Select(1));
	}
	
	public function getFactFromDate($id) {
		$this->getPeriodDates($id);
		return $this->dateFormat($this->dates["from"]);
	}

	public function getFactToDate($id) {
		$this->getPeriodDates($id);
		return $this->dateFormat($this->dates["to"]);
	}

	public function getFromDate($id) {
		$this->getPeriodDates($id);
		return $this->dateFormat($this->dates["date_from"]);
	}	
	
	public function getToDate($id) {
		$this->getPeriodDates($id);
		return $this->dateFormat($this->dates["date_to"]);
	}
	
	public function getYearId($id) {
		$this->getPeriodDates($id);
		return $this->dates["year_id"];
	}
	
	public function getYearFromDate($id) {
			global $EFF;
		$this->getYearId($id);
		$EFF->SetTable("effect_periods");
		$EFF->AddCondFS("year_id", "=", $this->dates["year_id"]);
		$EFF->AddOrder("date_from");
		$EFF->AddField("date_from");
		$r = $EFF->FetchAssoc($EFF->Select(1));
		return $this->dateFormat($r["date_from"]);
	}
	
	public function getYearToDate($id) {
			global $EFF;
		$this->getYearId($id);
		$EFF->SetTable("effect_periods");
		$EFF->AddCondFS("year_id", "=", $this->dates["year_id"]);
		$EFF->AddOrder("date_to", true);
		$EFF->AddField("date_to");
		$r = $EFF->FetchAssoc($EFF->Select(1));
		return $this->dateFormat($r["date_to"]);
	}
	
	
	public function getPeriods() {
			global $EFF;
		$EFF->SetTable("effect_periods");
		$resper = $EFF->Select();
		while($rowper = $EFF->FetchAssoc($resper)) {
			$rowper["date_from"] = $this->getFromDate($rowper["id"]);
			$rowper["date_to"] = $this->getToDate($rowper["id"]);
			$rowper["from"] = $this->getFactFromDate($rowper["id"]);
			$rowper["to"] = $this->getFactToDate($rowper["id"]);
			$result[$rowper["id"]] = $rowper;				
		}
		return $result;
	}
	
	public function dateFormat($d) {
		$datetime = explode(" ", $d);
		$date = explode("-", $datetime[0]);
		$time = explode(":", $datetime[1]);
		return date("d.m.Y", mktime($time[0] ? $time[0] : 0, $time[1] ? $time[1] : 0, $time[2] ? $time[2] : 0, $date[1], $date[2], $date[0]));
	}
}
<?
/**
* ��������� ������� ������ �� ������������� �����������
*	��� ������� ������������ eval. ��� ��� ��������� ������ ������� ������ ���� ���������
* ������ ���������������. 
* ������������� ������������ ������ ����������� ��� ������������� ������ �
* ����� getPayment() ��� ��������� ����� ������� �� ��������.
*/
class Formula {
	
	/**
	* �������� � ������������ 
	*/
	private $formula;
	private $pay;
	private $rate;
	private $data;	
	
	/**
	* bool $result  �������� ���������
	*/
	private $payments;
	
	/**
	* ��� ��������� ��� ������ �������� � ������������
	*	@param string $formula �������. r-������, f0-f10 - ���� �� �����, p - ������� �� ����������
	* �������� p[f0]*r -> ������� � ������������ �� ��������� �� 1 ����*������
	* �������� f2*r*p -> �������� 3-��� ����*������*������� �� ����������
	* �������� p -> ������� �� ����������
	* @param array $data �� ������� effect_indicators
	* �������� array(0=>$value, 1=>$value1)
	*	@param string $pay ��������� �������� �������� (������� �� ��). �����.
	* ���� ������ � ���� �������, ������� ����������� � ������.
	* �������� 900 => 900
	* �������� 1-3:1000;4:6900 => array(1=>1000, 2=>1000, 3=>1000, 4=>6900)
	* @param float $rate ������ �� ���������
	* @return void
	*/
	public function __construct($formula, $data, $pay, $rate) {
		$this->pay = $pay;	
		$this->rate = $rate;	
		$this->pay = $this->parsePay($this->pay);
		$this->data = $this->parseData($data);
		$this->formula = $this->parseFormula($formula);
		$this->countPayments();
	}
	
	/**
	* ��������� ����������� � Formula->parseFormula($f) ���.
	* @return void
	*/
	private function countPayments() {
		if(!empty($this->formula))
			eval($this->formula);		
	}
	
	/**
	* ��������� $pay �� ������ ������� ������ ��� ���� ��������.
	* @return bool
	*/
	protected function payIsArray($pay) {
		if(strpos($pay, ":")===false) 
			return false;
		else return true;			
	}
	
	/**
	* �������� � �������� ���� ������ �� ������������ �������� �� �������
	* effect_idicators
	* @return array
	*/
	protected function parseData($data) {
		foreach($data as $f_id => $val){
			if(strpos($val, "%")===false) {
				$result[$f_id] = (float)$val;
			} else {				
				$result[$f_id] = $val = (float)str_replace("%", "", $val)/100;
			}
			if(strpos($val, ",")===false)
				$result[$f_id] = (float)$val;
			else {				
				$result[$f_id] = $val = (float)str_replace(",", ".", $val);
			}
			if(strpos($val, "/")===false)
				$result[$f_id] = (float)$val;
			else {				
				$result[$f_id] = (float)$val = 0;
			}
		}
		return $result;
	}
	
	/**
	* ������ �� ������� php ������
	*	@param string $f �������. �������� ������� �������� � ������������
	* @return string  ����� php ���� ��� ����������
	*/
	private function parseFormula($f) {
		for($i=0;$i<10;$i++) {
			$f = str_replace("f".$i, '$this->data["text'.$i.'"]', $f); 
		}
		$f = str_replace("p", '$this->pay', $f);
		$f = str_replace("r", '$this->rate', $f);
		if(!empty($f))
			$f = '$this->payments = '.$f.";";
		return $f;
	}	
	
	/**
	* ��������� �������� � ���������� ����� 67% => 0.67
	*	@param string $val �������. �������� 67%
	* @return float
	*/
	private function procValToNum($val) {
		if(strpos($val, "%")===false) {
			$result = $val;
		} else {				
			$result = (int)str_replace("%", "", $val)/100;
		}
		return $result;
	}

	/**
	* �������� �� ������ ������� �� ���������� ���� ��� �������� � ���� �������.
	* ���� ������� ������ �����, �� ��� � ����������.
	*	@param string $pay. $pay - �������� �������� � ������������
	* @return array|string � ����������� �� ����������� ������
	*/
	protected function parsePay($pay) {
		if($this->payIsArray($pay)) {
			$i1 = explode(";", $pay);
			foreach($i1 as $id => $i2) {
				$i2 = explode(":", $i2);
				$i3 = explode("-", $i2[0]);
				if(count($i3)>1) {
					for($j=$i3[0];$j<=$i3[1]+1;$j++) {
						$f[$j] = $this->procValToNum($i2[1]);
					}
				}
				else $f[$i3[0]] = $this->procValToNum($i2[1]);
			}
			return $f;
		} else return $this->procValToNum($pay);
	}
	
	/**
	* ���������� �������� ���������.
	* @return int|float
	*/
	public function getPayment() {
		return $this->payments;
	}

}
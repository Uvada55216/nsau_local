<?
/**
* Интерфейс для реализации
*/
require_once INCLUDES . "/effect/macro/int/" . "MacroInterface" . CLASS_EXT ;
/**
* Базовый класс для макросов, реализующий интерфейс с необходимыми методами.
* Рекомендуется использовать для стандартизации ввода\вывода данных из макроса в основное приложение.
* В базовом классе есть все самые юзабельные и обязательные методы.
* Макросы для корректной работы приложения должны наследоваться от BaseMacro, либо
* реализовывать MacroInterface.
*/
class BaseMacro implements MacroInterface {
	/**
	* конечный результат
	* @var string	
	*/
	protected $val;
	
	/**
	* конечный результат для человека лично
	* используется для отчетов за кафедру
	* для деканов и зав. кафедр
	* @var float	
	*/
	protected $personal_val;	
	
	/**
	* значение необходимое для выполнения показателя
	* Используется для подробного отчета по кафедре
	* для деканов и зав кафедрой
	* если их требуемые значения не совпадают с
	* остальными должностями
	* @var float	
	*/
	protected $personal_cond;	
	
	/**
	* значение необходимое для выполнения показателя
	* Меняется если не устраивает condition из eаffect.indicators
	* @var string	
	*/
	protected $cond;

	/**
	* значение необходимое для выполнения показателя
	* Меняется если не устраивает condition из eаffect.indicators
	* @var string	
	*/
	protected $fields;

	/**
	* id года из effect_periods
	* @var int	
	*/
	protected $year;
	
	/**
	* инфа о теккущем контракте
	* @var array|mixed	
	*/
	protected $contract;
	
	/**
	* Возвращает конечный результат
	* @return string
	*/
	public function getVal() {
		return $this->val;
	}
	
	/**
	* Возвращает значение необходимое для выполнения показателя
	* @return string
	*/
	public function getCond() {
		return $this->cond;
	}
	
	/**
	* Возвращает то что будет выводится в полях формы
	* @return array|mixed
	*/
	public function getFields() {
		return	$this->fields;
	}	
	
	/**
	* Возвращает личный вклад человека
	* используется для деканов и зав кафедр
	* в подробном отчете за кафедру
	* @return float
	*/
	public function getPersonalVal() {
		return $this->personal_val;
	}

	/**
	* Возвращает требуемый личный вклад человека
	* используется для деканов и зав кафедр
	* в подробном отчете за кафедру
	* @return float
	*/
	public function getPersonalCond() {
		return $this->personal_cond;
	}	
	
	/**
	* Записывает то что будет выводится в полях формы
	* @param array|mixed $arr Данные из effect.effect_indicators для 1 критерия
	* @return void
	*/
	protected function addFields($arr) {
		$this->fields[] = $arr;
	}		
	
	/**
	* Запись значения в конечный результат
	* @param string $val Значения
	* @return void
	*/
	protected function addValues($val) {
		$this->val = $val;
	}	
	
	/**
	* Запись персонального значения для 
	* деканов и зав. кафедр
	* использовать для отчетов
	* чтобы в списке ППС кафедры отображалось
	* значение не за кафедру, а его личный вклад
	* если не установить, в подробном о тчете за кафедру
	* будут неправильные цифры
	* @param string $val Значение
	* @return void
	*/
	protected function addPersonalValues($val) {
		$this->personal_val = $val;
	}
	
	/**
	* Запись персонального требуемого значения для 
	* деканов и зав. кафедр
	* использовать для отчетов\
	* необходимо когда требуемые значения
	* отличаются в зависимости от должности
	* @param string $val Значение
	* @return void
	*/
	protected function addPersonalCond($val) {
		$this->personal_cond = $val;
	}
	
	/**
	* информация о контракте
	* @param int $contract_id
	* @return void
	*/
	protected function contractInfo($contract_id) {
		global $EFF;
		$EFF->SetTable("effect_contracts", "ec");
		$EFF->AddCondFS("ec.id", "=", $contract_id);
		$EFF->AddTable("effect_periods", "ep");
		$EFF->AddCondFF("ec.period_id", "=", "ep.id");
		
		$EFF->AddField("ec.department_id", "department_id");
		$EFF->AddField("ec.post_id", "post_id");
		$EFF->AddField("ec.user_id", "user_id");
		$EFF->AddField("ec.period_id", "period_id");
		$EFF->AddField("ec.rate", "rate");
		
		$EFF->AddField("ep.year_id", "year_id");
		$contract = $EFF->FetchAssoc($EFF->Select(1));
		
		$this->contract = $contract;
		$this->year_id = $contract["year_id"];
	}
	
	/**
	* информация о том находится ли человек в декрете
	* @param int $user_id
	* @return bool
	*/
	protected function isDecree($user_id) {
		global $DB;
		$DB->SetTable("nsau_people");
		$DB->AddCondFS("user_id", "=", $user_id);
		$DB->AddField("status_id");
		$status = $DB->FetchAssoc($DB->Select(1));
		if($status["status_id"]!=10) {
			return false;
		} 
		else {
			return true;
		}
	}
	
	
}
<?
/**
* Базовый класс
*/
require_once INCLUDES . "/effect/macro/" . "BaseMacro" . CLASS_EXT ;
/**
* Считает процент ППС прошедших курсы повышения квалификации в последние 3 года для кафедры, факультета,
* конкретного человека. 
*/
class CountCandDissertation extends BaseMacro {

	/**
	* id контракта
	* @var int	
	*/
	private $contract_id;

	/**
	* Конструктор для инициализации
	* @param int $contract_id ид контракта
	* @param string $cond значение необходимое для выполнения показателя
	* @return void
	*/
	public function __construct($contract_id, $cond = null) {
		$this->contract_id = $contract_id;
		$this->contractInfo($this->contract_id);
		$this->cond = $cond;
		$this->sp();
	}


	/**
	* Выбирает метод для расчета % успеваемости соответствии с должностью
	* @return void
	*/	
	private function sp() {
		if($this->contract["post_id"]==1) {//for faculty			
			$val = $this->spFaculty($this->contract["department_id"]);
			$this->addValues($val);		
		}
		elseif($this->contract["post_id"]==2) { //for department
			$val = $this->spDepartment($this->contract["department_id"]);
			$this->addValues($val);
		}		
	}
	
	/**
	* средний процент успеваемости на кафедре
	* @param int $department_id ид кафедры
	* @return float
	*/
	private function spDepartment($d_id) {
		global $EFF;
		$EFF->SetTable("effect_plan", "ep");
		$EFF->AddTable("effect_contracts", "ec");
		$EFF->AddCondFF("ep.contract_id", "=", "ec.id");
		$EFF->AddCondFS("ec.post_id", "=", 2);
		$EFF->AddCondFS("ec.department_id", "=", $d_id);
		$EFF->AddCondFS("ec.period_id", "=", $this->contract["period_id"]);
		$EFF->AddCondFS("ep.indicator_id", "=", 52);
		$EFF->AddTable("effect_indicators", "ei");
		$EFF->AddCondFF("ei.plan_id", "=", "ep.id");
		$EFF->AddField("ei.text0", "text0");
		$rowc = $EFF->FetchAssoc($EFF->Select(1));
		return $rowc["text0"];
	}

	/**
	* Возвращает требуемую и набранную сумму за факультет
	* @param int $department_id ид кафедры
	* @return array|mixed
	*/
	private function spFaculty($department_id) {
			global $DB;
		$DB->SetTable("nsau_departments");
		$DB->AddCondFS("id", "=", $department_id);
		$DB->AddField("faculty_id");
		$dep = $DB->FetchAssoc($DB->Select(1));		
		
		$DB->SetTable("nsau_departments");
		$DB->AddCondFS("faculty_id", "=", $dep["faculty_id"]);
		$DB->AddCondFS("is_active", "=", 1);
		$DB->AddField("id");
		$res = $DB->Select();
		while($row = $DB->FetchAssoc($res)) {
			$d += $this->procValToNum($this->spDepartment($row["id"]));
			$c++;
		}
		return round($d/$c, 2);
	}
	
	private function procValToNum($val) {
		if(strpos($val, "%")===false) {
			$result = $val;
		} else {				
			$result = (int)str_replace("%", "", $val)/100;
		}
		$result = str_replace(",", ".", $result);
		return $result;
	}
	
	
}
<?
/**
* Базовый класс
*/
require_once INCLUDES . "/effect/macro/" . "BaseMacro" . CLASS_EXT ;
/**
* Доля ППС, имеющих ученую степень доктора 
* наук или звание профессора (по списочной численности)
*/
class DokProfRanks extends BaseMacro {

	/**
	* id контракта
	* @var int	
	*/
	protected $contract_id;

	/**
	* id степеней подходящих под условия
	* @var array|int	
	*/	
	protected $degree_ids;

	/**
	* id званий подходящих под условия
	* @var array|int	
	*/		
	protected $academ_stat_ids;
	
	/**
	* Конструктор для инициализации
	* @param int $contract_id ид контракта
	* @param string $cond значение необходимое для выполнения показателя
	* @return void
	*/
	public function __construct($contract_id, $cond = null) {
		$this->contract_id = $contract_id;
		$this->contractInfo($this->contract_id);
		$this->degree_ids = array(2, 4, 6, 8, 10, 12, 14, 16, 31, 61, 81, 101);
		$this->academ_stat_ids = array(2);
		$this->cond = $cond;
		$this->dokProfRanks();
	}


	/**
	* % людей на факультете со степенью
	* доктора или званием профессора в 
	* зависимости от должности
	* @return void
	*/	
	protected function dokProfRanks() {
		if($this->contract["post_id"]==1) {//for faculty			
			$result = $this->dokProfRanksF($this->contract["department_id"]);
		}
		elseif($this->contract["post_id"]==2) { //for department
			$result = $this->dokProfRanksD($this->contract["department_id"]);
		}		
		$this->addValues($result."%");
	}
	
	
	/**
	* % людей на факультете со степенью
	* доктора или званием профессора
	* @return float
	*/	
	private function dokProfRanksF($department_id) {
		global $EFF, $DB;
		$DB->SetTable("nsau_departments");
		$DB->AddCondFS("id", "=", $department_id);
		$DB->AddField("faculty_id");
		$dep = $DB->FetchAssoc($DB->Select(1));		
		
		$DB->SetTable("nsau_departments");
		$DB->AddCondFS("faculty_id", "=", $dep["faculty_id"]);
		$DB->AddCondFS("is_active", "=", 1);
		$DB->AddField("id");
		$dres = $DB->Select();
		while($drow = $DB->FetchAssoc($dres)) {
			$r += $this->dokProfRanksD($drow["id"]);
			$c_d++;
		}
		return round(($r/$c_d), 2);
	}
	
	
	/**
	* % людей на кафедре со степенью
	* доктора или званием профессора
	* @return float
	*/	
	private function dokProfRanksD($department_id) {
		global $DB, $EFF;
		
		$EFF->SetTable("effect_contracts", "ec");
		$EFF->AddCondFS("ec.department_id", "=", $department_id);

		$EFF->AddTable("effect_periods", "epe");
		$EFF->AddCondFS("epe.year_id", "=", $this->year_id);
		$EFF->AddCondFF("epe.id", "=", "ec.period_id");
		
		$EFF->AddField("ec.user_id", "user_id");
		
		$res = $EFF->Select();
		while($row = $EFF->FetchAssoc($res)) {
			$users[$row["user_id"]] = 0;
		}
		foreach($users as $user_id => $user) {
			$DB->SetTable("nsau_people");
			$DB->AddCondFS("user_id", "=", $user_id);
			$DB->AddField("id");
			$p = $DB->FetchAssoc($DB->Select(1));
			
			$DB->SetTable("nsau_teachers");
			$DB->AddCondFS("people_id", "=", $p["id"]);
			$DB->AddFields(array("people_id", "degree", "academ_stat"));
			$t[$user_id] = $DB->FetchAssoc($DB->Select(1));
			
			$deg = explode(";", $t[$user_id]["degree"]);
			foreach($deg as $degree) {
				if(in_array($degree, $this->degree_ids)) {
					$users[$user_id] = 1;
				}
			}
			
			$as = explode(";", $t[$user_id]["academ_stat"]);
			foreach($as as $academ_stat) {
				if(in_array($academ_stat, $this->academ_stat_ids)) {
					$users[$user_id] = 1;
				}
			}
			
		}
		foreach($users as $id => $val) {
			if($val==1) {
				$r++;
			}
		}
		return round(($r/count($users))*100, 2);
	}

}
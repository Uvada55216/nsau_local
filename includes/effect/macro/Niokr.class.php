<?
/**
* Базовый класс
*/
require_once INCLUDES . "/effect/macro/" . "BaseMacro" . CLASS_EXT ;
/**
* Считает процент ППС прошедших курсы повышения квалификации в последние 3 года для кафедры, факультета,
* конкретного человека. 
*/
class Niokr extends BaseMacro {

	/**
	* id контракта
	* @var int	
	*/
	private $contract_id;

	/**
	* Конструктор для инициализации
	* @param int $contract_id ид контракта
	* @param string $cond значение необходимое для выполнения показателя
	* @return void
	*/
	public function __construct($contract_id, $cond = null) {
		$this->contract_id = $contract_id;
		$this->contractInfo($this->contract_id);
		$this->cond = $cond;
		$this->niokr();
	}


	/**
	* Выбирает метод для расчета НИОКР в соответствии с должностью
	* @return void
	*/	
	private function niokr() {
		if($this->contract["post_id"]==1) {//for faculty			
			$result = $this->niokrFaculty($this->contract["department_id"]);
			$this->cond = $result["cond"];
			$this->addValues($result["sum"]);		
			$this->addPersonalValues($result["personal_sum"]);			
			$this->selectFields();
		}
		elseif($this->contract["post_id"]==2) { //for department
			$result = $this->niokrDepartment($this->contract["department_id"]);
			$this->cond = $result["cond"];
			$this->addValues($result["sum"]);
			$this->addPersonalValues($result["personal_sum"]);
			$this->selectFields();
		}		
		else {
			$result = $this->niokrPeople($this->contract["user_id"], $this->contract["department_id"], array(3,4,5,6));		
			$this->addValues($result["sum"]);
			$this->cond = $result["cond"];
			$this->selectFields();
		}		
	}
	
	/**
	* Возвращает требуемую и набранную сумму за кафедру
	* @param int $department_id ид кафедры
	* @return array|mixed
	*/
	private function niokrDepartment($department_id) {
		global $EFF, $DB;
		$EFF->SetTable("effect_contracts", "ec");
		$EFF->AddCondFS("ec.department_id", "=", $department_id);
		
		
		$EFF->AddTable("effect_periods", "ep");
		$EFF->AddCondFS("ep.year_id", "=", $this->year_id);
		$EFF->AddCondFF("ep.id", "=", "ec.period_id");
		
		$EFF->AddField("ec.user_id", "user_id");

		$res = $EFF->Select();
		
		while($teacher = $DB->FetchAssoc($res)) {
			if(!$this->isDecree($teacher["user_id"])) {
				$teachers[$teacher["user_id"]] = $teacher["user_id"]; 
			}
		}
		
	
		foreach($teachers as $user_id => $t) {
			$result = $this->niokrPeople($user_id, $department_id);
			$total += $result["sum"];
			$total_cond += $result["cond"];
		}
	
		$ps = $this->niokrPeople($this->contract["user_id"], $this->contract["department_id"]);
		//CF::Debug(array("sum" => $total, "cond" => $total_cond, "personal_sum" => $ps["sum"]));
		return array("sum" => $total, "cond" => $total_cond, "personal_sum" => $ps["sum"]);
		
	}
	
	
	/**
	* Возвращает требуемую и набранную сумму за факультет
	* @param int $department_id ид кафедры
	* @return array|mixed
	*/
	private function niokrFaculty($department_id) {
			global $DB;
		$DB->SetTable("nsau_departments");
		$DB->AddCondFS("id", "=", $department_id);
		$DB->AddField("faculty_id");
		$dep = $DB->FetchAssoc($DB->Select(1));		
		
		$DB->SetTable("nsau_departments");
		$DB->AddCondFS("faculty_id", "=", $dep["faculty_id"]);
		$DB->AddCondFS("is_active", "=", 1);
		$DB->AddField("id");
		$res = $DB->Select();
		while($row = $DB->FetchAssoc($res)) {
			$d = $this->niokrDepartment($row["id"]);
			$need += $d["cond"];
			$total += $d["sum"];
		}
		$ps = $this->niokrPeople($this->contract["user_id"], $this->contract["department_id"]);
		return array("sum" => $total, "cond" => $need, "personal_sum" => $ps["sum"]);
	}
	
	
	/**
	* Возвращает требуемую и набранную сумму за человека
	* @param int $user_id ид пользователя (nsau.auth_users)
	* @param int $department_id ид кафедры
	* @return int
	*/
	public function niokrPeople($user_id, $department_id, $post_ids = null) {
		global $EFF, $DB;
		$EFF->SetTable("effect_plan", "ep");
		$EFF->AddTable("effect_contracts", "ec");
		
		$EFF->AddTable("effect_periods", "epe");
		$EFF->AddCondFS("epe.year_id", "=", $this->year_id);
		$EFF->AddCondFF("epe.id", "=", "ec.period_id");
		
		$EFF->AddCondFF("ep.contract_id", "=", "ec.id");
		$EFF->AddCondFS("ec.user_id", "=", $user_id);
		$EFF->AddCondFS("ec.department_id", "=", $department_id);
		$EFF->AddCondFS("ep.indicator_id", "=", 48);
		$EFF->AddField("ep.id", "id");
		$EFF->AddField("ec.rate", "rate");
		$EFF->AddField("ec.post_id", "post_id");
		//echo $EFF->SelectQuery();
		$res = $EFF->Select();
		while($row = $EFF->FetchAssoc($res)) {
			if($post_ids) {
				if(!in_array($row['post_id'], $post_ids))
					continue;
			}
			$rate = $row["rate"];
			if(!empty($row)) {
				$EFF->SetTable("effect_indicators");
				$EFF->AddCondFS("plan_id", "=", $row["id"]);
				$indr = $EFF->Select();
				while($ind = $EFF->FetchAssoc($indr)) {
					$p_total += $ind["text2"]*$this->procValToNum($ind["text3"]);
				}
			}
		}
		//sCF::Debug($this->year_id);
		return array("sum" => $p_total, "cond" => $this->cond*$rate);
	}
	
	
	/**
	* Строит массив, необходимый для вывода таблице в правом окне
	* при просмотре критериев, добавленных в показатель
	* @return void
	*/
	private function selectFields() {
			global $EFF;
		$EFF->SetTable("effect_plan", "ep");
		$EFF->AddCondFS("ep.contract_id", "=", $this->contract_id);
		$EFF->AddCondFS("ep.indicator_id", "=", 48);
		$EFF->AddField("ep.id", "id");
		$row = $EFF->FetchAssoc($EFF->Select(1));
		$EFF->SetTable("effect_indicators");
		$EFF->AddCondFS("plan_id", "=", $row["id"]);
		$indr = $EFF->Select();
		while($ind = $EFF->FetchAssoc($indr)) {
			$p_total += $ind["text2"]*$ind["text3"];
			$this->addFields($ind);
		}		
	}
	
	private function procValToNum($val) {
		if(strpos($val, "%")===false) {
			$result = $val;
		} else {				
			$result = (int)str_replace("%", "", $val)/100;
		}
		$result = str_replace(",", ".", $result);
		return $result;
	}
	
}
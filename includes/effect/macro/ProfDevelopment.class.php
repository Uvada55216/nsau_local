<?
/**
* Базовый класс
*/
require_once INCLUDES . "/effect/macro/" . "BaseMacro" . CLASS_EXT ;
/**
* Считает процент ППС прошедших курсы повышения квалификации в последние 3 года для кафедры, факультета,
* конкретного человека. 
*/
class ProfDevelopment extends BaseMacro {
	
	/**
	* id контракта
	* @var int	
	*/
	private $contract_id;


	
	/**
	* Конструктор для инициализации
	* @param int $contract_id ид контракта
	* @param string $cond значение необходимое для выполнения показателя
	* @return void
	*/	
	public function __construct($contract_id, $cond = null) {
		$this->contract_id = $contract_id;
		$this->contractInfo($this->contract_id);
		$this->cond = $cond;
		$this->profDevelopment();			
	}

	/**
	* В зависимости от должности вызывает необходимый метод
	* для расчета. Должности захардкодены(
	* @return void
	* @global $EFF бд эффективного контракта
	*/
	private function profDevelopment() {
		global $EFF;

		
		if($this->contract["post_id"]==1) {//for faculty
			$result = $this->profDevelopmentFaculty($this->contract["department_id"]);
			$this->addFields(array("text0"=>$result."%"));
			$this->addValues($result."%");
		}
		elseif($this->contract["post_id"]==2){ //for department
			$result = $this->profDevelopmentDepartment($this->contract["department_id"]);
			$this->addFields(array("text0"=>$result."%"));
			$this->addValues($result."%");
		}
		else {
			$result = $this->profDevelopmentPeople($this->contract["user_id"]);
			if($result) {
				$this->addValues(1);
			}
			$this->addFields(array("text0"=>iconv("utf-8", "cp1251", "Не реже 1 раза в 3 года")));
		}

		
	}

	/**
	* Для 1 человека. Либо false, либо сообщение об успехе
	* сообщение выводится в критерии. Это плохо. Переделать.
	* @param int $user_id id пользователя из nsau.auth_users
	* @return bool|string
	* @global $DB бд nsau
	*/
	private function profDevelopmentPeople($user_id) {
			global $DB;
		$DB->SetTable("nsau_people");
		$DB->AddCondFS("user_id", "=", $user_id);
		$DB->AddField("id");
		$p = $DB->FetchAssoc($DB->Select(1));
		$date_end  = date("Y", mktime(0, 0, 0, date("m"),   date("d"),   date("Y")-3))."-01-01";
		$DB->SetTable("nsau_prof_development");
		$DB->AddCondFS("people_id", "=", $p["id"]);
		$DB->AddCondFS("date_end", ">=", $date_end);
		$row = $DB->FetchAssoc($DB->Select(1));			
		if(!empty($row))
			return true;
		else return false;
	}
	
	/**
	* Для кафедры. Либо false, либо результат
	* @param int $department_id id кафедры из nsau.nsau_departments
	* @return bool|float
	* @global $DB бд nsau
	*/
	private function profDevelopmentDepartment($department_id) {
			global $DB, $EFF;
		$EFF->SetTable("effect_contracts", "ec");
		$EFF->AddCondFS("ec.department_id", "=", $department_id);
		
		
		$EFF->AddTable("effect_periods", "ep");
		$EFF->AddCondFS("ep.year_id", "=", $this->year_id);
		$EFF->AddCondFF("ep.id", "=", "ec.period_id");
		
		$EFF->AddField("ec.user_id", "user_id");

		$res = $EFF->Select();
		
		while($teacher = $DB->FetchAssoc($res)) {
			if(!$this->isDecree($teacher["user_id"])) {
				$teachers[$teacher["user_id"]] = $teacher["user_id"]; 
			}
		}
		
		foreach($teachers as $user_id => $people) {

			$row = $this->profDevelopmentPeople($user_id);
			
			if(!empty($row))
				$t++;
			else
				$f++;	
		}
		return round(($t/($t+$f))*100, 2);
	}

	/**
	* Для факультета. Либо false, либо результат
	* @param int $department_id id кафедры из nsau.nsau_departments
	* @return bool|float
	* @global $DB бд nsau
	*/
	private function profDevelopmentFaculty($department_id) {
			global $DB;
		$DB->SetTable("nsau_departments");
		$DB->AddCondFS("id", "=", $department_id);
		$DB->AddField("faculty_id");
		$dep = $DB->FetchAssoc($DB->Select(1));		
		
		$DB->SetTable("nsau_departments");
		$DB->AddCondFS("faculty_id", "=", $dep["faculty_id"]);
		$DB->AddCondFS("is_active", "=", 1);
		$DB->AddField("id");
		$res = $DB->Select();
		while($row = $DB->FetchAssoc($res)) {
			$arr += $this->profDevelopmentDepartment($row["id"]);
			$count++;
		}
		return round($arr/$count, 2);
	}
	
}
<?
/**
* Базовый класс
*/
require_once INCLUDES . "/effect/macro/" . "DokProfRanks" . CLASS_EXT ;
/**
* Доля ППС, имеющих ученую степень
*  или звание (по списочной численности)
*/
class Ranks extends DokProfRanks {

	/**
	* Конструктор для инициализации
	* @param int $contract_id ид контракта
	* @param string $cond значение необходимое для выполнения показателя
	* @return void
	*/
	public function __construct($contract_id, $cond = null) {
		$this->contract_id = $contract_id;
		$this->contractInfo($this->contract_id);
		$this->degree_ids = array(1, 2, 3, 4, 5, 6, 7, 8, 
															9, 10, 11, 12, 13, 14, 
															15, 16, 21, 31, 41, 51, 
															61, 71, 81, 91, 101, 111,
															121);
		$this->academ_stat_ids = array(1, 2, 3, 4, 5, 6);
		$this->cond = $cond;
		$this->dokProfRanks();
	}


	
	
}
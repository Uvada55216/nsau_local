<?
/**
* Базовый класс
*/
require_once INCLUDES . "/effect/macro/" . "BaseMacro" . CLASS_EXT ;
/**
* Общая ставка по факультету и кафедре
*/
class RateCount extends BaseMacro {

	/**
	* id контракта
	* @var int	
	*/
	private $contract_id;

	/**
	* Конструктор для инициализации
	* @param int $contract_id ид контракта
	* @param string $cond значение необходимое для выполнения показателя
	* @return void
	*/
	public function __construct($contract_id, $cond = null) {
		$this->contract_id = $contract_id;
		$this->contractInfo($this->contract_id);
		$this->cond = $cond;
		$this->rateCount();
	}


	/**
	* Выбирает метод для расчета в соответствии с должностью
	* @return void
	*/	
	private function rateCount() {
		if($this->contract["post_id"]==1) {//for faculty			
			$result = $this->rateCountFaculty($this->contract["department_id"]);
			$this->cond = $result["sum"];
			$this->addValues($result["cond"]);		
		}
		elseif($this->contract["post_id"]==2) { //for department
			$result = $this->rateCountDepartment($this->contract["department_id"]);
			$this->cond = $result;
			$this->addValues($this->depertmentCond($this->contract["department_id"]));
		}		else {
			// $this->addValues($this->ratePeople($this->contract_id));
			
		}
	}
	
	
	/**
	* Возвращает сумму ставок введенную зав кафедры
	* @param int $contract_id ид контракта
	* @return float
	*/	
	private function depertmentCond($d_id) {
			global $EFF;
		$EFF->SetTable("effect_plan", "ep");
		$EFF->AddTable("effect_contracts", "ec");
		$EFF->AddCondFF("ep.contract_id", "=", "ec.id");
		$EFF->AddCondFS("ec.post_id", "=", 2);
		$EFF->AddCondFS("ec.department_id", "=", $d_id);
		$EFF->AddCondFS("ep.indicator_id", "=", 58);
		$EFF->AddCondFS("ec.period_id", "=", $this->contract["period_id"]);
		$EFF->AddTable("effect_indicators", "ei");
		$EFF->AddCondFF("ei.plan_id", "=", "ep.id");
		$EFF->AddField("ei.text0", "text0");
		$EFF->AddField("ei.id", "id");
		$rowc = $EFF->FetchAssoc($EFF->Select(1));
		return $rowc["text0"];
	}
	
	
	
	/**
	* Возвращает сумму ставок по контрактам на кафедру
	* @param int $department_id ид кафедры
	* @return float
	*/
	private function rateCountDepartment($department_id) {
		global $EFF, $DB;
		$EFF->SetTable("effect_contracts", "ec");
		$EFF->AddCondFS("ec.department_id", "=", $department_id);
		$EFF->AddCondFS("ec.period_id", "=", $this->contract["period_id"]);
		$EFF->AddCondFS("ec.post_id", "!=", 2);
		$EFF->AddCondFS("ec.post_id", "!=", 1);
		$EFF->AddField("ec.user_id", "user_id");
		$EFF->AddField("ec.id", "id");

		$res = $EFF->Select();
		
		while($teacher = $DB->FetchAssoc($res)) {
			if(!$this->isDecree($teacher["user_id"])) {
				$teachers[$teacher["id"]] = $teacher["id"]; 
			}
		}
		
	
		foreach($teachers as $user_id => $t) {
			$total += $this->ratePeople($t);
		}
	
		
		return $total;
		
	}
	
	
	/**
	* Возвращает сумму ставок по контрактам на факультет
	* и сууму ставок введенных зав кафедрами
	* @param int $department_id ид кафедры
	* @return float
	*/
	private function rateCountFaculty($department_id) {
			global $DB;
		$DB->SetTable("nsau_departments");
		$DB->AddCondFS("id", "=", $department_id);
		$DB->AddField("faculty_id");
		$dep = $DB->FetchAssoc($DB->Select(1));		
		
		$DB->SetTable("nsau_departments");
		$DB->AddCondFS("faculty_id", "=", $dep["faculty_id"]);
		$DB->AddCondFS("is_active", "=", 1);
		$DB->AddField("id");
		$res = $DB->Select();
		while($row = $DB->FetchAssoc($res)) {			
			$c += $this->parseData($this->depertmentCond($row["id"]));
			$d += $this->rateCountDepartment($row["id"]);
		}
		
		return array("sum" => $d, "cond" => $c);
	}
	
	
	/**
	* Возвращает ставку по контракту
	* @param int $c_id ид контракта
	* @return float
	*/
	private function ratePeople($c_id) {
		global $EFF, $DB;
		$EFF->SetTable("effect_contracts", "ec");
		$EFF->AddCondFS("ec.id", "=", $c_id);
		$EFF->AddField("ec.rate", "rate");
		$row = $EFF->FetchAssoc($EFF->Select(1));
		return $this->parseData($row["rate"]);
	}

	
		/**
	* Приводит к единному виду данные по доле участия
	* effect_idicators
	* @return string
	*/
	private function parseData($val) {
		if(strpos($val, "%")===false) {
			$result = $val;
		} else {				
			$result = $val = (int)str_replace("%", "", $val)/100;
		}
		if(strpos($val, ",")===false)
			$result = $val;
		else {				
			$result = $val = str_replace(",", ".", $val);
		}
		return $result;
	}
	
}
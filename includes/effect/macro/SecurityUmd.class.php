<?
/**
* Базовый класс
*/
require_once INCLUDES . "/effect/macro/" . "BaseMacro" . CLASS_EXT ;
/**
* Считает процент ППС прошедших курсы повышения квалификации в последние 3 года для кафедры, факультета,
* конкретного человека. 
*/
class SecurityUmd extends BaseMacro {

	/**
	* id контракта
	* @var int	
	*/
	private $contract_id;

	/**
	* Конструктор для инициализации
	* @param int $contract_id ид контракта
	* @param string $cond значение необходимое для выполнения показателя
	* @return void
	*/
	public function __construct($contract_id, $cond = null) {
		$this->contract_id = $contract_id;
		$this->contractInfo($this->contract_id);
		$this->cond = $cond;
		$this->sp();
	}


	/**
	* Выбирает метод для расчета % успеваемости соответствии с должностью
	* @return void
	*/	
	private function sp() {
		if($this->contract["post_id"]==1) {//for faculty			
			$val = $this->spFaculty($this->contract["department_id"]);
			$this->addValues($val);		
		}
		elseif($this->contract["post_id"]==2) { //for department
			$val = $this->spDepartment($this->contract["department_id"]);
			$this->addValues($val);
		}		
		else {	
			$val = $this->spPeople($this->contract["user_id"]);
			$this->addValues($val);
		}		
	}
	
	/**
	* средний процент успеваемости на кафедре
	* @param int $department_id ид кафедры
	* @return float
	*/
	private function spDepartment($department_id) {
		global $EFF, $DB;
		$EFF->SetTable("effect_contracts", "ec");
		$EFF->AddCondFS("ec.department_id", "=", $department_id);
		$EFF->AddCondFS("ec.post_id", "!=", 2);
		$EFF->AddCondFS("ec.post_id", "!=", 1);
		
		
		$EFF->AddTable("effect_periods", "ep");
		$EFF->AddCondFS("ep.year_id", "=", $this->year_id);
		$EFF->AddCondFF("ep.id", "=", "ec.period_id");
		
		$EFF->AddField("ec.user_id", "u_id");

		$res = $EFF->Select();
		
		while($teacher = $DB->FetchAssoc($res)) {
			if(!$this->isDecree($teacher["u_id"])) {
				$teachers[$teacher["u_id"]] = $teacher["u_id"]; 
			}
		}
		foreach($teachers as $user_id => $people) {
			$total += $this->spPeople($user_id);
		}
		
		return round($total/count($teachers), 2);
	}

	/**
	* процент успеваемости 1 человека
	* @param int $department_id ид кафедры
	* @return float
	*/
	private function spPeople($user_id) {
		global $EFF, $DB;
		$EFF->SetTable("effect_contracts", "ec");
		$EFF->AddCondFS("ec.user_id", "=", $user_id);
		$EFF->AddTable("effect_periods", "ep");
		$EFF->AddCondFS("ep.year_id", "=", $this->year_id);
		$EFF->AddCondFF("ep.id", "=", "ec.period_id");
		$EFF->AddField("ec.id", "c_id");
		$res = $EFF->Select();
		while($row = $DB->FetchAssoc($res)) {
			$EFF->SetTable("effect_plan");
			$EFF->AddCondFS("indicator_id", "=", 42);
			$EFF->AddCondFS("contract_id", "=", $row["c_id"]);
			$EFF->AddField("id");
			$cr = $EFF->FetchAssoc($EFF->Select(1));
			$EFF->SetTable("effect_indicators");
			$EFF->AddCondFS("plan_id", "=", $cr["id"]);
			$EFF->AddField("text0");
			$v = $EFF->FetchAssoc($EFF->Select(1));
			$total += $this->procValToNum($v["text0"]);
		}
		return $total;
	}
	
	
	/**
	* Возвращает требуемую и набранную сумму за факультет
	* @param int $department_id ид кафедры
	* @return array|mixed
	*/
	private function spFaculty($department_id) {
			global $DB;
		$DB->SetTable("nsau_departments");
		$DB->AddCondFS("id", "=", $department_id);
		$DB->AddField("faculty_id");
		$dep = $DB->FetchAssoc($DB->Select(1));		
		
		$DB->SetTable("nsau_departments");
		$DB->AddCondFS("faculty_id", "=", $dep["faculty_id"]);
		$DB->AddCondFS("is_active", "=", 1);
		$DB->AddField("id");
		$res = $DB->Select();
		while($row = $DB->FetchAssoc($res)) {
			$d += $this->spDepartment($row["id"]);
			$c++;
		}
		return round($d/$c, 2);
	}
	

		private function procValToNum($val) {
		if(strpos($val, "%")===false) {
			$result = $val;
		} else {				
			$result = (int)str_replace("%", "", $val)/100;
		}
		$result = str_replace(",", ".", $result);
		if($result>1) {
			$result = $result/100; 
		}
		return $result;
	}
	
	
}
<?
/**
* Базовый класс
*/
require_once INCLUDES . "/effect/macro/" . "BaseMacro" . CLASS_EXT ;
/**
* Считает процент ППС прошедших курсы повышения квалификации 
* в последние 3 года для кафедры, факультета,
* конкретного человека. 
*/
class VakRintz extends BaseMacro {

	/**
	* id контракта
	* @var int	
	*/
	private $contract_id;


	/**
	* Конструктор для инициализации
	* @param int $contract_id ид контракта
	* @param string $cond значение необходимое для выполнения 
	* показателя
	* @return void
	*/
	public function __construct($contract_id, $cond = null) {
		$this->contract_id = $contract_id;
		$this->contractInfo($this->contract_id);
		$this->cond = $cond;
		$this->vakRintz();
	}

	
	/**
	* Выбирает метод для соответствующей
	* должности
	* @return void
 	*/	
	private function vakRintz() {
		if($this->contract["post_id"]==1) {//for faculty			
			$result = $this->vakRintzFaculty($this->contract["department_id"]);
			$this->addPersonalValues($this->vakRintzPeople($this->contract["user_id"], $this->contract["department_id"]));
		}
		elseif($this->contract["post_id"]==2) { //for department
			$result = $this->vakRintzDepartment($this->contract["department_id"]);
			$this->addPersonalValues($this->vakRintzPeople($this->contract["user_id"], $this->contract["department_id"]));
		}		
		else {
			$result = $this->vakRintzPeople($this->contract["user_id"], $this->contract["department_id"]);
		}		
		if(!empty($result)) {
			$this->addValues($result);
		}
		$this->selectFields();
	}
	
	
	/**
	* Подсчет для факультета
	* @param int $department_id ид кафедры
	* @return float коэффициент для декана
 	*/	
	private function vakRintzFaculty($department_id) {
		global $EFF, $DB;
		$DB->SetTable("nsau_departments");
		$DB->AddCondFS("id", "=", $department_id);
		$DB->AddField("faculty_id");
		$dep = $DB->FetchAssoc($DB->Select(1));		
		
		$DB->SetTable("nsau_departments");
		$DB->AddCondFS("faculty_id", "=", $dep["faculty_id"]);
		$DB->AddCondFS("is_active", "=", 1);
		$DB->AddField("id");
		$dres = $DB->Select();
		while($drow = $DB->FetchAssoc($dres)) {
			$r += $this->vakRintzDepartment($drow["id"]);
			$c_d++;
		}
		return $r/$c_d;
	}
	
	/**
	* Подсчет для кафедры
	* @param int $department_id ид кафедры
	* @return float коэффициент для зав кафедры
 	*/	
	private function vakRintzDepartment($department_id) {
		global $EFF;
		
		$EFF->SetTable("effect_contracts", "ec");
		$EFF->AddCondFS("ec.department_id", "=", $department_id);

		$EFF->AddTable("effect_periods", "epe");
		$EFF->AddCondFS("epe.year_id", "=", $this->year_id);
		$EFF->AddCondFF("epe.id", "=", "ec.period_id");
		
		$EFF->AddTable("effect_plan", "ep");
		$EFF->AddCondFF("ep.contract_id", "=", "ec.id");
		$EFF->AddAltFS("ep.indicator_id", "=", 24);
		$EFF->AddAltFS("ep.indicator_id", "=", 25);
		$EFF->AddAltFS("ep.indicator_id", "=", 26);
		$EFF->AddAltFS("ep.indicator_id", "=", 47);
		$EFF->AppendAlts();
		
		$EFF->AddTable("effect_indicators", "ei");
		$EFF->AddCondFF("ei.plan_id", "=", "ep.id");
		

		$EFF->AddField("ei.text1", "text1");
		$EFF->AddField("ec.user_id", "user_id");
		
		$res = $EFF->Select();
		if($this->isDecree($row["user_id"])) {
			$row["text1"] = 0;
		}
		while($row = $EFF->FetchAssoc($res)) {
			$r += $this->parseData($row["text1"]);
		}
		return $r/$this->countPpsD($department_id);
	}
	
	
	/**
	* Подсчет для 1 человека acsenova
	* @param int $department_id ид пользователя
	* @param int $department_id ид кафедры
	* @return float кол-во заполнений ВАК/РИНЦ
	* (ищет только в обязательных)
 	*/	
	private function vakRintzPeople($user_id, $department_id) {
		global $EFF;
		$EFF->SetTable("effect_contracts", "ec");
		$EFF->AddCondFS("ec.department_id", "=", $department_id);
		$EFF->AddCondFS("ec.user_id", "=", $user_id);

		$EFF->AddTable("effect_periods", "epe");
		$EFF->AddCondFS("epe.year_id", "=", $this->year_id);
		$EFF->AddCondFF("epe.id", "=", "ec.period_id");
		
		$EFF->AddTable("effect_plan", "ep");
		$EFF->AddCondFF("ep.contract_id", "=", "ec.id");
		$EFF->AddCondFS("ep.indicator_id", "=", 47);
		
		$EFF->AddTable("effect_indicators", "ei");
		$EFF->AddCondFF("ei.plan_id", "=", "ep.id");

		$EFF->AddField("ei.id", "id");
		
		$res = $EFF->Select();
		while($row = $EFF->FetchAssoc($res)) {
			$r[$row["id"]] = $row["id"];
		}
		return count($r);
	}
	
	/**
	* считает кол-во ппс на кафедре
	* считает за год текущего котракта
	* по созданным контрактам а не по nsau_teachers
	* @param int $department_id ид кафедры
	* @return int
	*/
	private function countPpsD($department_id) {
		global $EFF;
		$EFF->SetTable("effect_contracts", "ec");
		$EFF->AddCondFS("ec.department_id", "=", $department_id);

		$EFF->AddTable("effect_periods", "epe");
		$EFF->AddCondFS("epe.year_id", "=", $this->year_id);
		$EFF->AddCondFF("epe.id", "=", "ec.period_id");
		
		$EFF->AddField("ec.user_id", "user_id");
		
		$res = $EFF->Select();
		while($row = $EFF->FetchAssoc($res)) {
			if(!$this->isDecree($row["user_id"])) {
				$users[$row["user_id"]] = $row["user_id"];
			}
		}

		return count($users);
		
	}
	
	
	
	
	
	/**
	* считает кол-во ппс на факультете
	* считает за год текущего котракта
	* по созданным контрактам а не по nsau_teachers
	* @param int $department_id ид кафедры
	* @return int
	*/
	private function countPpsF($department_id) {
			global	$DB, $EFF;
		$DB->SetTable("nsau_departments");
		$DB->AddCondFS("id", "=", $department_id);
		$DB->AddField("faculty_id");
		$dep = $DB->FetchAssoc($DB->Select(1));		
		
		$DB->SetTable("nsau_departments");
		$DB->AddCondFS("faculty_id", "=", $dep["faculty_id"]);
		$DB->AddCondFS("is_active", "=", 1);
		$DB->AddField("id");
		$dres = $DB->Select();
		while($drow = $DB->FetchAssoc($dres)) {
			$EFF->SetTable("effect_contracts", "ec");
			$EFF->AddCondFS("ec.department_id", "=", $drow["id"]);

			$EFF->AddTable("effect_periods", "epe");
			$EFF->AddCondFS("epe.year_id", "=", $this->year_id);
			$EFF->AddCondFF("epe.id", "=", "ec.period_id");
			
			$EFF->AddField("ec.user_id", "user_id");
			
			$res = $EFF->Select();
			while($row = $EFF->FetchAssoc($res)) {
				if(!$this->isDecree($row["user_id"])) {
					$users[$row["user_id"]] = $row["user_id"];
				}
			}
			
		}
		return count($users);
	}
	
	/**
	* Строит массив, необходимый для вывода таблице в правом окне
	* при просмотре критериев, добавленных в показатель
	* @return void
	*/
	private function selectFields() {
			global $EFF;
		$EFF->SetTable("effect_plan", "ep");
		$EFF->AddCondFS("ep.contract_id", "=", $this->contract_id);
		$EFF->AddCondFS("ep.indicator_id", "=", 47);
		$EFF->AddField("ep.id", "id");
		$row = $EFF->FetchAssoc($EFF->Select(1));
		$EFF->SetTable("effect_indicators");
		$EFF->AddCondFS("plan_id", "=", $row["id"]);
		$indr = $EFF->Select();
		while($ind = $EFF->FetchAssoc($indr)) {
			$p_total += $ind["text2"]*$ind["text3"];
			$this->addFields($ind);
		}		
	}


	/**
	* Приводит к единному виду данные по доле участия
	* effect_idicators
	* @return string
	*/
	private function parseData($val) {
		if(strpos($val, "%")===false) {
			$result = $val;
		} else {				
			$result = $val = (int)str_replace("%", "", $val)/100;
		}
		if(strpos($val, ",")===false)
			$result = $val;
		else {				
			$result = $val = str_replace(",", ".", $val);
		}
		return $result;
	}



	
}
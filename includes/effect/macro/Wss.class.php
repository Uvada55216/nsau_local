<?
/**
* Базовый класс
*/
require_once INCLUDES . "/effect/macro/" . "BaseMacro" . CLASS_EXT ;
/**
* Считает кол-во публикаций в Web of Science и 
* Scorpus на 100 ППС
*/
class Wss extends BaseMacro {

	/**
	* id контракта
	* @var int	
	*/
	private $contract_id;
	
	/**
	* Конструктор для инициализации
	* @param int $contract_id ид контракта
	* @param string $cond значение необходимое для выполнения показателя
	* @return void
	*/
	public function __construct($contract_id, $cond = null) {
		$this->contract_id = $contract_id;
		$this->contractInfo($this->contract_id);
		$this->cond = $cond;
		$this->wss();
	}


	/**
	* считает web of science и scorpus
	* все доли участия из стимулирующих за факультет/ППС
	* @return void
	*/	
	private function wss() {
		global $EFF, $DB, $Auth; 
			
		$pps = $this->countPPS($this->contract["department_id"]);
		$this->cond = round($pps*0.0115, 2);
		$DB->SetTable("nsau_departments");
		$DB->AddCondFS("id", "=", $this->contract["department_id"]);
		$DB->AddField("faculty_id");
		$dep = $DB->FetchAssoc($DB->Select(1));		
		
		$DB->SetTable("nsau_departments");
		$DB->AddCondFS("faculty_id", "=", $dep["faculty_id"]);
		$DB->AddCondFS("is_active", "=", 1);
		$DB->AddField("id");
		$dres = $DB->Select();
		while($drow = $DB->FetchAssoc($dres)) {
			$EFF->SetTable("effect_plan", "ep");
			$EFF->AddTable("effect_contracts", "ec");
			
			$EFF->AddTable("effect_periods", "epe");
			$EFF->AddCondFS("epe.year_id", "=", $this->year_id);
			$EFF->AddCondFF("epe.id", "=", "ec.period_id");
			
			$EFF->AddCondFF("ep.contract_id", "=", "ec.id");
			$EFF->AddCondFS("ec.department_id", "=", $drow["id"]);
			$EFF->AddCondFS("ep.indicator_id", "=", 10);
			$EFF->AddField("ep.id", "id");
			$res = $EFF->Select();
			while($row = $EFF->FetchAssoc($res)) {
				if(!empty($row)) {
					$EFF->SetTable("effect_plan", "ep");
					$EFF->AddCondFS("ep.id", $row["id"]);
					$EFF->AddTable("effect_contracts", "ec");
					$EFF->AddCondFF("ep.contract_id", "=", "ec.id");
					$EFF->AddField("ec.user_id", "user_id");
					$ec = $EFF->FetchAssoc($EFF->Select(1));
						
					
					if(!$this->isDecree($ec["user_id"])) {
						$EFF->SetTable("effect_indicators");
						$EFF->AddCondFS("plan_id", "=", $row["id"]);
						$indr = $EFF->Select();
						while($ind = $EFF->FetchAssoc($indr)) {
							if(!empty($ind["text2"])) {
								$p_total += 1;
							}
						}
					}
				}
			}
		}
		$this->addValues($p_total);
	
		
	}
	
	/**
	* считает кол-во ппс на факультете
	* считает за год текущего котракта
	* по созданным контрактам а не по nsau_teachers
	* @param int $department_id ид кафедры
	* @return int
	*/
	private function countPPS($department_id) {
			global	$DB, $EFF;
		$DB->SetTable("nsau_departments");
		$DB->AddCondFS("id", "=", $department_id);
		$DB->AddField("faculty_id");
		$dep = $DB->FetchAssoc($DB->Select(1));		
		
		$DB->SetTable("nsau_departments");
		$DB->AddCondFS("faculty_id", "=", $dep["faculty_id"]);
		$DB->AddCondFS("is_active", "=", 1);
		$DB->AddField("id");
		$dres = $DB->Select();
		while($drow = $DB->FetchAssoc($dres)) {
			$EFF->SetTable("effect_contracts", "ec");
			$EFF->AddCondFS("ec.department_id", "=", $drow["id"]);

			$EFF->AddTable("effect_periods", "epe");
			$EFF->AddCondFS("epe.year_id", "=", $this->year_id);
			$EFF->AddCondFF("epe.id", "=", "ec.period_id");
			
			$EFF->AddField("ec.user_id", "user_id");
			
			$res = $EFF->Select();
			while($row = $EFF->FetchAssoc($res)) {
				if(!$this->isDecree($row["user_id"])) {
					$users[$row["user_id"]] = $row["user_id"];
				}
			}
			
		}
		return count($users);
	}
	
	
	/**
	* Приводит к единному виду данные по доле участия
	* effect_idicators
	* @return string
	*/
	private function parseData($val) {
		if(strpos($val, "%")===false) {
			$result = $val;
		} else {				
			$result = $val = (int)str_replace("%", "", $val)/100;
		}
		if(strpos($val, ",")===false)
			$result = $val;
		else {				
			$result = $val = str_replace(",", ".", $val);
		}
		return $result;
	}

	
	
	
}
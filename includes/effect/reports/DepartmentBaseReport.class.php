<?
require_once INCLUDES . "/effect/" . "Formula" . CLASS_EXT ;
require_once INCLUDES . "/effect/" . "Contract" . CLASS_EXT ;


class DepartmentBaseReport extends Contract {
	
	public $output;
		
	public function __construct() {
		parent::__construct();
	}
	

	public function getResult() {
		return $this->output;
	}
	
	protected function countFields($arr) { 
		for($i=0;$i<=9;$i++) {
			if(!empty($arr["text".$i]))
				$result++;
		}
		for($i=0;$i<=1;$i++) {
			if(!empty($arr["file".$i]))
				$result++;
		}
		return $result;
	}
	
	protected function getFacultyName($fid) {
			global $DB;
		$DB->SetTable("nsau_faculties");
		$DB->AddCondFS("id", "=", $fid);
		$row = $DB->FetchAssoc($DB->Select(1));
		return $row["name"];
	}
	
	/**
	* информация о том находится ли человек в декрете
	* @param int $user_id
	* @return bool
	*/
	protected function isDecree($user_id) {
		global $DB;
		$DB->SetTable("nsau_people");
		$DB->AddCondFS("user_id", "=", $user_id);
		$DB->AddField("status_id");
		$status = $DB->FetchAssoc($DB->Select(1));
		if($status["status_id"]!=10) {
			return false;
		} 
		else {
			return true;
		}
	}
}
<?
require_once INCLUDES . "/effect/reports/" . "DepartmentBaseReport" . CLASS_EXT ;


class DepartmentObligatoryReport extends DepartmentBaseReport {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function createReportByDepartment($department_id, $period_id) {
		global $DB, $EFF;
				
		$DB->SetTable("nsau_departments");
		$DB->AddCondFS("id", "=", $department_id);
		$dep = $DB->FetchAssoc($DB->Select(1));
		$this->output["department_name"][$dep["faculty_id"]][$dep["id"]] = $dep["name"];
		$this->output["faculty_name"][$dep["faculty_id"]] = $this->getFacultyName($dep["faculty_id"]);
		$this->output["department_id"] = $dep["id"];
		$this->output["period_id"] = $period_id;
		$this->output["date_from"] = $this->getYearFromDate($period_id);
		$this->output["date_to"] = $this->getYearToDate($period_id);
		
		$EFF->SetTable("indicators");
		$EFF->AddOrder("id");
		$EFF->AddCondFS("ind_type", "=", 1);
		$EFF->AddCondFS("post_id", "LIKE", "%2%");
		$i = $EFF->Select();
		while($ind = $EFF->FetchAssoc($i)) {
			$this->output["indicators"][$ind["id"]] = $ind;
		}
		
		$year_id = $this->getYearId($period_id);
		$EFF->SetTable("effect_contracts", "ec");
		$EFF->AddCondFS("ec.department_id", "=", $department_id);
		$EFF->AddAltFS("ec.post_id", "LIKE", "%2%");
					$EFF->AddAltFS("ec.post_id", "LIKE", "%3%");
					$EFF->AddAltFS("ec.post_id", "LIKE", "%4%");
					$EFF->AddAltFS("ec.post_id", "LIKE", "%5%");
					$EFF->AddAltFS("ec.post_id", "LIKE", "%6%");
					$EFF->AppendAlts();
		$EFF->AddTable("effect_periods", "epe");
		$EFF->AddCondFS("epe.year_id", "=", $year_id);
		$EFF->AddCondFF("epe.id", "=", "ec.period_id");
		
		$EFF->AddField("ec.id", "id");
		$EFF->AddField("ec.user_id", "user_id");
		$EFF->AddField("ec.rate", "rate");
		$EFF->AddField("ec.department_id", "department_id");
		$EFF->AddField("ec.post_id", "post_id");
		$EFF->AddField("ec.pps", "pps");
		$EFF->AddField("ec.period_id", "period_id");
		
		$f = $EFF->Select();
		while($contract = $EFF->FetchAssoc($f)) {
			// CF::Debug($contract);
			$user_id = $contract["user_id"];
			$DB->SetTable("auth_users");
			$DB->AddCondFS("id", "=", $contract["user_id"]);
			$user  = $DB->FetchAssoc($DB->Select());
			$DB->SetTable("nsau_departments");
			$DB->AddCondFS("id", "=", $contract["department_id"]);
			$department  = $DB->FetchAssoc($DB->Select());
			$EFF->SetTable("posts");
			$EFF->AddCondFS("id", "=", $contract["post_id"]);
			$post  = $EFF->FetchAssoc($EFF->Select());
			
			$DB->SetTable("nsau_people");
			$DB->AddCondFS("user_id", "=", $contract["user_id"]);
			$people = $DB->FetchAssoc($DB->Select(1));
			
			$DB->SetTable("nsau_teachers");
			$DB->AddCondFS("people_id", "=", $people["id"]);
			$teacher = $DB->FetchAssoc($DB->Select(1));
			if(!empty($teacher["degree"])) {
				$DB->SetTable("nsau_teachers_degree");
				$DB->AddCondFX("id", "IN", "(".implode(",", explode(";", $teacher["degree"])).")");
				$deg_res = $DB->Select();
				while($deg_row = $DB->FetchAssoc($deg_res))
					$this->output["pay_info"][$department["faculty_id"]][$department["id"]][$user_id]["degree"][$deg_row["id"]] = $deg_row["name"];
			}
			if(!empty($teacher["academ_stat"])) {
				$DB->SetTable("nsau_teachers_rank");
				$DB->AddCondFX("id", "IN", "(".implode(",", explode(";", $teacher["academ_stat"])).")");
				$deg_res = $DB->Select();
				while($deg_row = $DB->FetchAssoc($deg_res))
					$this->output["pay_info"][$department["faculty_id"]][$department["id"]][$user_id]["rank"][$deg_row["id"]] = $deg_row["name"];
			}
			
			if($post["id"]!=2) {
				if(!$this->isDecree($user_id))	{
					$this->output["pay_info"][$department["faculty_id"]][$department["id"]][$user_id]["username"] = $user["displayed_name"];
					$this->output["pay_info"][$department["faculty_id"]][$department["id"]][$user_id]["rate"] = $contract["rate"];
					$this->output["pay_info"][$department["faculty_id"]][$department["id"]][$user_id]["pps"] = $contract["pps"];
					$this->output["pay_info"][$department["faculty_id"]][$department["id"]][$user_id]["department"] = $department["name"];
					$this->output["pay_info"][$department["faculty_id"]][$department["id"]][$user_id]["post"][$post["type"]]["name"] = $post["name"];
					$this->output["pay_info"][$department["faculty_id"]][$department["id"]][$user_id]["post"][$post["type"]]["id"] = $post["id"];
					$this->output["pay_info"][$department["faculty_id"]][$department["id"]][$user_id]["contract_id"] = $contract["id"];
				}
			}
			$EFF->SetTable("effect_plan", "ep");
			$EFF->AddCondFS("ep.contract_id", "=", $contract["id"]);
			
			$EFF->AddTable("indicators", "i");
			$EFF->AddCondFS("i.ind_type", "=", 1);
			$EFF->AddCondFF("ep.indicator_id", "=", "i.id");
			
			$EFF->AddField("ep.id", "id");
			$EFF->AddField("ep.contract_id", "contract_id");
			$EFF->AddField("ep.indicator_id", "indicator_id");
			$EFF->AddField("ep.user_id", "user_id");
			
			$pres = $EFF->Select();
			
			while($prow = $EFF->FetchAssoc($pres)) {
				if(!$this->isDecree($prow["user_id"]))	{	
					$EFF->SetTable("indicators");
					$EFF->AddCondFS("id", "=", $prow["indicator_id"]);
					$indicator = $EFF->FetchAssoc($EFF->Select());
					$prow["indicator"] = $indicator;
					
					if($prow["indicator"]["auto"]!=1) {
						$EFF->SetTable("effect_indicators");
						$EFF->AddCondFS("plan_id", "=", $prow["id"]);
						$EFF->AddField("text0");
						$val = $EFF->FetchAssoc($EFF->Select(1));
						$prow["val"] = $this->convertData($val["text0"]);
						if($contract["post_id"]==2) {
							$prow["total"]["val"] = $this->convertData($prow["val"]);
						}
					} else {
						require_once INCLUDES . "/effect/macro/" . $prow["indicator"]["macros"] . CLASS_EXT ;
						$Macro = new $prow["indicator"]["macros"]($prow["contract_id"], $prow["indicator"]["conditions"]);
						$prow["val"] = $Macro->getPersonalVal();
						if($prow["val"]===NULL) {
							$prow["val"] = $this->convertData($Macro->getVal());
						}
						$prow["indicator"]["conditions"] = $Macro->getCond();
						if($contract["post_id"]==2) {
							$prow["total"]["val"] = $this->convertData($Macro->getVal());
						}
					}
					if($post["id"]!=2) {				
						$this->output["pay"][$department["faculty_id"]][$department["id"]][$user_id][$prow["indicator_id"]]["val"] = str_replace(".", ",", $prow["val"]);
					}
					if(!empty($prow["total"])) {
						$this->output["total"][$department["faculty_id"]][$department["id"]][$prow["indicator_id"]] = str_replace(".", ",", $prow["total"]);
					}
				}
			}
			
			
			
		}	
		return $this->output;
	}
	private function convertData($val) {
		if(strpos($val, "%")===false) {
			$result = $val;
		} else {				
			$result = $val = (int)str_replace("%", "", $val)/100;
		}
		if(strpos($val, ",")===false)
			$result = $val;
		else {				
			$result = $val = str_replace(",", ".", $val);
		}
		return round($result, 2);
	}

}
<?
require_once INCLUDES . "/effect/reports/" . "DepartmentBaseReport" . CLASS_EXT ;


class DepartmentStimulationReport extends DepartmentBaseReport {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function createReportByDepartment($department_id, $period_id) {
		global $DB, $EFF;
				
		$DB->SetTable("nsau_departments");
		$DB->AddCondFS("id", "=", $department_id);
		$dep = $DB->FetchAssoc($DB->Select(1));
		$this->output["department_name"][$dep["faculty_id"]][$dep["id"]] = $dep["name"];
		$this->output["faculty_name"][$dep["faculty_id"]] = $this->getFacultyName($dep["faculty_id"]);
		$this->output["department_id"] = $dep["id"];
		$this->output["period_id"] = $period_id;
		$this->output["date_from"] = $this->getFromDate($period_id);
		$this->output["date_to"] = $this->getToDate($period_id);
		
		$EFF->SetTable("indicators");
		$EFF->AddOrder("id");
		$EFF->AddCondFS("id", "!=", 23);
		$EFF->AddCondFS("id", "!=", 28);
		$EFF->AddCondFS("id", "!=", 29);
		$EFF->AddCondFS("ind_type", "=", 0);
		$i = $EFF->Select();
		while($ind = $EFF->FetchAssoc($i)) {
			$this->output["indicators"][$ind["id"]] = $ind;
		}
		$EFF->SetTable("effect_contracts");
		$EFF->AddCondFS("department_id", "=", $department_id);
		$EFF->AddCondFS("period_id", "=", $period_id);
		$f = $EFF->Select();
		while($contract = $EFF->FetchAssoc($f)) {
			$user_id = $contract["user_id"];
			$DB->SetTable("auth_users");
			$DB->AddCondFS("id", "=", $contract["user_id"]);
			$user  = $DB->FetchAssoc($DB->Select());
			$DB->SetTable("nsau_departments");
			$DB->AddCondFS("id", "=", $contract["department_id"]);
			$department  = $DB->FetchAssoc($DB->Select());
			$EFF->SetTable("posts");
			$EFF->AddCondFS("id", "=", $contract["post_id"]);
			$post  = $EFF->FetchAssoc($EFF->Select());
			
			$DB->SetTable("nsau_people");
			$DB->AddCondFS("user_id", "=", $contract["user_id"]);
			$people = $DB->FetchAssoc($DB->Select(1));
			
			$DB->SetTable("nsau_teachers");
			$DB->AddCondFS("people_id", "=", $people["id"]);
			$teacher = $DB->FetchAssoc($DB->Select(1));
			if(!empty($teacher["degree"])) {
				$DB->SetTable("nsau_teachers_degree");
				$DB->AddCondFX("id", "IN", "(".implode(",", explode(";", $teacher["degree"])).")");
				$deg_res = $DB->Select();
				while($deg_row = $DB->FetchAssoc($deg_res))
					$this->output["pay_info"][$department["faculty_id"]][$department["id"]][$user_id]["degree"][$deg_row["id"]] = $deg_row["name"];
			}
			if(!empty($teacher["academ_stat"])) {
				$DB->SetTable("nsau_teachers_rank");
				$DB->AddCondFX("id", "IN", "(".implode(",", explode(";", $teacher["academ_stat"])).")");
				$deg_res = $DB->Select();
				while($deg_row = $DB->FetchAssoc($deg_res))
					$this->output["pay_info"][$department["faculty_id"]][$department["id"]][$user_id]["rank"][$deg_row["id"]] = $deg_row["name"];
			}
			
			
			$this->output["pay_info"][$department["faculty_id"]][$department["id"]][$user_id]["username"] = $user["displayed_name"];
			if(!in_array($contract["post_id"], array(1,2)))
				$this->output["pay_info"][$department["faculty_id"]][$department["id"]][$user_id]["rate"] += $contract["rate"];
			$this->output["pay_info"][$department["faculty_id"]][$department["id"]][$user_id]["pps"] = $contract["pps"];
			$this->output["pay_info"][$department["faculty_id"]][$department["id"]][$user_id]["department"] = $department["name"];
			$this->output["pay_info"][$department["faculty_id"]][$department["id"]][$user_id]["post"][$post["type"]] = $post["name"];
			$this->output["pay_info"][$department["faculty_id"]][$department["id"]][$user_id]["contract_id"] = $contract["id"];
			
			
			$EFF->SetTable("effect_plan");
			$EFF->AddCondFS("contract_id", "=", $contract["id"]);
			$pres = $EFF->Select();
			
			while($prow = $EFF->FetchAssoc($pres)) {
				$EFF->SetTable("indicators");
				$EFF->AddCondFS("id", "=", $prow["indicator_id"]);
				$indicator = $EFF->FetchAssoc($EFF->Select());
				$prow["indicator"] = $indicator;

				$EFF->SetTable("effect_indicators");
				$EFF->AddCondFS("plan_id", "=", $prow["id"]);
				$ires = $EFF->Select();
				while($irow = $EFF->FetchAssoc($ires)) {
					$prow["fields"][$irow["id"]] = $irow["id"];
					
					$prow["haystack"][$irow["id"]] = $this->countFields($irow);	

					$EFF->SetTable("forms");
					$EFF->AddCondFS("indicator_id", "=", $prow["indicator_id"]);
					$EFF->AddOrder("id");
					$rrr = $EFF->Select();
					while($qqq =  $EFF->FetchAssoc($rrr))
						$test++;
						$prow["needle"][$irow["id"]] = $test;
					unset($test);
					
					
					
					if($prow["needle"][$irow["id"]]<=$prow["haystack"][$irow["id"]]) {
						$form = new Formula($indicator["formula"], $irow, $indicator["size"], $contract["rate"]);
						$prow["payments"] += number_format($form->getPayment(), 2, '.', '') + $this->output["pay"][$user_id][$prow["indicator_id"]]["payments"];
						$prow["count"] += 1 + $this->output["pay"][$user_id][$prow["indicator_id"]]["count"];
					}
				}	
				if(!empty($prow["fields"]))
					$this->output["pay"][$department["faculty_id"]][$department["id"]][$user_id][$prow["indicator_id"]] = $prow;
			}
		}
		return $this->output;
	}
	
}
<?
require_once INCLUDES . "/effect/reports/" . "DepartmentBaseReport" . CLASS_EXT ;


class ObligatoryShortReportFaculties extends DepartmentBaseReport {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function createReportByDepartment($department_id, $period_id, $ppost_id) {
		global $DB, $EFF;

		$DB->SetTable("nsau_departments");
		$DB->AddCondFS("is_active", "=", 1);
		$depres = $DB->Select();
		while($dep = $DB->FetchAssoc($depres)){
			$this->output["department_name"][$dep["faculty_id"]][$dep["id"]] = $dep["name"];
			$this->output["faculty_name"][$dep["faculty_id"]] = $this->getFacultyName($dep["faculty_id"]);
			$this->output["department_id"] = $dep["id"];
			$this->output["period_id"] = $period_id;
			$this->output["date_from"] = $this->getYearFromDate($period_id);
			$this->output["date_to"] = $this->getYearToDate($period_id);
			
			$EFF->SetTable("indicators");
			$EFF->AddOrder("id");
			$EFF->AddCondFS("ind_type", "=", 1);
			$EFF->AddCondFS("post_id", "LIKE", "%1%"); 
			$i = $EFF->Select();
			while($ind = $EFF->FetchAssoc($i)) {
				$this->output["indicators"][$ind["id"]] = $ind;
			}
			
			$year_id = $this->getYearId($period_id);
			$EFF->SetTable("effect_contracts", "ec");
			$EFF->AddCondFS("ec.department_id", "=", $dep["id"]);
			$EFF->AddCondFS("ec.post_id", "=", $ppost_id);
			$EFF->AddTable("effect_periods", "epe");
			$EFF->AddCondFS("epe.year_id", "=", $year_id);
			$EFF->AddCondFF("epe.id", "=", "ec.period_id");
			
			$EFF->AddField("ec.id", "id");
			$EFF->AddField("ec.user_id", "user_id");
			$EFF->AddField("ec.rate", "rate");
			$EFF->AddField("ec.department_id", "department_id");
			$EFF->AddField("ec.post_id", "post_id");
			$EFF->AddField("ec.pps", "pps");
			$EFF->AddField("ec.period_id", "period_id");
			
			$f = $EFF->Select();
			while($contract = $EFF->FetchAssoc($f)) {
				// CF::Debug($contract);
				$user_id = $contract["user_id"];
				$DB->SetTable("auth_users");
				$DB->AddCondFS("id", "=", $contract["user_id"]);
				$user  = $DB->FetchAssoc($DB->Select());
				$DB->SetTable("nsau_departments");
				$DB->AddCondFS("id", "=", $contract["department_id"]);
				$department  = $DB->FetchAssoc($DB->Select());
				$EFF->SetTable("posts");
				$EFF->AddCondFS("id", "=", $contract["post_id"]);
				$post  = $EFF->FetchAssoc($EFF->Select());
				
				$DB->SetTable("nsau_people");
				$DB->AddCondFS("user_id", "=", $contract["user_id"]);
				$people = $DB->FetchAssoc($DB->Select(1));
				
				$DB->SetTable("nsau_teachers");
				$DB->AddCondFS("people_id", "=", $people["id"]);
				$teacher = $DB->FetchAssoc($DB->Select(1));
				if(!empty($teacher["degree"])) {
					$DB->SetTable("nsau_teachers_degree");
					$DB->AddCondFX("id", "IN", "(".implode(",", explode(";", $teacher["degree"])).")");
					$deg_res = $DB->Select();
					while($deg_row = $DB->FetchAssoc($deg_res))
						$this->output["pay_info"][$department["faculty_id"]][$department["id"]][$user_id]["degree"][$deg_row["id"]] = $deg_row["name"];
				}
				if(!empty($teacher["academ_stat"])) {
					$DB->SetTable("nsau_teachers_rank");
					$DB->AddCondFX("id", "IN", "(".implode(",", explode(";", $teacher["academ_stat"])).")");
					$deg_res = $DB->Select();
					while($deg_row = $DB->FetchAssoc($deg_res))
						$this->output["pay_info"][$department["faculty_id"]][$department["id"]][$user_id]["rank"][$deg_row["id"]] = $deg_row["name"];
				}
				
				
				$this->output["pay_info"][$department["faculty_id"]][$department["id"]][$user_id]["username"] = $user["displayed_name"];
				$this->output["pay_info"][$department["faculty_id"]][$department["id"]][$user_id]["rate"] = $contract["rate"];
				$this->output["pay_info"][$department["faculty_id"]][$department["id"]][$user_id]["pps"] = $contract["pps"];
				$this->output["pay_info"][$department["faculty_id"]][$department["id"]][$user_id]["department"] = $department["name"];
				$this->output["pay_info"][$department["faculty_id"]][$department["id"]][$user_id]["post"][$post["type"]] = $post["name"];
				$this->output["pay_info"][$department["faculty_id"]][$department["id"]][$user_id]["contract_id"] = $contract["id"];
				
				$EFF->SetTable("effect_plan", "ep");
				$EFF->AddCondFS("ep.contract_id", "=", $contract["id"]);
				
				$EFF->AddTable("indicators", "i");
				$EFF->AddCondFS("i.ind_type", "=", 1);
				$EFF->AddCondFF("ep.indicator_id", "=", "i.id");
				
				$EFF->AddField("ep.id", "id");
				$EFF->AddField("ep.contract_id", "contract_id");
				$EFF->AddField("ep.indicator_id", "indicator_id");
				$EFF->AddField("ep.user_id", "user_id");
				
				$pres = $EFF->Select();
				
				while($prow = $EFF->FetchAssoc($pres)) {
						
					$EFF->SetTable("indicators");
					$EFF->AddCondFS("id", "=", $prow["indicator_id"]);
					$indicator = $EFF->FetchAssoc($EFF->Select());
					$prow["indicator"] = $indicator;

					
			
						
					
						
						
						
						if($prow["indicator"]["auto"]!=1) {
							$EFF->SetTable("effect_indicators");
							$EFF->AddCondFS("plan_id", "=", $prow["id"]);
							$EFF->AddField("text0");
							$val = $EFF->FetchAssoc($EFF->Select(1));
							$prow["val"] = $val["text0"];
						} else {
							require_once INCLUDES . "/effect/macro/" . $prow["indicator"]["macros"] . CLASS_EXT ;
							$Macro = new $prow["indicator"]["macros"]($prow["contract_id"], $prow["indicator"]["conditions"]);
							$prow["val"] = $Macro->getVal();
							$prow["indicator"]["conditions"] = $Macro->getCond();
						}
						
							// CF::Debug($prow);
						require_once INCLUDES . "/effect/" . "Condition" . CLASS_EXT ;
						$Condition = new Condition($prow["indicator"]["formula"], $prow["indicator"]["conditions"], array(0=>$prow["val"]), $contract["post_id"]);
						if(!empty($prow["val"])) {
							$prow["result"] = $Condition->getResult();
						} 
						else {
							$prow["result"] = false;
						}
						$prow["conditions"] = $Condition->getCond();
						$prow["val"] = $Condition->getVal();
						

						
						$result["complete"] = $row["result"];
						$result["cond"] = 
							(is_array($prow["conditions"]) 
							? str_replace(".", ",", $prow["conditions"][$contract["post_id"]]) 
							: str_replace(".", ",", $prow["conditions"]));
						$result["val"] = empty($prow["val"][0]) 
							? 0 : str_replace(".", ",", round($prow["val"][0], 2));
						
						
						
						
						$prow["result2"] = $result;
						
						
					// if(!empty($prow["fields"]))
						$this->output["pay"][$department["faculty_id"]][$department["id"]][$user_id][$prow["indicator_id"]] = $prow["result2"];
				
				}
				
				
				
			}	
	}
			return $this->output;
		}

}
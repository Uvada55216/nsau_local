<?
require_once INCLUDES . "/effect/" . "Contract" . CLASS_EXT ;


class PeopleBaseReport extends Contract {
	
	public $output;
		
	
	public function __construct() {
		parent::__construct();
	}
	
	
	protected function countFields($arr) {
		for($i=0;$i<=9;$i++) {
			if(!empty($arr["text".$i]))
				$result++;
		}
		for($i=0;$i<=1;$i++) {
			if(!empty($arr["file".$i]))
				$result++;
		}
		return $result;
	}
	
}
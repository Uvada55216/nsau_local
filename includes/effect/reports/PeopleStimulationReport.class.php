<?
require_once INCLUDES . "/effect/" . "Formula" . CLASS_EXT ;
require_once INCLUDES . "/effect/reports/" . "PeopleBaseReport" . CLASS_EXT ;


class PeopleStimulationReport extends PeopleBaseReport {
	
	public $output;
		
	
	public function __construct() {
		parent::__construct();
	}
	
	public function createReportByPeople($contract_id) {
		global $EFF;
		
		$EFF->SetTable("effect_plan", "ep");
		$EFF->AddCondFS("ep.contract_id", "=", $contract_id);
		
		$EFF->AddTable("indicators", "i");
		$EFF->AddCondFS("i.ind_type", "=", 0);
		$EFF->AddCondFF("i.id", "=", "ep.indicator_id");
		$EFF->AddField("ep.id", "id");
		$EFF->AddField("ep.user_id", "user_id");
		$EFF->AddField("ep.indicator_id", "indicator_id");
		$EFF->AddField("ep.contract_id", "contract_id");
		
		$pres = $EFF->Select();
		
		while($prow = $EFF->FetchAssoc($pres)) {
			
			$EFF->SetTable("indicators");
			$EFF->AddCondFS("id", "=", $prow["indicator_id"]);
			$indicator = $EFF->FetchAssoc($EFF->Select());
			$prow["indicator"] = $indicator;

			$EFF->SetTable("effect_indicators", "ei");
			$EFF->AddCondFS("ei.plan_id", "=", $prow["id"]);
			$ires = $EFF->Select();
			while($irow = $EFF->FetchAssoc($ires)) {
				$prow["fields"][$irow["id"]] = $irow;
				$prow["haystack"][$irow["id"]] = $this->countFields($irow);	

				$EFF->SetTable("forms");
				$EFF->AddCondFS("indicator_id", "=", $prow["indicator_id"]);
				$EFF->AddOrder("id");
				$rrr = $EFF->Select();
				while($qqq =  $EFF->FetchAssoc($rrr))
					$test++;
					$prow["needle"][$irow["id"]] = $test;
				unset($test);
				
				if($prow["needle"][$irow["id"]]<=$prow["haystack"][$irow["id"]]) {
					$form = new Formula($indicator["formula"], $irow, $indicator["size"], $this->getContractRate($contract_id));
					$prow["fields"][$irow["id"]]["payment"] = $form->getPayment();
					$prow["payments"] += $prow["fields"][$irow["id"]]["payment"];
				}
				
			}	
			if(!empty($prow["fields"]))
				$this->output["pay"][$prow["id"]] = $prow;
		} 
			
		return $this->output;
	}
	

	
}
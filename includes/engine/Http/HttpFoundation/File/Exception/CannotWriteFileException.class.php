<?php

namespace engine\Http\HttpFoundation\File\Exception;

/**
 * Thrown when an UPLOAD_ERR_CANT_WRITE error occurred with UploadedFile.
 *
 */
class CannotWriteFileException extends FileException
{
}

<?php

namespace engine\Http\HttpFoundation\File\Exception;

/**
 * Thrown when an error occurred in the component File.
 *
 */
class FileException extends \RuntimeException
{
}

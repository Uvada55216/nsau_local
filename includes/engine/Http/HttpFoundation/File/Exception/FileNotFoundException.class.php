<?php

namespace engine\Http\HttpFoundation\File\Exception;

/**
 * Thrown when a file was not found.
 *
 */
class FileNotFoundException extends FileException
{
    public function __construct($path)
    {
        parent::__construct(sprintf('The file "%s" does not exist', $path));
    }
}

<?php

namespace engine\Http\HttpFoundation\File\Exception;

/**
 * Thrown when an UPLOAD_ERR_FORM_SIZE error occurred with UploadedFile.
 */
class FormSizeFileException extends FileException
{
}

<?php

namespace engine\Http\HttpFoundation\File\Exception;

/**
 * Thrown when an UPLOAD_ERR_INI_SIZE error occurred with UploadedFile.
 */
class IniSizeFileException extends FileException
{
}

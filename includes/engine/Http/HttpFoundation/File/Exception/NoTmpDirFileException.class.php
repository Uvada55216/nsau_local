<?php

namespace engine\Http\HttpFoundation\File\Exception;

/**
 * Thrown when an UPLOAD_ERR_NO_TMP_DIR error occurred with UploadedFile.
 */
class NoTmpDirFileException extends FileException
{
}

<?php

namespace engine\Http\HttpFoundation\File\Exception;

/**
 * Thrown when an UPLOAD_ERR_PARTIAL error occurred with UploadedFile.
 */
class PartialFileException extends FileException
{
}

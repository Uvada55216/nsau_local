<?php

namespace engine\Http\HttpFoundation\File\Exception;

/**
 * Thrown when an error occurred during file upload.
 */
class UploadException extends FileException
{
}

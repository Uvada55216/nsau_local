<?php

namespace engine\Http\HttpFoundation\File\MimeType;

use engine\Http\HttpFoundation\File\Exception\AccessDeniedException;
use engine\Http\HttpFoundation\File\Exception\FileNotFoundException;

/**
 * Guesses the mime type of a file.
 *
 * @deprecated since Symfony 4.3, use {@link MimeTypesInterface} instead
 */
interface MimeTypeGuesserInterface
{
    /**
     * Guesses the mime type of the file with the given path.
     *
     * @param string $path The path to the file
     *
     * @return string|null The mime type or NULL, if none could be guessed
     *
     * @throws FileNotFoundException If the file does not exist
     * @throws AccessDeniedException If the file could not be read
     */
    public function guess($path);
}

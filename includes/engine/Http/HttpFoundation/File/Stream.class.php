<?php

namespace engine\Http\HttpFoundation\File;

/**
 * A PHP stream of unknown size.
 *
 */
class Stream extends File
{
    /**
     * {@inheritdoc}
     */
    public function getSize()
    {
        return false;
    }
}

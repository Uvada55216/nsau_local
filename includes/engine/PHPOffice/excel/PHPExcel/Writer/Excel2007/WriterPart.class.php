<?php

namespace engine\PHPOffice\excel\PHPExcel\Writer\Excel2007;

use engine\PHPOffice\excel\PHPExcel\Writer\WriterException;
use engine\PHPOffice\excel\PHPExcel\Writer\WriterIWriter;

/**
 * Writer_Excel2007_WriterPart
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    WriterExcel2007
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt    LGPL
 * @version    ##VERSION##, ##DATE##
 */
abstract class Writer_Excel2007_WriterPart
{
    /**
     * Parent IWriter object
     *
     * @var WriterIWriter
     */
    private $parentWriter;

    /**
     * Set parent IWriter object
     *
     * @param WriterIWriter    $pWriter
     * @throws WriterException
     */
    public function setParentWriter(WriterIWriter $pWriter = null)
    {
        $this->parentWriter = $pWriter;
    }

    /**
     * Get parent IWriter object
     *
     * @return WriterIWriter
     * @throws WriterException
     */
    public function getParentWriter()
    {
        if (!is_null($this->parentWriter)) {
            return $this->parentWriter;
        } else {
            throw new WriterException("No parent WriterIWriter assigned.");
        }
    }

    /**
     * Set parent IWriter object
     *
     * @param WriterIWriter    $pWriter
     * @throws WriterException
     */
    public function __construct(WriterIWriter $pWriter = null)
    {
        if (!is_null($pWriter)) {
            $this->parentWriter = $pWriter;
        }
    }
}

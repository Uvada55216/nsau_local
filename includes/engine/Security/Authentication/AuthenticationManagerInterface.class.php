<?php

namespace engine\Security\Authentication;

use engine\Security\Authentication\Token\TokenInterface;

/**
 * Интерфейс для менеджеров аутентификации,
 * процесс проверки подлинности на основе маркеров.
 */
interface AuthenticationManagerInterface
{
	/**
	 * Пытается аутентифицировать объект интерфейса токена
	 *
	 * @return TokenInterface An authenticated TokenInterface instance, never null
	 *
	 * @throws AuthenticationException if the authentication fails
	 */
	public function authenticate(TokenInterface $token);
}

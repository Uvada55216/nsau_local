<?php

namespace engine\Security\Authentication\Provider;

use engine\Security\Authentication\AuthenticationManagerInterface;
use engine\Security\Authentication\Token\TokenInterface;

/**
 * AuthenticationProviderInterface-это интерфейс для всех видов аутентификации
 * услуги поставщиков.
 *
 * Конкретные реализации обрабатывают конкретные экземпляры токенов.
 *
 */
interface AuthenticationProviderInterface extends AuthenticationManagerInterface
{
    /**
     * Используйте эту константу для не предоставленного имени пользователя.
     *
     * @var string
     */
    const USERNAME_NONE_PROVIDED = 'NONE_PROVIDED';

    /**
     * Проверяет, поддерживает ли данный поставщик данный токен.
     *
     * @return bool true if the implementation supports the Token, false otherwise
     */
    public function supports(TokenInterface $token);
}

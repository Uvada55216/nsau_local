<?php

namespace engine\Security\Authentication\Token\Storage;

use engine\Security\Authentication\Token\TokenInterface;

/**
 * Хранилище токенов содержит интерфейс токенов.
 *
 * Он предоставляет доступ к токену, представляющему текущую аутентификацию пользователя.
 *
 */
class TokenStorage implements TokenStorageInterface
{
	private $token;
	private $initializer;

	/**
	 *
	 */
	public function getToken()
	{
		if ($initializer = $this->initializer) {
			$this->initializer = null;
			$initializer();
		}

		return $this->token;
	}

	/**
	 *
	 */
	public function setToken(TokenInterface $token = null)
	{
		if (null !== $token && !method_exists($token, 'getRoleNames')) {
			@trigger_error(sprintf('Not implementing the "%s::getRoleNames()" method in "%s" is deprecated since Symfony 4.3.', "TokenInterface", \get_class($token)), \E_USER_DEPRECATED);
		}

		if ($token) {
			// ensure any initializer is called
			$this->getToken();
		}

		$this->initializer = null;
		$this->token = $token;
	}

	public function setInitializer($initializer)
	{
		$this->initializer = $initializer;
	}

	public function reset()
	{
		$this->setToken(null);
	}
}

<?php

namespace engine\Security\Authentication\Token\Storage;

use engine\Security\Authentication\Token\TokenInterface;

/**
 * The TokenStorageInterface.
 *
 */
interface TokenStorageInterface
{
	/**
	 * Возвращает текущий маркер безопасности.
	 *
	 * return TokenInterface|null A TokenInterface instance or null if no authentication information is available
	 */
	public function getToken();

	/**
	 * Устанавливает маркер аутентификации.
	 *
	 * param TokenInterface $token A TokenInterface token, or null if no further authentication information should be stored
	 */
	public function setToken(TokenInterface $token = null);
}

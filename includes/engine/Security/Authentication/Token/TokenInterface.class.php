<?php

namespace engine\Security\Authentication\Token;

/**
 * Интерфейс токена - это интерфейс для аутентификации пользователя.
 *
 * method string[] getRoleNames()
 */
interface TokenInterface
{
	/**
	 * Возвращает строковое представление токена.
	 *
	 * Это должно использоваться только для целей отладки.
	 *
	 * @return string
	 */
	public function __toString();

	/**
	 * Возвращает роли пользователей.
	 *
	 * @return Role[] массив экземпляров ролей
	 *
	 */
	public function getRoleNames();

	/**
	 * Возвращает учетные данные пользователя.
	 *
	 * @return mixed The user credentials
	 */
	public function getCredentials();

	/**
	 * Возвращает представление пользователя.
	 *
	 * @return string|\Stringable|UserInterface
	 *
	 * @see AbstractToken::setUser()
	 */
	public function getUser();

	/**
	 * Устанавливает пользователя в токен.
	 *
	 * Пользователь может быть экземпляром пользовательского интерфейса или объектом, реализующим
	 * метод __toString или имя пользователя в виде обычной строки.
	 *
	 * @param string|\Stringable|UserInterface $user
	 *
	 * @throws \InvalidArgumentException
	 */
	public function setUser($user);

	/**
	 * Возвращает имя пользователя.
	 *
	 * @return string
	 */
	public function getUsername();

	/**
	 * Возвращает, аутентифицирован ли пользователь или нет.
	 *
	 * @return bool true if the token has been authenticated, false otherwise
	 */
	public function isAuthenticated();

	/**
	 * Устанавливает флаг аутентификации.
	 *
	 * @param bool $isAuthenticated The authenticated flag
	 */
	public function setAuthenticated($isAuthenticated);

	/**
	 * Удаляет конфиденциальную информацию из токена.
	 */
	public function eraseCredentials();

	/**
	 * Возвращает атрибуты токена.
	 *
	 * @return array The token attributes
	 */
	public function getAttributes();

	/**
	 * Наборы атрибутов маркера.
	 *
	 * @param array $attributes The token attributes
	 */
	public function setAttributes(array $attributes);

	/**
	 * Возвращает true, если атрибут существует.
	 *
	 * @param string $name имя атрибута
	 *
	 * @return bool true if the attribute exists, false otherwise
	 */
	public function hasAttribute($name);

	/**
	 * Возвращает значение атрибута.
	 *
	 * @param string $name имя атрибута
	 *
	 * @return mixed The attribute value
	 *
	 * @throws \InvalidArgumentException When attribute doesn't exist for this token
	 */
	public function getAttribute($name);

	/**
	 * Устанавливает атрибут.
	 *
	 * @param string $name  The attribute name
	 * @param mixed  $value The attribute value
	 */
	public function setAttribute($name, $value);
}

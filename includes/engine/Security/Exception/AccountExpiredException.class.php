<?php

namespace engine\Security\Exception;

/**
 * AccountExpiredException is thrown when the user account has expired.
 *
 */
class AccountExpiredException extends AccountStatusException
{
    /**
     *
     */
    public function getMessageKey()
    {
        return 'Account has expired.';
    }
}

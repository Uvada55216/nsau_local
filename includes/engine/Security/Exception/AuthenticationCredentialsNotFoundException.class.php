<?php

namespace engine\Security\Exception;

/**
 * AuthenticationCredentialsNotFoundException is thrown when an authentication is rejected
 * because no Token is available.
 */
class AuthenticationCredentialsNotFoundException extends AuthenticationException
{
    /**
     *
     */
    public function getMessageKey()
    {
        return 'Authentication credentials could not be found.';
    }
}

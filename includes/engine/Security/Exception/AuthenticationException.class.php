<?php

namespace engine\Security\Exception;

use engine\Security\Authentication\Token\TokenInterface;

/**
 * AuthenticationException is the base class for all authentication exceptions.
 * AuthenticationException-это базовый класс для всех исключений аутентификации.
 *
 */
class AuthenticationException extends RuntimeException
{
	private $token;

	/**
	 * Get the token.
	 *
	 * return TokenInterface|null
	 */
	public function getToken()
	{
		return $this->token;
	}

	public function setToken(TokenInterface $token)
	{
		$this->token = $token;
	}

	/**
	 * internal
	 */
	public function __sleep()
	{

	}

	/**
	 * internal
	 */
	public function __wakeup()
	{

	}

	/**
	 * Ключ сообщения, который будет использоваться компонентом перевода.
	 *
	 * return string
	 */
	public function getMessageKey()
	{
		return 'An authentication exception occurred.';
	}

	/**
	 * Данные сообщения, которые будут использоваться компонентом перевода.
	 *
	 * return array
	 */
	public function getMessageData()
	{
		return array();
	}
}

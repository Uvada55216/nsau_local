<?php

namespace engine\Security\Exception;

/**
 * AuthenticationExpiredException is thrown when an authenticated token becomes un-authenticated between requests.
 *
 * In practice, this is due to the User changing between requests (e.g. password changes),
 * causes the token to become un-authenticated.
 *
 */
class AuthenticationExpiredException extends AccountStatusException
{
    /**
     *
     */
    public function getMessageKey()
    {
        return 'Authentication expired because your account information has changed.';
    }
}

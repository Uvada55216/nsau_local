<?php

namespace engine\Security\Exception;

/**
 * Исключение AuthenticationServiceException возникает, когда запрос Аутентификации не может быть обработан из-за системной проблемы.
 *
 */
class AuthenticationServiceException extends AuthenticationException
{
    /**
     *
     */
    public function getMessageKey()
    {
        return 'Authentication request could not be processed due to a system problem.';
    }
}

<?php

namespace engine\Security\Exception;

/**
 * Исключение BadCredentialsException возникает, когда учетные данные пользователя недействительны.
 *
 */
class BadCredentialsException extends AuthenticationException
{
    /**
     *
     */
    public function getMessageKey()
    {
        return 'Invalid credentials.';
    }
}

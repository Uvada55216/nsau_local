<?php

namespace engine\Security\Exception;

/**
 * This exception is thrown when the RememberMeServices implementation
 * detects that a presented cookie has already been used by someone else.
 *
 */
class CookieTheftException extends AuthenticationException
{
    /**
     *
     */
    public function getMessageKey()
    {
        return 'Cookie has already been used by someone else.';
    }
}

<?php

namespace engine\Security\Exception;

/**
 * CredentialsExpiredException is thrown when the user account credentials have expired.
 */
class CredentialsExpiredException extends AccountStatusException
{
    /**
     *
     */
    public function getMessageKey()
    {
        return 'Credentials have expired.';
    }
}

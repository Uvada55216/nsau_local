<?php

namespace engine\Security\Exception;

/**
 * DisabledException is thrown when the user account is disabled.
 */
class DisabledException extends AccountStatusException
{
    /**
     *
     */
    public function getMessageKey()
    {
        return 'Account is disabled.';
    }
}

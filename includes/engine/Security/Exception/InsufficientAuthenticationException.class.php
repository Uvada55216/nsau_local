<?php

namespace engine\Security\Exception;

/**
 * InsufficientAuthenticationException is thrown if the user credentials are not sufficiently trusted.
 *
 * This is the case when a user is anonymous and the resource to be displayed has an access role.
 */
class InsufficientAuthenticationException extends AuthenticationException
{
    /**
     *
     */
    public function getMessageKey()
    {
        return 'Not privileged to request the resource.';
    }
}

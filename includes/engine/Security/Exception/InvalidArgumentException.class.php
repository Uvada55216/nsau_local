<?php

namespace engine\Security\Exception;

/**
 * Base InvalidArgumentException for the Security component.
 *
 */
class InvalidArgumentException extends \InvalidArgumentException implements ExceptionInterface
{
}

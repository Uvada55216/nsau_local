<?php

namespace engine\Security\Exception;

/**
 * This exception is thrown when the csrf token is invalid.
 */
class InvalidCsrfTokenException extends AuthenticationException
{
    /**
     *
     */
    public function getMessageKey()
    {
        return 'Invalid CSRF token.';
    }
}

<?php

namespace engine\Security\Exception;

use engine\Http\HttpFoundation\Response;

/**
 * A signaling exception that wraps a lazily computed response.
 *
 */
class LazyResponseException extends \Exception implements ExceptionInterface
{
    private $response;

    public function __construct(Response $response)
    {
        $this->response = $response;
    }

    public function getResponse()
    {
        return $this->response;
    }
}

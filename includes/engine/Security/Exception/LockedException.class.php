<?php

namespace engine\Security\Exception;

/**
 * LockedException is thrown if the user account is locked.
 */
class LockedException extends AccountStatusException
{
    /**
     *
     */
    public function getMessageKey()
    {
        return 'Account is locked.';
    }
}

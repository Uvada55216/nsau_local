<?php

namespace engine\Security\Exception;

/**
 * Base LogicException for the Security component.
 */
class LogicException extends \LogicException implements ExceptionInterface
{
}

<?php

namespace engine\Security\Exception;

/**
 * LogoutException is thrown when the account cannot be logged out.
 */
class LogoutException extends RuntimeException
{
    public function __construct($message = 'Logout Exception', $previous = null)
    {
        parent::__construct($message, 403, $previous);
    }
}

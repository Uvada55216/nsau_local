<?php

namespace engine\Security\Exception;

/**
 * ProviderNotFoundException is thrown when no AuthenticationProviderInterface instance
 * supports an authentication Token.
 */
class ProviderNotFoundException extends AuthenticationException
{
    /**
     *
     */
    public function getMessageKey()
    {
        return 'No authentication provider found to support the authentication token.';
    }
}

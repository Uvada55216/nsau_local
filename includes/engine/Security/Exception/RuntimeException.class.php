<?php

namespace engine\Security\Exception;

/**
 * Base RuntimeException for the Security component.
 *
 */
class RuntimeException extends \RuntimeException implements ExceptionInterface
{
}

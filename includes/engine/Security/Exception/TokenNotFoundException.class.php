<?php

namespace engine\Security\Exception;

/**
 * TokenNotFoundException is thrown if a Token cannot be found.
 */
class TokenNotFoundException extends AuthenticationException
{
    /**
     *
     */
    public function getMessageKey()
    {
        return 'No token could be found.';
    }
}

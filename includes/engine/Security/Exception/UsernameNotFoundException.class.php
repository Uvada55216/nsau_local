<?php

namespace engine\Security\Exception;

/**
 * UsernameNotFoundException создается, если пользователь не может быть найден по своему имени пользователя.
 *
 */
class UsernameNotFoundException extends AuthenticationException
{
    private $username;

    /**
     * 
     */
    public function getMessageKey()
    {
        return 'Username could not be found.';
    }

    /**
     * Get the username.
     *
     * return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set the username.
     *
     * param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * 
     */
    public function getMessageData()
    {
        return array('{{ username }}' => $this->username);
    }
}

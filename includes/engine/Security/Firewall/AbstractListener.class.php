<?php

namespace engine\Security\Firewall;

use engine\Http\HttpFoundation\Request;

/**
 * A base class for listeners that can tell whether they should authenticate incoming requests.
 *
 */
abstract class AbstractListener implements ListenerInterface
{
    final public function __invoke(Request $request)
    {
	    if (false != $this->supports($request)) {
		    $this->authenticate($request);
	    }
	}

    /**
     * Tells whether the authenticate() method should be called or not depending on the incoming request.
     *
     * Returning null means authenticate() can be called lazily when accessing the token storage.
     */
    abstract public function supports(Request $request);

    /**
     * Does whatever is required to authenticate the request, typically calling $event->setResponse() internally.
     */
    abstract public function authenticate(Request $request);
}

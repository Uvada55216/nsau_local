<?php

namespace engine\Security\Firewall;

use engine\Http\HttpFoundation\Request;

/**
 * Interface that must be implemented by firewall listeners.
 *
 */
interface ListenerInterface
{
    public function __invoke(Request $request);
}

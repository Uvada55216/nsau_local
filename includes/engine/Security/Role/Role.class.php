<?php

namespace engine\Security\Role;

/**
 * Роль-это простая реализация, представляющая роль, идентифицируемую строкой.
 *
 */
class Role
{
	private $role;

	public function __construct($role)
	{
		if (\func_num_args() < 2 || func_get_arg(1)) {
			@trigger_error(sprintf('The "%s" class is deprecated since Symfony 4.3 and will be removed in 5.0. Use strings as roles instead.', __CLASS__), \E_USER_DEPRECATED);
		}

		$this->role = (string)$role;
	}

	/**
	 * Возвращает строковое представление роли.
	 *
	 * @return string
	 */
	public function getRole()
	{
		return $this->role;
	}

	public function __toString()
	{
		return $this->role;
	}
}

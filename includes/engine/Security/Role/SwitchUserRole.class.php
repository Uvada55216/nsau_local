<?php

namespace engine\Security\Role;

use engine\Security\Authentication\Token\TokenInterface;

/**
 * SwitchUserRole используется, когда текущий пользователь временно олицетворяет другого пользователя.
 *
 */
class SwitchUserRole extends Role
{
	private $deprecationTriggered = false;
	private $source;

	/**
	 * @param string $role роль в виде строки
	 */
	public function __construct($role, TokenInterface $source)
	{
		if ($triggerDeprecation = \func_num_args() < 3 || func_get_arg(2)) {
			@trigger_error(sprintf('The "%s" class is deprecated since Symfony 4.3 and will be removed in 5.0. Use strings as roles instead.', __CLASS__), \E_USER_DEPRECATED);

			$this->deprecationTriggered = true;
		}

		parent::__construct($role, $triggerDeprecation);

		$this->source = $source;
	}

	/**
	 * Возвращает исходный токен.
	 *
	 * возврат интерфейса токена исходный экземпляр интерфейса токена
	 */
	public function getSource()
	{
		if (!$this->deprecationTriggered && (\func_num_args() < 1 || func_get_arg(0))) {
			@trigger_error(sprintf('The "%s" class is deprecated since version 4.3 and will be removed in 5.0. Use strings as roles instead.', __CLASS__), \E_USER_DEPRECATED);

			$this->deprecationTriggered = true;
		}

		return $this->source;
	}
}

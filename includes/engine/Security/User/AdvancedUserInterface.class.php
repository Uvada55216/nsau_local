<?php

namespace engine\Security\User;

/**
 * Добавляет дополнительные функции в класс пользователя, связанные с флагами состояния учетной записи.
 *
 * Этот интерфейс может быть реализован вместо пользовательского интерфейса, если вы хотите
 * система аутентификации для учета различных флагов состояния учетной записи
 * во время аутентификации. Если какой-либо из методов в этом интерфейсе возвращает
 * false, аутентификация завершится неудачно.
 *
 * Если вам нужно выполнить пользовательскую логику для любой из этих ситуаций, то
 * вам нужно будет зарегистрировать прослушиватель исключений и следить за конкретными
 * экземпляры исключений, создаваемые в каждом конкретном случае. Все исключения являются подклассом
 * исключение AccountStatusException
 *
 * @see UserInterface
 * @see AccountStatusException
 */
interface AdvancedUserInterface extends UserInterface
{
	/**
	 * Проверяет, истек ли срок действия учетной записи пользователя.
	 *
	 * Внутренне, если этот метод возвращает false, система аутентификации
	 * вызовет исключение AccountExpiredException и предотвратит вход в систему.
	 *
	 * return bool true if the user's account is non expired, false otherwise
	 *
	 * @see AccountExpiredException
	 */
	public function isAccountNonExpired();

	/**
	 * Проверяет, заблокирован ли пользователь.
	 *
	 * Внутренне, если этот метод возвращает false, система аутентификации
	 * вызовет исключение LockedException и предотвратит вход в систему.
	 *
	 * return bool true if the user is not locked, false otherwise
	 *
	 * @see LockedException
	 */
	public function isAccountNonLocked();

	/**
	 * Проверяет, истек ли срок действия учетных данных пользователя (пароля).
	 *
	 * Внутренне, если этот метод возвращает false, система аутентификации
	 * вызовет исключение CredentialsExpiredException и предотвратит вход в систему.
	 *
	 * return bool true if the user's credentials are non expired, false otherwise
	 *
	 * @see CredentialsExpiredException
	 */
	public function isCredentialsNonExpired();

	/**
	 * Проверяет, включен ли пользователь.
	 *
	 * Внутренне, если этот метод возвращает false, система аутентификации
	 * вызовет отключенное исключение и предотвратит вход в систему.
	 *
	 * return bool true if the user is enabled, false otherwise
	 *
	 * @see DisabledException
	 */
	public function isEnabled();
}

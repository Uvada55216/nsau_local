<?php

namespace engine\Security\User;

use engine\Security\Exception\UnsupportedUserException;
use engine\Security\Exception\UsernameNotFoundException;

/**
 * InMemoryUserProvider-это простой непостоянный пользовательский поставщик.
 *
 * Полезно для тестирования, демонстрации, прототипирования и для простых нужд
 * (например, бэкэнд с уникальным администратором)
 *
 */
class InMemoryUserProvider implements UserProviderInterface
{
	private $users;

	/**
	 * Массив пользователей-это хэш, где ключами являются имена пользователей, а значениями
	 * -массив атрибутов: "пароль", "включено" и "роли".
	 *
	 * @param array $users An array of users
	 */
	public function __construct(array $users = array())
	{
		foreach ($users as $username => $attributes) {
			$password = isset($attributes['password']) ? $attributes['password'] : null;
			$enabled = isset($attributes['enabled']) ? $attributes['enabled'] : true;
			$roles = isset($attributes['roles']) ? $attributes['roles'] : array();
			$user = new User($username, $password, $roles, $enabled, true, true, true);

			$this->createUser($user);
		}
	}

	/**
	 * Добавляет нового пользователя к поставщику.
	 *
	 * @throws \LogicException
	 */
	public function createUser(UserInterface $user)
	{
		if (isset($this->users[strtolower($user->getUsername())])) {
			throw new \LogicException('Another user with the same username already exists.');
		}

		$this->users[strtolower($user->getUsername())] = $user;
	}

	/**
	 * @see UserProviderInterface
	 */
	public function loadUserByUsername($username)
	{
		$user = $this->getUser($username);

		return new User($user->getUsername(), $user->getPassword(), $user->getRoles(), $user->isEnabled(), $user->isAccountNonExpired(), $user->isCredentialsNonExpired(), $user->isAccountNonLocked());
	}

	/**
	 * @see UserProviderInterface
	 */
	public function refreshUser(UserInterface $user)
	{
		if (!$user instanceof User) {
			throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
		}

		$storedUser = $this->getUser($user->getUsername());

		return new User($storedUser->getUsername(), $storedUser->getPassword(), $storedUser->getRoles(), $storedUser->isEnabled(), $storedUser->isAccountNonExpired(), $storedUser->isCredentialsNonExpired() && $storedUser->getPassword() === $user->getPassword(), $storedUser->isAccountNonLocked());
	}

	/**
	 * @see UserProviderInterface
	 */
	public function supportsClass($class)
	{
//		return 'engine\Security\User\User' === $class;
		return \get_class($class);
	}

	/**
	 * Возвращает пользователя по заданному имени пользователя.
	 *
	 * @throws UsernameNotFoundException if user whose given username does not exist
	 */
	private function getUser($username)
	{
		if (!isset($this->users[strtolower($username)])) {
			$ex = new UsernameNotFoundException('Authentication failed. Invalid user name.');
			$ex->setUsername($username);

			throw $ex;
		}

		return $this->users[strtolower($username)];
	}
}

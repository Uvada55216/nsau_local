<?php

namespace engine\Security\User;

/**
 * Пользователь-это пользовательская реализация, используемая поставщиком пользователей в памяти.
 */
final class User implements UserInterface, EquatableInterface, AdvancedUserInterface
{
	private $username;
	private $password;
	private $enabled;
	private $accountNonExpired;
	private $credentialsNonExpired;
	private $accountNonLocked;
	private $roles;
	private $extraFields;

	public function __construct($username, $password, array $roles = array(), $enabled = true, $userNonExpired = true, $credentialsNonExpired = true, $userNonLocked = true, array $extraFields = array())
	{
		if ('' === $username || null === $username) {
			throw new \InvalidArgumentException('The username cannot be empty.');
		}

		$this->username = (string)$username;
		$this->password = (string)$password;
		$this->enabled = (bool)$enabled;
		$this->accountNonExpired = (bool)$userNonExpired;
		$this->credentialsNonExpired = (bool)$credentialsNonExpired;
		$this->accountNonLocked = (bool)$userNonLocked;
		$this->roles = $roles;
		$this->extraFields = $extraFields;
	}

	public function __toString()
	{
		return $this->getUsername();
	}

	/**
	 * @see AdvancedUserInterface
	 */
	public function getRoles()
	{
		return $this->roles;
	}

	/**
	 * @see AdvancedUserInterface
	 */
	public function getPassword()
	{
		return $this->password;
	}

	/**
	 * @see AdvancedUserInterface
	 */
	public function getSalt()
	{
		return null;
	}

	/**
	 * @see AdvancedUserInterface
	 */
	public function getUsername()
	{
		return $this->username;
	}

	/**
	 * @see AdvancedUserInterface
	 */
	public function isAccountNonExpired()
	{
		return $this->accountNonExpired;
	}

	/**
	 * @see AdvancedUserInterface
	 */
	public function isAccountNonLocked()
	{
		return $this->accountNonLocked;
	}

	/**
	 * @see AdvancedUserInterface
	 */
	public function isCredentialsNonExpired()
	{
		return $this->credentialsNonExpired;
	}

	/**
	 * @see AdvancedUserInterface
	 */
	public function isEnabled()
	{
		return $this->enabled;
	}

	/**
	 * @see AdvancedUserInterface
	 */
	public function eraseCredentials()
	{
	}

	/**
	 * @see AdvancedUserInterface
	 */
	public function getExtraFields()
	{
		return $this->extraFields;
	}

	/**
	 * @see EquatableInterface
	 */
	public function isEqualTo(UserInterface $user)
	{
		if (!$user instanceof self) {
			return false;
		}

		if ($this->getPassword() !== $user->getPassword()) {
			return false;
		}

		if ($this->getSalt() !== $user->getSalt()) {
			return false;
		}

		$currentRoles = array_map('strval', (array) $this->getRoles());
		$newRoles = array_map('strval', (array) $user->getRoles());
		$rolesChanged = \count($currentRoles) !== \count($newRoles) || \count($currentRoles) !== \count(array_intersect($currentRoles, $newRoles));
		if ($rolesChanged) {
			return false;
		}

		if ($this->getUsername() !== $user->getUsername()) {
			return false;
		}

		if ($this->isAccountNonExpired() !== $user->isAccountNonExpired()) {
			return false;
		}

		if ($this->isAccountNonLocked() !== $user->isAccountNonLocked()) {
			return false;
		}

		if ($this->isCredentialsNonExpired() !== $user->isCredentialsNonExpired()) {
			return false;
		}

		if ($this->isEnabled() !== $user->isEnabled()) {
			return false;
		}

		return true;
	}


	public function setPassword($password)
	{
		$this->password = (string)$password;
	}
}

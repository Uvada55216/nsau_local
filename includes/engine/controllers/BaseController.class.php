<?php

namespace engine\controllers;

use \helpers\EncodingHelper as H;

abstract class BaseController implements ControllerInterface
{
	/**
	 * Объект модуля
	 * @var \engine\modules\BaseModule
	 */
	public $Module;
	
	private $jsContent;

	public function __construct(\engine\modules\BaseModule $Creator) 
	{ 	
		$this->Module = $Creator;
	}

	public function render($view = "Default", $moduleData = null) 
	{
		$__requestUri = $this->Module->requestUri;
		$inputData = $moduleData['data'];
		ob_start();
		include "includes/modules/{$this->Module->className}/views/$view.php";
		$out = ob_get_contents();
		ob_end_clean();
		return $this->jsContent.$out;
	}

	public function renderBlock($view = "Default", $moduleData = null) 
	{
		$__requestUri = $this->Module->requestUri;
		$inputData = $moduleData['data'];
		ob_start();
		include "includes/modules/{$this->Module->className}/views/$view.php";
		$out = ob_get_contents();
		ob_end_clean();
		return $out;
	}

	public function renderAjax($view = "Default", $moduleData = null) 
	{
		header("Content-type: text/html; charset=windows-1251");
		include "includes/modules/{$this->Module->className}/views/$view.php";
	}

    public function renderJson($data, $arrayEncoding = null)
    {
        if ($arrayEncoding) {
            $data = H::changeArrayEncoding($data, $arrayEncoding);
        }
		$data = json_encode($data);
		ob_start();
		echo $data;
		$out = ob_get_contents();
		ob_end_clean();
		return $out;
	}

	public function renderJs($js, $moduleData = null, $vue = null)
	{
		$__route = str_replace('\\', '/', $this->Module->route);
		$__action = $this->Module->action;
		$__module = $this->Module->className;
		$__context = $this->Module->contextData->nodeId;
		ob_start();
		include "includes/modules/{$this->Module->className}/views/js/$js.js.php";
		if($vue) echo '<script type="text/javascript" src="/scripts/vue/dist/vue.min.js"><script src="https://unpkg.com/vee-validate@latest"></script></script><script src="/scripts/vue/vuejs-datepicker.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js"></script>';
		$out = ob_get_contents();
		ob_end_clean();
		$this->jsContent .= $out;
	}

	public function __call($name, $args)
	{
		throw new \Exception(get_class($this) . " - Unknown action $name (" . implode(',', $args) . ')');
	} 

}
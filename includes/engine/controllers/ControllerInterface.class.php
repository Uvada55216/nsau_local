<?php

namespace engine\controllers;

interface ControllerInterface
{
	public function __construct(\engine\modules\BaseModule $Creator) ;
	public function render();
}
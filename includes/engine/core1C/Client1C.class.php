<?
namespace engine\core1C;

use engine\db\Db;
/**
 * Êîííåêòèòñÿ ê 1ñ
 * Âûçûâàåò óäàëåííûå ïðîöåäóðû web ñåðâèñà
 * Àáñòðàêòíûé äëÿ òîãî ÷òîáû áûë ñîçäàí
 * íàñëåäíèê â êîòîðîì ìîæíî áóäåò îïèñàòü èñïîëüçóåìûå
 * óäàëåííûå ïðîöåäóðû web ñåðâèñîâ
 */
abstract class Client1C 
{
	protected $passwd;
	protected $login;
	protected $apiAdress;
	protected $alwaysCachableMethods;

	public $client;

	private $cacheLifetime = 1;
	private $useCache = true;

	private $user;

	public function __construct($init1c = true) 
	{
		$this->user = $GLOBALS['Auth'];
		if($init1c) {
			$this->init1c();
		}
	}
	
	public function __call($name, $arg) 
	{
		try { 
			if($this->useCache || in_array($name, $this->alwaysCachableMethods) || 1) {
				if(empty($this->client)) $this->init1c();
				return DbClient1c::init($this)->setCacheLifetime($this->cacheLifetime)->$name($arg[0]);
			} else {
				if(empty($this->client)) $this->init1c();
				// \CF::Debug(123);\CF::Debug($name);\CF::Debug($arg[0]);
				$result = $this->client->$name($arg[0]);
				return $result->return;
			}
		} catch (\Exception $e) {
			$msg = ($this->user->logged_in == 1 && $this->user->usergroup_id == 1)
				? $e->getMessage(). "<br>Bad $name"
				: \CF::Utf2Win("<h2>Сервис временно недоступен. Попробуйте позже!</h2>");
			throw new \Exception($msg);
		}
	}

	public function clearCache()
	{
		Db::init()->exec('TRUNCATE 1c_procedure_cache');
		return $this;
	}

	public function clearCachableMethods()
	{
		unset($this->alwaysCachableMethods);
		return $this;
	}

	public function setCacheLifetime($minutes) {
		$this->cacheLifetime = $minutes;
		return $this;
	}

	public function disableCache()
	{
		$this->useCache = false;
		return $this;
	}

	public function enableCache()
	{
		$this->useCache = true;
		return $this;
	}

	private function init1c() 
	{
		ini_set("soap.wsdl_cache_enabled", "0");
		ini_set('default_socket_timeout', 6000);
		try {  
			$this->client = new \SoapClient($this->apiAdress, 
		         array("trace"=>1,
		               "login" => $this->login,
		               "password" => $this->passwd,
		               "soap_version" => SOAP_1_1,
		               "features" => SOAP_SINGLE_ELEMENT_ARRAYS,
		        ));
		} catch (\Exception $e) {
			$msg = ($this->user->logged_in == 1 && $this->user->usergroup_id == 1)
				? "Bad init1c<br>" . $e->getMessage()
				: \CF::Utf2Win("<h2>Сервис временно недоступен. Попробуйте позже!</h2>");
			throw new \Exception($msg);
		}	
	}
}
<?php

namespace engine\core1C;

use DOMDocument;

class DbClient1c
{
	protected $cache;

	protected $procedureName;
	protected $params;
	protected $className;
	protected $client;
	protected $cacheLifeTime;


	public static function init(Client1C $Class) 
	{
		return new self($Class);
	}

	public function setCacheLifetime($minutes) {
		$this->cacheLifeTime = $minutes;
		return $this;
	}

	protected function __construct($Class) {
		$this->className = get_class($Class);
		$this->client = $Class->client;
		return $this;
	}

	public function __call($name, $arg)
	{
		global $DB;
        ini_set('max_execution_time', '300');
		$this->procedureName = $name;
		$this->params = $arg[0];
		$DateTime = new \DateTime("-{$this->cacheLifeTime} minutes");
		$row = $DB->SetTable('1c_procedure_cache')
			->AddCondFS('procedure_name', '=', $this->procedureName)
			->AddCondFS('procedure_params', '=', $this->convertJson($this->params))
			//->AddCondFS('class_name', '=', $this->className)
			//->AddCondFS('last_update', '>', $DateTime->format('Y-m-d H:i:s'))
			->SelectArray(1);
		if($row['cache']) {
			return json_decode($row['cache']); 
		} else {
			return $this->writeCache();
		}
	}

	protected function writeCache()
	{
		global $DB;
		$this->deleteCache();
		// \CF::Debug($this->client);
		// \CF::Debug($this->procedureName);
		// \CF::Debug($this->params);

		$result = $this->client->{$this->procedureName}($this->params);

		if ($this->params['idPk'] == '00000004f7' && $this->procedureName == 'getAbitBySpeciality'){
            $tmp = $this->params;
            $tmp['idPk'] = '000000048';
            $result1 = $this->client->{$this->procedureName}($tmp);
			foreach($result1->return->val as $abit) {
				$key = (trim($abit->peopleId) .':'.trim($abit->ConcursGroup) .':'. trim($abit->Priority));
				$result->return->valnew[$key] = $abit;
			}
            foreach($result->return->val as $abit) {
                $key = (trim($abit->peopleId) .':'.trim($abit->ConcursGroup) .':'. trim($abit->Priority));
                $result->return->valnew[$key] = $abit;
            }
			$result->return->val = $result->return->valnew;
			unset($result->return->valnew);

			$res = $this->getTextItems(35823);
			if($res){
				$html = iconv('windows-1251', 'utf-8', $res['text']);
				$arrOfHighPriorities = $this->getInfoFromText($html);
			}
			foreach($result->return->val as $key => $abit){
				if (isset($arrOfHighPriorities[$key])) {
					$abit->originalEducationDocuments = isset($arrOfHighPriorities[$key]['originalEducationDocuments'])?$arrOfHighPriorities[$key]['originalEducationDocuments']:false;
					$abit->highPriority = isset($arrOfHighPriorities[$key]['highPriority'])?$arrOfHighPriorities[$key]['highPriority']:false;
                    $abit->paid = isset($arrOfHighPriorities[$key]['paid'])?$arrOfHighPriorities[$key]['paid']:false;
				} else {
					unset($result->return->val[$key]);
				}
			}
		}

		$DB->SetTable('1c_procedure_cache')
			->AddValue('procedure_name', $this->procedureName)
			->AddValue('procedure_params', $this->convertJson($this->params))
			->AddValue('class_name', $this->className)
			->AddValue('cache', $this->convertJson($result->return))
			->AddValue('last_update', 'NOW()', 'X')
			->Insert();

		return $result->return;
	}

	protected function deleteCache() 
	{
		global $DB;
		$DB->SetTable('1c_procedure_cache')
			->AddCondFS('procedure_name', '=', $this->procedureName)
			->AddCondFS('procedure_params', '=', $this->convertJson($this->params))
			->AddCondFS('class_name', '=', $this->className)
			->Delete();
	}

	private function convertJson($json) 
	{
		if (get_magic_quotes_gpc()) {
			return quotemeta(json_encode($json));
		} else {
			return json_encode($json);
		}
	}

	private function getTextItems($id){
		global $DB;

		$DB->SetTable('text_items');
		$DB->AddCondFS('id', '=', $id);
		$DB->AddField('text');

		return $DB->SelectArray(1);
	}

	private function getInfoFromText($html) {
		$arrOfHighPriorities = array();
		$dom = new DOMDocument();
		$dom->loadHTML("\xEF\xBB\xBF".trim($html));
		$nod = $dom->getElementsByTagName('table');
		if ($nod->length) {
			$trList = $nod->item(0)->getElementsByTagName('tr');
			if($trList->length){
				for ($i_tr = 7; $i_tr < $trList->length; $i_tr++){
					$tdList = $trList->item($i_tr)->getElementsByTagName('td');
					if($tdList->length){
						$key = ($tdList->item(0)->nodeValue .':'.$tdList->item(4)->nodeValue .':'. $tdList->item(3)->nodeValue);
						$arrOfHighPriorities[$key]['originalEducationDocuments'] = (($tdList->item(1)->nodeValue == 'Оригинал') || ($tdList->item(2)->nodeValue))? true : false;
						//$arrOfHighPriorities[$key]['highPriority'] = ($tdList->item(9)->nodeValue)? true : false;
                        $arrOfHighPriorities[$key]['paid'] = ($tdList->item(5)->nodeValue)? true : false;
					}
				}
			}
		}
		return $arrOfHighPriorities;
	}

}
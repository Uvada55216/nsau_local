<?
namespace engine\core1C;

class Helper1C 
{
	/**
	 * utf8 -> cp1251
	 * @param string $str string
	 * @return string      result
	 */	
	public static function u2w($str) 
	{
		return iconv('utf-8', 'cp1251', $str);
	} 

	/**
	 * cp1251 -> utf8
	 * @param string $str string
	 * @return string      result
	 */
	public static function w2u($str) 
	{
		return iconv('cp1251', 'utf-8', $str);
	}

	/**
	 * CamelCase -> Camel case
	 * �������� ������ � 1251
	 * @param string $str string cp1251 
	 * @return string      result
	 */
	public static function CC2Str($str) 
	{
		$exceptions = array('�', '�');
		for ($i = 0; $i <= self::strlen($str); $i++) {
			if((mb_strtoupper($str[$i]) === $str[$i])
				&& !in_array($str[$i], $exceptions)) {
				if($i == 0) {
					$word .= $str[$i];
				} else {
					$word .= " " . mb_strtolower($str[$i]);
				}
			} else {
				$word .= $str[$i];
			}
		}

		return trim($word);
	}

	public static function strlen($str) {
		if (version_compare(PHP_VERSION, '5.4.0', '<')) {
			return mb_strlen($str);
		} 
		return strlen($str);
	}

}
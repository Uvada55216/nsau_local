<?php

namespace engine\db;

class Db 
{
	public static function init()
	{
		global $DB;
		return $DB;
	}
}
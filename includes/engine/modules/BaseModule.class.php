<?php

namespace engine\modules;

class BaseModule implements ModuleInterface
{
	/**
	 * 	результат выполнения модуля
	 * @var string
	 */
	private $output;
	/**
	 * Имя класса модуля
	 * @var string
	 */
	public $className;
	/**
	 * Пространство имен класса модуля
	 * @var string
	 */
	public $classNamespace;
	/**
	 * Uri запроса вызвавшего модуль
	 * @var string
	 */
	public $requestUri;
	/**
	 * 	Объект с настройками инициализации модуля
	 * (Настройки из управления разделами)
	 * @var \engine\modules\ModuleConfigInterface Object
	 */
	public $initParams;
	/**
	 * Путь до контроллера
	 * @var string
	 */
	public $route;
	/**
	 * Имя функции действия в контроллере
	 * @var strung
	 */
	public $action;
	/**
	 * Параметры передаваемые в action функцию
	 * @var array
	 */
	public $params;

	/**
	 * uri после uri расположения модуля.
	 * @var string
	 */
	public $contextData;

	/**
	 * 	
	 * @param ModuleInitInterface        $Init             Объект из которого можно получить
	 * Маршрут, действие, параметры.
	 * @param ModuleConfigInterface|null $moduleInitParams ПАраметры заданные для
	 * модуля в момент подключения модуля к определенному разделу на сайте.
	 */
	public function __construct(ModuleInitInterface $Init, 
								ModuleConfigInterface $moduleInitParams = null, 
								ContextData $contextData = null)
	{		
		$fullClassName = get_class($this);
		$cNameArr = explode('\\', $fullClassName);
		$this->className = array_pop($cNameArr);
		$this->classNamespace =  '\\' . implode('\\', $cNameArr);
		$this->requestUri = $_SERVER['REQUEST_URI'];
		$this->initParams = $moduleInitParams;
		$this->contextData = $contextData;

		$Init->configure($this); 
		$this->route =  $Init->getRoute();
		$this->action = $Init->getAction();
		$this->params = $Init->getParams();
		$this->initRoute();		
	}

	/**
	 * 	Выполнить код по маршруту, определенному в конструкторе.
	 */
	private function initRoute() {
		try {
			$Controller = new $this->route($this);
			if(!($Controller instanceof \engine\controllers\ControllerInterface)){
				throw new \Exception("$this->route not implements ControllerInterface");				
			}
			$this->output = call_user_func_array(array($Controller, trim($this->action)), $this->params ? $this->params : array()); 
		} catch(\Exception $e) {
			$this->output = $e->getMessage();
		}
	}

	public function Output() {
		return $this->output;
	}
}
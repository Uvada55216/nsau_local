<?php

namespace engine\modules;

abstract class BaseModuleConfig implements ModuleConfigInterface
{
	abstract public function params();

	public function runSaveCallbacks()
	{
		$params = $this->params();
		foreach ($params as $param) {
			if($param['onSave']) {				
				$param['onSave']->execute();
			}
		}		
	}

	public function runCreateCallbacks()
	{
		$params = $this->params();
		foreach ($params as $param) {
			if($param['onCreateForm']) {				
				$param['onCreateForm']->execute();
			}
		}		
	}

	protected function setError($error, $code = null)
	{
		throw new \Exception(get_class() . ' - ' . $error, $code);
	}

	public function __set($name, $value)
	{
		throw new \Exception("Varible $name not found in config class.");
	}
}
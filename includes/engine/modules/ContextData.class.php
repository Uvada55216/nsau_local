<?php

namespace engine\modules;

class ContextData
{
	public $nodeId;
	public $engineUri;

	public function __construct($nodeId, $engineUri) {
		$this->nodeId = $nodeId;
		$this->engineUri = $engineUri;
	}
}
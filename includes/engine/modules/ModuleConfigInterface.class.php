<?php

namespace engine\modules;

interface ModuleConfigInterface
{
	public function params();
	public function runSaveCallbacks();
	public function runCreateCallbacks();
}
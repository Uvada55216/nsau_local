<?php

namespace engine\modules;

interface ModuleInitInterface
{
	public function getRoute();
	public function getAction();
	public function getParams();
	public function configure(ModuleInterface $Module);
}
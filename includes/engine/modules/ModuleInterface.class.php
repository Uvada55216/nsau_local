<?php

namespace engine\modules;

interface ModuleInterface {
	public function __construct(ModuleInitInterface $Init, ModuleConfigInterface $moduleInitParams);
	public function Output();
}
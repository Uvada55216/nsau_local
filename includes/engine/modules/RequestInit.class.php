<?php

namespace engine\modules;

class RequestInit implements ModuleInitInterface
{
	private $route;
	private $action;
	private $params;
	private $request;

	public function __construct($request)
	{
		$this->request = $request;
	}

	public function configure(ModuleInterface $Module)
	{
		$Request = new \engine\url\RequestParser($this->request, $Module->rules());
		$this->route =  $Module->classNamespace  . '\\' .'controllers\\' .   $Request->namespace . $Request->controllerName;
		$this->action = $Request->actionName;
		$this->params = $Request->params;
	}

	public function getRoute()
	{
		return $this->route;
	}

	public function getAction()
	{
		return $this->action;
	}

	public function getParams()
	{
		return $this->params;
	}	
}
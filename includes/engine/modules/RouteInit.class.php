<?php

namespace engine\modules;

class RouteInit implements ModuleInitInterface
{
	private $route;
	private $action;
	private $params;

	public function __construct($route, $action, $params)
	{
		$this->route = $route;
		$this->action = $action;
		$this->params = $params;
	}

	public function configure(ModuleInterface $Module)
	{
		return true;
	}

	public function getRoute()
	{
		return $this->route;
	}

	public function getAction()
	{
		return $this->action;
	}

	public function getParams()
	{
		return $this->params;
	}	
}
<?php

namespace engine\modules\configElements;

class AjaxSearchSelect implements ConfigElementInterface
{ 
	private $data;
	private $current;
	private $selectParams;
	private $elementName;
	private $route;
	private $callback;

	public $name;

	public function __construct($elementName,
								$current, 
								$data, 
								$Creator, 
								$callback, 
								$pattern = array('label' => '%name', 'value' => '%id')) 
	{
		$this->route = str_replace('\\', '/', get_class($Creator));
		$this->callback = $callback;
		$this->name = $elementName;
		$this->elementName = "module_params[$elementName]";
		$this->current = $current;
		foreach ($data as $key => $val) {
			$this->data[$key]['label'] = $pattern['label'];
			$this->data[$key]['value'] = $pattern['value'];
			foreach($val as $index => $item) {
				$this->data[$key]['value'] = str_replace('%'.$index, $item, $this->data[$key]['value']);
				$this->data[$key]['label'] = str_replace('%'.$index, $item, $this->data[$key]['label']);
			}
		}
	}

	public function create()
	{
		ob_start();
		?>
<script type="text/javascript">
jQuery(document).ready(function($) {
	$(document).on('keyup', 'input.searchSelect[data-object="input"]', function() {
		var that = $(this);
		var text = $(this).val();
		if(text.length < 3) return false;
		var request = {'route': '<?=$this->route?>', 
					'callback': '<?=$this->callback?>', 
					'params': {text} };
		$.ajax({
			type: 'POST',
			dataType: 'html',
			url: '/ajax_mode/',
			data: request,
			success: function(data) {
				var obj = jQuery.parseJSON(data);
				var opt = "";
				$.each(obj, function(i, item) {
					opt = opt.concat('<option value = "'+item.id+'|'+item.name+'">' + item.name + '</option>');
				});
				that.closest('div').find('select.searchSelect').html(opt);
			}
		});
	});
	

});
</script>
<div class="input-group">
	<input type="text" class="form-control searchSelect" data-object="input" aria-label="...">
	<select
		name="<?=$this->elementName?>"
		class="form-control searchSelect" 
	>
		<option value=""></option>
		<?foreach($this->data as $value) {?>
			<option 
				<?=$value['value'] == $this->current ? 'selected' : ''?> 
				value="<?=$value['value']?>|<?=$value['label']?>">
					<?=$value['label']?>
			</option>
		<?}?>
	</select>
</div>
		<?
		$out = ob_get_contents();
		ob_end_clean();
		return $out;
	}


}
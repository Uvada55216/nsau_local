<?php

namespace engine\modules\configElements;

/**
 * Объект для запуска коллбеков
 */
class Callback
{
	private $object;
	private $function;
	private $params;

	/**
	 * 
	 * @param object/string $object   Объект или путь к классу
	 * @param string $function имя метода
	 * @param array $params   параметры
	 */
	public function __construct($object, $function, $params) {
		$this->object = $object;
		$this->function = $function;
		$this->params = $params;
	}

	public function execute()
	{
		return call_user_func_array(array($this->object, $this->function), $this->params ? $this->params : array());
	}
}
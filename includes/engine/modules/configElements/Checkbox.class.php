<?php

namespace engine\modules\configElements;

class Checkbox implements ConfigElementInterface
{
	private $value;
	private $elementName;

	public function __construct($elementName, $value) 
	{
		$this->value = $value;
		$this->elementName = "module_params[$elementName]";
	}

	public function create()
	{
		ob_start();
		?>
		<input 
			style="margin-left: 20px; margin-top: 8px"
			type="checkbox"
			name="<?=$this->elementName?>"
			value="true"
			<?=!empty($this->value) ? 'checked' : ''?>
		>
			
		<?
		$out = ob_get_contents();
		ob_end_clean();
		return $out;
	}


}
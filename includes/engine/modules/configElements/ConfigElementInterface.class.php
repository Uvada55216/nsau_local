<?php

namespace engine\modules\configElements;

interface ConfigElementInterface
{
	public function create();
}
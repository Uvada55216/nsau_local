<?php

namespace engine\modules\configElements;

class Input implements ConfigElementInterface
{
	private $value;
	private $elementName;

	public function __construct($elementName, $value) 
	{
		$this->value = $value;
		$this->elementName = "module_params[$elementName]";
	}

	public function create()
	{
		ob_start();
		?>
		<input 
			type="text"
			class="form-control"
			name="<?=$this->elementName?>"
			value="<?=$this->value?>"
		>
			
		<?
		$out = ob_get_contents();
		ob_end_clean();
		return $out;
	}


}
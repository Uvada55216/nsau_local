<?php

namespace engine\modules\configElements;

class Select implements ConfigElementInterface
{ 
	private $data;
	private $current;
	private $selectParams;
	private $elementName;

	public function __construct($elementName,
								$current, 
								$data, 
								$pattern = array('label' => '%name', 'value' => '%id')) 
	{
		$this->elementName = "module_params[$elementName]";
		$this->current = $current;
		foreach ($data as $key => $val) {
			$this->data[$key]['label'] = $pattern['label'];
			$this->data[$key]['value'] = $pattern['value'];
			foreach($val as $index => $item) {
				$this->data[$key]['value'] = str_replace('%'.$index, $item, $this->data[$key]['value']);
				$this->data[$key]['label'] = str_replace('%'.$index, $item, $this->data[$key]['label']);
			}
		}
	}

	public function create()
	{
		ob_start();
		?>
		<select
			name="<?=$this->elementName?>"
			class="form-control"
		>
			<option value=""></option>
			<?foreach($this->data as $value) {?>
				<option 
					<?=$value['value'] == $this->current ? 'selected' : ''?> 
					value="<?=$value['value']?>"><?=$value['label']?>
				</option>
			<?}?>
		</select>
		<?
		$out = ob_get_contents();
		ob_end_clean();
		return $out;
	}


}
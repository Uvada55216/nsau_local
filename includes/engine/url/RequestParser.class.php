<?php

namespace engine\url;

class RequestParser 
{
	protected $requestParams;
	protected $rules;
	protected $defaultController = 'DefaultController';
	protected $defaultAction = 'actionIndex';

	public $actionName;
	public $controllerName;
	public $namespace;
	public $params = null;

	public function __construct($request_url, $rules = null) 
	{		
		if($request_url[strlen($request_url)-1] == '/') $request_url[strlen($request_url)-1] = '';
		$request_url = !$request_url ? '/' : $request_url;

		$Alto = new AltoRouter();
		$Alto->addRoutes($rules);
		$match = $Alto->match(trim($request_url));
		if(!$match) {throw new \Exception("404");}
			
		//замена [something] подобных вхождений из target 
		//и удаление из параметров
		if(preg_match_all('`\[([^:\]]*+)(?::([^:\]]*+))?\](\?|)`', $match['target'], $matches, PREG_SET_ORDER)) {
			foreach ($matches as $value) {
				$match['target']= str_replace($value[0], $match['params'][$value[1]], $match['target']);
				unset($match['params'][$value[1]]);
			}
		}
		$this->params = $match['params'];

		$routeArr = explode('/', $match['target']);
		foreach ($routeArr as $i => $value) {
			if(strrpos($value, '#') !== false) {
				$x = explode('#', $value);
				$controller = $x[0];
				$controller[0] = strtoupper($controller[0]);
				if(strpos($controller, 'Controller') === false) {					
					$controller = $controller . 'Controller';			
				}
				$this->controllerName = $controller;
				$action = $x[1];
				if(strpos($action, 'action') === false) {
					$action[0] = strtoupper($action[0]);
					$action = 'action' . $action;			
				}
				$this->actionName = $action;
				unset($routeArr[$i]);
			}
		}

		$this->namespace = implode('\\', $routeArr);
		if(!empty($this->namespace)) $this->namespace .= '\\';

		// \CF::Debug($routeArr);
		// \CF::Debug($request_url);
		// \CF::Debug($match);
		// \CF::Debug($this->controllerName);
		// \CF::Debug($this->actionName);
		// \CF::Debug($this->namespace);
		// \CF::Debug($this->params);


	}



}
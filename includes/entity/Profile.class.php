<?php

namespace entity;

class Profile{

	/**
	 * int
	 */
	private $id;

	/**
	 * int
	 */
	private $specId;

	/**
	 * string
	 */
	private $name;


	public function __construct(array $profile = array()){
		$this->id = (isset($profile['id']) && $profile['id']) ? (int)$profile['id'] : null;
		$this->specId = (isset($profile['spec_id']) && $profile['spec_id']) ? (int)$profile['spec_id'] : null;
		$this->name = (isset($profile['name']) && $profile['name']) ? $profile['name'] : '';
	}

	public function __clone(){
		$this->id = 0;
	}

	public function __toString(){
		return (string)$this->name;
	}

	/**
	 * @return mixed
	 */
	public function getId(){
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId($id){
		$this->id = (int)$id;
	}

	/**
	 * @return mixed
	 */
	public function getSpecId(){
		return $this->specId;
	}

	/**
	 * @param mixed $spec_id
	 */
	public function setSpecId($specId){
		$this->specId = (int)$specId;
	}

	/**
	 * @return mixed
	 */
	public function getName(){
		return $this->name;
	}

	/**
	 * @param mixed $name
	 */
	public function setName($name){
		$this->name = $name;
	}

}
<?php

namespace entity;


/**
 * ����������� ����������
 */
class Speciality{

	/**
	 * int
	 */
	private $id;

	/**
	 * int
	 */
	private $facultyId;

	/**
	 * ��������
	 * string
	 */
	private $name;

	/**
	 * �����������
	 * string
	 */
	private $direction;

	/**
	 * ��� �������������
	 * string
	 */
	private $code;

	/**
	 * ��� �������� �������������
	 * date
	 */
	private $yearOpen;

	/**
	 * �������� �������������
	 * text
	 */
	private $description;

	/**
	 * ���� �������� �� ���
	 * text
	 */
	private $dataTraining;

	/**
	 * �� ������ ����� (1-���, 0-�� )
	 * smallint
	 */
	private $old;

	/**
	 * ������ �� ������������� �� ������ �����
	 * smallint
	 */
	private $oldId;

	/**
	 * smallint
	 */
	private $hideAccredit;

	/**
	 * int
	 */
	private $code1c;


	public function __construct(array $speciality = array()){
		$this->id = (isset($speciality['id']) && $speciality['id']) ? (int)$speciality['id'] : null;
		$this->facultyId = (int)$speciality['id_faculty'];
		$this->name = (string)$speciality['name'];
		$this->code = (string)$speciality['code'];
		$this->direction = (string)$speciality['direction'];
		$this->yearOpen = $speciality['year_open'];
		$this->description = (string)$speciality['description'];
		$this->dataTraining = (isset($speciality['data_training']) && $speciality['data_training']) ? $speciality['data_training'] : '';
		$this->old = (isset($speciality['old']) && $speciality['old']) ? $speciality['old'] : 1;
		$this->oldId = (isset($speciality['old_id']) && $speciality['old_id']) ? $speciality['old_id'] : 0;
		$this->hideAccredit = (isset($speciality['hide_accredit']) && $speciality['hide_accredit']) ? $speciality['hide_accredit'] : 0;
		$this->code1c = (isset($speciality['code1c']) && $speciality['code1c']) ? $speciality['code1c'] : null;
	}

	public function __clone(){
		$this->id = 0;
	}

	public function __toString(){
		return (string)$this->name;
	}

	/**
	 *  mixed
	 */
	public function getId(){
		return $this->id;
	}

	/**
	 *  mixed $id
	 */
	public function setId($id){
		$this->id = $id;
	}

	/**
	 *  mixed
	 */
	public function getFacultyId(){
		return $this->facultyId;
	}

	/**
	 *  mixed $facultyId
	 */
	public function setFacultyId($facultyId){
		$this->facultyId = $facultyId;
	}

	/**
	 *  mixed
	 */
	public function getName(){
		return $this->name;
	}

	/**
	 *  mixed $name
	 */
	public function setName($name){
		$this->name = $name;
	}

	/**
	 *  mixed
	 */
	public function getDirection(){
		return $this->direction;
	}

	/**
	 *  mixed $direction
	 */
	public function setDirection($direction){
		$this->direction = $direction;
	}

	/**
	 *  mixed
	 */
	public function getCode(){
		return $this->code;
	}

	/**
	 *  mixed $code
	 */
	public function setCode($code){
		$this->code = $code;
	}

	/**
	 *  mixed
	 */
	public function getYearOpen(){
		return $this->yearOpen;
	}

	/**
	 *  mixed $yearOpen
	 */
	public function setYearOpen($yearOpen){
		$this->yearOpen = $yearOpen;
	}

	/**
	 *  mixed
	 */
	public function getDescription(){
		return $this->description;
	}

	/**
	 *  mixed $description
	 */
	public function setDescription($description){
		$this->description = $description;
	}

	/**
	 *  mixed
	 */
	public function getDataTraining(){
		return $this->dataTraining;
	}

	/**
	 *  mixed $dataTraining
	 */
	public function setDataTraining($dataTraining){
		$this->dataTraining = $dataTraining;
	}

	/**
	 *  mixed
	 */
	public function getOld(){
		return $this->old;
	}

	/**
	 *  mixed $old
	 */
	public function setOld($old){
		$this->old = $old;
	}

	/**
	 *  mixed
	 */
	public function getOldId(){
		return $this->oldId;
	}

	/**
	 *  mixed $oldId
	 */
	public function setOldId($oldId){
		$this->oldId = $oldId;
	}

	/**
	 *  mixed
	 */
	public function getHideAccredit(){
		return $this->hideAccredit;
	}

	/**
	 *  mixed $hideAccredit
	 */
	public function setHideAccredit($hideAccredit){
		$this->hideAccredit = $hideAccredit;
	}

	/**
	 *  mixed
	 */
	public function getCode1c(){
		return $this->code1c;
	}

	/**
	 *  mixed $code1c
	 */
	public function setCode1c($code1c){
		$this->code1c = $code1c;
	}
}
<?php

namespace entity\cartrige;

use \engine\core1C\Helper1C as H;


class Cartrige
{
	/**
	 * @var int
	 */
	private $active;

	/**
	 * @var string
	 */
	private $subdivision;

	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var string
	 */
	private $work;

	/**
	 * @var int
	 */
	private $returned;

	/**
	 * @var int
	 */
	private $issued;

	/**
	 * @var int
	 */
	private $number;


	public function __construct($std){
		$this->active = (int)$std->active;
        $this->subdivision = trim(H::u2w($std->subdivision));
		$this->name = trim(H::u2w($std->name));
        $this->work = trim(H::u2w($std->work));
        $this->returned = (int)$std->returned;
        $this->issued = (int)$std->issued;
        $this->number = (int)$std->number;
	}

	public function __toString(){
		return (string)$this->name;
	}

	/**
	 * @return int
	 */
	public function getActive(){
		return $this->active;
	}

	/**
	 * @param int $active
	 */
	public function setActive($active){
		$this->active = $active;
	}

	/**
	 * @return string
	 */
	public function getSubdivision(){
		return (string)$this->subdivision;
	}

	/**
	 * @param string $subdivision
	 */
	public function setSubdivision($subdivision){
		$this->subdivision = $subdivision;
	}

	/**
	 * @return string
	 */
	public function getName(){
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name){
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getWork(){
		return $this->work;
	}

	/**
	 * @param string $work
	 */
	public function setWork($work){
		$this->work = $work;
	}

	/**
	 * @return int
	 */
	public function getReturned(){
		return $this->returned;
	}

	/**
	 * @param int $returned
	 */
	public function setReturned($returned){
		$this->returned = $returned;
	}

	/**
	 * @return int
	 */
	public function getNumber(){
		return $this->number;
	}

	/**
	 * @param int $number
	 */
	public function setNumber($number){
		$this->number = $number;
	}

	/**
	 * @return int
	 */
	public function getIssued(){
		return $this->issued;
	}

	/**
	 * @param int $issued
	 */
	public function setIssued($issued){
		$this->issued = $issued;
	}
}
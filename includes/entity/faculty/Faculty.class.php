<?php

namespace entity\faculty;

class Faculty
{
	/**
	 * int
	 */
	private $id;

	/**
	 * int
	 */
	private $code1c;

	/**
	 * int
	 */
	private $pid;

	/**
	 * int
	 */
	private $pos;

	/**
	 * int
	 */
	private $is_active;

	/**
	 * string
	 */
	private $name;

	/**
	 * string
	 */
	private $shortName;

	/**
	 * int
	 */
	private $groupNum;

	/**
	 * int
	 */
	private $groups;

	/**
	 * string
	 */
	private $link;

	/**
	 * int
	 */
	private $foundationYear;

	/**
	 * int
	 */
	private $closingYear;

	/**
	 * string
	 */
	private $comment;

	/**
	 * array
	 */
	private $arrGroups = array();

	/**
	 * array
	 */
	private $arrSpecialities = array();

	

	public function __construct(array $faculty = array()){
		$this->id = (isset($faculty['id']) && $faculty['id']) ? $faculty['id'] : null;
		$this->pid = (isset($faculty['pid']) && $faculty['pid']) ? (int)$faculty['pid'] : 0;
		$this->pos = (isset($faculty['pos']) && $faculty['pos']) ? (int)$faculty['pos'] : 0;
		$this->is_active = (isset($faculty['is_active']) && $faculty['is_active']) ? '1' : '0';
		$this->name = (isset($faculty['name']) && $faculty['name']) ? $faculty['name'] : '';
		$this->shortName = (isset($faculty['short_name']) && $faculty['short_name']) ? $faculty['short_name'] : '';
		$this->groupNum = (isset($faculty['group_num']) && $faculty['group_num']) ? (int)$faculty['group_num'] : null;
		$this->groups = (isset($faculty['groups']) && $faculty['groups']) ? $faculty['groups'] : '';
		$this->link = (isset($faculty['link']) && $faculty['link']) ? $faculty['link'] : '';
		$this->foundationYear = (isset($faculty['foundation_year']) && $faculty['foundation_year']) ? $faculty['foundation_year'] : date("Y");
		$this->closingYear = (isset($faculty['closing_year']) && $faculty['closing_year']) ? $faculty['closing_year'] : null;
		$this->comment = (isset($faculty['comment']) && $faculty['comment']) ? $faculty['comment'] : '';
		$this->code1c = (isset($faculty['code1c']) && $faculty['code1c']) ? (int)$faculty['code1c'] : null;
	}

	public function __clone(){
		$this->id = 0;
	}

	public function __toString(){
		return (string)$this->name;
	}

	/**
	 * return mixed
	 */
	public function getId(){
		return $this->id;
	}

	/**
	 * int $id
	 */
	public function setId($id){
		$this->id = (int)$id;
	}


	/**
	 * return mixed
	 */
	public function getCode1c(){
		return $this->code1c;
	}

	/**
	 * param mixed $code1c
	 */
	public function setCode1c($code1c){
		$this->code1c = !empty($code1c) ? (int)$code1c : null;
	}

	/**
	 * return mixed
	 */
	public function getPid(){
		return $this->pid;
	}

	/**
	 * param mixed $pid
	 */
	public function setPid($pid){
		$this->pid = !empty($pid) ? (int)$pid : 0;
	}

	/**
	 * return mixed
	 */
	public function getPos(){
		return $this->pos;
	}

	/**
	 * param mixed $pos
	 */
	public function setPos($pos){
		$this->pos = !empty($pos) ? (int)$pos : 0;
	}

	/**
	 * return mixed
	 */
	public function getIsActive(){
		return $this->is_active;
	}

	/**
	 * param mixed $is_active
	 */
	public function setIsActive($is_active){
		$this->is_active = !empty($is_active) ? '1' : '0';
	}

	/**
	 * return mixed
	 */
	public function getName(){
		return $this->name;
	}

	/**
	 * param mixed $name
	 */
	public function setName($name){
		$this->name = $name;
	}

	/**
	 * return mixed
	 */
	public function getShortName(){
		return $this->shortName;
	}

	/**
	 * param mixed $shortName
	 */
	public function setShortName($shortName){
		$this->shortName = !empty($shortName) ? $shortName : '';
	}

	/**
	 * return mixed
	 */
	public function getGroupNum(){
		return $this->groupNum;
	}

	/**
	 * param mixed $groupNum
	 */
	public function setGroupNum($groupNum){
		$this->groupNum = !empty($groupNum) ? (int)$groupNum : null;
	}

	/**
	 *  mixed
	 */
	public function getGroups(){
		return $this->groups;
	}

	/**
	 *  mixed $groups
	 */
	public function setGroups($groups){
		$this->groups = !empty($groups) ? $groups : '';
	}

	/**
	 * return mixed
	 */
	public function getLink(){
		return $this->link;
	}

	/**
	 * param mixed $link
	 */
	public function setLink($link){
		$this->link = !empty($link) ? $link : '';
	}

	/**
	 * return mixed
	 */
	public function getFoundationYear(){
		return $this->foundationYear;
	}

	/**
	 * param mixed $foundationYear
	 */
	public function setFoundationYear($foundationYear){
		$this->foundationYear = !empty($foundationYear) ? $foundationYear : date("Y");
	}

	/**
	 * return mixed
	 */
	public function getClosingYear(){
		return $this->closingYear;
	}

	/**
	 * param mixed $closingYear
	 */
	public function setClosingYear($closingYear){
		$this->closingYear = !empty($closingYear) ? $closingYear : null;
	}

	/**
	 * return mixed
	 */
	public function getComment(){
		return $this->comment;
	}

	/**
	 * param mixed $comment
	 */
	public function setComment($comment){
		$this->comment = !empty($comment) ? $comment : '';
	}

	/**
	 *  mixed
	 */
	public function getArrGroups(){
		return $this->arrGroups;
	}

	/**
	 *  mixed $arrGroups
	 */
	public function setArrGroups(array $arrGroups){
		$this->arrGroups = !empty($arrGroups) ? $arrGroups: $this->arrGroups;
	}

	/**
	 * mixed
	 */
	public function getArrSpecialities(){
		return $this->arrSpecialities;
	}

	/**
	 * mixed $arrSpecialities
	 */
	public function setArrSpecialities($arrSpecialities){
		$this->arrSpecialities = !empty($arrSpecialities) && is_array($arrSpecialities)? $arrSpecialities: $this->arrSpecialities;
	}
}
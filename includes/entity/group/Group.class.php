<?php

namespace entity\group;

class Group
{
	/**
	 * int
	 */
	private $id;

	/**
	 * int
	 */
	private $code1c;

	/**
	 * string
	 */
	private $name;

	/**
	 * int
	 */
	private $idFaculty;

	/**
	 * string
	 */
	private $specialitiesCode;

	/**
	 * string
	 */
	private $formEducation;

	/**
	 * int
	 */
	private $idSpec;

	/**
	 * int
	 */
	private $year;

	/**
	 * int
	 */
	private $hidden;

	/**
	 * int
	 */
	private $profile;

	/**
	 * enum
	 */
	private $qualification;


	public function __construct(array $group){
		$this->id = (isset($group['id']) && $group['id']) ? $group['id'] : null;
		$this->code1c = (isset($group['code1c']) && $group['code1c']) ? $group['code1c'] : null;

		$this->name = $group['name'];

		$this->idFaculty = (isset($group['id_faculty']) && $group['id_faculty']) ? $group['id_faculty'] : 0;
		$this->specialitiesCode = (isset($group['specialities_code']) && $group['specialities_code']) ? $group['specialities_code'] : null;
		$this->formEducation = (isset($group['form_education']) && $group['form_education']) ? $group['form_education'] : null;
		$this->idSpec = (isset($group['id_spec']) && $group['id_spec']) ? $group['id_spec'] : null;
		$this->year = (isset($group['year']) && $group['year']) ? $group['year'] : 1;
		$this->hidden = (isset($group['hidden']) && $group['hidden']) ? $group['hidden'] : 0;
		$this->profile = (isset($group['profile']) && $group['profile']) ? $group['profile'] : null;
		$this->qualification = (isset($group['qualification']) && $group['qualification']) ? $group['qualification'] : '';
	}

	public function __clone(){
		$this->id = 0;
	}

	public function __toString(){
		return (string)$this->name;
	}

	/**
	 *  mixed
	 */
	public function getId(){
		return $this->id;
	}

	/**
	 * int $id
	 */
	public function setId($id){
		$this->id = (int)$id;
	}

	/**
	 *  mixed
	 */
	public function getCode1c(){
		return $this->code1c;
	}

	/**
	 * mixed $code1c
	 */
	public function setCode1c($code1c){
		$this->code1c = $code1c;
	}

	/**
	 *  mixed
	 */
	public function getName(){
		return $this->name;
	}

	/**
	 * mixed $name
	 */
	public function setName($name){
		$this->name = $name;
	}

	/**
	 *  mixed
	 */
	public function getIdFaculty(){
		return $this->idFaculty;
	}

	/**
	 * mixed $idFaculty
	 */
	public function setIdFaculty($idFaculty){
		$this->idFaculty = (int)$idFaculty;
	}

	/**
	 *  mixed
	 */
	public function getSpecialitiesCode(){
		return $this->specialitiesCode;
	}

	/**
	 * mixed $specialitiesCode
	 */
	public function setSpecialitiesCode($specialitiesCode){
		$this->specialitiesCode = $specialitiesCode;
	}

	/**
	 *  mixed
	 */
	public function getFormEducation(){
		return $this->formEducation;
	}

	/**
	 * mixed $formEducation
	 */
	public function setFormEducation($formEducation){
		$this->formEducation = $formEducation;
	}

	/**
	 *  mixed
	 */
	public function getIdSpeciality(){
		return $this->idSpec;
	}

	/**
	 * mixed $idSpec
	 */
	public function setIdSpeciality($idSpec){
		$this->idSpec = $idSpec;
	}

	/**
	 *  mixed
	 */
	public function getYear(){
		return $this->year;
	}

	/**
	 * mixed $year
	 */
	public function setYear($year){
		$this->year = $year;
	}

	/**
	 *  mixed
	 */
	public function getHidden(){
		return $this->hidden;
	}

	/**
	 * mixed $hidden
	 */
	public function setHidden($hidden){
		$this->hidden = $hidden;
	}

	/**
	 *  mixed
	 */
	public function getProfile(){
		return $this->profile;
	}

	/**
	 * mixed $profile
	 */
	public function setProfile($profile){
		$this->profile = $profile;
	}

	/**
	 *  mixed
	 */
	public function getQualification(){
		return $this->qualification;
	}

	/**
	 * mixed $qualification
	 */
	public function setQualification($qualification){
		$this->qualification = $qualification;
	}
}
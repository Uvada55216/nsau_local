<? 
namespace files;

class AttachmentDto { 

	public $file_id;
	public $subject_id;
	public $resourse_id;
	public $qualification_id;
	public $semesters;
	public $profiles_id;
	public $speciality_id;
	public $edu_forms;


	public function __construct($file_id, $subject_id, $resourse_id, $qualification_id, $semesters, $profiles_id, $speciality_id, $edu_forms, $approved = null) {
		$this->file_id = $file_id;
		$this->subject_id = $subject_id;
		$this->resourse_id = $resourse_id;
		$this->qualification_id = $qualification_id;
		$this->semesters = $semesters;
		$this->profiles_id = $profiles_id;
		$this->speciality_id = $speciality_id;
		$this->edu_forms = $edu_forms;
		$this->approved = $approved;
	}

} 


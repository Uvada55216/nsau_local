<?
namespace files;

class File {

	private $_file;

	public function __construct($file_id) {
		global $DB;
		$DB->SetTable('nsau_files');
		$DB->AddCondFS('id', '=', $file_id);
		$this->_file = $DB->FetchAssoc($DB->Select(1));

		$this->create_time = \CF::reverseDateTime($this->create_time);
		$this->update_time = \CF::reverseDateTime($this->update_time);
		$this->delete_time = \CF::reverseDateTime($this->delete_time);
		$this->_file['link'] = 'https://'.$_SERVER['SERVER_NAME'].'/file/'.$this->id;
		$this->_file['has_decine_attachment'] = false;

		$Attachments = new SubjectAttachments($file_id);
		$Attachments = $Attachments->getAttachments(); 
		foreach ($Attachments as $key => $value) {
			if($value->approved == -1) {
				$this->_file['has_decine_attachment'] = true;
			}
		}

		if(empty($this->_file)) {
			throw new \Exception("File $file_id is not exist.");
		}
	}

	public static function create($arr) {
		global $DB, $Auth, $Engine;
		$parts = explode('.', $arr['file']['name']);
		$ext = array_pop($parts);
		$name = implode('.', $parts);

		
		$DB->AddValue('user_id', ($arr['send_to_people'] && self::guardSendFile() && self::guardUserExist($arr['send_to_people'])) ? $arr['send_to_people'] : $Auth->user_id);
		$DB->SetTable('nsau_files');
		$DB->AddValue('name', iconv('utf-8', 'cp1251', $name));
		$DB->AddValue('descr', iconv('utf-8', 'cp1251', $arr['description']));
		$DB->AddValue('author', iconv('utf-8', 'cp1251', $arr['author']));
		$DB->AddValue('year', $arr['year']);
		$DB->AddValue('volume', $arr['pages']);
		$DB->AddValue('edition', $arr['edition']);
		$DB->AddValue('place', iconv('utf-8', 'cp1251', $arr['place']));
		$DB->AddValue('filename', $ext);
		$DB->AddValue('folder_id', $arr['folder_id'] ? $arr['folder_id'] : NULL);
		$DB->AddValue('with_sig', (int)$arr['with_sig']);
		if(!empty($arr['zip_file'])) {
			$DB->AddValue('is_html', $arr['zip_file']);
		}
		$DB->AddValue("create_time", "NOW()", "X");    
		$DB->Insert();
		$id = $DB->LastInsertID();
		if(!empty($id)) {			
			move_uploaded_file($arr['file']['tmp_name'], FILES_DIR . $id . '.' . $ext);//FILES_DIR
			if(!empty($arr['sub_authors'])) {
				self::addSubAuthors($arr['sub_authors'], $id);
			}
			if(!empty($arr['allow_download'])) {
				self::allowDownload($id);
			}
			if(!empty($arr['zip_file'])) {
				self::extractZip($id);
			}
		}
		return $id;
	}

	public static function createSigFileAndUpdate($file, $sig){
		global $DB;

		if(move_uploaded_file($sig['tmp_name'], FILES_DIR .SIG_DIR. $file->id . '.' . $file->filename. '.sig')){
			$DB->SetTable('nsau_files');
			$DB->AddCondFS('id', '=', $file->id);
			$DB->AddValue("sig_id", $file->id);
			$DB->Update();
		}
	}

	public static function update($arr) {
		global $DB, $Auth, $Engine;
		self::guardFileOwner($arr['file_id']);
		//переписать!
		$DB->SetTable('nsau_files');
		$DB->AddCondFS('id', '=', $arr['file_id']);
		$DB->AddField('filename');
		$DB->AddField('name');
		$old_file = $DB->FetchAssoc($DB->Select(1));



		
		$DB->AddValue('user_id', ($arr['send_to_people'] && self::guardSendFile() && self::guardUserExist($arr['send_to_people'])) ? $arr['send_to_people'] : $Auth->user_id);
		$DB->SetTable('nsau_files');
		$DB->AddCondFS('id', '=', $arr['file_id']);
		if(!empty($arr['file'])) {
			$parts = explode('.', $arr['file']['name']);
			$ext = array_pop($parts);
			$name = implode('.', $parts);
			if(!empty($arr['zip_file']) && empty($arr['file']['size'])) {
				$name = $old_file['name'];
			}
			$DB->AddValue('name', iconv('utf-8', 'cp1251', $name));
			$DB->AddValue('filename', $ext);
		}
		if(!empty($arr['zip_file'])) {
			$DB->AddValue('is_html', $arr['zip_file']);
		} else {
			$DB->AddValue('is_html', 0);
		}
		$DB->AddValue('descr', iconv('utf-8', 'cp1251', $arr['description']));
		$DB->AddValue('author', iconv('utf-8', 'cp1251', $arr['author']));
		$DB->AddValue('year', $arr['year']);
		$DB->AddValue('volume', $arr['pages']);
		$DB->AddValue('edition', $arr['edition']);
		$DB->AddValue('place', iconv('utf-8', 'cp1251', $arr['place']));
		if(!empty($arr['send_to_people'])) {
			$DB->AddValue('folder_id', NULL);
		} else {
			$DB->AddValue('folder_id', $arr['folder_id'] ? $arr['folder_id'] : NULL);
		}
		$DB->AddValue('with_sig', (int)$arr['with_sig']);
		$DB->AddValue("update_time", "NOW()", "X");
		$DB->Update();
		$id = $arr['file_id'];
		if(!empty($arr['file']) && !empty($arr['file']['size'])) {
			unlink(FILES_DIR . $arr['file_id'] . '.' . $old_file['filename']);			
			move_uploaded_file($arr['file']['tmp_name'], FILES_DIR . $arr['file_id'] . '.' . $ext);			
			$SubjectAttachments = new SubjectAttachments($arr['file_id']);
			$SubjectAttachments->unapprove();
		}

		
		self::addSubAuthors($arr['sub_authors'], $id);
		

		if(!empty($arr['allow_download'])) {
			self::allowDownload($id);
		} else {
			self::denyDownload($id);
		}

		if(!empty($arr['zip_file'])) {
			self::extractZip($id);
		} else {
			\CF::removeDir($_SERVER["DOCUMENT_ROOT"]."/htmldocs/".$id."/");
		}
		
	}

	public static function delete($file_id) {
		global $DB, $Engine;
		self::guardFileOwner($file_id);
		$DB->SetTable('nsau_files');
		$DB->AddCondFS('id', '=', $file_id);
		$DB->AddValue('deleted', 1);
		$DB->AddValue("delete_time", "NOW()", "X"); 
		$DB->Update();
		$SubjectAttachments = new SubjectAttachments($file_id);
		$SubjectAttachments->delete();
	}

	public static function fullDelete($file_id) {
		global $DB, $Engine;
		self::guardFileOwner($file_id);
		$DB->SetTable('nsau_files');
		$DB->AddCondFS('id', '=', $file_id);
		$row = $DB->FetchAssoc($DB->Select(1));

		$DB->SetTable('nsau_files');
		$DB->AddCondFS('id', '=', $file_id);
		$DB->Delete();

		unlink(FILES_DIR . $row['id'] . '.' . $row['filename']);
	}

	public static function repair($file_id) {
		global $DB, $Engine;
		self::guardFileOwner($file_id);
		$DB->SetTable('nsau_files');
		$DB->AddCondFS('id', '=', $file_id);
		$DB->AddValue('deleted', 0);
		$DB->AddValue("delete_time", NULL); 
		$DB->Update();
	}

	private static function addSubAuthors($sub_authors, $file_id) {
		global $DB, $Auth;
		$DB->SetTable("nsau_autors");
		$DB->AddCondFS("id_file", "=", $file_id);
		$DB->Delete();
		foreach ($sub_authors as $author_id) {	
			$DB->SetTable("nsau_autors");
			$DB->AddValue("id_user", $Auth->user_id);
			$DB->AddValue("id_people", $author_id);
			$DB->AddValue("id_file", $file_id);	
			$DB->Insert();
		}
	}

	private static function allowDownload($id) {
		global $Engine;
		$Engine->AddPrivilege(7, "files.download", $id, 0, 1, "create;право на скачивание всем группам");
	}

	private static function denyDownload($id) {
		global $Engine;
		$Engine->DeletePrivilege(7, "files.download", $id, 0, 1);
	}


	private static function extractZip($id) {
		\CF::removeDir($_SERVER["DOCUMENT_ROOT"]."/htmldocs/".$id."/");
		$zip = new \ZipArchive();
		$zip->open(FILES_DIR . $id . '.zip');
		$zip->extractTo($_SERVER["DOCUMENT_ROOT"]."/htmldocs/".$id."/");
		echo $zip->status;die;
	}

	private static function guardSendFile() {
		global $Auth, $Engine;
		// $module_id = 7;
		// $array = $Engine->GetPrivileges($module_id, 'files.transfer', null, $Auth->usergroup_id);
		// $dep_arr = null;
		// foreach($array[$module_id]['files.transfer'] as $entry => $allowed) {
		// 	if($Engine->OperationAllowed($module_id, 'files.transfer', $entry, $Auth->usergroup_id)) {
		// 		return true;
		// 	}
		// }
		return true;
		throw new \Exception("File transfer not allowed");
	}

	private static function guardUserExist($user_id) {
		global $DB;
		$DB->SetTable('auth_users');
		$DB->AddCondFS('id', '=', $user_id);
		$row = $DB->FetchAssoc($DB->Select(1));
		if(!empty($row)) {
			return true;
		}
		return false;
	}

	public function guardFileOwner($file_id = null) {
		global $DB, $Auth;
		$DB->SetTable('nsau_files');
		$DB->AddCondFS('id', '=', $file_id ? $file_id : $this->id);
		$DB->AddCondFS('user_id', '=', $Auth->user_id);
		$row = $DB->FetchAssoc($DB->Select(1));
		if(empty($row)) {
			throw new \Exception("File edit operation not allowed");
		}
	}

	public function __get($name) {
		if(array_key_exists($name, $this->_file)) {
			return $this->_file[$name];
		}
		throw new \Exception("Member $name is not exist.");
	}

	public function __set($name, $val) {
		if(array_key_exists($name, $this->_file)) {
			$this->_file[$name] = $val;
		} else {
			throw new \Exception("Member $name is not exist.");
		}
	}
}

<?
namespace files;

class Folder {

	public static function rename($data){
		global $DB;
		self::guardFolderOwner($data['folder_id']);

		$DB->SetTable('nsau_files_folders');
		$DB->AddCondFS('id', '=', $data['folder_id']);
		$DB->AddValue('name', iconv('utf-8', 'cp1251', $data['folder_name']));
		$DB->Update();
	}

	public static function create($data){
		global $DB, $Auth;
		$DB->SetTable('nsau_files_folders');
		$DB->AddValue('name', iconv('utf-8', 'cp1251', $data['folder_name']));
		$DB->AddValue('user_id', $Auth->user_id);
		$DB->Insert();
		return $DB->LastInsertID();
	}

	public static function delete($id){
		global $DB;
		self::guardFolderOwner($id);

		$DB->SetTable('nsau_files');
		$DB->AddCondFS('folder_id', '=', $id);
		$DB->AddValue('folder_id', NULL);
		$DB->Update();

		$DB->SetTable('nsau_files_folders');
		$DB->AddCondFS('id', '=', $id);
		$DB->Delete();
	}

	private static function guardFolderOwner($id) {
		global $Auth, $DB;
		$DB->SetTable('nsau_files_folders');
		$DB->AddCondFS('id', '=', $id);
		$DB->AddCondFS('user_id', '=', $Auth->user_id);
		$row = $DB->FetchAssoc($DB->Select(1));
		if(empty($row)) {
			throw new \Exception("File operations not allowed");
		}
	}

}
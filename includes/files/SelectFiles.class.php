<?
namespace files;

class SelectFiles {

	private static $_db;

	public static function init(\MySQLhandle $db) {
		self::$_db = $db;
		self::$_db->SetTable('nsau_files', 'f');
		return new self();
	}

	public function fromSubject($subject_id) {
		self::$_db->AddTable('nsau_files_subj', 'fs');
		self::$_db->AddCondFS('fs.subject_id', '=', $subject_id);
		self::$_db->AddCondFF('f.id', '=', 'fs.file_id');
		return $this;
	}


	public function fromFolder($folder_id) {
		self::$_db->AddCondFS('f.folder_id', '=', $folder_id);
		return $this;
	}

	public function owner($user_id) {
		self::$_db->AddCondFS('f.user_id', '=', $user_id);
		return $this;
	}

	public function isStudentWork() {
		self::$_db->AddCondFS('f.sw_id', '!=', 0);
		return $this;
	}


	public function deleted($bool) {
		if($bool){
			self::$_db->AddCondFS('f.deleted', '=', 1);
		} else {
			self::$_db->AddCondFS('f.deleted', '=', 0);
		}
		return $this;
	}


	public function loadUserSubjects() {	
		self::$_db->AddTable('nsau_subjects', 'ns');
		self::$_db->AddTable('nsau_files_subj', 'fs');
		self::$_db->AddTable('nsau_departments', 'nd');
		self::$_db->AddCondFF('f.id', '=', 'fs.file_id');
		self::$_db->AddCondFF('fs.subject_id', '=', 'ns.id');
		self::$_db->AddCondFF('nd.id', '=', 'ns.department_id');
		self::$_db->AddField('fs.subject_id', 'id');
		self::$_db->AddField('ns.name', 'name');
		self::$_db->AddField('nd.name', 'department_name');
		return $this;
	}

	public function loadUserFolders($user_id) {
		self::$_db->SetTable('nsau_files_folders', 'f');
		self::$_db->AddOrder('f.name');
		return $this;	
	}

	public function withoutAnyAttachments() {
		self::$_db->AddCondFO('f.folder_id', ' is null ');	
		self::$_db->AddCondFO('f.sw_id', ' is null ');	
		self::$_db->AddCondXF('f.id not in(select file_id from nsau_files_subj where file_id = f.id)', "" );
		return $this;	
	}

	public function selectArray() {
		// echo self::$_db->SelectQuery();
		$res = self::$_db->Select(NULL, NULL, true, true, true);
		while($row = self::$_db->FetchAssoc($res)) {
			$result[$row['id']] = $row;
		}
		return $result;	
	}

	public function select() {
		self::$_db->AddField('f.id', 'id');
		// echo self::$_db->SelectQuery().'===';
		$res = self::$_db->Select(NULL, NULL, true, true, true);
		while($row = self::$_db->FetchAssoc($res)) {
			$result[$row['id']] = new File($row['id']);
		}
		return $result;
	}

}
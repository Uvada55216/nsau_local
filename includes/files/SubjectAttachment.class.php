<?
namespace files;

class SubjectAttachment {

	private $_attachment;

	public function __construct($attach_id) {
		global $DB;
		$this->_attachment = $DB->SetTable('nsau_files_subj')
			->AddCondFS('id', '=', $attach_id)
			->SelectArray(1);

		if(empty($this->_attachment)) {
			throw new \Exception("Attachment $attach_id is not exist.");
		}

		$this->_attachment['subject_name'] = $this->getSubjectName();
		$this->_attachment['speciality'] = $this->getSpecialityName();
		$this->_attachment['attach_type'] = $this->getAttachType();
		$this->_attachment['status'] = $this->getStatus();
		$this->_attachment['status_style'] = $this->getStatusStyle();
		$this->_attachment['profile'] = $this->getProfileName();
	}

	public function accept($comment) {
		global $DB, $Engine;
		self::guardIsModerator();
		$DB->SetTable('nsau_files_subj')
			->AddCondFS('id', '=', $this->_attachment['id'])
			->AddValue('approved', '1')
			->AddValue('comment', $comment)
			->AddValue('acepted_time', "NOW()", "X")
			->Update();
		$Engine->LogAction(7, "file_approved", $this->_attachment['id'], "approve");
	}

	public function decine($comment) {
		global $DB, $Engine;
		self::guardIsModerator();
		$DB->SetTable('nsau_files_subj')
			->AddCondFS('id', '=', $this->_attachment['id'])
			->AddValue('approved', '-1')
			->AddValue('comment', $comment)
			->AddValue("rejected_time", "NOW()", "X")
			->Update();
		$Engine->LogAction(7, "file_approved", $this->_attachment['id'], "decline");
	}

 	public static function create(AttachmentDto $Att) {
 		global $DB;

		self::guardFileOwner($Att->file_id);
 		if(empty($Att->profiles_id)) $Att->profiles_id = array(false);
 		unset($Att->semesters[0]);
 		if(empty($Att->semesters[1]) &&  empty($Att->semesters[2]) && empty($Att->semesters[3])){
 			$Att->semesters[1] = array(false);
 			$Att->semesters[2] = array(false);
 			$Att->semesters[3] = array(false);
 		} 
 		$DB->SetTable('nsau_spec_type');
 		$DB->AddCondFS('spec_id', '=', $Att->speciality_id);
 		$DB->AddFields(array('code', 'id'));
 		$spec_type = $DB->FetchAssoc($DB->Select(1));

        foreach($Att->edu_forms as $form) {
            foreach($Att->profiles_id as $profile) {
                foreach ($Att->semesters[$form] as $semester) {
                    $DB->SetTable('nsau_files_subj');

                    $DB->AddValue('file_id', $Att->file_id);
                    $DB->AddValue('subject_id', $Att->subject_id);
                    $DB->AddValue('spec_id', $Att->speciality_id);
                    $DB->AddValue('view_id', $Att->resourse_id);
                    $DB->AddValue('qual_id', $Att->qualification_id);

                    $DB->AddValue('education', $form);
                    if($profile) {
                    	$DB->AddValue('profile_id', $profile);
                    }
                    if($semester) {
                    	$DB->AddValue('semester', $semester);
                	}

                    $DB->AddValue('spec_code', $spec_type['code']);
                    $DB->AddValue('spec_type_id', $spec_type['id']);

                    $DB->AddValue("added_time", "NOW()", "X");                   

                    if($Att->approved) {
                    	$DB->AddValue('approved', 1);
                    }

					$DB->Insert();
					$ids[] = $DB->LastInsertID();
                }
            }
        }
        return $ids;
 	}

	public function delete() {
		global $DB;
		self::guardFileOwner($this->file_id);
		$DB->SetTable('nsau_files_subj');
		$DB->AddCondFS('id', '=', $this->id);
		$DB->Delete();
	}

	public function countSubjectAttachments(){
		global $DB, $Auth;
		$DB->SetTable('nsau_files_subj', 'fs');
		$DB->AddTable('nsau_files', 'f');
		$DB->AddCondFF('f.id', '=', 'fs.file_id');
		$DB->AddCondFS('f.user_id', '=', $Auth->user_id);
		$DB->AddCondFS('fs.subject_id', '=', $this->subject_id);
		$DB->AddExp("COUNT(fs.id) as count");
		$row = $DB->FetchAssoc($DB->Select(1));
		$result['count'] =  $row['count'];
		$result['subject_id'] =  $this->subject_id;
		return $result;
	}

	public function countSubjectFileAttachments(){
		global $DB, $Auth;
		$DB->SetTable('nsau_files_subj', 'fs');
		$DB->AddCondFS('fs.subject_id', '=', $this->subject_id);
		$DB->AddCondFS('fs.file_id', '=', $this->file_id);
		$DB->AddExp("COUNT(fs.id) as count");
		$row = $DB->FetchAssoc($DB->Select(1));
		$result['count'] =  $row['count'];
		$result['file_id'] =  $this->file_id;
		return $result;
	}

	public function unapprove() {
		global $DB;
		self::guardFileOwner($this->file_id);
		$DB->SetTable('nsau_files_subj');
		$DB->AddCondFS('id', '=', $this->id);
		$DB->AddValue('approved', NULL);
		$DB->Update();
	}

	public function __get($name) {
		if(array_key_exists($name, $this->_attachment)) {
			return $this->_attachment[$name];
		}
		throw new \Exception("Member $name is not exist.");
	}

	public function __set($name, $val) {
		if(array_key_exists($name, $this->_attachment)) {
			$this->_attachment[$name] = $val;
		} else {
			throw new \Exception("Member $name is not exist.");
		}
	}

	private static function guardFileOwner($file_id) {
		global $Auth, $DB;
		$DB->SetTable('nsau_files');
		$DB->AddField('id');
		$DB->AddCondFS('id', '=', $file_id);
		$DB->AddCondFS('user_id', '=', $Auth->user_id);
		$row = $DB->FetchAssoc($DB->Select(1));
		if(!empty($row) || self::guardIsModerator()) {
			return true;
		} else {
			throw new \Exception("You are not file owner.");
		}
	}

	public static function guardIsModerator() {
		global $Engine, $Auth;
		return $Engine->OperationAllowed(7, 'files.moderation', -1, $Auth->usergroup_id);
	}

	private function getSubjectName() {
		global $DB;
		$DB->SetTable('nsau_subjects');
		$DB->AddCondFS('id', '=', $this->subject_id);
		$subj = $DB->FetchAssoc($DB->Select(1));
		return $subj['name'];
	}

	private function getSpecialityName() {
		global $DB;
		$DB->SetTable('nsau_specialities');
		$DB->AddCondFS('id', '=', $this->spec_id);
		$spec = $DB->FetchAssoc($DB->Select(1));
		return $spec['name'];
	}

	private function getAttachType() {
		global $DB;
		$DB->SetTable('nsau_file_view');
		$DB->AddCondFS('id', '=', $this->view_id);
		$type = $DB->FetchAssoc($DB->Select(1));
		return $type['view_name'];
	}

	private function getProfileName() {
		global $DB;
		$DB->SetTable('nsau_profiles');
		$DB->AddCondFS('id', '=', $this->profile_id);
		$type = $DB->FetchAssoc($DB->Select(1));
		return $type['name'];
	}

	private function getStatus() {
		$approved = array('-1' => '�� ��������', '1' => '��������', '' => '�� ��������');
		return $approved[$this->approved];
	}

	private function getStatusStyle() {
		$style = array('-1' => 'danger', '1' => 'success', '' => 'default');
		return $style[$this->approved];
	}

}
<?
namespace files;

class SubjectAttachments {

	private $_attachments;

	public function __construct($file_id) {
		global $DB;
		$DB->SetTable('nsau_files_subj');
		$DB->AddCondFS('file_id', '=', $file_id);
		$DB->AddField('id');
		$res = $DB->Select();
		while($attach = $DB->FetchAssoc($res)) {
			$this->_attachments[] = new SubjectAttachment($attach['id']);
		}
	}

	public function delete() {
		foreach ($this->_attachments as $Attachment) {
			$Attachment->delete();
		}
	}

	public function unapprove() {
		foreach ($this->_attachments as $Attachment) {
			$Attachment->unapprove();
		}
	}

	public function getAttachments() {
		return $this->_attachments;
	}

}

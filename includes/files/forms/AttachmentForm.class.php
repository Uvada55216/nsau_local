<?
namespace files\forms;


use forms\BaseForm;

class AttachmentForm extends BaseForm{

	public function rules() {
		return array(
				array('fields' => array('edu_forms', 'file_id','subject_id', 'resourse_id', 'speciality_id'), 'validators' => array('integer', 'required')),
				array('fields' => array('qualification_id', 'profiles_id'), 'validators' => array('integer')),
				array('fields' => array('semesters'), 'validators' => array('integer')),
			);
	}


	public static function getFormData() {
		$arr['resourses'] = self::getResourses();
		$arr['qualifications'] = self::getQualifications();
		$arr['subjects'] = self::getSubjects();
		$arr['specialities'] = self::getSpecialities();
		return $arr;
	}



	private static function getSpecialities(){
		global $DB;
		$DB->SetTable("nsau_specialities");
		$DB->AddFields(array('name', 'code', 'id'));
		$DB->AddCondFS('old', '=', 0);
		$res = $DB->Select();
		while($row = $DB->FetchAssoc($res)) {
			$row['name'] = $row['code'] . ' - ' . $row['name'];
			$result[$row['id']] = $row;
		}
		return $result;
	}

	private static function getSubjects(){
		global $DB;
		$json = array();
		$DB->SetTable("nsau_subjects", "subj");
		$DB->AddTable("nsau_departments", "dep");
		$DB->AddCondFF("subj.department_id","=", "dep.id");		
		$DB->AddCondFS('subj.is_hidden', '=', 0);
		$DB->AddField("subj.id", "id");
		$DB->AddField("subj.name", "sname");
		$DB->AddField("dep.name", "dname");
		$DB->AddOrder("dep.name");
		$DB->AddOrder("subj.name");
		$res = $DB->Select();
		while($row = $DB->FetchAssoc($res)) {
			$row['name'] = $row['sname'];
			// $row['department'] = $row['dname'];
			
			$result[$row['dname']][$row['id']] = $row;
			// unset($row['dname'], $row['sname']);
		}
		return $result;	
	}

	private static function getQualifications(){
		global $DB;
		$DB->SetTable('nsau_qualifications');
		$DB->AddFields(array('name', 'id'));
		$res = $DB->Select();
		while($row = $DB->FetchAssoc($res)) {
			$result[$row['id']] = $row['name']; 
		}
		return $result;
	}

	private static function getResourses(){
		global $DB;
		$DB->SetTable('nsau_file_view');
		$DB->AddCondFO('is_sw', 'IS NULL');
		$DB->AddFields(array('view_name', 'id'));
		$DB->AddOrder('pos');
		$res = $DB->Select();
		while($row = $DB->FetchAssoc($res)) {
			$result[$row['id']] = $row['view_name']; 
		}
		return $result;
	}

	public static function searchSubject($name) {
		global $DB;
		$json = array();
		$DB->SetTable("nsau_subjects", "subj");
		$DB->AddTable("nsau_departments", "dep");
		$DB->AddCondFF("subj.department_id","=", "dep.id");
		$DB->AddCondFS("subj.name", "LIKE", "%".iconv("utf-8", "Windows-1251", $name)."%"); 
		$DB->AddField("subj.id", "id");
		$DB->AddField("subj.name", "sname");
		$DB->AddField("dep.name", "dname");
		$DB->AddOrder("dep.name");
		$DB->AddOrder("subj.name");
		$res = $DB->Select(100);
		while($row = $DB->FetchAssoc($res)) {
			$row['name'] = $row['sname'];
			$json[$row['id']] = $row;
		}
		self::ajaxHeaders();
		return $json;		
	}

	public static function searchSpeciality($code) {
		global $DB;
		$json = array();
		$DB->SetTable("nsau_specialities");
		$DB->AddCondFS("code", "LIKE", "%".iconv("utf-8", "Windows-1251", $code)."%"); 
		$DB->AddFields(array('name', 'code', 'id'));
		$res = $DB->Select(10);
		while($row = $DB->FetchAssoc($res)) {
			$row['name'] = $row['code'] . ' - ' . $row['name'];
			$json[$row['id']] = $row;
		}
		self::ajaxHeaders(); 
		return $json;
	}

	public static function selectProfile($id) {
		global $DB;
		$json = array();
		$DB->SetTable("nsau_profiles");
		$DB->AddCondFS("spec_id", "=", $id); 
		$DB->AddFields(array('name','id'));
		$res = $DB->Select();
		while($row = $DB->FetchAssoc($res)) {
			$json[$row['id']] = $row;
		}
		self::ajaxHeaders(); 
		return $json;
	}



}
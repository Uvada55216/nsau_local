<?
namespace files\forms;


use forms\BaseForm;

class CreateFolderForm extends BaseForm{

	public function rules() {
		return array(
			array('fields' => array('folder_name'), 'validators' => array('required'), 'filters' => array('text')),
		);
	}

}
<?
namespace files\forms;

use forms\FormValidator;
use forms\FormFilter;
use forms\FinalProcess;
use forms\Form;

class EditFileForm extends FileForm{

	public function rules() {
		return array(
			array('fields' => array('description'), 'validators' => array('required'), 'filters' => array('text')),
			array('fields' => array('file'), 'validators' => array('files\\validators\\nsauFilesSimpleFile')),
			array('fields' => array('file_sig'), 'validators' => array('files\\validators\\nsauFilesSigFile')),
			array('fields' => array('edition', 'year', 'folder_id', 'send_to_people', 'allow_download', 'zip_file', 'with_sig'), 'validators' => array('integer')),
			array('fields' => array('pages', 'author', 'place'), 'filters' => array('text')),
			array('fields' => array('sub_authors', 'file_id'), 'validators' => array('integer')),
		);
	}

	public function validate($FormData) {
		if($FormData["zip_file"]) {
			$this->rules = array(
				array('fields' => array('description'), 'validators' => array('required'), 'filters' => array('text')),
				array('fields' => array('file'), 'validators' => array('files\\validators\\nsauFilesZipHtmlFile')),
				array('fields' => array('edition', 'year', 'folder_id', 'send_to_people', 'allow_download', 'zip_file'), 'validators' => array('integer')),
				array('fields' => array('pages', 'author', 'place'), 'filters' => array('text')),
				array('fields' => array('sub_authors', 'file_id'), 'validators' => array('integer')),
			);
		}

		$FormValidate = new FormFilter(new FormValidator(new FinalProcess()));
		$FormValidate->process($Form = new Form($this->rules, $FormData));

		if(!$Form->isValid()) {
			$this->errors = $Form->getErrors();
			return false;
		} 
		$this->data = $Form->getData();
		return true;	
	}

	public static function getSubAuthors($file_id) {
		global $DB;
		$DB->SetTable("nsau_autors", "a");
		$DB->AddTable("nsau_people", "np");
		$DB->AddCondFS("a.id_file", "=", $file_id);
		$DB->AddCondFF('a.id_people', '=', 'np.id');
		$DB->AddField('np.name', 'name');
		$DB->AddField('np.last_name', 'last_name');
		$DB->AddField('np.patronymic', 'patronymic');
		$DB->AddField('np.id', 'id');
		$res = $DB->Select();
		while($row = $DB->FetchAssoc($res)) {
			$a[$row['id']] = $row['last_name'] . ' ' . $row['name'] . ' ' . $row['patronymic'];
		}
		return $a;
	}

	public static function getDownloadAccess($file_id) {
		global $DB;
		$DB->SetTable("engine_privileges");
		$DB->AddCondFS("operation_name", "=", "files.download");
		$DB->AddCondFS("entry_id", "=", $file_id);
		$DB->AddCondFS("usergroup_id", "=", 0);
		$DB->AddCondFS("is_allowed", "=", 1);
		$DB->AddCondFS("module_id", "=", 7);
		$row = $DB->FetchAssoc($DB->Select());
		if(!empty($row)) return 1;
	}

}
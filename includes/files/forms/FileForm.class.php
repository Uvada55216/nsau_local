<?
namespace files\forms;


use forms\BaseForm;

class FileForm extends BaseForm{

	public function rules() {
		return array(
			array('fields' => array('description'), 'validators' => array('required'), 'filters' => array('text')),
			array('fields' => array('file'), 'validators' => array('files\\validators\\nsauFilesSimpleFile', 'required')),
			array('fields' => array('file_sig'), 'validators' => array('files\\validators\\nsauFilesSigFile')),
			array('fields' => array('edition', 'year', 'folder_id', 'send_to_people', 'allow_download', 'zip_file', 'with_sig'), 'validators' => array('integer')),
			array('fields' => array('pages', 'author', 'place'), 'filters' => array('text')),
			array('fields' => array('sub_authors'), 'validators' => array('integer')),
		);
	}

	public function validate($Form) {
		if($Form["zip_file"]) {
			$this->rules = array(
				array('fields' => array('description'), 'validators' => array('required'), 'filters' => array('text')),
				array('fields' => array('file'), 'validators' => array('files\\validators\\nsauFilesZipHtmlFile', 'required')),
				array('fields' => array('edition', 'year', 'folder_id', 'send_to_people', 'allow_download', 'zip_file'), 'validators' => array('integer')),
				array('fields' => array('pages', 'author', 'place'), 'filters' => array('text')),
				array('fields' => array('sub_authors'), 'validators' => array('integer')),
			);
		}
		if(parent::validate($Form)) 
			return true;
	}

	public static function getUserFolders() {
		global $DB, $Auth;
		$DB->SetTable('nsau_files_folders');
		$DB->AddCondFS('user_id', '=', $Auth->user_id);
		$res = $DB->Select();
		while($row = $DB->FetchAssoc($res)) {
			$result[$row['id']] = $row['name'];
		}
		return $result;
	}

	public static function searchPeople($name) {
		global $DB;
		$json = array();
		$DB->SetTable("nsau_people");
		$DB->AddCondFS("last_name", "LIKE", "".iconv("utf-8", "Windows-1251", $name)."%");
		$DB->AddCondFO('status_id', " NOT IN( 9 ) ");
		$DB->AddFields(array('last_name', 'name', 'patronymic', 'id'));
		$DB->AddOrder("last_name");
		$res = $DB->Select(100);
		while($row = $DB->FetchAssoc($res)) {
			$row['name'] = $row['last_name'] . ' ' . $row['name'] . ' ' . $row['patronymic'];
			unset($row['last_name'], $row['patronymic']);
			$json[$row['id']] = $row;
		}
		self::ajaxHeaders();
		return $json;		
	}

	public static function searchPeopleWithUserId($name) {
		global $DB;
		$json = array();
		$DB->SetTable("nsau_people");
		$DB->AddCondFS("last_name", "LIKE", "".iconv("utf-8", "Windows-1251", $name)."%");
		$DB->AddCondFO('people_cat', " NOT IN( 1 ) ");
		$DB->AddCondFO('status_id', " NOT IN( 9 ) ");
		$DB->AddFields(array('last_name', 'name', 'patronymic', 'user_id'));
		$DB->AddOrder("last_name");
		$res = $DB->Select(100);
		while($row = $DB->FetchAssoc($res)) {
			$row['name'] = $row['last_name'] . ' ' . $row['name'] . ' ' . $row['patronymic'];
			unset($row['last_name'], $row['patronymic']);
			if(!empty($row['user_id'])) {
				$json[$row['user_id']] = $row;
			}
		}
		self::ajaxHeaders();
		return $json;		
	}

}
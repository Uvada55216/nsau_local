<?
namespace files\forms;


use forms\BaseForm;

class FolderForm extends BaseForm{

	public function rules() {
		return array(
			array('fields' => array('folder_name'), 'validators' => array('required'), 'filters' => array('text')),
			array('fields' => array('folder_id'), 'validators' => array('required', 'integer'), 'filters' => array('integer')),
		);
	}

}
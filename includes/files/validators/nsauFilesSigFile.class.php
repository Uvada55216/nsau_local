<?
namespace files\validators;

use \forms\validators\file;

class nsauFilesSigFile extends file {

	public function setConfig($cfg) {
		global $Engine, $Auth;		
		parent::setConfig($cfg);
		$cfg = parent::getConfig();
//		if($Engine->OperationAllowed(7, 'files.upload.msoffice', -1, $Auth->usergroup_id)) {
			$cfg['allowed_ext'] = array('sig');
			$cfg['max_size'] = 1;
//		}
		parent::setConfig($cfg);
	}

}
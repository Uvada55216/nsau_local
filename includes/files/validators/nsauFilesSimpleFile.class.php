<?
namespace files\validators;

use \forms\validators\file;

class nsauFilesSimpleFile extends file {

	public function setConfig($cfg) {
		global $Engine, $Auth;		
		parent::setConfig($cfg);
		$cfg = parent::getConfig();
		if($Engine->OperationAllowed(7, 'files.upload.msoffice', -1, $Auth->usergroup_id)) {			
			array_push($cfg['allowed_ext'], 'doc', 'docx');
		}
		parent::setConfig($cfg);
	}

}
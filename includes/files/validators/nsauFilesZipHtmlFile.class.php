<?
namespace files\validators;

use \forms\validators\ValidatorInterface;
use \forms\validators\NotArrayInterface;

class nsauFilesZipHtmlFile implements ValidatorInterface, NotArrayInterface {

	private $_error;
	protected $max_size = 150;
	protected $allowed_ext = array('zip');
	protected $allowed_files_ext = array("png", "jpg", "css", "jpeg", "html", "htm", "gif", "js");

	public function valid($data) {
		if(empty($data)) return true;
		$parts = explode('.', $data['name']);
		$ext = end($parts);

		if($data['size']>(1024*1024*$this->max_size)) {
			$this->_error[] = '�������� ������������ ������ ����� - ' . $this->max_size . ' mb';
		}
		if(!in_array($ext, $this->allowed_ext)) {
			$this->_error[] = '��� ������ ��������� ���� ������ ���� � ������� - (' . implode($this->allowed_ext, ', ') . ')';
			return false;
		}

		$zip = new \ZipArchive();
		$zip->open($data['tmp_name']);
		for ($i=0; $i<$zip->numFiles;$i++) {
			$curfile = $zip->statIndex($i);
			$name_ext = explode(".", $curfile["name"]);
			$name_ext = array_reverse($name_ext);
			if(!in_array(mb_strtolower(trim($name_ext[0])), $this->allowed_files_ext))
				if(!$this->isZipDir($curfile["name"])){
					$deny_files = 1;
				}
			if ($curfile["name"] == "index.html" || $curfile["name"] == "index.htm")
				$index_exists = 1;
		}
		$zip->close();

		if(!$index_exists) {
			$this->_error[] = '� ������ ����������� ��������� ���� - index.html ���� index.htm';
		}
		if($deny_files) {
			$this->_error[] = '� ������ ������������ ����� � ������������ �����������. ����� ������� ���� � ������� - (' . implode($this->allowed_files_ext, ', ') . ')';
		}

		if($this->_error) return false;
		return true;
	}

	private function isZipDir($filename) {
		if(strpos($filename, "/")===false)
			return false;
		return true;
	}

	public function errors() {
		return $this->_error;
	}	
}
<?php
/**
 * ������� �� �������� People
 * ������� ��������� ���������� ������������ ��� ������
 */
class Group {
	/**
	 * ���-�� 4, 5 ��� ������ ����������
	 * @var array
	 */
	private $qua_subject_total = array();
	/**
	 * ���-�� 3, 4, 5 ��� ������ ����������
	 * @var array
	 */
	private $abs_subject_total = array();
	/**
	 * ���-�� 4, 5 ��� ���� ������
	 * @var int
	 */
	private $qua_group_total;
	/**
	 * ���-�� 3, 4, 5 ��� ���� ������
	 * @var int
	 */
	private $abs_group_total;
	private $subjects_list;
	/**
	 * ���-�� ������� � ������
	 * @var int
	 */
	private $peoples;
	/**
	 * ������ �������� People
	 * @var array
	 */
	private $peoples_list;
	/**
	 * ����� ���� ������
	 * @var int
	 */
	private $middle_score;

	/**
	 * @param array $subjects_list ���������� ������������� � ������ array("����������_���")
	 */
	public function __construct($subjects_list) {
		$this->subjects_list = $subjects_list;	
	}

	/**
	 * ��������� �������� � ������
	 * ������������ ���-�� ������
	 * @param People $people [description]
	 */
	public function addPeople(People $people) {
		foreach($this->subjects_list as $subject) {
			$Subj = $people->getSubject(mb_strtolower($subject));
			if(gettype($Subj) == "object")
			if($Subj->isCountable()) {
				$val = $Subj->getVal();
				if(($val["score"]==5) || ($val["score"]==4)) {
					$this->qua_subject_total[$subject]++; 
					$this->qua_group_total++; 
				}
				if(($val["score"]==3) || ($val["score"]==4) || ($val["score"]==5)) {
					$this->abs_subject_total[$subject]++; 
					$this->abs_group_total++; 
				}
			}	
		}

		$this->middle_score += $people->getMiddleScore();

		$this->peoples_list[] = $people;
		$this->peoples++;
	}

	/**
	 * ���������� ������������ �� ������
	 * @return float 
	 */
	public function getAbsGroup() {
		return round(($this->abs_group_total/count($this->abs_subject_total)*100)/$this->peoples, 1);
	}

	/**
	 * ���������� ������������ ��� 1 ���������� ������
	 * @param string $name ����������_���
	 * @return float 
	 */
	public function getAbsSubject($name) {
		return round(($this->abs_subject_total[$name]*100)/$this->peoples, 1);
	}

	/**
	 * ������������ ������������ �� ������
	 * @return float 
	 */
	public function getQuaGroup() {
		return round(($this->qua_group_total/count($this->qua_subject_total)*100)/$this->peoples, 1);
	}

	/**
	 * ������������ ������������ ��� 1 ���������� ������
	 * @param string $name ����������_���
	 * @return float 
	 */
	public function getQuaSubject($name) {
		return round(($this->qua_subject_total[$name]*100)/$this->peoples, 1);
	}

	/**
	 * ������ �������� People ����������� � ������
	 * @return array 
	 */
	public function peoplesList() {
		return $this->peoples_list;
	}

	/**
	 * ������� ���� �� ������
	 * @return [type] [description]
	 */
	public function getMiddleScore() {
		return round($this->middle_score/$this->peoples, 1);
	}
}




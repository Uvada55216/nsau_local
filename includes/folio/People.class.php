<?php
require_once "Subject.class.php";
require_once "int/PeopleInterface.class.php";
/**
 * Добавляет студенту информацию о дисциплинах
 * дает доступ к данным по дисциплине
 */
class People implements PeopleInterface {
	private $subjects = array();
	private $name;
	private $middle_score;
	/**
	 * Добавляет студенту дисциплины
	 * @param array $study array("name" => $name, "type" => $type, "val" => $val)
	 * @param string $name  имя студента
	 */
	public function __construct($study, $name) {
		foreach($study as $subject) {
			$this->subjects[mb_strtolower($subject["name"]."_".$subject["type"])] = new Subject($subject["name"], $subject["type"], $subject["val"]);
		}
		$this->name = $name;
	}

	/**
	 * Получить объект дисциплины (Subject)
	 * @param  string $name дистиплина_тип, например математика_экзамен
	 * @return object       объект дисциплины (Subject)
	 */
	public function getSubject($name) {
		return $this->subjects[$name];
	}

	/**
	 * Имя студента
	 * @return string 
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Подсчет среднего балла студента
	 * @return float 
	 */
	public function getMiddleScore() {
		foreach($this->subjects as $s_id => $Subj) {
			if(gettype($Subj) == "object")
			if($Subj->isCountable()) {
				$val = $Subj->getVal();
				if(($val["score"]==3) || ($val["score"]==4) || ($val["score"]==5) || ($val["score"]==0)) {
					$p_score += $val["score"];
					$p_subj[$s_id] = 1;
				}
			}
		}
		return round($p_score/count($p_subj), 1);
	}

}
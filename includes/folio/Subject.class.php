<?php
/**
 * �������� � ������ ���� ������ � ���� �� �������� ��������.
 * ���������� ������� �� �� ���������� ������������, ������� ����.
 */
class Subject {
	private $val;
	private $name;
	private $type;
	private $countable = 0;

	/**
	 * ��������� ������� ���������� �� ����������
	 * @param string $name ��� ����������
	 * @param string $type ��� ����������
	 * @param string $val  ������
	 */
	public function __construct($name, $type, $val) {
		$this->val = $val;
		$this->type = $type;
		$this->name = $name;
		$counted_types = array('�������', '���.�����', '��������');
		if(in_array(mb_strtolower($type), $counted_types)) {
			$this->countable = true;
		}
	}

	/**
	 * ������
	 * @return array ������. ������ ������� � ������ ������
	 */
	public function getVal() {
		return $this->convertor(mb_strtolower($this->val));
	}

	/**
	 * ��� ���������� � �����
	 * @return string 
	 */
	public function getNameWithType() {
		return $this->name ." (". $this->type . ") ";
	}

	/**
	 * ��������� ��� ������ ���������� ������������
	 * @return [bool] true = yes
	 */
	public function isCountable() {
		return $this->countable;
	}

	/**
	 * ��������� ������ � ������ ����
	 * @param  string $val ������ �� xxx
	 * @return string]     ������� ������
	 */
	private function convertor($val) {
		$notgood = array("�������������������");
		$fail = array("�� ������", "�� ���� �", "���", "�� �������", "�� ������", "�� �������", "���������", "�� ���.");
		$awesome = array("�������");
		$good = array("������", "�-�����");
		$soso = array("����������", "����������", "����������");
		$zachet = array("�������", "�������", "�����");
		
		if(in_array($val, $notgood)) {
			return array("text" => "�� ����.", "score" => 0);
		}
		if(in_array($val, $fail)) {
			return array("text" => "�� ���.", "score" => 0);
		}
		if(in_array($val, $zachet)) {
			return array("text"  => "���.", "score" => 1);
		}
		if(in_array($val, $awesome)) {
			return array("text"  => "���.", "score" => 5);
		}
		if(in_array($val, $good)) {
			return array("text"  => "���.", "score" => 4);
		}
		if(in_array($val, $soso)) {
			return array("text"  => "��.", "score" => 3);
		}		

		return array("text"  => $val, "score" => 0);
	}
}
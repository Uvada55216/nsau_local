<?
namespace forms;

abstract class BaseForm {
	protected $errors;
	protected $rules;
	protected $data;

	/**
	 * В ней дочерний класс описывает правила валидации и фильтрации
	 * @return array 
	 * [
		    [
		     'fields' => ['field0'],                     // field0 -  поле которое будет провалидировано $FileForm->validate($_POST); будет обработано $_POST['field0']
		     'validators' => ['integer', 'required'],    //integer, required - валидаторы которые будет использованы к полям fields
		     'filters' => ['somefilter'],                //somefilter - фильтр которым будут обработаны данные, получаемые $FileForm->getData()    
		    ],
		    [
		     'fields' => ['field1'],                                  // field1 -  поле которое будет провалидировано $FileForm->validate($_POST); будет обработано $_POST['field1']
		     'validators' => ['length' => ['min' => 2, 'max' => 3]],  //length(с использованием параметров) - валидаторы которые будет использованы к полям fields
		     'filters' => ['integer'],                                //integer - фильтр которым будут обработаны данные, получаемые $FileForm->getData()    
		    ]    
		]
	 * 
	 */
	abstract public function rules();

	public function __construct() {
		$this->rules = $this->rules();
	}

	/**
	 * Валидирует и фильтрует данные формы
	 * @param  array $FormData данные формы
	 * @return bool           true - валидация пройдена, false - не пройлена
	 */
	public function validate($FormData) {
		$FormValidate = new FormFilter(new FormValidator(new FinalProcess()));
		$FormValidate->process($Form = new Form($this->rules, $FormData));

		if(!$Form->isValid()) {
			$this->errors = $Form->getErrors();
			return false;
		} 
		$this->data = $Form->getData();
		return true;			
	} 

	/**
	 * Возвращает массив прощедший через фильтры
	 * @return array [description]
	 */
	public function getData() {
		return $this->data;
	}

	/**
	 * Возвращает ошибки возникшие при валидации
	 * @return array [description]
	 */
	public function getErrors() {
		return $this->errors;
	}

	/**
	 * зря зря зря
	 * убрать бы это г отсюда
	 * лень
	 * @return bullshit [description]
	 */
	protected static function ajaxHeaders() {
		header("Cache-Control: no-cache, must-revalidate");
		header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
		header("Pragma: no-cache"); 
	}

}
<?
namespace forms;
class Form {

	public $data;
	public $rules;
	public $errors;
	public $valid_data;

	public function __construct($rules, $data) {
		foreach($rules as $rule) {
			$this->rules[] = $rule;
		}
		$this->data = $data;
	}

	public function getErrors() {
		return $this->errors;
	}

	public function getData() {
		return $this->valid_data;
	}

	public function isValid() {
		if(empty($this->errors))
			return true;
	}

}
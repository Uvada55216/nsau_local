<?
namespace forms;
class FormFilter extends FormDecorator {
	public function process(Form $FormData) {
		foreach($FormData->rules as $rule) {
			foreach($rule['fields'] as $field) {
				if(empty($rule['filters'])) {
					$rule['filters'] = array('safe');
				}
				foreach($rule['filters'] as $filter) {
					$className = '\\forms\\filters\\' . $filter;
					$Filter = new $className;
					if(is_array($FormData->data[$field])) {
						foreach ($FormData->data[$field] as $key => $value) {
							$FormData->valid_data[$field][$key] = $Filter->filter($FormData->valid_data[$field][$key] ? $FormData->valid_data[$field][$key] : $value);
						}
					} else {
						$FormData->valid_data[$field] = $Filter->filter($FormData->valid_data[$field] ? $FormData->valid_data[$field] : $FormData->data[$field]);
					}					
				}
			}
		}
		$this->processes->process($FormData);
	}
}
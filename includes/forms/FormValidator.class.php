<?
namespace forms;

class FormValidator extends FormDecorator {
	public function process(Form $FormData) {
		foreach($FormData->rules as $rule) {
			foreach($rule['fields'] as $field) {						
				foreach($rule['validators'] as $index => $validator) {		

					if(is_array($validator)) {
						$validator = $index;
						$cfg = $rule['validators'][$index];
					}

					if(strpos($validator, '\\') === false) {
						$className = '\\forms\\validators\\' . $validator;
					} else {
						$className = $validator;
					}
                    $Validator = new $className;
					if(is_array($FormData->data[$field]) && !($Validator instanceof validators\NotArrayInterface)) {
						$err = $this->validateArray($className, $FormData->data[$field], $cfg);
						if(!empty($err)) {
							$FormData->errors[$field][] = $err;
						}
					} 
					
					if(!is_array($FormData->data[$field]) || ($Validator instanceof validators\NotArrayInterface)){				
						$Validator = new $className;
						if($Validator instanceof validators\ConfigurableInterface) {
							$Validator->setConfig($cfg);
						}
						if(!$Validator->valid($FormData->data[$field])) {
							$FormData->errors[$field][] = $Validator->errors();
						}	
					}
					
				}
			}
		}
		$this->processes->process($FormData);
	}


	private function validateArray($className, $data, $cfg = null) {
		foreach ($data as $key => $value) {
			if(is_array($value)) {
				$error = $this->validateArray($className, $value);
				if(!empty($error)) {
					$errors= $error;
				}
			} else {
				$Validator = new $className;
				if($Validator instanceof validators\ConfigurableInterface) {
					$Validator->setConfig($cfg);
				}
				if(!$Validator->valid($value)) {
					$error = $Validator->errors();
					if(!empty($error)) {
						$errors= $error;
					}
				}
			}

		}
		return $errors;
	}

}
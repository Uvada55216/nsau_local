<?
namespace forms\filters;

interface FilterInterface {
	/**
	 * ФИльтрует полученные данные
	 * @param  mixed $data Данные для обработки
	 * @return mixed       отфильрованные данные
	 */
	public function filter($data);
}
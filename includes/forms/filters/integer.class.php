<?
namespace forms\filters;

class integer  implements FilterInterface{
	public function filter($data) {
		return intval($data);
	}
}
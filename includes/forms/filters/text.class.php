<?
namespace forms\filters;

class text  implements FilterInterface{
	public function filter($data) {
		return htmlspecialchars($data);
	}
}
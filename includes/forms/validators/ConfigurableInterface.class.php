<?
namespace forms\validators;
/**
 * Настраиваемые валидаторы
 */
interface ConfigurableInterface{
	/**
	 * Установка настроек валидатора
	 * @param array $cfg настройки
	 */
	public function setConfig($cfg);
}
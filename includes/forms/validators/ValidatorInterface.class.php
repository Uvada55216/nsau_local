<?
namespace forms\validators;

interface ValidatorInterface {
	/**
	 * Проверка данных
	 * @param  mixed $data данные для проверки
	 * @return bool       результат проверки
	 */
	public function valid($data);

	/**
	 * Возвращает массив с ошибками формата:
	 * ['error1', 'error2']
	 * @return array ошибки валидации
	 */
	public function errors();
}
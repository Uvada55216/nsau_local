<?
namespace forms\validators;

class file implements ValidatorInterface, ConfigurableInterface, NotArrayInterface {

	private $_config;
	private $_error;
	protected $max_size = 100;
	protected $allowed_ext = array('zip', 'jpeg', 'jpg', 'gif', 'pdf', 'xls', 'xlsx', 'ppt', 'pptx', 'rtf', 'dotx', 'dot', 'cdr');

	public function setConfig($cfg) {
		$this->_config = $cfg;
		$this->_config['allowed_ext'] = $cfg['allowed_ext'] ? $cfg['allowed_ext'] : $this->allowed_ext;
		$this->_config['max_size'] = $cfg['max_size'] ? $cfg['max_size'] : $this->max_size;
	}

	public function getConfig() {
		return $this->_config;
	}

	public function valid($data) {
		if(empty($data)) return true;
		$parts = explode('.', $data['name']);
		$ext = end($parts);

		if($data['size']>(1024*1024*$this->_config['max_size'])) {
			$this->_error[] = '�������� ������������ ������ ����� - ' . $this->_config['max_size'] . ' mb';
		}
		if(!in_array($ext, $this->_config['allowed_ext'])) {
			$this->_error[] = '�������� �������� ������ � ����������� - (' . implode($this->_config['allowed_ext'], ', ') . ')';
		}
		if($this->_error) return false;
		return true;
	}

	public function errors() {
		return $this->_error;
	}	
}
<?
namespace forms\validators;

class integer  implements ValidatorInterface{

	private $_error;

	public function valid($data) {
		if (!is_numeric($data) && !empty($data)) {
			$this->_error[] = '������ ���� ������, ���������� �����.';
			return false;
		}
		return true;
	}

	public function errors() {
		return $this->_error;
	}	
}
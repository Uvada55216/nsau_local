<?
namespace forms\validators;

class length implements ValidatorInterface, ConfigurableInterface, NotArrayInterface{
	private $_error;
	private $_config;
	protected $min = 1;
	protected $max = 1;

	public function setConfig($cfg) {
		$this->_config = $cfg;
		$this->_config['max'] = $cfg['max'] ? $cfg['max'] : $this->max;
		$this->_config['min'] = $cfg['min'] ? $cfg['min'] : $this->min;
	}

	public function valid($data) {
		// \CF::Debug($data);die;
		if(is_string($data)) {
			if((mb_strlen($data) > $this->_config['max']
			|| mb_strlen($data) < $this->_config['min']) && !empty($data)) {
				$this->_error[] = "����� ������ ������ ���� � ��������� �� {$this->_config['min']} �� {$this->_config['max']} ��������.";
				return false;
			}		
			return true;
		}
		if(is_array($data)) {
			if((count($data) > $this->_config['max']
			|| count($data) < $this->_config['min']) && !empty($data)) {
				$this->_error[] = "���������� ��������� ������ ���� � ��������� �� {$this->_config['min']} �� {$this->_config['max']}.";
				return false;
			}		
			return true;	
		}
	}

	public function errors() {
		return $this->_error;
	}	
}
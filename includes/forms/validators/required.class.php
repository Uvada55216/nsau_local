<?
namespace forms\validators;

class required implements ValidatorInterface, NotArrayInterface{

	private $_error;

	public function valid($data) {
		if (empty($data)) {
			$this->_error[] = '���� ����������� ��� ����������';
			return false;
		}
		return true;
	}

	public function errors() {
		return $this->_error;
	}	
}
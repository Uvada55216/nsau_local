<?php
namespace helpers;

class EncodingHelper
{
	/**
	 * utf8 -> cp1251
	 * @param string $str string
	 * @return string      result
	 */	
	public static function u2w($str) 
	{
		return iconv('utf-8', 'cp1251', $str);
	} 

	/**
	 * cp1251 -> utf8
	 * @param string $str string
	 * @return string      result
	 */
	public static function w2u($str) 
	{
		return iconv('cp1251', 'utf-8', $str);
	}

	/**
	 * Рекурсивная смена кодировки значений массива
	 * @param  [type] $array    Массив для преобразования
	 * @param  [type] $callback Функция преобразования из текущего класса
	 * @return [type]           Преобразованный массив
	 */
	public static function changeArrayEncoding($array, $callback)
	{
        if (!is_array($array)) {
            return self::$callback($array);
        }

		if(empty($callback)) return $array;
		foreach ($array as $key => $value) {
			if(is_array($value)) {
				$array[$key] = self::changeArrayEncoding($value, $callback);
			} else {
				$array[$key] = self::$callback($value);
			}
		}
		return $array;
	}

}
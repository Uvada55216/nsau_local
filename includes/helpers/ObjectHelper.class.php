<?php
namespace helpers;

class ObjectHelper 
{
	public static function json_encode($obj) 
	{
		if (get_magic_quotes_gpc()) {
			return quotemeta(json_encode($obj));
		} else {
			return json_encode($obj);
		}
	}

	public static function serialize($obj) 
	{
		if (get_magic_quotes_gpc()) {
			return quotemeta(serialize($obj));
		} else {
			return serialize($obj);
		}
	}
}
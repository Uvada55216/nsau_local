<?
namespace helpers;

class TplHelper {
	public static function beginCase($tpl, $case, $_input = null){
		$MODULE_OUTPUT = $_input;
		$MODULE_OUTPUT["mode"] = $case;
		include THEMES_DIR . $tpl . TEMPLATE_EXT;
		//Закоментил временно. НЕ работает на 5.4. Что логично ибо буффер не инициализирован
		//=>при отключении перестают работать вложенные конструкции
		// return ob_get_clean();
	}

	public static function endCase($x) {
		echo $x;
	}

	public static function registerJs($path, $variables = null) {
		$js_content = file_get_contents($_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . SCRIPTS_DIR . $path);
		foreach($variables as $id => $val) {
			$js_content = str_replace('%' . $id . '%', $val, $js_content);
		}
		echo $js_content; 
	}

}
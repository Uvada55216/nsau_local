<?php
namespace modules\AbitCalculator;

use \engine\modules\configElements\Select;

class AbitCalculatorConfig extends \engine\modules\BaseModuleConfig
{
	public $mode;
	private $__mode;

	public function __construct() 
	{
		$this->__mode = array
		(
			array('id' => 'calculator', 'name' => '����������� ���'),
		);
	}

	public function params() 
	{
		return array( 
			'����� ������' => array(
				'element' => new Select('mode', $this->mode, $this->__mode),
				'obligatory' => 'true',
				'switcher' => 'true',
				'group' => 'mode',
			)							  
		);
	}

}
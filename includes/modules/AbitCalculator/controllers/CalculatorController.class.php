<?php

namespace modules\AbitCalculator\controllers;

use repository\ExamRepository;
use helpers\EncodingHelper as EncHelp;

class CalculatorController extends \engine\controllers\BaseController
{

	/**
	 *
	 */
	public function actionIndex(){
		$exams = ExamRepository::getAssocEduLevelWithExams();

        $data['data'] = array(
            'exams'=> !empty($exams) ? array($exams[1]) : array(),
        );

		$this->renderAjax('css/styles'); //подключение css
		$this->renderJs('script');    //подключение js файла

		return $this->render('CalcList', $data);
    }

	/**
	 *
	 */
    public function getSpecialitiesByExams(){
	    $request = str_replace('\\', '', $_REQUEST['data']);
	    $data = json_decode($request, true);
	    $request = EncHelp::changeArrayEncoding($data, 'u2w');

	    if(!empty($request) && !empty($request['pass'])){
		    $examsAbit = !empty($request['not'])
			    ? ExamRepository::getSpecialtyForAbitByScoresExam($request['pass'], $request['not'])
			    : ExamRepository::getSpecialtyForAbitByScoresExam($request['pass']);
	    }

	    return $this->renderJson($examsAbit, 'w2u');
    }


}
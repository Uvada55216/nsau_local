<div id="content">
	<div class="calc">
		<div class="calc__title">
			�������� ��������
		</div>

		<?php if(!empty($inputData['exams'])){ ?>
			<div class="calc__plate">
                <div class="calc__plate-title">
                    ������� ������� ������ �����������
                </div>
                <nav class="cs_nav_tabs">
                    <div id="nav-calc-tab" class="" role="tablist">
                        <?php foreach($inputData['exams'] as $key=>$exam) { ?>
                            <div class="nav-item nav-link cs_nav_item <?= ($key == 0) ? "active" : "";?>"
                                id="nav-tab_<?=$exam['id']?>"
                                data-toggle="tab"
                                href="#nav-<?=$exam['id']?>"
                                role="tab"
                                aria-controls="nav-<?=$exam['id']?>"
                                aria-selected="true"
                                >������� / ������</div>
                        <?php } ?>
                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
	                <?php foreach($inputData['exams'] as $key=>$exam) { ?>
                        <div class="tab-pane fade <?= ($key == 0) ? "active in" : "";?>" id="nav-<?=$exam['id']?>" role="tabpanel" aria-labelledby="nav-tab_<?=$exam['id']?>">
                            <div class="calc__plate-grid">
                                <?php foreach($exam['exams'] as $k=>$ex) { ?>
                                    <div class="calc__plate-item">
                                        <div class="main__checkbox">
                                            <input type="checkbox" data-range="<?=$ex['id']?>" id="exam_<?=$ex['id']?>" name="checkbox-<?=$ex['id']?>" value="<?=$ex['min_vyz']?>">
                                            <label for="exam_<?=$ex['id']?>"><?=$ex['name']?></label>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
	                <?php } ?>
                </div>
			</div>
            <div class="calc__ranges">
                <?php foreach($inputData['exams'] as $key=>$exam){
	                foreach($exam['exams'] as $k_r => $ex_r){ ?>
                        <div class="calc__range" data-range="<?=$ex_r['id']?>" data-exam-id="exam_<?=$ex_r['id']?>"
                             data-point="<?=$ex_r['min_vyz']?>" style="display: none">
                            <strong><?=$ex_r['name']?></strong>
                            <input type="text" class="calc__range-slider" name="my_range" value=""/>
                        </div>
	                <?php }
                }?>
            </div>
		<?php } ?>
		<div class="calc__cards">
		</div>
        <div class="calc__card tmp" style="display: none">
            <div class="calc__card-left">
                <div class="calc__card-data">
                </div>
                <div class="calc__card-theme">
                    <div class="calc__card-title">
                    </div>
                    <div class="calc__card-subtitle">
                    </div>
                    <div class="calc__card-row" style="display: inline-flex;">
                        <strong>����������� ����� ���/��:</strong>
                        <div class="card_ex"></div>
                    </div>
                </div>
            </div>
            <div class="calc__card-scores"></div>
        </div>
        <div class="popover top tmp">
            <div class="arrow"></div>
            <div class="popover-content">
                <p>������ ���. ����� ���. �����.</p>
            </div>
        </div>
	</div>

</div>
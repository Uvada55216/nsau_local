<link rel="stylesheet" href="/themes/styles/ion.rangeSlider.min.css" type="text/css" />
<style type="text/css">
    .calc .main__checkbox input + label {
        transition: all ease .3s; }

    .calc .main__checkbox input {
        position: absolute;
        z-index: -1;
        opacity: 0;
        width: auto; }
    .calc .main__checkbox input + label {
        display: inline-flex;
        align-items: center;
        user-select: none;
        position: relative;
        cursor: pointer;
        padding-left: 35px; }
    .calc .main__checkbox input + label:hover {
        color: #ffd18d; }
    .calc .main__checkbox input + label span {
        opacity: 0.5;
        font-size: 12px;
        font-weight: 400;
        line-height: 2; }
    .calc .main__checkbox input + label:before {
        position: absolute;
        left: 0px;
        top: 0px;
        content: '';
        display: inline-block;
        width: 20px;
        height: 20px;
        flex-shrink: 0;
        flex-grow: 0;
        background-repeat: no-repeat;
        background-position: center center;
        background-size: 50% 50%;
        background-color: #f7f7f7; }
    .calc .main__checkbox input:checked + label::before {
        background-color: #f7f7f7;
        background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 8 8'%3e%3cpath fill='black' d='M6.564.75l-3.59 3.612-1.538-1.55L0 4.26 2.974 7.25 8 2.193z'/%3e%3c/svg%3e"); }
    .calc .main__checkbox input:disabled + label::before {
        background-color: #e9ecef; }

    .calc__plate-title, .calc__range strong {
        font-size: 18px; }
    @media screen and (max-width: 1919px) {
        .calc__plate-title, .calc__range strong {
            font-size: 18px; } }
    @media screen and (max-width: 1279px) {
        .calc__plate-title, .calc__range strong {
            font-size: 18px; } }
    @media screen and (max-width: 1023px) {
        .calc__plate-title, .calc__range strong {
            font-size: 18px; } }
    @media screen and (max-width: 639px) {
        .calc__plate-title, .calc__range strong {
            font-size: 13px; } }

    .calc__plate, .calc__ranges {
        margin-bottom: 50px; }
    @media screen and (max-width: 1919px) {
        .calc__plate, .calc__ranges {
            margin-bottom: 50px; } }
    @media screen and (max-width: 1279px) {
        .calc__plate, .calc__ranges {
            margin-bottom: 30px; } }
    @media screen and (max-width: 1023px) {
        .calc__plate, .calc__ranges {
            margin-bottom: 30px; } }
    @media screen and (max-width: 639px) {
        .calc__plate, .calc__ranges {
            margin-bottom: 20px; } }

    .calc__title {
        margin-bottom: 40px; }
    @media screen and (max-width: 1919px) {
        .calc__title {
            margin-bottom: 40px; } }
    @media screen and (max-width: 1279px) {
        .calc__title {
            margin-bottom: 30px; } }
    @media screen and (max-width: 1023px) {
        .calc__title {
            margin-bottom: 30px; } }
    @media screen and (max-width: 639px) {
        .calc__title {
            margin-bottom: 30px; } }

    .calc__plate-title, .calc__card-subtitle {
        margin-bottom: 30px; }
    @media screen and (max-width: 1919px) {
        .calc__plate-title, .calc__card-subtitle {
            margin-bottom: 30px; } }
    @media screen and (max-width: 1279px) {
        .calc__plate-title, .calc__card-subtitle {
            margin-bottom: 30px; } }
    @media screen and (max-width: 1023px) {
        .calc__plate-title, .calc__card-subtitle {
            margin-bottom: 30px; } }
    @media screen and (max-width: 639px) {
        .calc__plate-title, .calc__card-subtitle {
            margin-bottom: 20px; } }

    #nofooter, .wrapper {
        min-width: auto; }

    .calc {
        font-family: 'Roboto', sans-serif; }
    .calc__title {
        font-weight: bold;
        font-size: 30px; }
    @media screen and (max-width: 1919px) {
        .calc__title {
            font-size: 30px; } }
    @media screen and (max-width: 1279px) {
        .calc__title {
            font-size: 30px; } }
    @media screen and (max-width: 1023px) {
        .calc__title {
            font-size: 20px; } }
    @media screen and (max-width: 639px) {
        .calc__title {
            font-size: 18px; } }
    .calc__plate {
        padding-left: 50px;
        padding-right: 50px;
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #a30118;
        background-image: url("../../images/12_1_nsau.png");
        background-repeat: no-repeat; }
    @media screen and (max-width: 1919px) {
        .calc__plate {
            padding-left: 50px;
            padding-right: 50px; } }
    @media screen and (max-width: 1279px) {
        .calc__plate {
            padding-left: 30px;
            padding-right: 30px; } }
    @media screen and (max-width: 1023px) {
        .calc__plate {
            padding-left: 30px;
            padding-right: 30px; } }
    @media screen and (max-width: 639px) {
        .calc__plate {
            padding-left: 20px;
            padding-right: 20px; } }
    @media screen and (max-width: 1919px) {
        .calc__plate {
            padding-top: 40px;
            padding-bottom: 40px; } }
    @media screen and (max-width: 1279px) {
        .calc__plate {
            padding-top: 40px;
            padding-bottom: 40px; } }
    @media screen and (max-width: 1023px) {
        .calc__plate {
            padding-top: 40px;
            padding-bottom: 40px; } }
    @media screen and (max-width: 639px) {
        .calc__plate {
            padding-top: 20px;
            padding-bottom: 20px; } }
    .calc__plate-grid {
        display: grid;
        grid-template-columns: repeat(3, 1fr);
        grid-column-gap: 30px;
        grid-row-gap: 30px; }
    @media screen and (max-width: 1919px) {
        .calc__plate-grid {
            grid-template-columns: repeat(3, 1fr); } }
    @media screen and (max-width: 1279px) {
        .calc__plate-grid {
            grid-template-columns: repeat(3, 1fr); } }
    @media screen and (max-width: 1023px) {
        .calc__plate-grid {
            grid-template-columns: repeat(2, 1fr); } }
    @media screen and (max-width: 639px) {
        .calc__plate-grid {
            grid-template-columns: repeat(1, 1fr); } }
    @media screen and (max-width: 1919px) {
        .calc__plate-grid {
            grid-column-gap: 30px; } }
    @media screen and (max-width: 1279px) {
        .calc__plate-grid {
            grid-column-gap: 30px; } }
    @media screen and (max-width: 1023px) {
        .calc__plate-grid {
            grid-column-gap: 30px; } }
    @media screen and (max-width: 639px) {
        .calc__plate-grid {
            grid-column-gap: 30px; } }
    @media screen and (max-width: 1919px) {
        .calc__plate-grid {
            grid-row-gap: 30px; } }
    @media screen and (max-width: 1279px) {
        .calc__plate-grid {
            grid-row-gap: 30px; } }
    @media screen and (max-width: 1023px) {
        .calc__plate-grid {
            grid-row-gap: 30px; } }
    @media screen and (max-width: 639px) {
        .calc__plate-grid {
            grid-row-gap: 30px; } }
    .calc__plate-title {
        color: white; }
    .calc__range {
        display: flex;
        align-items: end; }
    .calc__range strong {
        font-weight: 500;
        flex-shrink: 0;
        margin-right: 50px;
        margin-bottom: 2px;
        width: 160px;
        margin-right: 15px; }
    @media screen and (max-width: 1919px) {
        .calc__range strong {
            margin-right: 50px; } }
    @media screen and (max-width: 1279px) {
        .calc__range strong {
            margin-right: 40px; } }
    @media screen and (max-width: 1023px) {
        .calc__range strong {
            margin-right: 30px; } }
    @media screen and (max-width: 639px) {
        .calc__range strong {
            margin-right: 0px; } }
    .calc__card {
        border: 1px solid #e6e6e6;
        background-color: #f5f5f5;
        background-repeat: no-repeat;
        background-image: url("../../images/2.webp");
        padding-left: 50px;
        padding-right: 50px;
        padding-top: 40px;
        padding-bottom: 40px;
        display: flex;
        justify-content: space-between;
        margin-bottom: 10px; }
    @media screen and (max-width: 1919px) {
        .calc__card {
            padding-left: 50px;
            padding-right: 50px; } }
    @media screen and (max-width: 1279px) {
        .calc__card {
            padding-left: 30px;
            padding-right: 30px; } }
    @media screen and (max-width: 1023px) {
        .calc__card {
            padding-left: 30px;
            padding-right: 30px; } }
    @media screen and (max-width: 639px) {
        .calc__card {
            padding-left: 20px;
            padding-right: 20px; } }
    @media screen and (max-width: 1919px) {
        .calc__card {
            padding-top: 40px;
            padding-bottom: 40px; } }
    @media screen and (max-width: 1279px) {
        .calc__card {
            padding-top: 40px;
            padding-bottom: 40px; } }
    @media screen and (max-width: 1023px) {
        .calc__card {
            padding-top: 40px;
            padding-bottom: 40px; } }
    @media screen and (max-width: 639px) {
        .calc__card {
            padding-top: 20px;
            padding-bottom: 20px; } }
    @media screen and (max-width: 1280px) {
        .calc__card {
            flex-direction: column; } }
    .calc .main__checkbox input + label {
        color: white; }
    .calc__card-data {
        width: 80px;
        height: 80px;
        margin-right: 40px;
        background: #a30118;
        color: white;
        font-size: 12px;
        display: flex;
        justify-content: center;
        align-items: center;
        border-radius: 50%;
        flex-shrink: 0; }
    @media screen and (max-width: 1919px) {
        .calc__card-data {
            width: 80px; } }
    @media screen and (max-width: 1279px) {
        .calc__card-data {
            width: 80px; } }
    @media screen and (max-width: 1023px) {
        .calc__card-data {
            width: 60px; } }
    @media screen and (max-width: 639px) {
        .calc__card-data {
            width: 60px; } }
    @media screen and (max-width: 1919px) {
        .calc__card-data {
            height: 80px; } }
    @media screen and (max-width: 1279px) {
        .calc__card-data {
            height: 80px; } }
    @media screen and (max-width: 1023px) {
        .calc__card-data {
            height: 60px; } }
    @media screen and (max-width: 639px) {
        .calc__card-data {
            height: 60px; } }
    @media screen and (max-width: 1919px) {
        .calc__card-data {
            margin-right: 40px; } }
    @media screen and (max-width: 1279px) {
        .calc__card-data {
            margin-right: 30px; } }
    @media screen and (max-width: 1023px) {
        .calc__card-data {
            margin-right: 20px; } }
    @media screen and (max-width: 639px) {
        .calc__card-data {
            margin-right: 0px; } }
    @media screen and (max-width: 1023px) {
        .calc__card-data {
            margin-bottom: 10px; } }
    .calc__card-title {
        font-size: 28px;
        margin-bottom: 10px;
        font-weight: 500; }
    @media screen and (max-width: 1919px) {
        .calc__card-title {
            font-size: 28px; } }
    @media screen and (max-width: 1279px) {
        .calc__card-title {
            font-size: 28px; } }
    @media screen and (max-width: 1023px) {
        .calc__card-title {
            font-size: 20px; } }
    @media screen and (max-width: 639px) {
        .calc__card-title {
            font-size: 18px; } }
    .calc__card-subtitle {
        font-size: 16px;
        color: #a30118;
        font-weight: bold; }
    .calc__card-left {
        display: flex; }
    @media screen and (max-width: 1280px) {
        .calc__card-left {
            margin-bottom: 15px; } }
    @media screen and (max-width: 1023px) {
        .calc__card-left {
            flex-direction: column; } }
    .calc__card-row {
        margin-bottom: 10px; }
    .calc__card-row strong {
        font-weight: normal;
        display: inline-block;
        width: 200px;
        margin-right: 20px; }
    @media screen and (max-width: 1023px) {
        .calc__card-row strong {
            width: 100%;
            margin-right: 0;
            margin-bottom: 10px; } }
    .calc__card-row span {
        padding: 5px 10px;
        background: white;
        color: #a30118;
        font-weight: bold;
        margin-right: 5px;
        display: inline-block; }
    .calc__card-score {
        display: flex;
        align-items: center;
        margin-bottom: 10px; }
    .calc__card-score strong {
        width: 30px;
        height: 30px;
        background-color: #ffdeac;
        align-items: center;
        justify-content: center;
        display: inline-flex;
        margin-right: 20px;
        border-radius: 50%;
        font-size: 14px;
        color: #a30118;
        flex-shrink: 0; }
    .calc__card-scores {
        margin-left: 15px; }

    .irs--flat .irs-bar {
        background: #a30118;
        height: 4px; }

    .irs--flat .irs-line {
        height: 4px; }

    .irs--flat .irs-handle {
        background: #a30118;
        border-radius: 50%;
        width: 15px;
        height: 15px;
        cursor: pointer;
        top: 19px; }
    .irs--flat .irs-handle i {
        display: none !important; }

    .irs--flat .irs-single {
        position: absolute;
        background: transparent;
        left: auto !important;
        color: black;
        font-size: 16px;
        right: -50px;
        top: 15px; }
    .irs--flat .irs-single:before {
        display: none; }

    .irs {
        width: 100%;
        margin-right: 50px; }

    .cs_nav_tabs{
        margin-bottom: 30px;
    }

    .cs_nav_item{
        display: inline-block;
        color: white;
        font-size: 18px;
        /*text-decoration: none;*/
        margin-right: 15px;
        cursor: pointer;
    }

    .cs_nav_item.active {
        border-bottom: 3px solid white;;
    }


</style>
<script type="text/javascript" src="/scripts/ion.rangeSlider.min.js"></script>
<script type='text/javascript'>
	jQuery(document).ready(function($){

        $('.calc__plate-item label').on('click', function () {
            let checkbox = $(this).parents('.calc__plate-item').find('input');
            let data = checkbox.attr('data-range');
            let calc = $('.calc__range[data-range='+ data +']')
            let ionRangeSlider = $(calc.find('input.calc__range-slider')).data("ionRangeSlider");

            if(!checkbox.prop('checked')){
                calc.show();
                $('div.calc__cards').empty();
            }else {
                checkbox.attr('checked', false);
                calc.hide();
                ionRangeSlider.reset();
                getSpecialityCards();
                checkbox.attr('checked', true);
            }
        });


        $('#nav-calc-tab .nav-item').on('click', function () {
            let tabId = $('.nav-item.active').attr('id');

            $('div[aria-labelledby="'+tabId+'"]').find('input:checkbox:checked').each(function () {
                let id = $(this).attr('id');
                let data = $(this).attr('data-range');
                let calc = $('.calc__range[data-range='+ data +']');

                $(calc.find('input.calc__range-slider')).data("ionRangeSlider").reset();

                $('.calc__ranges').find('div[data-exam-id="'+id+'"]').css('display', 'none');

                $(this).attr('checked' , false);
            });

            $('div.calc__cards').empty();

            $('.nav-item').removeClass('active');

            $(this).addClass('active');

        });

        //let optionsToolTip = {
        //    theme: 'tooltipster-borderless',
        //    //interactive: true,
        //    touchDevices: false,
        //    autoClose: false,
        //    trigger: 'custom',
        //    side: ['top'],
        //    functionPosition: function(instance, helper, position){
        //            position.coord.top -= 26;
        //            //position.coord.left += 10;
        //            return position;
        //     },
        //    contentAsHTML: false,
        //    functionInit: function(instance, helper){
        //        //
        //        let content = '������ ���. ����� ���. �����.';
        //        // save the edited content
        //        instance.content(content);
        //    }
        //};



        $('.calc__range-slider').ionRangeSlider({
            type: "single",
            step: 1,
            min: 0,
            max: 100,
            from: 0,
            //from_min: 29,
            hide_min_max: true,
            keyboard: false,
            onStart: function(data){
                data.from = 0;
                //data.form_min = $(data.input).parent().attr('data-point');
            },
            onChange: function(data) {

            },
            onFinish: function(data) {
                //let minPoint = parseInt($(data.input).parent().attr('data-point'));
                //if(parseInt(data.from) <= minPoint){
                    //let pop = $('div.popover.top.tmp').clone().removeAttr("style").removeClass('tmp');
                    //data.slider.append(pop);

                    //console.log(data.slider);
                //}else{
                //}

                getSpecialityCards();
            },
            onUpdate: function (data) {
            }
        });

        function getSpecialityCards(){
            let form = $('div.calc');
            let formData = { pass: [], not: [] };

                form.find('input:checkbox:checked').each(function () {
                    let id = $(this).attr('id'),
                        minVuzPoint = parseInt($(this).val()),
                        abitPoint = parseInt(form.find('[data-exam-id="'+id+'"] span.irs-single').html()),
                        obj = id.split('_', 2)[1];

                    if(abitPoint >= minVuzPoint) {
                        formData.pass.push(obj);
                    }else {
                        formData.not.push(obj);
                    }
                });

                if((formData.not.length + formData.pass.length) >= 3){
                        let request = {
                        'route': '<?=$__route?>',
                        'action': 'getSpecialitiesByExams',
                        'module': '<?=$__module?>',
                        'context': '<?=$__context?>',
                        'type': 'json',
                        'data': JSON.stringify(formData)
                    };

                    $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        url: '/ajax_mode/',
                        data: request,
                        success: function( data ) {
                             $('div.calc__cards').empty();

                            if(data){
                                $.each(data, function(index, value) {
                                    let card = $('div.calc__card.tmp').clone().removeAttr("style").removeClass('tmp');
                                    card.find('div.calc__card-data').html(value.speciality_code);
                                    card.find('div.calc__card-title').html('<a href="/speciality/' + value.speciality_code + '/' + value.spec_id +'/" target="_blank">'+value.spec_name+'</a>');
                                    card.find('div.calc__card-subtitle').html('<a href="' + value.fac_link +'" target="_blank">'+ value.fac_name +'</a>');

                                    if(value.exams.length > 0){
                                        $.each(value.exams, function (index, exam) {
                                            card.find('div.calc__card-row div.card_ex').append(cardExam(exam));
                                        });
                                    }

                                    if(value.exams_addit.length > 0){
                                        $.each(value.exams_addit, function (index, exam) {
                                            card.find('div.calc__card-row div.card_ex').append(cardExam(exam));
                                        });
                                    }

                                    if('budget' in value){
                                        card.find('div.calc__card-scores').append(cardAdmissPlan('��������� �����', value.budget));
                                    }

                                    if('comerce' in value){
                                        card.find('div.calc__card-scores').append(cardAdmissPlan('���������� �����', value.comerce));
                                    }

                                    card.appendTo('div.calc__cards');
                                });
                            }
                        },
                        error: function(){
                            //infoAlert('#vote', '������ ������!');
                        }
                    });
                }
        }

        function cardExam(ex){
            let name = ex.name.split(' ', 2);
            if (name.length > 1 && name[1][0] != '('){
                name = name[0].charAt(0).toUpperCase() + '.' + name[1].charAt(0).toUpperCase() + '.';
            }else{
                name = name[0].charAt(0).toUpperCase() + '.';
            }

            return '<span style="margin-bottom: 5px;">' + name + ' - ' + ex.min_vyz + '</span>';
        }

        function cardAdmissPlan(plase, plan){
            return '<div class="calc__card-score"><strong>' + plan + '</strong><span>' + plase + '</span></div>';
        }

	});
</script>
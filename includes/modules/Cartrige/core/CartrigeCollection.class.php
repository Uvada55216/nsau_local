<?php

namespace modules\Cartrige\core;


use entity\cartrige\Cartrige;

class CartrigeCollection
{

	/**
	 * @var Client1C
	 */
	protected $client1c;

	/**
	 * @var array
	 */
	protected $data;

	/**
	 * @var array|void
	 */
	protected $arrResult;


	public function __construct(\engine\core1C\Client1C $client){
		$this->client1c = $client;
		$this->getDataBy1C();
	}

	/**
	 * @return void
	 */
	private function getDataBy1C(){
		$r = $this->client1c->getListCartriges();

		if(!empty($r)){
			foreach($r->Cartrige as $el){
				$cartNum = (int)$el->number;
				$this->data[$cartNum] = new Cartrige($el);

				if((int)$el->issued){
					unset($this->data[$cartNum]);
					continue;
				}
			}
		}
	}


	/**
	 * @return array
	 */
	public function getCartBySubdivision(){
		$rs = array();
		if(!empty($this->data)){
			$rs = array_fill_keys(array_values($this->getSubdivision()), array());
			foreach($this->data as $el){
				if(isset($rs[$el->getSubdivision()])){
					$rs[$el->getSubdivision()][$el->getNumber()] = $el;
				}
			}
		}

		return $rs;
	}

	/**
	 * @return array
	 */
	public function getSubdivision(){
		$rs = array();
		if(!empty($this->data)){
			foreach($this->data as $el){
				if(!in_array($el->getSubdivision(), $rs)){
					$rs[] = $el->getSubdivision();
				}
			}
		}

		return $rs;
	}

}
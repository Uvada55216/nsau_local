<?php

namespace modules\Enrollee;

class Enrollee extends \engine\modules\BaseModule
{
	public function rules() {
		return array(
			array('GET|POST', '/', $this->initParams->mode . 'Controller#actionIndex'),
			array('GET|POST', 'people/[a:eduForm]/[i:id]', $this->initParams->mode . 'Controller#actionPeople'),
			array('GET|POST', 'people/[i:id]', $this->initParams->mode . 'Controller#actionPeople'),
			array('GET|POST', '[a:action]/[a:param]', $this->initParams->mode . 'Controller#[action]'),
			array('GET|POST', 'speciality/[a:eduForm]/[sc:code]/[a:program]', $this->initParams->mode . 'Controller#actionSpecialitySPO'),
			array('GET|POST', 'speciality/[a:eduForm]/[a:code]', $this->initParams->mode . 'Controller#actionSpeciality'),
			array('GET|POST', '[a:type]/[a:eduForm]/[a:code]/download', $this->initParams->mode . 'Controller#actionDownload'),
			array('GET|POST', '[a:type]/[a:eduForm]/[sc:code]/[a:program]/download', $this->initParams->mode . 'Controller#actionDownload'),
			array('GET|POST', '[a:type]/[a:eduForm]/download', $this->initParams->mode . 'Controller#actionDownload'),
			array('GET|POST', 'cache', $this->initParams->mode . 'Controller#actionCache'),
		);
	} 
}
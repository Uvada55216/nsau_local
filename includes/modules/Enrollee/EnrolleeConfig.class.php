<?php

namespace modules\Enrollee;

use \engine\modules\configElements\Select;
use \engine\modules\configElements\Input;
use \engine\modules\configElements\Checkbox;

class EnrolleeConfig extends \engine\modules\BaseModuleConfig
{
	public $mode;
	public $idPk;
	public $viewType;
	public $formEducation;
	public $levelEducation;
	public $cacheLifetime;
	public $useCache;
	//format d.m.Y
	public $datePrikaz;


	private $__mode = array(array('id' => 'Reports', 'name' => '������ �� (Reports)'),
						  array('id' => 'Enrollee', 'name' => '������ ������������ (Default)')
					);
	public $__idPk = array(

								array('id' => '000000047', 'name' => '�����������/����������� 2023 (000000047)'),
								array('id' => '000000046', 'name' => '���� 2023 (000000046)'),
								array('id' => '000000045', 'name' => '��� 2023 (000000045)'),
								array('id' => '000000044', 'name' => '������������ 2023 (000000044)'),
								array('id' => '000000043', 'name' => '����������� 2023 (000000043)'),

								array('id' => '000000038', 'name' => '���� 2022 (000000038)'),
								array('id' => '000000037', 'name' => '��� 2022 (000000037)'),
								array('id' => '000000039', 'name' => '�����������/����������� 2022 (000000039)'),
								array('id' => '000000035', 'name' => '����������� 2022 (000000035)'),
								array('id' => '000000036', 'name' => '������������ 2022 (000000036)'),

								array('id' => '000000027', 'name' => '����������� 2021 (000000027)'),
								array('id' => '000000028', 'name' => '������������ 2021 (000000028)'),
								array('id' => '000000029', 'name' => '��� 2021 (000000029)'),
								array('id' => '000000030', 'name' => '���� 2021 (000000030)'),
								array('id' => '000000031', 'name' => '�����������/����������� 2021 (000000031)'),
		//								array('id' => '000000021', 'name' => '�����������/����������� 2020 (000000021)'),
		//								array('id' => '000000018', 'name' => '������������ 2020 (000000018)'),
		//								array('id' => '000000017', 'name' => '����������� 2020 (000000017)'),
		//								array('id' => '000000020', 'name' => '���� 2020 (000000020)'),
		//								array('id' => '000000019', 'name' => '��� 2020 (000000019)'),
		//								array('id' => '000000014', 'name' => '�����������/����������� 2019 (000000014)'),
		//								array('id' => '000000003', 'name' => '������������ 2018 (000000003)'),
		//								array('id' => '000000012', 'name' => '������������ 2019 (000000012)'),
		//								array('id' => '000000005', 'name' => '����������� 2018 (000000005)'),
		//								array('id' => '000000011', 'name' => '����������� 2019 (000000011)'),
		//								array('id' => '000000007', 'name' => '���� 2018 (000000007)'),
		//								array('id' => '000000013', 'name' => '���� 2019 (000000013)'),
					);
	private $__viewType = array(array('id' => 'Table', 'name' => '�������'),
								array('id' => 'List', 'name' => '������ � �����������')
							   );
	public $__formEducation = array(array('id' => '�����', 'name' => '�����'),
									 array('id' => '�������', 'name' => '�������'),
									 array('id' => '����-�������', 'name' => '����-�������')
					   			);
	private $__levelEducation = array(array('id' => 'Magistracy', 'name' => '������������'),
									 array('id' => 'Graduate', 'name' => '�����������'),
									 array('id' => 'Bachelor', 'name' => '�����������'),
									 array('id' => 'SPO', 'name' => '���')
					   			);


	public function params() 
	{
		return array( 
			'����� ������ (��������� ����������)' => array(
				'element' => new Select('mode', $this->mode, $this->__mode),
				'obligatory' => 'true',
				'switcher' => 'true',
				'group' => 'mode',
			),
			'���' => array(
				'element' => new Select('viewType', $this->viewType, $this->__viewType),
				'group' => 'mode',
				'subGroup' => 'Enrollee',
				'obligatory' => 'true',
			),
			'����� ��������' => array(
				'element' => new Select('formEducation', $this->formEducation, $this->__formEducation),
				'group' => 'mode',
				'subGroup' => 'Enrollee',
				'obligatory' => 'true',
			),
			'��� ������� ������������' => array(
				'element' => new Select('levelEducation',  $this->levelEducation, $this->__levelEducation),
				'group' => 'mode',
				'obligatory' => 'true',
			),
			'Id �������� ��������' => array(
				'element' => new Select('idPk', $this->idPk, $this->__idPk),
				'obligatory' => 'true',
			),
			'���� ������� (d.m.Y)' => array(
				'element' => new Input('datePrikaz', $this->datePrikaz),
			),
			'������������ ���' => array(
				'element' => new Checkbox('useCache', $this->useCache)
			),	
			'����� ����� ����' => array(
				'element' => new Input('cacheLifetime', $this->cacheLifetime),
			),								  
		);
	}
}
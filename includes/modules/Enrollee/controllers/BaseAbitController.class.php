<?php

namespace modules\Enrollee\controllers;

use modules\Enrollee\core\AbitRequests;
use modules\Enrollee\core\RequestParams;
use modules\Enrollee\core\AbitClient1c;


abstract class BaseAbitController extends \engine\controllers\BaseController
{
	public $Params1c;
	public $Client1c;
	public $engineUri;

	public function __construct(\engine\modules\BaseModule $Creator)
	{ 
		parent::__construct($Creator);
		$this->engineUri = $Creator->contextData->engineUri;

		//отловить исключения и рендерить страницу с ошибкой.
		
		$this->Module->initParams->mode == 'Enrollee' ? $init1c = false : $init1c = true;

		$this->Params1c = new RequestParams();
		$this->Params1c->setParam('idPk', $Creator->initParams->idPk)
						->setParam('specialityForm', $Creator->initParams->formEducation)
						->setParam('educationLevel', $Creator->initParams->levelEducation);
		$this->Client1c = new AbitClient1c($init1c);
		$this->configureClientCache();
	}

	private function configureClientCache()
	{
		if(!empty($this->Module->initParams->cacheLifetime)) {
			$this->Client1c->setCacheLifetime($this->Module->initParams->cacheLifetime);
		}
		if($this->Module->initParams->useCache) {
			$this->Client1c->enableCache();

		} else {
			$this->Client1c->disableCache();	
		}		
	}
}
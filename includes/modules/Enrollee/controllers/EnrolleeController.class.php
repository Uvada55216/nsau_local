<?php

namespace modules\Enrollee\controllers;

use modules\Enrollee\core\AbitRequests;
use modules\Enrollee\core\SpecialityCollection;
use modules\Enrollee\core\SpecialityFacultyCollection;
use modules\Enrollee\core\Speciality;
use modules\Enrollee\core\AbitFilter;
use modules\Enrollee\core\AbitFilterSPO;
use modules\Enrollee\core\AbitCollection;
use modules\Enrollee\core\AbitCollectionSPO;
use modules\Enrollee\core\AbitClient1c;


use \requests\Request;
use \engine\db\Db;
use \engine\core1C\Helper1C as H;

class EnrolleeController extends BaseAbitController
{
	public function actionIndex()
	{
		$Specialities = new SpecialityCollection($this->Params1c, $this->Client1c);
        $output['formEducation'] = $this->Module->initParams->formEducation;

        if($this->Module->initParams->levelEducation === 'SPO') {
            if ($this->Module->initParams->viewType == 'Table') {
                $Spec = new SpecialityFacultyCollection($this->Params1c, $this->Client1c);
                $output['specialities'] = $Spec->getSpecialities();;
            } elseif ($this->Module->initParams->viewType == 'List') {
                $output['specialities'] = $Specialities->getSpecialities();
            }
        }else{
            $output['specialities'] = $Specialities->getSpecialities();
        }

		return $this->render('Specialities' . $this->Module->initParams->viewType, $output);
	}

	public function actionSpeciality($specialityCode, $formEducation = null)
	{
		$request = Request::init()->post();

        /* Доставляем префиксы:
         * Факультет среднего профессионального образования => H_
         * Куйбышевский сельскохозяйственный техникум => K_
         * */
        switch ($request['faculty']){
            case H::u2w('Факультет среднего профессионального образования'):
                $this->Params1c->setParam('specialityCode', 'Н_'.$specialityCode);
                $this->Params1c->setParam('education', $request['education']);
                $facultySpeciality = $this->Client1c->getSpecialityFaculty(array(
                        'idPk' => $this->Params1c->idPk,
                        'form' => $this->Params1c->specialityForm,
                        'specialityCode'=> $this->Params1c->specialityCode,
                        'education'=> $this->Params1c->education)
                );
                break;

            case H::u2w("Куйбышевский сельскохозяйственный техникум"):
                $this->Params1c->setParam('specialityCode', 'К_'.$specialityCode);
                $this->Params1c->setParam('education', $request['education']);
                $facultySpeciality = $this->Client1c->getSpecialityFaculty(array(
                    'idPk' => $this->Params1c->idPk,
                    'form' => $this->Params1c->specialityForm,
                    'specialityCode'=> $this->Params1c->specialityCode,
                    'education'=> $this->Params1c->education)
                );
                break;

            default:
                $this->Params1c->setParam('specialityCode', $specialityCode);
                $facultySpeciality = $this->Client1c->getSpecialityInfo(array(
                        'idPk' => $this->Params1c->idPk,
                        'form' => $this->Params1c->specialityForm,
                        'specialityCode'=> $this->Params1c->specialityCode
                    )
                );
                break;
        }

        $output['speciality'] = new Speciality($facultySpeciality);

		//проверяем дату приказа
		//если совподает то скрываем ФИО
		$date = new \DateTime();
		$output['prikaz'] = ((!empty($this->Module->initParams->datePrikaz)) && (strtotime($date->format('d.m.Y')) >= strtotime($this->Module->initParams->datePrikaz)))? true: false;

        $formEducation ? $this->setFormEducation($formEducation) : null;
		if(method_exists('modules\Enrollee\core\AbitFilter', $request['budgetType'])
		&& method_exists('modules\Enrollee\core\AbitFilter', $request['listType'])) {

            if ($this->Module->initParams->levelEducation === 'SPO'){
                $Abits = new AbitCollectionSPO($this->Params1c, $this->Client1c, $output['speciality']);

                $output['abits'] = AbitFilterSPO::init($Abits)
                    ->{$request['listType']}()->{$request['budgetType']}()
                    ->getAbits();
            }else{
                $Abits = new AbitCollection($this->Params1c, $this->Client1c, $output['speciality']);

                $output['abits'] = AbitFilter::init($Abits)
                    ->{$request['listType']}()->{$request['budgetType']}()
                    ->getAbits();
            }

        } else {
			$Abits = new AbitCollection($this->Params1c, $this->Client1c, $output['speciality']);
			$abitsTypeFilterFunction = 'is' . $this->Module->initParams->levelEducation;

			$output['abits'] = AbitFilter::init($Abits)
											->active()
											->allBudgetTypes()
											->{$abitsTypeFilterFunction}()
											->getAbits();
		}

		$Specialities = new SpecialityCollection($this->Params1c, $this->Client1c);
		$output['specialities'] = $Specialities->getSpecialities();
		$output['post'] = $request;
		$output['tableType'] = $this->Module->initParams->levelEducation . 'Table';
		return $this->render('AbitsTable', $output);
	}

	public function actionPeople($peopleId)
	{
		$this->Client1c = new AbitClient1c(true);
		$this->Params1c->setParam('peopleId', $peopleId);
		$Abits = new AbitRequests($this->Params1c, $this->Client1c);
		$output = $Abits->getAbits();
		//проверяем дату приказа
		//если совподает то скрываем ФИО
		$date = new \DateTime();
		$output['prikaz'] = ((!empty($this->Module->initParams->datePrikaz)) && (strtotime($date->format('d.m.Y')) >= strtotime($this->Module->initParams->datePrikaz)))? true: false;
		return $this->render('PeopleTable', $output);
	}

	public function actionSearch($name)
	{
		$params = array('idPk' => $this->Params1c->idPk,
						'name' => $name,
						'educationForm' => $this->Params1c->specialityForm,
					);
		$result = $this->Client1c->searchPeople($params);
		return $this->renderAjax('blocks/PeopleSearchLinks', $result);
	}

	private function setFormEducation($form) {
		switch ($form) {
			case 'ft':
				$this->Params1c->setParam('specialityForm', H::u2w('Очная'));
			break;
			case 'ex':
				$this->Params1c->setParam('specialityForm', H::u2w('Заочная'));
			break;
			case 'ftex':
				$this->Params1c->setParam('specialityForm', H::u2w('Очно-заочная'));
			break;
		}
	}

}
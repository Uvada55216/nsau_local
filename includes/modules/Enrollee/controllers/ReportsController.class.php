<?php

namespace modules\Enrollee\controllers;

use modules\Enrollee\core\AbitClient1c;
use modules\Enrollee\core\AbitRequests;
use modules\Enrollee\core\SpecialityReportCollectionSPO;
use \modules\Enrollee\core\SpecialityCollection;
use \modules\Enrollee\core\SpecialityReportCollection;
use \modules\Enrollee\core\Speciality;
use \modules\Enrollee\core\AbitCollection;
use \modules\Enrollee\core\AbitCollectionSPO;
use \modules\Enrollee\core\AbitFilter;
use \modules\Enrollee\core\AbitFilterSPO;

use \requests\Request;
use \engine\core1C\Helper1C as H;

class ReportsController extends BaseAbitController
{
	public function actionIndex()
	{ 
		$out['idPk'] = $this->Module->initParams->idPk;
		$this->renderJs('Reports');
		return $this->render('Reports', $out);
	}

	public function actionSpecialitites()
	{
        $request = Request::init()->ajax();
		$this->renderAjax('blocks/SpecialitiesReportTable', $this->getSpecialities($request['type']));
	}

	public function actionSpeciality($formEducation, $specialityCode)
	{
		return $this->render('blocks/SpecialityReportTable', $this->getAbits($formEducation, $specialityCode));
	}

    public function actionSpecialitySPO($formEducation, $specialityCode, $programEducation)
    {
        return $this->render('blocks/SpecialityReportTable', $this->getAbitsSPO($formEducation, $specialityCode, $programEducation));
    }

	public function actionDownload($type, $formEducation, $specialityCode = null, $programEducation = null)
	{
		global $Engine;
		$Engine->main_template = "_blank";
		$start_html = '<!DOCTYPE html><html><head><meta charset="windows-1251"></head><body>';
		$end_html = '</body></html>';

		ob_start(function($buffer) {return (str_replace("<a", "<span", $buffer));});
								
		switch($type) {
			case 'speciality': {
				$this->docHeaders('report_'.date('d.m.Y'));
				echo $start_html;
				$this->renderAjax('blocks/SimpleReportsStyles');

				if ($this->Module->initParams->levelEducation === 'SPO'){
                    $this->renderAjax('blocks/SPOTable', $this->getAbitsSPO($formEducation, $specialityCode, $programEducation));
                }else {
                    $this->renderAjax('blocks/BachelorTable', $this->getAbits($formEducation, $specialityCode));
                }

				echo $end_html;
			} break;
			case 'specialities': {
                $this->docHeaders('report_'.date('d.m.Y'));
				echo $start_html;
				$this->renderAjax('blocks/SimpleReportsStyles');
				$this->renderAjax('blocks/SpecialitiesReportTable', $this->getSpecialities($formEducation));
				echo $end_html;
			} break;
		}
		ob_end_flush();
	}


	protected function fullTime()
	{
		$this->Params1c->SetParam('specialityForm', '�����');
	}

	protected function extramural()
	{
		$this->Params1c->SetParam('specialityForm', '�������');
	}
	protected function extramural0()
	{
		$this->Params1c->SetParam('specialityForm', '����-�������');
	}

	public function actionPeople($formEducation, $peopleId)
	{
		$this->{$formEducation}();
		$this->Client1c = new AbitClient1c(true);
		$this->Params1c->setParam('peopleId', $peopleId);
		$Abits = new AbitRequests($this->Params1c, $this->Client1c);
		$output = $Abits->getAbits();
		return $this->render('PeopleTable', $output);
	}

	private function getSpecialities($formEducation)
	{		
		$this->{$formEducation}();
		$output['specialityForm'] = $formEducation;

        if($this->Module->initParams->levelEducation === 'SPO') {
            $Specialities = new SpecialityReportCollectionSPO($this->Params1c, $this->Client1c);
            $output['specialities'] = $Specialities->getSpecialities();

            /*
             * ������ URL ��� ���
             * */
            foreach ($output['specialities'] as $spName => $sp) {
                if ($spName === '��������� �������� ����������������� �����������'){
                    foreach($sp['specialities'] as $s) {
                        $programEducational = ($s->programEducational == '������� �����')? '11': ($s->programEducational == '�������� �����'? '09': "");
                        $s->url = "speciality/{$formEducation}/H_$s->specialityCode/{$programEducational}";
                    }
                }elseif ($spName === "������������ �������������������� ��������"){
                    foreach($sp['specialities'] as $s) {
                        $programEducational = ($s->programEducational == '������� �����')? '11': ($s->programEducational == '�������� �����'? '09': "");
                        $s->url = "speciality/{$formEducation}/K_$s->specialityCode/{$programEducational}";
                    }
                }
            }
        }else{
            $Specialities = new SpecialityReportCollection($this->Params1c, $this->Client1c);
            $output['specialities'] =  $Specialities->getSpecialities();
        }

		return $output;
	}

	private function getAbits($formEducation, $specialityCode) 
	{

		$this->Params1c->setParam('specialityCode', $specialityCode);
		$this->{$formEducation}();

		$SpecialityInfo = $this->Client1c->getSpecialityInfo(array(
			'idPk' => $this->Params1c->idPk, 
			'specialityCode'=> $this->Params1c->specialityCode, 
			'form' => $this->Params1c->specialityForm)
		);
		$output['formEducation'] = $formEducation;
		$output['speciality'] = new Speciality($SpecialityInfo);
		$output['tableType'] = $this->Module->initParams->levelEducation . 'Table';
		$Abits = new AbitCollection($this->Params1c, $this->Client1c);
		$output['abits'] = AbitFilter::init($Abits)->active()->allBudgetTypes()->getAbits();
		return $output;
	}

	private function getAbitsSPO($formEducation, $specialityCode, $programEducation){
        /*
         * ������ ������� ������� �� ��������� ��� 1�
         */
        if ($specialityCode[0] === 'K'){
            $strRpl = '�_';
            $specCpde  = str_replace('K_', $strRpl, $specialityCode);
        }elseif ($specialityCode[0] === 'H'){
            $strRpl = '�_';
            $specCpde  = str_replace('H_', $strRpl, $specialityCode);
        }

        $this->{$formEducation}();
        $this->Params1c->setParam('specialityCode', H::w2u($specCpde));
        $this->Params1c->setParam('education', $programEducation);

        $facultySpeciality = $this->Client1c->getSpecialityFaculty(array(
                'idPk' => $this->Params1c->idPk,
                'form' => $this->Params1c->specialityForm,
                'specialityCode'=> $this->Params1c->specialityCode,
                'education'=> $this->Params1c->education)
        );
		$output['formEducation'] = $formEducation;
        $output['speciality'] = new Speciality($facultySpeciality);
        $output['tableType'] = $this->Module->initParams->levelEducation . 'Table';
        $Abits = new AbitCollectionSPO($this->Params1c, $this->Client1c, $output['speciality']);
        $output['abits'] = AbitFilterSPO::init($Abits)->active()->allBudgetTypes()->getAbits();
        return $output;
    }

	private function docHeaders($filename) 
	{
		header('Content-Type: text/html; charset=windows-1251');
		header('P3P: CP="NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM"');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0', FALSE);
		header('Pragma: no-cache');
		header('Content-Transfer-Encoding: binary');
		header("Content-Disposition: attachment; filename=$filename.doc");
		header('Content-Type: application/msword');
	}

}
<?php
namespace modules\Enrollee\core;

use \engine\core1C\Helper1C as H;

class Abit
{
    public $name;
    public $priority;
    public $foundation;
    public $privilege;
    public $category;
    public $agreement = false;
    public $organization;
    public $peopleId;
    public $specialityCode;
    public $specialityName;
    public $ege;
    public $exam;
    public $achievements;
    public $achievementsScore;
    public $originalEducationDocuments = false;
    public $examScore;
    public $totalScore;
    public $status;
    public $speciality;
    public $form;
    public $failScore;
//    public $certificate;
    public $receptionFeatures;
    public $certificateScore;
    public $delo;
    public $snils;
    public $preferred;
    public $summary;
    public $highPriority = false;
    public $alreadyInPrikaz = false;
    public $priorityForExams;
    public $paid = false;


    private $Params;

    public function __construct($abitData, Speciality $Speciality = null, RequestParams $Params = null)
    {
        $this->Params = $Params;
        $this->name = H::u2w($abitData->Name);
        $this->form = H::u2w($abitData->Form);
        $this->priority = $abitData->Priority;
        $this->foundation = H::u2w($abitData->Foundations);
        $this->privilege = H::u2w($abitData->Privilege);
        $this->category = H::u2w($abitData->Category);
        $this->agreement = ($abitData->Agreement == 1) ? true : false;
        $this->organization = H::u2w($abitData->Organization);
        $this->peopleId = H::u2w($abitData->peopleId);
        $this->specialityCode = H::u2w($abitData->specialityCode);
        $this->specialityName = H::u2w($abitData->specialityName);
        $this->specialityName = H::u2w($abitData->specialityName);
        $this->status = H::CC2Str(H::u2w($abitData->Status));
        $this->speciality = $Speciality ? $Speciality : $this->getSpecialityInfo($this->specialityCode);

        $this->certificateScore = H::u2w($abitData->CertificateScore);
        $this->summary = H::u2w($abitData->CertificateScore);
	    //        $this->certificate = H::u2w($abitData->Certificate);
	    $this->receptionFeatures = H::u2w($abitData->ReceptionFeatures);
	    $this->delo = (int)($abitData->Delo);
	    $this->snils = H::u2w($abitData->Snils);
	    $this->preferred = (int)($abitData->Preferred);

	    foreach ($abitData->Exam as $achiv) {
            if($achiv->Form == 'Индивидуальное достижение'  && ($achiv->Form == $achiv->NeedForm)) {
                $this->achievements[H::u2w($achiv->Subject)] = $achiv->Score;
                $this->achievementsScore += $achiv->Score;
            }
        }
	    $this->highestEge($abitData->Exam);

	    //очередной костыль выбор экзамена с наивысшим баллом
	    //только для высшки
	    if(\CF::Win2Utf($this->speciality->educationLevel) == 'Бакалавриат' || \CF::Win2Utf($this->speciality->educationLevel) == 'Специалитет'){
		    $this->highestExamPriority($abitData->Exam);
	    }

	    foreach ($this->speciality->exams as $examName => $priority) {
            foreach ($abitData->Exam as $key => $Exam)  {
                if((H::u2w($Exam->Subject) == $examName) && ($Exam->Form == $Exam->NeedForm)) {
                    if($Exam->Form == 'ЕГЭ') {
                        $this->ege[$examName] = $Exam->Score;
                        $this->examScore += $Exam->Score;
                        $this->priorityForExams[$key] = $Exam->Score;
                    }
                    if($Exam->Form == 'Экзамен') {
                        $this->normalizeExam($Exam->Score);
                        $this->exam[$examName] = $Exam->Score;
	                    $this->summary += $Exam->Score;
                        $this->examScore += $Exam->Score;
                        $this->priorityForExams[$key] = $Exam->Score;
                    }

                }
            }
        }

        if(is_array($this->exam) && count($this->exam)) {
            foreach ($this->exam as $subj => $score) {
                $this->normalizeExam($this->speciality->examsMinScore[$subj]);
                if($score < $this->speciality->examsMinScore[$subj]) {
                     $this->failScore[$subj] = true;
                }
            }
        }
        if(is_array($this->ege) && count($this->ege) > 0) {
            foreach ($this->ege as $subj => $score) {
                if($score < $this->speciality->examsMinScore[$subj]) {
                     $this->failScore[$subj] = true;
                }
            }
        }
        $this->originalEducationDocuments = (isset($abitData->originalEducationDocuments))?$abitData->originalEducationDocuments:(($abitData->DocumentType == 'Оригинал') ? true : false);
        //$this->highPriority = $abitData->highPriority;
        $this->highPriority = $this->priority == "1";
        $this->totalScore = $this->examScore + $this->achievementsScore;
        $this->paid = $abitData->paid;
        //$this->status = ($abitData->paid)?"Включен в приказ":$this->status;

    }

    /**
     * Выполнение условий зачисления
     * оригинал документов и согласие на зачисление
     */
	public function fulfilledConditions(){
		if(\CF::Win2Utf($this->speciality->educationLevel) == 'Бакалавриат' || \CF::Win2Utf($this->speciality->educationLevel) == 'Специалитет'){
			if($this->originalEducationDocuments && $this->highPriority && ($this->getCountPassExams() == 3)){
				return true;
			}
		} else {
			if($this->originalEducationDocuments){
				return true;
			}
		}
	}

	/**
	 * Выполнение условий зачисления
	 * согласие на зачисление для платного поступления
	 */
	public function fulfilledConditionsCommerce(){
		if($this->originalEducationDocuments) {
			return true;
		}
	}

    /**
     * Абитуриент зачислен
     */
    public function isEnrolled()
    {
        if($this->status == H::u2w('Зачислен')) {
            return true;
        }
    }

     /**
     * Получить информацию по направлению подготовки
     * по коду
     * @param  string $specialityCode код специальности
     * @return abit\Speciality            abit\Speciality
     */
    protected function getSpecialityInfo($specialityCode)
    {
        $Client = new AbitClient1c(false);
        $SpecialityInfo = $Client->getSpecialityInfo(array(
            'idPk' => $this->Params->idPk,
            'specialityCode'=> $specialityCode,
            'form' => $this->Params->specialityForm)
        );
        return new Speciality($SpecialityInfo);
    }

    /**
     * Переводит текстовое обозначения экзамена
     * в числовое, для неявки меняет кодировку
     * @param  string &$exam оценка за экзамен
     * @return void
     */
    private function normalizeExam(&$exam) {
        switch ($exam) {
            case 'Удовлетворительно':
                $exam = 3;
            break;
            case 'Хорошо':
                $exam = 4;
            break;
            case 'Отлично':
                $exam = 5;
            break;
        }
        $exam = H::u2w($exam);
    }

    /**
     * Оставить максимальные балы еге по одной дисциплине
     * @param  array $ege массив ЕГЭ из $abitData
     * @return void
     */
    private function highestEge(&$ege)
    {
    	$b = array();
        foreach ($ege as $i => $exam) {
            if($exam->Form == 'ЕГЭ') {
                if($b[$exam->Subject]['score'] < $exam->Score) {
                    $b[$exam->Subject]['score'] = $exam->Score;
                    $b[$exam->Subject]['val'] = $exam;
                }
            }
            else {
                $a[] = $exam;
            }
        }
        foreach ($b as $ex) {
            $a[] = $ex['val'];
        }
        $ege = $a;
    }

	/**
	 * Количество экзаменнов и егэ
	 * @return int
	 */
    public function getCountPassExams(){
		return (count($this->ege) + count($this->exam));
    }

	/**
	 * Выбор экзаменов с наивысшим баллом
	 * @return void
	 */
	private function highestExamPriority(&$exams){
    	$rs = array();
    	$highExems = array();
		if($exams){
			foreach($exams as $i => $exam){
				if(isset($this->speciality->exams[\CF::Utf2Win($exam->Subject)]) && \CF::IsNaturalNum((int)$exam->Score) && $exam->Form == $exam->NeedForm){
					$rs[$this->speciality->exams[\CF::Utf2Win($exam->Subject)]][] = $exam;
				}
			}
			if($rs){
				foreach($rs as $key => $el) {
					$score = 0;
					foreach($el as $item){
						if((int)$item->Score >= $score ){
							$highExems[$key] = $item;
							$score = (int)$item->Score;
						}
					}
				}
			}

			$exams = $highExems;
		}

	}

}



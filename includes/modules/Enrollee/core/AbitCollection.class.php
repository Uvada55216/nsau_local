<?php
namespace modules\Enrollee\core;

use \engine\core1C\Helper1C as H;
/**
 * 	��������� ������������ �� ����������� ����������,
 * � ����� ��������
 */
class AbitCollection
{
	protected $abits;
	protected $abits_temp;
	protected $data;
	protected $foundations;
	protected $Client;
	protected $params;

	public function __construct(RequestParams $Params, \engine\core1C\Client1C $Client, Speciality $Speciality = null) 
	{
        $this->data = $Client->getAbitBySpeciality(array(
                'idPk' => $Params->idPk,
                'specialityCode'=>$Params->specialityCode,
                'form' => $Params->specialityForm)
        );

		$this->Client = $Client;
		$this->params = $Params;
		$this->speciality = $Speciality;
	}

	public function getAbits() 
	{		
		if(empty($this->speciality)) {
			$SpecialityInfo = $this->Client->getSpecialityInfo(array(
	            'idPk' => $this->params->idPk,
	            'specialityCode'=> $this->params->specialityCode,
	            'form' => $this->params->specialityForm)
	        );
	        $this->speciality = new Speciality($SpecialityInfo);
	    }
		foreach ($this->data->val as $value) {
			$abit = new Abit($value, $this->speciality);
			$key = ($abit->peopleId .':'.$abit->category .':'. $abit->foundation .':'. $abit->receptionFeatures .':'.$abit->priority);

			$this->abits[$key] = $abit;
		}
        $this->sortAbits();
        $this->sortEmptyAbits();
		$this->assignmentByFoundation();
		return $this->foundations;
	}


	/**
	 * ������������� �������� abit\Abit
	 * �� �������� ��������� �����������
	 * @return array array(
	 *  'foundation1'=> array(abit\Abit), 
	 * 	.....,
	 * 	'foundationN'=> array(abit\Abit)
	 * )
	 */
	protected function assignmentByFoundation() 
	{
		$this->foundations = array(
			'��� ������������� ���������' => false,
			'����. �����' => false,
			'������ �����' => false,
			'������� �����' => false, 
			'��������� ������' => false, 
			'������ ���������� ������' => false
		);
		$firstOrder = $this->FirstOrder();
        $firstOrder = false;
		//������������� ���� ��
		foreach ($this->abits as $abit) {
			if($firstOrder) {
				if(!empty($abit->privilege)) {
					if($abit->status == '��������') {
						$this->foundations['������ �����'][] = $abit;
					} else {
						$this->foundations['��������� ������'][] = $abit;
					}
				} else {
					$this->foundations['������ �����'][] = $abit;
				}
				
			} else {
				if($abit->category == '��� ������������� ���������'){
					$this->foundations['��� ������������� ���������'][] = $abit;
				}
				if($abit->category == '������� ������ �����'){
					if ($abit->receptionFeatures == '��������� �����') {
						$this->foundations['����. �����'][] = $abit;
					}
					else {
						$this->foundations['������ �����'][] = $abit;
					}
				}
				if($abit->category == '�� ����� ����������'){
					$this->foundations[$abit->foundation][] = $abit;
				}

			}
		}

	}

	/**
	 * 	�������� 1 ������ �� ���������� �� � ��
	 * @return bool true - y, false - n
	 */
	protected function FirstOrder() 
	{
		$Orders = $this->Client->getOrders(array('year' => 2023));

		foreach ($Orders->Orders as $key => $value) {
			if(H::u2w($value->Type) == '���������� � ��� � ������ �� � ��') {
				return true;
			}
		}
		return false;	
	}

	/**
	 * ���������� ������������
	 */
	protected function sortAbits() 
	{
		usort($this->abits, array('\modules\Enrollee\core\CompareObjects', 'abitsSort'));
	}

	/**
	 * ���������� ������������ ��� ���������� ���������� ������
	 */
	protected function sortEmptyAbits() 
	{
		foreach ($this->abits as $key => $abit) 
		{
			if ($abit->totalScore == 0) 
			{
				$this->abits_temp[] =$abit ;
				unset($this->abits[$key]);
			}
		}
		usort($this->abits_temp, function($a, $b) 
		{
		   return $a->name > $b->name;
		});
		//���� �� ������� ������� �������� ��������, �� ����
		foreach ($this->abits_temp as $key => $abit) 
		{
			$this->abits[]=$abit;
		}
	}

}
<?php
namespace modules\Enrollee\core;

use \engine\core1C\Helper1C as H;
/**
 * 	��������� ������������ ��� ���
 */
class AbitCollectionSPO
{
	protected $abits;
	protected $abits_temp;
	protected $data;
	protected $foundations;
	protected $Client;
	protected $params;

	public function __construct(RequestParams $Params, \engine\core1C\Client1C $Client, Speciality $Speciality = null)
	{
		$this->data = $Client->getAbitBySpecialityFaculty(array(
				'idPk' => $Params->idPk,
				'specialityCode' => $Params->specialityCode,
				'form' => $Params->specialityForm,
				'education' => $Params->education)
		);

		$this->Client = $Client;
		$this->params = $Params;
		$this->speciality = $Speciality;
	}

	public function getAbits()
	{
		if(empty($this->speciality)) {
			$SpecialityInfo = $this->Client->getSpecialityInfo(array(
					'idPk' => $this->params->idPk,
					'specialityCode'=> $this->params->specialityCode,
					'form' => $this->params->specialityForm)
			);
			$this->speciality = new Speciality($SpecialityInfo);
		}
		foreach ($this->data->val as $value) {
			if (empty($this->abits[$value->peopleId]))
				$this->abits[$value->peopleId.$value->Foundations] = new Abit($value, $this->speciality);
			else
				$this->abits[$value->peopleId.$value->Foundations]->originalEducationDocuments = true;
		}
		$this->sortAbits();
		$this->assignmentByFoundation();
		return $this->foundations;
	}


	/**
	 * ������������� �������� abit\Abit
	 * �� �������� ��������� �����������
	 * @return array array(
	 *  'foundation1'=> array(abit\Abit),
	 * 	.....,
	 * 	'foundationN'=> array(abit\Abit)
	 * )
	 */
	protected function assignmentByFoundation()
	{
		$this->foundations = array(
			'��� ������������� ���������' => false,
			'������ �����' => false,
			'������� �����' => false,
			'��������� ������' => false,
			'������ ���������� ������' => false
		);
		$firstOrder = $this->FirstOrder();
		//������������� ���� ��
		foreach ($this->abits as $abit) {
			if(!empty($abit->privilege)) {
				if($firstOrder) {
					if($abit->status == '��������') {
						$this->foundations['������ �����'][] = $abit;
					} else {
						$this->foundations['��������� ������'][] = $abit;
					}
				} else {
					$this->foundations['������ �����'][] = $abit;
				}

			} else {
				if($abit->category == '��� ������������� ���������') {
					$this->foundations['��� ������������� ���������'][] = $abit;
				} else {
					$this->foundations[$abit->foundation][] = $abit;
				}

			}
		}

	}

	/**
	 * 	�������� 1 ������ �� ���������� �� � ��
	 * @return bool true - y, false - n
	 */
	protected function FirstOrder()
	{
		$Orders = $this->Client->getOrders(array('year' => 2020));
		foreach ($Orders->Orders as $key => $value) {
			if(H::u2w($value->Type) == '���������� � ��� � ������ �� � ��') {
				return true;
			}
		}
		return false;
	}

	/**
	 * ���������� ������������
	 */
	protected function sortAbits()
	{
		usort($this->abits, array('\modules\Enrollee\core\CompareObjects', 'abitsSortSPO'));
	}

}
<?php
namespace modules\Enrollee\core;

class AbitFilter 
{
	protected $abits;
	protected $speciality;
	protected $result;

	private function __construct($Abits) 
	{
		$this->abits = $Abits->getAbits();
		$this->speciality = $Abits->speciality;
	}


	public static function init(AbitCollection $Abits) 
	{
		return new self($Abits);
	}

	/*���
	������(������ �������������) ��������� �� $this->abits
	������*/


	/**
	 * ��� �������� ������� �������� �� �������� (������ ������)
	 * @return array abit\Abit
	 */
	public function all()
	{
		return $this;
	}

	/**
	 * �������� ��������� (��������� � ��������)
	 * @return $this
	 */
	public function active()
	{
		foreach ($this->abits as $budgetType => $Abits) {
			foreach($Abits as $i => $Abit) {
				if(($Abit->status != '������')
				&& ($Abit->status != '��������')) {
					unset($this->abits[$budgetType][$i]);
				}
			}
		}
		return $this;
	}

	/**
	 * ���� ���������� ���������� �������
	 * @return $this
	 */
	public function ifToday()
	{
		$this->active();
		$this->addBudgetPlaces(true);
		$abitsInPrikaz = array();
		foreach($this->abits['��� ��������'] as $i => $Abit) {
			if($Abit->isEnrolled()) {
				$Abit->styleClass = 'greenLine';
                $abitsInPrikaz[$Abit->peopleId] = $Abit->peopleId;
				continue;
			}
			if($Abit->originalEducationDocuments)
			{
				$Abit->styleClass = 'yellowLine';
				$abitsInPrikaz[$Abit->peopleId] = $Abit->peopleId;
				$this->speciality->budgetPlaces--;
			}
		}

		if(isset($this->abits['����. �����'])){

			foreach($this->abits['����. �����'] as $i => $Abit){
				if($Abit->isEnrolled()){
					$Abit->styleClass = 'greenLine';
                    $abitsInPrikaz[$Abit->peopleId] = $Abit->peopleId;
					continue;
				}
				if($Abit->originalEducationDocuments)
				{
					if (!isset($abitsInPrikaz[$Abit->peopleId])){
						$Abit->styleClass = 'yellowLine';
						$abitsInPrikaz[$Abit->peopleId] = $Abit->peopleId;
						$this->speciality->budgetPlaces--;
					} else {
						$Abit->alreadyInPrikaz = true;
					}
				}
			}
			usort($this->abits['����. �����'], array('\modules\Enrollee\core\CompareObjects', 'abitsIfTodaySort'));
		}

		if(isset($this->abits['������ �����'])){
            $j = 0;
			foreach($this->abits['������ �����'] as $i => $Abit){
				if($Abit->isEnrolled()){
					$Abit->styleClass = 'greenLine';
                    $abitsInPrikaz[$Abit->peopleId] = $Abit->peopleId;
					continue;
				}

				if($j < $this->speciality->places['��������� ������'] && $Abit->fulfilledConditions()){
					if (!isset($abitsInPrikaz[$Abit->peopleId])){
						$Abit->styleClass = 'yellowLine';
						$abitsInPrikaz[$Abit->peopleId] = $Abit->peopleId;
						$this->speciality->budgetPlaces--;
						$j++;
					} else {
						$Abit->alreadyInPrikaz = true;
					}
				}
			}
			usort($this->abits['������ �����'], array('\modules\Enrollee\core\CompareObjects', 'abitsIfTodaySort'));
		}

		if(isset($this->abits['������� �����'])){
            $j = 0;
			foreach($this->abits['������� �����'] as $i => $Abit){
				if($Abit->isEnrolled()){
					$Abit->styleClass = 'greenLine';
                    $abitsInPrikaz[$Abit->peopleId] = $Abit->peopleId;
					continue;
				}

				if($j < $this->speciality->places['������� �����'] && $Abit->fulfilledConditions()){
					if (!isset($abitsInPrikaz[$Abit->peopleId])){
						$Abit->styleClass = 'yellowLine';
						$abitsInPrikaz[$Abit->peopleId] = $Abit->peopleId;
						$this->speciality->budgetPlaces--;
						$j++;
					} else {
						$Abit->alreadyInPrikaz = true;
					}
				}
			}
			usort($this->abits['������� �����'], array('\modules\Enrollee\core\CompareObjects', 'abitsIfTodaySort'));
		}

		if(isset($this->abits['��������� ������'])){
            $j = 0;
			foreach($this->abits['��������� ������'] as $i => $Abit){
				if($Abit->isEnrolled()){
					$Abit->styleClass = 'greenLine';
                    $abitsInPrikaz[$Abit->peopleId] = $Abit->peopleId;
					continue;
				}
				if($j < $this->speciality->budgetPlaces && $Abit->fulfilledConditions()){
					if (!isset($abitsInPrikaz[$Abit->peopleId])){
						$Abit->styleClass = 'yellowLine';
						$abitsInPrikaz[$Abit->peopleId] = $Abit->peopleId;
                        $j++;
					} else {
						$Abit->alreadyInPrikaz = true;
					}
				}
			}
			usort($this->abits['��������� ������'], array('\modules\Enrollee\core\CompareObjects', 'abitsIfTodaySort'));
		}

		if(isset($this->abits['������ ���������� ������'])){
            $j = 0;
			foreach($this->abits['������ ���������� ������'] as $i => $Abit){
				if($Abit->isEnrolled()){
					$Abit->styleClass = 'greenLine';
                    $abitsInPrikaz[$Abit->peopleId] = $Abit->peopleId;
					continue;
				}

				if($j < $this->speciality->places['������ ���������� ������'] && $Abit->fulfilledConditionsCommerce()){
					if (!isset($abitsInPrikaz[$Abit->peopleId])){
						$Abit->styleClass = 'yellowLine';
						$abitsInPrikaz[$Abit->peopleId] = $Abit->peopleId;
                        $j++;
					} else {
						$Abit->alreadyInPrikaz = true;
					}
				}
			}
			usort($this->abits['������ ���������� ������'], array('\modules\Enrollee\core\CompareObjects', 'abitsIfTodaySortCommerce'));
		}

		//\CF::Debug($this->speciality->places);
		/*
		foreach ($this->speciality->places as $budgetType => $places)
		{
			if($budgetType == '������ ���������� ������'){
				usort($this->abits[$budgetType], array('\modules\Enrollee\core\CompareObjects', 'abitsIfTodaySortCommerce'));
			}else{
				usort($this->abits[$budgetType], array('\modules\Enrollee\core\CompareObjects', 'abitsIfTodaySort'));
			}

//			$cntIsEnrolled = 0;
			foreach($this->abits[$budgetType] as $i => $Abit)
			{
				//���� ���������� ��������
				if($Abit->isEnrolled()) {
//					$cntIsEnrolled++;
					$Abit->styleClass = 'greenLine';
					continue;
				}

//				if($budgetType == '��������� ������')
//				{
				    //��� $firstWawe � �������� � ������� ����� �������
                    //
                    //���������� ����������� ����������� ��������������� ��� $firstWawe
                    //��� ������� ������� ������ � 1� � ���������� ������� ������� �� ���������� ������������
                    //
//					$firstWawe = ceil($places * 1);
//					if($i<$firstWawe && $Abit->fulfilledConditions())
//					if($i<$firstWawe)
//					{
//						if($i<$places && $Abit->fulfilledConditions() && ($Abit->getCountPassExams() ==3)){
//							$Abit->styleClass = 'yellowLine';
//						}
//						$Abit->styleClass = 'yellowLine';
//					}
//					elseif($i<$places && $Abit->fulfilledConditions() && ($Abit->getCountPassExams() ==3))
//					elseif($i<$places)
//					{
//						if ($this->speciality->formEducation=="�������")
//						{
//							$Abit->styleClass = 'yellowLine';
//						}
//						else
//						{
//							$Abit->styleClass = 'blueLine';
//						}
//					}
//				}

//				if($budgetType == '������� �����'){
//					if($i<$places && $Abit->fulfilledConditions() && ($Abit->getCountPassExams() == 3)){
//						$Abit->styleClass = 'yellowLine';
//					}
//				}

				if ($budgetType == "������ ���������� ������")
				{
					if($i<($places) && $Abit->fulfilledConditionsCommerce() && ($Abit->getCountPassExams() ==3)){
						$Abit->styleClass = 'yellowLine';
					}
				}elseif($budgetType == "��������� ������"){
					if($i<($this->speciality->budgetPlaces) && $Abit->fulfilledConditions() && ($Abit->getCountPassExams() == 3)){
						$Abit->styleClass = 'yellowLine';
					}
				}

			}
		} */
		return $this;
	}

	/**
	 * �������� � ��������� ��� ���� �������
	 * @return $this
	 */
	public function allBudgetTypes()
	{
		$this->result = $this->abits;
		return $this;
	}

	/**
	 * �������� � ��������� �����������
	 * @return $this
	 */
	public function isBudget(){
		$this->abits['��� ��������'] ? $this->result['��� ��������'] = $this->abits['��� ��������'] : null;
		$this->abits['����. �����'] ? $this->result['����. �����'] = $this->abits['����. �����'] : null;
		$this->abits['������ �����'] ? $this->result['������ �����'] = $this->abits['������ �����'] : null;
		$this->abits['������� �����'] ? $this->result['������� �����'] = $this->abits['������� �����'] : null;
		$this->abits['��������� ������'] ? $this->result['��������� ������'] = $this->abits['��������� ������'] : null;
		return $this;
	}

	/**
	 * �������� � ��������� ���������
	 * @return $this
	 */
	public function isCommerce()
	{
		$this->abits['������ ���������� ������'] ? $this->result['������ ���������� ������'] = $this->abits['������ ���������� ������'] : null;
		return $this;
	}

	/**
	 * �������� � ��������� ��� ��������
	 * @return $this
	 */
	public function withoutContest()
	{
		$this->abits['��� ��������'] ? $this->result['��� ��������'] = $this->abits['��� ��������'] : null;
		return $this;
	}

	/**
	 * ������� ������ ���� ������� �� ������������
	 * @return $this
	 */
	public function isMagistracy()
	{
		$this->deleteNotIncludedInSpecialityBudgets();
		return $this;
	}

	/**
	 * ������� ������ ���� ������� �� �����������
	 * @return $this
	 */
	public function isGraduate()
	{
		$this->deleteNotIncludedInSpecialityBudgets();
		return $this;
	}

	/**
	 * ����������� ���������
	 * @return $this
	 */
	public function isBachelor()
	{
		return $this;
	}

	/**
	 * 	������� ���� ������� �� �������  �� �������
	 *  	����� �� �������������
	 */
	private function deleteNotIncludedInSpecialityBudgets() {
		foreach ($this->result as $type => $value) {
			if(empty($this->speciality->places[$type]))
				unset($this->result[$type]);
		}	
	}

	/**
	 * �������� ���������� ��������
	 * abit\Abit
	 * ��������� ���������� �
	 * �������� �� ����������
	 */
//    protected function ifTodaySort($a, $b)
//    {
//
//    	if($a->originalEducationDocuments == $b->originalEducationDocuments) {
//    		if($a->agreement == $b->agreement) {
//		    	return AbitCollection::compareAbits($a, $b);
//    		}
//    		return ($a->agreement < $b->agreement) ? +1 : -1;
//    	} elseif($a->agreement == $b->agreement) {
//    		if($a->originalEducationDocuments != $b->originalEducationDocuments) {
//    			return ($a->originalEducationDocuments < $b->originalEducationDocuments) ? +1 : -1;
//    		}
//	    	return AbitCollection::compareAbits($a, $b);
//    	}
//    }

	private function addBudgetPlaces($prikaz = false){
		if($prikaz){
			$cntIsEnrolled = 0;
			foreach ($this->abits as $abits){
				if(!empty($abits)){
					foreach($abits as $i => $Abit){
						//���� ���������� ��������
						if($Abit->isEnrolled()){
							$cntIsEnrolled++;
						}
					}
				}
			}

			$this->speciality->budgetPlaces = $this->speciality->budget - $cntIsEnrolled;
			//������� ���� �����
			//if($this->speciality->specialityCode == '35.03.10'){
			//	$this->speciality->budgetPlaces--;
			//}
		}else {
			if(isset($this->speciality->places['��������� ������'])){
				//����� �� �������
				$this->speciality->budgetPlaces = $this->speciality->budget - (($this->speciality->places['��������� ������'] * 2) + $this->speciality->places['������� �����']);
			}
		}
	}

	/**
	 * ������ ������������
	 * @return array abit\Abit
	 */
	public function getAbits() 
	{
		return $this->result;
	}

}
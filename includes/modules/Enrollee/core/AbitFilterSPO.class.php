<?php
namespace modules\Enrollee\core;

class AbitFilterSPO
{
	protected $abits;
	protected $speciality;
	protected $result;

	private function __construct($Abits)
	{
		$this->abits = $Abits->getAbits();
		$this->speciality = $Abits->speciality;
	}


	public static function init(AbitCollectionSPO $Abits)
	{
		return new self($Abits);
	}

	/*���
	������(������ �������������) ��������� �� $this->abits
	������*/


	/**
	 * ��� �������� ������� �������� �� �������� (������ ������)
	 * @return array abit\Abit
	 */
	public function all()
	{
		return $this;
	}

	/**
	 * �������� ��������� (��������� � ��������)
	 * @return $this
	 */
	public function active()
	{
		foreach ($this->abits as $budgetType => $Abits) {
			foreach($Abits as $i => $Abit) {
				if(($Abit->status != '������')
					&& ($Abit->status != '��������')) {
					unset($this->abits[$budgetType][$i]);
				}
			}
		}
		return $this;
	}

	/**
	 * ���� ���������� ���������� �������
	 * @return $this
	 */
	public function ifToday()
	{
		$this->active();

		foreach($this->abits['��� ��������'] as $i => $Abit) {
			if($Abit->isEnrolled()) {$Abit->styleClass = 'greenLine'; continue;}
			if($Abit->fulfilledConditions())
			{
				$Abit->styleClass = 'yellowLine';
				$this->speciality->places['��������� ������']--;
			}
		}

		foreach ($this->speciality->places as $budgetType => $places)
		{
			usort($this->abits[$budgetType], array('\modules\Enrollee\core\CompareObjects', 'abitsIfTodaySortSPO'));
			foreach($this->abits[$budgetType] as $i => $Abit)
			{
				if($Abit->isEnrolled()) {$Abit->styleClass = 'greenLine'; continue;}


				if($budgetType == '��������� ������')
				{
					//$firstWawe = ceil($places * 0.8);

					if($i<$firstWawe && $Abit->fulfilledConditions())
					{
						$Abit->styleClass = 'yellowLine';
					} elseif($i<$places && $Abit->fulfilledConditions())
					{
						if ($this->speciality->formEducation=="�������")
						{
							$Abit->styleClass = 'yellowLine';
						}
						else
						{
							$Abit->styleClass = 'blueLine';
						}
					}
				} else
					if($i<$places && $Abit->fulfilledConditions())
					{
						$Abit->styleClass = 'yellowLine';
					}

				// if ($Abit->foundation == "������ ���������� ������") 
				// {
				// 	//$Abit->styleClass = 'greenLine';
				// }

			}
		}
		return $this;
	}



	/*���
	�������(����� �������������)
	��������� � $this->result ������*/


	/**
	 * �������� � ��������� ��� ���� �������
	 * @return $this
	 */
	public function allBudgetTypes()
	{
		$this->result = $this->abits;
		return $this;
	}

	/**
	 * �������� � ��������� �����������
	 * @return $this
	 */
	public function isBudget(){
		$this->abits['��� ��������'] ? $this->result['��� ��������'] = $this->abits['��� ��������'] : null;
		$this->abits['������ �����'] ? $this->result['������ �����'] = $this->abits['������ �����'] : null;
		$this->abits['������� �����'] ? $this->result['������� �����'] = $this->abits['������� �����'] : null;
		$this->abits['��������� ������'] ? $this->result['��������� ������'] = $this->abits['��������� ������'] : null;
		return $this;
	}

	/**
	 * �������� � ��������� ���������
	 * @return $this
	 */
	public function isCommerce()
	{
		$this->abits['������ ���������� ������'] ? $this->result['������ ���������� ������'] = $this->abits['������ ���������� ������'] : null;
		return $this;
	}

	/**
	 * �������� � ��������� ��� ��������
	 * @return $this
	 */
	public function withoutContest()
	{
		$this->abits['��� ��������'] ? $this->result['��� ��������'] = $this->abits['��� ��������'] : null;
		return $this;
	}

	/**
	 * ������� ������ ���� ������� �� ������������
	 * @return $this
	 */
	public function isMagistracy()
	{
		$this->deleteNotIncludedInSpecialityBudgets();
		return $this;
	}

	/**
	 * ������� ������ ���� ������� �� �����������
	 * @return $this
	 */
	public function isGraduate()
	{
		$this->deleteNotIncludedInSpecialityBudgets();
		return $this;
	}

	/**
	 * ����������� ���������
	 * @return $this
	 */
	public function isBachelor()
	{
		return $this;
	}

	/**
	 * 	������� ���� ������� �� �������  �� �������
	 *  	����� �� �������������
	 */
	private function deleteNotIncludedInSpecialityBudgets() {
		foreach ($this->result as $type => $value) {
			if(empty($this->speciality->places[$type]))
				unset($this->result[$type]);
		}
	}

	/**
	 * �������� ���������� ��������
	 * abit\Abit
	 * ��������� ���������� �
	 * �������� �� ����������
	 */
	protected function ifTodaySort($a, $b)
	{
		if($a->originalEducationDocuments == $b->originalEducationDocuments) {
			if($a->agreement == $b->agreement) {
				return AbitCollection::compareAbits($a, $b);
			}
			return ($a->agreement < $b->agreement) ? +1 : -1;
		} elseif($a->agreement == $b->agreement) {
			if($a->originalEducationDocuments != $b->originalEducationDocuments) {
				return ($a->originalEducationDocuments < $b->originalEducationDocuments) ? +1 : -1;
			}
			return AbitCollection::compareAbits($a, $b);
		}
	}



	/**
	 * ������ ������������
	 * @return array abit\Abit
	 */
	public function getAbits()
	{
		return $this->result;
	}

}
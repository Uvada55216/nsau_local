<?php
namespace modules\Enrollee\core;
/**
 * 	Все заявления одного абитуриента
 */
class AbitRequests extends AbitCollection
{
	public function __construct(RequestParams $Params, \engine\core1C\Client1C $Client) 
	{
		
		$this->data = $Client->getAbitBySpeciality(array(
			'idPk' => $Params->idPk, 
			'peopleId' => $Params->peopleId)
		);
		$this->params = $Params;
		$this->Client = $Client;
	}
	public function getAbits() 
	{
		foreach ($this->data->val as $value) {
			$abit = new Abit($value, null, $this->params);

			$key = md5($abit->peopleId .':'.$abit->category .':'. $abit->foundation );

			$this->abits[$key] = $abit;
		}
		$this->sortAbits();
		$this->assignmentByFoundation();
		return $this->foundations;
	}

	/**
	 * Сортировка абитуриентов
	 */
	protected function sortAbits() 
	{
		usort($this->abits, array('\modules\Enrollee\core\CompareObjects', 'abitSort'));
	}
}
<?php

namespace modules\Enrollee\core;

class CompareObjects
{

	/**
	 * �������� ���������� �������� ���/ ������� ������������ ������������
	 * abit\Abit
	 */
	public static function abitsSortSPO($a, $b){
		$statusPosition = array('��������' => 1, '������' => 2, '����� �� ��������' => 3);
		if ($statusPosition[$a->status] == $statusPosition[$b->status]) {
			if ($b->summary == $a->summary) {
				return 0;
			}
			return ($b->summary < $a->summary) ? -1 : 1;
		}
		return ($statusPosition[$a->status] > $statusPosition[$b->status]) ? +1 : -1;
	}

	/**
	 * �������� ���������� �������� ���/ ������������ ���
	 * ������� "���� �� ���������� ���� �������"
	 * abit\Abit
	 */
	public function abitsIfTodaySortSPO($a, $b)
	{
		if(!$a->originalEducationDocuments || !$b->originalEducationDocuments) {
			if($a->status == '��������') {
				return  -1;
			} if ($b->status == '��������') {
				return  +1;
			}
		}

		if($a->originalEducationDocuments == $b->originalEducationDocuments) {
			return self::abitsSortSPO($a, $b);

//			if ($b->certificateScore == $a->certificateScore) {
//				return 0;
//			}
//			return ($b->certificateScore < $a->certificateScore) ? -1 : 1;
		}elseif($a->originalEducationDocuments != $b->originalEducationDocuments) {
			return ($a->originalEducationDocuments < $b->originalEducationDocuments) ? +1 : -1;
		}
	}

	/**
	 * �������� ���������� ��������/ ������� ������������ ������������
	 * abit\Abit
	 */
	public static function abitsSort($a, $b)
	{

		if($a->failScore) {
			return  +1;
		} if ($b->failScore) {
			return  -1;
		}

		$statusPosition = array('��������' => 1, '������' => 2, '����� �� ��������' => 3);
        if ($statusPosition[$a->status] == $statusPosition[$b->status]) {
    	    if($a->totalScore == $b->totalScore) {
    	        if ($a->examScore == $b->examScore) {
                    return self::priorityExamSort($a, $b);
                }
    	        return ($a->examScore < $b->examScore) ? +1 : -1;
        	} 
        	return ($a->totalScore < $b->totalScore) ? +1 : -1;
        }            	
    	return ($statusPosition[$a->status] > $statusPosition[$b->status]) ? +1 : -1;
	}

	/**
	 * ���������� �� ������������� ��������
	 */
	public static function priorityExamSort($a, $b)
	{
	    if ($a->priorityForExams[1] == $b->priorityForExams[1]) {
            if ($a->priorityForExams[2] == $b->priorityForExams[2]) {
                return ($a->priorityForExams[3] > $b->priorityForExams[3])  ? -1 : +1;
            }
            return ($a->priorityForExams[2] > $b->priorityForExams[2])  ? -1 : +1;
        }
        return ($a->priorityForExams[1] > $b->priorityForExams[1])  ? -1 : +1;


	    /*
		$exam_a = $a->exam ? $a->exam : $a->ege;
		$exam_b = $b->exam ? $b->exam : $b->ege;

		$exam = array_search(1, $a->speciality->exams);

		if($exam_a[$exam] == $exam_b[$exam]){
			$exam2= array_search(2, $a->speciality->exams);

			if($exam_a[$exam2] == $exam_b[$exam2]){
				$exam3= array_search(3, $a->speciality->exams);
				return ($exam_a[$exam3] > $exam_b[$exam3])  ? -1 : +1;
			}

			return ($exam_a[$exam2] > $exam_b[$exam2])  ? -1 : +1;
		}
		return ($exam_a[$exam] > $exam_b[$exam])  ? -1 : +1;
		*/
	}

	/**
	 * �������� ���������� ��������
	 * abit\Abit
	 * ���������� �� ����������
	 * ������������ ��� ���������� �������� ���������
	 * �� �������� �����������
	 */
	public static function abitSort($a, $b)
	{
        if ($a->priority == $b->priority) {
            return 0;
        }
        return ($a->priority > $b->priority) ? +1 : -1;
	}

	/**
	 * �������� ���������� ��������/ ������������ ���
	 * ������� "���� �� ���������� ���� �������"
	 * abit\Abit
	 */
    public function abitsIfTodaySort($a, $b)
    {
    	if(!$a->agreement || !$b->agreement) {
	      	if($a->status == '��������') {
				return  -1;
			} if ($b->status == '��������') {
				return  +1;
			}
    	}
        //���� �� ���� �������
//    	if($a->failScore) {
//			return  +1;
//		} if ($b->failScore) {
//			return  -1;
//		}
		// $statusPosition = array('��������' => 1, '������' => 2, '����� �� ��������' => 3);
		// return ($statusPosition[$a->status] > $statusPosition[$b->status]) ? +1 : -1;


	    /*
		 * ����� "�������� �� �����������" ���� �������� �� ����������
		 *
		 * */
//	    if($a->agreement || $b->agreement){
//		    if($a->agreement == $b->agreement){
//			    return self::abitsSort($a, $b);
//		    } elseif($a->agreement != $b->agreement) {
//			    return ($a->agreement < $b->agreement) ? +1 : -1;
//		    }
//	    }else {
		    /*
			 * ���������� �� ���� "�������� �� �����������" ���� �� �������� �� ����������
			 *
			 * */
		    if($a->originalEducationDocuments == $b->originalEducationDocuments
			    && ($a->getCountPassExams() == 3)  == ($b->getCountPassExams() == 3)
		        && $a->highPriority == $b->highPriority
		        && $a->alreadyInPrikaz == $b->alreadyInPrikaz){
			    return self::abitsSort($a, $b);
		    }
		    if($a->originalEducationDocuments != $b->originalEducationDocuments){
			    return ($a->originalEducationDocuments < $b->originalEducationDocuments) ? +1 : -1;
		    } elseif ($a->highPriority != $b->highPriority) {
			    return ($a->highPriority < $b->highPriority) ? +1 : -1;
		    } elseif (($a->getCountPassExams() == 3) != ($b->getCountPassExams() == 3)) {
			    return (($a->getCountPassExams() == 3) < ($b->getCountPassExams() == 3)) ? +1 : -1;
		    } elseif ($a->alreadyInPrikaz != $b->alreadyInPrikaz) {
		    	return ($a->alreadyInPrikaz < $b->alreadyInPrikaz) ? -1 : +1;
		    }
//	    }
    }

	/**
	 * �������� ���������� ��������/ ������������ ���
	 * ������� "���� �� ���������� ���� �������" ��� ��������
	 * abit\Abit
	 */
	public function abitsIfTodaySortCommerce($a, $b)
	{
		if(!$a->agreement || !$b->agreement) {
			if($a->status == '��������') {
				return  -1;
			} if ($b->status == '��������') {
				return  +1;
			}
		}
		/*
		 * ���������� �������� �� ����������
		 *
		 * */


        if($a->originalEducationDocuments == $b->originalEducationDocuments
            && $a->paid == $b->paid
            && $a->alreadyInPrikaz == $b->alreadyInPrikaz){
            return self::abitsSort($a, $b);
        }
        if($a->originalEducationDocuments != $b->originalEducationDocuments){
            return ($a->originalEducationDocuments < $b->originalEducationDocuments) ? +1 : -1;
        } elseif ($a->paid != $b->paid) {
            return ($a->paid < $b->paid) ? +1 : -1;
        } elseif ($a->alreadyInPrikaz != $b->alreadyInPrikaz) {
            return ($a->alreadyInPrikaz < $b->alreadyInPrikaz) ? -1 : +1;
        }

	}
}

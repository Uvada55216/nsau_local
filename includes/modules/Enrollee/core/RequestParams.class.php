<?php
namespace modules\Enrollee\core;

use \engine\core1C\Helper1C as H;
/**
 * Параметры передаваемые в AbitCollection и SpecialityCollection
 * Магия может понадобится при дальнейшей валидации параметров
 * в данный момент не используется
 */
class RequestParams
{
    /**
     * Ид приемной компании. Узнать можно в 1 с.
     * Или смотреть документацию к abit\AbitClient1c
     * @var int
     */
    private $idPk;
    /**
     * Код физического лица в 1С
     * @var int
     */
    private $peopleId;
    /**
     * Код специальности
     * @var string
     */
    private $specialityCode;
    /**
     * Форма обучения
     * Очная
     * Заочная
     * ОчноЗаочная
     * @var string
     */
    private $specialityForm;
    /**
     * Уровень подготовки
     * Бакалавриат
     * Специалитет
     * Магистратура
     * Аспирантура
     * @var string
     */
    private $educationLevel;
    /**
     * Среднее образование
     * 9 классов
     * 11 классов
     * @var string
     */
    private $education;

    public function __construct() {
        $this->education = '';
        return $this;
    }

    public function __get($name) {
        if (!empty($this->{$name}) || $name == 'education') {
            if ($name == 'specialityForm') {
                return H::w2u($this->{$name});
            }
            return $this->{$name};
        } else {
            throw new \Exception("Param $name is empty");
        }
    }

    public function setParam($param, $value) {
        $this->{$param} = $value;
        return $this;
    }
}

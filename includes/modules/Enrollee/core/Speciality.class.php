<?php
namespace modules\Enrollee\core;

use \engine\core1C\Helper1C as H;

class Speciality
{
	public $facultyName; 
	public $specialityName;
	public $specialityCode;
	public $formEducation;
	public $educationLevel;
	public $places;
	public $exams;
	public $examsMinScore;
	public $countPlaces;
	public $countRequests;
	public $entryRate;
	public $commerce = 0;
	public $budget = 0;
	public $budgetFoundation = 0;
	public $targetFoundation = 0;
	public $commerceOriginals = 0;
	public $commerceCopy = 0;
	public $budgetOriginals = 0;
	public $budgetCopy = 0;
	public $programEducational;
	public $foundations;

	public function __construct($specialityData) 
	{

		$this->facultyName = H::u2w($specialityData->FacultyName);
		$this->specialityName = H::u2w($specialityData->Name);
		$this->specialityCode = H::u2w($specialityData->Code);
		$this->formEducation = H::u2w($specialityData->Form);
		$this->educationLevel = H::u2w($specialityData->EducationLevel);
		$this->programEducational = H::u2w($specialityData->ProgramEducational);
		$this->foundations = H::u2w($specialityData->Foundations);
		$this->countRequests = (int)$specialityData->CountRequests;
		$this->commerceOriginals = $specialityData->CommerceOriginal ? $specialityData->CommerceOriginal : 0;
		$this->commerceCopy = $specialityData->CommerceCopy ? $specialityData->CommerceCopy : 0;
		$this->budgetOriginals = $specialityData->BudjetOriginal ? $specialityData->BudjetOriginal : 0;
		$this->budgetCopy = $specialityData->BudjetCopy ? $specialityData->BudjetCopy : 0;
		foreach ($specialityData->Places as $place) {
			/*Хуйня из-за того что в 1с в основании приема
			(для особых прав)
			не основание приема а рандомное дерьмо*/
			$ebanoe1c = array_reverse(explode('_', $place->ConcursGroup));
			$analBudgetType = $ebanoe1c[1];
            if($place->Foundations == 'Полное возмещение затрат') {
            	$this->commerce = $place->Places;
            } else {           	
            	if($analBudgetType == 'особое право') {
                    $place->Foundations = 'Особое право';
            	}
                if($analBudgetType == 'отдельная квота') {
                    $place->Foundations = 'Отдельная квота';
                }
	            if($place->Foundations == 'Целевой прием') {
		            $this->targetFoundation = $place->Places;
	            }
	            if($place->Foundations == 'Бюджетная основа') {
		            $this->budgetFoundation += $place->Places;
	            }
            	$this->budget += $place->Places;
            	//if($analBudgetType == 'ОП') {
            	//	$place->Foundations = 'Особое право';
            	//}
            }
            $this->places[H::u2w($place->Foundations)] = $place->Places;
            $this->countPlaces += $place->Places;
        }
        foreach ($specialityData->Exams as $exam) {
            $this->exams[H::u2w($exam->Subject)] = $exam->Priority;
        }
        asort($this->exams);
        foreach ($specialityData->Exams as $exam) {
            $this->examsMinScore[H::u2w($exam->Subject)] = $exam->Min;
        }
        $this->entryRate = round(($this->countRequests / $this->budget), 2);
	}

	/**
	 * Добавить место
	 * @param string $placeName Основание
	 * @param int $places    Кол-во мест
	 */
	public function addPlace($placeName, $places = null) {
		$this->places[$placeName] = $places;
	}

	/**
	 * Удалить место
	 * @param string $placeName Основание
	 */
	public function deletePlace($placeName) {
		unset($this->places[$placeName]);
	}	

}
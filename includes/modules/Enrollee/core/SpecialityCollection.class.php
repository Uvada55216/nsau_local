<?php
namespace modules\Enrollee\core;
use \engine\core1C\Helper1C as H;

class SpecialityCollection
{

	protected $specialities;
	protected $faculties;
	protected $data;

	private $Client;
	private $Params;

	public function __construct(RequestParams $Params, \engine\core1C\Client1C $Client)
	{
        $this->data = $Client->getSpecialities(array(
                'idPK'=>$Params->idPk,
                'form' => $Params->specialityForm)
        );

		$this->Client = $Client;
		$this->Params = $Params;
	}

	public function getSpecialities()
	{
		foreach ($this->data->Speciality as $value) {
			$this->specialities[] = new Speciality($value);
		}
		$this->sortSpecialities();
		$this->assignmentByFaculty();
		return $this->faculties;
	}

    /**
     * Группировка объектов abit\Speciality
     * по программе образования и наименованию специальности
     * @return array
     */
    protected function assignmentByFacultyAndEducational()
    {
        foreach($this->faculties as $spName => $sp){
            $list = array();
            $this->faculties[$spName]['info']['budget'] = '';
            $this->faculties[$spName]['info']['commerce'] = '';
            foreach($sp['specialities'] as $s) {
                $hash = md5($s->programEducational . ':' . $s->specialityName. ':' .$s->facultyName);
                if (!isset($list[$hash])) {
                    $list[$hash] = $s;
                    $this->faculties[$spName]['info']['budget'] += $s->budget;
                    $this->faculties[$spName]['info']['commerce'] += $s->commerce;
                    continue;
                }

                $el = $list[$hash];
                $el->entryRate += $s->entryRate;
                $el->countRequests += $s->countRequests;

                $list[$hash] = $el; // ??
            }
            $this->faculties[$spName]['specialities'] = $list;
        }
    }

    /**
	 * Распределение объектов abit\Speciality
	 * по массивам факультетов
	 * @return array array(
	 *  'faculty1'=> array(abit\Speciality),
	 * 	.....,
	 * 	'facultyN'=> array(abit\Speciality)
	 * )
	 */
	protected function assignmentByFaculty()
	{
		foreach($this->specialities as $spec) {
			$this->faculties[$spec->facultyName]['specialities'][] = $spec;
			$this->faculties[$spec->facultyName]['info']['budget'] += $spec->budget;
			$this->faculties[$spec->facultyName]['info']['budgetFoundation'] += $spec->budgetFoundation;
			$this->faculties[$spec->facultyName]['info']['targetFoundation'] += $spec->targetFoundation;
			$this->faculties[$spec->facultyName]['info']['budgetOriginals'] += $spec->budgetOriginals;
			$this->faculties[$spec->facultyName]['info']['budgetCopy'] += $spec->budgetCopy;
			$this->faculties[$spec->facultyName]['info']['commerceOriginals'] += $spec->commerceOriginals;
			$this->faculties[$spec->facultyName]['info']['commerceCopy'] += $spec->commerceCopy;
			$this->faculties[$spec->facultyName]['info']['commerce'] += $spec->commerce;
			$this->faculties[$spec->facultyName]['info']['countRequests'] += $spec->countRequests;
			$this->faculties[$spec->facultyName]['info']['entryRate'] = round(
				$this->faculties[$spec->facultyName]['info']['countRequests']/
				($this->faculties[$spec->facultyName]['info']['budget']
				+$this->faculties[$spec->facultyName]['info']['commerce']), 2
			);
		}
	}

	/**
	 * Сортировка специальностей
	 */
	protected function sortSpecialities()
	{
		usort($this->specialities, array($this, 'cmp_obj'));
	}

	/**
	 * Алгоритм сортировки
	 * 1 факультет
	 * 2 наименование специальности
	 */
    protected function cmp_obj($a, $b)
    {
        if ($a->facultyName == $b->facultyName) {
            return ($a->specialityName > $b->specialityName) ? +1 : -1;
        }
        return ($a->facultyName > $b->facultyName) ? +1 : -1;
    }

    /**
     * Сортировка специальностей по образовательной программе
     */
    protected function sortProgramEducational()
    {
        foreach ($this->faculties as $facultyName => &$faculty) {
            usort($faculty["specialities"], array($this, 'cmp_educational'));
        }
    }

    /**
     * Алгоритм сортировки
     * 1 факультет
     * 2 образовательная программа
     */
    protected function cmp_educational($a, $b)
    {
        if ($a->specialityName == $b->specialityName) {
            return ($a->programEducational > $b->programEducational) ? +1 : -1;
        }
        return ($a->specialityName > $b->specialityName) ? +1 : -1;
    }

	/**
	 * Сортировка специальностей
	 */
	protected function sortSpecialityCode()
	{
		usort($this->specialities, array($this, 'cmp_code'));
	}

	/**
	 * Алгоритм сортировки
	 * 1 факультет
	 * 2 код специальности
	 */
	protected function cmp_code($a, $b)
	{
		if ($a->facultyName == $b->facultyName) {
			return ($a->specialityCode > $b->specialityCode) ? +1 : -1;
		}
		return ($a->facultyName > $b->facultyName) ? +1 : -1;
	}

}
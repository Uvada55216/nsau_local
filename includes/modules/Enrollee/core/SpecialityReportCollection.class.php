<?php
namespace modules\Enrollee\core;

class SpecialityReportCollection extends SpecialityCollection
{
	private $Client;
	private $Params;

	public function __construct(RequestParams $Params, \engine\core1C\Client1C $Client) 
	{
		$this->data = $Client->getReportSpecialities(array(
				  'idPK'=>$Params->idPk, 
				  'form' => $Params->specialityForm)
		);
		$this->Client = $Client;
		$this->Params = $Params;
	}

	public function getSpecialities()
	{
		foreach ($this->data->Speciality as $value) {
			$this->specialities[] = new Speciality($value);
		}
		$this->sortSpecialityCode();
		$this->assignmentByFaculty();
		return $this->faculties;
	}
}
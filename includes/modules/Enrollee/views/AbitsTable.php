<link rel="stylesheet" href="/themes/styles/abits.css" type="text/css"/>
<?=$this->renderBlock('blocks/AbitsSpecialitiesList', $moduleData);?>

<?php

$Speciality = $moduleData['speciality'];

//��������
$exams = array();
foreach ($moduleData['speciality']->exams as $key => $value){
	if($key === '�������' || $key === '����������� � ���' || $key === '����������'){
		$k = substr($key, 0, 3);
		$exams[$k] = $key;
	} else {
		$arrWord = explode(' ', $key);
		if(count($arrWord) == 2){
			$k = ucfirst($arrWord[0][0]) . $arrWord[1][0];
			$exams[$k] = $key;
		} else {
			$k = $key[0];
			$exams[$k] = $key;
		}
	}
}

if (!empty($Speciality->specialityName)) { ?>
    <h1><?=$Speciality->facultyName?></h1>
    <h2><?=($Speciality->specialityCode[0] == 't')?substr($Speciality->specialityCode, 1):$Speciality->specialityCode?> - <?=$Speciality->specialityName?></h2>
	<?php
	if ($this->Module->initParams->levelEducation === 'Bachelor') {
		if ($Speciality->budget != 0) { ?>
            <h3>���������� ���� ��� - <?=$Speciality->budget?> <br> �� ���:</h3>
            <ul>
				<?php foreach ($Speciality->places as $type => $places) {
					if ($type == '������ ���������� ������')
						continue;
                    if ($type == '��������� ������')
                        continue;?>

                    <li><?=$type?> - <?=$places?></li>
				<?php } ?>
            </ul>
		<?php }
		if ($Speciality->commerce != 0) { ?>
            <h3>������ ���������� ������ - <?=$Speciality->commerce?> </h3><br>
		<?php }
	} else { ?>
        <h3>���������� ����:</h3>
        <ul>
			<?php foreach ($Speciality->places as $type => $places) { ?>
                <li><?=$type?> - <?=$places?></li>
			<?php } ?>
        </ul>
	<?php } ?>
    <h3>��������:</h3>
    <ul>
		<?php foreach ($exams as $key => $value) { ?>
            <li><?=$key?> - <?=$value?></li>
		<?php } ?>
    </ul>
	<?php if ($moduleData['post']['listType']) { ?>
        <div class="enroll greenLine"></div> - ��������
        <div class="enroll yellowLine"></div> - � ����������
		<?php /*    <div class="enroll yellowLine"></div> - � ���������� � 1-�� �����
      <div class="enroll blueLine"></div> - � ���������� �� 2-�� ����� **/?>
	<?php } ?>


	<?php if (!empty($moduleData['abits'])) { ?>
		<?=$this->renderBlock('blocks/' . $moduleData['tableType'], $moduleData);?>
	<?php } else { ?>
        <h2>�� ������ ����������� ��� �������� ���������</h2>
	<?php } ?>
<?php } else { ?>
    <h2>����������� ���������� �� �������</h2>
<?php } ?>               

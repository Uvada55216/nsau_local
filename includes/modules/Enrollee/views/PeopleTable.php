<link rel="stylesheet" href="/themes/styles/abits.css" type="text/css" />	
<?if(!empty($moduleData )) {?>
	<?foreach ($moduleData as $foundation => $abits) {?>
		<?if(!empty($abits)) {
			$people = array_shift($abits);
			$info = $people->snils != '' ? $people->snils : $people->delo;
			?>
            <h1><?=(!$moduleData['prikaz']) ? "{$people->name}": "{$info}"?></h1>
			<?break;}?>
	<?}
	unset($moduleData['prikaz']);
	?>
	
	<h2>�������� ���������</h2>			
	<table class="table-bordered" width="100%">
		<thead>
			<tr>
				<th width="50%">�����������</th>
				<th width="10%">����� ��������</th>
				<th width="10%">���������</th>
				<th width="10%">����</th>
				<th width="10%">������</th>
				<th width="10%">�������� �� �����������</th>
			</tr>
		</thead>				
		<tbody>
			<?foreach ($moduleData as $foundation => $abits) {?>
				<?if(!empty($abits)) {?>
					<tr class="separator">
						<td colspan="6"><strong><?=$foundation?></strong></td>
					</tr>
					<?foreach ($abits as $abit) {?>
						<tr>
							<td>
								<a href="<?=$this->engineUri?>speciality/<?=$abit->specialityCode?>/<?=$abit->form == '�����' ? 'ft' : ($abit->form == '�������' ? 'ex' : 'ftex')?>">
									<?=$abit->specialityCode?> - <?=$abit->specialityName?>
								</a>
							</td>
							<td><?=$abit->form?></td>
							<td><?=$abit->priority?></td>
							<td><?=$abit->totalScore?></td>
							<td><?=$abit->status?></td>
							<td><?=($abit->originalEducationDocuments) ? "��������" : "�����"?></td>
						</tr>
					<?}?>
				<?}?>
			<?}?>																
		</tbody>
	</table>
<?} else {?>
	<h2>���������� �� ������</h2>
<?}?>
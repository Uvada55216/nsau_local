<script>
    jQuery(document).ready(function($) {
        $('#faculty').live('change', function(){
            $('#speciality > option').css('display', 'none');
            $('#speciality > option[data-faculty="'+$(this).val()+'"]').css('display', 'block');
        });
        $('#goToSpeciality').live('click', function() {
            var spec = $('#speciality > option:selected').val();
            $('#selectSpeciality').attr('action', '<?=$this->engineUri?>speciality/'+spec);
            if(!spec) {
                return false;
            }
        });
    });
</script>
<div class="panel" style="width: 500px">
    <div class="panel-heading">
        <h3 class="panel-title">��������� ������ ������������</h3>
    </div>
    <div class="panel-body">
        <form id="selectSpeciality" method="POST">
            <div class="input-group">
					 	<span class="input-group-addon without-bottom-border nsau-abits-form">
					  		<span>��� ������:</span>
						</span>
                <select name="listType" class="form-control without-bottom-border">
                    <option <?=$moduleData['post']['listType'] == 'active' ? 'selected' : ''?> value="active">��������� � ��������</option>
                    <option <?=$moduleData['post']['listType'] == 'ifToday' ? 'selected' : ''?> value="ifToday">���� �� ���������� ���������� �������</option>
                </select>
            </div>
            <div class="input-group">
					 	<span class="input-group-addon without-bottom-border nsau-abits-form">
					  		<span>��� �������:</span>
						</span>
                <select name="budgetType" class="form-control without-bottom-border">
                    <option <?=$moduleData['post']['budgetType'] == 'isBudget' ? 'selected' : ''?> value="isBudget">������</option>
                    <option <?=$moduleData['post']['budgetType'] == 'withoutContest' ? 'selected' : ''?> value="withoutContest">��� ��������</option>
                    <option <?=$moduleData['post']['budgetType'] == 'isCommerce' ? 'selected' : ''?> value="isCommerce">������� �����������</option>
                </select>
            </div>
            <?php if($this->Module->initParams->levelEducation === 'SPO') { ?>
                <div class="input-group">
					 	<span class="input-group-addon without-bottom-border nsau-abits-form">
					  		<span>�����������:</span>
						</span>
                    <select name="education" class="form-control without-bottom-border">
                        <option <?=$moduleData['post']['education'] == '09' ? 'selected' : ''?> value="09">9 �������</option>
                        <option <?=$moduleData['post']['education'] == '11' ? 'selected' : ''?> value="11">11 �������</option>
                    </select>
                </div>
            <? } ?>
            <div class="input-group">
					 	<span class="input-group-addon without-bottom-border nsau-abits-form">
					  		<span>���������:</span>
						</span>
                <select id="faculty" name='faculty' class="form-control without-bottom-border">
                    <option></option>
                    <?foreach($moduleData['specialities'] as $facultyName => $val) {?>
                        <option <?=$moduleData['post']['faculty'] == $facultyName ? 'selected' : ''?> value="<?=$facultyName?>"><?=$facultyName?></option>
                    <?}?>
                </select>
            </div>
            <div class="input-group">
					 	<span class="input-group-addon nsau-abits-form">
					  		<span>�����������:</span>
						</span>
						<select id="speciality" name='speciality' class="form-control">
							<option></option>
							<?foreach($moduleData['specialities'] as $facultyName => $val) {?>
                                <? $codes = array(); ?>
								<?foreach($val['specialities'] as $speciality) {?>
                                    <? if (!in_array($speciality->specialityCode, $codes)) { ?>
                                        <? $codes[] = $speciality->specialityCode;?>
                                    <?} else {?>
                                        <? continue; ?>
                                    <?}?>
									<option
										style="display: <?=$moduleData['post']['faculty'] == $facultyName ? 'block' : 'none'?> "
										value="<?=$speciality->specialityCode?>"
										data-faculty="<?=$speciality->facultyName?>"
										 >
										<?=($speciality->specialityCode[0] == 't')?substr($speciality->specialityCode, 1):$speciality->specialityCode?> - <?=$speciality->specialityName?>
									</option>
								<?}?>
							<?}?>
						</select>
					</div>
					<div class="row">
						<div class="col-sm-12" style="padding-top: 0px; text-align: right"><button type="submit" class="btn btn-danger" id="goToSpeciality">��������</button></div>
					</div>
				</form>
			  </div>
			</div>

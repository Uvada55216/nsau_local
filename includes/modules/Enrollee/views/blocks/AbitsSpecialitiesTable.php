<h1>���������� � ���������� �������� ��������� <br>� ������� ����������� (��������������) �� <?=date("d.m.Y")?> �.</h1>
<h2><?=$moduleData['formEducation']?> ����� ��������</h2>
<table class="table-bordered" width="100%">
	<thead>
		<tr ><!-- style="word-break: break-all;" -->
			<th rowspan="2">���</th>
			<th rowspan="2">����������� (�������������)</th>
            <?php if($this->Module->initParams->levelEducation === 'SPO') { ?>
			    <th rowspan="2">��������������� ���������</th>
            <?php } ?>
			<th colspan="2">���-�� ��������� <br>����</th>
			<th rowspan="2">���-�� ������������ <br>����</th>
			<th rowspan="2">���-�� �������� <br>���������</th>
			<th rowspan="2">�������</th>
		</tr>
        <tr>
            <th>��������� ������</th>
            <th>������� �����</th>
        </tr>
	</thead>

	<tbody>
		<?foreach ($moduleData['specialities'] as $facultyName => $faculty) {?>
			<tr class="separator">
				<td colspan="2"><strong><?=$facultyName?></strong></td>
                <?php if($this->Module->initParams->levelEducation === 'SPO') { ?>
                    <td></td>
                <?php } ?>
				<td><strong><?=$faculty['info']['budgetFoundation']?></strong></td>
                <td><strong><?=$faculty['info']['targetFoundation']?></strong></td>
				<td><strong><?=$faculty['info']['commerce']?></strong></td>
				<td><strong><?=$faculty['info']['countRequests']?></strong></td>
				<td><strong><?=round($faculty['info']['countRequests'] / ($faculty['info']['budget']), 2)?></strong></td>
			</tr>
			<?php foreach($faculty["specialities"] as $Spec) { ?>
				<tr>
					<!-- <td>&nbsp<?="<a href='speciality/{$Spec->specialityCode}'>$Spec->specialityCode</a>"?>&nbsp</td> -->
                    <td>&nbsp<?=($Spec->specialityCode[0] == 't')?substr($Spec->specialityCode, 1):$Spec->specialityCode?>&nbsp</td>
					<!-- <td style="text-align: left"><?="<a href='speciality/{$Spec->specialityCode}'>$Spec->specialityName</a>"?></td> -->
					<td style="text-align: left"><?=$Spec->specialityName?></td>
                    <?php if($this->Module->initParams->levelEducation === 'SPO') { ?>
                        <td><?=$Spec->programEducational == '������� �����'? '�� ���� 11 ��.': ($Spec->programEducational == '�������� �����'? '�� ���� 9 ��.': "")?></td>
                    <?php } ?>
                    <td><?=$Spec->budgetFoundation > 0 ? $Spec->budgetFoundation: '-'?></td>
                    <td><?=$Spec->targetFoundation > 0 ? $Spec->targetFoundation: '-'?></td>
					<td><?=$Spec->commerce > 0 ? $Spec->commerce: '-'?></td>
					<!-- <td><?=$Spec->countRequests>0 ? "<a href='speciality/{$Spec->specialityCode}'>$Spec->countRequests</a>" : "0"?></td> -->
					<td><?=$Spec->countRequests > 0 ? $Spec->countRequests: '-' ?></td>
					<td><?=$Spec->entryRate > 0 ? $Spec->entryRate: '-'?></td>
				</tr>
			<? } ?>
		<? } ?>
	</tbody>
</table>
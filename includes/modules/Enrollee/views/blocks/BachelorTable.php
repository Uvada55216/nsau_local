<?php
    $colspan = count($moduleData['speciality']->exams) + 11;
    //��������
    $exams = array();
    foreach ($moduleData['speciality']->exams as $key => $value){
        if($key === '�������' || $key === '����������� � ���' || $key === '����������'){
            $k = substr($key, 0, 3);
            $exams[$k] = $key;
        } else {
            $arrWord = explode(' ', $key);
            if(count($arrWord) == 2){
                $k = ucfirst($arrWord[0][0]) . $arrWord[1][0];
                $exams[$k] = $key;
            } else {
                $k = $key[0];
                $exams[$k] = $key;
            }
        }
    }
    //�������� �� ��� �������
    //������� ���� ����� ���� �� ������� �����������
    $specPravoShow = (count($moduleData['abits']) > 1)? true: false;
?>
<div style="overflow-x:auto;">
    <table class="table-bordered" width="100%">
        <thead>
            <tr>
                <th width="2%" rowspan="2">�</th>
                <th width="15%" rowspan="2">����� / <br>� ������� ����</th>
                <?php foreach (array_count_values(array_values($moduleData['speciality']->exams)) as $key => $value) { ?>
                    <th width="<?=$value * 6?>%" colspan="<?=$value?>">��. <?=$key?></th>
                <?php } ?>
                <th width="5%" rowspan="2">�����<br>������<br>�� ��</th>
                <th width="5%" rowspan="2">���. �����-<br>�����</th>
                <th width="5%" rowspan="2">�����<br>������</th>
                <th width="5%" rowspan="2">������� �����. �����</th>
                <th width="5%" rowspan="2">�������� �� �����������</th>
                <th width="5%" rowspan="2">���������</th>
                <th width="5%" rowspan="2">��� ������ ���������</th>
                <th width="18%" rowspan="2">��������</th>
                <th width="5%" rowspan="2">���-� � ����������</th>
            </tr>
            <tr>
            <?php foreach ($exams as $key => $value) { ?>
                <th width="6%?>" title="<?=$value?>"><?=$key?></th>
            <?php } ?>
            </tr>
        </thead>

        <tbody>

        <?php//���������� ����� �� ������ ��?>
        <?php if($moduleData['speciality']->formEducation == '�����') { ?>
            <?php if($moduleData['speciality']->specialityCode == '19.03.03') { ?>
                <tr class="separator">
                    <td colspan="<?=$colspan?>"><strong>�� ��������������������� ����������</strong></td>
                </tr>
                <tr class="">
                    <td colspan="1">1</td>
                    <td colspan="1">000072788</td>
                    <td colspan="1">-</td>
                    <td colspan="1">-</td>
                    <td colspan="1">-</td>
                    <td colspan="1">-</td>
                    <td colspan="1">-</td>
                    <td colspan="1">-</td>
                    <td colspan="1">-</td>
                    <td colspan="1">-</td>
                    <td colspan="1">-</td>
                    <td colspan="1">-</td>
                    <td colspan="1">-</td>
                    <td colspan="1">-</td>
                    <td colspan="1">-</td>
                    <td colspan="1">-</td>
                    <td colspan="1">-</td>
                    <td colspan="1">-</td>
                    <td colspan="1">-</td>
                    <td colspan="1">-</td>
                    <td colspan="1">-</td>
                    <td><strong>��������</strong></td>
                    <td colspan="1">-</td>
                    <td colspan="1">-</td>
                    <td colspan="1">-</td>
                    <td>��������</td>
                </tr>
                <?++$j;?>
            <?php } ?>
            <?php if($moduleData['speciality']->specialityCode == '40.03.01') { ?>
                <<tr class="separator">
                    <td colspan="<?=$colspan?>"><strong>�� ��������������������� ����������</strong></td>
                </tr>
                <tr class="">
                    <td colspan="1">1</td>
                    <td colspan="1">000072789</td>
                    <td colspan="1">-</td>
                    <td colspan="1">-</td>
                    <td colspan="1">-</td>
                    <td colspan="1">-</td>
                    <td colspan="1">-</td>
                    <td colspan="1">-</td>
                    <td colspan="1">-</td>
                    <td colspan="1">-</td>
                    <td colspan="1">-</td>
                    <td colspan="1">-</td>
                    <td colspan="1">-</td>
                    <td colspan="1">-</td>
                    <td colspan="1">-</td>
                    <td colspan="1">-</td>
                    <td colspan="1">-</td>
                    <td colspan="1">-</td>
                    <td colspan="1">-</td>
                    <td><strong>��������</strong></td>
                    <td colspan="1">-</td>
                    <td colspan="1">-</td>
                    <td colspan="1">-</td>
                    <td>��������</td>
                </tr>
                <?++$j;?>

            <?php } ?>

        <?php } ?>
        <?//END?>


            <?php foreach($moduleData['abits'] as $foundation => $abits) { ?>
                <tr class="separator">
                    <td colspan="<?=$colspan?>"><strong><?=$foundation?></strong></td>
                </tr>
                <?php if(empty($abits)) { ?>
                    <tr>
                        <td colspan="<?=$colspan?>"><p style="text-align: center;">��� �������� ���������</p></td>
                    </tr>
                <?php } ?>
                <?php foreach($abits as $key => $Abit) {
                    //
                    //������ �� ���������
                    //
                    if((isset($Abit->receptionFeatures) && $Abit->receptionFeatures == '����� �����') || 1){
                        $info = $Abit->snils != '' ? $Abit->snils : $Abit->delo; ?>
                        <tr class="<?=$Abit->styleClass?>">
                            <td><?=++$j?></td>
                            <?$snilsAndName = isset($moduleData['post'])?"{$info}":"{$info} ({$Abit->name})"?>
                            <?if ($foundation == '����. �����') {?>
                                <td style="text-align: left"><?=$Abit->peopleId?></td>
                            <?} else {?>
                                <td style="text-align: left"><a href="<?=$this->engineUri?>people<?= (isset($moduleData['formEducation']) && $moduleData['formEducation'] != '') ? '/'.$moduleData['formEducation'] : ''  ?>/<?=$Abit->peopleId?>"><?=(!$moduleData['prikaz']) ? $snilsAndName: $info?></a></td>
                            <? } ?>
                            <?php
                                if(!empty($Abit->ege)){
                                    foreach($Abit->ege as $k => $v){
                                        $Abit->exams[$k] = $v . '(�)';
                                    }
                                }
                                if(!empty($Abit->exam)){
                                    foreach($Abit->exam as $k => $v){
                                        $Abit->exams[$k] = $v . '(�)';
                                    }
                                }
                                foreach($moduleData['speciality']->exams as $key => $value){ ?>
                                    <?php if(!empty($Abit->exams[$key])){ ?>
                                        <th width="6%"><?=$Abit->exams[$key]?></th>
                                    <?php } else { ?>
                                        <td width="6%">-</td>
                                    <?php }
                            } ?>
                            <td><?=(!empty($Abit->examScore)) ? $Abit->examScore : '0'?></td>
                            <td><?=$Abit->achievementsScore?></td>
                            <td><?=$Abit->totalScore?></td>
                            <td><?=($Abit->preferred) ? '��': "���"?></td>
                            <td><?=($Abit->originalEducationDocuments) ? "<strong>��������</strong>" : "�����"?></td>
                            <td><?=$Abit->priority?></td>
                            <td><?=($Abit->highPriority) ? "<strong>��</strong>" : "���"?></td>
                            <td><?=$Abit->organization?></td>
                            <td><?=(($Abit->styleClass == 'yellowLine') || ($Abit->paid)) ? '������� � ������' : $Abit->status?></td>
                        </tr>
                    <?php }
                } ?>
            <?php } ?>
        </tbody>
    </table>
</div>
<div class="" style="font-size: 12px; margin-top: 5px"><b>(�)</b> - ���, <b>(�)</b> - �������</div>

<?php
$colspan = count($moduleData['speciality']->exams);
//��������
$exams = array();
foreach ($moduleData['speciality']->exams as $key => $value){
    if($key === '�������' || $key === '����������� � ���' || $key === '����������'){
        $k = substr($key, 0, 3);
        $exams[$k] = $key;
    } else {
        $arrWord = explode(' ', $key);
        if(count($arrWord) == 2){
            $k = ucfirst($arrWord[0][0]) . $arrWord[1][0];
            $exams[$k] = $key;
        } else {
            $k = $key[0];
            $exams[$k] = $key;
        }
    }
}
?>
			<table class="table-bordered" width="100%">
				<thead>
					<tr>
                        <?$N_exams = count($moduleData['speciality']->exams);?>
						<th width="2%" rowspan="2">�</th>
                        <th width="15%" rowspan="2">����� / <br>� ������� ����</th>
						<th width="20%" colspan="<?=$N_exams?>">����� �� <br>��������� ����</th>
						<th width="5%" rowspan="2">�������������� ����������</th>
						<th width="5%" rowspan="2">����� ������</th>
						<th width="5%" rowspan="2">�������� �� �����������</th>
						<th width="5%" rowspan="2">�������� �� ����������</th>
						<th width="5%" rowspan="2">���-� � ����������</th>
					</tr>
					<tr>
						<?php foreach($exams as $key => $value) { ?>
                            <th width="5%?>" title="<?=$value?>"><?=$key?></th>
						<?php } ?>
					</tr> 
				</thead>

                <?php if($moduleData['speciality']->specialityCode == '38.06.01') { ?>
                    <tr class="separator">
                        <td colspan="14"><strong>����� ������������� ��</strong></td>
                    </tr>
                    <tr class="">
                        <td class="">1</td>
                        <td >000072356</td>
                        <td width="6%">-</td>
                        <td width="6%">-</td>
                        <td width="6%">-</td>
                        <td width="6%">-</td>
                        <td width="6%">-</td>
                        <td class=""><strong>��������</strong></td>
                        <td class="">���</td>
                        <td class="1">��������</td>
                    </tr>
                    <?++$j;?>
                <?php } ?>

                <?php if($moduleData['speciality']->specialityCode == '36.06.01') { ?>
                    <tr class="separator">
                        <td colspan="14"><strong>����� ������������� ��</strong></td>
                    </tr>
                    <tr class="">
                        <td class="">1</td>
                        <td >000072564</td>
                        <td width="6%">-</td>
                        <td width="6%">-</td>
                        <td width="6%">-</td>
                        <td width="6%">-</td>
                        <td width="6%">-</td>
                        <td width="6%">-</td>
                        <td class=""><strong>��������</strong></td>
                        <td class="">���</td>
                        <td class="1">��������</td>
                    </tr>
                    <?++$j;?>
                <?php } ?>

				<tbody>
					<?php foreach($moduleData['abits'] as $foundation => $abits) { ?>
						<tr class="separator">
							<td colspan="14"><strong><?=$foundation?></strong></td>
						</tr>
						<?php if(empty($abits)) { ?>
							<tr>
								<td colspan="14"><p style="text-align: center; padding-bottom: 0px">��� �������� ���������</p></td>
							</tr>
						<?php } ?>
						<?php foreach($abits as $key => $Abit) {
							$info = $Abit->snils != '' ? $Abit->snils : $Abit->delo; ?>
							<tr class="<?=$Abit->styleClass?>">
								<td><?=++$j?></td>
								<?$snilsAndName = isset($moduleData['post'])?"{$info}":"{$info} ({$Abit->name})"?>
                                <td style="text-align: left"><a href="<?=$this->engineUri?>people/<?=$Abit->peopleId?>"><?=(!$moduleData['prikaz']) ? $snilsAndName: $info?></a></td>
								<?php foreach($moduleData['speciality']->exams as $key => $value) { ?>
									<?php if(!empty($Abit->exam[$key])) { ?>
										<th width="5%"><?=$Abit->exam[$key]?></th>
									<?php } else { ?>
										<td>-</td>
									<?php } ?>
								<?php } ?>
								<td><?=$Abit->achievementsScore?></td>
								<td><?=$Abit->totalScore?></td>
								<td><?=($Abit->originalEducationDocuments) ? "<strong>��������</strong>" : "�����"?></td>
								<td><?=($Abit->agreement) ? "��" : "���"?></td>
								<td><?=$Abit->status?></td>
							</tr>
						<?php } ?>
					<?php } ?>
				</tbody>
			</table>
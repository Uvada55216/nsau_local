<table class="table-bordered" width="100%">
    <thead>
    <tr>
	    <? $keysOfType = array_keys($moduleData['abits']); ?>
	    <? $keysOfExams = array_keys($moduleData['abits'][$keysOfType[0]][0]->speciality->exams); ?>
        <th width="2%" rowspan="2">�</th>
        <th width="15%" rowspan="2">����� / <br>� ������� ����</th>
	    <?if ($keysOfExams[0] != "���") { ?>
            <th width="5%" rowspan="2">�����</th>
            <th width="5%" rowspan="2">������� ���� ���������</th>
            <th width="5%" rowspan="2">�������</th>
        <?} else {?>
            <th width="5%" rowspan="2">������� ���� ���������</th>
        <?}?>
        <th width="5%" rowspan="2">�������� �� �����������</th>
        <th width="5%" rowspan="2">�������� �� ����������</th>
        <th width="10%" rowspan="2">���-� � ����������</th>
    </tr>
    </thead>

    <tbody>
    <?php foreach($moduleData['abits'] as $foundation => $abits) { ?>
        <tr class="separator">
            <td colspan="14"><strong><?=$foundation?></strong></td>
        </tr>
        <?php if(empty($abits)) { ?>
            <tr>
                <td colspan="14"><p style="text-align: center; padding-bottom: 0px">��� �������� ���������</p></td>
            </tr>
        <?php
        } else {
            foreach ($abits as $key => $Abit) {
	            $info = $Abit->snils != '' ? $Abit->snils : $Abit->delo; ?>
                <tr class="<?=$Abit->styleClass ?>">
                    <td><?= ++$j ?></td>
	                <?$snilsAndName = "{$info} ({$Abit->name})"?>
                    <td style="text-align: left"><a href="<?=$this->engineUri?>people/<?=$Abit->peopleId?>"><?=(!$moduleData['prikaz']) ? $snilsAndName: $info?></a></td>
                    <?if ($keysOfExams[0] != "���") { ?>
                        <td><?=$Abit->summary ?></td>
                        <td><?=$Abit->certificateScore ?></td>
                        <td><?=isset($Abit->exam[$keysOfExams[0]]) ? $Abit->exam[$keysOfExams[0]] : "-"?></td>
                    <?} else {?>
                        <td><?=$Abit->certificateScore ?></td>
                    <?}?>
                    <td><?=($Abit->originalEducationDocuments) ? "<strong>��������</strong>" : "�����" ?></td>
                    <td><?=($Abit->agreement) ? "��" : "���"?></td>
                    <td><?=$Abit->status ?></td>
                </tr>
            <?php }

        } ?>
    <?php } ?>
    </tbody>
</table>
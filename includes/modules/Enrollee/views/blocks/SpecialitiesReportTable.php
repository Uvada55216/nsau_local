<table class="table-bordered" width="100%">
	<thead>
		<tr ><!-- style="word-break: break-all;" -->
			<th rowspan="2">���</th>
			<th rowspan="2">����������� (�������������)</th>
            <?=$this->Module->initParams->levelEducation === 'SPO'? '<th rowspan="2">��������������� ���������</th>': ''?>
			<th colspan="3">���-�� ��������� <br>����</th>
			<th colspan="3">���-�� ������������ <br>����</th>
			<th rowspan="2">���-�� �������� <br>���������</th>
			<th rowspan="2">�������</th>
			<th rowspan="2">��������� � ���������� ����</th>
		</tr>
		<tr>
			<th>���</th>
			<th>����������</th>
			<th>�����</th>
			<th>���</th>
			<th>����������</th>
			<th>�����</th>
		</tr>
	</thead>

	<tbody>
		<?$form = $moduleData['specialityForm']?>
		<?foreach ($moduleData['specialities'] as $facultyName => $faculty) {?>
			<tr class="separator">
				<td colspan="2"><strong><?=$facultyName?></strong></td>
                <?=$this->Module->initParams->levelEducation === 'SPO'? '<td></td>': ''?>
				<td><strong><?=$faculty['info']['budget']?></strong></td><?$total['budget'] += $faculty['info']['budget']?>
				<td><strong><?=$faculty['info']['budgetOriginals']?></strong></td><?$total['budgetOriginals'] += $faculty['info']['budgetOriginals']?>
				<td><strong><?=$faculty['info']['budgetCopy']?></strong></td><?$total['budgetCopy'] += $faculty['info']['budgetCopy']?>
				<td><strong><?=$faculty['info']['commerce']?></strong></td><?$total['commerce'] += $faculty['info']['commerce']?>
				<td><strong><?=$faculty['info']['commerceOriginals']?></strong></td><?$total['commerceOriginals'] += $faculty['info']['commerceOriginals']?>
				<td><strong><?=$faculty['info']['commerceCopy']?></strong></td><?$total['commerceCopy'] += $faculty['info']['commerceCopy']?>
				<td><strong><?=$faculty['info']['countRequests']?></strong></td><?$total['countRequests'] += $faculty['info']['countRequests']?>
				<td><strong><?=$faculty['info']['entryRate']?></strong></td><?$total['entryRate'] += $faculty['info']['entryRate']?>
				<td></td>
			</tr>
			<?php foreach($faculty["specialities"] as $Spec) {
			    if (isset($Spec->url)) {
			        $url = $Spec->url;
                } else {
                    $url = "speciality/{$form}/{$Spec->specialityCode}";
                } ?>
				<tr>
					<td>&nbsp<a href='<?=$url?>'><?=($Spec->specialityCode[0] == 't')?substr($Spec->specialityCode, 1):$Spec->specialityCode?></a>&nbsp</td>
					<td style="text-align: left"><a href='<?=$url?>'><?=$Spec->specialityName?></a></td>
                    <?php if($this->Module->initParams->levelEducation === 'SPO') { ?>
                        <td><?=$Spec->programEducational == '������� �����'? '�� ���� 11 ��.': ($Spec->programEducational == '�������� �����'? '�� ���� 9 ��.': "")?></td>
                    <?php } ?>
					<td><?=$Spec->budget?></td>
					<td><?=$Spec->budgetOriginals?></td>
					<td><?=$Spec->budgetCopy?></td>
					<td><?=$Spec->commerce?></td>
					<td><?=$Spec->commerceOriginals?></td>
					<td><?=$Spec->commerceCopy?></td>
					<td><?=$Spec->countRequests>0 ? "<a href='{$url}'>$Spec->countRequests</a>" : "0"?></td>
					<td><?=$Spec->entryRate?></td>
					<td></td>
				</tr>
			<?}?>
		<?}?>
		<tr class="separator">
			<td colspan="2"><strong>�����</strong></td>
            <?=$this->Module->initParams->levelEducation === 'SPO'? '<td></td>': ''?>
			<td><strong><?=$total['budget']?></strong></td>
			<td><strong><?=$total['budgetOriginals']?></strong></td>
			<td><strong><?=$total['budgetCopy']?></strong></td>
			<td><strong><?=$total['commerce']?></strong></td>
			<td><strong><?=$total['commerceOriginals']?></strong></td>
			<td><strong><?=$total['commerceCopy']?></strong></td>
			<td><strong><?=$total['countRequests']?></strong></td>
            <td><strong><?=round($total['countRequests']/($total['budget'] + $total['commerce']), 2)?></strong></td>
			<td></td>
		</tr>
	</tbody>
</table>
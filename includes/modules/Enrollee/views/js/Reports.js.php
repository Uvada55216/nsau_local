<script type='text/javascript'>
	jQuery(document).ready(function($) {

		var type = 'fullTime';
		var idPk = $('ul.idPk').attr('data-idpk');
		var request = {'route': '<?=$__route?>', 
					'action': 'actionSpecialitites', 
					'module': '<?=$__module?>', 
					'context': '<?=$__context?>', 
					'type': 'html', 
					'data': {'type': type, 'idPk': idPk} };
		$.ajax({
			type: 'POST',
			dataType: 'html',
			url: '/ajax_mode/',
			data: request,
			beforeSend: function() {
				$('div.content').html("<img class='tmp_loader' style='height: 20px;width: 20px' src='/themes/styles/images/animated_spinner.gif'>" + 
					"������������ ������... ��������� ����� �������� 1 ������.");
			},
			success: function(data) {
				$('div.content').html(data);
				$('div.content').append('<a href="specialities/' + type + '/download">������� ������� � ������� MS Word</a>');
			}
		});


		$(document).on('click', 'li[role=form]', function(e) {
			var type = $(this).find('a').attr('data-type');
			var idPk = $(this).closest('ul').attr('data-idpk');
			$('li.active').removeClass('active');
			$(this).addClass('active');
			var request = {'route': '<?=$__route?>', 
						'action': 'actionSpecialitites', 
						'module': '<?=$__module?>', 
						'context': '<?=$__context?>', 
						'type': 'html', 
						'data': {'type': type, 'idPk': idPk} };

			$.ajax({
				type: 'POST',
				dataType: 'html',
				url: '/ajax_mode/',
				data: request,
				beforeSend: function() {
					$('div.content').html("<img class='tmp_loader' style='height: 20px;width: 20px' src='/themes/styles/images/animated_spinner.gif'>" + 
						"������������ ������... ��������� ����� �������� 1 ������.");
				},
				success: function(data) {
					$('div.content').html(data);
					$('div.content').append('<a href="specialities/' + type + '/download">������� ������� � ������� MS Word</a>');
				}
			});

			return false;
		});
	});
</script>
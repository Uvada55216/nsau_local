<script type='text/javascript'>
	jQuery(document).ready(function($) {
		// $(document).on('click', '#searchPeople', function() {
		$(document).on('keyup', '#searchName', function() {
			var name = $('#searchName').val();
			if(name.length < 3) return false;
			var request = {'route': '<?=$__route?>', 
						'action': 'actionSearch', 
						'module': '<?=$__module?>', 
						'context': '<?=$__context?>', 
						'type': 'html', 
						'params': {name} };
			$.ajax({
				type: 'POST',
				dataType: 'html',
				url: '/ajax_mode/',
				data: request,
				beforeSend: function() {
					$('#searchResult').html("<img class='tmp_loader' style='height: 20px;width: 20px' src='/themes/styles/images/animated_spinner.gif'>" + 
						"���� �����...");
				},
				success: function(data) {
					$('#searchResult').html(data);
				}
			});
		});
	});
</script>
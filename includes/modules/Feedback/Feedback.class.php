<?php
/**
 * Модуль обращений граждан
 */
namespace modules\Feedback;

class Feedback extends \engine\modules\BaseModule
{
	public function rules() {
		return array(
            array('GET|POST', '/', $this->initParams->mode . 'Controller#actionIndex'),
            array('GET', 'download/[i:id]', 'ConfigController#actionDownload')
		);
	} 
}
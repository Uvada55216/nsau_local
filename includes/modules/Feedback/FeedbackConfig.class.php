<?php
namespace modules\Feedback;

use \engine\modules\configElements\Select;

class FeedbackConfig extends \engine\modules\BaseModuleConfig
{
	public $mode;
	private $__mode;

	public function __construct() 
	{
		$this->__mode = array
		(
			array('id' => 'feedback', 'name' => '��������� �������'),
			array('id' => 'config', 'name' => '��������� ��������� �������')
		);
	}

	public function params() 
	{
		return array( 
			'����� ������' => array(
				'element' => new Select('mode', $this->mode, $this->__mode),
				'obligatory' => 'true',
				'switcher' => 'true',
				'group' => 'mode',
			)							  
		);
	}

}
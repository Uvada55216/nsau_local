<?php
/**
 * ���������� �������� ��������� �������
 */
namespace modules\Feedback\controllers;

use engine\db\Db;
use modules\Feedback\models\ConfigModel;
use modules\Feedback\models\File;
use modules\Feedback\models\FileForm;
use \requests\Request;

class ConfigController extends \engine\controllers\BaseController
{
    /**
     * ������ ����� �������� ���������
     * @return string    [config view]
     */
    public function actionIndex()
    {
        global $Auth;
        $this->renderAjax('css/ConfigStyles'); //����������� ����� �� �������
        $this->renderJs('Config', ConfigModel::GetSubjects(), true);    //����������� js �����
        return $this->render('Config' . $this->Module->initParams->viewType, array('Auth' => $Auth));
    }

    /**
     * ����� ��������� ��� ���������
     * @return string    [��������� �������]
     */
    public function actionGetSubjects()
    {

//        \CF::Debug(ConfigModel::GetSubjects());
//        die;
        return $this->renderJson(ConfigModel::GetSubjects(), 'w2u');
    }

    /**
     * ����� ���������
     * @return string    [��������� ������� json]
     */
    public function actionGetMessages()
    {
        return $this->renderJson(ConfigModel::GetMessages(), 'w2u');
    }

    /**
     * ����� ����� � ��
     * @return string    [��������� ������]
     */
    public function actionSearchPeople()
    {
        return $this->renderJson(ConfigModel::SearchPeople(Request::init()->ajax()), 'w2u');
    }

    /**
     * ���������� �������� ��� ���������
     * @return string    [��������� ����������]
     */
    public function actionAddSubject()
    {
        return $this->renderJson(ConfigModel::AddSubject(Request::init()->ajax()));
    }

    /**
     * �������� �������� ��� ���������
     * @return string    [��������� ����������]
     */
    public function actionDeleteSubject()
    {
        return $this->renderJson((ConfigModel::DeleteSubject(Request::init()->ajax())));
    }

    public function actionDeleteFile()
    {
        return $this->renderJson((ConfigModel::DeleteFile(Request::init()->ajax())));
    }

    /**
     * �������� ���������
     * @return string    [��������� ����������]
     */
    public function actionDeleteMessage()
    {
        return $this->renderJson(ConfigModel::DeleteMessage(Request::init()->ajax()));
    }

    public function actionRejectMessage()
    {
        return $this->renderJson(ConfigModel::RejectMessage(Request::init()->ajax()));
    }

    public function actionAnswerMessage()
    {
        return $this->renderJson(ConfigModel::AnswerMessage(Request::init()->ajax()));
    }

    public function actionUploadFile()
    {
        $x['file'] = $_FILES[0];
        $FileForm = new FileForm();
        if ($FileForm->validate($x)) {
            $file = File::create($FileForm->getData());
        } else {
            return $this->renderJson(array('errors' => $FileForm->getErrors()), 'w2u');
        }
        if (!empty($file)) {
            $result[] = array('id' => $file['id'], 'name' => $file['name'], 'ext' => $file['ext']);
            return $this->renderJson($result);
        }
    }

    public function actionDownload($id)
    {
        $file = Db::init()->SetTable("nsau_free_files")
            ->AddField("name")
            ->AddField("ext")
            ->AddField("id")
            ->AddCondFS("id", "=", $id)
            ->SelectArray(1);
        $fname = $file["name"] . "." . $file["ext"];
        $filepath = FILES_DIR . 'free/' . $file['id'] . "." . $file["ext"];
        if (file_exists($filepath)) {
            $filesize = filesize($filepath);
            header('Content-Disposition: attachment; filename="' . $fname . '"');
            header('Content-type: application/octet-stream');
            header('Content-length: ' . $filesize);
            header("Accept-Ranges: bytes");

            $f = fopen($filepath, "r");
            while (!feof($f)) {
                echo fread($f, 1024);
                flush();
            }
            fclose($f);
        }
    }
}
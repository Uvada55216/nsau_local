<?php
/**
 * Контроллер формы обращения граждан
 */
namespace modules\Feedback\controllers;

use modules\Feedback\models\ConfigModel;
use modules\Feedback\models\FeedbackModel;
use modules\Feedback\models\File;
use modules\Feedback\models\FileForm;
use \requests\Request;

class FeedbackController extends \engine\controllers\BaseController
{
    public function actionIndex()
    {
        $this->renderAjax('css/FeedbackStyles'); //подключение файла со стилями
        $data = array(
            'subjects'=> $this->renderJson(FeedbackModel::GetSubjects(), 'w2u'),
            'categories'=> $this->renderJson(FeedbackModel::GetCategories(), 'w2u')
        );
        $this->renderJs('Feedback', $data, true);    //подключение js файла
        return $this->render('Feedback' . $this->Module->initParams->viewType);
    }

    /**
     * Отправка сообщения
     * @return [bool]    [результат операции]
     */
    public function actionSendMessage()
    {
//        \CF::Debug($_POST);die;
        return $this->renderJson(
            FeedbackModel::SendMessage(Request::init()->jsonAjax('u2w'))
        );
    }

    public function actionDeleteFile()
    {
        return $this->renderJson((ConfigModel::DeleteFile(Request::init()->ajax())));
    }

    public function actionUploadFile()
    {
        $x['file'] = $_FILES[0];
        $FileForm = new FileForm();
        if ($FileForm->validate($x)) {
            $file = File::create($FileForm->getData());
        } else {
            return $this->renderJson(array('errors' => $FileForm->getErrors()), 'w2u');
        }
        if (!empty($file)) {
            $result[] = array('id' => $file['id'], 'name' => $file['name'], 'ext' => $file['ext']);
            return $this->renderJson($result);
        }
    }
}
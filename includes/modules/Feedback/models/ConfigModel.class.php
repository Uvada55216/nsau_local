<?php
namespace modules\Feedback\models;
use \engine\db\Db;
use helpers\EncodingHelper;

class ConfigModel
{
    const MESSAGE_CONSIDERATION = 1;
    const MESSAGE_ANSWERED = 2;
    const MESSAGE_REJECTED = 3;



    /**
     * Выборка адресатов для обращений из БД
     * @return array    массив элементов
     */
    public static function GetSubjects()
    {
        $result = Db::init()
            ->SetTable('engine_modules_options', 'e')
            ->AddTable('auth_users', 'a')
            ->AddTable('nsau_people', 'p')
            ->addCondFs('e.module_id', '=', 57)
            ->addCondFs('e.option', '=', 'feedback_subject')
            ->AddCondFF('a.id', '=', 'e.value')
            ->AddCondFF('p.user_id', '=', 'e.value')
            ->AddField('e.id', 'id')
            ->AddField('a.displayed_name', 'displayed_name')
            ->AddField('p.photo', 'photo')
            ->SelectArray();
        if ($result) {
            return $result;
        }
        return array('false');
    }

    /**
     * Выборка обращений из БД
     * @return array    массив элементов
     */
    public static function GetMessages()
    {
        global $Engine, $Auth;
        $priv = $Engine->GetPrivileges(57, 'request.people.handle');
        $priveleges = $priv['57']['request.people.handle'];
        foreach($priveleges as $entry_id => $data) {
            if(($data['-1'] == 1 || $data[$Auth->usergroup_id] == 1))
            {
                if(!empty($data['user_id'])) {
                    $user_ids = explode(';', $data['user_id']);
                    if(in_array($Auth->user_id, $user_ids)) {
                        $users[] = $entry_id;
                    }
                } else {
                    $users[] = $entry_id;
                }

            }
        }
        $users[] = $Auth->user_id;
        $users = implode(',', $users);
//        \CF::Debug($users);
//        \CF::Debug($priveleges);die;
        $res =  Db::init()
            ->SetTable('nsau_feedback_citizens', 'f')
            ->AddTable('nsau_people', 'p')
            ->AddTable('auth_users', 'a')
            ->AddCondFF('a.id', '=', 'f.subject')
            ->AddCondFF('p.user_id', '=', 'f.subject')
            ->AddField('f.name', 'name')
            ->AddField('f.last_name', 'last_name')
            ->AddField('f.patronymic', 'patronymic')
            ->AddField('f.email', 'email')
            ->AddField('f.time', 'time')
            ->AddField('f.date', 'date')
            ->AddField('f.phone', 'phone')
            ->AddField('f.track', 'track')
            ->AddField('f.status', 'status')
            ->AddField('f.id', 'id')
            ->AddField('f.subject', 'subject')
            ->AddField('f.message', 'message')
            ->AddField('f.document_date', 'document_date')
            ->AddField('f.document_number', 'document_number')
            ->AddField('f.feedback_message', 'feedback_message')
            ->AddField('a.displayed_name', 'subject_displayed_name')
            ->AddField('p.id', 'subject_people_id')
            ->AddField('f.answer_file_id', 'answer_file_id')
            ->AddField('f.message_file_id', 'message_file_id')
            ->AddField('f.sub_authors', 'sub_authors')
            ->AddField('f.category_id', 'category_id');

        if($Auth->usergroup_id != 1)
            $res->AddCondFX('f.subject', 'IN', '(' . $users . ')');

          $result = $res->SelectArray();
        foreach ($result as $item) {
            $category = Db::init()
                ->SetTable('nsau_feedback_categories')
                ->AddCondFS('id', '=', $item['category_id'])
                ->SelectArray();
            $file = Db::init()
                ->SetTable('nsau_free_files')
                ->AddCondFS('id', '=', $item['answer_file_id'])
                ->SelectArray();
            $file_m = Db::init()
                ->SetTable('nsau_free_files')
                ->AddCondFS('id', '=', $item['message_file_id'])
                ->SelectArray();
            $item['answer_files'] = $file;
            $item['message_files'] = $file_m;
            $item['category'] = $category;

            $item['sub_authors'] = unserialize($item['sub_authors']);
            $x[] = $item;
        }
        if ($x) {
            return $x;
        }
        return array('false');
    }

    /**
     * Поиск людей в БД
     * @return array    массив элементов
     */
    public static function SearchPeople($data)
    {
        $result = Db::init()
            ->SetTable('auth_users', 'a')
            ->AddTable('nsau_people', 'p')
            ->addCondFS('a.displayed_name', 'like', '%' . $data . '%')
            ->addCondFs('a.is_active', '=', 1)
            ->AddCondFF('p.user_id', '=', 'a.id')
            ->AddField('a.displayed_name', 'displayed_name')
            ->AddField('a.id', 'id')
            ->AddField('p.photo', 'photo')
            ->SelectArray(10);
        if ($result) {
            return $result;
        }
        return array('false');
    }

    /**
     * Запись нового адресата для обращений в БД
     * @return array    результат добавления, id адресата
     */
    public static function AddSubject($value)
    {

        if (Db::init()
            ->SetTable('engine_modules_options')
            ->addCondFs('module_id', '=', 57)
            ->addCondFs('option', '=', 'feedback_subject')
            ->addCondFs('value', '=', $value)
            ->SelectArray())
        {
            return array("data" => 'already_exist');
        }
        else
        {
            if (Db::init()
                ->SetTable('engine_modules_options')
                ->AddValue('module_id', 57)
                ->AddValue('option', 'feedback_subject')
                ->AddValue('value', str_replace('"', '', $value))
                ->AddValue('description', 'Адресат для обращений')
                ->Insert())
            {
                $id = Db::init()->SetTable('engine_modules_options')->LastInsertID();
                return array("data" => 'true', "id" => $id);
            }
            else
                return array("data" => 'false');
        }
    }

    /**
     * Удаление адресата для обращений в БД
     * @return array    результат добавления
     */
    public static function DeleteSubject($value)
    {
        if (Db::init()
            ->SetTable('engine_modules_options')
            ->addCondFs('module_id', '=', 57)
            ->addCondFs('option', '=', 'feedback_subject')
            ->addCondFs('id', '=', $value)
            ->Delete())
            return array("data" => 'true');
        return array("data" => 'false');
    }

    /**
     * Удаление обращения в БД
     * @return array    [результат добавления]
     */
    public static function DeleteMessage($value)
    {
        if (Db::init()
            ->SetTable('nsau_feedback_citizens')
            ->addCondFs('id', '=', $value)
            ->Delete())
            return array("data" => 'true');
        return array("data" => 'false');
    }

    public static function DeleteFile($value)
    {
        $file = Db::init()
            ->SetTable('nsau_free_files')
            ->addCondFs('id', '=', $value)
            ->SelectArray(1);
        File::delete($file['id'] . '.' . $file['ext']);
        if (Db::init()
            ->SetTable('nsau_free_files')
            ->addCondFs('id', '=', $value)
            ->Delete())
            return array("data" => 'true');
        return array("data" => 'false');
    }

    public static function RejectMessage($data)
    {
        Db::init()
            ->SetTable('nsau_feedback_citizens')
            ->AddCondFS('id', '=', $data['id'])
            ->AddValue('feedback_message', $data['message'])
            ->AddValue('status', self::MESSAGE_REJECTED)
            ->Update();
        return array("data" => 'rejected');
    }

    public static function AnswerMessage($data)
    {
        // \CF::Debug($data);die;
        Db::init()
            ->SetTable('nsau_feedback_citizens')
            ->AddCondFS('id', '=', $data['id'])
            ->AddValue('feedback_message', $data['message'])
            ->AddValue('status', self::MESSAGE_ANSWERED)
            ->AddValue('answer_file_id', ($data['file_id'] && $data['file_id']!='null') ? $data['file_id'] : null)
            ->AddValue('document_date', $data['document_date'] ? $data['document_date'] : null)
            ->AddValue('document_number', $data['document_number'] ? $data['document_number'] : null)
            ->Update();

        $mail = new Mailer($data['id']);
        $mail->onSendAnswer();

        return array("data" => 'answered');
    }
}
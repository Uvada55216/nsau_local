<?php
namespace modules\Feedback\models;
use modules\Feedback\models\FeedbackModel as F;
use \engine\db\Db;
use helpers\EncodingHelper as H;

class FeedbackModel
{
    /**
     * ������� ��������� ��� ��������� �� ��
     * @return [array]    [������ ���������]
     */
    public static function GetSubjects()
	{
		$result = Db::init()
		->SetTable('engine_modules_options', 'e')
		->AddTable('auth_users', 'a')
		->AddTable('nsau_people', 'p')
		->addCondFs('e.module_id', '=', 57)
		->addCondFs('e.option', '=', 'feedback_subject')
		->AddCondFF('a.id', '=', 'e.value')
		->AddCondFF('p.user_id', '=', 'e.value')
		->AddField('e.value', 'id')
		->AddField('a.displayed_name', 'displayed_name')
		//->AddField('p.photo', 'photo')
		->SelectArray();
		if ($result)
            return $result;
		else
            return array('false');
	}

    public static function GetCategories()
    {
        $result = Db::init()
            ->SetTable('nsau_feedback_categories')
            ->SelectArray();
        if ($result)
            return $result;
        else
            return array('false');
    }

    /**
     * ������ ��������� � ��
     * @return array    [��������� ������]
     */
    public static function SendMessage($data)
	{

        $recaptcha = $data['captcha'];
        if(!empty($recaptcha))
        {
            $google_url ="https://www.google.com/recaptcha/api/siteverify";
            $secret     ="6LevLxMUAAAAAHeI4DXLX2YNZIu3R0b7zF5w8l33";
            $ip         =$_SERVER['REMOTE_ADDR'];
            $url        =$google_url."?secret=".$secret."&response=".$recaptcha."&remoteip=".$ip;
            $res=self::getCurlData($url);
            $res= json_decode($res, true);
            if(!$res['success'])
            {
                return array('status' => 'false', 'error' => 'wrong captcha');
            }
        }
        else
        {
            return array('status' => 'false', 'error' => 'wrong captcha');
        }
//        \CF::Debug($bad);die;
		if ($data['name'] && $data['last_name'] && $data['patronymic'] && $data['subject'] && $data['email'] && $data['message']) 
		{

            $track_number = F::NumberGenerate(4);
            $DB = Db::init()->SetTable('nsau_feedback_citizens')
                ->AddValue('date', date("Y.m.d"))
                ->AddValue('time', date("h:m:s"))
                ->AddValue('name', $data['name'])
                ->AddValue('last_name', $data['last_name'])
                ->AddValue('patronymic', $data['patronymic'])
                ->AddValue('email', $data['email'])
                ->AddValue('phone', $data['phone'] ? $data['phone'] : NULL)
                ->AddValue('subject', $data['subject'])
                ->AddValue('message', $data['message'])
                ->AddValue('track', $track_number)
                ->AddValue('sub_authors', ($data['sub_authors'] && $data['sub_authors']!='[]') ? serialize($data['sub_authors']) : NULL);
            if(!empty($data['file_id'])) {
                $DB->AddValue('message_file_id', $data['file_id'] ? $data['file_id'] : null);
            }
            if(!empty($data['category_id'])) {
                $DB->AddValue('category_id', $data['category_id']);
            }
//            echo $DB->InsertQuery();die;
			if ($DB->Insert())
			{
			    $id = DB::init()->LastInsertID();
			    $mail = new Mailer($id);
			    $mail->messageForSubject();
			    $mail->messageForUser();
                return array('status' => 'true', 'track_number' => $track_number);
			}
		}
        return array('status' => 'false');
    }

    public static function NumberGenerate($len)
	{
		$chars="qazxswedcvfrtgbnhyujmkiolp1234567890"; 
		$max=$len; 
		$size=StrLen($chars)-1; 
		$result=null; 
		while($max--) 
	    $result.=$chars[rand(0,$size)]; 
		return $result;
	}
    public static function getCurlData($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16");
        $curlData = curl_exec($curl);
        curl_close($curl);
        return $curlData;
    }
}
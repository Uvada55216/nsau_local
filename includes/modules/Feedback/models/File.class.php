<?php
/**
 * Created by PhpStorm.
 * User: Roman
 * Date: 17.09.2018
 * Time: 9:34
 */

namespace modules\Feedback\models;


class File
{
    public static function create($arr)
    {
        global $DB, $Auth, $Engine;
        $parts = explode('.', $arr['file']['name']);
        $ext = array_pop($parts);
        $name = implode('.', $parts);

        $DB->SetTable('nsau_free_files');
        $DB->AddValue('name', iconv('utf-8', 'cp1251', $name));
        $DB->AddValue('ext', iconv('utf-8', 'cp1251', $ext));
        $DB->AddValue("upload_date", "NOW()", "X");
        $DB->Insert();
        $id = $DB->LastInsertID();
        if (!empty($id)) {
            move_uploaded_file($arr['file']['tmp_name'], FILES_DIR . 'free/' . $id . '.' . $ext);
            return array('id' => $id, 'name' => $name, 'ext' => $ext);
        }
    }

    public static function delete($name)
    {
        unlink(FILES_DIR . 'free/' . $name);
    }
}
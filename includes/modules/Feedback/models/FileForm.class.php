<?php

namespace modules\Feedback\models;

use forms\BaseForm;

class FileForm extends BaseForm
{

    public function rules()
    {
        return array(
            array('fields' => array('file'), 'validators' => array('forms\\validators\\file' => array('allowed_ext' => array('pdf', 'jpg', 'doc', 'docx', 'rar', 'zip')))),
        );
    }

}
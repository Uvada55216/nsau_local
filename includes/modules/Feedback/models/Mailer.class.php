<?php
/**
 * Created by PhpStorm.
 * User: Roman
 * Date: 19.09.2018
 * Time: 17:28
 */

namespace modules\Feedback\models;


use engine\db\Db;
use engine\PHPMailer\PHPMailer;
use helpers\EncodingHelper;

class Mailer
{

    private $mail;
    private $message;
    private $subject;

    /**
     * Mailer constructor.
     * @param $id
     */
    public function __construct($id)
    {
        $this->mail = new PHPMailer;

        $this->message = EncodingHelper::changeArrayEncoding(DB::init()
            ->SetTable('nsau_feedback_citizens')
            ->AddCondFS('id', '=', $id)
            ->SelectArray(1), 'w2u');

        $this->subject = EncodingHelper::changeArrayEncoding(DB::init()
            ->SetTable('auth_users')
            ->AddCondFS('id', '=', $this->message['subject'])
            ->SelectArray(1), 'w2u');

        $this->mail->isHTML(true);


    }

    private function addAttachments()
    {
        if($this->message['answer_file_id']) {
            $file = EncodingHelper::changeArrayEncoding(DB::init()
                ->SetTable('nsau_free_files')
                ->AddCondFS('id', '=', $this->message['answer_file_id'])
                ->SelectArray(1), 'w2u');
            if($file) {
                $this->mail->addAttachment('files/free/' . $file['id'] . '.' . $file['ext'], $file['name'] . '.' . $file['ext']);
            }
        }
    }

    private function renderBody($view, $data)
    {
        ob_start();
        include "includes/modules/Feedback/views/$view.php";
        $out = ob_get_contents();
        ob_end_clean();
        return $out;
    }

    public function messageForUser()
    {
        $this->mail->Subject = 'Ваше обращение принято к рассмотрению';
        $this->mail->setFrom('noreply@nsau.edu.ru');
        $this->mail->addAddress($this->message['email']);
        $this->mail->Body = $this->renderBody('onSendMessageUser');
        $this->mail->send();
    }

    public function messageForSubject()
    {
        $this->mail->Subject = 'Вам поступило обращение ' . $this->message['track'];
        $this->mail->setFrom('noreply@nsau.edu.ru');
        $this->mail->addAddress($this->subject['email']);
        $this->mail->Body = $this->renderBody('onSendMessageSubject', $this->message['track']);
        $this->mail->send();
    }

    public function onSendAnswer()
    {
        $this->mail->setFrom($this->subject['email'], $this->subject['displayed_name']);
        $this->mail->addAddress($this->message['email']);
        $this->mail->addReplyTo($this->subject['email'], $this->subject['displayed_name']);
        $this->addAttachments();
        $this->mail->Subject = 'Ваше обращение обработано';
        $this->mail->Body = $this->renderBody('mailAnswer', $this->message['feedback_message']);
        $this->mail->send();
    }
}
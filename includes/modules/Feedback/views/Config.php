<div id="config_app">
	<?if($moduleData['Auth']->usergroup_id == 1) {?>
	<div class="row">
		<div class="col-sm-6">
			<!-- ���������� �������� ��� ��������� -->
			<div class="panel panel-primary">
				<div class="panel-heading"> <h3 class="panel-title">��������� ���������</h3></div>
				<div class="panel-body" >
					<ul class="list-group">
					 	<li class="list-group-item">
							<div class="input-group">
								<span class="input-group-addon">���������� ��������</span>	
								<input type="text" class="form-control" v-model="new_subject" placeholder="���">
							</div>
							<div v-for="people in people_search_results" class="people_search_results">		
								<div class="media">
							  		<div class="media-left">
							      		<img class="media-object" src="/images/people/" v-bind:src="'/images/people/'+people.photo">
							  		</div>
							  		<div class="media-body">
							    		<h4 class="media-heading">{{people.displayed_name}}</h4>
							    		<button class="btn btn-default" @click="add_subject(people.id)">��������</button>
							  		</div>
								</div>	
			    			</div>
					 	</li>
					 	<li v-if="errors.length" class="list-group-item list-group-item-danger" id="errors">
				      		<strong v-for="error in errors">{{ error }}</strong>
				  		</li>
			    	</ul>								
				</div>
			</div>
		</div>

		<div class="col-sm-6" id="subjects">
			<!-- �������� ��� ��������� -->
			<div class="panel panel-primary">
				<div class="panel-heading"> <h3 class="panel-title">�������� ��� ���������</h3></div>
				<div v-if="empty_subjects" class="alert alert-info">
					<div>��� �� ������ �������� ��� ���������</div>
				</div>
				<div class="panel-body" v-for="subject in subjects">
					<div class="media">
				  		<div class="media-left">
				      		<img class="media-object" v-bind:src="'/images/people/'+subject.photo">
				  		</div>
				  		<div class="media-body">
				    		<h4 class="media-heading">{{subject.displayed_name}}</h4>
				    		<button class="btn btn-default" @click="delete_subject(subject.id)">�������</button>
				  		</div>
					</div>				
				</div>
			</div>	
		</div>
	</div>
	<?}?>
	<div class="row">
		<div class="col-sm-12">
			<!-- ��������� ��������� -->
			<div class="panel panel-primary">
				<div class="panel-heading"> <h3 class="panel-title">��������� ���������</h3></div>
				<div class="panel-body" >
				<div v-if="empty_messages" class="alert alert-info">
					<div>��� �� ������ ���������</div>
				</div>
					<table class="table table-hover" v-else>
						<thead>
					    	<tr>
					    		<th>����</th>
					      		<th>���</th>
					      		<th>����</th>
					      		<th>����</th>
					      		<th>������</th>
					      		<th>��������</th>
					    	</tr>
					  	</thead>
					  	<tbody>
                        <tr v-for="(message, index) in messages" id="message_row"
                            @click="open_message(message.id, index)" data-toggle="modal"
                            data-target="#feedback_message">
					    		<td>{{message.track}}</td>
					      		<td>{{message.last_name}} {{message.name}} {{message.patronymic}}</td>
					      		<td><a v-bind:href="'/people/'+message.subject_people_id">{{message.subject_displayed_name}}</a></td>
					      		<td>{{message.time}}, {{message.date}}</td>
					      		<td>
					      			<span class="label label-warning" v-if='message.status==1'>�������</span>
					      			<span class="label label-success" v-if='message.status==2'>������������</span>
                                    <span class="label label-danger" v-if='message.status==3'>���������</span>
					      		</td>
					      		<td>
					      			<button class="btn btn-default" v-on:click.prevent="delete_message(message.id)"><span class="glyphicon glyphicon-trash"></span></button>
					      		</td>
					    	</tr>
					  	</tbody>
					</table>					
				</div>
			</div>
		</div>
	</div>
	
	<!-- message Modal -->
	<form class="modal fade" id="feedback_message" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" role="document">
	    	<div class="modal-content" v-for="message in messages" v-if="message.id==opening_message">
	      		<div class="modal-header">
	        		<div class="modal-title" id="exampleModalLabel">��������� <strong>{{message.track}}</strong> �� {{message.last_name}} {{message.name}} {{message.patronymic}} 
	        			<button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close_feedback_form">
	         	 			<span aria-hidden="true">&times;</span>
	        			</button>
	      			</div>
	  			</div>
		    	<div class="modal-body">
		    		<div class="row"  v-if="message.category">
		    			<div class="col-sm-12">
							<ul class="list-group">
  								<li v-for="(category, index) in message.category" class="list-group-item">
  									{{category.name}} 					
  								</li>
							</ul>
		    			</div>
		    		</div>
		    		<div class="row">
		    			<div class="col-sm-6">
		    				<p>��: {{message.last_name}} {{message.name}} {{message.patronymic}}</p>
		    				<p>Email: {{message.email}}</p>
		    				<p v-if="message.phone">�������: {{message.phone}}</p>
		    				<p>����: <a v-bind:href="'/people/'+message.subject_people_id">{{message.subject_displayed_name}}</a></td></p>
		    			</div>
		    			<div class="col-sm-6">
		    				<p> ������:
                                <span class="label label-warning" v-if='message.status==1'>�� ������������</span>
                                <span class="label label-success" v-if='message.status==2'>������������</span>
                                <span class="label label-danger" v-if='message.status==3'>���������</span>
		    				</p>
		    				<p>����: {{message.time}}, {{message.date}}</p>
                            <p>������������� ����</p>
                            <div v-for="file in message.message_files" :key="file.id" class="alert">
                                <li>
                                    <a :href="'download/' + file.id">{{ file.name }}.{{file.ext}}</a>
                                </li>
                            </div>
                        </div>
		    		</div>
		    		<div class="row" v-if="message.sub_authors">
		    			<div class="col-sm-12">
		    				<div>��������:</div>
							<ul class="list-group">
  								<li v-for="(author, index) in message.sub_authors" class="list-group-item">
  									{{author.fio}} ({{author.phone}}, {{author.email}})					
  								</li>
							</ul>
		    			</div>
		    		</div>
		    		<div class="row">
		    			<div class="col-sm-6">
		    				<p>���� ����������� ���������:</p>
							<datepicker :bootstrap-styling="true" format="dd.MM.yyyy" v-model="message.document_date"></datepicker>
		    			</div>
		    			<div class="col-sm-6">
		    				<p>����� ���������� ���������:</p>
                            <input type="text" v-model='message.document_number' class="form-control">
		    			</div>
		    		</div>
		    		<div class="row">
		    			<div class="col-sm-12">
		    				<p>����� ���������:</p>
                            <textarea disabled class="form-control" id="message" rows="5">{{message.message}}</textarea>
		    			</div>
		    		</div>
		    		<div class="row">
		    			<div class="col-sm-12">
		    				<p>����� ������ �� ���������:</p>
                            <textarea v-model="message.feedback_message" class="form-control" id="feedback_message"
                                      rows="5">{{message.feedback_message}}</textarea>
                            
		    			</div>
		    		</div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p>���������� ����:</p>
                            <div id='fileUploadProgress'>
                                <input v-model="file" type="file" id="uploadFile" ref="file"
                                       v-on:change="uploadFile()"/>
                                <span v-if="progress" v-model="progress">{{progress}}%</span>
                                <div v-for="file in fileData" class="alert alert-info">
                                    <li id="file_id">
                                        {{ appendFile(file.id) }} {{ file.name }}.{{file.ext}}
                                        <button class="btn btn-default" v-on:click.prevent="delete_file(file.id)"><span
                                                    class="glyphicon glyphicon-trash"></span></button>
                                    </li>
                                </div>
                            </div>
                        </div>
                    </div>
		      	</div>
				<div class="modal-footer">
                    <button type="button" class="btn btn-danger"
                            @click="reject_message(message.id, message.feedback_message, file)" data-dismiss="modal">
                        ���������
                    </button>
                    <button type="button" class="btn btn-success"
                            @click="answer_message(message.id, message)" data-dismiss="modal">��������
                    </button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal" @click="get_messages">�������</button>
				</div>
	    	</div>
	  	</div>
	</form>
</div>





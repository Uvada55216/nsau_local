<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#feedback_form" id="test">
    ��������� ������
</button>
<!-- Button trigger track modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#feedback_track" id="feedback_track_trigger" style="display: none;"></button>

<div id="feedback_app">
	<!-- feedback Modal -->
	<form class="modal fade" id="feedback_form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
	    	<div class="modal-content">
	      		<div class="modal-header">
                    <div class="modal-title" id="exampleModalLabel">�������� ���������
	        			<button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close_feedback_form">
	         	 			<span aria-hidden="true">&times;</span>
	        			</button>
	      			</div>
	  			</div>
		    	<div class="modal-body">
					<ul class="list-group">
						<li class="list-group-item">
							<div class="input-group">
                                <span class="input-group-addon">���������</span>
								<select class="form-control" v-model="category_id">
									<option v-bind:value="category.id" v-for="category in categories">{{category.name}}</option>
								</select>
							</div>
						</li>
						<li class="list-group-item">
							<div class="input-group">
                                <span class="input-group-addon">����</span>
								<select id="subject" class="form-control" v-model="subject">
									<option v-bind:value="subject.id" v-for="subject in subjects">{{subject.displayed_name}}</option>
								</select>
							</div>
						</li>
						<li class="list-group-item">
							<div class="input-group">
                                <span class="input-group-addon">�������</span>
								<input type="text" class="form-control" id="last_name" v-model="last_name">
							</div>
						</li>
						<li class="list-group-item">
							<div class="input-group">
                                <span class="input-group-addon">���</span>
								<input type="text" class="form-control" id="name" v-model="name">
							</div>
						</li>
						<li class="list-group-item">
							<div class="input-group">
                                <span class="input-group-addon">��������</span>
								<input type="text" class="form-control" id="patronymic" v-model="patronymic">
							</div>
						</li>
						<li class="list-group-item">
							<div class="input-group">
                                <span class="input-group-addon">����� ��������</span>
								<input type="text" class="form-control" id="phone" v-model="phone">
							</div>
						</li>
						<li class="list-group-item disabled">
							<div class="input-group">
                                <span class="input-group-addon">������������ ����������� (������������ ����)</span>
								<input type="text" class="form-control" id="organisation">
							</div>
						</li>
						<li class="list-group-item">
							<div class="input-group">
                                <span class="input-group-addon">E-mail (��� ��������� ������)</span>
								<input type="text" class="form-control" id="email" v-model="email">
							</div>
						</li>
						<li class="list-group-item">
							<div class="input-group">
                                <span class="input-group-addon">��������</span>
		
							</div>

									<div class="input-group">
		                                <span class="input-group-addon">���</span>
										<input type="text" class="form-control" v-model="subAuthor.fio">
									</div>									
									<div class="input-group">
		                                <span class="input-group-addon">�������</span>
										<input type="text" class="form-control" v-model="subAuthor.phone">
									</div>
									<div class="input-group">
		                                <span class="input-group-addon">E-mail</span>
										<input type="text" class="form-control" v-model="subAuthor.email">
									</div>
								
                                <button class="btn" type="button" @click="add_sub_author(subAuthor)">�������� ��������</button>		
							<ul class="list-group">
  								<li v-for="(author, index) in subAuthors" class="list-group-item">
  									{{author.fio}} ({{author.phone}}, {{author.email}})
									<button class="btn btn-default" v-on:click.prevent="delete_sub_author(index)">
										<span class="glyphicon glyphicon-trash"></span>
									</button>  									
  								</li>
							</ul>
						</li>
						<li class="list-group-item">
							<div class="input-group">
                                <span class="input-group-addon">����� ���������</span>
								<textarea class="form-control" id="message" rows="5" v-model="message"></textarea>
							</div>
						</li>
						<li v-if="errors.length" class="list-group-item list-group-item-danger" id="errors">
                            <b>���������� ��������� ��������� ������:</b>
				    		<ul>
				      			<li v-for="error in errors">{{ error }}</li>
				    		</ul>
				  		</li>
					</ul>
                    <div class="row">
                        <div class="col-sm-12">
                            <p>�� ������ ��������� 1 ���� � �������: pdf, doc, docx, zip, rar, jpg</p>
                            <div id='fileUploadProgress'>
                                <input v-model="file" type="file" id="uploadFile" ref="file"
                                       v-on:change="uploadFile()"/>
                                <span v-if="progress" v-model="progress">{{progress}}%</span>
                                <div v-for="file in fileData" class="alert alert-info">
                                    <li id="file_id">
                                        {{ appendFile(file.id) }} {{ file.name }}.{{file.ext}}
                                        <button class="btn btn-default" v-on:click.prevent="delete_file(file.id)"><span
                                                    class="glyphicon glyphicon-trash"></span></button>
                                    </li>
                                </div>
                            </div>
                        </div>
                    </div>
		      	</div>


				<div class="modal-footer">
					     		                            <div class="input-group input-group-sm">
		                                <div class="g-recaptcha" data-sitekey="6LevLxMUAAAAAKcz_AZOCeKpx92QXDBVD79wcWd5"></div>
		                                <script src='https://www.google.com/recaptcha/api.js'></script>
		                                <label class="er_capth" data-original-title="<?=$MODULE_OUTPUT["options"]["er_capth"]?>" data-toggle="tooltip" data-placement="right" aria-describedby="tooltip836805"></label>
		                            </div>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">������</button>
                    <button type="button" class="btn btn-primary" @click="checkForm">���������</button>
				</div>
	    	</div>
	  	</div>
	</form>
	<!-- track Modal -->
	<form class="modal fade" id="feedback_track" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" role="document">
	    	<div class="modal-content">
	      		<div class="modal-header">
                    <div class="modal-title" id="exampleModalLabel">������� �������! 
���� ������ ���������� � �������� ������������ ����������� � ������� ���� ���� � ������� ����������� � ����� �� ������������� ���
	        			<button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close_feedback_form">
	         	 			<span aria-hidden="true">&times;</span>
	        			</button>
	      			</div>
	  			</div>
		    	<div class="modal-body">
                    ���� ��������� ������� ����������. 
		      	</div>
				<div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">�������</button>
				</div>
	    	</div>
	  	</div>
	</form>
</div>

<script type="text/javascript">
	//$("#test").click();
</script>
<script type='module'>
	// import * as moment from 'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js';
	jQuery(document).ready(function($) 
	{
		Vue.component('modal', {
		  template: '#modal-template'
		})
		new Vue(
	    {
	        el: '#config_app',
	        components: {
  				'datepicker': vuejsDatepicker
  			},
	        data: 
	        {
                progress: 0,
                fileData: null,
	            subjects: [],
	            messages: [],
	            new_subject:'',
	            people_search_results: [],
	            errors: [],
	            empty_subjects: false,
	            empty_messages: false,
	            opening_message: null,
	            message: 	null,
	            default_feedback_message: '�� �������� ���� � ������� �� ���� ���������.',
	            file: 	null,
	            file_id: null,
	        },
	        created() 
	        {
	            this.get_subjects();
	            this.get_messages();
	        },
	        methods: 
	        {
                //���������� ���������
                reject_message: function (id, message, file)
                {
                    fetch('/ajax_mode/?route=<?=$__route?>&action=actionRejectMessage&module=<?=$__module?>&type=json&data[id]='+id+'&data[message]='+message,
                    	{'method': 'POST',
                    	'body': data})
                		.then((response) => {if(response.ok){response.json(); return 1;} else throw new Error('Network response was not ok');})
		                .then((json) =>
		                {
		                    this.get_messages();
		                }).catch(function(error) {
    						console.log(error);
  						})

                },
                //����� �� ���������
                answer_message: function (id, message)
                {
                	var file_id = $('#file_id').attr('data-id');
                    fetch('/ajax_mode/?route=<?=$__route?>&action=actionAnswerMessage&module=<?=$__module?>&type=json&data[id]='+id
                    	+'&data[message]='+message.feedback_message
                    	+'&data[file_id]='+this.file_id
                    	+'&data[document_number]='+message.document_number
                    	+'&data[document_date]='+this.format_date(message.document_date))
                		.then((response) => {if(response.ok){response.json(); return 1;} else throw new Error('Network response was not ok');})
		                .then((json) =>
		                {
		                	console.log(json);
		                    this.get_messages();
		                })
                },
	        	//������� ���������
	        	get_subjects: function ()
	        	{
	                fetch('/ajax_mode/?route=<?=$__route?>&action=actionGetSubjects&module=<?=$__module?>&type=json')
	                .then((response) => {if(response.ok){return response.json();}throw new Error('Network response was not ok');})
	                .then((json) => 
	                {
	                	 if (json=='false') 
	                	{
	                		this.subjects = []
	                		this.empty_subjects = true;
	                	}
	                	else
	                	{
	                		this.empty_subjects = false;
	                		this.subjects = json
	                	}
	                    
	                })
	        	},
	        	//������� ���������
	        	get_messages: function ()
	        	{
	                fetch('/ajax_mode/?route=<?=$__route?>&action=actionGetMessages&module=<?=$__module?>&type=json')
	                .then((response) => {if(response.ok){return response.json();}throw new Error('Network response was not ok');})
	                .then((json) => 
	                {
	                	 if (json=='false') 
	                	{
	                		this.messages = []
	                		this.empty_messages = true;
	                	}
	                	else
	                	{
	                		this.emptu_messages = false;
	                		this.messages = json
	                		console.log(json);
	                	}
	                    
	                })
	        	},
	        	//�������� ��������
	        	delete_subject: function (id)
	        	{
	                fetch('/ajax_mode/?route=<?=$__route?>&action=actionDeleteSubject&module=<?=$__module?>&type=json&data='+id)
					.then((response) => {if(response.ok){return response.json();}throw new Error('Network response was not ok');})
	                .then((json) => 
	                {
	                	this.get_subjects();
	                })
	        	},
	        	//���������� ��������
	        	add_subject: function (subject)
	        	{
	                fetch('/ajax_mode/?route=<?=$__route?>&action=actionAddSubject&module=<?=$__module?>&type=json&data='+subject)
	                .then((response) => {if(response.ok){return response.json();}throw new Error('Network response was not ok');})
	                .then((json) => 
	                {
	                	if (json.data == 'already_exist') 
	                	{
	                		this.errors = [];
	                		this.errors.push("���� ������������ ��� �������� � ��������");
	                	}
	                	else
	                	{
		                	this.get_subjects();
		                	this.people_search_results = [];
	            			this.new_subject = '';
	            			this.errors = [];
            			}
	                })
	        	},
	        	//�������� ��������
	        	delete_message: function (id)
	        	{
	                fetch('/ajax_mode/?route=<?=$__route?>&action=actionDeleteMessage&module=<?=$__module?>&type=json&data='+id)
					.then((response) => {if(response.ok){return response.json();}throw new Error('Network response was not ok');})
	                .then((json) => 
	                {
	                	this.get_messages();
	                })
	        	},
	        	open_message: function (id, i)
	        	{	    			
	    			this.fileData = this.messages[i].answer_files;
	    			this.file = null;
	        		this.opening_message = id;
	        		if(!this.messages[i].feedback_message) {
	        			this.messages[i].feedback_message = this.default_feedback_message;
	        		}
	        	},
	        	delete_file: function (id)
	        	{
	                fetch('/ajax_mode/?route=<?=$__route?>&action=actionDeleteFile&module=<?=$__module?>&type=json&data='+id)
					.then((response) => {if(response.ok){return response.json();}throw new Error('Network response was not ok');})
	                .then((json) => 
	                {
	                	return false;
	                })
	                this.fileData = [];
	                
	        	},
	            appendFile: function(file) {
                	this.file_id = file;
                },
                uploadFile: function () {
                	if(this.file_id) {
                		this.delete_file(this.file_id);
                	}
                    var files;
                    var that = this;
                    var data = new FormData();
                    $('#uploadFile').each(function () {
                        files = this.files;
                    });

                    $.each(files, function (key, value) {
                        data.append(key, value);
                    });
                    data.append('route', '<?=$__route?>');
                    data.append('action', 'actionUploadFile');
                    data.append('module', '<?=$__module?>');
                    data.append('type', 'json');


                    $.ajax({
                        type: 'POST',
                        url: '/ajax_mode/',
                        dataType: 'json',
                        data: data,
                        processData: false,
                        contentType: false,
                        xhr: function () {
                            var xhr = $.ajaxSettings.xhr();
                            xhr.upload.addEventListener('progress', function (evt) {
                                if (evt.lengthComputable) {
                                    var percentComplete = Math.ceil(evt.loaded / evt.total * 100);
                                    that.progress = percentComplete;
                                }
                            }, false);
                            return xhr;
                        },
                        success: function (data) {
                            if (data == '') {
                            	that.errors.push('������ ��� �������� �����.');
                            } else if(data.errors) {
                            	that.errors.push(data.errors['file'][0][0]);
                            	that.file = null;
                            } else {
                                that.fileData = data;    
                                that.file = null;     
                            }
                            that.progress = 0;
                        }
                    });

                },
 	 			format_date: function (date) {
	 				// console.log(date);
	 				if(date instanceof Date) {
						let d = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds()));
						return moment(d).format("YYYY-MM-DD")	 					
	 				} 
	 				return '';
				},

	        },
	 		watch: 
	 		{

	 			//����� �����
	            new_subject: function(val, oldVal)
	            {
					if (val.length > 3) 
					{
		                fetch('/ajax_mode/?route=<?=$__route?>&action=actionSearchPeople&module=<?=$__module?>&type=json&data='+val)
						.then((response) => {if(response.ok){return response.json();}throw new Error('Network response was not ok');})
		                .then((json) => 
		                {
		                	if (json == 'false') 
		                		this.people_search_results = []
		                	else
		                		this.people_search_results = json		                	
		                })
					}
					else
					{
						this.people_search_results = []
					}
	            }
	        }
	    });
	});
</script>
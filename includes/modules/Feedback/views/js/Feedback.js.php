<script type='text/javascript'>
	jQuery(document).ready(function($) 
	{
		new Vue(
	    {
	        el: '#feedback_app',
	        data: 
	        {
	        	subjects: [],
	        	categories: [],
	            errors: [],
	            name: 		null,
	            last_name: 	null,
	            patronymic: null,
	            subject: 	null,
	            email: 		null, 
	            message: 	null,
	            phone: null,
	            category_id: null,
	            track_number: null,
                progress: 0,
                fileData: null,
	            file: 	null,
	            file_id: null,
	            subAuthor: {'fio': null, 'phone': null, 'email': null},
	            subAuthors: [],
	            captcha: null,
	        },
	        created() 
	        {
	            this.subjects = JSON.parse('<?=$moduleData['subjects']?>');
	            this.categories = JSON.parse('<?=$moduleData['categories']?>');
	        },
	        methods: 
	        {
	        	delete_file: function (id)
	        	{
	                fetch('/ajax_mode/?route=<?=$__route?>&action=actionDeleteFile&module=<?=$__module?>&type=json&data='+id)
					.then((response) => {if(response.ok){return response.json();}throw new Error('Network response was not ok');})
	                .then((json) => 
	                {
	                	return false;
	                })
	                this.fileData = [];
	                this.file = null;
	                this.file_id = null;
	                
	        	},
	            appendFile: function(file) {
                	this.file_id = file;
                },
                uploadFile: function () {
                	if(this.file_id) {
                		this.delete_file(this.file_id);
                	}
                    var files;
                    var that = this;
                    var data = new FormData();
                    $('#uploadFile').each(function () {
                        files = this.files;
                    });

                    $.each(files, function (key, value) {
                        data.append(key, value);
                    });
                    data.append('route', '<?=$__route?>');
                    data.append('action', 'actionUploadFile');
                    data.append('module', '<?=$__module?>');
                    data.append('type', 'json');


                    $.ajax({
                        type: 'POST',
                        url: '/ajax_mode/',
                        dataType: 'json',
                        data: data,
                        processData: false,
                        contentType: false,
                        xhr: function () {
                            var xhr = $.ajaxSettings.xhr();
                            xhr.upload.addEventListener('progress', function (evt) {
                                if (evt.lengthComputable) {
                                    var percentComplete = Math.ceil(evt.loaded / evt.total * 100);
                                    that.progress = percentComplete;
                                }
                            }, false);
                            return xhr;
                        },
                        success: function (data) {
                            if (data == '') {
                            	that.errors.push('������ ��� �������� �����.');
                            } else if(data.errors) {
                            	that.errors.push(data.errors['file'][0][0]);
                            	that.file = null;
                            } else {
                                that.fileData = data;    
                                that.file = null;     
                            }
                            that.progress = 0;
                        }
                    });

                },
   				checkForm:function(e) 
   				{
   					this.captcha = grecaptcha.getResponse();
			      	this.errors = [];
			      	if(!this.name) this.errors.push("������� ���");
			      	if(!this.last_name) this.errors.push("������� �������");
			      	if(!this.patronymic) this.errors.push("������� ��������");
			      	if(!this.captcha) this.errors.push("�������� ������");
 					if(!this.email) 
 					{
        				this.errors.push("������� ����� ����������� �����");
      				} 
      				else if(!this.validEmail(this.email)) 
      				{					
				        this.errors.push("������� ���������� ����� ����������� �����.");        
				    }
			      	if(!this.subject) this.errors.push("������� �������� ���������");
			      	if(!this.category_id) this.errors.push("������� ���������");
			      	if(!this.message) this.errors.push("������ ����� ���������");
			      	if(!this.errors.length) 
			      	{
			      		this.sendMessage();
			      		return true;
			      	}
			      	e.preventDefault();
			    },
			    sendMessage:function()
			    {
			    	var data = {
			    				'type': 'json',
			    				'action': 'actionSendMessage',
			    				'module': '<?=$__module?>',
			    				'route': '<?=$__route?>',
			    				'data': {
				    				"last_name": this.last_name,
				    				"patronymic": this.patronymic,
				    				"name": this.name,
				    				"email": this.email,
				    				"phone": this.phone,
				    				"subject": this.subject,
				    				"message": this.message,
				    				"file_id": this.file_id,
				    				"category_id": this.category_id,
				    				"sub_authors": this.subAuthors,
				    				"captcha": this.captcha,
			    				}
			   		 };
	                fetch('/ajax_mode/', 
{'method': 'POST', 'body': JSON.stringify(data)}
	                	)
					.then((response) => {if(response.ok){return response.json();}throw new Error('Network response was not ok');})
	                .then((json) => 
	                {
	                	if (json.status == 'true') 
	                	{
	                		this.track_number = json.track_number
	                		$("#close_feedback_form").click()
	                		this.name = null
				            this.last_name=null
				            this.patronymic=null
				            this.subject=null
				            this.email=null
				            this.message=null
				            this.phone=null
				            $("#feedback_track_trigger").click()
	                	}
	                })
			    },
				validEmail:function(email) 
				{
      				var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      				return re.test(email);
    			},
    			add_sub_author: function(sub)
    			{
    				this.errors = [];
    				if(!this.validEmail(sub.email) && sub.email != null) 
      				{					
				        this.errors.push("������� ���������� ����� ����������� ����� ��������.");        
				    }
    				if(sub.fio == null) this.errors.push("��� �������� ����������� ��� ����������.");        
				    
				   if(!this.errors.length) 
			      	{
	    				const clone = (obj => JSON.parse ( JSON.stringify(obj) ) );
	    				this.subAuthors.push(clone(sub));
	    				sub.fio = null;
	    				sub.phone = null;
	    				sub.email = null;  	
			      	}
			
    			},
    			delete_sub_author: function(index)
    			{
    				this.subAuthors.splice(index, 1);
    				console.log(this.subAuthors);    				
    			},

	        },
	        computed:
	        {

	        }
	    });
	});
</script>



<?php

namespace modules\Group;

class Group extends \engine\modules\BaseModule
{
	public function rules() {

		return array(
			array('GET|POST', '/', $this->initParams->mode . 'Controller#actionIndex'),
			array('GET|POST', 'delete/[i:id]', $this->initParams->mode . 'Controller#actionDeleteGroup'),
			array('GET|POST', 'edit/[i:id]', $this->initParams->mode . 'Controller#actionEdit'),
			array('GET|POST', 'update', $this->initParams->mode . 'Controller#actionUpdate'),
			array('GET|POST', 'editBy1C', $this->initParams->mode . 'Controller#actionEditGroupBy1c'),
		);
	} 
}
<?php

namespace modules\Group;

use \engine\modules\configElements\Select;
use \engine\modules\configElements\Input;
use \engine\modules\configElements\Checkbox;

class GroupConfig extends \engine\modules\BaseModuleConfig
{
	public $mode;
	public $cacheLifetime;
	public $useCache;

	private $__mode = array(
		array('id' => 'group', 'name' => '������ ����� 1c'),
		array('id' => 'faculty', 'name' => '������ ����������� 1c')
	);


	public function params() 
	{
		return array(
			'����� ������ (��������� ����������)' => array(
				'element' => new Select('mode', $this->mode, $this->__mode),
				'obligatory' => 'true',
				'switcher' => 'true',
				'group' => 'mode',
			),
			'������������ ���' => array(
				'element' => new Checkbox('useCache', $this->useCache)
			),
			'����� ����� ����' => array(
				'element' => new Input('cacheLifetime', $this->cacheLifetime),
			),

		);
	}
}
<?php

namespace modules\Group\controllers;

//use modules\Group\core\RequestParams;

use modules\Group\core\GroupsClient1c;
use modules\Group\models\FacultyForm;
use modules\Group\models\GroupForm;
use repository\faculty\Faculty1cRepository;
use repository\faculty\FacultyRepository;
use repository\group\GroupRepository;
use repository\ProfileRepository;
use repository\SpecialityRepository;

abstract class BaseGroupController extends \engine\controllers\BaseController
{
	public $Params1c;
	public $Client1c;
	public $engineUri;
	public $Repository;
	public $Validator;

	public function __construct(\engine\modules\BaseModule $Creator)
	{ 
		parent::__construct($Creator);
		$this->engineUri = $Creator->contextData->engineUri;

		//отловить исключения и рендерить страницу с ошибкой.
		$this->Module->initParams->mode == 'Group' || 'Faculty' ? $init1c = false : $init1c = true;
		$this->Client1c = new GroupsClient1c($init1c);

		//$this->Params1c = new RequestParams();

		$this->Repository = array(
			'faculty' => new FacultyRepository(),
			'group' => new GroupRepository(),
			'profile' => new ProfileRepository(),
			'speciality' => new SpecialityRepository(),
			'faculty1c' => new Faculty1cRepository($this->Client1c),
		);
		$this->Validator = array(
			'faculty' => new FacultyForm(),
			'group' => new GroupForm(),
		);
		$this->configureClientCache();
	}

	private function configureClientCache()
	{
		if(!empty($this->Module->initParams->cacheLifetime)) {
			$this->Client1c->setCacheLifetime($this->Module->initParams->cacheLifetime);
		}
		if($this->Module->initParams->useCache) {
			$this->Client1c->enableCache();

		} else {
			$this->Client1c->disableCache();
		}
	}

	public function renderError($view = "error", $moduleData = null)
	{
		$__requestUri = $this->Module->requestUri;
		$inputData = $moduleData['data'];
		ob_start();
		include "includes/views/exception/$view.php";
		$out = ob_get_contents();
		ob_end_clean();
		return $out;
	}
}
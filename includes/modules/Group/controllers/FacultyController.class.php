<?php

namespace modules\Group\controllers;

use \requests\Request;
use entity\faculty\Faculty;


class FacultyController extends BaseGroupController
{
	public function actionIndex(){
		$output['faculties'] = $this->Repository[$this->Module->initParams->mode]->getFaculties();
		return $this->render($this->Module->initParams->mode.'/FacultyList', $output);
	}

	/**
	 * Редактирование
	 */
	public function actionEdit($facultyId){
		$facultyId = (int)$facultyId;
		$request = Request::init()->post();

		$rf = $this->Repository[$this->Module->initParams->mode];
		$facultyEdit = $rf->getFacultyById($facultyId);

		$output['faculty'] = $facultyEdit;
		$output['faculties'] = $rf->getFaculties();

		if($request){
			$facultyEdit->setName(isset($request['name']) ? htmlspecialchars($request['name']): '');
			$facultyEdit->setShortName(isset($request['short_name']) ? htmlspecialchars($request['short_name']): '');
			$facultyEdit->setCode1c(isset($request['code1c']) ? (int)($request['code1c']): '');
			$facultyEdit->setPid(isset($request['pid']) ? (int)($request['pid']): '');
			$facultyEdit->setGroupNum(isset($request['group_num']) ? (int)($request['group_num']): '');
			$facultyEdit->setGroups(isset($request['groups']) ? htmlspecialchars($request['groups']): '');
			$facultyEdit->setLink(isset($request['link']) ? htmlspecialchars($request['link']): '');
			$facultyEdit->setPos(isset($request['pos']) ? (int)($request['pos']): '');
			$facultyEdit->setFoundationYear(isset($request['foundation_year']) ? (int)($request['foundation_year']): '');
			$facultyEdit->setClosingYear(isset($request['closing_year']) ? (int)($request['closing_year']): '');
			$facultyEdit->setComment(isset($request['comment']) ? htmlspecialchars($request['comment']): '');
			$facultyEdit->setIsActive(isset($request['is_active']) ? (int)($request['is_active']): '');

			$formValid = $this->Validator[$this->Module->initParams->mode];
			$v = $formValid->validate($request);

			$output['faculty'] = $facultyEdit;
			if(!$v){
				$output['errors'] = $formValid->getErrors();
			}else{
				$u = $rf->updateFaculty($facultyEdit);

				if($u){
					\CF::Redirect($this->engineUri);
				}else{
					$output['error'] = \CF::Utf2Win('Ошибка обновления в БД');
					$output['url'] = '/office/faculty/';
					return $this->renderError('error', $output);
				}
			}
		}

		return $this->render($this->Module->initParams->mode.'/FacultyEdit', $output);
	}

	/**
	 * Удаление из БД
	 */
	public function actionDelete($facultyId){
		$facultyId = (int)$facultyId;
		$rf = $this->Repository[$this->Module->initParams->mode];

		if($facultyId && $facultyId > 0){
			$d = $rf->deleteFaculty($facultyId);

			if($d){
				\CF::Redirect($this->engineUri);
			} else {
				$output['error'] = \CF::Utf2Win('Ошибка удаления из БД');
				$output['url'] = '/office/faculty/';
				return $this->renderError('error', $output);
			}
		}
	}

	/**
	 * Обновление из 1с
	 */
	public function actionUpdate(){
		$faculties1c = $this->Repository['faculty1c']->getFaculties();

		$rm = $this->Repository['faculty']->agreeFacultyBy1c($faculties1c);

		if($rm){
			\CF::Redirect($this->engineUri);
		}else{
			$output['error'] = \CF::Utf2Win('Ошибка обновления из 1с');
			$output['url'] = '/office/faculty/';
			return $this->renderError('error', $output);
		}
	}

	/**
	 * Добавление
	 */
	public function actionAdd(){
		$request = Request::init()->post();
		$rf = $this->Repository[$this->Module->initParams->mode];

		$facultyAdd = new Faculty();
		$output['faculty'] = $facultyAdd;
		$output['faculties'] = $rf->getFaculties();

		if($request){
			$facultyAdd->setName(isset($request['name']) ? htmlspecialchars($request['name']): '');
			$facultyAdd->setShortName(isset($request['short_name']) ? htmlspecialchars($request['short_name']): '');
			$facultyAdd->setCode1c(isset($request['code1c']) ? (int)($request['code1c']): '');
			$facultyAdd->setPid(isset($request['pid']) ? (int)($request['pid']): '');
			$facultyAdd->setGroupNum(isset($request['group_num']) ? htmlspecialchars($request['group_num']): '');
			$facultyAdd->setGroups(isset($request['groups']) ? htmlspecialchars($request['groups']): '');
			$facultyAdd->setLink(isset($request['link']) ? htmlspecialchars($request['link']): '');
			$facultyAdd->setPos(isset($request['pos']) ? (int)($request['pos']): '');
			$facultyAdd->setFoundationYear(isset($request['foundation_year']) ? (int)($request['foundation_year']): '');
			$facultyAdd->setClosingYear(isset($request['closing_year']) ? (int)($request['closing_year']): '');
			$facultyAdd->setComment(isset($request['comment']) ? htmlspecialchars($request['comment']): '');
			$facultyAdd->setIsActive(isset($request['is_active']) ? (int)($request['is_active']): '');

			$formValid = $this->Validator[$this->Module->initParams->mode];
			$v = $formValid->validate($request);

			$output['faculty'] = $facultyAdd;

			if(!$v){
				$output['errors'] = $formValid->getErrors();
			}else{
				$u = $rf->addFaculty($facultyAdd);

				if($u){
					\CF::Redirect($this->engineUri);
				}else{
					$output['error'] = \CF::Utf2Win('Ошибка записи БД');
					$output['url'] = '/office/faculty/';
					return $this->renderError('error', $output);
				}
			}
		}
		return $this->render($this->Module->initParams->mode.'/FacultyEdit', $output);
	}
}
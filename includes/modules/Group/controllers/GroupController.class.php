<?php

namespace modules\Group\controllers;

use engine\Http\HttpFoundation\Response;
use entity\faculty\Faculty;
use entity\group\Group;
use repository\group\GroupRepository;
use \requests\Request;
use engine\PHPMailer\PHPMailer;


class GroupController extends BaseGroupController
{
	public function actionIndex()
	{
		$rf = $this->Repository['faculty'];
		$output['faculties'] = $rf->getFacultyAndGroups();
		$output['directions'] = $rf->getFacultyAndSpecialities();
		$groupsNotFac = $this->Repository[$this->Module->initParams->mode]->getGroupsByIdFaculty('0');
		$output = array_merge($output, $this->addMetaData());

		if(count($groupsNotFac)> 0){
			$emptyFac = new Faculty();
			$emptyFac->setId('0');
			$emptyFac->setIsActive(1);
			$emptyFac->setName('��������� �� ������');
			$emptyFac->setArrGroups($groupsNotFac);
			$output['faculties'][] = $emptyFac;
		}
//		$this->renderAjax('css/ConfigStyles'); //����������� ����� �� �������
		$this->renderJs('Group');    //����������� js �����
		return $this->render($this->Module->initParams->mode.'/GroupList', $output);
	}

	/**
	 * ��������������
	 */
	public function actionEdit($groupId){
		$groupId = (int)$groupId;
		$request = Request::init()->post();
		$rg = $this->Repository[$this->Module->initParams->mode];
		$rf = $this->Repository['faculty'];
		$groupEdit = $rg->getGroupById($groupId);

		$output['faculty'] = ($groupEdit->getIdFaculty() !=0)
							? $rf->getFacultyById($groupEdit->getIdFaculty())
							: new Faculty(array('name' => '��������� �� ������'));

		$output['group'] = $groupEdit;
		$output['profiles'] = $this->Repository['profile']->getProfilesBySpecId($groupEdit->getIdSpeciality());;
		$output['faculties'] = $rf->getFacultyAndSpecialities();
		$output = array_merge($output, $this->addMetaData());

		if($request){
			$specId = (int)$request['id_spec'];

			if($specId != 0){
				$newSpec = $this->Repository['speciality']->getSpecialityById($specId);
				$specialitiesCode = $newSpec->getCode();
				$idSpeciality = $newSpec->getId();
				$idFaculty = $newSpec->getFacultyId();
			}else{
				$specialitiesCode = '';
				$idSpeciality = null;
				$idFaculty = $specId;
			}

			$groupEdit->setName(\CF::H($request['name']));
			$groupEdit->setIdFaculty($idFaculty);
			$groupEdit->setSpecialitiesCode($specialitiesCode);
			$groupEdit->setIdSpeciality($idSpeciality);
			$groupEdit->setFormEducation((int)($request['form_education']));
			$groupEdit->setYear((int)($request['year']));
			$groupEdit->setProfile((int)$request['profile']);
			$groupEdit->setQualification((int)($request['qualification']));
			$groupEdit->setHidden(isset($request['hidden'])? 1: 0);

			$formValid = $this->Validator[$this->Module->initParams->mode];
			$v = $formValid->validate($request);

			$output['group'] = $groupEdit;

			if(!$v){
				$output['errors'] = $formValid->getErrors();
			} else {
				$u = $rg->updateGroup($groupEdit);
				if($u){
					\CF::Redirect($this->engineUri);
				} else {
					$output['error'] = \CF::Utf2Win('������ ���������� � ��');
					return $this->renderError('error', $output);
				}
			}
		}

		return $this->render($this->Module->initParams->mode . '/GroupEdit', $output);
	}

	/**
	 *
	 */
	public function actionIssetGroup($name){
		$rg = $this->Repository[$this->Module->initParams->mode];
		$name = iconv('utf-8','windows-1251', $name);
		$out = $rg->getGroupByName($name, false);

		return json_encode($out);
	}

	/**
	 * @return mixed
	 */
	protected function addMetaData(){
		$output['qualif'] = require INCLUDES.'/modules/Group/metadata/qualification.php';
		$output['formEdu'] = require INCLUDES.'/modules/Group/metadata/formEducation.php';
		$output['yearArray'] = require INCLUDES.'/modules/Group/metadata/yearLearning.php';

		return $output;
	}

	/**
	 *
	 */
	public function actionAddGroup(){
		$request = Request::init()->ajax();
		$rg = $this->Repository[$this->Module->initParams->mode];

		$request['id_spec'] = (int)$request['id_spec'];
		$request['name'] = \CF::H($request['name']);
		$request['form_education'] = (int)$request['form_education'];
		$request['year'] = (int)$request['year'];
		$request['qualification'] = (int)$request['qualification'];

		$newGroup = new Group($request);

		$result['success'] = 0;

		if(!$request['id_spec']){
			$result['success'] = $rg->addGroup($newGroup);
		}else{
			$rs = $this->Repository['speciality'];
			$spec = $rs->getSpecialityById($request['id_spec']);

			$newGroup->setIdFaculty($spec->getFacultyId());
			$newGroup->setSpecialitiesCode($spec->getCode());
			$newGroup->setIdSpeciality($spec->getId());
			$rs = $rg->addGroup($newGroup);

			if($rs){
				$result['success'] = $rs;
			}
		}
		return json_encode($result);
	}

	/**
	 * Delete group by admin panel
	 * @param $id
	 *
	 * @return false|redirect
	 */
	public function actionDeleteGroup($id){
		$id = (int)$id;
		$rg = $this->Repository[$this->Module->initParams->mode];

		if($delGroup = $rg->getGroupById($id)){
			$rs = $rg->deleteGroup($delGroup);

			if($rs){
				\CF::Redirect($this->engineUri);
			} else {
				$output['error'] = \CF::Utf2Win('������ �������� �� ��');
				$output['url'] = '/office/group/';
				return $this->renderError('error', $output);
			}
		}
		\CF::Redirect($this->engineUri);
	}

	/**
	 * Edit group by 1c
	 *
	 * @param $code id group by 1c
	 * @param $name name group by 1c
	 * @param null $method
	 *
	 * @return void
	 *
	 */
	public function actionEditGroupBy1c($code, $name, $delete = 0){
		$response = new Response('BadEditGroup', Response::HTTP_INTERNAL_SERVER_ERROR, array('content-type' => 'application/json'));
		$response->setCharset('ISO-8859-1');

		if(isset($name, $code)) {
			try {
				$rg = new GroupRepository();
				$name = \CF::H($name);
				$name = \CF::Utf2Win($name);
				$code = (int)$code;
				$delete = (boolean)$delete;
				$oldName ='';

				if(($code != false && $name != false)){
					$edit ='';

					if(!is_null($group = $rg->getGroupByCode($code))){//if group isset that update group or delete.
						$editGroup = new Group($group);

						if($delete == 1){//if $delete == 1
							$rs = $rg->deleteGroup($editGroup);
							$edit = 'delete';
						}else {
							$oldName= $editGroup->getName();

							$editGroup->setName($name);
							$rs = $rg->updateGroup($editGroup);
							$edit = 'update';
						}
					} else {//add new group
						$addGroup = new Group(array('name' => $name));
						$addGroup->setCode1c($code);
						$rs = $rg->addGroup($addGroup);
						$edit = 'add';
					}

					$name = \CF::Win2Utf($name);

					if($rs){
						$edit = ucfirst($edit);
						$mail = new PHPMailer();
						$mail->setFrom((string)'noreply@nsau.edu.ru',  'User1c');
						$mail->addAddress((string)'info@nsau.edu.ru');

						if($edit == 'Update'){
							$oldName = \CF::Win2Utf($oldName);

							$mail->Subject = $edit.' group '.$oldName;
							$mail->msgHTML("<html><body>
				               <p>{$edit} name group {$oldName} by new name {$name}</p>
				               </html></body>");
						}else{
							$mail->Subject = $edit.' group '.$name;
							$mail->msgHTML("<html><body>
				               <p>{$edit} group {$name}</p>
				               </html></body>");
						}

						$mail->send();

						$response->setContent(json_encode(array('success' => sprintf('%s group %s.', $edit, $name))));
						$response->setStatusCode(Response::HTTP_OK);
						$response->send();
						exit;
					} else {
						throw new \Exception(sprintf('Server error. Group %s not edit.', $name));
					}
				} else {
					throw new \Exception(sprintf('Server error. Invalid arguments'));
				}
			}catch (\Exception $e){
				$response->setContent(json_encode(array('error' => $e->getMessage())));
				$response->send();
				exit;
			}
		}

		$response->setStatusCode(Response::HTTP_FORBIDDEN);
		$response->headers->set('Content-type', 'text/html');
		$response->send();
		exit;
	}
}
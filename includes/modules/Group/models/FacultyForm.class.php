<?php

namespace modules\Group\models;

use forms\BaseForm;


class FacultyForm extends BaseForm
{
    public function rules(){
        return array(
            array(
                'fields' => array('name'),
                'validators' => array('length' => array('min' => 3, 'max' => 120), 'required'),
	            'filters' => array('text'),
            ),
            array(
                'fields' => array('pid', 'code1c', 'pos', 'foundation_year'),
	            'filters' => array('integer'),
            ),
            array(
	            'fields' => array('faculty_link', 'comment'),
	            'filters' => array('text'),
            ),
        );
    }
}
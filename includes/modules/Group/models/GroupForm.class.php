<?php

namespace modules\Group\models;

use forms\BaseForm;


class GroupForm extends BaseForm
{
    public function rules(){
        return array(
            array(
                'fields' => array('name'),
                'validators' => array('required'),
	            'filters' => array('text'),
            ),
        );
    }
}
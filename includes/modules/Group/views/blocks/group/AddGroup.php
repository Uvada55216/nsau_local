	<div name="add_group" id="add_group">
		<div class="panel panel-nsau">
			<div class="panel-heading">���������� ������</div>
			<div class="panel-body">
				<dd id="new_group_name">
                    <input type="text" name="group_name" class="form-control" placeholder="��� ������" id="new_group">
					<a class="btn btn-nsau btn-sm ajax_edit_group" href="">�������������</a>
				</dd>
				<dd id="group_faculties">
					<select name="faculties" size="1" class="form-control" id="napr">
						<option  value="0" selected="selected" id="option_prev">����������� ����������</option>
						<?php foreach($moduleData["directions"] as $faculty){ ?>
                            <optgroup label="<?=$faculty->getName()?>">
								<?php if(count($arrSpecialities = $faculty->getArrSpecialities()) > 0){
									foreach($arrSpecialities as $spec){ ?>
                                            <option value="<?=(int)($spec->getId())?>"><?=$spec->getCode()?>
                                                - <?=$spec->getName()?></option>
											<?php
										}
									} ?>
                            </optgroup>
						<?php } ?>
					</select>
				</dd>
				<dd id="form_education" >
					<select id="group_form" name="form_edu" class="form-control">
						<option value="0" selected="selected">����� ��������</option>
						<?php foreach($moduleData['formEdu'] as $form_code => $form_edu){ ?>
							<option value="<?=(int)$form_code?>"><?=$form_edu?></option>
						<?php } ?>
					</select>
				</dd>
				<dd id="group_year">
					<select name="year" size="1" class="form-control" id="group_course">
                        <option value="bad" hidden>���� �� ����������</option>
                        <?php foreach($moduleData['yearArray'] as $key=> $val) { ?>
						    <option value="<?=(int)$key?>" <?=($key == 1 ? ' selected="selected"': '')?>><?=$val?></option>
						<?php }?>
					</select>
				</dd>
				<dd id="group_qualification">
					<select name="qualif" class="form-control" id="group_qualif">
						<?php foreach($moduleData['qualif'] as $qual_code => $qualif){
							if($qual_code == 62) { ?>
								<option value="<?=(int)$qual_code?>" selected="selected"><?=$qualif?></option>
							<?php }	elseif ($qualif == "�����������") {	?>
							    <option value="<?=$qual_code?>" id="asp"><?=$qualif?></option>
							<?php } else{ ?>
							    <option value="<?=(int)$qual_code?>"><?=$qualif?></option>
							<?php }
						} ?>
					</select>
				</dd>
                <a id="ajax_add_group" class="btn btn-nsau btn-sm">��������</a>
			</div>
		</div>
	</div>
	<div class="alert alert-success" hidden>������ ���������</div>
	<div class="alert alert-danger" hidden>������ ����������</div>
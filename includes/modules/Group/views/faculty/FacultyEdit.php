<link rel="stylesheet" href="/themes/styles/faculty.css" type="text/css"/>
<?php
$facultyEdit = $moduleData['faculty'];
$faculties = $moduleData['faculties'];

if(isset($moduleData['errors'])){
    $error = array(
            'name' => "<small id=\"emailHelp\" class=\"form-text text-muted\" style=\"color: red\">".$moduleData['errors']['name'][0][0]."</small>",
    );
} ?>
<div class="col-sm-12">
    <div class="row">
<?php if(!$facultyEdit->getName()){ ?>
    <h3>�������� ��������� ��� ��������:</h3>
<?php } else { ?>
    <h3>�������������� ���������� � ���������� <?=$facultyEdit->getName()?>:</h3>
<?php } ?>
        <form name="" method="post">
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="name">�������� ����������:</label>
                    <input type="text" name="name" value="<?=$facultyEdit->getName()?>" class="form-control" id="name"/>
                    <?= $error['name'] ? $error['name'] : '' ?>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="pid">��������� ��� ��������, � �������� ��������� ������ ���������:</label>
                    <select name="pid" size="1" id="pid" class="form-control">
                        <option value="0" selected="selected">�������� ��������� (���� �� ���������)</option>
                        <?php foreach($faculties as $faculty){
                                if($facultyEdit->getPid() == $faculty->getId()){
                                    ?>
                                    <option value="<?=$faculty->getId()?>" selected="selected"><?=$faculty->getName()?></option>
                                    <?php
                                } else { ?>
                                    <option value="<?=$faculty->getId()?>"><?=$faculty->getName()?></option>
                                    <?php
                                }
                            }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-2">
                    <label for="short_name">����. ��������:</label>
                    <input type="text" name="short_name" value="<?=$facultyEdit->getShortName()?>" class="form-control" id="short_name"/>
                </div>
                <div class="form-group col-md-10">
                    <label for="link">���� ����������:</label>
                    <input type="text" name="link" id="link" class="form-control" value="<?=$facultyEdit->getLink()?>"/>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-2">
                    <label for="code1c">��� � 1�:</label>
                    <input type="text" name="code1c" id="code1c" class="form-control" value="<?=$facultyEdit->getCode1c()?>"/>
                </div>
                <div class="form-group col-md-2">
                    <label for="group_num">����� ������:</label>
                    <input type="text" name="group_num" id="group_num" class="form-control" value="<?=$facultyEdit->getGroupNum()?>"/>
                </div>
                <div class="form-group col-md-2">
                    <label for="groups">������ �����:</label>
                    <input type="text" name="groups" id="groups" class="form-control" value="<?=$facultyEdit->getGroups()?>"/>
                </div>
                <div class="form-group col-md-2">
                    <label for="pos">������� � ������:</label>
                    <input type="text" name="pos" id="pos" class="form-control" value="<?=$facultyEdit->getPos()?>"/>
                </div>
                <div class="form-group col-md-2">
                    <label for="foundation_year">��� ���������:</label>
                    <input type="text" name="foundation_year" id="foundation_year" class="form-control" value="<?=$facultyEdit->getFoundationYear()?>"/>
                </div>
                <div class="form-group col-md-2">
                    <label for="closing_year">��� ��������:</label>
                    <input type="text" name="closing_year" id="closing_year" class="form-control" value="<?=$facultyEdit->getClosingYear()?>"/>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="comment">��������:</label>
                    <textarea name="comment" id="comment" class="form-control" rows="3"><?=$facultyEdit->getComment()?></textarea>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <div class="form-check">
                        <input type="checkbox" name="is_active" class="form-check-input" id="is_active" value="1" <?php if($facultyEdit->getIsActive()) { ?>checked="checked"<?php } ?>>
                        <label class="form-check-label" for="is_active">���� �����������</label>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-2">
                    <input type="submit" value="���������" class="button"/>
                </div>
                <div class="form-group col-md-2">
                    <a href="/office/faculty/" class="btn btn-primary btn-sm" role="button" aria-pressed="true">���������</a>
                </div>
            </div>
        </form>
    </div>
</div>
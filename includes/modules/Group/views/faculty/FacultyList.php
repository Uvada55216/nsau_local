<link rel="stylesheet" href="/themes/styles/faculty.css" type="text/css"/>
<div class="col-md-12">
    <div class="row">
        <div class="faculty fac-head">
            <h2>������ �����������:</h2>
            <div class="">
                <a class="btn btn-nsau btn-sm" href="update">�������� ����������</a>
                <a class="btn btn-nsau btn-sm" href="add">�������� ���������</a>
            </div>
        </div>
        <div class="panel-group">
			<?php if(isset($moduleData['faculties']) && count($moduleData['faculties']) > 0){
				foreach($moduleData['faculties'] as $faculty){ ?>
                    <div class="panel panel-default faculty">
                        <div class="panel-heading col-md-9">
                            <h5 class="panel-title"
                                style="<?=!$faculty->getIsActive() ? 'color: gray' : ''?>"
                                title="<?=$faculty->getIsActive() ? $faculty->getComment() : '��������� �� �����������'?>">
                                <?=$faculty->getName()?>
                            </h5>
                        </div>
                        <div class="group_button_form col-md-3">
                            <a class="btn btn-nsau btn-sm" href="edit/<?=$faculty->getId()?>">�������������</a>
                            <a class="btn btn-nsau btn-sm" href="delete/<?=$faculty->getId()?>"
                                onclick="return confirm('������ � ����������� �������� ������� � �������������! �� �������, ��� ������ ������� ������� ���������?');"}>�������</a>
                        </div>
                    </div>
				<?php }
			} ?>
        </div>
    </div>
</div>
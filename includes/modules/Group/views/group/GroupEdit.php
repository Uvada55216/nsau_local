<?php
$facultyEdit = $moduleData['faculty'];
$groupEdit = $moduleData['group'];

if(isset($moduleData['errors'])){
	$error = array(
		'name' => "<small id=\"emailHelp\" class=\"form-text text-muted\" style=\"color: red\">".$moduleData['errors']['name'][0][0]."</small>",
	);
}
?>
<form name="" method="post" id="edit_group">
	<fieldset>
		<legend>�������������� ���������� � ������ <?=$groupEdit->getName()?> (<?=$facultyEdit->getName()?>)</legend>
		<div class="row">
			<div class="col-sm-4">
				<div id="group_name">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon1" style="border-bottom: 0px!important;">�������� ������</span>
						<input type="text" class="form-control" name="name" value="<?=\CF::H($groupEdit->getName())?>" style="border-bottom: 0px!important;"/>
						<?= $error['name'] ? $error['name'] : '' ?>
					</div>
				</div>
				<div id="group_year">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon1">����</span>
						<select name="year" size="1" class="form-control">
							<?php foreach($moduleData['yearArray'] as $key=> $val){
								if($key == $groupEdit->getYear()){ ?>
									<option value="<?=(int)$key?>" selected="selected"><?=$val?></option>
							<?php }	else { ?>
                                    <option value="<?=(int)$key?>"><?=$val?></option>
							<?php }
							} ?>
						</select>
					</div>
				</div>
				<br>
				<div class="checkbox-nsau">
					<input type="checkbox" id="check1" name="hidden" value="1" <?=($groupEdit->getHidden() ? "checked" : "")?>>
					<label for="check1">������</label>
				</div>
			</div>
			<div class="col-sm-8">
				<div id="group_faculties">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon1" style="border-bottom: 0px!important;">�������������, � ������� ��������� ������</span>
						<select name="id_spec" style="max-width: 100%;border-bottom: 0px!important" class="form-control">
							<option value='0'>�� ������� �������������</option>
							<?php foreach($moduleData["faculties"] as $faculty){ ?>
								<optgroup label="<?=$faculty->getName()?>">
									<?php if(count($arrSpecialities = $faculty->getArrSpecialities()) > 0){
	                                    foreach($arrSpecialities as $spec){
		                                    if(($spec->getId() == $groupEdit->getIdSpeciality()) && ($spec->getFacultyId() == $groupEdit->getIdFaculty())){ ?>
                                                <option value="<?=(int)($spec->getId())?>"
                                                        selected="selected"><?=$spec->getCode()?>
                                                    - <?=$spec->getName()?></option>
			                                <?php } else {
			                                    ?>
                                                <option value="<?=(int)($spec->getId())?>"><?=$spec->getCode()?>
                                                    - <?=$spec->getName()?></option>
                                            <?php
		                                    }
	                                    }
									} ?>
								</optgroup>
							<?php } ?>
						</select>
					</div>
				</div>
				<div id="group_qualification">
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon1" style="border-bottom: 0px!important;">������� ���������� ������</span>
						<select name="qualification" style="border-bottom: 0px!important" class="form-control">
							<?php foreach($moduleData['qualif'] as $qual_code => $qualif){
								if($qual_code == $groupEdit->getQualification())
								{
									?>
									<option value="<?=\CF::H($qual_code)?>" selected="selected"><?=$qualif?></option>
									<?php
								}
								else
								{
									?>
									<option value="<?=\CF::H($qual_code)?>"><?=$qualif?></option>
									<?php
								}
							} ?>
						</select>
					</div>
					<div id="form_educat">
						<div class="input-group">
							<span class="input-group-addon" id="basic-addon1">�������� ����� ��������</span>
							<select name="form_education" class="form-control">
								<?php
								if($groupEdit->getFormEducation()==!NULL)
								{
									foreach($moduleData['formEdu'] as $form_code => $form_edu)
									{
										if($form_code == $groupEdit->getFormEducation())
										{
											?>
											<option value="<?=\CF::H($form_code)?>" selected="selected"><?=$form_edu?></option>
											<?php
										}
										else
										{
											?>
											<option value="<?=\CF::H($form_code)?>"><?=$form_edu?></option>
											<?php
										}
									}
								}
								if($groupEdit->getFormEducation()==NULL)
								{
									foreach($moduleData['formEdu'] as $form_code => $form_edu)
									{
										if($form_code == $groupEdit->getFormEducation())
										{
											?>
											<option value="<?=\CF::H($form_code)?>" selected="selected"><?=$form_edu?></option>
											<?php
										}
										else
										{
											?>
											<option value="<?=\CF::H($form_code)?>"><?=$form_edu?></option>
											<?php
										}
									}
								} ?>
							</select>
						</div>
						<div id="form_educat">
							<div class="input-group">
								<span class="input-group-addon" id="basic-addon1">�������� �������</span>
								<select name="profile" class="form-control">
									<?php if (isset($moduleData['profiles']) && count($moduleData['profiles'])> 0){ ?>
                                            <option value="">�������� �������</option>
                                        <?php foreach($moduleData['profiles'] as $v){
											if ($groupEdit->getProfile() == $v->getId()){ ?>
												<option value="<?=(int)($v->getId())?>" selected="selected"><?=$v->getName()?></option>
											<?php }else { ?>
												<option value="<?=(int)($v->getId())?>"><?=$v->getName()?></option>
											<?php
											}
										}
									}else {?>
                                        <option value="">�������� ���</option>
                                    <?php } ?>
								</select>
							</div>
						</div>
					</div>
	</fieldset>
	<input type="submit" value="���������" class="button btn btn-nsau btn-md" />
</form>
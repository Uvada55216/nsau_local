<link rel="stylesheet" href="/themes/styles/group.css" type="text/css"/>
<div class="row">
        <div class="col-sm-5">
    	    <?=$this->renderBlock('blocks/group/AddGroup', $moduleData);?>
        </div>
    <div class="col-sm-7">
        <div class="">
            <div id="Accordian" class="panel-group">
				<?php if(isset($moduleData['faculties']) && count($moduleData['faculties']) > 0){
					foreach($moduleData['faculties'] as $fac){ ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#Accordian" href="#<?=$fac->getId()?>">
                                        <h5 style="<?=!$fac->getIsActive() ? 'color: gray' : ''?>"><?=$fac->getName() ? $fac->getName() : "��������� �� ������"?></h5>
                                    </a>
                                </h4>
                            </div>
                            <div>
                                <!--
                            <span class="btn btn-nsau group_button btn-sm"
                                  data-code ="<?=$fac->getCode1c()?>"
                                  onclick="updateSpecialitiesFaculty('<?=$fac->getCode1c()?>');">�������� �������������
                            </span>
                                -->
                            </div>
                            <div class="panel-collapse collapse out" id="<?=$fac->getId()?>">
                                <div class="panel-body">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>����� �����</th>
                                            <th>������� �����</th>
                                            <th>����-������� �����</th>
                                            <th>��� �����</th>
                                        </tr>
                                        </thead>
                                        <tbody>
											<?php if(count($fac->getArrGroups()) > 0){
												foreach($fac->getArrGroups() as $group){ ?>
                                                     <tr>
                                                     <td>
                                                         <?php if($group->getFormEducation() == 1) { ?>
                                                             <a id="group_link" href="edit/<?=$group->getId()?>/"
                                                                style="color: <?=($group->getHidden() ? "gray" : "")?>"><?=$group->getName()?></a>
                                                             <a class="btn btn-nsau delete_group_button btn-sm"
                                                                href="delete/<?=$group->getId()?>/"
                                                                onclick="return confirm('�� �������, ��� ������ ������� ��������� �������?');"
                                                                }><span class="glyphicon glyphicon-trash"></span></a>
                                                             <!-- [<a href="/student/timetable/groups/<?=$group->getId()?>/">������� � ���������� ������</a>] -->
                                                         <?php } ?>
                                                     </td>
                                                     <td>
                                                         <?php if($group->getFormEducation() == 2) { ?>
                                                             <a id="group_link" href="edit/<?=$group->getId()?>/"
                                                                style="color: <?=($group->getHidden() ? "gray" : "")?>"><?=$group->getName()?></a>
                                                             <a class="btn btn-nsau delete_group_button btn-sm"
                                                                href="delete/<?=$group->getId()?>/"
                                                                onclick="return confirm('�� �������, ��� ������ ������� ��������� �������?');"
                                                                }><span class="glyphicon glyphicon-trash"></span></a>
                                                             <!-- [<a href="/student/timetable/groups/<?=$group->getId()?>/">������� � ���������� ������</a>] -->
                                                         <?php } ?>
                                                     </td>
                                                     <td>
                                                        <?php if($group->getFormEducation() == 3) { ?>
                                                             <a id="group_link" href="edit/<?=$group->getId()?>/"
                                                                style="color: <?=($group->getHidden() ? "gray" : "")?>"><?=$group->getName()?></a>
                                                             <a class="btn btn-nsau delete_group_button btn-sm"
                                                                href="delete/<?=$group->getId()?>/"
                                                                onclick="return confirm('�� �������, ��� ������ ������� ��������� �������?');"
                                                                }><span class="glyphicon glyphicon-trash"></span></a>
                                                             <!-- [<a href="/student/timetable/groups/<?=$group->getId()?>/">������� � ���������� ������</a>] -->
						    	                        <?php } ?>
                                                     </td>
                                                     <td>
                                                         <?php if($group->getFormEducation() !=1 && $group->getFormEducation() !=2 && $group->getFormEducation() !=3) { ?>
                                                         <a id="group_link" href="edit/<?=$group->getId()?>/"
                                                            style="color: <?=($group->getHidden() ? "gray" : "")?>"><?=$group->getName()?></a>
                                                         <a class="btn btn-nsau delete_group_button btn-sm"
                                                            href="delete/<?=$group->getId()?>/"
                                                            onclick="return confirm('�� �������, ��� ������ ������� ��������� �������?');"
                                                            }><span class="glyphicon glyphicon-trash"></span></a>
                                                         <!-- [<a href="/student/timetable/groups/<?=$group->getId()?>/">������� � ���������� ������</a>] -->
                                                     <?php } ?>
                                                     </td>
                                                     </tr>
									            <?php }
											} ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
						<?php
					}
				}
				?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	jQuery(document).ready(function($)
    {
        $("#ajax_add_group").hide();

        $("#new_group").live("keyup", function()
        {
            var group_name = $(this).val();

            if (group_name.length >=4){
                let name = group_name;

                var request = {'route': '<?=$__route?>',
                    'action': 'actionIssetGroup',
                    'module': '<?=$__module?>',
                    'context': '<?=$__context?>',
                    'type': 'json',
                    'params': {name}
                };

                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: '/ajax_mode/',
                    data: request,
                    success: function( data )
                    {
                        if (data)
                        {
                            //������ ��� ����������, ������������ � ������� � ������� ������ �������������
                            $("#new_group").css("background-color", "#80071a");
                            $("#new_group").css("color", "#fff");
                            $(".ajax_edit_group").attr("href", "edit/"+data.id+"/")
                            $(".ajax_edit_group").show();
                            $("#ajax_add_group").hide();

                            //����
                            if (data.year){
                                $('#group_course option[value="'+data.year+'"]').prop('selected', true);
                            } else {
                                $('#group_course option[value="1"]').prop('selected', true);
                            }
                            //����� ��������
                            if (data.form_education){
                                $('#group_form option[value="'+data.form_education+'"]').prop('selected', true);
                            }else {
                                $('#group_form option[value="0"]').prop('selected', true);
                            }
                            //������� ����������
                            if (data.qualification){
                                $('#group_qualif option[value="'+data.qualification+'"]').prop('selected', true);
                            } else {
                                $('#asp').prop('selected', true);
                            }
                            //���� ����������(�������������)
                            if (data.id_spec){
                                $('#napr option[value="'+data.id_spec+'"]').prop('selected', true);
                            } else {
                                $('#option_prev').prop('selected', true);
                            }
                        }
                        else
                        {
                            //������ ���, ������������ � ������� � ��������
                            $("#new_group").css("background-color", "#409c3c");
                            $("#new_group").css("color", "#fff");
                            $(".ajax_edit_group").hide();

                            var first_char = group_name.charAt(0);

                            if (!isNaN(first_char)){
                                if(chekCourse(group_name.charAt(1))){
                                    $('#group_course option[value="'+group_name.charAt(1)+'"]').prop('selected', true);
                                    $("#group_course").css("background-color", "#fff");
                                    $("#group_course").css("color", "#555");
                                    $("#ajax_add_group").show();
                                }else{
                                    $("#group_course").css("background-color", "#80071a");
                                    $("#group_course").css("color", "#fff");
                                    $('#group_course option[value="bad"]').prop('selected', true);
                                }
                            }else{
                                if(chekCourse(group_name.charAt(2))){
                                    $('#group_course option[value="'+group_name.charAt(2)+'"]').prop('selected', true);
                                    $("#group_course").css("background-color", "#fff");
                                    $("#group_course").css("color", "#555");
                                    $("#ajax_add_group").show();
                                }else{
                                    $("#group_course").css("background-color", "#80071a");
                                    $("#group_course").css("color", "#fff");
                                    $('#group_course option[value="bad"]').prop('selected', true);
                                }
                            }

                            $('#group_form option[value="0"]').prop('selected', true);
                            $('#group_qualif option[value="62"]').prop('selected', true);
                            $('#option_prev').prop('selected', true);
                        }
                    }
                });

            }else{
                $("#new_group").css("background-color", "#fff");
                $("#new_group").css("color", "#555");
                $("#group_course").css("background-color", "#fff");
                $("#group_course").css("color", "#555");
                $('#group_course option[value="1"]').prop('selected', true);
                $(".ajax_edit_group").hide();
                $("#ajax_add_group").hide();
            }
        });

        $('#ajax_add_group').on('click', function(){

            let data = {
                'name': $('#new_group').val(),
                'id_spec': $('#napr').val(),
                'form_education': $('#group_form').val(),
                'year': $('#group_course').val(),
                'qualification': $('#group_qualif').val(),
            };

            let request = {'route': '<?=$__route?>',
                'action': 'actionAddGroup',
                'module': '<?=$__module?>',
                'context': '<?=$__context?>',
                'type': 'json',
                'data': data
            };
            $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: '/ajax_mode/',
                    data: request,
                    success: function( data )
                    {
                        if (data.success){
                            $('div.alert-success').show();
                            $("#new_group").val('');
                            $("#new_group").css("background-color", "#fff");
                            $("#new_group").css("color", "#555");
                            $('#group_form option[value="0"]').prop('selected', true);
                            $('#group_qualif option[value="62"]').prop('selected', true);
                            $('#option_prev').prop('selected', true);
                            $('#group_course option[value="1"]').prop('selected', true);
                            setTimeout(function (){
                                $('div.alert-success').hide();
                            }, 5000);
                        }else{
                            $('div.alert-danger').show();
                            setTimeout(function (){
                                $('div.alert-danger').hide();
                            }, 5000);
                        }
                    }
                });
        });

        function chekCourse(curs){
            if(curs >=1 && curs <=6){return true;}
            return false;
        }

    });
</script>
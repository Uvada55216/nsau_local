<?php

namespace modules\HttpFoundation;

class HttpFoundation extends \engine\modules\BaseModule
{
	public function rules() {

		return array(
			array('GET|POST', '/', $this->initParams->mode . 'Controller#actionIndex'),
		);
	} 
}
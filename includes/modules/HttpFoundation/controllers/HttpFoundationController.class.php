<?php

namespace modules\HttpFoundation\controllers;

use engine\Http\HttpFoundation\Response;
use engine\modules\RouteInit;
use engine\Security\Authentication\Token\Storage\TokenStorage;
use engine\Security\Exception\AuthenticationException;
use engine\Security\User\InMemoryUserProvider;
use engine\Http\HttpFoundation\Request;
use security\Authentication\Listener\BasicAuthenticationListener;
use security\Authentication\Provider\HttpBasicProvider;


class HttpFoundationController extends \engine\controllers\BaseController
{
	public function actionIndex(){
		$request = Request::createFromGlobals();
		$response = new Response('', Response::HTTP_FORBIDDEN, array('content-type' => 'application/json'));
		$response->setCharset('ISO-8859-1');
		if($request->getUser() != '' && 'POST' == $request->getMethod()){
			$user1c = require INCLUDES . '/security/Authentication/metadata/user1c.php';

			$userProvider = new InMemoryUserProvider($user1c);
			$provider1c = new HttpBasicProvider($userProvider);
			$tokenStorage = new TokenStorage();
			$listener1c = new BasicAuthenticationListener($tokenStorage, $provider1c);

			try {
				$listener1c($request);

				$content = json_decode($request->getContent(), true);

				if(!empty($content["module"]) && !empty($content["action"])){
					$route = '\\modules\\' . $content["module"] . '\\controllers\\' . $content["module"] . 'Controller';
					$className = '\\modules\\' . $content['module'] . '\\' . $content['module'];
					$Module = new $className(new RouteInit($route, $content["action"], $content["params"]));
				}

			} catch (AuthenticationException $e){
				$response->setContent(json_encode(array('error' => $e->getMessage())));
				$response->send();
//				exit;
			}
		}

		$response->setStatusCode(Response::HTTP_FORBIDDEN);
		$response->headers->set('Content-type', 'text/html');
		$response->send();
		exit;
	}
}
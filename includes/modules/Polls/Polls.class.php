<?php

/**
 * ������ ������ � ������
 */
namespace modules\Polls;

class Polls extends \engine\modules\BaseModule
{
	public function rules(){
		return array(
			array('GET|POST', '/', $this->initParams->mode . 'Controller#actionIndex'),
			array('GET|POST', '[i:id]', $this->initParams->mode .'Controller#actionVote'),
			array('POST', 'reports', 'ReportsController#actionReports'),
		);
	}
}
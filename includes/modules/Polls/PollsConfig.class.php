<?php

namespace modules\Polls;

use \engine\modules\configElements\Select;

class PollsConfig extends \engine\modules\BaseModuleConfig
{
	public $mode;
	private $__mode;


	public function __construct(){
		$this->__mode = array(
			array('id' => 'polls', 'name' => '������ � ������'),
			array('id' => 'config', 'name' => '���������� �������� � ��������'),
			array('id' => 'pollsStudents', 'name' => '������ � ������ (��������)'),
		);
	}

	public function params(){
		return array(
			'����� ������' => array(
				'element' => new Select('mode', $this->mode, $this->__mode),
				'obligatory' => 'true',
				'switcher' => 'true',
				'group' => 'mode',
			)
		);
	}
}
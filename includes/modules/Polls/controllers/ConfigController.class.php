<?php

/**
 * Контроллер управления анкетами и опросами
 */
namespace modules\Polls\controllers;

use modules\Polls\models\PollsModel;
use \requests\Request;
use helpers\EncodingHelper as EncHelp;

class ConfigController extends \engine\controllers\BaseController
{
	/**
	 * @return string    [config view]
	 */
	public function actionIndex(){
		global $Auth;

		$this->renderAjax('css/ConfigStyles'); //
		$this->renderJs('Config');    // js
		$worksheet = PollsModel::getWorksheet();
		$answers = PollsModel::getAnswers();
		$questions = PollsModel::getQuestions();
		$faculties = PollsModel::getFacultyAndGroups();

		return $this->render('Config' . $this->Module->initParams->viewType, array(
			'Auth' => $Auth,
			'worksheet'=> $worksheet,
			'answers'=> $answers,
			'questions'=> $questions,
			'faculties'=> $faculties
		));
	}

	/**
	* @return string    [config view]
	*/
	public function actionDelete(){
		$request = Request::init()->ajax();
		return $this->renderJson(PollsModel::delete((int)$request['id'], strtoupper($request['tbl'])));
	}


	/**
	 * @return string    [config view]
	 */
	public function actionEdit(){
		$request = Request::init()->ajax();
		$out['success'] = 0;

		switch (strtolower($request['tbl'])){
			case PollsModel::ANSWERS :
				if($request['isText'] == true){
					$op = $this->textareaOpt();
				}else{
					$op = array();
					foreach(explode('/', EncHelp::changeArrayEncoding($request['option'], 'w2u')) as $val){
						$op[] = trim($val);
					}
					$op = $this->shiftKey($op);
				}

				if(PollsModel::editAnswers((int)$request['id'], $op)){
					$out['success'] = 1;
				}
				break;

			case PollsModel::QUESTIONS :
				if(PollsModel::editQuestion((int)$request['id'], \CF::H($request['question']), (int)$request['answers'], (int)$request['active'], (int)$request['multiple'], (int)$request['required'])){
					$out['success'] = 1;
				}
				break;

			case PollsModel::WORKSHEET:
				$out = PollsModel::editWorksheet((int)$request['id'], \CF::H($request['prev']), \CF::H($request['text']), (int)$request['pos'], $request['questions'], (int)$request['active'], (int)$request['verdict']);
				break;

			case PollsModel::GROUPS :
				$teachers = (isset($request['teachers']))
					? $request['teachers']
					: null;
				$out = PollsModel::editGroup((int)$request['id'], null, (int)$request['active'], $teachers);
				break;

			default:
				break;
		}

		return $this->renderJson($out);
	}

	/**
	 * @return string    [config view]
	 */
	public function actionAdd(){
		$request = Request::init()->ajax();
		$out = array();

		switch (strtolower($request['tbl'])){
			case PollsModel::ANSWERS:
				if((int)$request['isText'] === 1){
					$op = $this->textareaOpt();
				}else{
					$op = array();
					foreach(explode('/', EncHelp::changeArrayEncoding($request['option'], 'w2u')) as $val){
						$op[] = trim($val);
					}
					$op = $this->shiftKey($op);
				}

				$out = PollsModel::addAnswers($op);
				break;

			case PollsModel::QUESTIONS:
				$out = PollsModel::addQuestion(\CF::H($request['question']), (int)$request['answers'], (int)$request['active'], (int)$request['multiple'], (int)$request['required']);
				break;

			case PollsModel::WORKSHEET:
				$out = PollsModel::addWorksheet(\CF::H($request['prev']), \CF::H($request['text']), (int)$request['pos'], $request['questions'], (int)$request['active'], (int)$request['verdict']);
				break;

			default:
				$out["success"] = 0;
				break;
		}

		return $this->renderJson($out);
	}

	/**
	 * @return string    [config view]
	 */
	public function actionChangeActivity(){
		$request = Request::init()->ajax();
		return $this->renderJson(PollsModel::changeActivity((int)$request['id'], (int)$request['active'], strtoupper($request['tbl'])));
	}

	/**
	 * @return false|string
	 */
	public function changeActivAllGroupsFaculty(){
		$request = str_replace('\\', '', $_REQUEST['data']);
		$data = json_decode($request, true);
		$request = EncHelp::changeArrayEncoding($data, 'u2w');

		return $this->renderJson(PollsModel::changeActivAllGroupsFaculty($request));
	}

	/**
	 * @return false|string
	 */
	public function actionGetGroupById(){
		$request = Request::init()->ajax();
		$group = PollsModel::getGroups((int)$request['id']);
		return $this->renderJson($group[0]['teachers'], 'w2u');
	}

	/**
	 * @return false|string
	 */
	public function actionUpdateGroup(){
		return $this->renderJson(PollsModel::updateGroups());
	}

	/**
	 * @return false|string
	 */
	public function actionSearchTeacher(){
		$request = Request::init()->ajax();
		return $this->renderJson(PollsModel::searchTeacherByName($request['name']), 'w2u');
	}

	/**
	 * @param $arr
	 * @return array
	 */
	protected function shiftKey($arr){
		return array_combine(range(1, count($arr)), $arr);
	}

	protected function textareaOpt(){
		return EncHelp::changeArrayEncoding(array(1=> 'textarea'), 'w2u');
	}
}
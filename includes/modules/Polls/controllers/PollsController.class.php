<?php

namespace modules\Polls\controllers;

use modules\Polls\models\PollsModel;
use \requests\Request;

class PollsController extends \engine\controllers\BaseController
{

	/**
	 * @return string    [config view]
	 */
	public function actionIndex(){
		global $Auth;

		$this->renderAjax('css/PollsStyles'); //
		$arrWorksheet = PollsModel::getWorksheet(null, 0, 1);
		return $this->render('Polls', array(
			'Auth' => $Auth,
			'arrWorksheet'=> $arrWorksheet,
			'heading' => array('people')
		));
	}

	/**
	 * @param $id
	 * @return string    [config view]
	 */
	public function actionVote($id){
		$this->renderAjax('css/PollsStyles'); //
		$this->renderJs('Vote');    // js
		$vote = PollsModel::getWorksheet((int)$id, 0, 1, true);

		return $this->render('Vote', array(
			'vote'=> $vote,
		));
	}

	/**
	 * @return false|string
	 */
	public function actionAddVote(){
		$request = Request::init()->ajax();
		$out = array("success" => 0);

		$request['worksheetId'] = ( !empty($request['worksheetId']) && ((int)$request['worksheetId'] > 0) )? (int)$request['worksheetId'] : false;

		if($request['worksheetId'] && !empty($request['choices'])){
			if(PollsModel::addVote($request['worksheetId'], $request['choices'], null, null, null, \CF::getHttpIP())){
				$out['success'] = 1;
			}
		}

		return $this->renderJson($out);
	}
}
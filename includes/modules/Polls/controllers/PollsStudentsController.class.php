<?php

namespace modules\Polls\controllers;

use modules\Polls\models\PollsModel;
use requests\Request;

class PollsStudentsController extends \engine\controllers\BaseController
{

	/**
	 * @return string    [config view]
	 */
	public function actionIndex(){
		$this->renderAjax('css/PollsStyles'); //
		$arrWorksheet= PollsModel::getWorksheet(null, 1, 1);
		return $this->render('Polls', array(
			'arrWorksheet'=> $arrWorksheet,
			'heading' => array('student')
		));
	}

	/**
	 * @param $id
	 * @return string    [config view]
	 */
	public function actionVote($id){
		global $Auth;

		$this->renderAjax('css/PollsStyles'); //
		$this->renderJs('Vote');    // js
		$vote = PollsModel::getWorksheet((int)$id, 1, 1, true);

		if(!empty($vote) && !empty($vote['questions'])){
			$group = PollsModel::getPollsGroupByPeopleId($Auth->people_id);
			if((int)$group['active'] && (int)$group['hidden'] !=1 && !empty($group['teachers'])){
				$vote['group'] = $group;
			}else{
				$vote = array();
			}
		}else{
			$vote = array();
		}

		return $this->render('Vote', array(
			'vote'=> $vote,
		));
	}

	/**
	 * @return false|string
	 */
	public function actionAddVote(){
		global $Auth;
		$request = Request::init()->ajax();
		$out = array("success" => 0);

		$request['worksheetId'] = ( !empty($request['worksheetId']) && ((int)$request['worksheetId'] > 0) )? (int)$request['worksheetId'] : false;
		$request['groupId'] = ( !empty($request['groupId']) && ((int)$request['groupId'] > 0) )? (int)$request['groupId'] : false;
		$teacherId = (  isset($request['teacher']) && is_array($request['teacher']) && isset($request['teacher']['people_id']))? (int)$request['teacher']['people_id'] : false;

		if($request['worksheetId'] && $request['groupId'] && $request['teacher'] && !empty($request['choices'])){
			if(PollsModel::addVote($request['worksheetId'], $request['choices'], $teacherId, $Auth->people_id, $request['groupId'], \CF::getHttpIP())){
				$out['success'] = 1;
			}
		}

		return $this->renderJson($out);
	}
}
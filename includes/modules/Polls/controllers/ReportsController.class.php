<?php

namespace modules\Polls\controllers;

use engine\PHPOffice\excel\PHPExcel;
use engine\PHPOffice\excel\PHPExcel\Cell;
use engine\PHPOffice\excel\PHPExcel\Style\Alignment;
use engine\PHPOffice\excel\PHPExcel\Style\Border;
use engine\PHPOffice\excel\PHPExcel\Style\Fill;
use engine\PHPOffice\excel\PHPExcel\Writer\WriterExcel5;

use modules\Polls\reports\PollsReports;
use \requests\Request;
use helpers\EncodingHelper as EncHelp;


class ReportsController extends \engine\controllers\BaseController
{
	/**
	 * @return false|string
	 * @throws PHPExcel\Exception
	 * @throws PHPExcel\Writer\WriterException
	 */
	public function actionReports(){
		$request = Request::init()->post();

		if(!empty($request['dateFrom']) && !empty($request['dateOn']) && !empty($request['worksheetId'])){
			$frtFrom = '!Y-m-d';
			$frtOn = 'Y-m-d H:i:s';
			$dtFrom = \DateTime::createFromFormat($frtFrom, $request['dateFrom']);
			$dtOn = \DateTime::createFromFormat($frtOn, $request['dateOn'] . ' 23:59:59');
			$worksheetId = (int)$request['worksheetId'];
			$xls = new PHPExcel();

			//������������� ����� ��� ����
			$xls->getDefaultStyle()->applyFromArray(array(
				'font' => array(
					'name'  => 'Times New Roman',
					'size'  => 12,
					'color'     => array('rgb' => '000000'),
				),
				'alignment' => array(
					'horizontal' 	=> Alignment::HORIZONTAL_CENTER,
					'vertical'   	=> Alignment::VERTICAL_CENTER,
					'rotation'   	=> 0,
					'wrap'       	=> false,
					'shrinkToFit'	=> false,
					'indent'	=> 0
				)

			));

			try {
				$polls = new PollsReports($worksheetId, $dtFrom->format('Y-m-d H:i:s'), $dtOn->format('Y-m-d H:i:s'));

				$border = array(
					'allborders' => array(
						'style' => Border::BORDER_THIN,
						'color' => array(
							'	rgb' => '000000'
						)
					),
					'alignment' => array(
						'horizontal' 	=> Alignment::HORIZONTAL_CENTER,
						'vertical'   	=> Alignment::VERTICAL_CENTER,
						'rotation'   	=> 0,
						'wrap'       	=> false,
						'shrinkToFit'	=> false,
						'indent'	=> 15
					)
				);

				$styleTbl = array(
					'size'     	=> 11,
				);

				$styleHead = array(
					'name'  	=> 'Times New Roman',
					'size'     	=> 14,
					'bold'     	=> true,
				);

				$styleQuestHead = array(
					'rotation'   	=> 90,
					'wrap'       	=> true,
					'indent'	=> 5
				);

				//������ ���������
				if($polls->isPublic()){
					// ������������� ������ ��������� ����� ����
					$xls->setActiveSheetIndex(0);
					// �������� �������� ����
					$sheet = $xls->getActiveSheet();
					// ����������� ����
					$sheet->setTitle(EncHelp::changeArrayEncoding('�����', 'w2u'));
					// ��������� ����� � ������ A1
					$sheet->setCellValue("A1", $polls->getTitle());
					$sheet->getStyle('A1')->getFill()->setFillType(Fill::FILL_SOLID);
					$sheet->getStyle('A1')->getFont()
						->setName('Times New Roman')
						->setSize(14)
						->setBold(true);

					//�������
					$questHead = $polls->getHeadQuestions();
					//���-�� ��������
					$cntQuestHead = count($questHead);
					$sheet->getStyle('A1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
					$sheet->mergeCellsByColumnAndRow(0, 1, $cntQuestHead-1, 1);
					// ��������� ����� � ������ A3
					$sheet->setCellValue("A3", $polls->getTextHadQuestions());
					$sheet->getStyle('A3')->getFill()->setFillType(Fill::FILL_SOLID);
					$sheet->getStyle('A3')->getFont()->applyFromArray($styleTbl);
					$sheet->getStyle('A3')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

					//Array
					//(
					//    [A1:E1] => A1:E1
					//    [A3:E3] => A3:E3
					//)

					// ����������� ����� A3:E3
					$sheet->mergeCellsByColumnAndRow(0, 3, $cntQuestHead-1, 3);
					//�����
					$sheet->getStyleByColumnAndRow(0, 3, $cntQuestHead-1, 3)->getBorders()->applyFromArray($border);
					//����� � ���������
					foreach($polls->getHeadQuestions() as $k=>$v){
						$sheet->setCellValueByColumnAndRow($k, 4, $v);
						// ��������� ������������
						$sheet->getStyleByColumnAndRow($k, 4)->getAlignment()->applyFromArray($styleQuestHead);
						$sheet->getStyleByColumnAndRow($k, 4)->getFont()->applyFromArray($styleTbl);
						$sheet->getStyleByColumnAndRow($k, 4)->getBorders()->applyFromArray($border);
					}
					//������
					foreach($polls->getStatAnswers() as $k=>$v){
						$pVal = (!key_exists('textarea', $v)) ? $polls->mappedImplode(', ', $v) : $v['textarea'];

						$sheet->setCellValueByColumnAndRow($k, 5, $pVal);
						// ��������� ������������
						$sheet->getStyleByColumnAndRow($k, 5)->getAlignment()->applyFromArray(array(
							'horizontal' 	=> Alignment::HORIZONTAL_CENTER,
							'vertical'   	=> Alignment::VERTICAL_CENTER,
							'rotation'   	=> 0,
							'wrap'       	=> true,
							'shrinkToFit'	=> false,
							'indent'	=> 5
						));
						$sheet->getStyleByColumnAndRow($k, 5)->getFont()->applyFromArray($styleTbl);
						$sheet->getStyleByColumnAndRow($k, 5)->getBorders()->applyFromArray($border);
					}
				}elseif($polls->isStudent()){//������ ��� ���������
					// ������������� ������ ��������� �����
					$xls->setActiveSheetIndex(0);
					// �������� �������� ����
					$sheet = $xls->getActiveSheet();

					//
					//������� ��(BD)
					//
					// ����������� ����
					$sheet->setTitle(EncHelp::changeArrayEncoding('��', 'w2u'));
					//����� � ���������
					foreach($polls->getHeadQuestions('base') as $k=>$v){
						$sheet->setCellValueByColumnAndRow($k, 1, $v);
						// ��������� ������������
						$sheet->getStyleByColumnAndRow($k, 1)->getAlignment()->applyFromArray($styleQuestHead);
						$sheet->getStyleByColumnAndRow($k, 1)->getBorders()->applyFromArray($border);
					}
					$rowHead = $sheet->getHighestRow();
					//					$colHead = Cell::columnIndexFromString($sheet->getHighestColumn($rowHead));
					//������
					$data = $d = array();
					foreach($polls->getStatAnswers('bd') as $k=>$v){
						$data[0] = $v['facultyShortName'];//���������

						if(!empty($v['teachers'])){//������, ������
							foreach($v['teachers'] as $key_tch=>$val_tch){
//								$data[1] = (count($val_tch['groups']) > 1) ? implode(', ', $val_tch['groups']): array_shift($val_tch['groups']);
								$data[2] = $val_tch['teacherName'];

								if(!empty($val_tch['choices'])){//������
									foreach($val_tch['choices'] as $key_ch=>$val_ch){
										$data[1] = $val_ch['group'];
//										unset($val_ch['group']);

										$dt = array();
										foreach($val_ch[0] as $value){
											if(is_numeric($value)){
												$dt[] = $value;
											}elseif(is_array($value)){
												if(key($value) == 'textarea'){//���� �����
													$dt[] = $value['textarea'];
												}
											}
										}
										ksort($data, SORT_NUMERIC);
										$d[] = array_merge($data, array_values($dt));
									}
								}
							}
						}
					}
					$sheet->fromArray($d, null, 'A2');
					//�����
					$sheet->getStyleByColumnAndRow(0, $rowHead+1, (Cell::columnIndexFromString($sheet->getHighestColumn($rowHead))-1), $sheet->getHighestRow())->getBorders()->applyFromArray($border);
					$data = $d = array();

					/**
					//������� ����(base)
					//

					// ����������� ����
					$sheet->setTitle(EncHelp::changeArrayEncoding('����', 'w2u'));
					//����� � ���������
					foreach($polls->getHeadQuestions('base') as $k=>$v){
						$sheet->setCellValueByColumnAndRow($k, 1, $v);
						// ��������� ������������
						$sheet->getStyleByColumnAndRow($k, 1)->getAlignment()->applyFromArray($styleQuestHead);
						$sheet->getStyleByColumnAndRow($k, 1)->getBorders()->applyFromArray($border);
					}
					$rowHead = $sheet->getHighestRow();
//					$colHead = Cell::columnIndexFromString($sheet->getHighestColumn($rowHead));
					//������
					$data = $d = array();
					foreach($polls->getStatAnswers('base') as $k=>$v){
						$data[0] = $v['facultyShortName'];//���������

						if(!empty($v['teachers'])){//������, ������
							foreach($v['teachers'] as $key_tch=>$val_tch){
								$data[1] = (count($val_tch['groups']) > 1) ? implode(', ', $val_tch['groups']): array_shift($val_tch['groups']);
								$data[2] = $val_tch['teacherName'];

								if(!empty($val_tch['choices'])){//������
									foreach($val_tch['choices'] as $key_ch=>$val_ch){
										$dt = array();
										foreach($val_ch as $value){
											if(is_numeric($value)){
												$dt[] = $value;
											}elseif(is_array($value)){
												if(key($value) == 'textarea'){//���� �����
													$dt[] = $value['textarea'];
												}
											}
										}
										$d[] = array_merge($data, array_values($dt));
									}
								}
							}
						}
					}
					$sheet->fromArray($d, null, 'A2');
					//�����
					$sheet->getStyleByColumnAndRow(0, $rowHead+1, (Cell::columnIndexFromString($sheet->getHighestColumn($rowHead))-1), $sheet->getHighestRow())->getBorders()->applyFromArray($border);
					$data = $d = array();*/

					//
					//������� �����(report)
					//

					$sheet = $xls->createSheet(1);
					// ����������� ����
					$sheet->setTitle(EncHelp::changeArrayEncoding('�����', 'w2u'));
					//�������� �����
					$questHead = $polls->getHeadQuestions('report');
					$cntQuestHead = count($questHead);
					//��������
					$sheet->setCellValue("A1", $polls->getTitle());
					$sheet->getStyle("A1")->getFill()->setFillType(Fill::FILL_SOLID);
					$sheet->getStyle("A1")->getFont()->applyFromArray($styleHead);
					$sheet->getStyle("A1")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
					$sheet->mergeCellsByColumnAndRow(0, 1, $cntQuestHead-1, 1);
					// ��������� ����� � ������ A3
					$pRowA3 = $sheet->getHighestRow()+1;
					$sheet->setCellValue("A".$pRowA3, $polls->getTextHadQuestions());
					$sheet->getStyle("A".$pRowA3)->getFill()->setFillType(Fill::FILL_SOLID);
					$sheet->getStyle("A".$pRowA3)->getFont()->applyFromArray($styleTbl);
					$sheet->getStyle("A".$pRowA3)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
					// ����������� ����� A4:E3
					$sheet->mergeCellsByColumnAndRow(0, $pRowA3, $cntQuestHead-1, $pRowA3);
					//�����
					$sheet->getStyleByColumnAndRow(0, $pRowA3, $cntQuestHead-1, $pRowA3)->getBorders()->applyFromArray($border);
					//����� � ���������
					$rowHead = $sheet->getHighestRow()+1;
					foreach($questHead as $k=>$v){
						$sheet->setCellValueByColumnAndRow($k, $rowHead, $v);
						// ��������� ������������
						$sheet->getStyleByColumnAndRow($k, $rowHead)->getAlignment()->applyFromArray($styleQuestHead);
						$sheet->getStyleByColumnAndRow($k, $rowHead)->getBorders()->applyFromArray($border);
					}
					//������
					$n = 1;
					$rowReport = $sheet->getHighestRow()+1;

					foreach($polls->getStatAnswers('report') as $k=>$v){
						$facultyShortName = $v['facultyShortName'];//���������
						if(!empty($v['teachers'])){//������
							foreach($v['teachers'] as $key_tch=>$val_tch){
								$data[0] = $n;
								$data[1] = $facultyShortName;
								$data[2] = $val_tch['teacherName'];

								if(!empty($val_tch['choices'])){//������
									$dt = array();

									foreach($val_tch['choices'] as $choices){
										foreach($choices as $k => $ch_){
											if(isset($dt[$k])){
												$dt[$k] += (int)$ch_;
												continue;
											}
											$dt[$k] = (int)$ch_;
										}
									}

									foreach($dt as $sr_key=>$sr_val){//�������
										$dt[$sr_key] = round($sr_val / count($val_tch['choices']),2 );
									}

									$valTeacherSr = array(round(array_sum($dt) / count($dt), 1));
									$d[] = array_merge($data, $dt, $valTeacherSr);
									$dSt[] = array_merge($dt, $valTeacherSr);
								}
								$n++;
							}
						}
					}
					$sheet->fromArray($d, null, 'A'.$rowReport);
					//����� ����
					$cntDSt = count($dSt);
					$cntQuest = count($dSt[0]);
					for($i= 0; $i <=($cntQuest-1); $i++){
						$dStList = array();
						foreach($dSt as $k=>$item){
							if(isset($item[$i])){
								$dStList[] = $item[$i];
							}
						}
						$st[] = round(array_sum($dStList)/$cntDSt, 1);
					}
					// ��������� ����� ����� ����
					$pRowItog = $sheet->getHighestRow()+1;
					$sheet->setCellValue("A".$pRowItog, $polls->getTextHadQuestions('total'));
					$sheet->getStyle("A".$pRowItog)->getFill()->setFillType(Fill::FILL_SOLID);
					$sheet->getStyle("A".$pRowItog)->getFont()->applyFromArray($styleTbl);
					$sheet->getStyle("A".$pRowItog)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
					// ����������� �����
					$sheet->mergeCellsByColumnAndRow(0, $pRowItog, $cntQuestHead-$cntQuest-1, $pRowItog);
					//�����
					$sheet->getStyleByColumnAndRow(0, $pRowItog, $cntQuestHead-$cntQuest-1, $pRowItog)->getBorders()->applyFromArray($border);
					$pColItog = Cell::stringFromColumnIndex(Cell::columnIndexFromString($sheet->getHighestColumn($pRowItog)));
					$sheet->fromArray($st, null, $pColItog.$pRowItog);
					//�����
					$sheet->getStyleByColumnAndRow(0, $rowHead+1, (Cell::columnIndexFromString($sheet->getHighestColumn($rowHead))-1), $sheet->getHighestRow())->getBorders()->applyFromArray($border);
					$data = $d = $dSt = $st = array();
					//
					// ��������� ����� ���-�� ���������� ���������
					$pRowCntStud = $sheet->getHighestRow()+1;
					$sheet->setCellValue("A".$pRowCntStud, $polls->getTextHadQuestions('students'));
					$sheet->getStyle("A".$pRowCntStud)->getFill()->setFillType(Fill::FILL_SOLID);
					$sheet->getStyle("A".$pRowCntStud)->getFont()->applyFromArray($styleTbl);
					$sheet->getStyle("A".$pRowCntStud)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
					// ����������� �����
					$sheet->mergeCellsByColumnAndRow(0, $pRowCntStud, $cntQuestHead-$cntQuest-1, $pRowCntStud);
					//�����
					$sheet->getStyleByColumnAndRow(0, $pRowCntStud, $cntQuestHead-$cntQuest-1, $pRowCntStud)->getBorders()->applyFromArray($border);
					$pColCntStud = Cell::stringFromColumnIndex(Cell::columnIndexFromString($sheet->getHighestColumn($pRowCntStud)));
					$sheet->fromArray(array($polls->getCountStudents()), null, $pColCntStud.$pRowCntStud);
					$sheet->getStyleByColumnAndRow(3, $pRowCntStud, 3, $pRowCntStud)->getBorders()->applyFromArray($border);
					//
					// ��������� ����� ���-�� ���������� ���������
					$studByGroupList = $polls->getCountStudentsByGroup();
					foreach($studByGroupList as $item){
						$d = array();
						$pRowCntStud = $sheet->getHighestRow()+1;
						$sheet->setCellValue("A".$pRowCntStud, EncHelp::changeArrayEncoding($item[0], 'w2u'));
						$sheet->getStyle("A".$pRowCntStud)->getFill()->setFillType(Fill::FILL_SOLID);
						$sheet->getStyle("A".$pRowCntStud)->getFont()->applyFromArray($styleTbl);
						$sheet->getStyle("A".$pRowCntStud)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
						// ����������� �����
						$sheet->mergeCellsByColumnAndRow(0, $pRowCntStud, $cntQuestHead-$cntQuest-1, $pRowCntStud);
						//�����
						$sheet->getStyleByColumnAndRow(0, $pRowCntStud, $cntQuestHead-$cntQuest-1, $pRowCntStud)->getBorders()->applyFromArray($border);
						$pColCntStud = Cell::stringFromColumnIndex(Cell::columnIndexFromString($sheet->getHighestColumn($pRowCntStud)));

						foreach($item as $ik=>$iel){
							if($ik === 0) continue;
							$d[] = $ik.' = '.$iel;
						}
						$sheet->fromArray($d, null, $pColCntStud.$pRowCntStud);
						$sheet->getStyleByColumnAndRow(0, $pRowCntStud, (Cell::columnIndexFromString($sheet->getHighestColumn($pRowCntStud))-1), $sheet->getHighestRow())->getBorders()->applyFromArray($border);

					}
					$data = $d = $dSt = $st = array();

					//
					//������� ������(comment)
					//

					$sheet = $xls->createSheet(2);
					// ����������� ����
					$sheet->setTitle(EncHelp::changeArrayEncoding('������', 'w2u'));
					//�������� �����
					$questHead = $polls->getHeadQuestions('comment');
					$cntQuestHead = count($questHead);
					//��������
					$sheet->setCellValue("A1", $polls->getTextHadQuestions('comment'));
					$sheet->getStyle("A1")->getFill()->setFillType(Fill::FILL_SOLID);
					$sheet->getStyle("A1")->getFont()->applyFromArray($styleHead);
					$sheet->getStyle("A1")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
					$sheet->mergeCellsByColumnAndRow(0, 1, $cntQuestHead-1, 1);
					//�����
					$pRowComments = $sheet->getHighestRow()+1;
					foreach($questHead as $k=>$v){
						$sheet->setCellValueByColumnAndRow($k, $pRowComments, $v);
						// ��������� ������������
						$sheet->getStyleByColumnAndRow($k, $pRowComments)->getFill()->setFillType(Fill::FILL_SOLID);
						$sheet->getStyleByColumnAndRow($k, $pRowComments)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
						$sheet->getStyleByColumnAndRow($k, $pRowComments)->getBorders()->applyFromArray($border);
					}
					//�������
					$pRowComments = $sheet->getHighestRow()+1;
					foreach($polls->getStatAnswers('comment') as $k=>$v){
						$facultyShortName = $v['facultyShortName'];//���������
						if(!empty($v['teachers'])){//������
							foreach($v['teachers'] as $key_tch=>$val_tch){
								$data[] = $facultyShortName;
								$data[] = $val_tch['teacherName'];
								if(!empty($val_tch['choices'])){//������
									foreach($val_tch['choices'] as $k_ch=>$val_ch){
										$ch[] = $val_ch['textarea'];
									}
								}
								$d[] = array_merge($data, $ch);
								$ch = $data= array();
							}
						}
					}
					$sheet->fromArray($d, null, 'A'.$pRowComments);
					//�����
					$sheet->getStyleByColumnAndRow(0, $pRowComments, (Cell::columnIndexFromString($sheet->getHighestColumn($pRowComments))-1), $sheet->getHighestRow())->getBorders()->applyFromArray($border);
					$data = $d = $dSt = $st = array();

				}
			} catch (\Exception $e){
				echo $e->getMessage();
			}

			// ������� HTTP-���������
			header('Content-Type: text/html; charset=windows-1251');
			header('P3P: CP="NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM"');
			header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
			header('Cache-Control: no-store, no-cache, must-revalidate');
			header('Cache-Control: post-check=0, pre-check=0', false);
			header('Pragma: no-cache');
			header('Content-transfer-encoding: binary');
			header("Content-Disposition: attachment; filename=" . $worksheetId . "_" . $dtFrom->format('Y-m-d') . "_" .$dtOn->format('Y-m-d') . ".xls");
			header('Content-Type: application/x-unknown');

			// ������� ���������� �����
			$objWriter = new WriterExcel5($xls);
			$objWriter->save('php://output');
		}
		exit();
	}

}
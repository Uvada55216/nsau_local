<?php

namespace modules\Polls\models;

use \engine\db\Db;
use helpers\EncodingHelper;
use repository\faculty\FacultyRepository;
use repository\group\GroupRepository;

class PollsModel
{
	/**
	 * �������� ���� ��
	 */
	const VOTES = 'votes';
	const WORKSHEET = 'worksheet';
	const QUESTIONS = 'questions';
	const ANSWERS = 'answers';
	const GROUPS = 'groups';
	const PEOPLE = 'people';
	const FACULTIES = 'nsau_faculties';
	const NSAU_GROUPS = 'nsau_groups';

	/**
	 * @param $id
	 * @param null $verdict
	 * @param $active
	 * @param bool $questions
	 * @return array
	 */
	public static function getWorksheet($id = null, $verdict = null, $active = null, $questions = false){
		$query = Db::init()->SetTable('nsau_polls_'.self::WORKSHEET, 'v');
		$result = array();

		if(!is_null($id)){
			$query->addCondFs('id', '=', $id);
		}
		if(!is_null($active)){
			$active = (int)$active;
			if(is_int($active) && ($active >=0 && $active <=1)){
				$query->addCondFs('v.active', '=', $active);
			}
		}
		if(!is_null($verdict)){
			$verdict = (int)$verdict;
			if(is_int($verdict) && ($verdict >=0 && $verdict <=1)){
				$query->addCondFs('v.verdict', '=', $verdict);
			}
		}

		$result = $query->AddField('v.id', 'id')
			->AddField('v.prev', 'prev')
			->AddField('v.text', 'text')
			->AddField('v.verdict', 'verdict')
			->AddField('v.user_id', 'user')
			->AddField('v.active', 'active')
			->AddField('v.pos', 'pos')
			->AddField('v.questions', 'questions')
			->AddField('v.date', 'date')
			->AddOrder('v.pos')
			->SelectArray();

		if ($result) {
			self::jsonDecoder($result, 'questions');
		}
		if($questions && !empty($result)){
			$q = self::getQuestions(null, 1, true);
			self::mergeByID($result, 'questions', $q);
			self::sortQuestion($result);
		}
		return (!is_null($id)) ? $result[0] : $result;
	}

	/**
	 * @param $id
	 * @return array
	 */
	public static function getAnswers($id = null){
		$query = Db::init()->SetTable('nsau_polls_'.self::ANSWERS, 'a');
		$result = array();

		if(!is_null($id)){
			$query->addCondFs('id', '=', $id);
		}

		$result = $query->AddField('a.id', 'id')
			->AddField('a.option', 'option')
			->AddField('a.user_id', 'user')
			->AddField('a.date', 'date')
			->SelectArray();

		if ($result) {
			self::jsonDecoder($result, 'option');
			return $result;
		}
		return $result;
	}

	/**
	 * @param $id
	 * @param null $active
	 * @param bool $answers
	 * @return array
	 */
	public static function getQuestions($id = null, $active = null, $answers=false){
		$query = Db::init()->SetTable('nsau_polls_'.self::QUESTIONS, 'q');
		$result = array();

		if(!is_null($id)){
			if(!is_array($id)){
				$query->addCondFs('id', '=', $id);
			}
			if(is_array($id)){
				$query->AddCondFO('q.id', " IN (".implode(",", $id).")");
			}
		}

		if(!is_null($active)){
			$active = (int)$active;
			if(is_int($active) && ($active >=0 && $active <=1)){
				$query->addCondFs('v.active', '=', $active);
			}
		}

		$result = $query->AddField('q.id', 'id')
			->AddField('q.question', 'question')
			->AddField('q.active', 'active')
			->AddField('q.required', 'required')
			->AddField('q.multiple', 'multiple')
			->AddField('q.answer_id', 'answer')
			->AddField('q.user_id', 'user')
			->AddField('q.date', 'date')
			->SelectArray();

		if($answers && !empty($result)){
			$ans = self::getAnswers();

			foreach($result as $key=>$q){
				if(!is_null($q['answer'])){
					foreach($ans as $a){
						if((int)$q['answer'] === (int)$a['id']){
							$result[$key]['answer'] = $a;
						}
					}
				}else{//���� ��� �������, ������� ������
					unset($result[$key]);
					continue;
				}
			}

		}

		if ($result) {
			return $result;
		}
		return $result;
	}

	/**
	 * @param $id
	 * @return array
	 */
	public static function getGroups($id = null){
		$result = array();
		$query = Db::init()
			->SetTable('nsau_polls_'.self::GROUPS, 'g')
			->AddTable('nsau_groups', 'gr')
			->AddCondFF("gr.id", "=", "g.group_id")
			->AddTable('auth_users', 'au')
			->AddCondFF('au.id', '=', 'g.user_id');

		if(!is_null($id)){
			$query->addCondFs('g.id', '=', $id);
		}

		$result = $query->AddField('g.id', 'id')
			->AddField('g.active', 'active')
			->AddField('g.teachers', 'teachers')
			->AddField('g.user_id', 'user_id')
			->AddField('au.displayed_name', 'username')
			->AddField('g.group_id', 'group_id')
			->AddField('gr.name', 'group_name')
			->AddField('gr.id_faculty', 'id_faculty')
			->AddField('gr.form_education', 'form_education')
			->AddField('gr.hidden', 'hidden')
			->AddField('g.date', 'date')
			->SelectArray();

		if ($result) {
			if(!is_null($result[0]['teachers'])){
				$result[0]['teachers'] = self::getPeopleByArrId(json_decode($result[0]['teachers'], true));
			}
		}

		return $result;
	}

	/**
	 * @param null $workId
	 * @param null $dateFrom
	 * @param null $dateOn
	 * @return array
	 */
	public static function getVotes($workId = null, $dateFrom = null, $dateOn = null, $teacher = false, $group = false){
		$query = Db::init()->SetTable('nsau_polls_'.self::VOTES, 'v')
			->AddField('v.worksheet_id', 'worksheetId')
			->AddField('v.student_id', 'studentId')
			->AddField('v.teacher_id', 'teacherId')
			->AddField('v.poll_group_id', 'pollGroupId')
			->AddField('v.choices', 'choices')
			->AddField('v.ip', 'ip')
			->AddField('v.date', 'date');

		if($group){
			$query
				->AddTable('nsau_polls_'.self::GROUPS, 'npgr')
				->AddCondFF("npgr.id", "=", "v.poll_group_id")
				->AddTable('nsau_groups', 'ngr')
				->AddCondFF("ngr.id", "=", "npgr.group_id")
				->AddTable('nsau_faculties', 'f')
				->AddCondFF("f.id", "=", "ngr.id_faculty")
				->AddField('npgr.group_id', 'groupId')
				->AddField('ngr.name', 'groupName')
				->AddField('ngr.id_faculty', 'facultyId')
				->AddField('f.name', 'facultyName')
				->AddField('f.short_name', 'facultyShortName');
		}

		if($teacher){
			$query
				->AddTable('nsau_' . self::PEOPLE, 'p')
				->AddCondFF('p.id',  '=', 'v.teacher_id')
				->AddCondFO('p.people_cat', " NOT IN( 1,3 ) ")
				//				->AddCondFF('p.people_cat', "=", 2)
				->AddCondFO('p.status_id', " NOT IN( 9 ) ")
				//				->AddCondFF('p.status_id', "=", 2)
				->AddField('p.last_name', 'teacherLastName')
				->AddField('p.name', 'teacherName')
				->AddField('p.patronymic', 'teacherPatronymic');
		}

		if(!is_null($workId))$query->AddCondFS('v.worksheet_id', '=', $workId);
		if(!is_null($dateFrom))	$query->AddCondFS('v.date', '>=', $dateFrom);
		if(!is_null($dateOn)) $query->AddCondFS('v.date', '<=', $dateOn);

		if ($result = $query->SelectArray()) {
			foreach($result as $k=>$row){
				if($row['choices'] != ''){
					$result[$k]['choices'] = EncodingHelper::changeArrayEncoding($row['choices'], 'w2u');
				}
			}
			self::jsonDecoder($result, 'choices', true);
			return $result;
		}
		return array();
	}

	/**
	 * �������� ������ ��������������� �� �����������
	 * � ��������������� �� ��. ������ �����
	 * @return array|null
	 */
	public static function getFacultyAndGroups(){
		$arrFacAndGroups = self::getAssocActiveFaculties();
		if(count($arrFacAndGroups) > 0){
			//������ ��� �����
			$arrGroupsPolls = self::getGroups();

			foreach($arrFacAndGroups as $key => $val){
				foreach($arrGroupsPolls as $k => $gr){
					if((int)$val['id'] === (int)$gr['id_faculty']){
						$arrFacAndGroups[$key]['arr_groups'][] = $gr;
						unset($arrGroupsPolls[$key]);
						continue;
					}
				}
			}

			//cs ���� ���� ���� ��������������� ��� ������ ����������
			self::checkActiveGroups($arrFacAndGroups);
		}
		return $arrFacAndGroups;
	}

	/**
	 * �������� �������� ���������� � ���� �������������� �������
	 * @param null $id
	 * @return array|null
	 */
	public static function getAssocActiveFaculties($id = null){
		$arr = self::getAssocFaculties($id);

		$res = array();
		if(!is_null($arr)){
			foreach($arr as $key=>$fac){
				if($fac['is_active'] != 0){
					$res[] = $arr[$key];
					continue;
				}
			}
		}

		return $res;
	}


	/**
	 * �������� ���������� � ���� �������������� �������
	 * @param null $id
	 * @return array|null
	 */
	public static function getAssocFaculties($id = null){
		$faculties = Db::init()->SetTable(self::FACULTIES)
			->AddField('id')
			->AddField('code1c')
			->AddField('pid')
			->AddField('pos')
			->AddField('is_active')
			->AddField('name')
			->AddField('short_name')
			->AddField('group_num')
			->AddField('groups')
			->AddField('link')
			->AddField('foundation_year')
			->AddField('closing_year')
			->AddField('comment')
			->AddOrder('id')
		;

		if(!is_null($id)){
			$faculties->AddCondFS('id', '=', (int)$id);
		}
		$arrResult = $faculties->SelectArray();

		if($arrResult){
			return $arrResult;
		}else{
			return null;
		}
	}

	/**
	 * @param $rs
	 */
	protected static function checkActiveGroups(&$rs){
		foreach($rs as $k=>$f){
			foreach($f['arr_groups'] as $ky=>$item){
				$activeList[] = $item['active'];
			}

			$rs[$k]['deactivation'] = ((int)array_sum($activeList) === 0)? 0: 1;
			$activeList = array();
		}
	}

	/**
	 * @param $id
	 * @param $active
	 * @param $tbl
	 * @return int
	 */
	public static function changeActivity($id, $active, $tbl){
		global $Auth;
		$out["success"] = 0;
		$const = constant("self::{$tbl}");

		if(!is_null($const)){
			if(Db::init()
				->SetTable('nsau_polls_' . $const)
				->addCondFs('id', '=', $id)
				->AddValue('active', $active)
				->AddValue('user_id', $Auth->user_id)
				->AddValue('date', "NOW()", "X")
				->Update())
			{
				$out["success"] = 1;
			}
		}

		return $out;
	}

	/**
	 * @param $id
	 * @return resource
	 */
	public static function delete($id, $tbl){
		$out["success"] = 0;
		$const = constant("self::{$tbl}");
		if(!is_null($const)){
			if(Db::init()
				->SetTable('nsau_polls_' . $const)
				->addCondFs('id', '=', $id)
				->Delete())
			{
				$out["success"] = 1;
			}
		}

		return $out;
	}

	/**
	 * @param $id
	 * @param $op
	 * @return resource
	 */
	public static function editAnswers($id, $op){
		global $Auth;

		return $result = Db::init()
			->SetTable('nsau_polls_'.self::ANSWERS)
			->addCondFs('id', '=', $id)
			->AddValue('option', self::jsonEncoder($op))
			->AddValue('user_id', $Auth->user_id)
			->AddValue('date', "NOW()", "X")
			->Update();
	}

	/**
	 * @param $op
	 * @return array
	 */
	public static function addAnswers($op){
		global $Auth;

		if(Db::init()
			->SetTable('nsau_polls_'.self::ANSWERS)
			->AddValue('option', self::jsonEncoder($op))
			->AddValue('user_id', $Auth->user_id)
			->AddValue('date', "NOW()", "X")
			->Insert())
		{	return array("success" => 1);
		}else {
			return array("success" => 0);
		}
	}

	/**
	 * @param $question
	 * @param $answers
	 * @param $active
	 * @param $required
	 * @param $multiple
	 * @return array
	 */
	public static function addQuestion($question, $answers, $active, $multiple, $required){
		global $Auth;

		if(Db::init()
			->SetTable('nsau_polls_'.self::QUESTIONS)
			->AddValue('question', $question)
			->AddValue('answer_id', $answers)
			->AddValue('active', $active)
			->AddValue('required', $required)
			->AddValue('multiple', $multiple)
			->AddValue('user_id', $Auth->user_id)
			->AddValue('date', "NOW()", "X")
			->Insert())
		{
			return array("success" => 1);
		}else {
			return array("success" => 0);
		}
	}

	/**
	 * @param $id
	 * @param $question
	 * @param $answers
	 * @param $active
	 * @param $required
	 * @param $multiple
	 * @return resource
	 */
	public static function editQuestion($id, $question, $answers, $active, $multiple, $required){
		global $Auth;

		return $result = Db::init()
			->SetTable('nsau_polls_'.self::QUESTIONS)
			->addCondFs('id', '=', $id)
			->AddValue('question', $question)
			->AddValue('answer_id', $answers)
			->AddValue('active', $active)
			->AddValue('required', $required)
			->AddValue('multiple', $multiple)
			->AddValue('user_id', $Auth->user_id)
			->AddValue('date', "NOW()", "X")
			->Update();
	}

	/**
	 * @param $id
	 * @param $prev
	 * @param $text
	 * @param $pos
	 * @param $questions
	 * @param $active
	 * @param $verdict
	 * @return array
	 */
	public static function editWorksheet($id, $prev, $text, $pos, $questions, $active, $verdict){
		global $Auth;

		if(Db::init()
			->SetTable('nsau_polls_'.self::WORKSHEET)
			->addCondFs('id', '=', $id)
			->AddValue('prev', $prev)
			->AddValue('verdict', $verdict)
			->AddValue('text', $text)
			->AddValue('user_id', $Auth->user_id)
			->AddValue('active', $active)
			->AddValue('pos', $pos)
			->AddValue('questions', $questions)
			->AddValue('date', "NOW()", "X")
			->Update())
		{
			return array("success" => 1);
		}else{
			return array("success" => 0);
		}
	}

	/**
	 * @param $prev
	 * @param $text
	 * @param $pos
	 * @param $questions
	 * @param $active
	 * @param $verdict
	 * @return array
	 */
	public static function addWorksheet($prev, $text, $pos, $questions, $active, $verdict){
		global $Auth;

		if(Db::init()
			->SetTable('nsau_polls_'.self::WORKSHEET)
			->AddValue('prev', $prev)
			->AddValue('text', $text)
			->AddValue('user_id', $Auth->user_id)
			->AddValue('verdict', $verdict)
			->AddValue('active', $active)
			->AddValue('pos', $pos)
			->AddValue('questions', $questions)
			->AddValue('date', "NOW()", "X")
			->Insert())
		{
			return array("success" => 1);
		}else{
			return array("success" => 0);
		}
	}

	/**
	 * @param $groupID
	 * @param int $active
	 * @param null $teachers
	 * @return resource
	 */
	public static function addGroup($groupID, $active = 0, $teachers = null){
		global $Auth;

		return Db::init()
			->SetTable('nsau_polls_'.self::GROUPS)
			->AddValue('active', $active)
			->AddValue('teachers', $teachers)
			->AddValue('user_id', $Auth->user_id)
			->AddValue('date', "NOW()", "X")
			->AddValue('group_id', $groupID)
			->Insert();
	}

	/**
	 * @param $id
	 * @param $groupID
	 * @param int $active
	 * @param null $teachers
	 * @return array
	 */
	public static function editGroup($id, $groupID = null, $active = 0, $teachers = null){
		global $Auth;

		$query = Db::init()
			->SetTable('nsau_polls_'.self::GROUPS)
			->addCondFs('id', '=', $id)
			->AddValue('active', $active)
			->AddValue('user_id', $Auth->user_id)
			->AddValue('date', "NOW()", "X");

		if(!is_null($teachers)){
			$query->AddValue('teachers', self::jsonEncoder($teachers));
		}elseif(is_null($teachers)){
			$query->AddValue('teachers', null);
		}

		if(!is_null($groupID)){
			$query->AddValue('group_id', $groupID);
		}

		if($query->Update()){
			return array("success" => 1);
		}else{
			return array("success" => 0);
		}
	}

	/**
	 * @param null $facID
	 * @return array
	 */
	public static function updateGroups($facID = null){
		//������ � �������� ����
		$arrGroups = self::getAssocGroups();

		$groupsList = array();
		if(!is_null($arrGroups)){
			foreach($arrGroups as $key=>$fac){
				if($fac['id_faculty'] != 0){
					$groupsList[] = $arrGroups[$key];
					continue;
				}
			}
		}

		$arrGroups = self::changeKey($groupsList);

		//������ ��� �����
		$arrGroupsPolls = self::getGroups();
		if (!empty($arrGroupsPolls)){
			$arrGroupsPolls = self::changeKey(self::getGroups(), 'group_id');

			foreach($arrGroups as $key=>$group){
				if(array_key_exists($key, $arrGroupsPolls)){//update
					unset($arrGroupsPolls[$key]);
				}else{//insert
					self::addGroup($group['id'], 0, null);
				}
			}

			if(!empty($arrGroupsPolls)){//delete
				foreach($arrGroupsPolls as $groupsPoll){
					self::delete($groupsPoll['id'], 'GROUPS');
				}
			}
		}else{
			foreach($arrGroups as $group){
				self::addGroup($group['id'],0, null);
			}
		}

		return array("success" => 1);
	}

	/**
	 * �������� ������ � ���� �������������� �������
	 */
	public static function getAssocGroups($id = null){
		$groups = Db::init()->SetTable(self::NSAU_GROUPS)
			->AddField('id')
			->AddField('code1c')
			->AddField('name')
			->AddField('id_faculty')
			->AddField('specialities_code')
			->AddField('form_education')
			->AddField('id_spec')
			->AddField('year')
			->AddField('hidden')
			->AddField('profile')
			->AddField('qualification')
			->AddOrder('id')
		;

		if(!is_null($id)){
			$groups->AddCondFS('id', '=', (int)$id);
		}

		$arrResult = $groups->SelectArray();

		if($arrResult){
			return $arrResult;
		}else{
			return null;
		}
	}


	/**
	 * @param $name
	 * @return array
	 */
	public static function searchTeacherByName($name) {
		$result = array();
		$query = Db::init()
			->SetTable("nsau_" . self::PEOPLE, 'p')
			->AddCondFS("p.last_name", "LIKE", "". $name ."%")
			->AddCondFO('p.status_id', " NOT IN( 9 ) ")
			->AddCondFO('p.people_cat', " NOT IN( 1,3 ) ")
			->AddFields(array('p.last_name', 'p.name', 'p.patronymic', 'p.id'))
			->AddOrder("p.last_name")
			->SelectArray(50);

		foreach($query as $row){
			$row['name'] = $row['last_name'] . ' ' . $row['name'] . ' ' . $row['patronymic'];
			unset($row['last_name'], $row['patronymic']);
			if(!empty($row['id'])) {
				$result[$row['id']] = $row;
			}
		}

		return $result;
	}

	/**
	 * @param null $people_id
	 * @return array
	 */
	public static function getPollsGroupByPeopleId($people_id = null){
		$query = Db::init()
			->SetTable('nsau_' . self::PEOPLE, 'p')
			->AddCondFO('p.people_cat', " NOT IN( 2,3 ) ")
			->AddCondFO('p.status_id', " NOT IN( 9 ) ")
			->AddTable('nsau_groups', 'gr')
			->AddCondFF("gr.id", "=", "p.id_group")
			->AddTable('nsau_polls_' . self::GROUPS, 'grp')
			->AddCondFF("grp.group_id", "=", "p.id_group");


		if(!is_null($people_id)){
			$query->addCondFS('p.id', '=', $people_id);
		}

		$result = $query->AddField('grp.id', 'id')
			->AddField('grp.active', 'active')
			->AddField('grp.teachers', 'teachers')
			->AddField('grp.group_id', 'group_id')
			->AddField('gr.name', 'group_name')
			->AddField('gr.id_faculty', 'id_faculty')
			->AddField('gr.form_education', 'form_education')
			->AddField('gr.hidden', 'hidden')
			->SelectArray();

		if ($result) {
			if(!is_null($result[0]['teachers'])){
				$result[0]['teachers'] = self::getPeopleByArrId(json_decode($result[0]['teachers'], true));
			}

			return (!is_null($people_id)) ? $result[0] : $result;
		}

		return array();
	}

	/**
	 * @param $worksheetId
	 * @param $choices
	 * @param null $teacherId
	 * @param null $studentId
	 * @param null $pollGroupId
	 * @param $ip
	 * @return bool
	 */
	public static function addVote($worksheetId, $choices, $teacherId = null, $studentId = null, $pollGroupId = null, $ip = null){
		$query = Db::init()->SetTable('nsau_polls_'.self::VOTES)
			->AddValue('student_id', $studentId)
			->AddValue('worksheet_id', $worksheetId)
			->AddValue('teacher_id', $teacherId)
			->AddValue('poll_group_id', $pollGroupId)
			->AddValue('choices', $choices)
			->AddValue('ip', $ip)
			->AddValue('date', "NOW()", "X");

		if($query->Insert()){
			return  true;
		}else{
			return false;
		}
	}

	/**
	 * @param $arr
	 * @param $key
	 * @param bool $encod
	 */
	protected static function jsonDecoder(&$arr, $key, $encod = true){
		foreach($arr as $k=>$v){
			$arr[$k][$key] = ($encod)
				? EncodingHelper::changeArrayEncoding(json_decode($arr[$k][$key], true), 'u2w')
				: json_decode($arr[$k][$key], true);
		}
	}

	/**
	 * @param $json
	 * @return false|string
	 */
	public static function jsonEncoder($json){
		//		if (get_magic_quotes_gpc()) {
		//			return quotemeta(json_encode($json));
		//		} else {
		return json_encode($json);
		//		}
	}

	/**
	 * @param $arr
	 * @param string $key
	 * @return array
	 */
	protected static function changeKey($arr, $key = 'id'){
		$result = array();
		foreach($arr as $k=>$v){
			if(!array_key_exists($v[$key], $result)){
				$result[$v[$key]] = $v;
			}
		}

		return $result;
	}

	/**
	 * @param $request
	 * @return mixed
	 */
	public static function changeActivAllGroupsFaculty($request){
		global $Auth;
		$rs = array();
		if(!empty($request['id_groups']) && isset($request['active'])){
			foreach($request['id_groups'] as $k => $id){
				if(!empty($id) && 0 !== $id){
					$query = Db::init()
						->SetTable('nsau_polls_'.self::GROUPS)
						->addCondFs('id', '=', (int)$id)
						->AddValue('active', (int)$request['active'])
						->AddValue('user_id', $Auth->user_id)
						->AddValue('date', "NOW()", "X");
					if($query->Update()){
						$rs["success"] = 1;
					}else{
						$rs["success"] = 0;
					}
				}
			}
		}

		return $rs;
	}

	/**
	 * @return array
	 */
	protected static function getNotHiddenGroups(){
		return array_filter(self::getGroups(), function($el){
			return ($el['hidden'] != 1);
		}, ARRAY_FILTER_USE_BOTH);
	}

	/**
	 * @param $result
	 * @param $field
	 * @param $arrMerge
	 */
	protected static function mergeByID(&$result, $field, $arrMerge){
		$arrMerge = self::setHashKey($arrMerge, 'id');
		foreach($result as $k=>$rs){
			if(!empty($rs[$field])){
				foreach($rs[$field] as $key => $r){
					$mdID = md5($r['id']);
					if(isset($arrMerge[$mdID])){
						$result[$k][$field][$key]['id'] = $arrMerge[$mdID];
					} else {//���� ���, �������
						unset($result[$k][$field][$key]);
					}
				}
			}
		}
	}

	/**
	 * @param $arr
	 * @param $field
	 * @return mixed
	 */
	protected static function setHashKey($arr, $field){
		if(count($arr) > 0){
			foreach($arr as $key=>$v){
				$arr[md5($v[$field])] = $v;
				unset($arr[$key]);
			}
		}
		return $arr;
	}

	/**
	 * @param $result
	 * @return array $result
	 */
	protected static function sortQuestion(&$result){
		foreach($result[0]['questions'] as $ky=>$item){
			$pos[] = $item['pos'];
		}
		array_multisort($pos, SORT_ASC, $result[0]['questions']);
	}

	/**
	 * @param $values
	 * @return array
	 */
	private static function getPeopleByArrId($values){
		$result = array();
		if(is_array($values) && !empty($values)){
			$query = Db::init()->SetTable("nsau_" . self::PEOPLE, 'p')
				->AddCondFO("p.id", "IN(". implode(",", $values) .")")
				->AddCondFO('p.status_id', " NOT IN( 9 ) ")
				->AddCondFO('p.people_cat', " NOT IN( 1,3 ) ")
				->AddFields(array('p.last_name', 'p.name', 'p.patronymic', 'p.id'))
				->AddOrder('p.id')
				->SelectArray();

			foreach($query as $row){
				if(!empty($row['id'])){
					$result[] = array(
						'people_id' => $row['id'],
						'name' => $row['last_name'] . ' ' . $row['name'] . ' ' . $row['patronymic']
					);
				}
			}
		}

		return $result;
	}
}
<?php

namespace modules\Polls\reports;

use SplObserver;

abstract class AbstractPolls
{
	const P1 = '�';
	const P2 = '�';

	/**
	 * ��������� ��� ������
	 * @var string
	 */
	protected $title = '';

	/**
	 *
	 * @var array
	 * ��������� ��������, ����������� ���� �����������.
	 */
	public $state = array();

	/**
	 * @var \SplObjectStorage ������ �����������
	 */
	protected $observers;

	/**
	 * ���-�� �������
	 * @var
	 */
	protected $cntVotes = 0;


	/**
	 * @param $key
	 * @param array $arr
	 * @return array|mixed
	 */
	protected function arrayValueRecursive($key, array $arr){
	    $val = array();
	    array_walk_recursive($arr, function($v, $k) use ($key, &$val){
	        if($k == $key) array_push($val, $v);
	    });

	    return count($val) > 1 ? $val : array_pop($val);
	}

	/**
	 * @param $array
	 * @param string $key
	 * @param int $order
	 */
	protected function arraySortByKey(&$array, $key = 'id', $order = SORT_ASC){
		$keyList = array();
		foreach($array as $k=>$item){
			if(isset($item[$key])){
				$keyList[] = $item[$key];
			}
		}

		array_multisort($keyList, $order, SORT_NUMERIC, $array);
	}

	/**
	 * Attach an SplObserver
	 * @link https://php.net/manual/en/splsubject.attach.php
	 * @param SplObserver $observer <p>
	 * The <b>SplObserver</b> to attach.
	 * </p>
	 * @return void
	 * @since 5.1.0
	 */
	public function attach(SplObserver $observer){
		$this->observers->attach($observer);
	}

	/**
	 * Detach an observer
	 * @link https://php.net/manual/en/splsubject.detach.php
	 * @param SplObserver $observer <p>
	 * The <b>SplObserver</b> to detach.
	 * </p>
	 * @return void
	 * @since 5.1.0
	 */
	public function detach(SplObserver $observer){
		$this->observers->detach($observer);
	}

	/**
	 * Notify an observer
	 * @link https://php.net/manual/en/splsubject.notify.php
	 * @return void
	 * @since 5.1.0
	 */
	public function notify(){
		foreach ($this->observers as $observer) {
			$observer->update($this);
		}
	}

	/**
	 * @return int
	 */
	public function getCountVotes(){
		return $this->cntVotes;
	}

	/**
	 * @param null $filter
	 * @return array
	 */
	public function getQuestions($filter = null){
		$q = array();
		$this->observers->rewind();
		while($this->observers->valid()){
			$obj = $this->observers->current();
			if(!is_null($filter) && ((int)$filter === $obj->getMultiple())){
				$this->observers->next();
			}else{
				$q[] = $obj->getQuestion();
				$this->observers->next();
			}
		}

		return $q;
	}

	/**
	 * @param string $str
	 * @return string
	 */
	abstract public function getTitle($str = '');

	/**
	 * @param string $page
	 * @return array
	 */
	abstract public function getHeadQuestions($page = '');

	/**
	 * @param string $page
	 * @return array
	 */
	abstract public function getStatAnswers($page = '');

	/**
	 * @param array $questions
	 * @return mixed
	 */
	abstract protected function attachObs(array $questions);


}
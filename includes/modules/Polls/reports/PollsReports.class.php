<?php

namespace modules\Polls\reports;


use modules\Polls\models\PollsModel;
use helpers\EncodingHelper as EH;

class PollsReports
{
	/**
	 * ������
	 * @var array
	 */
	protected $worksheet = array();

	/**
	 * ���� �
	 * format ('Y-m-d H:i:s')
	 * @var \DateTime
	 */
	protected $dtFrom;

	/**
	 * ���� ��
	 * format ('Y-m-d H:i:s')
	 * @var \DateTime
	 */
	protected $dtOn;

	/**
	 * ������ �������
	 * @var array
	 */
	protected $arrayVotes = array();

	/**
	 * ����
	 * @var bool
	 */
	private $isPublic;

	/**
	 * ���������
	 * @var bool
	 */
	private $isStudent;

	/**
	 * @var object
	 */
	protected $handler;


	public function __construct($worksheetId, $dtFrom = '', $dateOn = ''){
		($dtFrom !='') ? $this->setDateFrom($dtFrom): $this->dtFrom = '';
		($dateOn !='') ? $this->setDateOn($dateOn): $this->dtOn = '';
		if(!empty($worksheetId)) $this->setWorksheet($worksheetId);
	}

	/**
	 * ������������� ��������� ������
	 */
	private function checkVerdict(){
		if(!empty($this->worksheet)){
			$this->isPublic = $this->isStudent = false;
			switch ((int)$this->worksheet['verdict']){
				case 0:
					$this->isPublic = true;
				break;
				case 1:
					$this->isStudent = true;
				break;
				default;
					$this->isPublic = true;
				break;
			}
		}
	}

	/**
	 * ������������� ���������� � ����������� �� ��������� ��� ������,
	 * ���������� ����� ������
	 */
	private function setDataVotes(){
		if(!empty($this->worksheet) && ($this->dtFrom instanceof \DateTime) && ($this->dtOn instanceof \DateTime)){
			$d1 = $this->dtFrom->format('Y-m-d H:i:s');
			$d2 = $this->dtOn->format('Y-m-d H:i:s');
			if($this->isPublic){
				$this->arrayVotes = PollsModel::getVotes($this->worksheet['id'], $d1, $d2);
				if(($h = new PublicPolls($this->worksheet['prev'], $this->worksheet['questions'], $this->arrayVotes)) instanceof AbstractPolls){
					$this->handler = $h;
				}else {
					throw new \Exception(get_class($h) . ' not instanceof AbstractPolls');
				}
			}elseif($this->isStudent) {
				$this->arrayVotes = PollsModel::getVotes($this->worksheet['id'], $d1, $d2, true, true);
				if(($h = new StudentPolls($this->worksheet['questions'], $this->arrayVotes)) instanceof AbstractPolls){
					$this->handler = $h;
				}else{
					throw new \Exception(get_class($h) . ' not instanceof AbstractPolls');
				}
			}
		}
	}

	/**
	 * @param $w
	 * @return $this
	 * @throws \Exception
	 */
	public function setWorksheet($w){
		if($wsheet = PollsModel::getWorksheet((int)$w)){
			$this->worksheet = $wsheet;
			$this->checkVerdict();
			$this->setDataVotes();
			return $this;
		}
		throw new \Exception('Worksheet not find.');
	}

	/**
	 * @param $date
	 * @return $this
	 * @throws \Exception
	 */
	public function setDateFrom($date){
		if($dt = \DateTime::createFromFormat('Y-m-d H:i:s', $date)){
			$this->dtFrom = $dt;
			$this->setDataVotes();
			return $this;
		}
		throw new \Exception('Invalid string format(Y-m-d H:i:s) dateFrom');
	}

	/**
	 * @param $date
	 * @return $this
	 * @throws \Exception
	 */
	public function setDateOn($date){
		if($dt = \DateTime::createFromFormat('Y-m-d H:i:s', $date)){
			$this->dtOn = $dt;
			$this->setDataVotes();
			return $this;
		}
		throw new \Exception('Invalid string format(Y-m-d H:i:s) dateOn');
	}

	/**
	 * @return bool
	 */
	public function isPublic(){
		return $this->isPublic;
	}

	/**
	 * @return bool
	 */
	public function isStudent(){
		return $this->isStudent;
	}

	/**
	 * @return mixed
	 */
	public function getTitle(){
		return EH::changeArrayEncoding($this->handler->getTitle(), 'w2u');
	}

	/**
	 * @param string $page
	 * @return array
	 */
	public function getHeadQuestions($page = ''){
		return EH::changeArrayEncoding($this->handler->getHeadQuestions($page), 'w2u');
//		return $this->handler->getHeadQuestions($page);
	}

	/**
	 * @return int
	 */
	public function getCountColumn(){
		return $this->handler->getCountColumn();
	}

	/**
	 * @param string $key
	 * @return string
	 */
	public function getTextHadQuestions($key=''){
		$txt = '';
		switch ($key){
			case 'total':
				$txt = '����� ����';
				break;
			case 'comment':
				$txt = '������ �����������';
				break;
			case 'students':
				$txt = '��������� ���������';
				break;
			default:
				$txt = '������ (������� ����)';
				break;
		}
		return EH::changeArrayEncoding($txt, 'w2u');
	}

	/**
	 * @param string $page
	 * @return mixed
	 */
	public function getStatAnswers($page = ''){
		return EH::changeArrayEncoding($this->handler->getStatAnswers($page), 'w2u');
	}

	/**
	 * @param $glue
	 * @param $array
	 * @param string $symbol
	 * @return string
	 */
	public function mappedImplode($glue, $array, $symbol = ' = '){
		return EH::changeArrayEncoding(implode($glue, array_map(
				function($k, $v) use($symbol) {
					return $k . $symbol . $v;
				},
				array_keys($array),
				array_values($array)
			)
		), 'w2u');
	}

	/**
	 * @return int
	 */
	public function getCountStudents(){
		$rs = array();
		if($this->isStudent){
			foreach($this->arrayVotes as $k=> $v){
				$id = (int)$v['studentId'];
				if(!empty($id) && !in_array($id, $rs)){
					$rs[] = $id;
				}
			}
		}

		return count($rs);
	}

	/**
	 * @return array
	 */
	public function getCountStudentsByGroup(){
		$rs = array();
		if($this->isStudent){
			foreach($this->arrayVotes as $k=>$vote){
				$facId = (int)$vote['facultyId'];
				$facName = $vote['facultyName'];
				$groupName = (int)$vote['groupName'];
				$studentId = (int)$vote['studentId'];

				if(!isset($rs[$facId])){
					$rs[$facId][] = $facName;
					$rs[$facId][$groupName][] = $studentId;
					continue;
				}

				if(!isset($rs[$facId][$groupName])){
					$rs[$facId][$groupName][] = $studentId;
					continue;
				}

				if(!in_array($studentId, $rs[$facId][$groupName])){
					$rs[$facId][$groupName][] = $studentId;
				}
			}
			//���-��
			foreach($rs as $k_el=>$el){
				foreach($el as $k=>$v){
					if($k === 0) continue;
					$rs[$k_el][$k] = count($v);
				}
			}
		}

		return $rs;
	}
}
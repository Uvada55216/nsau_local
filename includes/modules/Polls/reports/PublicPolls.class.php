<?php

namespace modules\Polls\reports;

use modules\Polls\models\PollsModel;
use modules\Polls\reports\observers\PublicObserver;

final class PublicPolls extends AbstractPolls implements \SplSubject
{

	public function __construct($strTitle = '', array $questions = array() , array $votes = array()){
		$this->observers = new \SplObjectStorage();
		if(is_string($strTitle) && $strTitle !=='') $this->setTitle($strTitle);
		if(!empty($questions)) $this->attachObs($questions);
		if(!empty($votes)){
			$this->cntVotes = count($votes);
			$this->setVotes($votes);
		}
	}

	/**
	 * @param string $str
	 * @return string
	 */
	public function getTitle($str = ''){
		if(is_string($str) && $str !== ''){
			$this->setTitle($str);
		}
		return $this->title;
	}

	/**
	 * @param string $page
	 * @return array
	 */
	public function getHeadQuestions($page = ''){
		$rs = array();
		$arrQuestion = parent::getQuestions();
		if(!empty($arrQuestion)){
			foreach($arrQuestion as $key => $val){
				$rs[] = ++$key . '. ' . $val;
			}
		}
		return $rs;
	}

	/**
	 * @param $prv
	 */
	private function setTitle($prv){
		if(is_string($prv) && $prv != ''){
			if(($p1 = strpos($prv, self::P1)) !== false && ($p2 = strrpos($prv, self::P2)) !== false){
				$prv = substr($prv, $p1+1, $p2-$p1-1);
			}
		}

		$this->title = '���������� ������������� ' . $prv;
	}

	/**
	 * @param array $votes
	 */
	private function setVotes(array $votes){
		foreach($votes as $v){
			foreach($v['choices'] as $choice){
				$this->state = $choice;
				$this->notify();
			}
		}
		$this->state = array();
	}

	/**
	 * @param string $page
	 * @return array
	 */
	public function getStatAnswers($page=''){
		$rs = array();

		$this->observers->rewind();
		while($this->observers->valid()){
			$obj = $this->observers->current();
			$rs[] = $obj->getStatQuestions();
			$this->observers->next();
		}

		return $rs;
	}

	/**
	 * @param array $questions
	 */
	protected function attachObs(array $questions){
		$arrID = parent::arrayValueRecursive('id', $questions);
		$arrIdPos = array_combine($arrID , parent::arrayValueRecursive('pos', $questions));

		foreach(PollsModel::getQuestions($arrID, null, true) as $question){
			if(isset($arrIdPos[$question['id']])){
				$question['pos'] = $arrIdPos[$question['id']];
			}
			$arrQuest[] = $question;
		}
		parent::arraySortByKey($arrQuest, 'pos');
		unset($arrID, $arrIdPos);

		foreach($arrQuest as $q){
			$this->attach( new PublicObserver($q));
		}
	}
}
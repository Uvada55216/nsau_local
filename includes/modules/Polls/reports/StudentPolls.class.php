<?php

namespace modules\Polls\reports;

use modules\Polls\models\PollsModel;
use modules\Polls\reports\observers\StudentObserver;

final class StudentPolls extends AbstractPolls implements \SplSubject
{

	const BASE = 'base';
	const REPORT = 'report';
	const COMMENT = 'comment';
	const BD = 'bd';

	/**
	 * @var array
	 */
	private $data;

	/**
	 * @var int
	 */
	private $cntTeacherVotes;

	public function __construct(array $questions = array(), array $votes = array()){
		$this->observers = new \SplObjectStorage();
		$this->getTitle();
		if(!empty($questions)) $this->attachObs($questions);
		if(!empty($votes)){
			$this->cntVotes = count($votes);
			$this->setData($votes);
		}
	}

	/**
	 * ��������� ������
	 * @param array $votes
	 */
	private function setData(array $votes){
		foreach($votes as $key=>$vote){
			if(!isset($this->data[$vote['facultyId']])){//faculty
				$this->data[$vote['facultyId']] = array(
					'facultyName' => $vote['facultyName'],
					'facultyShortName' => $vote['facultyShortName'],
					'teachers' => array()
				);
				$this->addTeacherAndGroup($vote);//teachers and groups
				continue;
			}
			$this->addTeacherAndGroup($vote);
		}
		$this->createBaseData();
	}

	/**
	 * @param $vote
	 */
	private function addTeacherAndGroup($vote){
		$teacherId = $vote['teacherId'];
		$groupId = $vote['groupId'];
		$groupName = $vote['groupName'];
		$vote['choices']['group'] = $groupName;

		if(!isset($this->data[$vote['facultyId']]['teachers'][$teacherId])){
			$teacherName = $vote['teacherLastName'].' '.$vote['teacherName'][0].'. '.$vote['teacherPatronymic'][0].'. ';

			$this->data[$vote['facultyId']]['teachers'][$teacherId] = array(
				'teacherName' => $teacherName,
				'groups' => array(
					$groupId => $groupName
				),
				'choices' => array($vote['choices'])
			);
		}else{
			if(!isset($this->data[$vote['facultyId']]['teachers'][$teacherId]['groups'][$groupId])){
				$this->data[$vote['facultyId']]['teachers'][$teacherId]['groups'][$groupId] = $groupName;
			}

			$this->data[$vote['facultyId']]['teachers'][$teacherId]['choices'][] = $vote['choices'];
		}
	}

	/**
	 * @param array $questions
	 */
	protected function attachObs(array $questions){
		$arrID = parent::arrayValueRecursive('id', $questions);
		$arrIdPos = array_combine($arrID , parent::arrayValueRecursive('pos', $questions));

		foreach(PollsModel::getQuestions($arrID, null, true) as $question){
			if(isset($arrIdPos[$question['id']])){
				$question['pos'] = $arrIdPos[$question['id']];
			}
			$arrQuest[] = $question;
		}
		parent::arraySortByKey($arrQuest, 'pos');
		unset($arrID, $arrIdPos);

		foreach($arrQuest as $q){
			$this->attach( new StudentObserver($q));
		}
	}


	/**
	 * @param string $str
	 * @return string
	 */
	public function getTitle($str = ''){
		$f_str = '���������� ������������� �����������';
		$s_str = '202__/202__ ������� ��� (__ �������)';

		if(is_string($str) && $str !== ''){
			$f_str = $str;
		}

		return $this->title = $f_str . ' ' . $s_str;
	}

	/**
	 * @param string $page
	 * @return array
	 */
	public function getHeadQuestions($page = ''){
		$rs = array();
		switch ($page){
			case self::BASE:
				$rs = array_merge(array('���������, �� ������� ��������� ��������', '����� ������', '��� �������������'), $this->questions());
				break;
			case self::REPORT:
				$rs = array_merge(array('� �/�', '�������������, � �������� ��������� ���', '��� �������������'), $this->questions(2), array('������� ������ �� �������������'));
				break;
			case self::COMMENT:
				$rs = array_merge(array('�������������', '���'), $this->questionsText());
				break;
			default:
				$rs = $this->questions();
				break;
		}
		return $rs;
	}

	/**
	 * @param null $filter
	 * @return array
	 */
	private function questions($filter = null){
		$q = array();
		$arrQuestion = parent::getQuestions($filter);
		if(!empty($arrQuestion)){
			foreach($arrQuestion as $key => $val){
				$q[] = ++$key . '. ' . $val;
			}
		}
		return $q;
	}


	private function questionsText(){
		$q = array();
		$this->observers->rewind();
		while($this->observers->valid()){
			$obj = $this->observers->current();
			if(2 != $obj->getMultiple()){
				$this->observers->next();
			}else{
				$q[] = $obj->getQuestion();
				$this->observers->next();
			}
		}
		return $q;
	}

	/**
	 * @param string $page
	 * @return array
	 */
	public function getStatAnswers($page=''){
		$arrRs = array();
		if(!empty($this->data)){
			switch ($page){
				case self::BD;
					$arrRs = $this->createBDPageData($page);
					break;

				case self::COMMENT;
					$arrRs = $this->createPageData($page);
					break;

				default;
					$arrRs = $this->createBasePageData($page);
					break;
				}
		}

		return $arrRs;
	}

	/**
	 * @return int
	 */
	public function getCntTeacherVotes(){
		return (int)$this->cntTeacherVotes;
	}

	/**
	 * ������� ������ ������
	 */
	private function createBaseData(){
		if(!empty($this->data)){
			foreach($this->data as $key_1 => $val_1){
				foreach($val_1['teachers'] as $key_2 => $val_2){
					if(array_key_exists('choices', $val_2)){
						$arrCh = array();
						foreach($val_2['choices'] as $key_3 => $val_3){
							foreach($val_3 as $val_ch){
								if(isset($val_ch[2]['text'])){
									$arrCh[$val_ch[0]] = $val_ch[2]['text'];
								} else {
									$arrCh[$val_ch[0]] = $val_ch[2];
								}
							}
							$arrCh['group'] = $val_3['group'];
							$ch[] = $arrCh;
						}
						$this->data[$key_1]['teachers'][$key_2]['choices'] = $ch;
						unset($ch);
					}
				}
			}
		}
	}

	/**
	 * ������ ������ ��� ������
	 * @param $page
	 * @return array
	 */
	private function createPageData($page){
		if(!empty($this->data)){
			$data = $this->data;

			foreach($data as $key_1 => $val_1){
				foreach($val_1['teachers'] as $key_2 => $val_2){
					if(array_key_exists('choices', $val_2)){
						foreach($val_2['choices'] as $key_3 => $val_3){
							$this->cntTeacherVotes = count($val_2['choices']);
							foreach($val_3 as $key_ch=>$val_ch){
								$this->state = array($key_ch, $val_ch);
								$this->notify();
							}
						}
						$data[$key_1]['teachers'][$key_2]['choices'] = $this->getObsData($page);
						$this->clearObsStat();
					}
				}
			}
			return $data;
		}
	}

	/**
	 * @param $page
	 * @return array
	 */
	private function getObsData($page){
		$rs = array();

		$this->observers->rewind();
		while($this->observers->valid()){
			$obj = $this->observers->current();
			if($page == self::REPORT){
				if($obj->getMultiple() != 2){
					$rs += $obj->getStatQuestions();
				}
			}elseif($page == self::COMMENT){
				if($obj->getMultiple() != 0){
					$rs += $obj->getStatQuestions();
				}
			}elseif($page == self::BASE || $page == self::BD){
					$rs += $obj->getStatQuestions();
			}
			$this->observers->next();
		}

		return $rs;
	}

	/**
	 *
	 */
	private function clearObsStat(){
		$this->observers->rewind();
		while($this->observers->valid()){
			$obj = $this->observers->current();
			$obj->clearStat();
			$this->observers->next();
		}
	}

	/**
	 * @param $page
	 * @return array
	 */
	private function createBasePageData($page){
		if(!empty($this->data)){
			$data = $this->data;
			foreach($data as $key_1 => $val_1){
				foreach($val_1['teachers'] as $key_2 => $val_2){
					if(array_key_exists('choices', $val_2)){
						foreach($val_2['choices'] as $key_3 => $val_3){
							$this->cntTeacherVotes = count($val_2['choices']);
							foreach($val_3 as $key_ch=>$val_ch){
								if($key_ch == 'group') continue;
								$this->state = array($key_ch, $val_ch);
								$this->notify();
							}
							$data[$key_1]['teachers'][$key_2]['choices'][$key_3] = $this->getObsData($page);
							$this->clearObsStat();
						}

					}
				}
			}

			return $data;
		}
	}

	private function createBDPageData($page){
		if(!empty($this->data)){
			$data = $this->data;
			foreach($data as $key_1 => $val_1){
				foreach($val_1['teachers'] as $key_2 => $val_2){
					if(array_key_exists('choices', $val_2)){
						foreach($val_2['choices'] as $key_3 => $val_3){
							$this->cntTeacherVotes = count($val_2['choices']);
							foreach($val_3 as $key_ch=>$val_ch){
								if($key_ch == 'group') continue;

								$this->state = array($key_ch, $val_ch);
								$this->notify();
							}

							$data[$key_1]['teachers'][$key_2]['choices'][$key_3] = array(
								$this->getObsData($page),
								'group' => $val_3['group']
							);
							$this->clearObsStat();
						}

					}
				}
			}

			return $data;
		}
	}
}
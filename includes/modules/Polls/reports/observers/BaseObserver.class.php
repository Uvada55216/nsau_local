<?php

namespace modules\Polls\reports\observers;


abstract class BaseObserver
{

	/**
	 * @return string
	 */
	abstract public function getQuestion();

	/**
	 * @return mixed
	 */
	abstract public function getStatQuestions();

	/**
	 * @return int
	 */
	abstract public function getMultiple();

	/**
	 * @return mixed
	 */
	abstract public function clearStat();

}
<?php

namespace modules\Polls\reports\observers;

use modules\Polls\reports\slaves\SlaveMultiple;
use modules\Polls\reports\slaves\SlaveText;
use SplSubject;

class PublicObserver extends BaseObserver implements \SplObserver
{
	/**
	 * @var
	 */
    protected $id;

	/**
	 * @var
	 */
    protected $question;

	/**
	 * @var
	 */
//    protected $active;

	/**
	 * @var
	 */
//    protected $required;

	/**
	 * @var
	 */
    protected $multiple;

	/**
	 * @var
	 */
//    protected $answer;

	/**
	 * @var
	 */
//    protected $user;

	/**
	 * @var
	 */
//    protected $date;

	/**
	 * @var
	 */
//    protected $pos;

	/**
	 * @var object
	 */
    protected $slave;


	public function __construct(array $params){
		if(!empty($params)){
			$this->id = $params['id'];
			$this->question = $params['question'];
//			$this->active = $params['active'];
//			$this->required = $params['required'];
			$this->multiple = $params['multiple'];
//			$this->answer = $params['answer'];
//			$this->user = $params['user'];
//			$this->date = $params['date'];
//			$this->pos = $params['pos'];
			//slave
			switch ($this->multiple){
				case 0:
					$this->slave = new SlaveMultiple($params['answer']['option']);
					break;
				case 1:
					$this->slave = new SlaveMultiple($params['answer']['option']);
					break;
				case 2:
					$this->slave = new SlaveText($params['answer']['option']);
					break;
				default;
				break;
			}



		}

	}

	/**
	 * Receive update from subject
	 * @link https://php.net/manual/en/splobserver.update.php
	 * @param SplSubject $subject <p>
	 * The <b>SplSubject</b> notifying the observer of an update.
	 * </p>
	 * @return void
	 * @since 5.1.0
	 */
	public function update(SplSubject $subject){
		if($subject->state[0] === $this->id){
			$this->slave->estimation($subject);

			if($this->slave instanceof SlaveMultiple ){
				$this->slave->setCntVotes($subject->getCountVotes());
			}
		}
	}

	/**
	 * @return string
	 */
	public function getQuestion(){
		return (string)$this->question;
	}

	/**
	 * @return mixed
	 */
	public function getStatQuestions(){
		return $this->slave->getOptionData();
	}

	/**
	 * @return int
	 */
	public function getMultiple(){
		return (int)$this->multiple;
	}

	/**
	 * @return mixed
	 */
	public function clearStat(){
		$this->slave->clear();
	}
}
<?php

namespace modules\Polls\reports\observers;

use modules\Polls\reports\slaves\SlaveOnly;
use modules\Polls\reports\slaves\SlaveText;
use SplSubject;

class StudentObserver extends BaseObserver implements \SplObserver
{
	/**
	 * @var int
	 */
    protected $id;

	/**
	 * @var
	 */
    protected $question;

	/**
	 * @var int
	 */
    protected $multiple;

	/**
	 * @var object
	 */
    protected $slave;


	public function __construct(array $params){
		if(!empty($params)){
			$this->id = (int)$params['id'];
			$this->question = $params['question'];
			$this->multiple = (int)$params['multiple'];

			//slave
			if($this->multiple === 0){
				$this->slave = new SlaveOnly($params['answer']['option']);
			}
//			elseif($this->multiple === 1){
//				$this->slave = new SlaveMany($params['answer']['option']);
//			}
			elseif($this->multiple === 2){
				$this->slave = new SlaveText($params['answer']['option']);
			}
		}
	}

	/**
	 * Receive update from subject
	 * @link https://php.net/manual/en/splobserver.update.php
	 * @param SplSubject $subject <p>
	 * The <b>SplSubject</b> notifying the observer of an update.
	 * </p>
	 * @return void
	 * @since 5.1.0
	 */
	public function update(SplSubject $subject){
		if((int)$subject->state[0] === $this->id){
			$this->slave->estimation($subject);
		}
	}

	/**
	 * @return string
	 */
	public function getQuestion(){
		return (string)$this->question;
	}


	public function getStatQuestions(){
		return array($this->id => $this->slave->getOptionData());
	}

	/**
 * @return int
 */
	public function getMultiple(){
		return (int)$this->multiple;
	}

	/**
	 * @return mixed
	 */
	public function clearStat(){
		$this->slave->clear();
	}
}
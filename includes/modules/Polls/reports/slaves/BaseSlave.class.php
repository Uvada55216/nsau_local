<?php

namespace modules\Polls\reports\slaves;

abstract class BaseSlave
{
	/**
	 * @var array
	 */
	protected $option;

	public function __construct(array $options){
		if(!empty($options)){
			$this->option = $options;
		}
	}

	/**
	 * @param $subject
	 * @return mixed
	 */
	abstract public function estimation($subject);

	/**
	 * @return mixed
	 */
	abstract public function getOptionData();

	/**
	 * @return mixed
	 */
	abstract public function clear();
}
<?php

namespace modules\Polls\reports\slaves;

class SlaveMultiple extends BaseSlave
{
	/**
	 * @var
	 */
	protected $statOpt = array();

	/**
	 * @var int
	 */
	protected $cnt;

	public function __construct(array $options){
		parent::__construct($options);

		foreach(array_keys($options) as $key){
			$this->statOpt[$key] = 0;

		}
	}

	/**
	 * @param SplSubject $subject
	 * @return mixed
	 */
	public function estimation($subject){
		if(isset($this->statOpt[$subject->state[2]])){
			$this->statOpt[$subject->state[2]]++;
		}
	}

	/**
	 * @return mixed
	 */
	public function getOptionData(){
		if(!empty($this->option) && !empty($this->statOpt)){
			$arrVal = array();
			foreach(array_values($this->statOpt) as $v){
				$arrVal[] = round(($v / $this->cnt) * 100, 1);
			}
			return array_combine(array_values($this->option), $arrVal);
		}
	}

	/**`
	 * @param $cnt
	 */
	public function setCntVotes($cnt){
		if(($cnt = (int)$cnt) >0 ){
			$this->cnt = $cnt;
		}
	}

	/**
	 * @return mixed
	 */
	public function clear(){
		foreach($this->statOpt as $key=>$value){
			$this->statOpt[$key] = 0;
		}
	}
}
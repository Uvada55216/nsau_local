<?php

namespace modules\Polls\reports\slaves;

use modules\Polls\reports\PublicPolls;
use modules\Polls\reports\StudentPolls;

class SlaveOnly extends BaseSlave
{
	/**
	 * @var
	 */
	protected $statOpt = 0;

	/**
	 * @var int
	 */
	protected $cnt;

	public function __construct(array $options){
		parent::__construct($options);
	}

	/**
	 * @param SplSubject $subject
	 * @return mixed
	 */
	public function estimation($subject){
		if($subject instanceof StudentPolls){
			$sbjVal = $subject->state[1];
			$this->cnt = $subject->getCntTeacherVotes();
			if(isset($this->option[$sbjVal])){
				$this->statOpt = (int)$this->option[$sbjVal];
			}
		}
	}

	/**
	 * @return mixed
	 */
	public function getOptionData(){
//		if(!empty($this->option) && !empty($this->statOpt)){
//			$arrVal = array();
//			foreach(array_values($this->statOpt) as $key=>$v){
//				$arrVal[] = $key*$v;
//			}
//			return round((array_sum($arrVal) / $this->cnt), 1);
			return $this->statOpt;
//		}
	}

	/**
	 * @return mixed
	 */
	public function clear(){
			$this->statOpt = 0;
	}
}
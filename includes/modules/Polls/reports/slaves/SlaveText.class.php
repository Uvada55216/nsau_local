<?php

namespace modules\Polls\reports\slaves;

use modules\Polls\reports\PublicPolls;
use modules\Polls\reports\StudentPolls;

class SlaveText extends BaseSlave
{

	/**
	 * @var array
	 */
	protected $arrText = array();

	public function __construct(array $options){
		parent::__construct($options);
	}

	/**
	 * @param $subject
	 * @return mixed
	 */
	public function estimation($subject){
		$sbjText = '';
		if($subject instanceof PublicPolls){
			if($subject->state[2]['text'] != ''){
				$sbjText = $subject->state[2]['text'];
			}
		}elseif($subject instanceof StudentPolls){
			if($subject->state[1] != ''){
				$sbjText = $subject->state[1];
			}
		}
		$this->arrText[1][] = strtolower(trim($sbjText));
	}

	/**
	 * @return mixed
	 */
	public function getOptionData(){
		if(!empty($this->option) && !empty($this->arrText)){
			$rs = array();
			foreach(array_values($this->arrText[1]) as $text){
				if(!isset($rs[$text])){
					$rs[] = $text;
				}
			}
			return array_combine(array_values($this->option), array_values(array(implode(', ', $rs))));
		}
	}

	/**
	 * @return mixed
	 */
	public function clear(){
		$this->arrText = array();
	}
}
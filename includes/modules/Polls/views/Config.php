<div class="polls" role="tablist">
	<ul id="myTab" class="nav nav-pills files_tab_navigation">
        <?php if($moduleData['worksheet']){ ?>
            <li class="nav-item active">
                <a id="reports-tab" class="nav-link" data-toggle="tab" href="#reports" role="tab" aria-expanded="true" aria-selected="true">������</a>
            </li>
        <?php } ?>
		<li class="nav-item">
            <a id="worksheet-tab" class="nav-link" data-toggle="tab" href="#worksheet" role="tab" aria-expanded="false" aria-selected="false">������ � ������</a>
        </li>
        <li class="nav-item">
            <a id="questions-tab" class="nav-link" data-toggle="tab" href="#questions" role="tab" aria-expanded="false" aria-selected="false">�������</a>
        </li>
        <li class="nav-item">
            <a id="answers-tab" class="nav-link" data-toggle="tab" href="#answers" role="tab" aria-expanded="false" aria-selected="false">������</a>
        </li>
        <li class="nav-item">
            <a id="groups-teachers-tab" class="nav-link" data-toggle="tab" href="#groups-teachers" role="tab" aria-expanded="false" aria-selected="false">������ / �������������</a>
        </li>
	</ul>
	<div id="tabContent" class="tab-content">
        <div id="worksheet" class="tab-pane fade" role="tabpanel" aria-labelledby="home-tab">
                <div class="list-group">
                    <div class="">
                        <?php if($moduleData['worksheet']){
	                        foreach($moduleData['worksheet'] as $worksheet){ ?>
                                <div class="list-group-item list-group-item-secondary">
                                    <div class="input-group">
                                        <input type="text" id="worksheet_<?=$worksheet['id']?>" class="form-control"
                                               data-id="<?=$worksheet['id']?>"
                                               value="<?=$worksheet['prev']?>"
                                               data-text="<?=$worksheet['text']?>"
                                               data-pos="<?=$worksheet['pos']?>"
                                               data-question="<?=htmlspecialchars(json_encode($worksheet['questions']), ENT_QUOTES, 'UTF-8')?>"
                                               data-active="<?=($worksheet['active']) ? 0 : 1?>"
                                               data-verdict="<?=($worksheet['verdict'])?>"
                                               data-multiple="<?=($worksheet['multiple'])?>"
                                               data-object="worksheet"
                                               readonly>
                                        <div class="input-group-btn">
                                            <button type="button" id="changeActivity" class="btn btn-default"
                                                    data-suorce="#worksheet_<?=$worksheet['id']?>"
                                                    title="<?=($worksheet['active']) ? '������' : '�������'?>">
                                                <span id="worksheet_<?=$worksheet['id']?>"
                                                      class="glyphicon <?=($worksheet['active']) ? ' glyphicon-ok' : 'glyphicon-remove'?>"></span>
                                            </button>
                                            <button type="button" id="editWorksheet" class="btn btn-default"
                                                    data-toggle="modal" data-target="#editWorksheetModal"
                                                    data-suorce="#worksheet_<?=$worksheet['id']?>"
                                                    title="�������������">
                                                <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                            </button>
                                            <button id="delete" type="button" class="btn btn-default"
                                                    data-suorce="#worksheet_<?=$worksheet['id']?>" title="�������">
                                                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
	                        <?php }
                        } ?>
                    </div>
                </div>
                <div class="list-group">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addWorksheetModal">��������</button>
                    <div id="addWorksheetModal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="addWorksheetModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 id="addWorksheetModalLabel" class="modal-title">�������� ������, �����</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon1">��������<span class="obligatorily"> *</span></span>
                                        <input type="text" class="form-control" name="worksheetPrev"/>
                                    </div>
                                    <div class="form-group shadow-textarea">
                                        <label for="worksheetModalTextarea"></label>
                                        <textarea class="form-control z-depth-1" id="worksheetModalTextarea" name="worksheetText" rows="5" placeholder="�������������� �������� ������, ������"></textarea>
                                    </div>
                                    <div class="list-group row">
                                        <div class="col-md-2">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="sizing-addon2">���.</span>
                                                <input type="text" class="form-control num" name="worksheetPos" aria-describedby="sizing-addon2" maxlength="3">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="input-group">
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="activeWorksheet">
                                                    <span class="custom-control-description">�������</span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="input-group">
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="verdictWorksheet">
                                                    <span class="custom-control-description">������ �������������</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-group">
                                        <div class="input-group">
                                            <h2 class="tag tag-default">�������</h2>
                                        </div>
                                        <?php if(isset($moduleData['questions']) && is_array($moduleData['questions'])){
                                            foreach($moduleData['questions'] as $question){ ?>
                                                <div class="list-group-item list-group-item-secondary">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <input type="checkbox" data-id="<?=$question['id']?>"/>
                                                        </div>
                                                        <div class="col-md-1 pos">
                                                            <input type="text" class="form-control num" name="posQuestion_<?=$question['id']?>" maxlength="3" placeholder="���.">
                                                        </div>
                                                        <div class="col-md-11 pos">
                                                            <input class="form-control" value="<?=$question['question']?>" readonly/>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php }
                                        } ?>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">������</button>
                                    <button id="saveWorksheet" type="button" class="btn btn-primary">���������</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="editWorksheetModal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="editWorksheetModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 id="editWorksheetModalLabel" class="modal-title">������������� ������, �����</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon1">��������<span class="obligatorily"> *</span></span>
                                        <input type="text" class="form-control" data-id="" name="worksheetPrev"/>
                                    </div>
                                    <div class="form-group shadow-textarea">
                                        <label for="editWorksheetModalTextarea"></label>
                                        <textarea class="form-control z-depth-1" id="editWorksheetModalTextarea" name="worksheetText" rows="5" placeholder="�������������� �������� ������, ������"></textarea>
                                    </div>
                                    <div class="list-group row">
                                        <div class="col-md-2">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="sizing-addon2">���.</span>
                                                <input type="text" class="form-control num" name="worksheetPos" aria-describedby="sizing-addon2" maxlength="3"/>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="input-group">
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="activeWorksheet">
                                                    <span class="custom-control-description">�������</span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="input-group">
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="verdictWorksheet">
                                                    <span class="custom-control-description">������ �������������</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="list-group">
                                        <div class="input-group">
                                            <h2 class="tag tag-default">�������</h2>
                                        </div>
						                <?php if(isset($moduleData['questions']) && is_array($moduleData['questions'])){
							                foreach($moduleData['questions'] as $question){ ?>
                                                <div class="list-group-item list-group-item-secondary">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <input type="checkbox" data-id="<?=$question['id']?>"/>
                                                        </div>
                                                        <div class="col-md-1 pos">
                                                            <input type="text" class="form-control num" name="posQuestion_<?=$question['id']?>" maxlength="3" placeholder="���."/>
                                                        </div>
                                                        <div class="col-md-11 pos">
                                                            <input class="form-control" value="<?=$question['question']?>" readonly/>
                                                        </div>
                                                    </div>
                                                </div>
							                <?php }
						                } ?>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">������</button>
                                    <button id="saveWorksheet" type="button" class="btn btn-primary">���������</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <div id="questions" class="tab-pane fade fade" role="tabpanel" aria-labelledby="home-tab">
                <div class="list-group">
                    <div class="other_files">
						<?php if($moduleData['questions']){
                            foreach($moduleData['questions'] as $question){ ?>
                                <div class="list-group-item list-group-item-secondary">
                                    <div class="input-group">
                                        <input type="text"
                                               id="questions_<?=$question['id']?>"
                                               class="form-control"
                                               value="<?=$question['question']?>"
                                               data-answer="<?=$question['answer']?>"
                                               data-id="<?=$question['id']?>"
                                               data-active="<?=($question['active'])? 0: 1?>"
                                               data-multiple="<?=($question['multiple'])?>"
                                               data-required="<?=($question['required'])?>"
                                               data-object="questions"
                                               readonly
                                        >
                                        <div class="input-group-btn">
                                            <button type="button" id="changeActivity" class="btn btn-default" data-suorce="#questions_<?=$question['id']?>" title="<?=($question['active'])? '������': '�������'?>">
                                                <span id="questions_<?=$question['id']?>" class="glyphicon <?=($question['active'])? ' glyphicon-ok': 'glyphicon-remove'?>"></span>
                                            </button>
                                            <button type="button" class="btn btn-default" data-suorce="#questions_<?=$question['id']?>" data-toggle="modal" data-target="#editQuestionModal" title="�������������">
                                                <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                            </button>
                                            <button id="delete" type="button" class="btn btn-default" data-suorce="#questions_<?=$question['id']?>" title="�������">
                                                <span class="glyphicon glyphicon-trash"></span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            <?php }
						} ?>
                    </div>
                </div>
                <div class="list-group">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addQuestionModal">��������</button>
                    <div id="addQuestionModal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="addQuestionModalLabel" aria-hidden="false">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 id="addQuestionModalLabel" class="modal-title">�������� ������</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon1">������<span class="obligatorily"> *</span></span>
                                        <input type="text" class="form-control" name="question"/>
                                    </div>
                                    <h3 class="modal-title">������</h3>
                                    <?php if(isset($moduleData['answers']) && is_array($moduleData['answers'])){
	                                    foreach($moduleData['answers'] as $answers){
		                                $option = implode(' / ', $answers['option']); ?>
                                        <div class="list-group-item list-group-item-secondary">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <input type="checkbox"  data-id="<?=$answers['id']?>"/>
                                                </span>
                                                <input type="text" class="form-control"  id="text_<?=$answers['id']?>" value="<?=$option?>" readonly/>
                                            </div>
                                            <?php if($answers['option'][1] !== 'textarea') { ?>
                                                <div class="input-group">
                                                    <?php /*
                                                    <label class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="multiple_<?=$answers['id']?>">
                                                        <span class="custom-control-description">������������� �����</span>
                                                    </label>
                                                     */?>
                                                    <label class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="required_<?=$answers['id']?>">
                                                        <span class="custom-control-description">������������ �����</span>
                                                    </label>
                                                </div>
                                            <?php } ?>
                                        </div>
	                                    <?php }
                                    } ?>
                                    <div class="input-group">
                                        <label for="activeQuestion">�������</label>
                                        <input type="checkbox" id="activeQuestion">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">������</button>
                                    <button id="addQuestion" type="button" class="btn btn-primary">���������</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="editQuestionModal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="editQuestionModal" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 id="editQuestionModal" class="modal-title">������������� ������</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon1">������<span class="obligatorily"> *</span></span>
                                        <input type="text" class="form-control" name="question" data-id=""/>
                                    </div>
                                    <h3 class="modal-title">�������� �������</h3>
					                <?php if(isset($moduleData['answers']) && is_array($moduleData['answers'])){
						                foreach($moduleData['answers'] as $answers){
							                $option = implode(' / ', $answers['option']); ?>
                                            <div class="list-group-item list-group-item-secondary">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <input type="checkbox"  data-id="<?=$answers['id']?>"/>
                                                    </span>
                                                    <input type="text" class="form-control" value="<?=$option?>" readonly/>
                                                </div>
	                                            <?php if($answers['option'][1] !== 'textarea') { ?>
                                                    <div class="input-group">
                                                        <?php /*
                                                        <label class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="multiple_<?=$answers['id']?>">
                                                            <span class="custom-control-description">������������� �����</span>
                                                        </label>
                                                        */?>
                                                    </div>
                                                    <label class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="required_<?=$answers['id']?>">
                                                        <span class="custom-control-description">������������ �����</span>
                                                    </label>
	                                            <?php } ?>
                                            </div>
						                <?php }
					                } ?>
                                    <div class="input-group">
                                        <label for="activeQuestion">�������</label>
                                        <input type="checkbox" id="activeQuestion">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">������</button>
                                    <button id="editQuestion" type="button" class="btn btn-primary">���������</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <div id="answers" class="tab-pane fade fade" role="tabpanel" aria-labelledby="home-tab">
            <div class="list-group">
                <div class="other_files">
					<?php if($moduleData['answers']){
						foreach($moduleData['answers'] as $answers){
							$option = implode(' / ', $answers['option']);
							?>
                            <div id="item_<?=$answers['id']?>" class="list-group-item list-group-item-secondary">
                                <div class="input-group">
                                    <input type="text" class="form-control" value="<?=$option?>" readonly>
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default" data-toggle="modal"
                                                data-target="#answersModal_<?=$answers['id']?>"
                                                data-action="edit" title="�������������">
                                            <span class="glyphicon glyphicon-pencil"></span>
                                        </button>
                                        <button id="deleteAnswers" type="button" class="btn btn-default"
                                                data-target="#answersModal_<?=$answers['id']?>"
                                                data-object="answers" title="�������">
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div id="answersModal_<?=$answers['id']?>" class="modal fade bd-example-modal-lg"
                                 tabindex="-1" role="dialog" aria-labelledby="answersModalLabel_<?=$answers['id']?>"
                                 aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <h4 class="modal-title" id="myModalLabel">������������� �����</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1">������<span
                                                            class="obligatorily"> *</span></span>
                                                <input type="text" class="form-control" name="answers"
                                                       value="<?=$option?>" data-id="<?=$answers['id']?>"/>
                                            </div>
                                            <div class="input-group">
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input"
                                                           id="textAnswer" <?=($answers['option'][1] === 'textarea') ? 'checked' : ''?>>
                                                    <span class="custom-control-description">��������� ����</span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                ������
                                            </button>
                                            <button id="editAnswers" type="button" class="btn btn-primary"
                                                    data-target="#answersModal_<?=$answers['id']?>">���������
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
						<?php }
					} ?>
                </div>
            </div>
            <div class="">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addAnswersModal">��������</button>
                <div id="addAnswersModal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="addAnswersModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">�������� �����</h4>
                            </div>
                            <div class="modal-body">
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1">������<span class="obligatorily"> *</span></span>
                                    <input type="text" class="form-control" name="answers" value=""/>
                                </div>
                                <div class="input-group">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="textAnswer">
                                        <span class="custom-control-description">��������� ����</span>
                                    </label>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">������</button>
                                <button id="addAnswers" type="button" class="btn btn-primary">���������</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if(isset($moduleData['faculties']) && is_array($moduleData['faculties'])) { ?>
            <div id="groups-teachers" class="tab-pane fade fade" role="tabpanel" aria-labelledby="home-tab">
                <div class="list-group">
                    <div id="Accordian" class="panel-group">
					    <?php foreach($moduleData['faculties'] as $fac){ ?>
                            <div id="faculties_<?=$fac['id']?>" class="list-group-item list-group-item-secondary">
                                <div class="input-group">
                                    <a data-toggle="collapse" data-parent="#Accordian" href="#<?=$fac['id']?>">
                                        <input type="text" class="form-control" value="<?=$fac['name']?>" readonly>
                                    </a>
                                    <div class="input-group-btn">
                                        <div class="input-group-btn">
                                            <button type="button" id="changeActivGroupsByFacult" class="btn btn-default" data-active="<?=($fac['deactivation'])? 0: 1?>" data-id="<?=$fac['id']?>" title="<?=($fac['deactivation'])? '������ ������': '������� ������'?>">
                                                <span id="faculties_<?=$fac['id']?>" class="glyphicon <?=($fac['deactivation'])? ' glyphicon-ok': 'glyphicon-remove'?>"></span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-collapse collapse out" id="<?=$fac['id']?>">
                                <div class="panel-body">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>����� �����</th>
                                            <th>������� �����</th>
                                            <th>����-������� �����</th>
                                            <th>��� �����</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(count($fac['arr_groups']) > 0){
                                               foreach($fac['arr_groups'] as $group){ ?>
                                               <tr>
                                                   <td>
                                                    <?php if($group['form_education'] == 1) {
                                                        echo $this->renderBlock('blocks/Group', $group);
                                                    } ?>
                                                   </td>
                                                   <td>
                                                    <?php if($group['form_education'] == 2) {
		                                               echo $this->renderBlock('blocks/Group', $group);
	                                               } ?>
                                                   </td>
                                                   <td>
                                                    <?php if($group['form_education'] == 3) {
	                                                    echo $this->renderBlock('blocks/Group', $group);
                                                    } ?>
                                                   </td>
                                                   <td>
                                                    <?php if($group['form_education'] !=1 && $group['form_education'] !=2 && $group['form_education'] !=3) {
	                                                    echo $this->renderBlock('blocks/Group', $group);
                                                    } ?>
                                                   </td>
                                               </tr>
                                               <?php }
                                            } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
						<?php } ?>
                    </div>
                </div>
                <div class="">
                    <button id="updatePollsGroup" type="" class="btn btn-primary">�������� ������</button>
                    <div id="editGroupTeacherModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="editGroupTeacherModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 id="editGroupTeacherModalLabel" class="modal-title">������������� ������</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="panel-body file_edit_form">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon1">������</span>
                                        <input type="text" class="form-control" data-id="" name="groupName" value="" readonly/>
                                    </div>

                                    <div class="input-group">
                                        <span class="input-group-addon nsau-files-form without-bottom-border">
                                            <span data-tooltip="teachers">�������������</span>
                                        </span>
                                        <input type="text" class="form-control without-bottom-border" name="searchTeacher" placeholder="����� �� �������">
                                        <select class="form-control post without-bottom-border select_teacher">
                                            <option value=""></option>
                                        </select>
                                    </div>

                                    <div class="list-group teachers_list">
                                        <a href="#" class="list-group-item list-group-item-nsau-grey-button" data-object="teacher" data-action="add" style="border-radius: 0px;">
                                            �������� �������������
                                        </a>
                                        <span class="list-group-item tmp hidden">
                                            <div class="row">
                                                <div class="col-md-11">
                                                    <span class="name" data-id=""></span>
                                                </div>
                                                <div class="col-md-1">
                                                    <button type="button" class="btn btn-xs btn-default" data-object="teacher" data-action="delete" aria-label="Left Align" title="�������">
                                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                                    </button>
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <input type="checkbox" class="custom-control" name="groupActive">
                                            <span class="custom-control">����������</span>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">������</button>
                                    <button type="button" data-object="teacher" data-action="edit" class="btn btn-primary">���������</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php if($moduleData['worksheet']){ ?>
            <div id="reports" class="tab-pane fade active in" role="tabpanel" aria-labelledby="home-tab">
            <div class="form-row">
                <form class="form-horizontal" action="reports/" method="post">
                    <div class="list-group">
                        <div class="list-group-item">
                            <div class="input-group col-md-8">
                                <h3 style="margin-bottom: 10px;">�������� ������(�����)</h3>
                                <div style="padding-left: 15px;">
                                    <label for="workshRep"></label>
                                    <select class="form-control" name="worksheetId" required>
                                        <option></option>
										<?php if($moduleData['worksheet']){
											foreach($moduleData['worksheet'] as $worksheet){ ?>
                                                <option value="<?=$worksheet['id']?>"><?=$worksheet['prev']?></option>
											<?php }
										} ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="input-group col-md-8">
                                    <h3>������� ������</h3>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <label class="sr-only-focusable" for="formDate">c</label>
                                            <input type="date" class="search-query" name="dateFrom" placeholder="�" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <label class="sr-only-focusable" for="onDate">��</label>
                                            <input type="date" class="search-query" name="dateOn" placeholder="��" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="list-group">
                            <div class="input-group">
                            <div class="col-sm-3">
                                <button type="submit" class="btn btn-primary">�������</button>
                            </div>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
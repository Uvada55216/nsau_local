<?php
    $required = 'required_'.$moduleData['id'];
?>
<li class="vote-item <?=($required !='')? ' js_errors':''?>">
	<div class="vote-item-header">
		<div class="vote-item-title vote-item-question"><?=$moduleData['question']?>
			<?php if($moduleData['required']) { ?>
                <script type="text/javascript">
                    "use strict";
                    jQuery(document).ready(function($){
                        $.validator.addClassRules({
                            '<?=$required?>': {
                                required: true
                            }
                        });
                    });
                </script>
                <span class="starrequired">*</span>
            <?php } ?>
		</div>
	</div>
        <ol class="vote-items-list vote-answers-list">
            <?php foreach($moduleData['answer']['option'] as $key => $answer) { ?>
                <li class="vote-item-odd">
                    <span class="vote-answer-item vote-answer-item-checkbox">
                        <input type="checkbox" class="<?=$required?>" name="<?=$required?>" id="vote_<?=$moduleData['id']?>_<?=$moduleData['answer']['id']?>_<?=$key?>" value="<?=$moduleData['id']?>_<?=$moduleData['answer']['id']?>_<?=$key?>" title="<?=($required !='')? ' ':''?>">
                        <label for="vote_<?=$moduleData['id']?>_<?=$moduleData['answer']['id']?>_<?=$key?>"><?=$answer?></label>
                    </span>
                </li>
            <?php } ?>
        </ol>
</li>


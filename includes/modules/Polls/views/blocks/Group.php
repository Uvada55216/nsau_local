<div class="input-group">
	<div class="group-cont">
		<div class="group-teacher" style="<?=($moduleData['hidden'] ? "color: gray" : "")?>"><?=$moduleData['group_name']?></div>
		<?php if($moduleData['hidden'] !=1) { ?>
			<div class="input-group-btn">
				<button type="button" class="btn btn-default"
				        data-toggle="modal"
				        data-target="#editGroupTeacherModal"
				        data-id="<?=$moduleData['id']?>"
				        data-active="<?=($moduleData['active'])? 0: 1?>"
				        data-name="<?=$moduleData['group_name']?>"
				        title="�������������"
				>
					<span style="<?=($moduleData['active'] ? "color: green" : "")?>" class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
				</button>
			</div>
		<?php }else { ?>
            <button class="btn btn-default hidden" data-id="<?=$moduleData['id']?>"></button>
        <?php } ?>
	</div>
</div>
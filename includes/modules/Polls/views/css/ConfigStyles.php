<style type="text/css">

.polls{
    min-height: 400px;
}
.polls div.list-group-item .btn{
    padding: 8px 12px;
    font-size: 16px;
}

.shadow-textarea textarea.form-control::placeholder {
    font-weight: 300;
}
.shadow-textarea textarea.form-control {
    padding-left: 0.8rem;
    resize: none;
}
.pos {
    padding: 0px!important;
}

#vote .list-group-item .num{
    border-right: 0px!important;
}
#vote .pos .form-control{
    border-radius: 0px!important;
}
div.group-cont div{
    display: inline-block;
    text-align: center;
}

.group-teacher{
    color: #9c0605;
}

#reports .list-group-item div.input-group{
    margin: 15px 0px;
}

.ui-datepicker .ui-datepicker-title {
    margin: 0px 31%!important;
}

/*.custom-control{*/
    /*display: block;*/
    /*float: left;*/
    /*position: relative;*/
/*}*/

input.custom-control{
    margin-right: 5px!important;
}

</style>
<style type="text/css">
.starrequired{
	color: red;
	font-weight: bold;
}

div#polls_content p {
    text-align: center;
}

.star, .vote-but{
    margin-left: 2em;
}

.vote-item {
    margin-bottom: 7px;
}

.vote-item-odd{
    margin: 2px 0px 2px 0px;
}

.vote-text{
    height: auto;
    resize: none;
    width: 65%;
    margin-top: 5px;
}
.form-polls{
    margin: 2em 0em;
}

div.errorMsg {
    /*color: red;*/
}

.invalidElem {
    color: red;
    /*border: medium solid red;*/
}

.heading-polls{
    margin-bottom: 10px;
}

.heading-polls h2, .heading-polls p{
    padding: 8px 5px;
}

.heading-polls h2, .heading-polls {
    text-align: center!important;
}

.heading-polls ul li {
    list-style-type: none;
    font-weight: bold;
}

.short-polls{
    position: relative;
    padding-bottom: 25px;
    padding-right: 30px;
    margin-bottom: 15px;
    box-shadow: 2px 3px 9px 0px #e8e4e4;
    padding-left: 15px;
}

form input[type="submit"] {
    width: auto!important;
    height: auto!important;
}


.js-errors {
    display:none;
    background:#f93337;
    border-radius:4px;
    color:#FFF;
    font-size:.8em;
    list-style-type:square;
    margin-bottom:1em;
    padding:1em;
}

.js-errors li {
  margin-left:1em;
  margin-bottom:.5em;
  padding-left:0;
}

ul.error input[type="checkbox"] + label::before,
ul.error input[type="radio"] + label::before {
  	border-color:#F93337;
}

ul.error input[type="checkbox"] + label,
ul.error input[type="radio"] + label {
	  color:#F93337;
}

.vote-teacher-block{
    padding: 20px 25px;
}

</style>
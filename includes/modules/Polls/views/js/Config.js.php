<script type="text/javascript">
    "use strict";
    jQuery(document).ready(function($){

    $.browser = {
        msie: false,
        version: 0
    };

    var regexp = new RegExp('^([0-9a-zA-Z�-���-ߨ\.\, ]+){1}\/*([0-9a-zA-Z�-���-ߨ\.\, ]+\/)*([0-9a-zA-Z�-���-ߨ\, ]+){1}$', 'ig');

    //�������� ����� ���� active ����� ����������
    function changeActiveGroups(facId, active){
        $('div#'+facId).find('button.btn.btn-default').each(function(e){
            $(this).attr('data-active', active);
            if(!active){
                $(this).children('span').attr("style", "color:green");
            }else{
                $(this).children('span').attr("style", "");
            }
        });
    }

    //� ���. ������ �����
    $(".num").on('input', function(e){
      this.value = this.value.replace(/[^\d]/ig, '');
    });

    $("#answers button#deleteAnswers").click(function () {
        if(confirm('�� ������������� ������ ������� �����?')){
            let target = $(this).attr('data-target');
            let id = $(target + ' input[name="answers"]').attr('data-id');
            let tbl = $(this).attr('data-object');

            let request = {'route': '<?=$__route?>',
                'action': 'actionDelete',
                'module': '<?=$__module?>',
                'context': '<?=$__context?>',
                'type': 'json',
                'data':  {'id' : id, 'tbl': tbl}
            };

            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/ajax_mode/',
                data: request,
                success: function( data ) {
                    if (data.success){
                        $('div'+target).remove();
                        $('#answers div#item_'+id).remove();
                    }else{
                        infoAlert('#answers', '<strong>������ ����!</strong> ���������� �����.');
                    }
                },
                error: function(){
                    infoAlert('#answers', '<strong>������ ����!</strong> ���������� �����.');
                }
            });
        }
    });

    $("#answers button#editAnswers").click(function () {
        let target = $(this).attr('data-target');
        let id = $(target + ' input[name="answers"]').attr('data-id');
        let isText = ($(target + ' input#textAnswer').prop('checked')) ? 1: 0;
        let val = $.trim($(target + ' input[name="answers"]').val());

        if(isText !=1){
            regexp.lastIndex = 0;
            if(regexp.test(val) !=1){
                infoModalErr(target, '�������� ������');
                $(target + ' input[name="answers"]').focus();
                return false;
            }
        }

        let request = {'route': '<?=$__route?>',
            'action': 'actionEdit',
            'module': '<?=$__module?>',
            'context': '<?=$__context?>',
            'type': 'json',
            'data':  {'id' : id, 'option': val, 'tbl': 'answers', 'isText': isText}
        };

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/ajax_mode/',
            data: request,
            success: function( data ) {
                if (data.success){
                    $('#item_'+id+" input.form-control").val(data.option);
                    infoModalSuc(target, '������ ��������!');
                    window.location.reload(true);
                }else{
                    infoModalErr(target, '������ ������!');
                }
            },
            error: function(){
                infoModalErr(target, '������ ������!');
            }
        });
    });

    $("#answers button#addAnswers").click(function () {
        let modal = $('#addAnswersModal');
        let isText = (modal.find('input#textAnswer').prop('checked')) ? 1: 0;
        let val = $.trim(modal.find('input[name="answers"]').val());

        if(isText!=1){
            regexp.lastIndex = 0;
            if(regexp.test(val) !=1){
                infoModalErr('#addAnswersModal', '�������� ������');
                $('#addAnswersModal input[name="answers"]').focus();
                return false;
            }
        }

        let request = {'route': '<?=$__route?>',
            'action': 'actionAdd',
            'module': '<?=$__module?>',
            'context': '<?=$__context?>',
            'type': 'json',
            'data': {'option': val, 'tbl': 'answers', 'isText': isText }
        };

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/ajax_mode/',
            data: request,
            success: function( data ) {
                if (data.success){
                    infoModalSuc('#addAnswersModal', '������ ���������!');
                    window.location.reload(true);
                }else{
                    infoModalErr('#addAnswersModal', '������ ������!');
                }
            },
            error: function(){
                infoModalErr('#addAnswersModal', '������ ������!');
            }
        });
    });

    //�������
    $("#questions button#addQuestion").click(function () {
        let modal = $('#addQuestionModal');
        let checkAnswer = modal.find('.list-group-item .input-group-addon input:checkbox:checked');
        let val = $.trim(modal.find('input[name="question"]').val());

        if(val ==''){
            infoModalErr('#addQuestionModal', '�������� ������!');
            modal.find('input[name="question"]').focus();
            return false;
        }

        if((checkAnswer.length > 1) || (checkAnswer.length < 1)){
            infoModalErr('#addQuestionModal', '�������� ���� ������� ������!');
            return false;
        }

        let answersId = $(checkAnswer).attr('data-id');
        let multiple = 0;
        let required = 0;
        if(modal.find('input#text_'+answersId).val() !== 'textarea'){
           multiple = (modal.find('input#multiple_'+answersId).prop('checked')) ? 1: 0;
           required = (modal.find('input#required_'+answersId).prop('checked')) ? 1: 0;
        }else{
            multiple = 2;
        }

        let request = {'route': '<?=$__route?>',
            'action': 'actionAdd',
            'module': '<?=$__module?>',
            'context': '<?=$__context?>',
            'type': 'json',
            'data': {
                'question': val,
                'answers': answersId,
                'tbl': 'questions',
                'active': (modal.find('input#activeQuestion').prop('checked')) ? 1: 0,
                'required': required,
                'multiple': multiple,
            }
        };

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/ajax_mode/',
            data: request,
            success: function( data ) {
                if (data.success){
                    infoModalSuc('#addQuestionModal', '������ ��������!');
                    window.location.reload(true);
                }else{
                    infoModalErr('#addQuestionModal', '������ ������!');
                }
            },
            error: function(){
                infoModalErr('#addQuestionModal', '������ ������!');
            }
        });
    });

    //����� �������������� ��������
    //���������� �����
    $(document).on('show.bs.modal', '#editQuestionModal', function (event) {
        let button = $(event.relatedTarget); // Button that triggered the modal
        let target = $("input"+button.attr('data-suorce'));
        let id = target.attr('data-id');
        let val = target.val();
        let answer = target.attr('data-answer');
        let active = (target.attr('data-active')==0) ? true : false;

        let modal = $(this);
        modal.find('.list-group-item input:checkbox').prop('checked', false);
        modal.find('input[name="question"]').val(val).attr('data-id', id);
        modal.find('input#activeQuestion').prop('checked', active);
        modal.find('input#multiple_'+answer).prop('checked', (target.attr('data-multiple')==0) ? false: true);
        modal.find('input#required_'+answer).prop('checked', (target.attr('data-required')==0) ? false: true);
        if(answer !=''){
            modal.find('.list-group-item input:checkbox').each(function(){
                if(answer == $(this).attr('data-id')){
                    $(this).prop('checked', true);
                }
            });
        }
    });
    //�������� ������ ����
    $(document).on('hide.bs.modal', '#questions .modal', function (event) {
        let modal = $(this);
        modal.find('input[name="question"]').val('').attr('data-id', '');
        modal.find('input#activeQuestion').prop('checked', false);
        modal.find('.list-group-item input:checkbox').prop('checked', false);
    });

    $("#editQuestionModal button#editQuestion").click(function () {
        let modal = $('#editQuestionModal');

        let val = $.trim(modal.find('input[name="question"]').val());
        if(val ==''){
            infoModalErr('#editQuestionModal', '�������� ������!');
            modal.find('input[name="question"]').focus();
            return false;
        }

        let checkAnswer = modal.find('.list-group-item .input-group-addon input:checkbox:checked');
        if((checkAnswer.length > 1) || (checkAnswer.length < 1)){
            infoModalErr('#editQuestionModal', '�������� ���� ������� ������!');
            return false;
        }

        let answersId = $(checkAnswer).attr('data-id');
        let multiple = 0;
        let required = 0;
        if($(checkAnswer).parent().parent().find('input.form-control').val() !== 'textarea'){
           multiple = (modal.find('input#multiple_'+answersId).prop('checked')) ? 1: 0;
           required = (modal.find('input#required_'+answersId).prop('checked')) ? 1: 0;
        }else{
           multiple = 2;
        }

        let request = {'route': '<?=$__route?>',
            'action': 'actionEdit',
            'module': '<?=$__module?>',
            'context': '<?=$__context?>',
            'type': 'json',
            'data': {
                'id': modal.find('input[name="question"]').attr('data-id'),
                'question': val,
                'answers': answersId,
                'tbl': 'questions',
                'active': (modal.find('input#activeQuestion').prop('checked')) ? 1: 0,
                'multiple': multiple,
                'required': required,
            }
        };

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/ajax_mode/',
            data: request,
            success: function( data ) {
                if (data.success){
                    infoModalSuc('#editQuestionModal', '������ �������!');
                    window.location.reload(true);
                }else{
                    infoModalErr('#tabContent', '<strong>������ ����!</strong> ���������� �����.');
                }
            },
            error: function(){
                infoModalErr('#tabContent', '<strong>������ ����!</strong> ���������� �����.');
            }
        });
    });


    //������, ������
    //��������
    $("#addWorksheetModal button#saveWorksheet").click(function () {
        let modal = $('#addWorksheetModal');

        let worksheetPrev = $.trim(modal.find('input[name="worksheetPrev"]').val());
        if(worksheetPrev ==''){
            infoModalErr('#addWorksheetModal', '�������� �������� ������, ������!');
            modal.find('input[name="worksheetPrev"]').focus();
            return false;
        }

        let questions = modal.find('.list-group-item input:checkbox:checked');
        let chekQuestions = [];
        if(questions.length > 0){
            questions.each(function(){
                let pos = modal.find('input[name="posQuestion_'+$(this).attr('data-id')+'"]').val();
                chekQuestions[chekQuestions.length] = {
                        id: $(this).attr('data-id'),
                        pos: (pos !='')? pos: 0,
                };
            });
        }

        let worksheetText = $.trim(modal.find('textarea[name="worksheetText"]').val());
        let worksheetPos = $.trim(modal.find('input[name="worksheetPos"]').val());
        let active = (modal.find('input#activeWorksheet').prop('checked')) ? 1: 0;
        let verdict = (modal.find('input#verdictWorksheet').prop('checked')) ? 1: 0;

        let request = {'route': '<?=$__route?>',
            'action': 'actionAdd',
            'module': '<?=$__module?>',
            'context': '<?=$__context?>',
            'type': 'json',
            'data': {
                'prev': worksheetPrev,
                'text': worksheetText,
                'pos': worksheetPos,
                'questions': JSON.stringify(chekQuestions),
                'active': active,
                'verdict': verdict,
                'tbl': 'worksheet'
            }
        };
        console.log(request);
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/ajax_mode/',
            data: request,
            success: function( data ) {
                if (data.success){
                    infoModalSuc('#addWorksheetModal', '������(�����) ��������!');
                    window.location.reload(true);
                }else{
                    infoModalErr('#addWorksheetModal', '������ ������!');
                }
            },
            error: function(){
                infoModalErr('#addWorksheetModal', '������ ������!');
            }
        });
    });

    //�������������
    $("#editWorksheetModal button#saveWorksheet").click(function () {
        let modal = $('#editWorksheetModal');

        let worksheetPrev = $.trim(modal.find('input[name="worksheetPrev"]').val());
        if(worksheetPrev ==''){
            infoModalErr('#editWorksheetModal .modal-content', '�������� �������� ������, ������!');
            modal.find('input[name="worksheetPrev"]').focus();
            return false;
        }

        let questions = modal.find('.list-group-item input:checkbox:checked');
        let chekQuestions = [];
        if(questions.length > 0){
            questions.each(function(){
                let pos = modal.find('input[name="posQuestion_'+$(this).attr('data-id')+'"]').val();
                chekQuestions[chekQuestions.length] = {
                        id: $(this).attr('data-id'),
                        pos: (pos !='')? pos: 0,
                };
            });
        }

        let id = modal.find('input[name="worksheetPrev"]').attr('data-id');
        let worksheetText = $.trim(modal.find('textarea[name="worksheetText"]').val());
        let worksheetPos = modal.find('input[name="worksheetPos"]').val();
        let active = (modal.find('input#activeWorksheet').prop('checked')) ? 1: 0;
        let verdict = (modal.find('input#verdictWorksheet').prop('checked')) ? 1: 0;

        let request = {'route': '<?=$__route?>',
            'action': 'actionEdit',
            'module': '<?=$__module?>',
            'context': '<?=$__context?>',
            'type': 'json',
            'data': {
                'id': id,
                'prev': worksheetPrev,
                'text': worksheetText,
                'pos': worksheetPos,
                'questions': JSON.stringify(chekQuestions),
                'active': active,
                'verdict': verdict,
                'tbl': 'worksheet'
            }
        };

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/ajax_mode/',
            data: request,
            success: function( data ) {
                if (data.success){
                    infoModalSuc('#editWorksheetModal', '������(�����) ��������!');
                    window.location.reload(true);
                }else{
                    infoModalErr('#editWorksheetModal', '������ ������!');
                }
            },
            error: function(){
                infoModalErr('#editWorksheetModal', '������ ������!');
            }
        });
    });

    //����� �������������� ������, ������
    //���������� �����
    $(document).on('show.bs.modal', '#editWorksheetModal', function (event) {
        //������ ������
        let button = $(event.relatedTarget); // Button that triggered the modal
        let target = $("input"+button.attr('data-suorce'));
        let id = target.attr('data-id');
        let question = JSON.parse(target.attr('data-question'));

        let modal = $(this);
        modal.find('input[name="worksheetPrev"]').val(target.val()).attr('data-id', id);
        modal.find('textarea[name="worksheetText"]').val(target.attr('data-text'));
        modal.find('input[name="worksheetPos"]').val(target.attr('data-pos'));
        modal.find('input#activeWorksheet').prop('checked', (target.attr('data-active')==0) ? true : false);
        modal.find('input#verdictWorksheet').prop('checked', (target.attr('data-verdict')==1) ? true : false);

        if(question !='' && question.length > 0){
            modal.find('.list-group-item input:checkbox').each(function(){
                let id = $(this).attr('data-id');
                let q = question.find(item => item.id == id);
                if(typeof q != "undefined"){
                    $(this).prop('checked', true);
                    modal.find('input[name="posQuestion_'+q.id+'"]').val(q.pos);
                }
            });
        }
    });

    //�������� ����
    $(document).on('hide.bs.modal', '#worksheet .modal', function (event) {
        let modal = $(this);
        modal.find('input[name="worksheetPrev"]').val('').attr('data-id', '');
        modal.find('.list-group-item input:checkbox').prop('checked', false);
        modal.find('input[name*="posQuestion_"]').val('');
        modal.find('textarea[name="worksheetText"]').val('');
        modal.find('input[name="worksheetPos"]').val('');
        modal.find('input#activeWorksheet').prop('checked', false);
        modal.find('input#verdictWorksheet').prop('checked', false);
    });


    //����� ����������
    $("#tabContent button#changeActivity").click(function () {
        let target = $(this).attr('data-suorce');
        let id = $("input"+target).attr('data-id');
        let active = $("input"+target).attr('data-active');
        let tbl = $("input"+target).attr('data-object');

        let request = {'route': '<?=$__route?>',
            'action': 'actionChangeActivity',
            'module': '<?=$__module?>',
            'context': '<?=$__context?>',
            'type': 'json',
            'data': {'id': id, 'active': active, 'tbl': tbl}
        };

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/ajax_mode/',
            data: request,
            success: function( data ) {
                if (data.success){
                    let chActive = (active==1) ? 0 : 1;
                    $("input"+target).attr('data-active', chActive);

                    if(chActive){
                        $('span'+target).removeClass('glyphicon-ok').prop('title', '�������').parent().prop('title', '�������');
                        $('span'+target).addClass('glyphicon-remove');
                    }else{
                        $('span'+target).removeClass('glyphicon-remove').prop('title', '������').parent().prop('title', '������');
                        $('span'+target).addClass('glyphicon-ok');
                    }
                }else{
                    infoAlert('#tabContent', '<strong>������ ����!</strong> ���������� �����.');
                }
            },
            error: function(){
                infoAlert('#tabContent', '<strong>������ ����!</strong> ���������� �����.');
            }
        });
    });



    //����� ���������� ����� �����������
    $("#tabContent button#changeActivGroupsByFacult").click(function () {
        let button = $(this);
        let facId = button.attr('data-id');
        let arr = [];
        let active = button.attr('data-active');

        $('div#'+facId).find('button.btn.btn-default').each(function(e){
            let id = parseInt($(this).attr('data-id'));
            if(id !=0 && !isNaN(id)){
                arr.push(parseInt(id));
            }
        });

        let request = {'route': '<?=$__route?>',
            'action': 'changeActivAllGroupsFaculty',
            'module': '<?=$__module?>',
            'context': '<?=$__context?>',
            'type': 'json',
            'data': JSON.stringify({
                'id_groups': arr,
                'active': active
            })
        };

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/ajax_mode/',
            data: request,
            success: function( data ) {
                if (data.success){
                    let chActive = (active==1) ? 0 : 1;
                    button.attr('data-active', chActive);

                    if(chActive){
                        $('span#faculties_'+facId).removeClass('glyphicon-ok').prop('title', '������� ������').parent().prop('title', '������� ������');
                        $('span#faculties_'+facId).addClass('glyphicon-remove');
                        changeActiveGroups(facId, chActive);
                    }else{
                        $('span#faculties_'+facId).removeClass('glyphicon-remove').prop('title', '������ ������').parent().prop('title', '������ ������');
                        $('span#faculties_'+facId).addClass('glyphicon-ok');
                        changeActiveGroups(facId, chActive);
                    }
                }else{
                    infoAlert('#tabContent', '<strong>������ ����!</strong> ���������� �����.');
                }
            },
            error: function(){
                infoAlert('#tabContent', '<strong>������ ����!</strong> ���������� �����.');
            }
        });
    });

    //�������� �������, ������
    $("#tabContent button#delete").click(function () {
        let target = $(this).attr('data-suorce');
        let tbl = $("input"+target).attr('data-object');
        let questOrWork = '';

        switch (tbl) {
            case 'questions':
                questOrWork = '���� ������?';
                break;
            case 'worksheet':
                questOrWork = '��� ������(�����)?';
                break;
            default:
                break;
        }

        if(confirm('�� ������������� ������ ������� ' + questOrWork)){
            let id = $("input"+target).attr('data-id');
            let request = {'route': '<?=$__route?>',
                'action': 'actionDelete',
                'module': '<?=$__module?>',
                'context': '<?=$__context?>',
                'type': 'json',
                'data':  {'id': id, 'tbl': tbl}
            };

            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/ajax_mode/',
                data: request,
                success: function( data ) {
                    if (data.success){
                        window.location.reload(true);
                        //$("input"+target).parents('div.list-group-item').remove();
                    }else{
                        infoAlert('#tabContent', '<strong>������ ����!</strong> ���������� �����.');
                    }
                },
                error: function(){
                    infoAlert('#tabContent', '<strong>������ ����!</strong> ���������� �����.');
                }
            });
        }
    });


    //������ / �������������

    //���������� �����
    $(document).on('show.bs.modal', '#editGroupTeacherModal', function (event) {
        //������ �����
        let button = $(event.relatedTarget); // Button that triggered the modal
        let id = button.attr('data-id');
        let name = button.attr('data-name');
        let formData = {};

        let modal = $(this);

        modal.find('input[name="groupName"]').val(name).attr('data-id', id);
        modal.find('input[name="groupActive"]').prop('checked', (button.attr('data-active')==0) ? true : false);

        formData.id = id;

        let request = {
            'route': '<?=$__route?>',
            'action': 'actionGetGroupById',
            'module': '<?=$__module?>',
            'context': '<?=$__context?>',
            'type': 'json',
            'data': formData
        };

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/ajax_mode/',
            data: request,
            success: function( data ) {
                if (data.length > 0){
                    data.forEach(function(elem, index) {
                        let tmp  = modal.find('span.list-group-item.tmp.hidden').clone(true).removeClass('tmp hidden').addClass('add_teacher');
                        tmp.find('span.name').attr('data-id', elem.people_id).html(elem.name);
                        modal.find('div.list-group.teachers_list').append(tmp);
                    });
                }
            },
            error: function(){
                infoModalErr('#editGroupTeacherModal', '������ �������!');
            }
        });

    });

    //�������� ����
    $(document).on('hide.bs.modal', '#editGroupTeacherModal', function (event) {
        let modal = $(this);
        modal.find('input[name="groupName"]').val('').attr('data-id', '');
        modal.find('input[name="searchTeacher"]').val('');
        modal.find('input[name="groupActive"]').prop('checked', false);
        modal.find('span.add_teacher').remove();
        modal.find('select.select_teacher > option').remove();
    });

    //����� ��������������
    $('#editGroupTeacherModal').on('keyup', 'input[name="searchTeacher"]', function(){
    	let text = $(this).val();
		let that = $(this);

		if(text.length < 3) return false;

		let request = {
		    'route': '<?=$__route?>',
            'action': 'actionSearchTeacher',
            'module': '<?=$__module?>',
            'context': '<?=$__context?>',
            'type': 'json',
            'data':  {
                'name': text
            }
        };

		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: '/ajax_mode/',
			data: request,
			success: function( data ) {
				var select = that.closest('div.input-group').find('select');
				select.empty();
				select.append('<option value=""></option>');
				$.each(data, function(i, val){
					select.append('<option value="' + val.id + '">' + val.name + '</option>');
				});
				select.find('option:eq(1)').attr('selected', 'selected');
				return false;
			}
		});
	});

    //���������� �����
    $('#updatePollsGroup').click(function(){
        let request = {
            'route': '<?=$__route?>',
            'action': 'actionUpdateGroup',
            'module': '<?=$__module?>',
            'context': '<?=$__context?>',
            'type': 'json',
        };

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/ajax_mode/',
            data: request,
            success: function( data ) {
                if (data.success){
                    infoSuccess('#tabContent', '<strong>������ ���������!</strong>');
                    window.location.reload(true);
                }else{
                    infoAlert('#tabContent', '<strong><strong>������ ����!</strong> ���������� �����.');
                }
            },
            error: function(){
                infoAlert('#tabContent', '<strong><strong>������ ����!</strong> ���������� �����.');
            }
        });
    });

    //������������� ������
    $('#editGroupTeacherModal').on('click', 'button[data-object=teacher],a[data-object=teacher]', function() {
		switch($(this).attr('data-action')) {
			case "delete": { //���� ��������������
				$(this).closest('span.list-group-item').remove();
			}
			break;

			case "add": { //��� ��������������
                let modal = $('#editGroupTeacherModal');
				let id = modal.find('select.select_teacher>option:selected').val();
				let name = modal.find('select.select_teacher>option:selected').html();
				let tmp  = modal.find('span.list-group-item.tmp.hidden').clone(true).removeClass('tmp hidden').addClass('add_teacher');
				tmp.find('span.name').attr('data-id', id).html(name);
                modal.find('div.list-group.teachers_list').append(tmp);
			}
			break;

		    case "edit": {//��� ���
                let modal = $('#editGroupTeacherModal');
                let formData = {};

                let name =  modal.find('input[name="groupName"]').val();
                formData.id = modal.find('input[name="groupName"]').attr('data-id');
                formData.active = (modal.find('input[name="groupActive"]').prop('checked')) ? 1 : 0;

                let addTeachers = modal.find('span.add_teacher');
                let teachers = [];
                if(addTeachers.length > 0){
                    addTeachers.each(function(){
                        teachers[teachers.length] = $(this).find('span.name').attr('data-id');
                    });

                    formData.teachers = teachers;
                }

                formData.tbl = 'groups';

                let request = {
                    'route': '<?=$__route?>',
                    'action': 'actionEdit',
                    'module': '<?=$__module?>',
                    'context': '<?=$__context?>',
                    'type': 'json',
                    'data': formData
                };

                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: '/ajax_mode/',
                    data: request,
                    success: function( data ) {
                        if (data.success){
                            infoModalSuc('#editGroupTeacherModal', '������ '+ name +' ��������!');
                            window.location.reload(true);
                        }else{
                            infoModalErr('#editGroupTeacherModal', '������ ������!');
                        }
                    },
                    error: function(){
                        infoModalErr('#editGroupTeacherModal', '������ ������!');
                    }
                });
            }
            break;
		}
		return false;
	});

    function infoAlert(id, text){
        let alert = '<div class="alert alert-danger" role="alert"><strong>'+text+'</div>';

        $(alert).appendTo(id);
        setTimeout(function (){
            $(id+' div.alert-danger').remove();
        }, 4000);
    };

    function infoSuccess(id, text){
        let success = '<div class="alert alert-success" role="alert"><strong>'+text+'</strong></div>';

        $(success).appendTo(id);
        setTimeout(function (){
            $(id+' div.alert-success').remove();
        }, 4000);
    };


    function infoModalErr(id, text){
        let inf = '<div class="alert alert-danger" role="alert"><strong>'+text+'</strong></div>';

        $(inf).appendTo(id + ' div.modal-body');
        setTimeout(function (){
            $(id+' div.alert-danger').remove();
        }, 4000);
    };

    function infoModalSuc(id, text){
        let inf = '<div class="alert alert-success" role="alert"><strong>'+text+'</strong></div>';

        $(inf).appendTo(id + ' div.modal-body');
        setTimeout(function (){
            $(id+' div.alert-success').remove();
        }, 4000);
    };
});</script>
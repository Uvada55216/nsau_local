<script src="/scripts/jquery/jquery.validate.min.js"></script>
<script type="text/javascript">
	"use strict";
	jQuery(document).ready(function($){
        var numbers = /[0-9]/;
        var hex = /_/;

	    //��������� �����
        $("form#vote").validate({
            highlight: function(element, errorClass, validClass) {
                $(element).parents('li.vote-item').addClass("invalidElem");
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('li.vote-item').removeClass("invalidElem");
            },
            errorElement: "span",
            errorClass: "errorMsg",
            submitHandler: function(form){
                let formData = {};
                let arrAnsw = [];

                //answers
                $(form).find('.vote-answers-list').each(function () {
                    let answers = $(this);
                    //option, checkbox
                    if (answers.prop('tagName').toLowerCase() == 'ol'){
                        answers.find('.vote-item-odd input').each(function () {
                            if ($(this).is(':checked')){
                                arrAnsw[arrAnsw.length] = $(this).val().split(hex);
                            }
                        });
                    }
                    //textarea
                    if (answers.prop('tagName').toLowerCase() == 'div'){
                        let m = answers.find('textarea').prop('id').split(hex);
                        m[m.length] = {
                            text: answers.find('textarea').val()
                        }
                        arrAnsw[arrAnsw.length] = m;
                    }
                });
                formData.choices = JSON.stringify(arrAnsw);

                //worksheet
                formData.worksheetId = $(form).attr('data-worksheet');

                //teacher, groupId
                let selTeacher = $(form).find('select[name="selectTeacher"]');
                if(selTeacher.get().length != 0){
                    //array is not empty
                        formData.teacher = {
                            people_id : selTeacher.find('option:selected').val(),
                            name : selTeacher.find('option:selected').html()
                        };

                        formData.groupId = selTeacher.attr('data-group');
                }

                let request = {
                    'route': '<?=$__route?>',
                    'action': 'actionAddVote',
                    'module': '<?=$__module?>',
                    'context': '<?=$__context?>',
                    'type': 'json',
                    'data': formData
                };

                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: '/ajax_mode/',
                    data: request,
                    success: function( data ) {
                        if (data.success){
                            infoSuccess('#vote', '����� ��������!');
                            $(form).find('input').prop('checked', false);
                            $(form).find('textarea').val('')
                        }else{
                            infoAlert('#vote', '������ ������!');
                        }
                    },
                    error: function(){
                        infoAlert('#vote', '������ ������!');
                    }
                });
            }
        });


        function infoAlert(id, text){
            let alert = '<div class="alert alert-danger" role="alert"><strong>'+text+'</div>';

            $(alert).appendTo(id);
            setTimeout(function (){
                $(id+' div.alert-danger').remove();
            }, 6000);
        };

        function infoSuccess(id, text){
            let success = '<div class="alert alert-success" role="alert"><strong>'+text+'</strong></div>';

            $(success).appendTo(id);
            setTimeout(function (){
                $(id+' div.alert-success').remove();
            }, 6000);
        };

});</script>
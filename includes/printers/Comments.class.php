<?
class Comments {
	
	const SUCCESS = "Коментарий добавлен.";
	const EMPTY_TEXT = "Не введен текст.";
	const WRONG_REQUEST_ID = "Некоректный идентификатор заявки.";
	
	private $module_id;
	
	public function __construct($module_id) {	
		$this->module_id = $module_id;
	}
	
	public function addComment($data) {
		global $DB, $Engine;
		if(!$this->validateComment($data)) {
			return false;
		}
		$DB->SetTable("printers_comments");
		$DB->AddValue("request_id", $data["request_id"]);
		$DB->AddValue("date", date("Y-m-d"));
		$DB->AddValue("text", iconv("utf-8", "cp1251", htmlspecialchars($data["text"])));
		$DB->Insert();
		$this->setResponse("good", SUCCESS);
		$Engine->LogAction($this->module_id, "comment".$data["request_id"], $DB->LastInsertId(), "add");	
	}
	
	private function validateComment($data) {
		global $DB;
		$DB->SetTable("printers_requests");
		$DB->AddCondFS("id", "=", $data["request_id"]);
		$r = $DB->FetchAssoc($DB->Select(1));
		if(empty($r["id"])) {
			$this->setResponse("bad", WRONG_REQUEST_ID);
			return false;
		}
		if(empty($data["text"])) {
			$this->setResponse("bad", EMPTY_TEXT);
			return false;
		}
		return true;
	}
	
	public function deleteComment($id) {
		
	}
	
	public function getComments($request_id) {
		global $DB;
		$DB->SetTable("printers_comments");
		$DB->AddCondFS("request_id", "=", $request_id);
		$DB->AddOrder("date");
		$res = $DB->Select();
		while($row = $DB->FetchAssoc($res)) {
			$row["date"] = CF::reverseDate($row["date"]);
			$result[$row["date"]][$row["id"]] = $row["text"];
		}
		return $result;
	}
	
	public function getLastComments($request_id) {
		global $DB;
		$DB->SetTable("printers_comments");
		$DB->AddCondFS("request_id", "=", $request_id);
		$DB->AddOrder("date", true);
		$ld = $DB->FetchAssoc($DB->Select(1));
		
		$DB->SetTable("printers_comments");
		$DB->AddCondFS("request_id", "=", $request_id);
		$DB->AddCondFS("date", "=", $ld["date"]);
		$DB->AddOrder("date");
		$res = $DB->Select();
		while($row = $DB->FetchAssoc($res)) {
			$row["date"] = CF::reverseDate($row["date"]);
			$result[$row["date"]][$row["id"]] = $row["text"];
		}
		return $result;
	}
	
	public function getResponse($array) {
		return $this->response;
	}
	
	private function setResponse($key, $val) {
		$this->response[$key][] = $val;
	}
		
}
<?
class Item {

	const NOT_FOUND_ID = "Устройство с данным id не найдено.";
	const NOT_FOUND_INVENTORY = "Устройство с данным инвентарным номером не найдено.";
	const IS_REGISTERED = "Устройство зарегистрировано.";
	const EDIT_SUCCESS = "Редактирование успешно.";
	const IS_REGISTERED_BAD = "Устройство с данным инвентарным номером уже зарегистрировано.";
	
	private $module_id;
	private $response = array();
	private $database_fields = array("unit_id", "responsible_person_id",  "responsible_person",  "auditorium",  "inventory",  "serial",  "device_id",  "building_id");
	private $extended_database_fields = array("date_start", "start_price");
	
	public function __construct($module_id) {	
		$this->module_id = $module_id;
	}
	
	public function addItem($array) {
		global $DB, $Engine;
		if(!$this->validateItem($array)) return false;
		
		$DB->AddTable("printers_register");
		foreach($this->database_fields as $field) {
			$DB->AddValue($field, trim(iconv("utf-8", "cp1251", $array[$field])));
		}
		$DB->Insert();
		$this->setResponse("good", self::IS_REGISTERED); 
		$Engine->LogAction($this->module_id, "register device", $DB->LastInsertId(), "add");	
		return true;
	}
	
	public function editItem($array) {
		global $DB, $Engine;
		if(!$this->validateItem($array, $array["id"])) return false;
		
		$DB->AddTable("printers_register");
		$DB->AddCondFS("id", "=", $array["id"]);
		foreach($this->database_fields as $field) {
			$DB->AddValue($field, trim(iconv("utf-8", "cp1251", $array[$field])));
		}
		foreach($this->extended_database_fields as $field) {
			$DB->AddValue($field, trim(iconv("utf-8", "cp1251", $array[$field])));
		}
		$DB->Update();
		$this->setResponse("good", self::EDIT_SUCCESS); 
		$Engine->LogAction($this->module_id, "register device", $array["id"], "edit");	
		return true;
	}
	
	private function validateItem($array, $edit_id= null) {
		global $DB;
		$DB->SetTable("printers_register");
		if(!empty($edit_id)) {
			$DB->AddCondFS("id", "!=", $edit_id);
		}
		$DB->AddCondFS("inventory", "=", $array["inventory"]);
		$q = $DB->FetchAssoc($DB->Select(1));
		if(!empty($q)) {
			$this->setResponse("bad", self::IS_REGISTERED_BAD); 
			return false;
		} 
		return true;
	}
	
	public function getResponse($array) {
		return $this->response;
	}
	
	private function setResponse($key, $val) {
		$this->response[$key][] = $val;
	}
	
	public function deleteItem($id) {
		
	}
	
	public function selectItem($id) {
		global $DB;
		$DB->SetTable("printers_register");
		$DB->AddCondFS("id", "=", $id);
		$r = $DB->FetchAssoc($DB->Select(1));
		if(!empty($r)) {
			return $r;
		} else {
			$this->setResponse("bad", self::NOT_FOUND_ID); 
			return false;
		}
	}
	
	public function getItemInfo($id) {
		$item = $this->selectItem($id);
		$item["device"] = $this->getDeviceName($item["device_id"]);
		$item["man"] = $this->getDeviceInfo($item["device_id"]);
		$item["unit"] = $this->getUnitPath($item["unit_id"], 1);
		$item["building"] = $this->getBuiding($item["building_id"]);
		$item["responsible_person"] = (empty($item["responsible_person_id"]) ? $item["responsible_person"] : $this->getPersonName($item["responsible_person_id"]));
		return $item;
	}
	
	private function getDeviceInfo($id) {
		global $DB;
		$DB->SetTable("printers_devices");
		$DB->AddCondFS("id", "=", $id);
		$row = $DB->FetchAssoc($DB->Select(1));
		return $row;
	}

	private function getBuiding($id) {
		global $DB;
		$DB->SetTable("nsau_buildings");
		$DB->AddCondFS("id", "=", $id);
		$row = $DB->FetchAssoc($DB->Select(1));
		return $row;		
	}
	
	private function getPersonName($id) {
		global $DB;
		$DB->SetTable("nsau_people");
		$DB->AddCondFS("id", "=", $id);
		$row = $DB->FetchAssoc($DB->Select(1));
		return $row["last_name"] . " " . $row["name"] . " " . $row["patronymic"];		
	}
	
	private function getUnitPath($id, $d = null) {
		global $DB;
		$DB->SetTable("printers_units");
		$DB->AddCondFS("id", "=", $id);
		$row = $DB->FetchAssoc($DB->Select(1));
		// if($level) return $row["name"];
		$dep = 0;
		if(!empty($row["pid"]) && ($row["pid"] != $row["id"]) && ($dep<$d)) 
		{
			$result = $this->getUnitPath($row["pid"], --$d)."/".$row["name"];		
		}
		else
		{
			$result = $result.$row["name"];
		}
		$dep++;
		return $result;
	}
	
	private function getDeviceName($id) {
		global $DB;
		$DB->SetTable("printers_devices");
		$DB->AddCondFS("id", "=", $id);
		$row = $DB->FetchAssoc($DB->Select(1));
		$DB->SetTable("cartrige_manufacturers");
		$DB->AddCondFS("id", "=", $row["manufacturer_id"]);
		$man = $DB->FetchAssoc($DB->Select(1));
		return $man["name"] . " " . $row["name"];		
	}
	
	public function searchItem($inventory) {
		global $DB;
		$DB->SetTable("printers_register");
		$DB->AddCondFS("inventory", "LIKE", "".iconv("utf-8", "Windows-1251", $inventory)."%");
		$res = $DB->Select();
		while($row = $DB->FetchAssoc($res)) {
			$result[] = $row;
		}
		if(!empty($result)) {
			return $result;
		} else {
			$this->setResponse("bad", self::NOT_FOUND_INVENTORY); 
			return false;
		}
	}
		
}
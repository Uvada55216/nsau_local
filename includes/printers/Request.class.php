<?
require_once INCLUDES . "/printers/" . "Item" . CLASS_EXT ;
require_once INCLUDES . "/printers/" . "Comments" . CLASS_EXT ;
class Request {

	const SUCCESS = "������ ����������.";
	const EDIT_SUCCESS = "������ ���������������.";
	const EMPTY_REASON = "�� ������� �������.";
	const INCORRECT_DEVICE_ID = "������������ ������������� ����������.";
	const IS_OPENED = "������ �� ������ ���������� ��� ����������.";
	const NO_REQUESTS = "��� ������.";


	private $module_id;
	private $response = array();
	private $extended_database_fields = array("register_id", "reason");
	
	public function __construct($module_id) {	
		$this->module_id = $module_id;
	}
	
	public function addRequest($array) {
		global $DB, $Auth, $Engine;
		if(!$this->validateRequest($array)) return false;
		$DB->AddTable("printers_requests");
		foreach($this->extended_database_fields as $field) {
			$DB->AddValue($field, iconv("utf-8", "cp1251", $array[$field]));
		}
		$DB->AddValue("status", 1);
		$DB->AddValue("date_open", date("Y-m-d"));
		$DB->AddValue("user_id", $Auth->user_id);		
		$DB->Insert();
		
		$this->setResponse("good", self::SUCCESS); 
		$Engine->LogAction($this->module_id, "request", $DB->LastInsertId(), "add");	
		return true;
	}
	
	public function editRequest($array) {
		global $DB, $Engine;
		$DB->AddTable("printers_requests");
		$DB->AddCondFS("id", "=", $array["id"]);
		$DB->AddValue("status", iconv("utf-8", "cp1251", $array["status"]));
		if(iconv("utf-8", "cp1251", $array["status"])=="���������") {
			$DB->AddValue("date_close", date("Y-m-d"));
		} else {
			$DB->AddValue("date_close", NULL);
		}
		$DB->Update();
		
		$this->setResponse("good", self::EDIT_SUCCESS); 
		$Engine->LogAction($this->module_id, "request", $array["id"], "edit");	
		return true;
	}
	
	public function getResponse($array) {
		return $this->response;
	}
	
	private function setResponse($key, $val) {
		$this->response[$key][] = $val;
	}
	
	private function validateRequest($data) {
		global $DB;
		$DB->SetTable("printers_register");
		$DB->AddCondFS("id", "=", $data["register_id"]);
		$dev = $DB->FetchAssoc($DB->Select(1));
		if(empty($dev["id"])) {
			$this->setResponse("bad", self::INCORRECT_DEVICE_ID);
			return false;
		}
		if(empty($data["reason"])) {
			$this->setResponse("bad", self::EMPTY_REASON);
			return false;
		}
		$DB->SetTable("printers_requests");
		$DB->AddCondFS("register_id", "=", $data["register_id"]);
		$DB->AddAltFX("status", "=", 1);
		$DB->AddAltFX("status", "=", 2);
		$DB->AppendAlts();
		$req = $DB->FetchAssoc($DB->Select(1));
		if(!empty($req["id"])) {
			$this->setResponse("bad", self::IS_OPENED);
			return false;
		}
		return true;
	}	
	
	public function getRequests($status, $page = 0, $sort = false) {
		global $DB, $Auth;
		$items_per_page = 20;
		if(empty($page)) $page = 1;
		
		$DB->SetTable("printers_requests");
		$DB->AddCondFS("status", "=", $status);	
		if(!$this->isAdminUser()) {
			$DB->AddCondFS("user_id", "=", $Auth->user_id);
		}
		$res_count = $DB->Select();
		$count_items = $DB->NumRows($res_count);
		require_once INCLUDES . "Pager" . CLASS_EXT;
		$Pager = new Pager($count_items, $items_per_page, null, null, null, array("page"=>$page));  
		$result["pager_output"]  = $Pager->Act();
		$limit = $result["pager_output"]["db_limit"];
		$from = $result["pager_output"]["db_limit"]*$page-($page ? $limit : 0);

		
		$DB->SetTable("printers_requests");
		$DB->AddCondFS("status", "=", $status);
		if(!$this->isAdminUser()) {
			$DB->AddCondFS("user_id", "=", $Auth->user_id);
		}
		
		$DB->AddOrder("date_open", $sort);
		$res = $DB->Select($limit, $from);
		$Item = new Item();
		$Comments = new Comments();
		while($row = $DB->FetchAssoc($res)) {
			$row["device_info"] = $Item->getItemInfo($row["register_id"]);
			$row["comments"] = $Comments->getLastComments($row["id"]);
			$user_info = $Auth->getUserData(null, $row["user_id"]);
			$row["displayed_name"] = $user_info["displayed_name"];
			$result["data"][$row["id"]] = $row;
		}
		
		
		if(empty($result["data"])) {			
			$this->setResponse("bad", self::NO_REQUESTS);
			return false;
		}
		return $result;
	}
	
	public function getRequest($id) {
		global $DB, $Auth;
		$DB->SetTable("printers_requests");
		$DB->AddCondFS("id", "=", $id);
		$row = $DB->FetchAssoc($DB->Select(1));
		$Item = new Item();	
		$Comments = new Comments();
		$row["device_info"] = $Item->getItemInfo($row["register_id"]);
		$row["comments"] = $Comments->getComments($row["id"]);
		$user_info = $Auth->getUserData(null, $row["user_id"]);
		$row["displayed_name"] = $user_info["displayed_name"];
		
		if(empty($row)) {			
			$this->setResponse("bad", self::NO_REQUESTS);
			return false;
		}
		return $row;
	}
	
	
	private function isAdminUser() {
		global $Auth;
		if(($Auth->usergroup_id!=1) && ($Auth->user_id!=215)) return false;
		return true;
	}
	
	
	
	public function deleteRequest($id) {
		global $DB, $Engine;
		$DB->SetTable("printers_requests");
		$DB->AddCondFS("id", "=", $id);
		$DB->Delete();
		$DB->SetTable("printers_comments");
		$DB->AddCondFS("request_id", "=", $id);
		$DB->Delete();
		$Engine->LogAction($this->module_id, "request", $id, "delete");	
	}
			
}
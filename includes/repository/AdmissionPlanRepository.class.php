<?php

namespace repository;

use DOMDocument;
use engine\db\Db;


class AdmissionPlanRepository
{

	/**
	 * �������� ���� ��
	 */
	protected static $text = 'text_items';

	/**
	 * ID ���������
	 */
	protected static $secondary = 32491;//���

	protected static $bachelor = 24201;//�����������, ������ �����������

	protected static $magistracy = 24211;//������������

	protected static $higher = 25131;//�����������

	protected static $graduate = 24241;//�����������

	protected static $tomsk = 33241;//�����������, �����������

	/**
	 * @param $type
	 * @param $code
	 * @return array
	 */
	public static function get($type, $code){
		$plan = array();

		switch ($type){
			case 'bachelor':
				$res = self::getText(self::$bachelor);
				if($res){
					$html = iconv('windows-1251','utf-8', $res["text"]);
					$plan = self::getPlanBySpeciality($code, $html);
				}
				break;
			case 'higher':
				$res = self::getText(self::$higher);
				if($res){
					$html = iconv('windows-1251','utf-8', $res["text"]);
					$plan = self::getPlanBySpeciality($code, $html);
				}
				break;
			case 'magistracy':
				$res = self::getText(self::$magistracy);
				if($res){
					$html = iconv('windows-1251', 'utf-8', $res["text"]);
					$plan = self::getPlanBySpeciality($code, $html);
				}
				break;
			case 'graduate':
				$res = self::getText(self::$graduate);
				if($res){
					$html = iconv('windows-1251', 'utf-8', $res["text"]);
					$plan = self::getPlanBySpeciality($code, $html);
				}
				break;
			case 'secondary':
				$res = self::getText(self::$secondary);
				if($res){
					$html = iconv('windows-1251', 'utf-8', $res["text"]);
					$plan = self::getPlanBySpecialitySPO($code, $html);
				}
				break;
			case 'tomsk':
				$res = self::getText(self::$tomsk);
				if($res){
					$html = iconv('windows-1251','utf-8', $res["text"]);
					$plan = self::getPlanBySpeciality($code, $html);
				}
				break;
			default:
				break;
		}

		return $plan;
	}

	/**
	 * @param $id
	 * @return array
	 */
	private static function getText($id){
		return Db::init()->SetTable(self::$text)
			->AddCondFS('id', '=', (int)$id)
			->AddField('text')
			->SelectArray(1);
	}

	/**
	 * @param $code
	 * @param $html
	 * @return array
	 */
	private static function getPlanBySpeciality($code, $html){
		$dom = new DOMDocument();
		$dom->loadHTML(trim($html));
		$nod = $dom->getElementsByTagName('tbody');
		$planList = array();
		$budget = array(3,4,5,6,7,8);
		$comerce = array(9,10,11);

		if ($nod->length) {
			$trList = $nod->item(0)->getElementsByTagName('tr');
			if($trList->length){
				$i_tr = 0;
				foreach($trList as $tr){
					$tdList = $trList->item($i_tr)->getElementsByTagName('td');
					if($tdList->length){
						$i_td = 0;
						foreach($tdList as $td){
							if($tdList->item(0)->nodeValue == $code){
								$num = trim($tdList->item($i_td)->nodeValue);

								if(in_array($i_td, $budget, true) && is_numeric($num)){
									$planList['budget']+= $num;
								}
								if(in_array($i_td, $comerce, true) && is_numeric($num)){
									$planList['comerce']+= $num;
								}
							}
							$i_td++;
						}
					}
					$i_tr++;
				}
			}
		}

		return $planList;
	}

	/**
	 * @param $code
	 * @param $html
	 * @return array
	 */
	private static function getPlanBySpecialitySPO($code, $html){
		$dom = new DOMDocument();
		$dom->loadHTML(trim($html));
		$nod = $dom->getElementsByTagName('thead');
		$planList = array();
		$budget = array(2,4,6);
		$comerce = array(3,5,7);

		if ($nod->length) {
			$trList = $nod->item(0)->getElementsByTagName('tr');
			if($trList->length){
				$i_tr = 3;
				$nodeListLength = $trList->length;
				for ($i_tr; $i_tr < $nodeListLength; $i_tr++){
					$tdList = $trList->item($i_tr)->getElementsByTagName('td');
					if($tdList->length){
						$i_td = 0;
						foreach($tdList as $td){
							if($tdList->item(0)->nodeValue == $code){
								$num = trim($tdList->item($i_td)->nodeValue);

								if(in_array($i_td, $budget, true) && is_numeric($num)){
									$planList['budget']+= $num;
								}
								if(in_array($i_td, $comerce, true) && is_numeric($num)){
									$planList['comerce']+= $num;
								}
							}
							$i_td++;
						}
					}
				}
			}
		}

		return $planList;
	}
}
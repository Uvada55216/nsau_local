<?php

namespace repository;

use engine\db\Db;

class ExamRepository
{
	/**
	 * �������� ���� ��
	 */
	protected static $exams = 'nsau_exams';

	protected static $specExams = 'nsau_spec_exams';

	protected static $specExamsAdditional = 'nsau_spec_exams_additional';

	protected static $abitEduLevel = 'nsau_abit_edu_level';

	protected static $specialities = 'nsau_specialities';

	protected static $faculties = 'nsau_faculties';

	protected static $specType = 'nsau_spec_type';

	/**
	 * �������� �������� � ���� �������������� �������
	 *
	 * @param int $id|null
	 * @return array ������ ���������
	 */
	public static function getAssocExams($id = null){
		$profiles = Db::init()->SetTable(self::$exams, 'ex')
			->AddField('ex.id', 'id')
			->AddField('ex.name', 'name')
			->AddField('ex.n_pp', 'n_pp')
			->AddField('ex.spec', 'spec')
			->AddField('ex.min_vyz', 'min_vyz')
			->AddField('ex.max_vyz', 'max_vyz')
			->AddField('ex.min_mins', 'min_mins')
			->AddField('ex.prikaz', 'prikaz')
			->AddField('ex.required', 'required')
			->AddField('ex.edu_level_id', 'edu_level_id')
			->AddOrder('ex.n_pp')
		;

		if(!is_null($id)){
			$profiles->AddCondFS('id', '=', $id);
		}
		$arrRes = $profiles->SelectArray();

		return !empty($arrRes) ? $arrRes : null;
	}


	/**
	 * �������� �������� � ���� �������������� �������
	 *
	 * @param int $id|null
	 * @return array ������ ���������
	 */
	public static function getAssocAbitEduLevel($id = null){
		$arrRes = array();

		$profiles = Db::init()->SetTable(self::$abitEduLevel)
			->AddField('id')
			->AddField('name')
			->AddField('short_name')
			->AddOrder('id' )
		;

		if(!is_null($id)){
			$profiles->AddCondFS('id', '=', $id);
		}

		$arrRes = $profiles->SelectArray();

		return $arrRes;
	}

	/**
	 * �������� �������� � ���� �������������� �������
	 * ������ �� ������ �����������
	 * �� ������� ������� ����� �����������
	 *
	 * @param int $level | null
	 * @return array ������ ���������
	 */
	public static function getAssocExamsByEduLevel($level = null){
		$examList = self::getAssocExams();

		if(!empty($examList) && is_null($level)){
			return $examList;
		}else{
			$arrRes = array();
			foreach($examList as $k=>$exam){
				if($exam['edu_level_id'] != (int)$level) continue;
				$arrRes[] = $exam;
			}
		}

		return $arrRes;

	}

	/**
	 * �������� ������������� � ���� �������������� ������� ��� �����������
	 * �� ������ ���
	 *
	 * @param $passExam
	 * @param null $notExam
	 * @return array
	 */
	public static function getSpecialtyForAbitByScoresExam($passExam, $notExam = null){
		$arrRes = array();
		if(!empty($passExam)){
			$rs = Db::init()->SetTable(self::$specExams, 'spex')
				->AddTable(self::$exams, 'ex')
				->AddCondFX("spex.exam_id", "=", 'ex.id')
				->AddTable(self::$specialities, 'spec')
				->AddCondFX("spex.spec_id", "=", 'spec.id')
				->AddTable(self::$faculties, 'fac')
				->AddCondFX("spec.id_faculty", "=", 'fac.id')
				->AddTable(self::$specType, 'sptype')
				->AddCondFX("sptype.spec_id", "=", 'spec.id')
				->AddCondFX("spex.spec_id", "=", 'spec.id')
				->AddCondFX('spex.qual_id', '!=', 0)
				->AddCondFX('sptype.has_vacancies', '!=', 0)
				->AddCondFX("spex.exam_id",  "IN", "(".implode(",", $passExam).")")
				->AddField('ex.min_vyz', 'min_vyz')
				->AddField('ex.name', 'name')
				->AddField('spec.name', 'spec_name')
				->AddField('spex.spec_id', 'spec_id')
				->AddField('spex.speciality_code', 'speciality_code')
				->AddField('spex.type', 'type')
				->AddField('spex.exam_id', 'exam_id')
				->AddField('fac.link', 'fac_link')
				->AddField('fac.name', 'fac_name')
				->SelectArray();

			if(!empty($rs)){
				$exams = self::getExamsData($rs);
				$exams = self::filterByCountExams($exams);
			}

			if(!is_null($notExam) && !empty($notExam)){
				$exams = self::filterByExams($exams, $notExam);
			}

			//exams additional
			if(!empty($exams)){
				$arrId = self::getValueByKey($exams, 'spec_id');

				$examsAddit = Db::init()->SetTable(self::$specExamsAdditional, 'exm_addit')
					->AddTable(self::$exams, 'exm')
					->AddCondFX("exm_addit.exam_id", "=", 'exm.id')
					->AddCondFX("exm_addit.spec_id",  "IN", "(".implode(",", $arrId).")")
					->AddField('exm.min_vyz', 'min_vyz')
					->AddField('exm_addit.spec_id', 'spec_id')
					->AddField('exm.name', 'name')
					->AddField('exm_addit.exam_id', 'exam_id')
					->SelectArray();

				if(!empty($examsAddit)){
					$examsAddit = self::getExamsData($examsAddit);
				}

				if(!is_null($notExam) && !empty($notExam)){
					$examsAddit = self::filterByExams($examsAddit, $notExam);
				}

				if(!empty($examsAddit)){
					$examsAddit = self::filterByExams($examsAddit, $passExam, false);
				}
				if(!empty($examsAddit)){
					$arrRes = self::getSpecialitiesByExams($exams, $examsAddit);
					$arrRes = self::addAdmissionPlanBySpecialities($arrRes);
				}
			}

		}

		return $arrRes;
	}

	/**
	 * @return array
	 */
	public static function getAssocEduLevelWithExams(){
		$exams = ExamRepository::getAssocExamsByEduLevel();
		$eduLevels = ExamRepository::getAssocAbitEduLevel();

		if($exams && $eduLevels){
			foreach($eduLevels as $k => $level){
				foreach($exams as $exam)
					if($exam['edu_level_id'] == $level['id']){
						$eduLevels[$k]['exams'][] = $exam;
					}
			}
		}

		return $eduLevels;
	}

	/**
	 * @param $arr
	 * @param $key
	 * @return array
	 */
	private static function getValueByKey($arr, $key){
		$result = array();
		foreach($arr as $k=>$v){
			$result[] = $arr[$k][$key];
		}

		return $result;
	}

	/**
	 * @param $arr
	 * @param $exam
	 * @return array
	 */
	private static function filterByExams($arr, $exam, $not = true){
		if(!empty($arr) && !empty($exam)){
			foreach($arr as $k=>$i){
				if(!empty($i['exams'])){
					foreach($i['exams'] as $ex){
						if(in_array($ex['exam_id'], $exam)){
							if($not){
								unset($arr[$k]);
							}else{
								continue 2;
							}
						}
					}

					if($not){
						continue;
					}else{
						unset($arr[$k]);
					}
				}
			}
		}

		return $arr ? $arr : array();
	}

	/**
	 * @param $data
	 * @return array
	 */
	private static function getExamsData($data){
		$result = array();

		foreach($data as $key=>$el){
			$ex = array();
			if(!isset($result[$el['spec_id']])){
				$ex[] = array(
					'exam_id' => $el['exam_id'],
					'min_vyz' => $el['min_vyz'],
					'name' => $el['name']
				);

				$result[$el['spec_id']] = array(
					'spec_id' => $el['spec_id'],
					'exams' => $ex
				);

				if(isset($el['speciality_code'])){
					$result[$el['spec_id']]['speciality_code'] = $el['speciality_code'];
				}
				if(isset($el['type'])){
					$result[$el['spec_id']]['type'] = $el['type'];
				}
				if(isset($el['spec_name'])){
					$result[$el['spec_id']]['spec_name'] = $el['spec_name'];
				}
				if(isset($el['fac_link'])){
					$result[$el['spec_id']]['fac_link'] = $el['fac_link'];
				}
				if(isset($el['fac_name'])){
					$result[$el['spec_id']]['fac_name'] = $el['fac_name'];
				}

				continue;
			}

			$result[$el['spec_id']]['exams'][] = array(
				'exam_id' => $el['exam_id'],
				'min_vyz' => $el['min_vyz'],
				'name' => $el['name']
			);
		}

		return $result;
	}

	/**
	 * @param $exams
	 * @param $examsAddit
	 * @return array
	 */
	private static function getSpecialitiesByExams($exams, $examsAddit){
		$result = array();
		foreach($examsAddit as $k=>$el){
			if(isset($exams[$k])){
				$exam = $exams[$k];
				$exam['exams_addit'] = $el['exams'];
				$result[$k] = $exam;
			}
		}

		return $result;
	}

	/**
	 * @param $arrSpec
	 * @param int $cntExam
	 * @return array
	 */
	private static function filterByCountExams($arrSpec, $cntExam = 2){
		if(!empty($arrSpec)){
			foreach($arrSpec as $k=>$i){
				if(!empty($i['exams']) && ((count($i['exams']) < $cntExam) || (count($i['exams']) > $cntExam))){
					unset($arrSpec[$k]);
					continue;
				}
			}
		}

		return $arrSpec ? $arrSpec : array();
	}

	/**
	 * @param $specialities
	 * @return array
	 */
	private static function addAdmissionPlanBySpecialities($specialities){
		if(!empty($specialities)){
			foreach($specialities as $id => $speciality){
				$type = ($speciality['fac_link'] === '/tomsk/')//bad practices
					? 'tomsk'
					: $speciality['type'];

				$admiss = AdmissionPlanRepository::get($type, $speciality['speciality_code']);

				if(!empty($admiss)){
					$specialities[$id] = array_merge($speciality, $admiss);
				}
			}
		}

		return $specialities ? $specialities : array();
	}

}
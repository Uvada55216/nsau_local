<?php

namespace repository;

use engine\db\Db;
use entity\Profile;

class ProfileRepository{

	/**
	 * �������� ���� ��
	 */
	protected $dbTable = 'nsau_profiles';

	public function getTableName(){
		return $this->dbTable;
	}

	/**
	 * �������� ������� � ���� �������������� �������
	 */
	public function getAssocProfiles($id = null){
		$profiles = Db::init()->SetTable($this->dbTable)
			->AddField('id')
			->AddField('spec_id')
			->AddField('name')
			->AddOrder('id')
		;

		if(!is_null($id)){
			$profiles->AddCondFS('id', '=', $id);
		}
		$arrResult = $profiles->SelectArray();

		if($arrResult){
			return $arrResult;
		}else{
			return null;
		}
	}

	/**
	 * �������� ������� � ���� ��������
	 */
	public function getProfiles($id = null){
		$arrObj = array();
		$arrProf = $this->getAssocProfiles($id);

		if(!empty($arrProf) && count($arrProf) > 0){
			foreach($arrProf as $profile){
				$arrObj[] = new Profile($profile);
			}
		}
		return $arrObj;
	}

	/**
	 * �������� ������� � ���� ��������
	 */
	public function getProfilesBySpecId($id, $obj = true){
		$id = (int)$id;
		$arrObj = array();
		$arrProf = $this->getAssocProfiles();

		if(!empty($arrProf) && count($arrProf) > 0){
			foreach($arrProf as $profile){
				if($profile['spec_id'] == $id){
					$arrObj[] = $obj ? new Profile($profile) : $profile;
				}
			}
		}
		return $arrObj;
	}
}
<?php

namespace repository;

use engine\db\Db;
use entity\Speciality;

class SpecialityRepository{

	/**
	 * �������� ���� ��
	 */
	protected $dbTable = 'nsau_specialities';

	public function getTableName(){
		return $this->dbTable;
	}

	/**
	 * �������� ������������� � ���� �������������� �������
	 */
	public function getAssocSpecialities($id = null){
		$speciality = Db::init()->SetTable($this->dbTable)
			->AddField('id')
			->AddField('id_faculty')
			->AddField('code')
			->AddField('name')
			->AddField('direction')
			->AddField('year_open')
			->AddField('description')
			->AddField('data_training')
			->AddField('old')
			->AddField('old_id')
			->AddField('hide_accredit')
			->AddField('code1c')
			->AddOrder('id')
		;

		if(!is_null($id)){
			$speciality->AddCondFS('id', '=', (int)$id);
		}
		$arrResult = $speciality->SelectArray();

		if($arrResult){
			return $arrResult;
		}else{
			return null;
		}
	}

	/**
	 * �������� ������������� � ���� ��������
	 */
	public function getSpecialities(){
		$arrObj = array();
		$arrSpec = $this->getAssocSpecialities();

		if(!empty($arrSpec) && count($arrSpec) > 0){
			foreach($arrSpec as $spec){
				$arrObj[] = new Speciality($spec);
			}
		}
		return $arrObj;
	}

	/**
	 * �������� ������ �� id (���. ������/������)
	 * param int $id
	 * param bool $obj
	 * return array|Speciality|null
	 */
	public function getSpecialityById($id, $obj = true){
		$gr = $this->getAssocSpecialities($id);

		return $obj ? new Speciality($gr[0]) : $gr;
	}

	/**
	 * @return array
	 */
	public function getNewAssocSpecialities(){
		$arrResult = array();
		$arrSpec = $this->getAssocSpecialities();

		if(!empty($arrSpec) && count($arrSpec) > 0){
			foreach($arrSpec as $spec){
				if($spec['old'] == 1) continue;
				$arrResult[] = array(
					'id' => $spec['id'],
					'id_faculty' => $spec['id_faculty'],
					'code' => $spec['code'],
					'name' => $spec['name'],
				);
			}
		}

		return $arrResult;
	}

}
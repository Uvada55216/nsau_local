<?php

namespace repository\education;

use \engine\db\Db;
use helpers\EncodingHelper;

class TeacherEducation
{

	/**
	 * �������� ���� ��
	 */
	const PROGRAM = 'nsau_teacher_edu_program';

	/**
	 * @param $id
	 * @return array
	 */
	public static function getProgramByPeopleId($id){
		$result = array();

		$result = Db::init()->SetTable(self::PROGRAM, 'pr')
			->addCondFs('pr.people_id', '=', (int)$id)
			->AddField('pr.id', 'id')
			->AddField('pr.spec_id', 'spec_id')
			->AddOrder('pr.id')
			->SelectArray();

		return $result;
	}

}
<?php

namespace repository\faculty;

use entity\faculty\Faculty;
use modules\Group\core\GroupsClient1c;
use \engine\core1C\Helper1C as H;

class Faculty1cRepository implements FacultyInterface
{
	private $client1c;
	protected $data = array();

	public function __construct(GroupsClient1c $client1c){
		$this->client1c = $client1c;
		$dataClient = $this->client1c->getFaculties();

		if(isset($dataClient->Faculty)){
			foreach($dataClient->Faculty as $obj){
				$this->data[] = $this->iconvConvert(get_object_vars($obj));
			}
		}
	}

	/**
	 * Получить факультеты в виде объектов
	 */
	public function getFaculties(){
		$arrObj = array();
		if(count($this->data) > 0){
			foreach($this->data as $dt){
				$arrObj[] = new Faculty($dt);
			}
		}
		return $arrObj;
	}

	/**
	 * Получить факультеты в виде ассоциативного массива
	 */
	public function getAssocFaculties(){
		return $this->data;
	}

	protected function iconvConvert($faculty){
		$faculty['code1c'] = (int)$faculty['code1c'];
		$faculty['name'] = H::u2w($faculty['name']);
		$faculty['short_name'] = !empty($faculty['short_name']) ? H::u2w($faculty['short_name']) : '';

		return $faculty;
	}
}
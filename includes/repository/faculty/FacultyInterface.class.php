<?php

namespace repository\faculty;

interface FacultyInterface{

	/**
	 * Получить факультеты в виде ассоциативного массива
	 */
	public function getAssocFaculties();

	/**
	 * Получить факультеты в виде объектов
	 */
	public function getFaculties();

}
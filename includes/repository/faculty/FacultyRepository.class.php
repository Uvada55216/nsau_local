<?php

namespace repository\faculty;

use engine\db\Db;
use entity\faculty\Faculty;
use entity\group\Group;
use entity\Speciality;

class FacultyRepository implements FacultyInterface
{
	/**
	 * Название табл БД
	 */
	protected $dbTable = 'nsau_faculties';

	public function getTableName(){
		return $this->dbTable;
	}

	/**
	 * Получить факультеты в виде ассоциативного массива
	 * @param null $id
	 * @return array|null
	 */
	public function getAssocFaculties($id = null){
		$faculties = Db::init()->SetTable($this->dbTable)
			->AddField('id')
			->AddField('code1c')
			->AddField('pid')
			->AddField('pos')
			->AddField('is_active')
			->AddField('name')
			->AddField('short_name')
			->AddField('group_num')
			->AddField('groups')
			->AddField('link')
			->AddField('foundation_year')
			->AddField('closing_year')
			->AddField('comment')
			->AddOrder('id')
		;

		if(!is_null($id)){
			$faculties->AddCondFS('id', '=', (int)$id);
		}
		$arrResult = $faculties->SelectArray();

		if($arrResult){
			return $arrResult;
		}else{
			return null;
		}
	}

	/**
	 * Получить активные факультеты в виде ассоциативного массива
	 * @param null $id
	 * @return array|null
	 */
	public function getAssocActiveFaculties($id = null){
		$arr = $this->getAssocFaculties($id);

		if(!is_null($arr)){
			return array_filter($arr, function($el){
				return ($el['is_active'] != 0);
			}, ARRAY_FILTER_USE_BOTH);
		}

		return $arr;
	}



	/**
	 * Получить факультеты в виде объектов
	 */
	public function getFaculties(){
		$arrObj = array();
		$assocFaculty = $this->getAssocFaculties();

		if(!empty($assocFaculty) && count($assocFaculty) > 0){
			foreach($assocFaculty as $faculty){
				$arrObj[] = new Faculty($faculty);
			}
		}
		return $arrObj;
	}

	/**
	 * Получить факульт по id в виде объекта
	 */
	public function getFacultyById($id){
		$facObj = null;
		$assocFaculty = $this->getAssocFaculties($id);

		if(!empty($assocFaculty)){
			$facObj = new Faculty($assocFaculty[0]);
		}
		return $facObj;
	}

	/**
	 * Обновить факультеты
	 *
	 */
	public function updateFaculty(Faculty $faculty){

		$db = Db::init()->SetTable($this->dbTable)
			->AddCondFS("id", "=", $faculty->getId())
			->AddValue("name", $faculty->getName())
			->AddValue("short_name", $faculty->getShortName())
			->AddValue("code1c", $faculty->getCode1c())
			->AddValue("pid", $faculty->getPid())
			->AddValue("group_num", $faculty->getGroupNum())
			->AddValue("groups", $faculty->getGroups())
			->AddValue("link", $faculty->getLink())
			->AddValue("pos", $faculty->getPos())
			->AddValue("foundation_year", $faculty->getFoundationYear())
			->AddValue("closing_year", $faculty->getClosingYear())
			->AddValue("comment", $faculty->getComment())
			->AddValue("is_active", $faculty->getIsActive())
			->Update();

		return $db;
	}

	/**
	 * Удалить факультет
	 * + удалятся кафедры факультета
	 * и преподаватели закрепленные за кафедрами
	 */
	public function deleteFaculty($id){
		$dbDep = Db::init()->SetTable("nsau_departments")
			->AddCondFS("faculty_id", "=", (int)$id)
			->Select();

		while($arrDep = Db::init()->FetchAssoc($dbDep)){
			Db::init()->SetTable("nsau_teachers")
				->AddCondFS("department_id", "=", $arrDep["id"])
				->Delete();
		}

		Db::init()->SetTable("nsau_departments")
			->AddCondFS("faculty_id", "=", (int)$id)
			->Delete();

		$db = Db::init()->SetTable($this->dbTable)
			->AddCondFS("id", "=", (int)$id)
			->Delete();

		return $db;
	}

	/**
	 * Актуализировать факультеты с 1с
	 *
	 */
	public function agreeFacultyBy1c($arrObj1c){
		$data = $this->getFaculties();
		$arrResult = 0;

		foreach($arrObj1c as $obj1c){
			$result = 1;
			$rs = false;
			foreach($data as $objPortal){
				if(!is_null($objPortal->getCode1c()) && $objPortal->getCode1c() == $obj1c->getCode1c()){
					$objPortal->setName($obj1c->getName());
					$objPortal->setShortName($obj1c->getShortName());

					$dt = $this->updateFaculty($objPortal);

					if($dt){
						$result = 0;
						$rs = true;
					}
				}
			}
			if(!$rs){
				$dt = $this->addFaculty($obj1c);

				if($dt){
					$result = 0;
				}
			}
			$arrResult += $result;
		}

		if(!array_sum($arrResult)){
			return true;
		}

		return false;
	}

	/**
	 * Добавление нового факультета
	 *
	 */
	public function addFaculty($faculty){
		$dt = Db::init()->SetTable($this->dbTable)
			->AddValue("name", $faculty->getName())
			->AddValue("short_name", $faculty->getShortName())
			->AddValue("code1c", $faculty->getCode1c())
			->AddValue("pid", $faculty->getPid())
			->AddValue("group_num", $faculty->getGroupNum())
			->AddValue("groups", $faculty->getGroups())
			->AddValue("link", $faculty->getLink())
			->AddValue("pos", $faculty->getPos())
			->AddValue("foundation_year", $faculty->getFoundationYear())
			->AddValue("closing_year",$faculty->getClosingYear())
			->AddValue("comment", $faculty->getComment())
			->AddValue("is_active", $faculty->getIsActive())
			->Insert();

		return $dt;
	}

	/**
	 * Получить группы в виде объектов сгруппированных по факультетам
	 */
	public function getFacultyAndGroups(){

		$res = Db::init()->SetTable($this->dbTable, 'fc')
				->AddJoin('nsau_groups gr.id_faculty', 'fc.id')
				->AddField('fc.id', 'fc_id')
				->AddField('fc.code1c', 'fc_code1c')
				->AddField('fc.pid')
				->AddField('fc.pos')
				->AddField('fc.is_active')
				->AddField('fc.name', 'fc_name')
				->AddField('fc.short_name')
				->AddField('fc.group_num')
				->AddField('fc.groups')
				->AddField('fc.link')
				->AddField('fc.foundation_year')
				->AddField('fc.closing_year')
				->AddField('fc.comment')
				->AddField('gr.id', 'gr_id')
				->AddField('gr.code1c', 'gr_code1c')
				->AddField('gr.name', 'gr_name')
				->AddField('gr.id_faculty')
				->AddField('gr.specialities_code')
				->AddField('gr.form_education')
				->AddField('gr.id_spec')
				->AddField('gr.year')
				->AddField('gr.hidden')
				->AddField('gr.profile')
				->AddField('gr.qualification')
				->AddOrder('fc.id')
				->SelectArray()
		;
		$arrFac = array();

		if(!empty($res)){
			foreach($res as $el){
				if(!isset($arrFac[$el['fc_id']])){
					$f = new Faculty($el);
					$f->setId($el['fc_id']);
					$f->setCode1c($el['fc_code1c']);
					$f->setName($el['fc_name']);
					$arrFac[$el['fc_id']] = $f;
				}
				$el1 = $arrFac[$el['fc_id']];
				$arrGroups = $el1->getArrGroups();

				if(($el['gr_id'] > 0) && (!isset($arrGroups[$el['gr_id']]))){
					$gr = new Group($el);
					$gr->setId($el['gr_id']);
					$gr->setCode1c($el['gr_code1c']);
					$gr->setName($el['gr_name']);
					$arrGroups[$el['gr_id']] = $gr;
					$el1->setArrGroups($arrGroups);
				}
			}
		}
		return $arrFac;
	}

	/**
	 * Получить специальности в виде объектов сгруппированных по факультетам
	 */
	public function getFacultyAndSpecialities(){

		$res = Db::init()->SetTable($this->dbTable, 'fc')
			->AddJoin('nsau_specialities sp.id_faculty', 'fc.id')
			->AddField('fc.id', 'fc_id')
			->AddField('fc.code1c', 'fc_code1c')
			->AddField('fc.pid')
			->AddField('fc.pos')
			->AddField('fc.is_active')
			->AddField('fc.name', 'fc_name')
			->AddField('fc.short_name')
			->AddField('fc.group_num')
			->AddField('fc.groups')
			->AddField('fc.link')
			->AddField('fc.foundation_year')
			->AddField('fc.closing_year')
			->AddField('fc.comment')
			->AddField('sp.id', 'sp_id')
			->AddField('sp.code1c', 'sp_code1c')
			->AddField('sp.name', 'sp_name')
			->AddField('sp.id_faculty')
			->AddField('sp.code')
			->AddField('sp.direction')
			->AddField('sp.year_open')
			->AddField('sp.description')
			->AddField('sp.data_training')
			->AddField('sp.old')
			->AddField('sp.old_id')
			->AddField('sp.hide_accredit')
			->AddOrder('fc.id')
			->SelectArray()
		;
		$arrFac = array();

		if(!empty($res)){
			foreach($res as $el){
				if(!isset($arrFac[$el['fc_id']])){
					$f = new Faculty($el);
					$f->setId($el['fc_id']);
					$f->setCode1c($el['fc_code1c']);
					$f->setName($el['fc_name']);
					$arrFac[$el['fc_id']] = $f;
				}
				$el1 = $arrFac[$el['fc_id']];
				$arrSpec = $el1->getArrSpecialities();

				if(($el['sp_id'] > 0) && (!isset($arrSpec[$el['sp_id']]))){
					$gr = new Speciality($el);
					$gr->setId($el['sp_id']);
					$gr->setCode1c($el['sp_code1c']);
					$gr->setName($el['sp_name']);
					$arrSpec[$el['sp_id']] = $gr;
					$el1->setArrSpecialities($arrSpec);
				}
			}
		}
		return $arrFac;

	}
}
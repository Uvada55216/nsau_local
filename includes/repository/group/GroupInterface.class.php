<?php

namespace repository\group;

interface GroupInterface{

	/**
	 * Получить группы в виде ассоциативного массива
	 */
	public function getAssocGroups();

	/**
	 * Получить группы в виде объектов
	 */
	public function getObjectGroups();

}
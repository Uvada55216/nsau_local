<?php

namespace repository\group;

use engine\db\Db;
use entity\group\Group;

class GroupRepository implements GroupInterface
{
	/**
	 * �������� ���� ��
	 */
	protected $dbTable = 'nsau_groups';


	public function getTableName(){
		return $this->dbTable;
	}

	/**
	 * �������� ������ � ���� �������������� �������
	 */
	public function getAssocGroups($id = null){
		$groups = Db::init()->SetTable($this->dbTable)
			->AddField('id')
			->AddField('code1c')
			->AddField('name')
			->AddField('id_faculty')
			->AddField('specialities_code')
			->AddField('form_education')
			->AddField('id_spec')
			->AddField('year')
			->AddField('hidden')
			->AddField('profile')
			->AddField('qualification')
			->AddOrder('id')
		;

		if(!is_null($id)){
			$groups->AddCondFS('id', '=', (int)$id);
		}

		$arrResult = $groups->SelectArray();

		if($arrResult){
			return $arrResult;
		}else{
			return null;
		}
	}

	/**
	 * �������� ������ � ���� ��������
	 */
	public function getObjectGroups(){
		$arrObj = array();
		$assocGroups = $this->getAssocGroups();

		if(!empty($assocGroups) && count($assocGroups) > 0){
			foreach($assocGroups as $group){
				$arrObj[] = new Group($group);
			}
		}
		return $arrObj;
	}

	/**
	 * �������� ������ �� id (���. ������/������)
	 * param int $id
	 * param bool $obj
	 * return array|Group|null
	 */
	public function getGroupById($id, $obj = true){
		$gr = $this->getAssocGroups($id);

		return $obj ? new Group($gr[0]) : $gr;
	}

	/**
	 * �������� ������ �� name(���. ������/������)
	 *
	 */
	public function getGroupByName($name, $obj = true){
		$group = Db::init()->SetTable($this->dbTable)
			->AddField('id')
			->AddField('code1c')
			->AddField('name')
			->AddField('id_faculty')
			->AddField('specialities_code')
			->AddField('form_education')
			->AddField('id_spec')
			->AddField('year')
			->AddField('hidden')
			->AddField('profile')
			->AddField('qualification')
			->AddCondFS('name', '=', $name)
		;
		$res = $group->SelectArray(1);

		if($res){
			$res['name'] = iconv("cp1251", "utf-8", $res["name"]);
		}

		return $obj ? new Group($res) : $res;
	}

	/**
	 * �������� ������ �� id ���������� (���. ������/������)
	 */
	public function getGroupsByIdFaculty($idFac, $obj = true){
		$idFac = (int)$idFac;
		$groups = array();
		$arrGr = $this->getAssocGroups();

		if(!empty($arrGr) && count($arrGr) > 0){
			foreach($arrGr as $gr){
				if($gr['id_faculty'] == $idFac){
					$groups[] = $obj ? new Group($gr) : $gr;
				}
			}
		}
		return $groups;
	}

	/**
	 * �������� ������
	 *
	 */
	public function updateGroup(Group $group){

		$db = Db::init()->SetTable($this->dbTable)
			->AddCondFS("id", "=", $group->getId())
			->AddValue('code1c', $group->getCode1c())
			->AddValue('name', $group->getName())
			->AddValue('id_faculty', $group->getIdFaculty())
			->AddValue('specialities_code', $group->getSpecialitiesCode())
			->AddValue('form_education', $group->getFormEducation())
			->AddValue('id_spec', $group->getIdSpeciality())
			->AddValue('year', $group->getYear())
			->AddValue('hidden', $group->getHidden())
			->AddValue('profile', $group->getProfile())
			->AddValue('qualification', $group->getQualification())
			->Update()
		;

		return $db;
	}

	/**
	 * �������� ����� ������
	 */
	public function addGroup(Group $group){
		$db = Db::init()->SetTable($this->dbTable)
			->AddValue('name', $group->getName())
			->AddValue('code1c', $group->getCode1c())
			->AddValue('id_faculty', $group->getIdFaculty())
			->AddValue('specialities_code', $group->getSpecialitiesCode())
			->AddValue('form_education', $group->getFormEducation())
			->AddValue('id_spec', $group->getIdSpeciality())
			->AddValue('year', $group->getYear())
			->AddValue('hidden', $group->getHidden())
			->AddValue('profile', $group->getProfile())
			->AddValue('qualification', $group->getQualification())
			->Insert()
		;

		return $db;
	}

	/**
	 * �������� ������ � ���� �������������� ������� �� ���� 1�
	 *
	 * @param $code id group by 1c
	 * @return array|null
	 */
	public function getGroupByCode($code){
		$group = Db::init()->SetTable($this->dbTable)
			->AddField('id')
			->AddField('code1c')
			->AddCondFS('code1c', '=', (int)$code)
			->AddField('name')
			->AddField('id_faculty')
			->AddField('specialities_code')
			->AddField('form_education')
			->AddField('id_spec')
			->AddField('year')
			->AddField('hidden')
			->AddField('profile')
			->AddField('qualification')
			->AddOrder('id')
		;

		$res = $group->SelectArray(1);

		if($res){
			$res['name'] = iconv("cp1251", "utf-8", $res["name"]);
			return $res;
		}

		return null;
	}

	/**
	 * ������� ������
	 */
	public function deleteGroup(Group $group){
		$db = Db::init()->SetTable($this->dbTable)
			->AddCondFS("id", "=", $group->getId())
			->Delete();

		return $db;
	}
}
<?
namespace requests;

use helpers\EncodingHelper as H;
use forms\FormFilter;
use forms\FormValidator;
use forms\FinalProcess;
use forms\Form;

class Request 
{

	protected $rules;
	protected $formData = array();

	public static function init($rules = null) 
	{
		return new self($rules);
	}

	protected function __construct($rules = null) 
	{
		$this->rules = $rules;
	}

	public function post()
	{
		$this->formData = $_POST;
		$this->validate();
		return $this->formData;
	}

	public function get()
	{
		$this->formData = $_GET;
		$this->validate();
		return $this->formData;
	}



	public function ajax()
	{
		if(!empty($_REQUEST['data'])) {
			$this->formData = H::changeArrayEncoding($_REQUEST['data'], 'u2w');
		} 
		$this->validate();
		return $this->formData;
	}

    public function jsonAjax($arrayEncoding = null)
    {
        $this->formData = json_decode($_REQUEST['data'], true);
        if ($arrayEncoding) {
            $this->formData = H::changeArrayEncoding($this->formData, $arrayEncoding);
        }
        return $this->formData;
    }

	private function validate() 
	{
		if(!empty($this->rules)) {
			$FormValidate = new FormFilter(new FormValidator(new FinalProcess()));
			$FormValidate->process($Form = new Form($this->rules, $this->formData));
			if(!$Form->isValid()) {
				throw new RequestException($Form->getErrors());
			} 
			$this->formData = $Form->getData();
		}
	}




}
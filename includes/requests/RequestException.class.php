<?php
namespace requests;


class RequestException extends \Exception
{
	private $messages;

	public function __construct($messages = null)
	{
		$this->messages = $messages;
	}

	public function getErrors() {
		return $this->messages;
	}

}
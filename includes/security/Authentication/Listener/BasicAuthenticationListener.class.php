<?php

namespace security\Authentication\Listener;

use engine\Http\HttpFoundation\Request;
use engine\Security\Authentication\AuthenticationManagerInterface;
use engine\Security\Authentication\Token\Storage\TokenStorageInterface;
use engine\Security\Exception\AuthenticationException;
use engine\Security\Firewall\AbstractListener;
use security\Authentication\Token\HttpBasicToken;


/**
 * BasicAuthenticationListener implements Basic HTTP authentication.
 *
 */
class BasicAuthenticationListener extends AbstractListener
{
    private $tokenStorage;
    private $authenticationManager;
    private $ignoreFailure;

    public function __construct(TokenStorageInterface $tokenStorage, AuthenticationManagerInterface $authenticationManager)
    {
        $this->tokenStorage = $tokenStorage;
        $this->authenticationManager = $authenticationManager;
        $this->ignoreFailure = false;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request)
    {
        return $request->headers->get('PHP_AUTH_USER') != '';
    }

    /**
     * Handles basic authentication.
     */
    public function authenticate(Request $request)
    {
        if ('' == $username = $request->headers->get('PHP_AUTH_USER')) {
	        throw new AuthenticationException('Authentication failed. Username is empty.');
	        exit;
        }

	    $token = $this->authenticationManager->authenticate(new HttpBasicToken($username, $request->headers->get('PHP_AUTH_PW')));

	    if ($token instanceof HttpBasicToken && $token->getUsername() == $username && $token->isAuthenticated()) {
		    $this->tokenStorage->setToken($token);
		    return;
	    }

		throw new AuthenticationException('Authentication failed. Token is isset');
	    exit;
    }
}

<?php

namespace security\Authentication\Provider;

use engine\Security\Authentication\Provider\AuthenticationProviderInterface;
use engine\Security\Authentication\Token\TokenInterface;
use engine\Security\Exception\AuthenticationException;
use engine\Security\User\UserInterface;
use engine\Security\User\UserProviderInterface;
use security\Authentication\Token\HttpBasicToken;

class HttpBasicProvider implements AuthenticationProviderInterface
{

	private $hideUserNotFoundExceptions;
	private $userProvider;

	public function __construct(UserProviderInterface $userProvider, $hideUserNotFoundExceptions = true){
		$this->userProvider = $userProvider;
		$this->hideUserNotFoundExceptions = $hideUserNotFoundExceptions;
	}

	/**
	 * Пытается аутентифицировать объект интерфейса токена
	 *
	 * @param TokenInterface $token
	 *
	 * @return TokenInterface An authenticated TokenInterface instance, never null
	 *
	 * @throws AuthenticationException if the authentication fails
	 */
	public function authenticate(TokenInterface $token){

		if (!$this->supports($token)) {
			throw new AuthenticationException('Authentication failed. Authentication provider not supported.');
		}

		$user = $this->userProvider->loadUserByUsername($token->getUsername());

		if($user && $this->comparePasswords($user, $token)){
			$authenticatedToken = new HttpBasicToken($user->getUsername(), $user->getPassword());
			$authenticatedToken->setUser($user);
			$authenticatedToken->setAuthenticated(true);

			return $authenticatedToken;
		}

		throw new AuthenticationException('Authentication failed. Invalid password.');
	}

	/**
	 * Проверяет, поддерживает ли данный поставщик данный токен.
	 *
	 * @param TokenInterface $token
	 *
	 * @return bool true if the implementation supports the Token, false otherwise
	 */
	public function supports(TokenInterface $token){

		return $token instanceof HttpBasicToken;
	}

	/**
	 * Выполняет проверку пароля пользователя и токена
	 *
	 * @param UserInterface $user
	 * @param HttpBasicToken $token
	 *
	 * @return bool true if the two passwords are the same, false otherwise
	 *
	 */
	protected function comparePasswords(UserInterface $user, HttpBasicToken $token){
		if(!function_exists('hash_equals')) {
			function hash_equals($str1, $str2) {
				if(strlen($str1) != strlen($str2)) {
					return false;
				} else {
					$res = $str1 ^ $str2;
					$ret = 0;
					for($i = strlen($res) - 1; $i >= 0; $i--) $ret |= ord($res[$i]);
					return !$ret;
				}
			}
		}

		return hash_equals($user->getPassword(), $token->getCredentials());
	}
}
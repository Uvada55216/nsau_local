<?php

namespace security\Authentication\Token;

use engine\Security\Authentication\Token\AbstractToken;
use engine\Security\Exception\AuthenticationException;
use engine\Security\User\UserInterface;

/**
 * HttpBasicToken реализует токен имени пользователя и пароля.
 *
 */
class HttpBasicToken extends AbstractToken
{
	private $credentials;

	/**
	 * @param string|\Stringable|UserInterface $user        The username (like a nickname, email address, etc.) or a UserInterface instance
	 * @param mixed                            $credentials
	 * @param array                        $roles
	 *
	 * @throws \InvalidArgumentException
	 */
	public function __construct($user, $credentials, array $roles = array()){
		parent::__construct($roles);

		if ($credentials =='') {
			throw new AuthenticationException('Authentication failed. Password is empty.');
		}

		if ($user =='') {
			throw new AuthenticationException('Authentication failed. Username is empty.');
		}

		$this->setUser((string)$user);
		$this->credentials  = md5(trim($credentials));
	}

	/**
	 * @see TokenInterface
	 */
	public function getCredentials()
	{
		return $this->credentials;
	}

}

<link rel="stylesheet" href="/themes/styles/error.css" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Encode+Sans+Semi+Condensed:100,200,300,400" rel="stylesheet">
<div class="cs-error col-md-12">
    <div class="row">
        <div id="body" class="loading">
        <h2><?=$moduleData['error']?> <b>:(</b></h2>
        <div class="gears">
            <div class="gear one">
                <div class="bar"></div>
                <div class="bar"></div>
                <div class="bar"></div>
            </div>
            <div class="gear two">
                <div class="bar"></div>
                <div class="bar"></div>
                <div class="bar"></div>
            </div>
            <div class="gear three">
                <div class="bar"></div>
                <div class="bar"></div>
                <div class="bar"></div>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
        <script src="js/main.js" type="text/javascript"></script>
        <script>
            $(function() {
                setTimeout(function(){
                    $('#body').removeClass('loading');
                }, 1000);
            });

        </script>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-2">
            <a href="<?=$moduleData['url']?>" class="btn btn-primary btn-sm" role="button" aria-pressed="true">Вернуться</a>
        </div>
    </div>
</div>
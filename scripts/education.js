jQuery(document).ready(function ($) {
    $('.banner').slick({
        dots: true,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 4000
    })

    $('.preparation__tab-title').on('click', function () {
        $(this).toggleClass('open')
        $(this).siblings('.preparation__tab-body').toggle()
    })


    $('.content__tabs-head-item').on('click', function () {
        var index = $(this).index()

        $('.content__tabs-head-item').removeClass('active')
        $(this).addClass('active')
        $('.content__tabs-body-item').hide()
        $('.content__tabs-body-item:eq(' + index +')').show()

    })

    $('.switcher').on('click', function () {

            var index = $(this).attr("data-switcher")


        $("[data-switcher ="+ index +"]").toggle()
        $(this).toggleClass('open')



    })



    if ($('.progress').length > 0) {
        function progressbar() {
            var top = $(window).scrollTop(),
                present = 0,
                wrapIndex = 0


            var firstBlock = $('#special').offset().top - 1,
                secondBlock = $('#education').offset().top - 1,
                thirdBlock = $('#know').offset().top - 1,
                fourthBlock = $('#preparation').offset().top - 1,
                fifthBlock = $('#practicle').offset().top - 1,
                sixthBlock = $('#who_will_teach').offset().top - 1,
                seventhBlock = $('#graduate').offset().top - 1,
                // eighthBlock = $('#graduate').offset().top - 1,
                footer = $('#docs').offset().top


            if (top > firstBlock && top < footer) {
                $('.progress').fadeIn()
            } else if (top < firstBlock) {
                $('.progress').fadeOut()
            } else if (top > footer) {
                $('.progress').fadeOut()
            }


            if (top < firstBlock) {
                present = 0
                wrapIndex = 0
            } else if (top > firstBlock && top < secondBlock) {
                present = 16
                wrapIndex = 1
            } else if (top > secondBlock && top < thirdBlock) {
                present = 30
                wrapIndex = 2
            } else if (top > thirdBlock && top < fourthBlock) {
                present = 43
                wrapIndex = 3
            } else if (top > fourthBlock && top < fifthBlock) {
                present = 57
                wrapIndex = 4
            } else if (top > fifthBlock && top < sixthBlock) {
                present = 69
                wrapIndex = 5
            } else if (top > sixthBlock && top < seventhBlock) {
                present = 84
                wrapIndex = 6
            } else if (top > seventhBlock && top < footer) {
                present = 96
                wrapIndex = 7
            } else if (top > footer) {
                present = 105
                wrapIndex = 8
            }

            $('.progress__ancors-wrap').removeClass('done')
            $('.progress__ancors-wrap:lt(' + wrapIndex + ')').addClass('done')
            $('.progress__bar-done').height(present + "%")
        }

        $(window).on("scroll", function () {
            progressbar()
        });
    }


})

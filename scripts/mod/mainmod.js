$(window).on("load", function () {
    $('#preloader').fadeOut()
});


$(document).ready(function () {
    $('.header__burger').on('click', function () {
        $(this).toggleClass('active');
        $('.header-second__dropdown-area').toggle('open')
    });

    if ($(window).width() < 1024) {
        $('.footer-second__title').on('click', function () {
            $(this).toggleClass('active')
            $(this).siblings().slideToggle()
        })
    };

    $('.partners__slider').slick({
        slidesToShow: 6,
        arrows: true,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        nextArrow: '<i class="fa-solid fa-circle-chevron-right"></i>',
        prevArrow: '<i class="fa-solid fa-circle-chevron-left"></i>',
        dots: true,
        responsive: [
            {
                breakpoint: 1919,
                settings: {
                    slidesToShow: 5
                }
            }, {
                breakpoint: 1023,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 639,
                settings: {
                    slidesToShow: 2
                }
            }
        ]
    });

    $('.banner-slider').slick({
        dots: true,
        arrows: true,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 4000,
        nextArrow: $('.banner-arrows__right'),
        prevArrow: $('.banner-arrows__left')
    });

    $('.slider__box').slick({
        slidesToShow: 3,
        arrows: true,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        nextArrow: '<i class="fa-solid fa-circle-chevron-right"></i>',
        prevArrow: '<i class="fa-solid fa-circle-chevron-left"></i>',
        responsive: [
            {
                breakpoint: 1919,
                settings: {
                    slidesToShow: 2
                }
            }, {
                breakpoint: 1023,
                settings: {
                    slidesToShow: 1
                }
            },
            {
                breakpoint: 639,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });


    if ($(window).width() < 639) {
        $('.haeader-second__dropdown-item').addClass('blocked')

    };

    $('.header-second__dropdown-item.blocked').on('click', function (e) {
        e.preventDefault()
        $(this).removeClass('blocked')
        $(this).siblings().slideDown()
    });


    $('.header-first__search').on('click', function () {
        $(this).addClass('active')
    });


    $('.header-first__search-field').on('focus', function () {
        $('.header-first__search').addClass('active')
    });


    $('.header-first__search-field').keyup(function () {
        let count = $(this).val().length()
        if (count > 0) {
            $('.header-first__search').addClass('active')
        }
    });


    $(document).mouseup(function (e) { // ������� ����� �� ���-���������
        let div = $(".header-first__search-field"); // ��� ��������� ID ��������
        if (!div.is(e.target) // ���� ���� ��� �� �� ������ �����
            && div.has(e.target).length === 0) {
            if ($('.header-first__search').hasClass('active')) {
                setTimeout(function () {
                    $('.header-first__search').removeClass('active')
                }, 100)

            }

        }
    });

    $('.footer-second__wrap em.fa-angle-down').on('click', function () {
        // console.log(this);
    });

    $('.enrollee-slider').slick({
        arrows: true,
        dots: true,
        infinite: true,
        slidesToShow: 1,
        autoplay: true,
        autoplaySpeed: 4000,
        lazyLoad: 'ondemand',
        nextArrow: '<i class="fa-solid fa-circle-chevron-right"></i>',
        prevArrow: '<i class="fa-solid fa-circle-chevron-left"></i>',
        responsive: [
            {
                breakpoint: 1920,
                settings: {
                    lazyLoad: 'ondemand'
                }
            }
        ]
    });

    if ($(window).width() < 1024) {
        $('.enrollee-info__lists-mobile').append($('.enrollee-info__lists'))
    }


    // $(".lazy").lazyload();

});

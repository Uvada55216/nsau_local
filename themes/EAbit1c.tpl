<?php
global $Engine, $Auth, $EE;

use core1C\Helper1C as H;

if(!empty($MODULE_OUTPUT['errors'])) {$MODULE_OUTPUT["mode"] = null;?>
	<div class="red"><?=$MODULE_OUTPUT['errors']?></div>
<?}?>

	<?switch ($MODULE_OUTPUT["mode"])	{ 
		case "pk_reports_main": {?>
<link rel="stylesheet" href="/themes/styles/abits.css" type="text/css" />	
<ul class="nav nav-tabs idPk" data-idpk="<?=$MODULE_OUTPUT['idPk']?>">
	<li role="form" class="active"><a href="#" data-type='full-time'>�����</a></li>
	<li role="form"><a href="#" data-type='extramural'>�������</a></li>
</ul>
<div class="content"></div>
		<?} break;

		case "_pk_reports_table": {//CF::Debug($MODULE_OUTPUT['specialities']);?>
<table class="table-bordered" width="100%">
	<thead>
		<tr ><!-- style="word-break: break-all;" -->
			<th rowspan="2">���</th>
			<th rowspan="2">����������� (�������������)</th>
			<th colspan="3">���-�� ��������� <br>����</th>
			<th colspan="3">���-�� ������������ <br>����</th>
			<th rowspan="2">���-�� �������� <br>���������</th>
			<th rowspan="2">�������</th>
			<th rowspan="2">��������� � ���������� ����</th>
		</tr>
		<tr>
			<th>���</th>
			<th>�����</th>
			<th>����������</th>
			<th>���</th>
			<th>�����</th>
			<th>����������</th>
		</tr>
	</thead>
	
	<tbody>
		<?$form = $MODULE_OUTPUT['specialityForm']==H::w2u('�����') ? 'full-time' : 'extramural'?>
		<?foreach ($MODULE_OUTPUT['specialities'] as $facultyName => $faculty) {?>
			<tr class="separator">
				<td colspan="2"><strong><?=$facultyName?></strong></td>
				<td><strong><?=$faculty['info']['budget']?></strong></td><?$total['budget'] += $faculty['info']['budget']?>
				<td><strong><?=$faculty['info']['budgetOriginals']?></strong></td><?$total['budgetOriginals'] += $faculty['info']['budgetOriginals']?>
				<td><strong><?=$faculty['info']['budgetCopy']?></strong></td><?$total['budgetCopy'] += $faculty['info']['budgetCopy']?>				
				<td><strong><?=$faculty['info']['commerce']?></strong></td><?$total['commerce'] += $faculty['info']['commerce']?>
				<td><strong><?=$faculty['info']['commerceOriginals']?></strong></td><?$total['commerceOriginals'] += $faculty['info']['commerceOriginals']?>
				<td><strong><?=$faculty['info']['commerceCopy']?></strong></td><?$total['commerceCopy'] += $faculty['info']['commerceCopy']?>				
				<td><strong><?=$faculty['info']['countRequests']?></strong></td><?$total['countRequests'] += $faculty['info']['countRequests']?>
				<td><strong><?=$faculty['info']['entryRate']?></strong></td><?$total['entryRate'] += $faculty['info']['entryRate']?>
				<td></td>
			</tr>
			<?foreach($faculty["specialities"] as $Spec) {?>
				<tr>
					<td>&nbsp<?="<a href='speciality/{$form}/{$Spec->specialityCode}'>$Spec->specialityCode</a>"?>&nbsp</td>
					<td style="text-align: left"><?="<a href='speciality/{$form}/{$Spec->specialityCode}'>$Spec->specialityName</a>"?></td>
					<td><?=$Spec->budget?></td>
					<td><?=$Spec->budgetOriginals?></td>
					<td><?=$Spec->budgetCopy?></td>
					<td><?=$Spec->commerce?></td>
					<td><?=$Spec->commerceOriginals?></td>
					<td><?=$Spec->commerceCopy?></td>					
					<td><?=$Spec->countRequests>0 ? "<a href='speciality/{$Spec->specialityCode}'>$Spec->countRequests</a>" : "0"?></td>
					<td><?=$Spec->entryRate?></td>
					<td></td>
				</tr>
			<?}?>
		<?}?>
		<tr class="separator">
			<td colspan="2"><strong>�����</strong></td>
			<td><strong><?=$total['budget']?></strong></td>
			<td><strong><?=$total['budgetOriginals']?></strong></td>
			<td><strong><?=$total['budgetCopy']?></strong></td>
			<td><strong><?=$total['commerce']?></strong></td>
			<td><strong><?=$total['commerceOriginals']?></strong></td>
			<td><strong><?=$total['commerceCopy']?></strong></td>
			<td><strong><?=$total['countRequests']?></strong></td>
			<td><strong><?=round($total['entryRate']/count($MODULE_OUTPUT['specialities']), 2)?></strong></td>
			<td></td>
		</tr>																							
	</tbody>
</table>
		<?} break;

		case "people": {?>
			<link rel="stylesheet" href="/themes/styles/abits.css" type="text/css" />	
			<?if(!empty($MODULE_OUTPUT['abits'] )) {?>
				<?foreach ($MODULE_OUTPUT['abits'] as $foundation => $abits) {?>
					<?if(!empty($abits)) {?>
						<h1><?=array_shift($abits)->name?></h1>
					<?break;}?>
				<?}?>
				
				<h2>�������� ���������</h2>			
				<table class="table-bordered" width="100%">
					<thead>
						<tr>
							<th width="70%">�����������</th>
							<th width="10%">���������</th>
							<th width="10%">����</th>
							<th width="10%">������</th>
						</tr>
					</thead>				
					<tbody>
						<?foreach ($MODULE_OUTPUT['abits'] as $foundation => $abits) {?>
							<?if(!empty($abits)) {?>
								<tr class="separator">
									<td colspan="4"><strong><?=$foundation?></strong></td>
								</tr>
								<?foreach ($abits as $abit) {?>
									<tr>
										<td>
											<a href="<?=$Engine->engine_uri?>speciality/<?=$abit->specialityCode?>"><?=$abit->specialityCode?> - <?=$abit->specialityName?></a>
										</td>
										<td><?=$abit->priority?></td>
										<td><?=$abit->totalScore?></td>
										<td><?=$abit->status?></td>
									</tr>
								<?}?>
							<?}?>
						<?}?>																
					</tbody>
				</table>
			<?} else {?>
				<h2>���������� �� ������</h2>
			<?}?>
		<?} break;

		case "_block_abits_specialities_list": {?>
			<script>
				jQuery(document).ready(function($) {
					$('#faculty').live('change', function(){
						$('#speciality > option').css('display', 'none');
						$('#speciality > option[data-faculty="'+$(this).val()+'"]').css('display', 'block');
					});
					$('#goToSpeciality').live('click', function() {
						var spec = $('#speciality > option:selected').val();
						$('#selectSpeciality').attr('action', '<?=$Engine->engine_uri?>speciality/'+spec);
						if(!spec) {
							return false;
						}
					});
				});
			</script>
			<div class="panel" style="width: 500px">
			  <div class="panel-heading">
			    <h3 class="panel-title">��������� ������ ������������</h3>
			  </div>
			  <div class="panel-body">
				<form id="selectSpeciality" method="POST">
					<div class="input-group">
					 	<span class="input-group-addon without-bottom-border nsau-abits-form">
					  		<span>��� ������:</span>
						</span>  
						<select name="listType" class="form-control without-bottom-border">
							<option <?=$MODULE_OUTPUT['post']['listType'] == 'active' ? 'selected' : ''?> value="active">��������� � ��������</option>
							<option <?=$MODULE_OUTPUT['post']['listType'] == 'ifToday' ? 'selected' : ''?> value="ifToday">���� �� ���������� ���������� �������</option>
						</select>
					</div>
					<div class="input-group">
					 	<span class="input-group-addon without-bottom-border nsau-abits-form">
					  		<span>��� �������:</span>
						</span>  
						<select name="budgetType" class="form-control without-bottom-border">
							<option <?=$MODULE_OUTPUT['post']['budgetType'] == 'isBudget' ? 'selected' : ''?> value="isBudget">������</option>
							<option <?=$MODULE_OUTPUT['post']['budgetType'] == 'withoutContest' ? 'selected' : ''?> value="withoutContest">��� ��������</option>
							<option <?=$MODULE_OUTPUT['post']['budgetType'] == 'isCommerce' ? 'selected' : ''?> value="isCommerce">������� �����������</option>
						</select>
					</div>
					<div class="input-group">
					 	<span class="input-group-addon without-bottom-border nsau-abits-form">
					  		<span>���������:</span>
						</span> 
						<select id="faculty" name='faculty' class="form-control without-bottom-border">
							<option></option>
							<?foreach($MODULE_OUTPUT['specialities'] as $facultyName => $val) {?>
								<option <?=$MODULE_OUTPUT['post']['faculty'] == $facultyName ? 'selected' : ''?> value="<?=$facultyName?>"><?=$facultyName?></option>
							<?}?>
						</select>
					</div>
					<div class="input-group">
					 	<span class="input-group-addon nsau-abits-form">
					  		<span>�����������:</span>
						</span>  
						<select id="speciality" name='speciality' class="form-control">
							<option></option>
							<?foreach($MODULE_OUTPUT['specialities'] as $facultyName => $val) {?>
								<?foreach($val['specialities'] as $speciality) {?>
									<option 
										style="display: <?=$MODULE_OUTPUT['post']['faculty'] == $facultyName ? 'block' : 'none'?> " 
										value="<?=$speciality->specialityCode?>" 
										data-faculty="<?=$speciality->facultyName?>"
										<?=$MODULE_OUTPUT['post']['speciality'] == $speciality->specialityCode ? 'selected' : ''?> >
										<?=$speciality->specialityCode?> - <?=$speciality->specialityName?>
									</option>
								<?}?>
							<?}?>
						</select>
					</div>
					<div class="row">
						<div class="col-sm-12" style="padding-top: 0px; text-align: right"><button type="submit" class="btn btn-danger" id="goToSpeciality">��������</button></div>
					</div>
				</form>
			  </div>
			</div>
		<?}
		break;

		case "abits_specialities_list": {?>
			<?\helpers\TplHelper::beginCase('EAbit1c', '_block_abits_specialities_list', $MODULE_OUTPUT);?>
		<?}
		break;

		case "_block_abits_specialities_table": {?>
			<h1>���������� � ���������� �������� ��������� <br>� ������� ����������� (��������������) �� <?=date("d.m.Y")?> �.</h1>
			<h2><?=H::u2w($MODULE_OUTPUT['specialityForm'])?> ����� ��������</h2>
			<table class="table-bordered" width="100%">
				<thead>
					<tr ><!-- style="word-break: break-all;" -->
						<th>���</th>
						<th>����������� (�������������)</th>
						<th>���-�� ��������� <br>����</th>
						<th>���-�� ������������ <br>����</th>
						<th>���-�� �������� <br>���������</th>
						<th>�������</th>
					</tr>
				</thead>
				
				<tbody>
					<?foreach ($MODULE_OUTPUT['specialities'] as $facultyName => $faculty) {?>
						<tr class="separator">
							<td colspan="2"><strong><?=$facultyName?></strong></td>
							<td><strong><?=$faculty['info']['budget']?></strong></td>
							<td><strong><?=$faculty['info']['commerce']?></strong></td>
							<td><strong><?=$faculty['info']['countRequests']?></strong></td>
							<td><strong><?=$faculty['info']['entryRate']?></strong></td>
						</tr>
						<?foreach($faculty["specialities"] as $Spec) {?>
							<tr>
								<td>&nbsp<?="<a href='speciality/{$Spec->specialityCode}'>$Spec->specialityCode</a>"?>&nbsp</td>
								<td style="text-align: left"><?="<a href='speciality/{$Spec->specialityCode}'>$Spec->specialityName</a>"?></td>
								<td><?=$Spec->budget?></td>
								<td><?=$Spec->commerce?></td>
								<td><?=$Spec->countRequests>0 ? "<a href='speciality/{$Spec->specialityCode}'>$Spec->countRequests</a>" : "0"?></td>
								<td><?=$Spec->entryRate?></td>
							</tr>
						<?}?>
					<?}?>																					
				</tbody>
			</table>		
		<?}
		break;

		case "_block_simple_report_style": {?>
			<style type="text/css">
				.table-bordered {
					   border: 1px solid #000;
					   border-collapse: collapse;
				}
				td, th {
					border: 1px solid black;
					text-align: center;
					border-collapse: collapse;
				}
			</style>
		<?}
		break;

		case "abits_specialities_table": {?>
			<link rel="stylesheet" href="/themes/styles/abits.css" type="text/css" />
			<?\helpers\TplHelper::beginCase('EAbit1c', '_block_abits_specialities_table', $MODULE_OUTPUT);?>
			<?if($MODULE_OUTPUT["show_report"]){?>
				<a href="report">������� ������� � ������� MS Word</a>
			<?}?>
		<?} break;

		case "_abits_magistracy_table_table": {?>
			<table class="table-bordered" width="100%">
				<thead>
					<tr>
						<th width="2%" rowspan="2">�</th>
						<th width="25%" rowspan="2">���</th>
						<th width="5%" colspan="1">����� �� <br>��������� ����</th> 
						<th width="5%" rowspan="2">�������������� ����������</th>
						<th width="5%" rowspan="2">����� ������</th>
						<th width="5%" rowspan="2">�������� �� �����������</th>
						<th width="5%" rowspan="2">�������� �� ����������</th>
						<th width="10%" rowspan="2">���-� � ����������</th>
					</tr>
					<tr>
						<?foreach ($MODULE_OUTPUT['speciality']->exams as $key => $value) {?>
							<th width="5%"><?=$key[0]?></th>
						<?}?>
					</tr> 
				</thead>
				
				<tbody>
					<?foreach($MODULE_OUTPUT['abits'] as $foundation => $abits) {?>
						<tr class="separator">
							<td colspan="14"><strong><?=$foundation?></strong></td>
						</tr>
						<?if(empty($abits)) {?>
							<tr>
								<td colspan="14"><p style="text-align: center; padding-bottom: 0px">��� �������� ���������</p></td>
							</tr>
						<?}?>
						<?foreach ($abits as $key => $Abit) {?>
							<tr class="<?=$Abit->styleClass?>">
								<td><?=++$j?></td>
								<td style="text-align: left"><a href="<?=$Engine->engine_uri?>people/<?=$Abit->peopleId?>"><?=$Abit->name?></a></td>
								<?foreach ($MODULE_OUTPUT['speciality']->exams as $key => $value) {?>
									<?if(!empty($Abit->exam[$key])) {?>
										<th width="5%"><?=$Abit->exam[$key]?></th>
									<?} else {?>
										<td>-</td>
									<?}?>
								<?}?>
								<td><?=$Abit->achievementsScore?></td>
								<td><?=$Abit->totalScore?></td>
								<td><?=($Abit->originalEducationDocuments) ? "��������" : "�����"?></td>
								<td><?=($Abit->agreement) ? "��" : "���"?></td>
								<td><?=$Abit->status?></td>
							</tr>
						<?}?>
					<?}?>																						
				</tbody>
			</table>
		<?}break;

		case "_abits_graduate_table_table": {?>
			<table class="table-bordered" width="100%">
				<thead>
					<tr>
						<th width="2%" rowspan="2">�</th>
						<th width="15%" rowspan="2">���</th>
						<th width="20%" colspan="3">����� �� <br>��������� ����</th> 
						<th width="5%" rowspan="2">�������������� ����������</th>
						<th width="5%" rowspan="2">����� ������</th>
						<th width="5%" rowspan="2">�������� �� �����������</th>
						<th width="5%" rowspan="2">�������� �� ����������</th>
						<th width="5%" rowspan="2">���-� � ����������</th>
					</tr>
					<tr>
						<?foreach ($MODULE_OUTPUT['speciality']->exams as $key => $value) {?>
							<th width="5%"><?=$key[0]?></th>
						<?}?>
					</tr> 
				</thead>
				
				<tbody>
					<?foreach($MODULE_OUTPUT['abits'] as $foundation => $abits) {?>
						<tr class="separator">
							<td colspan="14"><strong><?=$foundation?></strong></td>
						</tr>
						<?if(empty($abits)) {?>
							<tr>
								<td colspan="14"><p style="text-align: center; padding-bottom: 0px">��� �������� ���������</p></td>
							</tr>
						<?}?>
						<?foreach ($abits as $key => $Abit) {?>
							<tr class="<?=$Abit->styleClass?>">
								<td><?=++$j?></td>
								<td style="text-align: left"><a href="<?=$Engine->engine_uri?>people/<?=$Abit->peopleId?>"><?=$Abit->name?></a></td>
								<?foreach ($MODULE_OUTPUT['speciality']->exams as $key => $value) {?>
									<?if(!empty($Abit->exam[$key])) {?>
										<th width="5%"><?=$Abit->exam[$key]?></th>
									<?} else {?>
										<td>-</td>
									<?}?>
								<?}?>
								<td><?=$Abit->achievementsScore?></td>
								<td><?=$Abit->totalScore?></td>
								<td><?=($Abit->originalEducationDocuments) ? "��������" : "�����"?></td>
								<td><?=($Abit->agreement) ? "��" : "���"?></td>
								<td><?=$Abit->status?></td>
							</tr>
						<?}?>
					<?}?>																						
				</tbody>
			</table>
		<?}break;
		case "_abits_table_table": {?>
			<table class="table-bordered" width="100%">
				<thead>
					<tr>
						<th width="2%" rowspan="2">�</th>
						<th width="15%" rowspan="2">���</th>
						<th width="20%" colspan="3">����� �� <br>����������� ���</th>
						<th width="20%" colspan="3">����� �� <br>��������� ����</th> 
						<th width="5%" rowspan="2">�������������� ����������</th>
						<th width="5%" rowspan="2">����� ������</th>
						<th width="5%" rowspan="2">�������� �� �����������</th>
						<th width="5%" rowspan="2">�������� �� ����������</th>
						<th width="18%" rowspan="2">��������</th>
						<th width="5%" rowspan="2">���-� � ����������</th>
					</tr>
					<tr>
						<?for($i=0; $i<2; $i++) {?>
							<?foreach ($MODULE_OUTPUT['speciality']->exams as $key => $value) {?>
								<th width="5%"><?=$key[0]?></th>
							<?}?>
						<?}?>
					</tr> 
				</thead>
				
				<tbody>
					<?foreach($MODULE_OUTPUT['abits'] as $foundation => $abits) {?>
						<tr class="separator">
							<td colspan="14"><strong><?=$foundation?></strong></td>
						</tr>
						<?if(empty($abits)) {?>
							<tr>
								<td colspan="14"><p style="text-align: center; padding-bottom: 0px">��� �������� ���������</p></td>
							</tr>
						<?}?>
						<?foreach ($abits as $key => $Abit) {?>
							<tr class="<?=$Abit->styleClass?>">
								<td><?=++$j?></td>
								<td style="text-align: left"><a href="<?=$Engine->engine_uri?>people/<?=$Abit->peopleId?>"><?=$Abit->name?></a></td>
								<?foreach ($MODULE_OUTPUT['speciality']->exams as $key => $value) {?>
									<?if(!empty($Abit->ege[$key])) {?>
										<th width="5%"><?=$Abit->ege[$key]?></th>
									<?} else {?>
										<td>-</td>
									<?}?>
								<?}?>
								<?foreach ($MODULE_OUTPUT['speciality']->exams as $key => $value) {?>
									<?if(!empty($Abit->exam[$key])) {?>
										<th width="5%"><?=$Abit->exam[$key]?></th>
									<?} else {?>
										<td>-</td>
									<?}?>
								<?}?>
								<td><?=$Abit->achievementsScore?></td>
								<td><?=$Abit->totalScore?></td>
								<td><?=($Abit->originalEducationDocuments) ? "��������" : "�����"?></td>
								<td><?=($Abit->agreement) ? "��" : "���"?></td>
								<td><?=$Abit->organization?></td>
								<td><?=$Abit->status?></td>
							</tr>
						<?}?>
					<?}?>																						
				</tbody>
			</table>
		<?}break;

		case "abits_report_table": {?>
			<link rel="stylesheet" href="/themes/styles/abits.css" type="text/css" />
			<?$Speciality = $MODULE_OUTPUT['speciality']?>
			<?if(!empty($Speciality->specialityName)) {?>
				<h1><?=$Speciality->facultyName?></h1>
				<h2><?=$Speciality->specialityCode?> - <?=$Speciality->specialityName?></h2>
				<h3>���������� ����:</h3>
				<ul>
				<?foreach ($Speciality->places as $type => $places) {?>
					<li><?=$type?> - <?=$places?></li>
				<?}?>
				</ul>
				<h3>��������:</h3>
				<ul>
					<?foreach ($MODULE_OUTPUT['speciality']->exams as $key => $value) {?>
						<li><?=$key[0]?> - <?=$key?></li>
					<?}?>
				</ul>
				<?if(!empty($MODULE_OUTPUT['abits'])) {?>
					<a href="<?=$Engine->unqueried_uri?>/download">������� ������� � ������� MS Word</a>
					<?\helpers\TplHelper::beginCase('EAbit1c', '_abits_table_table', $MODULE_OUTPUT);?>
				<?} else {?>
					<h2>�� ������ ����������� ��� �������� ���������</h2>
		        <?}?>  
		    <?} else {?>
				<h2>����������� ���������� �� �������</h2>
		    <?}?>  		
		<?} break;

		case "abits_table": {?>
		<link rel="stylesheet" href="/themes/styles/abits.css" type="text/css" />		
		<?\helpers\TplHelper::beginCase('EAbit1c', '_block_abits_specialities_list', $MODULE_OUTPUT);?>
		<?$Speciality = $MODULE_OUTPUT['speciality']?>
		<?if(!empty($Speciality->specialityName)) {?>
			<h1><?=$Speciality->facultyName?></h1>
			<h2><?=$Speciality->specialityCode?> - <?=$Speciality->specialityName?></h2>
			<h3>���������� ����:</h3>
			<ul>
			<?foreach ($Speciality->places as $type => $places) {?>
				<li><?=$type?> - <?=$places?></li>
			<?}?>
			</ul>
			<h3>��������:</h3>
			<ul>
				<?foreach ($MODULE_OUTPUT['speciality']->exams as $key => $value) {?>
					<li><?=$key[0]?> - <?=$key?></li>
				<?}?>
			</ul>
			<?if(!empty($MODULE_OUTPUT['abits'])) {?>
				<?\helpers\TplHelper::beginCase('EAbit1c', $MODULE_OUTPUT['abits_table_type'], $MODULE_OUTPUT);?>
			<?} else {?>
				<h2>�� ������ ����������� ��� �������� ���������</h2>
	        <?}?>  
	    <?} else {?>
			<h2>����������� ���������� �� �������</h2>
	    <?}?>               
		<?} break;
	}
?>
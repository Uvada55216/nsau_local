<?php
// version: 1.00.3
// date: 2013-07-15
global $Engine, $Auth, $EE, $ADMIN_USER;
$MODULE_OUTPUT = $MODULE_DATA["output"];

foreach ($MODULE_DATA["output"]["messages"]["good"] as $data)
{
  echo "<p class=\"message\">$data</p>\n";
}

foreach ($MODULE_DATA["output"]["messages"]["bad"] as $data)
{
	echo "<p class=\"message red\">$data</p>\n";
}


if (isset($MODULE_OUTPUT["mode"])) 
{
  switch ($MODULE_OUTPUT["mode"]) 
  {
    case "apply":
      $timepars = array("09:00 - 10:30","10:40 - 12:10","12:50 - 14:20","14:30 - 16:00","16:10 - 17:40","17:45 - 19:15","19:20 - 20:50"); 
      if ($Engine->OperationAllowed($MODULE_OUTPUT["module_id"], "multimedia.apply", -1, $Auth->usergroup_id)) 
      {   
        ?>
        <script>jQuery(document).ready(function($) {InputCalendar();});</script>
        <div class="page-header">
        <h2>���������� ������ �� ������������� ��������������� ������������</h2>
      </div>
        <form action="<?=$EE["unqueried_uri"]?>" method="post" name="apply_form">
			    <input type="hidden" name="module_id" value="<?=$MODULE_DATA["output"]["module_id"]?>" />


          <div class="row"> 
              <div class="col-sm-4">
                <p>���������:<br />
                  <select name="auditorium" size="1" class="form-control">
                    <?php 
                    foreach($MODULE_OUTPUT["buildings"] as $build) 
                    {
                      ?>
                      <optgroup label="<?=$build["name"] ?>">
                      <?
                        foreach ($build["auditories"] as $auditorium) {
                          ?>
                          <option value="<?=$auditorium["id"]?>"><?=$auditorium['label'];?>-<?=$auditorium['name'] ?></option>
                          <?
                        }
                        ?>
                      </optgroup>
                      <?php 
                    }
                    ?>


                  </select>
                </p>

                <p>����: <br />
                  <input type="text" name="date" class="date_time form-control"  value="<?=date('Y-m-d')?>" id="apply_date" /> 
                </p> 


                <p>����� ����: <br />
                  <select name="num[]" id="apply_num" multiple size="8" class="form-control">
                    <?php           
                    for($i=1; $i<8; $i++) 
                    { 
                      ?>
                      <option value="<?=$i?>"><?=$i?> (<?=$timepars[$i-1]?>)</option>
                      <?php       
                    } 
                    ?>
                  </select>
                </p>
                <input type="submit" value="�������� ������" class="btn btn-nsau btn-md" id="apply" style="margin-top: 12.5px;" />        
              </div>
          </div>
        </form>
        <?php 
      } 
      
      if (isset($MODULE_OUTPUT["applications"]) && !empty($MODULE_OUTPUT["applications"])) 
      { 
        ?>
        <table width="80%" cellspacing="2" cellpadding="3">
          <tr bgcolor="#BBBBBB">
            <td><a href="?sort=teacher<?=(($_GET["sort"]=="teacher") && ($_GET["type"]!="asc") ? "&type=asc" : "")?>">�������������</a></td>
            <td><a href="?sort=auditorium<?=(($_GET["sort"]=="auditorium") && ($_GET["type"]!="asc") ? "&type=asc" : "")?>">���������</a></td>
            <td><a href="?sort=pair<?=(($_GET["sort"]=="pair") && ($_GET["type"]!="asc") ? "&type=asc" : "")?>">����</a></td>
            <td><a href="?sort=date<?=(($_GET["sort"]=="date") && ($_GET["type"]!="asc") ? "&type=asc" : "")?>">����</a></td>
            <td></td>
          </tr>
          <?
          $i = 0;
          foreach ($MODULE_OUTPUT["applications"] as $application) 
          {
            ?>
            <tr id="app_row_<?=$application['id']?>" style="background-color: <?php echo (($i++ % 2) ? '#E5DDDD':'#F5EAEA');?>">
              <td>
                <?php 
                if(!empty($application['people_id']))
                { 
                  ?><a href="/people/<?=$application['people_id']?>/"><?php } ?><?=$application['last_name']?> <?=$application['first_name']?> <?=$application['patronymic']?><?php if(!empty($application['people_id'])) { ?></a><?php 
                }
                ?>
              </td>
              <td>
                <?php 
                if($MODULE_OUTPUT["is_admin_user"]) 
                { 
                  ?><a href="/office/auditorium/edit/<?=$application['auditorium_id'];?>" title="<?=$application["building_name"];?>"><?php } ?><?=$application['label'] ?>-<?=$application['name'] ?><?php if($MODULE_OUTPUT["is_admin_user"]) { ?></a><?php 
                } 
                ?>
              </td>
              
              <td><?=$application['num'] ?>  (<?=$timepars[$application['num']-1]?>)</td>
              <td><?=implode(".", array_reverse(explode("-", $application['date']))) ?></td>
              <td>
                <?php 
                if($Auth->user_id == $application["lecturer_id"] || $MODULE_OUTPUT["is_admin_user"] || ($Auth->people_id == $application["people_id"])) 
                { 
                  ?><a href="del_app/<?=$application['id']?>" class="del_app glyphicon glyphicon-remove" onclick="if(confirm('�� ������������� ������� ������� ������?')){return true;}else{return false;}">
                    </a>


                    <?php 
                } 
                ?>
              </td>
            </tr>
            <?php 
          }
          ?>
        </table>

                        <?
                if($MODULE_OUTPUT["show_all"] ) 
                { 
                  ?>
                  <br/> 
                  <?php 
                  if(!$MODULE_OUTPUT["history"]) 
                  { 
                    ?>
                    <a href="<?php echo $EE["engine_uri"]; ?>history/" style="margin-top: 8px;"> ������� ������</a>
                    <?php 
                  } 
                  else 
                  { 
                    ?>
                    <a href="<?php echo $EE["engine_uri"]; ?>" style="margin-top: 8px;">������� ������</a>
                    <?php 
                  } 
                } 
                ?>  
        <?php 
      } 
    break;
  }
}

?>
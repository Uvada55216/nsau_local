<?php
switch ($MODULE_OUTPUT["mode"])
{
    case 'education_chat':
    {
        ?>
        <?header("Content-type: text/html; charset=windows-1251");?>

        <link rel="stylesheet" href="/themes/styles/echat.css" type="text/css" /> 
        <link rel="stylesheet" href="/themes/styles/echat_custom.css" type="text/css" /> 

        <button class="btn btn-default btn-block horizontal_menu_button" type="button">������ ���� ������� ��������</button>





        <!-- <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css'> -->
        <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.2/css/font-awesome.min.css'>



        <div class="theme_create_panel">
            <div class="modal fade" id="show_theme_create_panel" role="dialog" style="margin-top: 100px;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header"> 
                            <h4 class="modal-title">������� ����� ����</h4> 
                        </div> 
                        <div class="modal-body">
                            <input class="form-control" id="theme_name" type="text" placeholder="�������� ����"> 
                            <input class="form-control" id="theme_description" type="text" placeholder="�������� ����" style="border-top: 0px!important;">
                        </div> 
                        <div class="modal-footer">
                            <button type="button" class="btn btn-nsau-grey" id="theme_create" data-dismiss="modal"> 
                                �������
                            </button>
                            <button type="button" class="btn btn-nsau-grey" data-dismiss="modal"> 
                                �������
                            </button>
                        </div> 
                    </div> 
                </div> 
            </div> 
            <button class="btn btn-nsau" type="button" data-toggle="modal" data-target="#show_theme_create_panel">������� ����� ����</button>
        </div>

        <script id="hidden-template-creator" type="text/x-custom-template">    
            <div class="menu-category list-group theme_card" id="{id}">
                <div class="theme_card" id="{id}">
                    <div class="[ panel panel-default ] panel-google-plus">
                        <div class="panel-google-plus-tags">
                            <ul>
                                <li onclick="javascript:location.href='/people/{people_id}'">{displayed_name}</li>
                            </ul>
                        </div>
                        <div class="panel-heading">
                            <h3>{theme_name}</h3>
                            <h5>{description}</h5>
                        </div>
                        <div class="panel-body">
                            <div class="alert alert-info">
                                {last_message}
                            </div>
                        </div>
                        <div class="panel-footer">
                            <button type="button" class="[ btn btn-default ] goto_theme" id="{id}"><span class="glyphicon glyphicon-comment"></span> ������� � ����</button>
                            <button type="button" class="[ btn btn-default ] delete_theme" id="{id}"><span class="glyphicon glyphicon-trash"></span> ������� ����</button>
                            <button type="button" class="[ btn btn-default ] show_theme_users" id="{id}" style="float: right"><span class="glyphicon glyphicon-comment"></span> {count_messages}</button>
                            <button type="button" class="[ btn btn-default ] show_theme_users" id="{id}" style="float: right"><span class="glyphicon glyphicon-user"></span> {count_useres}</button>
                        </div>
                    </div>
                </div>
            </div>            
        </script>

        <script id="hidden-template-user" type="text/x-custom-template">
            <div class="menu-category list-group theme_card" id="{id}">
                <div class="theme_card" id="{id}">
                    <div class="[ panel panel-default ] panel-google-plus">
                        <div class="panel-google-plus-tags">
                            <ul>
                                <li onclick="javascript:location.href='/people/{people_id}'">{displayed_name}</li>
                            </ul>
                        </div>
                        <div class="panel-heading">
                            <h3>{theme_name}</h3>
                            <h5>{description}</h5>
                        </div>
                        <div class="panel-body">
                            <div class="alert alert-info">
                                {last_message}
                            </div>
                        </div>
                        <div class="panel-footer">
                            <button type="button" class="[ btn btn-default ] goto_theme" id="{id}"><span class="glyphicon glyphicon-comment"></span> ������� � ����</button>
                            <!-- <button type="button" class="[ btn btn-default ] delete_from_chat" id="{id}"><span class="glyphicon glyphicon-log-out"></span> ����� �� ����</button> -->
                            <button type="button" class="[ btn btn-default ] show_theme_users" id="{id}" style="float: right"><span class="glyphicon glyphicon-comment"></span> {count_messages}</button>
                            <button type="button" class="[ btn btn-default ] show_theme_users" id="{id}" style="float: right"><span class="glyphicon glyphicon-user"></span> {count_useres}</button>
                        </div>
                    </div>
                </div>
            </div>                         
        </script>

        <script id="hidden-template-theme-chat" type="text/x-custom-template">
            <div class="modal fade" id="invite_user" role="dialog" style="margin-top: 100px;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header"> 
                            <h4 class="modal-title">���������� ������������</h4> 
                        </div> 
                        <div class="modal-body">
                            <input type="text" class="form-control invite_user_input" placeholder="��� ������������">
                            <div class="invite_user_list"></div>
                        </div> 
                        <div class="modal-footer">
                            <button type="button" class="btn btn-nsau-grey" data-dismiss="modal"> 
                                �������
                            </button>
                        </div> 
                    </div> 
                </div> 
            </div> 
            <div class="modal fade" id="theme_settings" role="dialog" style="margin-top: 100px;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">���������</h4> 
                        </div> 
                        <div class="modal-body">
                            <div class="input-group"> 
                                <span class="input-group-addon" id="basic-addon1" style="min-width: 150px;">��������</span>
                                <input type="text" class="form-control settings_theme_name" value="{theme_name}">
                            </div>
                            <div class="input-group"> 
                                <span class="input-group-addon" id="basic-addon1" style="border-top: 0px!important; min-width: 150px;">��������</span>
                                <input type="text" class="form-control settings_theme_description" value="{description}" style="border-top: 0px!important;">
                            </div>
                        </div> 
                        <div class="modal-footer">
                            <button type="button" class="btn btn-nsau-grey apply_settings" id="{id}" data-dismiss="modal"> 
                                ���������
                            </button>
                            <button type="button" class="btn btn-nsau-grey" data-dismiss="modal"> 
                                �������
                            </button>
                        </div> 
                    </div> 
                </div> 
            </div> 
            <div class="row theme_chat" id="{id}">
                <div id="frame">
                    <div id="sidepanel">
                        <div id="contacts">
                            <ul class="chat_users_list">
                                
                            </ul>
                        </div>
                        <div id="bottom-bar">
                            <button id="addcontact show_invite_window" data-toggle="modal" data-target="#invite_user">
                                <i class="fa fa-user-plus fa-fw" aria-hidden="true"></i> 
                                <span>��������</span>
                            </button>
                            <button id="settings" data-toggle="modal" data-target="#theme_settings"><i class="fa fa-cog fa-fw" aria-hidden="true"></i> <span>���������</span></button>
                        </div>
                    </div>
                    <div class="content">
                        <div class="contact-profile">
                            <div class="theme_info">
                                <div class="name">{theme_name}</div>
                                <div class="description">{description}</div>
                            </div>
                            <div class="social-media">
                                <i class="fa-chevron-left back_to_themes_list" aria-hidden="true">
                                    <span class="glyphicon glyphicon-th-large"></span></i>
                                </i>
                               
                                <!-- <i class="fa fa-instagram" aria-hidden="true"></i> -->
                            </div>
                        </div>
                        <div class="messages">
                            <ul class="chat_messages">
                                
                            </ul>
                        </div>
                        <div class="message-input">
                            <div class="wrap">
                            <input type="text" class="chat_input" id="{id}"/>
                            <!-- <i class="fa fa-paperclip attachment" aria-hidden="true"></i> -->
                            <button class="submit"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </script>

        <script id="hidden-template-message-self" type="text/x-custom-template">
            <li class="sent" data-type="message" data-id="{id}">
                <img class="invite_photo" width="10" height="20" src="https://nsau.edu.ru/images/people/{photo}" alt="" />
                <span class="glyphicon glyphicon-trash delete_message" id="{id}"></span>
                <p><strong>{name}:</strong><br>{message}<i>{date} {time}</i></p>
            </li>
        </script>

        <script id="hidden-template-message-other" type="text/x-custom-template">
            <li class="replies" data-type="message" data-id="{id}">
                <img class="invite_photo" width="10" height="20" src="https://nsau.edu.ru/images/people/{photo}" alt="" />
                <span class="glyphicon glyphicon-trash delete_message" id="{id}"></span>
                <p><strong>{name}:</strong><br>{message}<i>{date} {time}</i></p>
            </li>
        </script>
        <div class="menu row" id="chat_list"></div>
        <div class="menu row" id="themes_list"></div>
        <?
    }
    break;

}


?>
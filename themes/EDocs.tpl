<?
$MODULE_MESSAGES = array(
301 => "���� '@0' ����������� ��� ����������",
302 => "����� �������� ��� ��������",
201 => "�������� ��������",
303 => "�� ������� �� ���� ����� ��������"
);
$CRITICAL_ERRORS = array(401);//����� ������ �� ��������� ��������� ���������� ��������

if ($MODULE_OUTPUT["messages"]["good"])
{
    $array = array();

    foreach ($MODULE_OUTPUT["messages"]["good"] as $elem)
    {
			if(is_array($elem)) {				
				foreach($elem as $err => $data) {
					if(is_array($data)){
						for($i=0;$i<count($data);$i++) {
						$MODULE_MESSAGES[$err] = str_replace("@{$i}", $data[$i], $MODULE_MESSAGES[$err]);
						}
					} elseif(!empty($data)) {
						$t = str_replace("@0", $data, $MODULE_MESSAGES[$err]);
					} else {
						$t = $MODULE_MESSAGES[$err];
					}
					$array[] = $t;
				}
			}else {
				if (isset($MODULE_MESSAGES[$elem]))
				{
						$array[] = $MODULE_MESSAGES[$elem];
				}
			}
    }

    if ($array)
    {
        foreach ($array as $elem)
        {
            echo "<p class=\"message green\">".$elem."</p>\n";
        }
    }
}

if ($MODULE_OUTPUT["messages"]["bad"])
{
    $array = array();

    foreach ($MODULE_OUTPUT["messages"]["bad"] as $elem)
    {
			if(is_array($elem)) {				
				foreach($elem as $err => $data) {
					if(is_array($data)){
						for($i=0;$i<count($data);$i++) {
						$MODULE_MESSAGES[$err] = str_replace("@{$i}", $data[$i], $MODULE_MESSAGES[$err]);
						}
					} elseif(!empty($data)) {
						$t = str_replace("@0", $data, $MODULE_MESSAGES[$err]);
					} else {
						$t = $MODULE_MESSAGES[$err];
					}
					$array[] = $t;
				}
			}else {
				if (isset($MODULE_MESSAGES[$elem]))
				{
						$array[] = $MODULE_MESSAGES[$elem];
				}
			}
    }

    if ($array)
    {
        foreach ($array as $elem)
        {
            echo "<p class=\"message red\">".$elem."</p>\n";
        }
    }

}
switch($MODULE_OUTPUT["mode"])
{

	case "upload": 
  	{	
  		?>
  		<style type="text/css">
  			.file-form-upload-button {
  				right: 20px;
  				top: 16px;
  			}

  			.file-form-cancel-button {
  				right: 16px;
  				top: 16px;
  			}
  		</style>


		<legend>�������� ��������</legend>

		<form method="post" action="">
			<div class="row"> 
			    <div class="col-sm-6">
						<!-- <div class="form-notes"> -->
							<div class="input-group">
								<span class="input-group-addon" style="border-bottom: 0px!important; min-width: 140px;">������������<span class="obligatorily">*</span></span>
								<select name="qualification_id" class="form-control" style="border-bottom: 0px!important;">
									<option value=""></option>
									<?foreach($MODULE_OUTPUT["info"]["qualifications"] as $id  => $val){?>
										<option <?=$MODULE_OUTPUT["form_back"]["qualification_id"]==$id ? "selected" : ""?> value="<?=$id?>"><?=$val?></option>
									<?}?>
								</select>
							</div>

							<div class="input-group">
								<span class="input-group-addon" style="border-bottom: 0px!important; min-width: 140px;">��� ���������<span class="obligatorily">*</span></span>
								<select name="doc_type_id" class="form-control" style="border-bottom: 0px!important;">
									<option value=""></option>
									<?foreach($MODULE_OUTPUT["info"]["types"] as $id  => $val){?>
										<option <?=$MODULE_OUTPUT["form_back"]["doc_type_id"]==$id ? "selected" : ""?> value="<?=$id?>"><?=$val?></option>
									<?}?>
								</select>
							</div>

							<!-- 				
							<label>����� ��������<span class="obligatorily">*</span></label><br>
							<select name="form">
								<option value=""></option>
								<option <?=$MODULE_OUTPUT["form_back"]["form"]==1 ? "selected" : ""?> value="1">�����</option>
								<option <?=$MODULE_OUTPUT["form_back"]["form"]==2 ? "selected" : ""?> value="2">�������</option>
								<option <?=$MODULE_OUTPUT["form_back"]["form"]==3 ? "selected" : ""?> value="3">����-�������</option>
							</select><br /> 
							-->

							<div class="input-group">
								<span class="input-group-addon" style="border-bottom: 0px!important; min-width: 140px;">�������<span class="obligatorily">*</span></span>
								<select name="profile_id[]" class="form-control" style="border-bottom: 0px!important;" multiple>
									<option value=""></option>
									<?foreach($MODULE_OUTPUT["info"]["profiles"] as $id  => $val){?>
										<option <?=$MODULE_OUTPUT["form_back"]["profile_id"]==$id ? "selected" : ""?> value="<?=$id?>"><?=$val?></option>
									<?}?>
								</select>
							</div>

							<div class="input-group">
								<span class="input-group-addon" style="min-width: 140px;">��� ������<span class="obligatorily">*</span></span>
								<select class="year2 form-control" name="year">
									<?
										$year = date("o");
										for ($i=$year-10; $i < $year+5; $i++) 
										{ 
											?>
												<option value=<?=$i?> ><?=$i?></option>
											<?
										}
									?>
								</select>	
							</div>
							
							<p>
								<div class="checkbox-nsau"> 
								    <input type="checkbox" name="form_1" value="1" class="form-control" id="check1">
								    <label for="check1">�����</label> 
								</div>

								<div class="checkbox-nsau"> 
								    <input type="checkbox" name="form_2" value="1" class="form-control" id="check2">
								    <label for="check2">�������</label> 
								</div>

								<div class="checkbox-nsau"> 
								    <input type="checkbox" name="form_3" value="1" class="form-control" id="check3">
								    <label for="check3">����-�������</label> 
								</div>

			<!-- 								<div class="checkbox-nsau"> 
								    <input type="checkbox" name="form_4" value="1" class="form-control" id="check4">
								    <label for="check4">�������-����������</label> 
								</div> -->
							</p>

							<!-- <input type="text" class="year2" name="year" value="<?=$MODULE_OUTPUT["form_back"]["year"]?>" /> -->
							
							<div class="wide">
								<input type="submit" value="��������">
							</div>	

						<!-- </div>					 -->				
			    </div>
						<ul class="list-group tmp hidden">
							<li class="list-group-item list-group-item-danger under-input-tooltip"></li>
						</ul>
			    <div class="col-sm-6"> 
        			<div class="panel panel-default">
            			<div class="panel-heading" >
            				<button type="button" class="btn btn-nsau-grey btn-md attach_file_modal_button"><span class="glyphicon glyphicon-paperclip"></span> ���������� �����<span class="obligatorily">*</span></button>

              				<button type="button" class="btn btn-nsau-grey btn-md spoiler-trigger" data-toggle="collapse"><span class="glyphicon glyphicon-download-alt"></span> ��������� �����</button>
            				<div class="selected_attach_files"></div>
            			</div>

            			<div class="panel-collapse collapse out">
							<?header("Content-type: text/html; charset=windows-1251");?>
							<div class="upload_file_form">
								<?\helpers\TplHelper::beginCase('EFiles2.0', '_block_upload_form', $MODULE_OUTPUT);?>
							</div>
            			</div>
          			</div>


			        <script type="text/javascript">
			            $(".spoiler-trigger").click(function() 
			            {
			                $(this).parent().next().collapse('toggle');
			            });

			            $(".attach_file_modal_button").click(function() 
			            {
			                $("#attach_file_modal").modal('show');
			            });
			        </script>

			   
		            <div id="attach_file_modal" class="modal fade">
		                <div class="modal-dialog" style="width: 700px; top: 10%;">
		                    <div class="modal-content">
		                        <div class="modal-header">
		                            <h4 class="modal-title">���������� �����</h4>
		                        </div>
		                        <div class="modal-body" style="text-align: left; font-size: 12px;">


									<div class="row"> 
									     <div class="col-sm-12">
									     	<div class="input-group"> 
											    <span class="input-group-addon count_files" id="basic-addon1">�����</span>
											    <input type="text" class="form-control" id="search_file_name" placeholder="������� ��� �����">
											</div>
									     </div>
									</div>

									<select name="file_id" class="select_file form-control" multiple size="20">
										<option value=""></option>
										<?foreach($MODULE_OUTPUT["info"]["files"] as $id  => $val){?>
											<option <?=$MODULE_OUTPUT["form_back"]["file_id"]==$id ? "selected" : ""?> value="<?=$id?>"><?=$val?></option>
										<?}?>
									</select>

					

<!-- 					<input type="text" size="10" title="������� �����" placeholder="������� ����� ��������" class="menu_search" id="menu_test" style="display: none; width: 265px;"/>
					<a href="javascript: void(0)" onclick="jQuery(this).parent().find('.menu_search').show(); jQuery(this).parent().find('.menu_list').hide(); jQuery(this).hide(); " class="menu_list" title="����� �� ���������� ��������">������</a>  -->








		                        </div>
		                        <div class="modal-footer">
		                            <button type="button" class="btn btn-nsau close_summary" onclick="$('#attach_file_modal').modal('hide');">Ok</button>
		                        </div>
		                        </form>
		                    </div>
		                </div>
		            </div>

		            <script type="text/javascript">
		            	//$("#attach_file_modal").modal('show');
		            </script>



			    </div>

			</div>
		</form>

	

		<fieldset>
		<legend>���������</legend>
		<table class="nsau_table" style="width: 100%">
			<thead>
				<tr>
					<td colspan="6"><?=$MODULE_OUTPUT["info"]["speciality"]["code"]?> - <?=$MODULE_OUTPUT["info"]["speciality"]["name"]?></td>
				</tr>
				<tr>
					<th>���</th>
					<th>������������</th>
					<th>�����</th>
					<th>��� ������</th>
					<th>����</th>
					<th>�������������/�������</th>
				</tr>
			</thead>
			<tbody>
				<?foreach($MODULE_OUTPUT["docs"] as $p_id => $docs) {?>
					<?if(!empty($p_id)){?>
						<tr class="effect table_seporator_left">
							<td colspan="6"><?=$MODULE_OUTPUT["info"]["profiles"][$p_id]?></td>
						</tr>
					<?} else {?>
						<tr class="effect table_seporator_left">
							<td colspan="6">����� ���������</td>
						</tr>
					<?}?>
					<?foreach($docs as $doc) {?>
						<tr data-id="<?=$doc["id"]?>">
							<td rowspan="2"><?=$MODULE_OUTPUT["info"]["types"][$doc["doc_type_id"]]?></td>
							<td rowspan="2"><?=$MODULE_OUTPUT["info"]["qualifications"][$doc["qualification_id"]]?></td>
							<td rowspan="2"><?
							switch ($doc["form"]) 
							{
								case 1:
									echo "�����";
									break;
								case 2:
									echo "�������";
									break;
								case 3:
									echo "����-�������";
									break;
							}
							?>
							</td>
							<td rowspan="2"><?=$doc["year"]?></td>
							<td rowspan="2"><a href="/file/<?=$doc["file_id"]?>"><?=$doc["file_name"]?></a></td>
							<td style="width: 10%" class="save_button effect edit"><small>[ ������������� / ���������� ]</small></td>
						</tr>
						<tr data-id="<?=$doc["id"]?>">
							<td style="width: 10%" class="save_button effect delete"><small>[�������]</small></td>
						</tr>
					<?}?>
				<?}?>
			</tbody>
		</table>
		</fieldset>
	<?}
	break;?>

<?
case 'all_spec':
	{
		//CF::Debug($MODULE_OUTPUT['all_spec']);
		?>
			<p>����������: ��� ���������� ���������� ������� �� ��������� �������.</p>
            <div id='app'>
                <table class="nsau_table">
                    <thead style="cursor: pointer;">
                        <tr>
							<th @click="sort('code')">��� </th>
							<th @click="sort('name')">������������ �������������</th>
							<th @click="sort('type')">������� �����������</th>
							<th @click="sort('year')">��� ������</th>
							<th>����</th>
                        </tr>
                    </thead>
                    <tbody v-for='item in sortedSpecs'>
                        <tr>
                            <td>{{item.code}}</td>
                            <td><a v-bind:href="item.link">{{item.name}}</a></td>
                            <td>{{item.type}}</td>
                            <td>{{item.year}}</td>
                            <td> <a v-bind:href="item.file_id">����</a></td>
                        </tr>  
                    </tbody>
                </table>
            </div>       

		<?
	}
	break;
?>


<? case "edit": 
	{
	?>
		<fieldset>
			<div class="row"> 
			    <div class="col-sm-5">
			    	<legend><center>������������� ��������</center></legend>
				    <form method="post" action="">
						<div class="input-group"> 
						    <span class="input-group-addon" style="border-bottom: 0px!important; min-width: 150px;">������������<span class="obligatorily">*</span></span>
							<select name="qualification_id" class="form-control" style="border-bottom: 0px!important;">
								<option value=""></option>
								<?foreach($MODULE_OUTPUT["info"]["qualifications"] as $id  => $val){?>
									<option <?=$MODULE_OUTPUT["form_back"]["qualification_id"]==$id ? "selected" : ""?> value="<?=$id?>"><?=$val?></option>
								<?}?>
							</select>
						</div>

						<div class="input-group"> 
						    <span class="input-group-addon" style="border-bottom: 0px!important; min-width: 150px;">��� ���������<span class="obligatorily">*</span></span>
							<select name="doc_type_id" class="form-control"  style="border-bottom: 0px!important;">
								<option value=""></option>
								<?foreach($MODULE_OUTPUT["info"]["types"] as $id  => $val){?>
									<option <?=$MODULE_OUTPUT["form_back"]["doc_type_id"]==$id ? "selected" : ""?> value="<?=$id?>"><?=$val?></option>
								<?}?>
							</select>
						</div>

						<div class="input-group"> 
						    <span class="input-group-addon" style="border-bottom: 0px!important; min-width: 150px;">����<span class="obligatorily">*</span></span>
							<select name="file_id" class="form-control" style="border-bottom: 0px!important;">
								<option value=""></option>
								<?foreach($MODULE_OUTPUT["info"]["files"] as $id  => $val){?>
									<option <?=$MODULE_OUTPUT["form_back"]["file_id"]==$id ? "selected" : ""?> value="<?=$id?>"><?=$val?></option>
								<?}?>
							</select>
						</div>
					
						<div class="input-group"> 
						    <span class="input-group-addon" style="border-bottom: 0px!important; min-width: 150px;">����� ��������<span class="obligatorily">*</span></span>
							<select name="form" class="form-control" style="border-bottom: 0px!important;">
								<option value=""></option>
								<option <?=$MODULE_OUTPUT["form_back"]["form"]==1 ? "selected" : ""?> value="1">�����</option>
								<option <?=$MODULE_OUTPUT["form_back"]["form"]==2 ? "selected" : ""?> value="2">�������</option>
								<option <?=$MODULE_OUTPUT["form_back"]["form"]==3 ? "selected" : ""?> value="3">����-�������</option>
								<!-- <option <?=$MODULE_OUTPUT["form_back"]["form"]==4 ? "selected" : ""?> value="4">�������-����������</option> -->
							</select>
						</div>

						<div class="input-group"> 
						    <span class="input-group-addon" style="border-bottom: 0px!important; min-width: 150px;">�������</span>
							<select name="profile_id" class="form-control" style="border-bottom: 0px!important;">
								<option value=""></option>
								<?foreach($MODULE_OUTPUT["info"]["profiles"] as $id  => $val){?>
									<option <?=$MODULE_OUTPUT["form_back"]["profile_id"]==$id ? "selected" : ""?> value="<?=$id?>"><?=$val?></option>
								<?}?>
							</select>
						</div>

						<div class="input-group"> 
						    <span class="input-group-addon" style="min-width: 150px;">��� ������<span class="obligatorily">*</span></span>
							<input type="text" class="form-control year2" name="year" value="<?=$MODULE_OUTPUT["form_back"]["year"]?>" />
							<input type="hidden" name="id" value="<?=$MODULE_OUTPUT["form_back"]["id"]?>" />
						</div>

						<button type="submit" class="btn btn-nsau btn-block" name="update_doc">���������</button>
						<button type="button" class="btn btn-nsau btn-block" onclick="history.back(-2);" "/>������</button>
					</form>
			    </div>

			    <div class="col-sm-2">
			    </div>

			    <div class="col-sm-5">
			    	<legend><center>���������� ��������</center></legend>
				    <form method="post" action="">
						<div class="input-group" style="display: none"> 
						    <span class="input-group-addon" style="border-bottom: 0px!important; min-width: 150px;">������������<span class="obligatorily">*</span></span>
							<select name="qualification_id" class="form-control" style="border-bottom: 0px!important;">
								<option value=""></option>
								<?foreach($MODULE_OUTPUT["info"]["qualifications"] as $id  => $val){?>
									<option <?=$MODULE_OUTPUT["form_back"]["qualification_id"]==$id ? "selected" : ""?> value="<?=$id?>"><?=$val?></option>
								<?}?>
							</select>
						</div>

						<div class="input-group" style="display: none"> 
						    <span class="input-group-addon" style="border-bottom: 0px!important; min-width: 150px;">��� ���������<span class="obligatorily">*</span></span>
							<select name="doc_type_id" class="form-control"  style="border-bottom: 0px!important;">
								<option value=""></option>
								<?foreach($MODULE_OUTPUT["info"]["types"] as $id  => $val){?>
									<option <?=$MODULE_OUTPUT["form_back"]["doc_type_id"]==$id ? "selected" : ""?> value="<?=$id?>"><?=$val?></option>
								<?}?>
							</select>
						</div>

						<div class="input-group" style="display: none"> 
						    <span class="input-group-addon" style="border-bottom: 0px!important; min-width: 150px;">����<span class="obligatorily">*</span></span>
							<select name="file_id" class="form-control" style="border-bottom: 0px!important;">
								<option value=""></option>
								<?foreach($MODULE_OUTPUT["info"]["files"] as $id  => $val){?>
									<option <?=$MODULE_OUTPUT["form_back"]["file_id"]==$id ? "selected" : ""?> value="<?=$id?>"><?=$val?></option>
								<?}?>
							</select>
						</div>
					
						<div class="input-group"> 
						    <span class="input-group-addon" style="border-bottom: 0px!important; min-width: 150px;">����� ��������<span class="obligatorily">*</span></span>
							<select name="form" class="form-control" style="border-bottom: 0px!important;">
								<option value=""></option>
								<option <?=$MODULE_OUTPUT["form_back"]["form"]==1 ? "selected" : ""?> value="1">�����</option>
								<option <?=$MODULE_OUTPUT["form_back"]["form"]==2 ? "selected" : ""?> value="2">�������</option>
								<option <?=$MODULE_OUTPUT["form_back"]["form"]==3 ? "selected" : ""?> value="3">����-�������</option>
								<!-- <option <?=$MODULE_OUTPUT["form_back"]["form"]==4 ? "selected" : ""?> value="4">�������-����������</option> -->
							</select>
						</div>

						<div class="input-group"> 
						    <span class="input-group-addon" style="border-bottom: 0px!important; min-width: 150px;">�������</span>
							<select name="profile_id" class="form-control" style="border-bottom: 0px!important;">
								<option value=""></option>
								<?foreach($MODULE_OUTPUT["info"]["profiles"] as $id  => $val){?>
									<option <?=$MODULE_OUTPUT["form_back"]["profile_id"]==$id ? "selected" : ""?> value="<?=$id?>"><?=$val?></option>
								<?}?>
							</select>
						</div>

						<div class="input-group" style="display: none"> 
						    <span class="input-group-addon" style="min-width: 150px;">��� ������<span class="obligatorily">*</span></span>
							<input type="text" class="form-control year2" name="year" value="<?=$MODULE_OUTPUT["form_back"]["year"]?>" />
							<input type="hidden" name="id" value="<?=$MODULE_OUTPUT["form_back"]["id"]?>" />
						</div>

						<button type="submit" class="btn btn-nsau btn-block" name="copy_doc">�����������</button>
						<button type="button" class="btn btn-nsau btn-block" onclick="history.back(-2);" "/>������</button>
					</form>	
					<?
					if ($MODULE_OUTPUT['copy_result']==1) 
					{
						?><div class="alert alert-success">���� ������� ����������</div><?
					}
					elseif($MODULE_OUTPUT['copy_result']==2)
					{
						?><div class="alert alert-danger">������ ����������� �����</div><?
					}
					?>
			    </div>
			</div>
		</fieldset>
	<?
	}//CF::Debug($MODULE_OUTPUT);
	break;?>
	
	<?case "show_table_education":
    {
		?>
		<h2>���������� �� ��������������� ����������</h2>
		<h4>� ������������� ��� �� �������� ��������������� ���������� ������� � �������� ����������������� ����������� <strong>�� �����������</strong> �������� �� ������� �����, � ����� �������� � ����������� <strong>�������������</strong> ������������ �������� � ������������� ��������������� ����������.</h4>
		<br>
	 	<table class="nsau_table employees" itemprop="eduOp">
	    	<tbody>
		        <thead>
    				<tr>
						<!-- <th width="1%">� �/�</th> -->
						<th width="1%">��� �������������</th>
						<th width="7%">������������ ������������� � ����������� ����������</th>

						<th width="10%">������� �����������</th>
						<th width="1%">����� ��������</th>
						<!-- <th width="1%">��������������� ���������</th> -->

						<th width="4%">������� ����</th>
						<th width="10%">��������� � ������� ���������� �����������</th>
						<th width="7%" >����������� ������� ������</th>
						<th width="2%" >������������ ���������, ������������� ���� ��� ����������� ���������������� ��������</th>
						<th width="4%" >��������</th>
						<th width="1%" >������������� ��� ���������� ��������������� �������� ������������ �������� � ������������� ��������������� ����������</th>
					</tr>
		        </thead>
		        <?
		        foreach ($MODULE_OUTPUT['all_qual_old_type'] as $global_type )
		        {
		        	/*��������� ������� ����������*/
		        	switch ($global_type)
		    		{
		    			case 'secondary':
		    			{
		    				?>
		    				<!-- <tr class="tr_highlight_honey"><td colspan="10"><a><strong>������� ���������������� ����������� - ���������� ������������ �������� �����</strong></a></td></tr> -->
		    				<?
		    			}
		    			break;
		    			case 'bachelor':
		    			{
		    				?>
		    				<!-- <tr class="tr_highlight_honey"><td colspan="10"><a ><strong>������ ����������� - �����������</strong></a></td></tr> -->
		    				<?
		    			}
		    			break;
		    			case 'magistracy':
		    			{
		    				?>
		    				<!-- <tr class="tr_highlight_honey"><td colspan="10"><a ><strong>������ ����������� - ������������</strong></a></td></tr> -->
		    				<?
		    			}
		    			break;
		    			case 'graduate':
		    			{
		    				?>
		    				<!-- <tr class="tr_highlight_honey"><td colspan="10"><a ><strong>������ ����������� - �����������</strong></a></td></tr> -->
		    				<?
		    			}
		    			break;

		    			case 'higher':
		    			{
		    				?>
		    				<!-- <tr class="tr_highlight_honey"><td colspan="10"><a ><strong>������ ����������� - �����������</strong></a></td></tr> -->
		    				<?
		    			}
		    			break;
		    		}
			        foreach ($MODULE_OUTPUT[$global_type] as $type_name => $type)
					{
						if($type["hide_accredit"]!=1)
						{
							$ok=0;
							foreach ($type['files'] as $files)
							{
								if ($files['doc_type_id'])
								{
									if ($files['doc_type_id']==2 && $files['year']==$type['year'] && $files['form']==$type['form'] && $files['profile']==$type['profile'])
									{
										$ok=1;
									}

									if ($files['doc_type_id']==5 && $files['year']==$type['year'] && $files['form']==$type['form'] && $files['profile']==$type['profile'])
									{
										$ok=1;
									}

									if ($files['doc_type_id']==4 && $files['year']==$type['year'] && $files['form']==$type['form'] && $files['profile']==$type['profile'])
									{
										$ok=1;
									}

									if ($files['doc_type_id']==3 && $files['year']==$type['year'] && $files['form']==$type['form'] && $files['profile']==$type['profile'])
									{
										$ok=1;
									}

									if ($files['doc_type_id']==6 && $files['year']==$type['year'] && $files['form']==$type['form'] && $files['profile']==$type['profile'])
									{
										$ok=1;
									}


									if ($files['doc_type_id']==1 && $files['year']==$type['year'] && $files['form']==$type['form'] && $files['profile']==$type['profile'])
									{
										$ok=1;
									}
								}
							}

							if(!$type['form'] && !$type['year'])
							{
								$ok=1;
							}


							if ($ok==1)
							{
							$ok=0;
							?>
							<tr>
								<!-- ��� -->
								<td align="center" itemprop="eduCode">
									<?=$type['code']?>
									<?
									//$id++;echo $id;
									?>
								</td>

								<td align="center" itemprop="eduName">
									<?
									foreach ($type['files'] as $files)
									{
										if ($files['doc_type_id'])
										{
											if ($files['doc_type_id']==5 && $files['year']==$type['year'] && $files['form']==$type['form'] && $files['profile']==$type['profile'])
											{
												$opop = $files['file_id'];
											}
										}
									}

									
									$current_name = explode ("[", $type_name);
									$year = explode ("]", $type_name);

									if ($opop)
									{?>
										<a class="<?= isset($files['with_sig']) ? 'sig_with': (isset($files['sig']) ? 'sig_files': '')?>" href="/file/<?=$opop?>" itemprop="opMain"><?=$current_name[0]?> <?=$year[1]?><br>
										<?unset($opop);
										
									}
									else
									{
										echo $current_name[0]." ".$year[1]."<br>";
										
									}
									?>
								</td>

								<!-- ������� ����������� -->
								<td align="center" itemprop="eduLevel">
								<?	        	
						        	switch ($global_type)
						    		{
						    			case 'secondary':
						    			{
						    				?>������� ���������������� ����������� - ���������� ������������ �������� �����<?
						    			}
						    			break;
						    			case 'bachelor':
						    			{
						    				?>������ ����������� - �����������<?
						    			}
						    			break;
						    			case 'magistracy':
						    			{
						    				?>������ ����������� - ������������<?
						    			}
						    			break;
						    			case 'graduate':
						    			{
						    				?>������ ����������� - ���������� ������ ������ ������������<?
						    			}
						    			break;

						    			case 'higher':
						    			{
						    				?>������ ����������� - �����������<?
						    			}
						    			break;
						    		}
								?>
								</td>

								<!-- ����� �������� -->
								<td align="center" itemprop="eduForm">
									<?
									switch ($type['form'])
									{
										case '1':
											?>�����<?;
											break;

										case '2':
											?>�������<?;
											break;

										case '3':
											?>����-�������<?;
											break;
										// case '4':
										// 	?>�������-����������<?;
										// 	break;
									}
									?>
								</td>

								<!-- ��������������� ��������� -->
																<!-- <td align="center"> 
									<?
									foreach ($type['files'] as $files)
									{
										if ($files['doc_type_id'])
										{
											if ($files['doc_type_id']==5 && $files['year']==$type['year'] && $files['form']==$type['form'] && $files['profile']==$type['profile'])
											{
												$opop = $files['file_id'];
											}
										}
									}
									if ($opop)
									{?>
										<a itemprop="opMain" href="/file/<?=$opop?>">��������������� ���������<br> 
										<?unset($opop);
									}
									?>
								 </td> -->

								<!-- ������� ���� -->
								<td align="center">
									<?
									foreach ($type['files'] as $files)
									{
										if ($files['doc_type_id'])
										{
											if ($files['doc_type_id']==2 && $files['year']==$type['year'] && $files['form']==$type['form'] && $files['profile']==$type['profile'])
											{?>
												<a class="<?= $files['with_sig'] ? 'sig_with': (isset($files['sig']) ? 'sig_files': '')?>" itemprop="educationPlan" href="/file/<?=$files['file_id']?>">������� ����</a><br>
											<?}
										}
									}
									?>
								</td>

								<!-- ��������� -->
								<td align="center">
									<?
									foreach ($type['files'] as $files)
									{
										if ($files['doc_type_id'])
										{
											if ($files['doc_type_id']==4 && $files['year']==$type['year'] && $files['form']==$type['form'] && $files['profile']==$type['profile'])
											{?>
												<a class="<?= $files['with_sig'] ? 'sig_with': (isset($files['sig']) ? 'sig_files': '')?>" itemprop="educationAnnotation" href="/file/<?=$files['file_id']?>">��������� � ������� ���������� �����������</a><br>
											<?}
										}
									}
									?>
								</td>

								<!-- ����������� ������� ������ -->
								<td align="center">
									<?
									foreach ($type['files'] as $files)
									{
										if ($files['doc_type_id'])
										{
											if ($files['doc_type_id']==3 && $files['year']==$type['year'] && $files['form']==$type['form'] && $files['profile']==$type['profile'])
											{?>
												<a class="<?= $files['with_sig'] ? 'sig_with': (isset($files['sig']) ? 'sig_files': '')?>" itemprop="educationShedule" href="/file/<?=$files['file_id']?>">����������� ������� ������</a><br>
											<?}
										}
									}
									?>
								</td>


					    		<!-- ������������ ��������� -->
						        <td align="center">
							        <?
							        	if ($global_type == "secondary") 
							        	{
							        		if( strripos($current_name[0], '(����)') === false)
							        		{
							        			echo "<a href='https://nsau.edu.ru/spo-fac/uchebno-metodicheskoe-obespechenie/'>������������ ���������</a><br>";
							        		}
							        		else
							        		{
												echo "<a href='https://nsau.edu.ru/kainsk/otdeleniya/'>������������ ���������</a><br>";
							        		}
							        	}
							        	elseif($global_type == "graduate")
							        	{?>
							        		<a itemprop="methodology" href="https://nsau.edu.ru/nir/depart_teaching/kafedra-gumanitarnykh-nauk/metodicheskaya-rabota/?list_type=spec#<?=$type['code']?>">������������ ���������</a><br>
							        	<?}
							        	else
							        	{?>
							        		<a itemprop="methodology" href="<?=$type['facultet']?>#<?=$type['code']?>">������������ ���������</a><br>
							        	<?}

										//������� ��������� ����������
							        	foreach ($type['files'] as $files)
										{
											if ($files['doc_type_id'])
											{
												if ($files['doc_type_id']==31 && $files['year']==$type['year'] && $files['form']==$type['form'] && $files['profile']==$type['profile'])
												{ ?>
													<a class="<?= $files['with_sig'] ? 'sig_with': (isset($files['sig']) ? 'sig_files': '')?>" itemprop="educationShedule" href="/file/<?=$files['file_id']?>">������� ��������� ����������</a><br>
												<? }
											}
										}
						        	?>
						        </td>

								<!-- �������� -->
								<td align="center">
									<?
									foreach ($type['files'] as $files)
									{
										if ($files['doc_type_id'])
										{
											if ($files['doc_type_id']==6 && $files['year']==$type['year'] && $files['form']==$type['form'] && $files['profile']==$type['profile'])
											{?>
												<a class="<?= $files['with_sig'] ? 'sig_with': (isset($files['sig']) ? 'sig_files': '')?>" itemprop="eduPr" href="/file/<?=$files['file_id']?>">��������</a><br>
											<?}
										}
									}
									?>
								</td>

								<td align="center">
									<a itemprop="eduEl">-</a>
								</td>
							</tr>
							<?
						}
						}


					}
				}
				?>
		    </tbody>
		</table>
	    <?
    }
    break;

    case 'fucult_education':
    {
    	?>
		<h1>���������� �� ��������������� ����������</h1>
		        <?

		        foreach ($MODULE_OUTPUT['all_qual_old_type'] as $global_type )
		        {
					$hideAccreditCount = array_sum(\CF::arrayColumnExt($MODULE_OUTPUT[$global_type['old_type']['old_type']], 'hide_accredit'));
		            $typeCount = count($MODULE_OUTPUT[$global_type['old_type']['old_type']]);
		        	if (!empty($MODULE_OUTPUT[$global_type['old_type']['old_type']]) && ($hideAccreditCount == 0 || $hideAccreditCount < $typeCount))
		        	{
		        	/*��������� ������� ����������*/
		        	switch ($global_type['old_type']['old_type'])
		    		{
		    			case 'secondary':
		    			{
		    				?><center><a href="http://nsau.edu.ru/sveden/education/spo/" itemprop="EduLavel"><strong>������� ���������������� ����������� - ���������� ������������ �������� �����</strong></a></center><br><?
		    			}
		    			break;
		    			case 'bachelor':
		    			{
		    				?><center><a href="http://nsau.edu.ru/sveden/education/bach/" itemprop="EduLavel"><strong>������ ����������� - �����������</strong></a></center><br><?
		    			}
		    			break;
		    			case 'magistracy':
		    			{
		    				?><center><a href="http://nsau.edu.ru/sveden/education/mag/" itemprop="EduLavel"><strong>������ ����������� - ������������</strong></a></center><br><?
		    			}
		    			break;
		    			case 'graduate':
		    			{
		    				?><center><a href="http://nsau.edu.ru/sveden/education/graduate/" itemprop="EduLavel"><strong>������ ����������� - ���������� ������ ������ ������������</strong></a></center><br><?
		    			}
		    			break;
		    			case 'higher':
		    			{
		    					?><center><a href="http://nsau.edu.ru/sveden/education/vpo/" itemprop="EduLavel"><strong>������ ����������� - �����������</strong></a></center><br><?
		    			}
		    			break;
		    		}
		    		}

			       	foreach ($MODULE_OUTPUT[$global_type['old_type']['old_type']] as $type_name => $type)
					{
						//CF::Debug($MODULE_OUTPUT[$global_type['old_type']['old_type']]);
						if($type["hide_accredit"]!=1)
						{
							$ok=0;
							foreach ($type['files'] as $files)
							{
								if ($files['doc_type_id'])
								{
									if ($files['doc_type_id']==2 && $files['year']==$type['year'] && $files['form']==$type['form'] && $files['profile']==$type['profile'])
									{
										$ok=1;
									}

									if ($files['doc_type_id']==5 && $files['year']==$type['year'] && $files['form']==$type['form'] && $files['profile']==$type['profile'])
									{
										$ok=1;
									}

									if ($files['doc_type_id']==4 && $files['year']==$type['year'] && $files['form']==$type['form'] && $files['profile']==$type['profile'])
									{
										$ok=1;
									}

									if ($files['doc_type_id']==3 && $files['year']==$type['year'] && $files['form']==$type['form'] && $files['profile']==$type['profile'])
									{
										$ok=1;
									}

									if ($files['doc_type_id']==6 && $files['year']==$type['year'] && $files['form']==$type['form'] && $files['profile']==$type['profile'])
									{
										$ok=1;
									}


									if ($files['doc_type_id']==1 && $files['year']==$type['year'] && $files['form']==$type['form'] && $files['profile']==$type['profile'])
									{
										$ok=1;
									}
								}
							}

							if(!$type['form'] && !$type['year'])
							{
								$ok=1;
							}


							if ($ok==1)
							{
							$ok=0;
							?>
							<tr>
							
							<a name="<?=$type['code'];?>" style="height: 1px; display: block; padding-top: 50px;"></a>
							<!-- <strong><div class="izop_fac" style="margin-bottom: 10px;"><?=$type['facultet_name']?></div></strong> -->
							<div class="program">
								
								<div style="margin-top:-12px;">
									<span style="background:#fff">
										<?echo $type_name."<br>";?>
									</span>
								</div>
								<!-- ���� -->
								<td align="center" itemprop="OOP_main">
									<?
									foreach ($type['files'] as $files)
									{
										if ($files['doc_type_id'])
										{
											if ($files['doc_type_id']==5 && $files['year']==$type['year'] && $files['form']==$type['form'] && $files['profile']==$type['profile'])
											{
												echo "<a href='/file/".$files['file_id']."'>����</a><br>";
											}
										}
									}
									?>
								</td>

								<!-- ���� -->
								<td align="center">
									<?
									foreach ($type['files'] as $files)
									{
										if ($files['doc_type_id'])
										{
											if ($files['doc_type_id']==1 && $files['year']==$type['year'] && $files['form']==$type['form'] && $files['profile']==$type['profile'])
											{
												echo "<a href='/file/".$files['file_id']."'>����</a><br>";
											}
										}
									}
									?>
								</td>

								<!-- ������� ���� -->
								<td align="center" itemprop="education_plan">
									<?
									foreach ($type['files'] as $files)
									{
										if ($files['doc_type_id'])
										{
											if ($files['doc_type_id']==2 && $files['year']==$type['year'] && $files['form']==$type['form'] && $files['profile']==$type['profile'])
											{
												echo "<a href='/file/".$files['file_id']."'>������� ����</a><br>";
											}
										}
									}
									?>
								</td>

								<!-- ��������� -->
								<td align="center" itemprop="education_annotation">
									<?
									foreach ($type['files'] as $files)
									{
										if ($files['doc_type_id'])
										{
											if ($files['doc_type_id']==4 && $files['year']==$type['year'] && $files['form']==$type['form'] && $files['profile']==$type['profile'])
											{
												echo "<a href='/file/".$files['file_id']."'>��������� � ������� ���������� �����������</a><br>";
											}
										}
									}
									?>
								</td>

								<!-- ����������� ������� ������ -->
								<td align="center" itemprop="education_shedule">
									<?
									foreach ($type['files'] as $files)
									{
										if ($files['doc_type_id'])
										{
											if ($files['doc_type_id']==3 && $files['year']==$type['year'] && $files['form']==$type['form'] && $files['profile']==$type['profile'])
											{
												echo "<a href='/file/".$files['file_id']."'>����������� ������� ������</a><br>";
											}
										}
									}
									?>
								</td>

								<!-- �������� -->
								<td align="center" itemprop="EduPr" >
									<?
									foreach ($type['files'] as $files)
									{
										if ($files['doc_type_id'])
										{
											if ($files['doc_type_id']==6 && $files['year']==$type['year'] && $files['form']==$type['form'] && $files['profile']==$type['profile'])
											{
												echo "<a href='/file/".$files['file_id']."'>��������� ������� � ���������������� �������</a><br>";
											}
										}
									}
									?>
								</td>

								<!-- ��������� ��������������� �������� ���������� -->
								<td align="center" itemprop="EduPr" >
									<?
									foreach ($type['files'] as $files)
									{
										if ($files['doc_type_id'])
										{
											if ($files['doc_type_id']==7 && $files['year']==$type['year'] && $files['form']==$type['form'] && $files['profile']==$type['profile'])
											{
												echo "<a href='/file/".$files['file_id']."'>��������� ��������������� �������� ����������</a><br>";
											}
										}
									}
									?>
								</td>

								<!-- �������� ����������� -->
								<td align="center" itemprop="EduPr" >
									<?
									foreach ($type['files'] as $files)
									{
										if ($files['doc_type_id'])
										{
											if ($files['doc_type_id']==8 && $files['year']==$type['year'] && $files['form']==$type['form'] && $files['profile']==$type['profile'])
											{
												echo "<a href='/file/".$files['file_id']."'>�������� �����������</a><br>";
											}
										}
									}
									?>
								</td>

								<!-- �����������-����������� ����������� -->
								<td align="center" itemprop="EduPr" >
									<?
									foreach ($type['files'] as $files)
									{
										if ($files['doc_type_id'])
										{
											if ($files['doc_type_id']==9 && $files['year']==$type['year'] && $files['form']==$type['form'] && $files['profile']==$type['profile'])
											{
												echo "<a href='/file/".$files['file_id']."'>�����������-����������� �����������</a><br>";
											}
										}
									}
									?>
								</td>

								<!-- �������������� ������� � ������-������������ ����������� -->
								<td align="center" itemprop="EduPr" >
									<?
									foreach ($type['files'] as $files)
									{
										if ($files['doc_type_id'])
										{
											if ($files['doc_type_id']==10 && $files['year']==$type['year'] && $files['form']==$type['form'] && $files['profile']==$type['profile'])
											{
												echo "<a href='/file/".$files['file_id']."'>�������������� ������� � ������-������������ �����������</a><br>";
											}
										}
									}
									?>
								</td>

								<!-- ������������ �������� �� ���������� ��������� ���������������� ������ -->
								<td align="center" itemprop="EduPr" >
									<?
									foreach ($type['files'] as $files)
									{
										if ($files['doc_type_id'])
										{
											if ($files['doc_type_id']==11 && $files['year']==$type['year'] && $files['form']==$type['form'] && $files['profile']==$type['profile'])
											{
												echo "<a href='/file/".$files['file_id']."'>������������ �������� �� ���������� ��������� ���������������� ������</a><br>";
											}
										}
									}
									?>
								</td>

								<!-- ������ ��������������� ��������� -->
								<td align="center" itemprop="EduPr" >
									<?
									foreach ($type['files'] as $files)
									{
										if ($files['doc_type_id'])
										{
											if ($files['doc_type_id']==12 && $files['year']==$type['year'] && $files['form']==$type['form'] && $files['profile']==$type['profile'])
											{
												echo "<a href='/file/".$files['file_id']."'>������ ��������������� ���������</a><br>";
											}
										}
									}
									?>
								</td>

								<!-- ������� ��������� ���������� -->
								<td align="center" itemprop="EduPr">
									<? foreach ($type['files'] as $files) {
										if ($files['doc_type_id'])
										{
											if ($files['doc_type_id']==31 && $files['year']==$type['year'] && $files['form']==$type['form'] && $files['profile']==$type['profile'])
											{ ?>
									<a href="/file/<?=$files['file_id']?>">������� ��������� ����������</a><br>
									<? }
										}
									}
									?>
								</td>







							</div>
							<?
						}
						}
					}
				}
				?>
	    <?
    }
    break;

    case 'import_panel':
    {
    	?>
<!--     		<form method="post" action="">
    			<input type="submit" name="import" value="import">
    		</form> -->
    	<?
    	//echo $MODULE_OUTPUT['import_message'];
	}

    break;
}
?>
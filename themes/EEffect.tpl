<?header("Content-type: text/html; charset=windows-1251");
$MODULE_MESSAGES = array(
101 => "��� ���� ����������� ��� ����������",
401 => "������ �������������� ���������. ���������� � ��������������.",
402 => "������ �������� �� ���������� ��� �� �������� ��� ��������������. ���������� � ��������������.",
102 => "�������� � ����� ���������� � �������� ��� ��������.",
403 => "������ �������.",
404 => "������ �������������� ��������� �����.",
405 => "� ������ ������ �������� ���������� ����������."
);
$CRITICAL_ERRORS = array(401, 402, 405);//����� ������ �� ��������� ��������� ���������� ��������

if ($MODULE_OUTPUT["messages"]["good"])
{
    $array = array();

    foreach ($MODULE_OUTPUT["messages"]["good"] as $elem)
    {
        if (isset($MODULE_MESSAGES[$elem]))
        {
            $array[] = $MODULE_MESSAGES[$elem];
        }
    }

    if ($array)
    {
        foreach ($array as $elem)
        {
            echo "<p class=\"message\">".$elem."</p>\n";
        }
    }
}

if ($MODULE_OUTPUT["messages"]["bad"])
{
    $array = array();

    foreach ($MODULE_OUTPUT["messages"]["bad"] as $elem)
    {
        if (isset($MODULE_MESSAGES[$elem]))
        {
            $array[] = $MODULE_MESSAGES[$elem];
        }
    }

    if ($array)
    {
        foreach ($array as $elem)
        {
            echo "<p class=\"message red\">".$elem."</p>\n";
        }
    }
		
}
switch($MODULE_OUTPUT["mode"])
{  
  case "indicators": {
		$types = $MODULE_OUTPUT["work_type"];
		$form_back = $MODULE_OUTPUT["form_back"];
		?>

		<fieldset>
			<legend>�������� ����������</legend>
			<form method="post" action="">
				<div class="wide">
					<div class="form-notes">
						<label>������������ ����������<span class="obligatorily">*</span></label><br>
						<input type="text" name="name" value="<?=$form_back["name"]?>"><br>
						<label>�������� ������<span class="obligatorily">*</span></label><br>
						<input type="text" name="indicator" value="<?=$form_back["indicator"]?>"><br>
						<label>�������������<span class="obligatorily">*</span></label><br>
						<input type="text" name="period" value="<?=$form_back["period"]?>"><br>	
						<label>������ ������<span class="obligatorily">*</span></label><br>
						<input type="text" name="size" value="<?=$form_back["size"]?>"><br>	
						<label>��� ������<span class="obligatorily">*</span></label><br>	
						<select name="type" id="">
							<?foreach($types as $t_id => $name) {?>
								<option value="<?=$t_id?>" <?=($t_id==$form_back["type"]) ? "selected" : ""?>><?=$name?></option>
							<?}?>
						</select><br>
						<label>������� ���������</label><br>
						<input type="text" name="conditions" value="<?=$form_back["conditions"]?>"><br>	
						<label>������� ��� ������� ������</label><br>
						<input type="text" name="formula" value="<?=$form_back["formula"]?>"><br>				
						<label>������ (������ ��� ��������������� ������� ����������)</label><br>
						<input type="text" name="macros" value="<?=$form_back["macros"]?>"><br>		
						<label>�������������� ������ ���������� (������ ��� ������������ �����������)</label>
						<input type="checkbox" name="auto" <?=$form_back["auto"] ? "checked" : ""?> value="1"><br />	
						<label>��������� ��������� �������� (���������� ��� ��������� �������������� �����������)</label>
						<input type="checkbox" name="deny_form" <?=$form_back["deny_form"] ? "checked" : ""?> value="1"><br />	
						<label>����������� �����<span class="obligatorily">*</span></label><br>
						<table class="nsau_table" style="width: 100%">
							<thead>
								<tr>
									<th width="15%">���</th>
									<th width="40%">��������</th>
									<th width="35%">���������</th>
									<th width="10%"></th>
								</tr>
							</thead>
							<tbody>
								<?foreach($form_back["fields"]["type"] as $f_id => $field) {?>
									<tr class="new_field">
										<td>
											<select class="select_type" style="width: 100%" name="fields[type][]">
												<option value="text" <?=($field=="text") ? "selected" : ""?>>�����</option>
												<option value="file" <?=($field=="file") ? "selected" : ""?>>����</option>
											</select>
										</td>
										<td><input class="add_desc" name="fields[desc][]" type="text" style="width: 90%" value="<?=$form_back["fields"]["desc"][$f_id]?>"></td>
										<td><input class="add_help" name="fields[help][]" type="text" style="width: 90%" value="<?=$form_back["fields"]["help"][$f_id]?>"></td>
										<td style="height: auto;" class="save_button del_field"><small><strong>[�������]</strong></small></td>
									</tr>						
								<?}?>
								<tr class="add_tmp">
									<td>
										<select class="select_type" style="width: 100%">
											<option value="text">�����</option>
											<option value="file">����</option>
										</select>
									</td>
									<td><input class="add_desc" type="text" style="width: 90%"></td>
									<td><input class="add_help" type="text" style="width: 90%"></td>
									<td class="save_button add_field"><small><strong>[��������]</strong></small></td>
								</tr>
							</tbody>
						</table>
						<label><input type="radio" name="ind_type" value="1" checked> ������������</label><br>
						<label><input type="radio" name="ind_type" value="0"> �������������</label><br>
						<label>������� � ��������� ���������:</label><br>
						<?foreach($MODULE_OUTPUT["posts"] as $post) {?>
							<input type="checkbox" name="posts[]" <?=in_array($post["id"], $indicator["posts"]) ? "checked" : ""?> value="<?=$post["id"]?>"><?=$post["name"]?>
						<?}?>
						<div class="wide">
							<input type="submit" value="��������">
						</div>
					</div>
				</div>
			</form>
		</fieldset>	
		<div class="wide">
			<h1>���������� ������������� ������������ ��������������</h1>
			<input type="radio" name="qwe" class="ind_selector" checked value="all">���
			<?foreach($MODULE_OUTPUT["posts"] as $post) {?>
				<input type="radio" name="qwe" class="ind_selector" value="<?=$post["id"]?>"><?=$post["name"]?>
			<?}?>
			<table class="nsau_table" style="width: 100%">
				<thead>
					<tr>
						<th width="20%">������������ ����������</th>
						<th width="15%">�������� ������</th>
						<th width="15%">�������������</th>
						<th width="35%">������� ���������</th>  
						<th width="5%">������ ������</th>  
						<th width="10%"></th>
					</tr>
				</thead>
				<tbody>
					<?foreach($MODULE_OUTPUT["indicators"] as $ind_id=>$ind){?>
						<tr><td colspan="6" class="effect head_table_seporator"><?=(($ind_id==0) ? "������������� ����������" : "������������ ����������")?></td></tr>
						<?foreach($types as $t_id => $name) {?>	
							<?if(!empty($ind[$t_id])) {?>
								<tr><td colspan="6" class="effect table_seporator"><?=$name?></td></tr>
								<?foreach($ind[$t_id] as $indicator) {?>
									<tr data-id="<?=$indicator["id"]?>" class="postall row_ind<?foreach($indicator["posts"] as $post_id) {?><?if(!empty($post_id)){?> post<?=$post_id?><?}}?>">
										<td rowspan="3"><?=$indicator["name"]?></td>
										<td rowspan="3"><?=$indicator["indicator"]?></td>
										<td rowspan="3"><?=$indicator["period"]?></td>
										<td rowspan="3"><?=$indicator["conditions"]?></td>
										<td rowspan="3"><?=$indicator["size"]?></td>
										<td class="save_button effect view"><small><strong>[��������]</strong></small></td>
									</tr>	
									<tr data-id="<?=$indicator["id"]?>">
										<td class="save_button effect edit"><small><strong>[�������������]</strong></small></td>
									</tr>
									<tr data-id="<?=$indicator["id"]?>">
										<td class="save_button effect delete"><small><strong>[�������]</strong></small></td>
									</tr>
								<?}?>
							<?}?>
						<?}?>
					<?}?>
					<?//CF::Debug($MODULE_OUTPUT["indicators"])?>
				</tbody>
			</table>	
		</div>

	<?}
	break;
	
	case "teacher_indicators": {?>
		<fieldset>
			<legend>�������� ��������</legend>
			<form method="post" action="">
				<div class="wide">
					<div class="form-notes">
						<label>�������<span class="obligatorily">*</span></label><br>
						<select name="department_id">
							<option value=""></option>
							<?foreach($MODULE_OUTPUT["departments"] as $dep_id  => $dep){?>
								<option <?=$MODULE_OUTPUT["form_back"]["department_id"]==$dep_id ? "selected" : ""?> value="<?=$dep_id?>"><?=$dep?></option>
							<?}?>
						</select><br />
						<label>���������<span class="obligatorily">*</span></label><br>
						<select name="post_id">
							<option value=""></option>
							<?foreach($MODULE_OUTPUT["posts"] as $post){?>
								<option <?=$MODULE_OUTPUT["form_back"]["post_id"]==$post["id"] ? "selected" : ""?> value="<?=$post["id"]?>"><?=$post["name"]?></option>
							<?}?>
						</select><br />
						<label>������<span class="obligatorily">*</span></label><br>
						<select name="rate">
							<option value=""></option>
							<?$i=0.1;?>
							<?while($i<=1.55){?>
								<option <?=(number_format($MODULE_OUTPUT["form_back"]["rate"], 2, '.', '')==number_format($i, 2, '.', '') ? "selected" : "")?> value="<?=$i?>"><?=$i?></option>								
								<?$i += 0.05;?>
							<?}?>
						</select><br />
						<label>��������� ���<span class="obligatorily">*</span></label><br>
						<select name="pps">
							<option value=""></option>
							<option <?=$MODULE_OUTPUT["form_back"]["pps"]==1 ? "selected" : ""?> value="1">�������</option>
							<option <?=$MODULE_OUTPUT["form_back"]["pps"]==2 ? "selected" : ""?> value="2">������� ����������������</option>
						</select><br />
						<div class="wide">
							<input type="submit" value="��������">
						</div>						
					</div>					
				</div>
			</form>
		</fieldset>
		<fieldset>
			<legend>��� ���������</legend>
<!-- 			<h3>
				<strong>
					<div class="red">
						<p>� ����� � ����������� ��������� ������ ������������ ��� ������ � ���������� �������� ������ �� �������������� �����.</p> 
						<p>� ������ ���������� ������������ ��������� ����� �������� �������������.</p>
					</div>
				</strong>
			</h3> -->
			<?if($MODULE_OUTPUT["dates"]["fact_to"] != '01.01.1970') {?>
				<h3>������� ����������� �������� ����������� �� <?=$MODULE_OUTPUT["dates"]["fact_to"]?> �� ������ � <?=$MODULE_OUTPUT["dates"]["from"]?> �� <?=$MODULE_OUTPUT["dates"]["to"]?></h3><br />
			<?}?>
			<table class="nsau_table" width="100%">
				<thead>
					<th>���������</th>
					<th>������ ����������</th>
					<th>������</th>
					<th>�������</th>
					<th>��������� ���</th>
				</thead>
				<tbody>
					<?if(!empty($MODULE_OUTPUT["contracts"])) {?>
						<?foreach($MODULE_OUTPUT["contracts"] as $cid=>$contract){?>
							<tr>
								<td><?=$MODULE_OUTPUT["posts"][$contract["post_id"]]["name"]?> (<a href="plan/<?=$cid?>"><?=$contract["edit_access"] ? "���������" : "��������"?></a>/<a href="post_info/<?=$contract["post_id"]?>">�������� ��������� �����������</a><?if($contract["edit_access"]){?>/<a class="contract_delete" href="delete/<?=$cid?>">�������</a><?}?>)</td>
								<td><?=$contract["date_from"]?> - <?=$contract["date_to"]?></td>
								<td class="edit rate">	
									<?if($contract["edit_access"]) {?>
										<select class="change_rate" data-id="<?=$cid?>">
											<?$i=0.1;?>
											<?while($i<=1.55){?>
												<option <?=(number_format($contract["rate"], 2, '.', '')==number_format($i, 2, '.', '') ? "selected" : "")?> value="<?=$i?>"><?=$i?></option>								
												<?$i += 0.05;?>
											<?}?>
										</select>
									<?} else {?>
										<?=$contract["rate"]?>
									<?}?>
								</td>
								<td><?=$MODULE_OUTPUT["departments"][$contract["department_id"]]?></td>
								<td>
									<?if($contract["edit_access"]) {?>
										<select class="change_pps" data-id="<?=$cid?>">
											<?foreach($MODULE_OUTPUT["pps"] as $pps_id => $pps_val){?>
												<option value="<?=$pps_id?>" <?=($contract["pps"]==$pps_id) ? "selected" : ""?>><?=$pps_val?></option>
											<?}?>
										</select>
									<?} else {?>
										<?=$contract["pps"]==1 ? $MODULE_OUTPUT["pps"][$contract["pps"]] : ($contract["pps"]==2 ? $MODULE_OUTPUT["pps"][$contract["pps"]] : $MODULE_OUTPUT["pps"][$contract["pps"]])?>
									<?}?>
								</td>
							</tr>
						<?}?>
					<?} else {?>
						<tr>
							<td colspan="5">��� ����������</td>
						</tr>
					<?}?>
				</tbody>
			</table>
		</fieldset>
		
	<?}
	break;
	
	
	case "post_info": {
		$types = $MODULE_OUTPUT["work_type"];?>
				<div class="wide">
			<h1><?=$MODULE_OUTPUT["post_name"]?>. ��������� ����������.</h1>
			<table class="nsau_table" style="width: 100%">
				<thead>
					<tr>
						<th width="35%">������������ ����������</th>
						<th width="30%">�������� ������</th>
						<th width="15%">�������������</th>
						<th width="15%">������� ���������</th>
						<th width="5%">������ ������</th>  
					</tr>
				</thead>
				<tbody>
					<?foreach($MODULE_OUTPUT["indicators"] as $ind_id=>$ind){?>
						<tr><td colspan="5" class="effect head_table_seporator"><?=(($ind_id==0) ? "������������� ����������" : "������������ ����������")?></td></tr>
						<?foreach($types as $t_id => $name) {?>	
							<?if(!empty($ind[$t_id])) {?>
								<tr><td colspan="5" class="effect table_seporator"><?=$name?></td></tr>
								<?foreach($ind[$t_id] as $indicator) {?>
									<tr data-id="<?=$indicator["id"]?>">
										<td><?=$indicator["name"]?></td>
										<td><?=$indicator["indicator"]?></td>
										<td><?=$indicator["period"]?></td>
										<td><?=$indicator["conditions"]?></td>
										<td><?=$indicator["size"]?></td>										
									</tr>	
								<?}?>
							<?}?>
						<?}?>
					<?}?>
				</tbody>
			</table>	
		</div>
	<?}
	break;
	
	
  case "indicators_edit": {
		$types = $MODULE_OUTPUT["work_type"];
		$indicator = (!empty($MODULE_OUTPUT["form_back"])) ? $MODULE_OUTPUT["form_back"] : $MODULE_OUTPUT["indicators"];?>
	<fieldset>
		<legend>������������� ��������</legend>
		<h2><?=$indicator["name"]?></h2>
		<form method="post" action="">
			<div class="wide">
			<div class="form-notes">
				<strong><p>��������� ��������</p></strong><br>
				<label>������������ ����������</label><br>
				<input type="text" name="name" value="<?=$indicator["name"]?>"><br>
				<label>�������� ������</label><br>
				<input type="text" name="indicator" value="<?=$indicator["indicator"]?>"><br>
				<label>�������������</label><br>
				<input type="text" name="period" value="<?=$indicator["period"]?>"><br>	
				<label>������ ������</label><br>
				<input type="text" name="size" value="<?=$indicator["size"]?>"><br>
				<label>��� ������</label><br>	
				<select name="type" id="">
					<?foreach($types as $t_id => $type) {?>
						<option value="<?=$t_id?>" <?=($indicator["type"]==$t_id) ? "selected" : ""?>><?=$type?></option>
					<?}?>
				</select><br>
				<label>������� ���������</label><br>
				<input type="text" name="conditions" value="<?=$indicator["conditions"]?>"><br>	
				<label>������� ��� ������� ������</label><br>
				<input type="text" name="formula" value="<?=$indicator["formula"]?>"><br>
				<label>�������������� ������ ���������� (������ ��� ������������ �����������)</label>
				<input type="checkbox" name="auto" <?=$indicator["auto"] ? "checked" : ""?> value="1"><br />				
				<label>������ (������ ��� ��������������� ������� ����������)</label><br>
				<input type="text" name="macros" value="<?=$indicator["macros"]?>"><br>	
				<label>��������� ��������� �������� (���������� ��� ��������� �������������� �����������)</label>
				<input type="checkbox" name="deny_form" <?=$indicator["deny_form"] ? "checked" : ""?> value="1"><br />	
			</div>
			<div class="form-notes">
				<strong><p>��������� �����</p></strong><br>
				<table class="nsau_table" style="width: 100%">
					<thead>
						<tr>
							<th width="15%">���</th>
							<th width="40%">��������</th>
							<th width="35%">���������</th>
							<th width="10%"></th>
						</tr>
					</thead>
					<tbody>
						<?foreach($indicator["fields"]["type"] as $f_id => $field) {?>
							<tr class="new_field">
								<td>
									<select class="select_type" style="width: 100%" name="fields[type][]">
										<option value="text" <?=($field=="text") ? "selected" : ""?>>�����</option>
										<option value="file" <?=($field=="file") ? "selected" : ""?>>����</option>
									</select>
								</td>
								<td><input class="add_desc" name="fields[desc][]" type="text" style="width: 90%" value="<?=$indicator["fields"]["desc"][$f_id]?>"></td>
								<td><input class="add_help" name="fields[help][]" type="text" style="width: 90%" value="<?=$indicator["fields"]["help"][$f_id]?>"></td>
								<td style="height: auto;" class="save_button del_field"><small><strong>[�������]</strong></small></td>
							</tr>						
						<?}?>
						<tr class="add_tmp">
							<td>
								<select class="select_type" style="width: 100%">
									<option value="text">�����</option>
									<option value="file">����</option>
								</select>
							</td>
							<td><input class="add_desc" type="text" style="width: 90%"></td> 
							<td><input class="add_help" type="text" style="width: 90%"></td> 
							<td style="height: auto;" class="save_button add_field"><small><strong>[��������]</strong></small></td>
						</tr>
					</tbody>
				</table>
				<strong><p>������� � ��������� ���������:</p></strong><br>
				<?foreach($MODULE_OUTPUT["posts"] as $post) {?>
					<input type="checkbox" name="posts[]" <?=in_array($post["id"], $indicator["posts"]) ? "checked" : ""?> value="<?=$post["id"]?>"><?=$post["name"]?>
				<?}?>
			</div>
			</div>
			<div class="wide">
				<label><input type="radio" name="ind_type" value="1" <?=$indicator["ind_type"]==1 ? "checked" : ""?>> ������������</label><br>
				<label><input type="radio" name="ind_type" value="0" <?=$indicator["ind_type"]==0 ? "checked" : ""?>> �������������</label><br>
				<input type="submit" value="���������">
			</div>
		</form>
	</fieldset>	
	<?}
	break;
	
	case "form": {?>
	<?header("Content-type: text/html; charset=windows-1251");?>
		<?if(!empty($MODULE_OUTPUT["iname"]["name"])){?>
			<h2 style="text-align: center; padding-top: 10px;">�������� �: "<?=$MODULE_OUTPUT["iname"]["name"]?>"</h2>	
		<?}?>
		<?if(!empty($MODULE_OUTPUT["name"]["name"])){?>
			<h2 style="text-align: center; padding-top: 10px;"><?=$MODULE_OUTPUT["name"]["name"]?></h2>	
		<?}?>
		<?foreach($MODULE_OUTPUT["forms"] as $id => $form) {?>
			<label><strong><?=$form["desc"]?></strong></label><br />
			<?if($form["type"]!="file") {?>
				<input  class="effect input" type="<?=$form["type"]?>" name="fields[<?=$form["type"]?>][<?=$id?>]">
			<?} else {?>
				<select class="file search_select" name="fields[<?=$form["type"]?>][<?=$id?>]">
					<option></option>
					<?foreach($MODULE_OUTPUT["files"] as $f_id => $file) {?>
						<option value="<?=$f_id?>"><?=$file?></option>
					<?}?>
				</select><input type="text" class="search_string file" placeholder="������� ������� �������� �����" value=""><br />
				<input type="text" class="file effect input select_input hidden" placeholder="������ �� ����" value=""><br />
				<a href="" class="toggle_file_field">����� ����������� ��������</a>
			<?}?>
			<img style="width: 20px; height: 20px; cursor: pointer;" title="<?=$form["help"]?>" src="/themes/images/question.png"><br />
		<?}?>
		<div data-id="<?=$_REQUEST["data"]["id"]?>" class="effect lmenu item2 add_ind_compl">[��������]</div>
	<?}
	break;

	case "show": {?>
	<?header("Content-type: text/html; charset=windows-1251");?>
		<?$indicator = $MODULE_OUTPUT["indicator"];$t=0;$f=0;?>
		<h2 style="text-align: center; padding-top: 10px;">�������������� ����������</h2>
		<?foreach($MODULE_OUTPUT["forms"] as $id => $form) {?>
			<label style="margin-left: 5px;"><strong><?=$form["desc"]?></strong></label>: <br>
			<?if($form["type"]=="text") {?>
				<input type="<?=$form["type"]?>" class="effect input" value="<?=$indicator["text".$t]?>"><?$t++;?>
			<?}?>
			<?if($form["type"]=="file") {?>
				<select class="file search_select <?=intval($indicator["file".$f]) ? "" : "hidden"?>" name="fields[<?=$form["type"]?>][<?=$id?>]">
					<option></option>
					<?foreach($MODULE_OUTPUT["files"] as $f_id => $file) {?>
						<option <?=$f_id==$indicator["file".$f] ? "selected" : ""?> value="<?=$f_id?>"><?=$file?></option>
					<?}?>
				</select><input type="text" class="search_string file <?=intval($indicator["file".$f]) ? "" : "hidden"?>" placeholder="������� ������� �������� �����" value=""><br />
				<input type="text" class="file effect input select_input <?=intval($indicator["file".$f]) ? "hidden" : ""?>" placeholder="������ �� ����" value="<?=intval($indicator["file".$f]) ? "" : $indicator["file".$f]?>"><br />
				<a href="" class="toggle_file_field">����� ����������� ��������</a>
				<?$f++;?>
			<?}?>	
			<img style="width: 20px; height: 20px; cursor: pointer;" title="<?=$form["help"]?>" src="/themes/images/question.png"><br />
		<?}?>
		<div data-id="<?=$_REQUEST["data"]["id"]?>" class="effect lmenu item2 edit_ind_compl">[c��������]</div>
	<?}
	break;	
	
	case "show_full": {?>
	<?header("Content-type: text/html; charset=windows-1251");?><?//CF::Debug($MODULE_OUTPUT["files"]);?>
		<h2 style="text-align: center; padding-top: 10px;"><?=$MODULE_OUTPUT["iname"]["name"]?></h2>
		<table class="nsau_table effect_table" width="100%">
			<thead>
				<tr>
					<?foreach($MODULE_OUTPUT["fields"] as $field) {	$types[] = $field["type"];?>
						<th><?=$field["desc"]?></th>
					<?}?>
				</tr>
			</thead>
			<tbody>
			<?foreach($MODULE_OUTPUT["indicators"] as $val) {?>
				<tr>
					<?$t=0;$f=0;?>
					<?foreach($types as $type) {?>
						<?if($type=="text"){?>
							<td><?=$val[$type.$t]?></td>
						<?}?>
						<?if($type=="file"){?>
							<td>
								<?if(intval($val[$type.$f])) {?>
									<a href="/file/<?=$val[$type.$f]?>"><?=$MODULE_OUTPUT["files"][$val[$type.$f]]?></a>
								<?} elseif(!empty($val[$type.$f])) {?>
									<?if(strpos($val[$type.$f], "http://")===false 
												&& strpos($val[$type.$f], "https://")===false) {
										$link = 'http://'.$val[$type.$f];
									} else {
										$link = $val[$type.$f];
									}?>
									<a href="<?=$link?>"><?=$link?></a>
								<?}?>	
							</td>
						<?}?>
						<?$type=="text" ? $t++ : $f++;?>
					<?}?>
				</tr>
			<?}?>
			</tbody>
		</table>
	<?}
	break;	
	
	case "plan": {?>
	<?if(CF::crtError($MODULE_OUTPUT["messages"]["bad"], $CRITICAL_ERRORS)) {?>
		<?$info = $MODULE_OUTPUT["pay_info"]?>
		<label>���: <?=$info["username"]?></label> <br />
		<label>����� ������: <?=$info["department"]?></label> <br />
		<label>���������: <?=$info["post"]?></label> <br />
		<label>������: <?=$info["rate"]?></label> <br />
		<label>��������� ���: <?=$info["pps"] == 1 ? "�������" : ($info["pps"] == 2 ? "������� ����������������" : "�� ������� ������")?></label> <br />
		<div class="effect tcontainer">
			<div class="effect tabs current">�������������</div>
			<!--<div class="effect tabs">������ ��������</div>!-->
			<div class="effect tabs">�����</div>
		</div>
		<input type="hidden" class="contract_id" value="<?=$MODULE_OUTPUT["contract_id"]?>">
		<div class="effect box" id="plan_ind">
			<div class="effect lmenu">
				<div class="effect lmenu item" id="add_indicator_form">[�������� ������������� ����������]</div>
				<div class="effect add_ind">
					<!-- <input type="radio" class="ind_type" checked name="ind_type" value="obligatory_ind"> ������������ -->
					<!-- <input type="radio" class="ind_type" name="ind_type" value="not_obligatory_ind"> �������������<br /> -->
					<select name="" id="indicators" style="max-width: 300px;">	
						<option></option>
						<?foreach($MODULE_OUTPUT["indicators"] as $ind_id => $ind){?>
							<option class="ind_sel_opt <?=$ind["ind_type"]==0 ? "" : "hidden"?>" value="<?=$ind_id?>"><?=$ind["name"]?></option>
						<?}?>
					</select>
					<div class="effect lmenu item2" id="add_indicator">[��������]</div>
				</div>
				<?//CF::Debug($MODULE_OUTPUT["indicators"]);?>
				<ul class="ind">
					<?for($i=1;$i>=0;$i--){?>
						<?$indicator = $MODULE_OUTPUT["plan_indicators"][$i]?>
						<li class="li_seporator <?=$i==0 ? "not_obligatory_ind" : "obligatory_ind"?>"><?=$i==0 ? "������������� ����������" : "������������ ���������� (����������: ".$MODULE_OUTPUT["contract_info"]["progress"]."%)"?></li>
						<?foreach($indicator as $ind_id => $ind) {?>
							<li class="show_full" data-id="<?=$ind_id?>"><?if($MODULE_OUTPUT["indicators"][$ind["ind"]["ind_id"]]["ind_type"]!=1) {?><span title="������� ����������" class="effect menu_button delete">x</span><?}?>
								<?if($MODULE_OUTPUT["indicators"][$ind["ind"]["ind_id"]]["deny_form"]!=1) {?>
									<span title="�������� ��������" class="effect menu_button add">+</span>
								<?}?>
							<div class="text head_ind_line"><?if($MODULE_OUTPUT["indicators"][$ind["ind"]["ind_id"]]["ind_type"]==1) {?>
								<?if($MODULE_OUTPUT["indicators"][$ind["ind"]["ind_id"]]["complete"]==1) {?>
									<img src="/themes/images/status_ok.png" style="width: 15px; height: 15px;"/>
								<?} else {?>
									<img src="/themes/images/status_cancel.png" style="width: 15px; height: 15px;"/>
								<?}?>
							<?}?><?=$MODULE_OUTPUT["indicators"][$ind["ind"]["ind_id"]]["name"]?> </div></li>
							<ul class="edit_ind" data-id="<?=$ind_id?>">
								<?foreach($ind["vals"] as $v_id => $v_name) {?>
									<li class="show" data-id="<?=$v_id?>"><span title="������� ��������" class="effect menu_button ind_buttons delete">x</span><div class="text ind_line"><?=$v_name?></div></li>
								<?}?>						
							</ul>
						<?}?>
					<?}?>
					<li class="lmenu_tmp hidden"><span title="������� ����������" class="effect menu_button delete">x</span><span title="�������� ��������" class="effect menu_button add">+</span><div class="text head_ind_line"></div></li>
					<ul class="edit_ind_tmp hidden"></ul>
					<li class="show_tmp hidden" data-id=""><span title="������� ��������" class="effect menu_button ind_buttons delete">x</span><div class="text ind_line"></div></li>
				</ul>
				
			</div>
			<div class="effect content"></div>
		</div>
		<div class="effect box" id="plan_report" style="display: none;">
		<br />
			<h2>��������������� ������ ������ �������������� ���������</h2>
		</div>
	<?}?>
	<?}
	break;?>
	<?case "plan_report": {?>
		<?header("Content-type: text/html; charset=windows-1251");?>
		<?if(CF::crtError($MODULE_OUTPUT["messages"]["bad"], $CRITICAL_ERRORS)) {//CF::Debug($MODULE_OUTPUT);?>
			<br />
			<h2>��������������� ������ ������ �������������� ��������� � ���������� ������������ �����������</h2>
			<table class="nsau_table" style="width: 100%">
				<thead>
					<tr>
						<th>������������ ����������</th>
						<th>���������/�����</th>
						<th>������</th>  
					</tr>
				</thead>
				<tbody>
					<tr class="effect table_seporator_left" style="text-align: center;">
						<td colspan="3"><h2>������������ ����������</h2></td>
					</tr>	
					<?foreach($MODULE_OUTPUT["plan_indicators"][1] as $ind_id => $o_ind) {?>
						<tr class="effect table_seporator_left">
							<td><?=$MODULE_OUTPUT["indicators"][$o_ind["ind"]["ind_id"]]["name"]?></td>
							<td><?=round($MODULE_OUTPUT["indicators"][$o_ind["ind"]["ind_id"]]["val"], 2)?>/<?=$MODULE_OUTPUT["indicators"][$o_ind["ind"]["ind_id"]]["cond"]?></td>
							<td>
								<span class="<?=($MODULE_OUTPUT["indicators"][$o_ind["ind"]["ind_id"]]["complete"]==1) ? "green" : "red"?>">
									<?=($MODULE_OUTPUT["indicators"][$o_ind["ind"]["ind_id"]]["complete"]==1) ? "��������" : "�� ��������"?>
								</span>
							</td>
						</tr>
						<?if(!empty($MODULE_OUTPUT["indicators"][$o_ind["ind"]["ind_id"]]["personal"])){?>
							<?foreach($MODULE_OUTPUT["indicators"][$o_ind["ind"]["ind_id"]]["personal"] as $p_ie => $per) {?>
								<tr class="effect table_seporator_left">
									<td>&nbsp;&nbsp;&nbsp;-<?=$per["name"]?></td>
									<td>
									
										<?if(!empty($per["val"])) {?>
											<?=round($per["val"], 2)?>/<?=$per["cond"]?>
										<?} else {?>
											<span class="red">�� ��������</span>
										<?}?>
									</td>
									<td>
										<span class="<?=($per["complete"]==1) ? "green" : "red"?>">
											<?=($per["complete"]==1) ? "��������" : "�� ��������"?>
										</span>
									</td>
								</tr>
							<?}?>
						<?}?>
					<?}?>
				</tbody>
			</table>
			<br \>
			<table class="nsau_table" style="width: 100%">
				<thead>
					<tr>
						<th>������������ ����������</th>
						<th>����������, ��</th>
						<th>������ ������</th>
					</tr>
				</thead>
				<tbody>
					<tr class="effect table_seporator_left">
						<td colspan="3" style="text-align: center;"><h2>������������� ����������</h2><br><small>������ ������ �������������� ��������� ����������� �������</small></td>
					</tr>	
					<?foreach($MODULE_OUTPUT["pay"] as $pid => $indicator) {?>
						<tr class="effect table_seporator_left">
							<td><?=$indicator["indicator"]["name"]?></td>
							<td><?=count($indicator["fields"])?></td>
							<td><?=$indicator["payments"] ? "-" : "��������� ��� ���� � ��������������� ����������."//$indicator["payments"]?></td>
							<?$total += $indicator["payments"];?>
						</tr>
						<?foreach($indicator["fields"] as $field) {?>
							<tr>
								<td colspan="2"><?=$field["text0"]?></td>
								<td>-<?//=$field["payment"]?></td>
							</tr>
						<?}?>
					<?}?>
					<tr class="effect table_seporator_left">
						<td colspan="2">�����:</td>
						<td><?//=$total?>-</td>
					</tr>
				</tbody>
			</table>
			<br>	
		<?}?>
	<?}?>
	<?break;?>
	<?case "plan_ind": {?>
		<?header("Content-type: text/html; charset=windows-1251")?>
		<?if(CF::crtError($MODULE_OUTPUT["messages"]["bad"], $CRITICAL_ERRORS)) {?>
			<div class="effect lmenu">
				<div class="effect lmenu item" id="add_indicator_form">[�������� ������������� ����������]</div>
				<div class="effect add_ind">
					<!-- <input type="radio" class="ind_type" checked name="ind_type" value="obligatory_ind"> ������������ -->
					<!-- <input type="radio" class="ind_type" name="ind_type" value="not_obligatory_ind"> �������������<br /> -->
					<select name="" id="indicators" style="max-width: 300px;">	
						<option></option>
						<?foreach($MODULE_OUTPUT["indicators"] as $ind_id => $ind){?>
							<option class="ind_sel_opt <?=$ind["ind_type"]==0 ? "" : "hidden"?>" value="<?=$ind_id?>"><?=$ind["name"]?></option>
						<?}?>
					</select>
					<div class="effect lmenu item2" id="add_indicator">[��������]</div>
				</div>
				
				<ul class="ind">
					<?for($i=1;$i>=0;$i--){?>
						<?$indicator = $MODULE_OUTPUT["plan_indicators"][$i]?>
						<li class="li_seporator <?=$i==0 ? "not_obligatory_ind" : "obligatory_ind"?>"><?=$i==0 ? "������������� ����������" : "������������ ���������� (����������: ".$MODULE_OUTPUT["contract_info"]["progress"]."%)"?></li>
						<?foreach($indicator as $ind_id => $ind) {?>
							<li class="show_full" data-id="<?=$ind_id?>"><?if($MODULE_OUTPUT["indicators"][$ind["ind"]["ind_id"]]["ind_type"]!=1) {?><span title="������� ����������" class="effect menu_button delete">x</span><?}?>
	
							<?if(($MODULE_OUTPUT["indicators"][$ind["ind"]["ind_id"]]["deny_form"]!=1)) {?>
								<span title="�������� ��������" class="effect menu_button add">+</span>
							<?}?>							
							<div class="text head_ind_line"><?if($MODULE_OUTPUT["indicators"][$ind["ind"]["ind_id"]]["ind_type"]==1) {?>
								<?if($MODULE_OUTPUT["indicators"][$ind["ind"]["ind_id"]]["complete"]==1) {?>
									<img src="/themes/images/status_ok.png" style="width: 15px; height: 15px;"/>
								<?} else {?>
									<img src="/themes/images/status_cancel.png" style="width: 15px; height: 15px;"/>
								<?}?>
							<?}?><?=$MODULE_OUTPUT["indicators"][$ind["ind"]["ind_id"]]["name"]?> </div></li>
							<ul class="edit_ind" data-id="<?=$ind_id?>">
								<?foreach($ind["vals"] as $v_id => $v_name) {?>
									<li class="show" data-id="<?=$v_id?>"><span title="������� ��������" class="effect menu_button ind_buttons delete">x</span><div class="text ind_line"><?=$v_name?></div></li>
								<?}?>						
							</ul>
						<?}?>
					<?}?>
					<li class="lmenu_tmp hidden"><span title="������� ����������" class="effect menu_button delete">x</span><span title="�������� ��������" class="effect menu_button add">+</span><div class="text head_ind_line"></div></li>
					<ul class="edit_ind_tmp hidden"></ul>
					<li class="show_tmp hidden" data-id=""><span title="������� ��������" class="effect menu_button ind_buttons delete">x</span><div class="text ind_line"></div></li>
				</ul>
				
			</div>
			<div class="effect content"></div>
		<?}?>
	<?}?>
	<?break;?>
	
	
	
	
	
	
		<?case "creports": {
		if($MODULE_OUTPUT["html"]) {?>
		<html>
		<head title="report">
			<style type="text/css">
				table.effect_report_table {
					border-collapse: collapse;
					border: 1px #000 solid;
					vertical-align: middle;
					font-weight: normal;
					text-align: left;
					width: 100%;
					/*margin: 0 auto;*/
				}
				table.effect_report_table thead,table.effect_report_table thead tr,table.effect_report_table thead td,table.effect_report_table thead th {
					width: 50px;
					text-align: center;
				}
				table.effect_report_table thead a {
				}
				table.effect_report_table tbody th {
					text-align: center;
				}
				table.effect_report_table th,table.effect_report_table td {
					border: 1px #000 solid;
					font-weight: normal;
				}
				table.effect_report_table .th_horizontal {
					
				}
				table.effect_report_table .div_vertical {

					/*-webkit-transform: rotate(270deg); transform: rotate(270deg);*/
				}
				table.effect_report_table .th_vertical {
					border-collapse: collapse;
					width: 50px;
				}

				table.effect_report_table .two_col {
					width: 25px;
				}
			</style>
		</head>
		<body>
	<?} else {		?>	
			<form action="" method="POST">
				������:
				<select name="period_id">
					<?foreach($MODULE_OUTPUT["periods"] as $pid => $period) {?>
						<option value="<?=$pid?>"><?=$period["date_from"]?> - <?=$period["date_to"]?></option>
					<?}?>
				</select><br />
				<input type="submit" value="��������"><br />
			</form>
	<?}?>
			<?$OBLIGATORY_SHORT = $MODULE_OUTPUT["obligatory_short"];?>
			<?if(!empty($OBLIGATORY_SHORT["pay"]) && $MODULE_OUTPUT["smode"]!="osf") {?>
				<h2>���������� ������������ ����������� �� ��������</h2>
				<table class="effect_report_table" style="font: 8px/10px 'Tahoma', Arial, sans-serif; ">
						<tr>
							<th rowspan="3">���</th>
							<th colspan="2">���������</th>
							<th rowspan="3" class="th_vertical">������ ������</th>
							<th rowspan="3" class="th_vertical">������ �������</th>
							<th rowspan="3">��������� ���</th>
							<th rowspan="3"><span  class="div_vertical" >���������� ������ �������� �������� ����������</span></th>
							<?foreach($OBLIGATORY_SHORT["indicators"] as $i=>$indicator) {?>
								<th colspan="2" class="th_vertical"><div class="div_vertical"><?=$indicator["name"]?></div></th>
							<?}?>
						</tr>
						<tr>
							<th rowspan="2">����������������</th>
							<th rowspan="2">�����������������</th>
							<?for($i=1;$i<=count($OBLIGATORY_SHORT["indicators"]);$i++){?>
								<th colspan="2" class="th_horizontal"><?=$i?></th>
							<?}?>
						</tr>
						<tr>
							<?for($i=1;$i<=count($OBLIGATORY_SHORT["indicators"]);$i++){?>
								<th class="th_horizontal">���.</th>
								<th class="th_horizontal">�����</th>
							<?}?>
						</tr>
						<?foreach($OBLIGATORY_SHORT["faculty_name"] as $f_id => $faculty) {?>
							<tr>
								<td colspan="27" align="center"><strong><?=$faculty?></strong></td>
							</tr>
							<?foreach($OBLIGATORY_SHORT["department_name"][$f_id] as $d_id => $department) {?>
								<tr>
									<td colspan="27"><strong><?=$department?><strong></td>
								</tr>
								<?foreach($OBLIGATORY_SHORT["pay_info"][$f_id][$d_id] as $user_id => $contract) {?>
										<tr>
											<td><?=$contract["username"]?></td>
											<td><?=$contract["post"][0] ? $contract["post"][0] : "-"?></td>
											<td><?=$contract["post"][1] ? $contract["post"][1] : "-"?></td>
											<td><?if(empty($contract["rank"])) {?>-<?} else {?><?foreach($contract["rank"] as $i=>$degree){?> <?=CF::lowCaseOne($degree)?><?=($i<count($contract["rank"])-1) ? "," : ""?> <?}?><?}?></td>
											<td><?if(empty($contract["degree"])) {?>-<?} else {?><?foreach($contract["degree"] as $i=>$degree){?> <?=CF::lowCaseOne($degree)?><?=($i<count($contract["degree"])-1) ? "," : ""?> <?}?><?}?></td>
											<td><?=$contract["pps"] == 1 ? "�������" : ($contract["pps"] == 2 ? "������� ����������������" : "�� ������� ������")?></td>
											<td><?=str_replace(".", ",", $contract["rate"]); $rtotal += $contract["rate"];?></td>
											<?foreach($OBLIGATORY_SHORT["indicators"] as $i=>$indicator) {?>
												<td class="two_col"><?=$OBLIGATORY_SHORT["pay"][$f_id][$d_id][$user_id][$i]["val"] ? $OBLIGATORY_SHORT["pay"][$f_id][$d_id][$user_id][$i]["val"] : 0?></td>
												<td class="two_col"><?=$OBLIGATORY_SHORT["pay"][$f_id][$d_id][$user_id][$i]["cond"] ? $OBLIGATORY_SHORT["pay"][$f_id][$d_id][$user_id][$i]["cond"] : 0?></td>
												<? $ctotal[$i] += $OBLIGATORY_SHORT["pay"][$f_id][$d_id][$user_id][$i]["cond"]?>
												<? $ptotal[$i] += $OBLIGATORY_SHORT["pay"][$f_id][$d_id][$user_id][$i]["val"]?>
												<? $row_ptotal += $OBLIGATORY_SHORT["pay"][$f_id][$d_id][$user_id][$i]["val"]?>
											<?}?>
											
										</tr>
								<?}?>	
							<?}?>		
						<?}?>	
				</table>
					<?if(!$MODULE_OUTPUT["html"]) {?>
						<br />
							<a href="/office/effect/reports/download/<?=$_POST["period_id"]?>/<?=$_POST["period_id"]?>/os">������� � ������� MS Excel</a>	
							<a href="/office/effect/reports/print/<?=$_POST["period_id"]?>/<?=$_POST["period_id"]?>/os">������</a>	
						<br />
					<?}?>
			<?}?>
				<br /><br />
				
				<?$OBLIGATORY_SHORT = $MODULE_OUTPUT["obligatory_short_faculty"];?>
				<?if(!empty($OBLIGATORY_SHORT["pay"]) && $MODULE_OUTPUT["smode"]!="os") {?>
					<h2>���������� ������������ ����������� �� �����������</h2>
					<table class="effect_report_table" style="font: 8px/10px 'Tahoma', Arial, sans-serif; ">
							<tr>
								<th rowspan="3">���</th>
								<th colspan="2">���������</th>
								<th rowspan="3" class="th_vertical">������ ������</th>
								<th rowspan="3" class="th_vertical">������ �������</th>
								<th rowspan="3">��������� ���</th>
								<th rowspan="3"><span  class="div_vertical" >���������� ������ �������� �������� ����������</span></th>
								<?foreach($OBLIGATORY_SHORT["indicators"] as $i=>$indicator) {?>
									<th colspan="2" class="th_vertical"><div class="div_vertical"><?=$indicator["name"]?></div></th>
								<?}?>
							</tr>
							<tr>
								<th rowspan="2">����������������</th>
								<th rowspan="2">�����������������</th>
								<?for($i=1;$i<=count($OBLIGATORY_SHORT["indicators"]);$i++){?>
									<th colspan="2" class="th_horizontal"><?=$i?></th>
								<?}?>
							</tr>
							<tr>
								<?for($i=1;$i<=count($OBLIGATORY_SHORT["indicators"]);$i++){?>
									<th class="th_horizontal">���.</th>
									<th class="th_horizontal">�����</th>
								<?}?>
							</tr>
							<?foreach($OBLIGATORY_SHORT["faculty_name"] as $f_id => $faculty) {?>
								<tr>
									<td colspan="41" align="center"><strong><?=$faculty?></strong></td>
								</tr>
								<?foreach($OBLIGATORY_SHORT["department_name"][$f_id] as $d_id => $department) {?>
									<?if(!empty($OBLIGATORY_SHORT["pay_info"][$f_id][$d_id])){?>
										<?foreach($OBLIGATORY_SHORT["pay_info"][$f_id][$d_id] as $user_id => $contract) {?>
												<tr>
													<td><?=$contract["username"]?></td>
													<td><?=$contract["post"][0] ? $contract["post"][0] : "-"?></td>
													<td><?=$contract["post"][1] ? $contract["post"][1] : "-"?></td>
													<td><?if(empty($contract["rank"])) {?>-<?} else {?><?foreach($contract["rank"] as $i=>$degree){?> <?=CF::lowCaseOne($degree)?><?=($i<count($contract["rank"])-1) ? "," : ""?> <?}?><?}?></td>
													<td><?if(empty($contract["degree"])) {?>-<?} else {?><?foreach($contract["degree"] as $i=>$degree){?> <?=CF::lowCaseOne($degree)?><?=($i<count($contract["degree"])-1) ? "," : ""?> <?}?><?}?></td>
													<td><?=$contract["pps"] == 1 ? "�������" : ($contract["pps"] == 2 ? "������� ����������������" : "�� ������� ������")?></td>
													<td><?=str_replace(".", ",", $contract["rate"]); $rtotal += $contract["rate"];?></td>
													<?foreach($OBLIGATORY_SHORT["indicators"] as $i=>$indicator) {?>
														<td class="two_col"><?=$OBLIGATORY_SHORT["pay"][$f_id][$d_id][$user_id][$i]["val"] ? $OBLIGATORY_SHORT["pay"][$f_id][$d_id][$user_id][$i]["val"] : 0?></td>
														<td class="two_col"><?=$OBLIGATORY_SHORT["pay"][$f_id][$d_id][$user_id][$i]["cond"] ? $OBLIGATORY_SHORT["pay"][$f_id][$d_id][$user_id][$i]["cond"] : 0?></td>
														<? $ctotal[$i] += $OBLIGATORY_SHORT["pay"][$f_id][$d_id][$user_id][$i]["cond"]?>
														<? $ptotal[$i] += $OBLIGATORY_SHORT["pay"][$f_id][$d_id][$user_id][$i]["val"]?>
														<? $row_ptotal += $OBLIGATORY_SHORT["pay"][$f_id][$d_id][$user_id][$i]["val"]?>
													<?}?>
													
												</tr>
										<?}?>	
									<?}?>	
								<?}?>		
							<?}?>	
					</table>
					<?if(!$MODULE_OUTPUT["html"]) {?>
						<br />
							<a href="/office/effect/reports/download/<?=$_POST["period_id"]?>/<?=$_POST["period_id"]?>/osf">������� � ������� MS Excel</a>
							<a href="/office/effect/reports/print/<?=$_POST["period_id"]?>/<?=$_POST["period_id"]?>/osf">������</a>	
						<br />
					<?}?>
				<?}?>
				
	<?if($MODULE_OUTPUT["html"]) {?>	
		</body>
		</html>
	<?}?>		
	
		<?} 
		break;?>
	<?case "reports": {
		$STIMUL = $MODULE_OUTPUT["stimulations"];
		$OBLIGATORY = $MODULE_OUTPUT["obligatory"];
		
		if($MODULE_OUTPUT["html"]) {?>
		<html>
		<head title="report">
			<style type="text/css">
				table.effect_report_table {
					border-collapse: collapse;
					border: 1px #000 solid;
					vertical-align: middle;
					font-weight: normal;
					text-align: left;
					width: 100%;
					/*margin: 0 auto;*/
				}
				table.effect_report_table thead,table.effect_report_table thead tr,table.effect_report_table thead td,table.effect_report_table thead th {
					width: 50px;
					text-align: center;
				}
				table.effect_report_table thead a {
				}
				table.effect_report_table tbody th {
					text-align: center;
				}
				table.effect_report_table th,table.effect_report_table td {
					border: 1px #000 solid;
					font-weight: normal;
				}
				table.effect_report_table .th_horizontal {
					
				}
				table.effect_report_table .div_vertical {

					/*-webkit-transform: rotate(270deg); transform: rotate(270deg);*/
				}
				table.effect_report_table .th_vertical {
					border-collapse: collapse;
					width: 50px;
				}

				table.effect_report_table .two_col {
					width: 25px;
				}
			</style>
		</head>
		<body>
	<?} else {		?>
	<h2>��������������� ������ ������ �������������� ���������</h2>
	<form action="" method="POST">
		<select name="department_id" class="departments">
				<option value=""></option>
			<?if($Engine->OperationAllowed(34, "contracts.fulldepartment.report", -1, $Auth->usergroup_id)){?><option value="all">��� �������������</option><?}?>
			<?foreach($MODULE_OUTPUT["departments"] as $dep_id => $dep_name) {?>
				<option <?=$STIMUL["department_id"]==$dep_id ? "selected" : ""?> value="<?=$dep_id?>"><?=$dep_name?></option>
			<?}?>
		</select><input type="text" class="search_dep" placeholder="�����"><br /> 
		������:
		<select name="period_id">
			<?foreach($MODULE_OUTPUT["periods"] as $pid => $period) {?>
				<option value="<?=$pid?>"><?=$period["date_from"]?> - <?=$period["date_to"]?></option>
			<?}?>
		</select><br />
		<input type="submit" value="��������"><br />
	</form>
	<?}?>
		<?foreach($OBLIGATORY["pay_info"] as $f_id=>$faculty) {?>
			<?if($MODULE_OUTPUT["all"]){?><h2><?=$OBLIGATORY["faculty_name"][$f_id]?></h2><?}?>
			<?foreach($faculty as $d_id=>$department) {?>
				<h2>���������� ������������ ����������� ��� ������� <?=str_replace("�������", "", $OBLIGATORY["department_name"][$f_id][$d_id])?></h2>
				<h2>c <?=$OBLIGATORY["date_from"]?> �� <?=$OBLIGATORY["date_to"]?></h2>
				<table class="effect_report_table" style="font: 8px/10px 'Tahoma', Arial, sans-serif; ">

						<tr>
							<th rowspan="3">���</th>
							<th colspan="2">���������</th>
							<th rowspan="3" class="th_vertical">������ ������</th>
							<th rowspan="3" class="th_vertical">������ �������</th>
							<th rowspan="3">��������� ���</th>
							<th rowspan="3"><span  class="div_vertical" >���������� ������ �������� �������� ����������</span></th>
							<?foreach($OBLIGATORY["indicators"] as $i=>$indicator) {?>
								<th colspan="1" class="th_vertical"><div class="div_vertical"><?=$indicator["name"]?></div></th>
							<?}?>
						</tr>
						<tr>
							<th rowspan="2">����������������</th>
							<th rowspan="2">�����������������</th>
							<?for($i=1;$i<=count($OBLIGATORY["indicators"]);$i++){?>
								<th colspan="1" class="th_horizontal"><?=$i?></th>
							<?}?>
						</tr>
						<tr>
							<?for($i=1;$i<=count($OBLIGATORY["indicators"]);$i++){?>
								<th class="th_horizontal">���.</th>
							<?}?>
						</tr>
						<?if(!empty($department)) {?>		
							<?foreach($department as $user_id => $contract) {?>
								<?if(!empty($contract["username"])) {?>
									<tr>
										<td><?=$contract["username"]?></td>
										<td><?=$contract["post"][0]["name"] ? $contract["post"][0]["name"] : "-"?></td>
										<td><?=$contract["post"][1]["name"] ? $contract["post"][1]["name"] : "-"?></td>
										<td><?if(empty($contract["rank"])) {?>-<?} else {?><?foreach($contract["rank"] as $i=>$degree){?> <?=CF::lowCaseOne($degree)?><?=($i<count($contract["rank"])-1) ? "," : ""?> <?}?><?}?></td>
										<td><?if(empty($contract["degree"])) {?>-<?} else {?><?foreach($contract["degree"] as $i=>$degree){?> <?=CF::lowCaseOne($degree)?><?=($i<count($contract["degree"])-1) ? "," : ""?> <?}?><?}?></td>
										<td><?=$contract["pps"] == 1 ? "�������" : ($contract["pps"] == 2 ? "������� ����������������" : "�� ������� ������")?></td>
										<td><?=str_replace(".", ",", $contract["rate"]);$rtotal += $contract["rate"];?>
										
										</td>
										<?foreach($OBLIGATORY["indicators"] as $i=>$indicator) {?>
											<td class="two_col"><?=$OBLIGATORY["pay"][$f_id][$d_id][$user_id][$i]["val"] ? $OBLIGATORY["pay"][$f_id][$d_id][$user_id][$i]["val"] : 0?></td>
										<?}?>
										
									</tr>
								<?}?>
							<?}?>	
						<?}?>						
						<tr>
							<td colspan="6">�����:</td>
							<td><?=str_replace(".", ",", $rtotal)?></td>
							<?foreach($OBLIGATORY["indicators"] as $i=>$indicator) {?>
								<td class="two_col"><?=$OBLIGATORY["total"][$f_id][$d_id][$i]["val"] ? $OBLIGATORY["total"][$f_id][$d_id][$i]["val"] : 0?></td>
							<?}?>
						</tr>
						
				</table>
			<?$ctotal=$row_ptotal=$super_total=$ptotal=$rtotal=null;} ?>
			
			<?if(!empty($STIMUL["pay_info"]) && !$MODULE_OUTPUT["all"]) {?>
				<br />
					<a href="print/<?=$STIMUL["department_id"]?>/<?=$STIMUL["period_id"]?>/o">������</a>	
					<a href="download/<?=$STIMUL["department_id"]?>/<?=$STIMUL["period_id"]?>/o">������� � ������� MS Excel</a>	
				<br />
			<?}?>
			
		<?}?>
		

		<?foreach($STIMUL["pay_info"] as $f_id=>$faculty) {?>
			<?if($MODULE_OUTPUT["all"]){?><h2><?=$STIMUL["faculty_name"][$f_id]?></h2><?}?>
			<?foreach($faculty as $d_id=>$department) {?>
				<h2>������������ � ���������� ��������� �������� ��� ������� <?=str_replace("�������", "", $STIMUL["department_name"][$f_id][$d_id])?></h2>
				<h2>c <?=$STIMUL["date_from"]?> �� <?=$STIMUL["date_to"]?></h2>
				<table class="effect_report_table" style="font: 8px/10px 'Tahoma', Arial, sans-serif; ">

						<tr>
							<th rowspan="3">���</th>
							<th colspan="2">���������</th>
							<th rowspan="3" class="th_vertical">������ ������</th>
							<th rowspan="3" class="th_vertical">������ �������</th>
							<th rowspan="3">��������� ���</th>
							<th rowspan="3"><span  class="div_vertical" >���������� ������ �������� �������� ����������</span></th>
							<?foreach($STIMUL["indicators"] as $i=>$indicator) {?>
								<th colspan="2" class="th_vertical"><div class="div_vertical"><?=$indicator["name"]?></div></th>
							<?}?>
							<th rowspan="3">�����:</th>
						</tr>
						<tr>
							<th rowspan="2">����������������</th>
							<th rowspan="2">�����������������</th>
							<?for($i=1;$i<=count($STIMUL["indicators"]);$i++){?>
								<th colspan="2" class="th_horizontal"><?=$i?></th>
							<?}?>
						</tr>
						<tr>
							<?for($i=1;$i<=count($STIMUL["indicators"]);$i++){?>
								<th class="th_horizontal">��.</th>
								<th class="th_horizontal">���.</th>
							<?}?>
						</tr>
						<?if(!empty($department)) {?>		
							<?foreach($department as $user_id => $contract) {?>
									<tr>
										<td><?=$contract["username"]?></td>
										<td><?=$contract["post"][0] ? $contract["post"][0] : "-"?></td>
										<td><?=$contract["post"][1] ? $contract["post"][1] : "-"?></td>
										<td><?if(empty($contract["rank"])) {?>-<?} else {?><?foreach($contract["rank"] as $i=>$degree){?> <?=CF::lowCaseOne($degree)?><?=($i<count($contract["rank"])-1) ? "," : ""?> <?}?><?}?></td>
										<td><?if(empty($contract["degree"])) {?>-<?} else {?><?foreach($contract["degree"] as $i=>$degree){?> <?=CF::lowCaseOne($degree)?><?=($i<count($contract["degree"])-1) ? "," : ""?> <?}?><?}?></td>
										<td><?=$contract["pps"] == 1 ? "�������" : ($contract["pps"] == 2 ? "������� ����������������" : "�� ������� ������")?></td>
										<td><?=str_replace(".", ",", $contract["rate"]);$rtotal += $contract["rate"];?></td>
										<?foreach($STIMUL["indicators"] as $i=>$indicator) {?>
											<td class="two_col"><?=$STIMUL["pay"][$f_id][$d_id][$user_id][$i]["count"] ? $STIMUL["pay"][$f_id][$d_id][$user_id][$i]["count"] : 0?></td>
											<td class="two_col"><?=$STIMUL["pay"][$f_id][$d_id][$user_id][$i]["payments"] ? $STIMUL["pay"][$f_id][$d_id][$user_id][$i]["payments"] : 0?></td>
											<? $ctotal[$i] += $STIMUL["pay"][$f_id][$d_id][$user_id][$i]["count"]?>
											<? $ptotal[$i] += $STIMUL["pay"][$f_id][$d_id][$user_id][$i]["payments"]?>
											<? $row_ptotal += $STIMUL["pay"][$f_id][$d_id][$user_id][$i]["payments"]?>
										<?}?>
										<td><?=$row_ptotal; $super_total += $row_ptotal; unset($row_ptotal);?></td>
									</tr>
							<?}?>	
						<?}?>						
						<tr>
							<td colspan="6">�����:</td>
							<td><?=str_replace(".", ",", $rtotal)?></td>
							<?foreach($STIMUL["indicators"] as $i=>$indicator) {?>
								<td class="two_col"><?=$ctotal[$i]?></td>
								<td class="two_col"><?=$ptotal[$i]?></td>
							<?}?>
							<td><?=$super_total?></td>
						</tr>
						
				</table>
			<?$ctotal=$row_ptotal=$super_total=$ptotal=$rtotal=null;} ?>
		<?}?>
	

		<?if(!empty($STIMUL["pay_info"]) && !$MODULE_OUTPUT["all"] && !$MODULE_OUTPUT["html"]) {?>
			<br />
				<a href="print/<?=$STIMUL["department_id"]?>/<?=$STIMUL["period_id"]?>/s">������</a>	
				<a href="download/<?=$STIMUL["department_id"]?>/<?=$STIMUL["period_id"]?>/s">������� � ������� MS Excel</a>	
			<br />
			<br />
		<?}?>
		<?if($MODULE_OUTPUT["html"] && !$MODULE_OUTPUT["all"]) {?>	
		<p>����� ���������� (�������� ���������): __________________________________</p>
		<p>���������� �������: __________________________________</p>
		</body>
		</html>
	<?}?>	
	<?//CF::Debug($OBLIGATORY["total"]);?>


			

			
		
	

	
	<?} break;?>
<?}?>
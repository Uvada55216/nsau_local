<?php
global $Engine, $Auth, $EE;

switch ($MODULE_OUTPUT["mode"])	{
	case "_block_files": {?>
		<?foreach ($_input as $id => $val) {?>
			<a data-id="<?=$val->id?>" data-type="file" class="list-group-item list-group-item-secondary">							
				<div class="input-group">
					<div class="input-group-btn">
						<button type="button" class="btn btn-default" data-object="file" data-action="download" title="<?=$val->name?>.<?=$val->filename?>"><?=substr($val->name, 0, 70)?>....<?=$val->filename?> (��������: <?=$val->down_count?>)</button>
					</div>

					<input type="text" class="form-control" value="<?=$val->link?>" readonly>
				  	<div class="input-group-btn">
				  		<?if($val->sig_id) {?>
                            <button type="button" class="btn btn-default" data-object="file" data-action="download_sig" aria-label="Left Align" title="���">
                                    <span class="sig" aria-hidden="true"></span>
                            </button>
						<?}?>
						<button type="button" class="btn btn-default" data-object="file" data-action="copy" aria-label="Left Align" title="���������� ����� ������">
								<span class="glyphicon glyphicon-copy" aria-hidden="true"></span>
						</button>
						<button type="button" class="btn btn-default" data-object="file" data-type="modal" data-action="edit" aria-label="Left Align" title="�������������">
								<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
						</button>
						<button type="button" class="btn btn-default" data-object="file" data-action="delete" aria-label="Left Align" title="�������">
								<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
						</button>
						<button type="button" class="btn btn-default" data-object="file" data-type="modal" data-action="attachments"  aria-label="Left Align" title="���������� ��������">
								<span class="glyphicon glyphicon-paperclip <?=$val->has_decine_attachment ? 'red' : ''?>" aria-hidden="true"></span>
						</button>
						<?if($val->comment){?>
							<button type="button" class="btn btn-default" data-object="file" data-type="modal" data-action="comments"  aria-label="Left Align" title="����������� � �����">
									<span class="glyphicon glyphicon-comment" aria-hidden="true"></span>
							</button>	
						<?}?>
				  </div>
				</div>			
			</a>
		<?}?>
	<?}break;
	case "_block_deleted_files": {?>
		<?foreach ($_input as $id => $val) {?>
			<a data-id="<?=$val->id?>" data-type="file" class="list-group-item list-group-item-secondary">								
				<div class='row'>
					<div class="col-md-5"><strong><?=$val->name?>.<?=$val->filename?></strong></div>
					<div class="col-md-5">������: <?=$val->delete_time?></div>
					<div class="col-md-2 text-right">
					  	<div class="input-group-btn"> 									
							<button type="button" class="btn btn-default btn-sm" data-object="file" data-action="repair" aria-label="Left Align" title="������������">
									<span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span>
							</button>
					  	</div>
					</div>
			  	</div>			
			</a>
		<?}?>
	<?}break;
	case "_block_attachments": {?>
		<?foreach ($MODULE_OUTPUT["attachments"] as $key => $val) {?>
			<div class="panel panel-default main-form">
				<div class="panel-heading">
					<div class="row">
						<div class="col-md-8"><?=$val->subject_name?></div>
						<div class="col-md-2"><span class="label label-<?=$val->status_style?>"><?=$val->status?></span></div>
						<div class="col-md-2">
							<div class="btn-group  btn-group-xs" role="group" aria-label="...">
								<?if($MODULE_OUTPUT["moderator"]){//�������� ����� �� ���������?>	
									<button type="button" class="btn btn-default" data-object="attachment" data-id="<?=$val->id?>" data-action="moderate" aria-label="Left Align" title="������������ ��������">
										<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
									</button>
								<?}?>							
								<button type="button" class="btn btn-default" data-object="attachment" data-id="<?=$val->id?>" data-action="delete" aria-label="Left Align" title="�������">
									<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
								</button>								
							</div>
					  	</div>
					</div>
				</div>
				<div class="panel-body">
					<div class="moderate-form panel panel-default" data-id="<?=$val->id?>" style="display: none;">
						<div class="panel-heading">��������� ��������</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-4">
									<input name="moderateStatus" class="moderate-status" type="radio" value="accept" <?=$val->approved == 1 ? "checked" : ""?>> ���������
								</div>
								<div class="col-md-8">
									<input name="moderateStatus" class="moderate-status" type="radio" value="decine" <?=$val->approved == -1 ? "checked" : ""?>> ���������
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<p>�����������</p>
									<textarea class="attachment-moderate-comment"><?=$val->comment?></textarea>
								</div>
							</div>	
							<div class="row">
								<div class="col-md-7"></div>
							  	<div class="col-md-2">
									<div class="input-group-btn">
										<button type="button" class="btn btn-default" data-object="attachment" data-action="cancel-moderation">������</button>
									</div>
							  	</div>
							  	<div class="col-md-3 text-right">
									<div class="input-group-btn ">
										<button type="button" class="btn btn-default" data-object="attachment" data-action="save-moderation">���������</button>
									</div>
							  	</div>
							</div>	
						</div>						
					</div>


					<div class="row">
					  <div class="col-md-4"><?=$val->spec_code?></div>
					  <div class="col-md-8"><?=$val->speciality?></div>
					</div>
					<div class="row">
					  <div class="col-md-4">���</div>
					  <div class="col-md-8"><?=$val->attach_type?></div>
					</div>
					<div class="row">
					  <div class="col-md-4">����� ��������: <?=$val->education?></div>
					  <div class="col-md-8">�������: <?=$val->semester ? $val->semester : "-"?></div>
					</div>
					<div class="row">
					  <div class="col-md-4">�������</div>
					  <div class="col-md-8"><?=$val->profile ? $val->profile : '-'?></div>
					</div>
					<br />
					<?if($val->comment) {?>
						<div class="row">
							<div class="col-md-12">
								<div class="alert alert-<?=$val->status_style?>" role="alert"><?=$val->comment?></div>
							</div>
						</div>
					<?}?>
				</div>
			</div>
		<?}?>
	<?}
	break;

	case "_block_subjects_list": {?>
		<?foreach ($_input as $id => $val) {?>
			<a href="#" data-id="<?=$id?>" data-type="subject" class="list-group-item list-group-item-action list-group-item-warning">
				<?=$val['name']?> <small>(<?=$val['department_name']?>)</small>
			</a>
			<div class="folder_content" style="display: none" data-id="<?=$val['id']?>" data-type="subject"></div>				
		<?}?>
	<?}break;

	case "_block_folders_list": {?>
		<?foreach ($_input as $id => $val) {?>
			<a href="#" data-id="<?=$id?>" data-type="folder" class="list-group-item list-group-item-action list-group-item-warning folders_list"><?=$val['name']?></a>
			<div class="folder_content" style="display: none" data-id="<?=$val['id']?>" data-type="folder"></div>
		<?}?>
	<?}break;

	case "_block_upload_form": {?>
		<div class="panel-body">
			<div class="input-group">
			  	<span title='����' class="input-group-addon without-bottom-border nsau-files-form">
			  		<span data-tooltip='file'>�������� ����</span>
			  		<span class="obligatorily"> *</span>
			  	</span>  
			  	<input type="file" name="file" class="form-control without-bottom-border file">
			</div>				

			<div class="input-group">
			  <span title='������ ��������' class="input-group-addon without-bottom-border nsau-files-form">
			  	<span data-tooltip='description'>������ ��������</span>
			  	<span class="obligatorily"> *</span>
			  </span>  
			  <input type="text"  placeholder="������������ ����������: ��� �������" class="form-control without-bottom-border description">
			</div>

			<div class="input-group">
			  	<span title='����������� �������� �������' class="input-group-addon without-bottom-border nsau-files-form">
			  		<span data-tooltip='file_sig'>���</span>
			  	</span>
			  	<div class="col-md-6" style="padding: 0px;">
			  	    <input type="file" name="file_sig" class="form-control without-bottom-border file">
			  	</div>
			  	<div class="col-md-6" style="border-top: 1px solid #ccc;border-right: 1px solid #ccc;">
                    <label class="checkbox" style="margin: 7px 20px;font-weight: inherit;">
                        <input type="checkbox" id="with_sig" class='' <?=$MODULE_OUTPUT['form']['with_sig'] ? 'checked' : ''?>> ����������</label>
				</div>
			</div>

			<div class="input-group">
			  <span title='�����' class="input-group-addon without-bottom-border nsau-files-form">
			  	<span data-tooltip='author'>������</span>
			  </span>  
			  <input type="text" class="form-control without-bottom-border author">
			</div>

			<div class="input-group">
			  <span title='�����' class="input-group-addon without-bottom-border nsau-files-form">
			  	<span data-tooltip='pages'>�����</span>
			  </span>  
			  <input type="text" class="form-control without-bottom-border pages">
			</div>

<!-- 			<div class="input-group">
			  <span title='�����' class="input-group-addon without-bottom-border nsau-files-form">
			  	<span data-tooltip='edition'>�����</span>
			  </span>  
			  <input type="text" class="form-control without-bottom-border edition">
			</div> -->

			<div class="input-group">
			  <span title='����� �������' class="input-group-addon without-bottom-border nsau-files-form">
			  	<span data-tooltip='place'>����� �������</span>
			  </span>
			  <input type="text"  placeholder="��������: �����������, ����" class="form-control without-bottom-border place" list="cityname">
			  <datalist id="cityname">
				  <option value="�����������, ����">
				  <option value="������, �����">
			  </datalist>
			</div>

			<div class="input-group">
			 	<span class="input-group-addon without-bottom-border nsau-files-form">
			  		<span data-tooltip='year'>��� �������</span>
				</span>  
			  	<select class="form-control post without-bottom-border year">
			  		<option value></option>
				  	<?for ($i = 2007; $i <= date('Y'); $i++) {?>
				  		<option value="<?=$i?>"><?=$i?></option>
				  	<?}?>							  	
			  	</select>
			</div>

			<div class="input-group">
			 	<span class="input-group-addon without-bottom-border nsau-files-form">
			  		<span data-tooltip='folder_id'>�����</span>
				</span>  
			  	<select class="form-control post without-bottom-border folder_id">
			  		<option value></option>
			  		<?foreach($MODULE_OUTPUT['form']['folders'] as $f_id => $f_name) {?>
				  		<option value="<?=$f_id?>"><?=$f_name?></option>	
				  	<?}?>				  							  	
			  	</select>
			</div>


			<div class="input-group">
			  <span title='qwe' class="input-group-addon without-bottom-border nsau-files-form">
			  	<span data-tooltip='send_to_people'>�������� �������������</span>
			  </span>  
			  <input type="text" class="form-control without-bottom-border search_people_with_user_id" placeholder="�����">
			  <select class="form-control post without-bottom-border send_to_people">
			  	<option value=""></option>
			  </select>
			</div>
			<div class="input-group">
			 	<span class="input-group-addon nsau-files-form without-bottom-border">
			  		<span data-tooltip='sub_authors'>��������</span>
				</span>				
				<input type="text" class="form-control without-bottom-border search_people" placeholder="�����">  
				<select class="form-control post without-bottom-border select_sub_autor">
				  	<option value></option>					  							  	
			  	</select> 					
			</div>
			<div class="list-group sub_autors_list">
			  	<a href="#" class="list-group-item list-group-item-nsau-grey-button" data-object="sub_autor" data-action="add" style="border-radius: 0px; margin-right: 1px;">
			    	�������� ��������
			  	</a>
			  	<span class="list-group-item tmp hidden">
			  		<div class="row">
			  			<div class="col-md-11">
							<span class='name' data-id=''></span>
			  			</div>
			  			<div class="col-md-1">
							<button type="button" class="btn btn-xs btn-default" data-object="sub_autor" data-action="delete" aria-label="Left Align" title="�������">
									<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
							</button>
			  			</div>
			  		</div>				  			
			  	</span>
		  	</div>	
			<div class="row" style="margin-top: -15px;">
				<div class="col-md-8">
					<input type="checkbox" class='allow_download'> ��������� ���������� ���� �������������<br>
					<input type="checkbox" class='zip_file'> ��������� ��� ������-��������� (���� ������ ���� � ������� zip)
				</div>
			  	<div class="col-md-2">
					<div class="input-group-btn ">
						<button type="button" class="btn btn-default file-form-upload-button" data-object="file" data-action="upload_file">���������</button>
					</div>
			  	</div>
			  	<div class="col-md-2">
					<div class="input-group-btn">
						<button type="button" class="btn btn-default file-form-cancel-button" data-object="file" data-action="cancel">������</button>
					</div>
			  	</div>
			</div>	

			<div class="progress">
	  			<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
			</div>
		</div>
	<?}
	break;

	case "_block_files_list":{?>
		<div class="tab-pane fade active in" id="subjects" role="tabpanel" aria-labelledby="home-tab">
			<div class="list-group">
				<div class="subjects_list_content">
					<?\helpers\TplHelper::beginCase('EFiles2.0', '_block_subjects_list', $MODULE_OUTPUT['files']['subject']);?>
				</div>
				<div class="other_files">
					<?\helpers\TplHelper::beginCase('EFiles2.0', '_block_files', $MODULE_OUTPUT['files']['others']);?>	
				</div>		  
			</div>
		</div>
	  	<div class="tab-pane fade" id="folders" role="tabpanel" aria-labelledby="profile-tab">
			<div class="list-group">
				<div class="folders_list_content">
					<?\helpers\TplHelper::beginCase('EFiles2.0', '_block_folders_list', $MODULE_OUTPUT['files']['folders']);?>
				</div>
				<div class="other_files">
					<?\helpers\TplHelper::beginCase('EFiles2.0', '_block_files', $MODULE_OUTPUT['files']['others']);?>
				</div>				
			</div>
	  	</div>
		<div class="tab-pane fade" id="works" role="tabpanel" aria-labelledby="contact-tab">
			<div class="list-group">
				<?\helpers\TplHelper::beginCase('EFiles2.0', '_block_files', $MODULE_OUTPUT['files']['works']);?>
			</div>
	  	</div>
	  	<div class="tab-pane fade" id="deleted" role="tabpanel" aria-labelledby="contact-tab">
			<div class="list-group">
				<?\helpers\TplHelper::beginCase('EFiles2.0', '_block_deleted_files', $MODULE_OUTPUT['files']['deleted']);?>  
			</div>
	  	</div>
	<?}break;
}
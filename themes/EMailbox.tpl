<?php
// version 2.3  2012/09/11

$MODULE_MESSAGES = array(
	301 => "������ ��������� ����� �������",
	302 => "�� ������ ������",
	303 => "�� ������� ��� ���������",
	304 => "�� ������� �����",
	305 => "�� ������� �����",
	306 => "�� ������ �����",
	307 => "�� ������ �������� ����",
	308 => "������ ���������� �������",
	309 => "������ email ������ � ������� ������������.",
	310 => "����������� ������ ����� ����������� �����.",
	311 => "����� ������ ���� �� ����� ������� (@nsau.edu.ru, @nripk.ru).",
	312 => "�������� ���� �������.",
	313 => "������ �� ���������.",
	314 => "������ ������ ��������� �� 6 �� 16 �������� ���������� �������� (������� ����� ��������) �/��� ���� 0-9.",
	315 => "������ �������."
	);

foreach ($MODULE_DATA["output"]["messages"]["good"] as $data) {
	echo "<p class=\"message\">$data</p>\n";
}

/*foreach ($MODULE_DATA["output"]["messages"]["bad"] as $data)
{
	echo "<p class=\"message red\">$data</p>\n";
}*/

if (isset($MODULE_DATA["output"]["data"])) {
	$data = $MODULE_DATA["output"]["data"];
} else {
	$data = "";
}
	//echo isset($data["username"]) ? $data["username"] : "11111";

$array = array();

foreach ($MODULE_OUTPUT["messages"]["bad"] as $elem) {
	if (isset($MODULE_MESSAGES[$elem])) {
		$array[] = $MODULE_MESSAGES[$elem];
	}
}

if ($array) { ?>
	<ul>
<?php
	foreach ($array as $elem) { ?>
		<li><?=$elem?></li>
<?php
	} ?>
	</ul>
<?php
}

if (isset($MODULE_OUTPUT["mode"])) {
	switch ($MODULE_OUTPUT["mode"]) {
		case "request": {
			if ($_SESSION["user_id"]) { ?>
	<br /><br />���� �� �������� ����� � ������ ��� ������ ��������� �����, �� ������� �� � ������� "��� ���������".<br />� ��������� ������ ���������� � ���������� �������������� ������ �������������.
<?php		}
		}
		break;
	
		case "hidden_form": {?>
			<?header("Content-type: text/html; charset=windows-1251");?>
			<div class="ch_pass_email">
				<a class="change_email_password">�������� ������ �� ��������� �����</a>
				<?$ch_email = $MODULE_OUTPUT["another_domen"];?>
				<div class="change_email_password hidden">
					<?if($ch_email) {?>
						<p>� ������ �������� �������� �������� ���� - <strong><?=$MODULE_OUTPUT["email"]?></strong>.</p> 
						<p>������ ����� ������� ������ �� ����� �� ����� �������(@nsau.edu.ru, @nripk.ru).</p>
						<p>��� ���� ����� ������� ������ �� ����� ���������� �������� �������� ���� ����������� � ��������</p>
						<input type="text" id="email" value="<?=$MODULE_OUTPUT["email"]?>" />
						<p><input type="button" id="change_email" value="���������" /></p>
					<?} else {?>
						<p><strong><?=$MODULE_OUTPUT["email"]?></strong></p>
						<p>������� ����� ������</p>
						<input type="text" placeholder="����� ������" id="pwd1" />
						<p>��������� ����� ������</p>
						<input type="text" placeholder="��������� ����� ������" id="pwd2" />
						<p><input type="button" class="change_password" value="���������" /></p>
					<?}?>
				</div>
				<?if(!empty($MODULE_OUTPUT["ok_msg"])) {?>
					<div id="ok_msg"><p style="color: #049708"><?=$MODULE_MESSAGES[$MODULE_OUTPUT["ok_msg"]]?></p></div>
				<?}?>
				<?if(!empty($MODULE_OUTPUT["err_msg"])) {?>
					<div id="err_msg"><p style="color: #CC0100"><?=$MODULE_MESSAGES[$MODULE_OUTPUT["err_msg"]]?></p></div>
				<?}?>
			</div>			
		<?}
		break;
	
		case "save_post":
		case "edit_post": { ?>
	<script src="/scripts/mailbox.js"></script>
	<form action="<?=$EE["unqueried_uri"]?>" method="post">
		<p>
			<label class="block wide"><strong>�������� ����:</strong></label>
			<input type="text" id="username" name="<?=$NODE_ID?>[<?=$MODULE_DATA["output"]["mode"]?>][username]" value="<?=(isset($data["username"]) ? $data["username"] : "")?>" onblur="if (this.value!='') javascript:process(); else { document.getElementById('exists').innerHTML=''; document.getElementById('exists_short').innerHTML=''; }" />
			<select  onchange="javascript:process();" id="domain" name="<?=$NODE_ID?>[<?=$MODULE_DATA["output"]["mode"]?>][domain]"><option>@nsau.edu.ru</option><option>@nripk.ru</option></select> <span id="exists"></span><span id="exists_short" style="display:none;"></span><span id="username_double" style="display:none;"><?=(isset($data["username"]) ? $data["username"] : "")?></span><br />
		</p>
		<p>
			<label class="block wide"><strong>������:</strong>&nbsp;<span id="genpass"></span></label>
			<span id="copypass"><a onclick="copypass('genpass','add_pass','repeatpass');">������������ ���� ������<span id="instead"></span></a></span><br />
			<input type="password" id= "add_pass" class="input_long" name="<?=$NODE_ID?>[<?=$MODULE_DATA["output"]["mode"]?>][password]" value="<?=(isset($data["password"]) ? $data["password"] : "")?>" />
			<a onclick="passgen('genpass',3,1,1,'add_pass');">������������� ������</a>
<?php		if ($MODULE_DATA["output"]["mode"] == "edit_post") { ?>
				<br /><small>(���� �� ������ ������ - �������� ���� ������)</small>
<?php 		} ?>
		</p>
		<p>
			<label class="block wide"><strong>��������� ������:</strong></label><br />
			<input type="password" id="repeatpass" class="input_long" name="<?=$NODE_ID?>[<?=$MODULE_DATA["output"]["mode"]?>][repeat_password]" value="<?=(isset($data["repeat_password"]) ? $data["repeat_password"] : "")?>" /><br />
<?php 		if ($MODULE_DATA["output"]["mode"] == "edit_post") { ?>
			<small>(���� �� ������ ������ - �������� ���� ������)</small>
<?php 		} ?>
		</p>
		<p>
			<label class="block wide"><strong>��� ���������:</strong></label><br />
			<input type="text" class="input_long" name="<?=$NODE_ID?>[<?=$MODULE_DATA["output"]["mode"]?>][name]" value="<?=(isset($data["name"]) ? $data["name"] : "")?>" />
		</p>
		<p>
			<label class="block wide"><strong>�����:</strong></label>
<?php 		$min_quota = "300";		
			if ($MODULE_DATA["output"]["quota_handle"]) { ?>
			<input type="text" size="4" name="<?=$NODE_ID?>[<?=$MODULE_DATA["output"]["mode"]?>][quota]" value="<?=(isset($data["quota"]) ? $data["quota"] : $min_quota)?>" class="block text" /> ��
		</p>
<?php 		} else { ?>
		<?=(isset($data["quota"]) ? $data["quota"] : $min_quota)?> ��
		<input type="hidden" size="4" name="<?=$NODE_ID?>[<?=$MODULE_DATA["output"]["mode"]?>][quota]" value="<?=(isset($data["quota"]) ? $data["quota"] : $min_quota)?>" />
<?php		} ?>
			<p><input type="checkbox" name="<?=$NODE_ID?>[<?=$MODULE_DATA["output"]["mode"]?>][active]" <? echo isset($data["active"])&&!$data["active"] ? "" : "checked=\"checked\""; ?>> <label class="block wide"><strong>��������</strong></label></p>
			<p><input type="submit" onclick="if (document.getElementById('exists_short').innerHTML == 'occ') { alert('������� ��������� ��� ��������� �����'); return false; }" value="<?php echo $MODULE_DATA["output"]["mode"]=="save_post" ? "������� ����" : "������������� ����"; ?>" /></p>
		</form>
<?php	}
		break;
		case "upload_mails":
			$creation_results = $MODULE_OUTPUT["creation_results"];
			if(!empty($MODULE_OUTPUT["mailboxes"])){
				?>
				<table>
				<tr>
					<th>�������� ����</th>
					<th>��� ���������</th>
					<th>���������</th>
				</tr>
				<?php 
				foreach($MODULE_OUTPUT["mailboxes"] as $mailbox){
				$result = $creation_results[$mailbox["username"]];
					?>
					<tr>
					<td>
						<?=$mailbox["username"]?>
					</td>
					<td>
						<?=$mailbox["name"]?>
					</td>
					<td>
						<span class="message<?php if($result["bad"]){?> red<?php } ?>"><?=$result["bad"]?><?=$result["good"]?></span>
					</td>
					</tr>
					<?php 
				}
				?>
				</table>
				<?php 
			}
		?>
		<div class="message red" id="upload_status"></div>
		<form action="<?=$EE["unqueried_uri"]?>" method="post" enctype="multipart/form-data">
		<p>
			<label class="block_wide">���� � csv-�����:<br></label>
			<input type="file" class="input_long" name="<?=$NODE_ID?>[<?=$MODULE_DATA["output"]["mode"]?>][csv_file]" id="csv_file" /> 
		</p>
		<p>
			<label class="block_wide">�����: </label>
			<select id="domain2" name="<?=$NODE_ID?>[<?=$MODULE_DATA["output"]["mode"]?>][domain]"><option>@nsau.edu.ru</option><option>@nripk.ru</option></select>
		</p>
		<p>
			<label class="block wide"><strong>�����:</strong></label>
<?php 		$min_quota = "300";		
			if ($MODULE_DATA["output"]["quota_handle"]) { ?>
			<input type="text" size="4" name="<?=$NODE_ID?>[<?=$MODULE_DATA["output"]["mode"]?>][quota]" value="<?=(isset($data["quota"]) ? $data["quota"] : $min_quota)?>" class="block text" /> ��
		</p>
<?php 		} else { ?>
		<?=(isset($data["quota"]) ? $data["quota"] : $min_quota)?> ��
		<input type="hidden" size="4" name="<?=$NODE_ID?>[<?=$MODULE_DATA["output"]["mode"]?>][quota]" value="<?=(isset($data["quota"]) ? $data["quota"] : $min_quota)?>" />
<?php		} ?>
		<p><input type="submit" onclick="if (document.getElementById('csv_file').value=='') { alert('������� ��� �����'); return false; } if(!(/\.csv$/).test(document.getElementById('csv_file').value)) { alert('���� ����� �������� ����������'); return false; }" value="������" />
		</form>
		<a href="/office/mailboxes/">��������� �� �������� ���������� ��������� �������</a>
<?php	//}
		break;

		
		case "list_mailboxes": { ?>
		<p>
			<b><a href="create/">������� ����</a></b> | <b><a href="upload/">��������� �������������</a></b>
		</p>
		<p>
			����������� ��:
<?php 		if (isset($_GET["sortby"]) && $_GET["sortby"] == "name") { ?>
			<a href="?sortby=email">������</a> | <a href="?sortby=size">�������</a> | ��������� 
<?php 		} else if (isset($_GET["sortby"]) && $_GET["sortby"] == "size"){ ?>
			<a href="?sortby=email">������</a> | ������� | <a href="?sortby=name">���������</a>
<?php 		} else { ?>
			������ | <a href="?sortby=size">�������</a> | <a href="?sortby=name">���������</a>
<?php		}
?>
		</p>
		<table>
<?php		foreach ($MODULE_DATA["output"]["mailboxes"] as $mailbox) { ?>
			<tr>
				<td>
					<a href="edit/<?=$mailbox["username"]?>/"><?=$mailbox["username"]?></a>
				</td>
				<td>
					<?=($mailbox["name"] ? " (".$mailbox["name"].")" : "")?>
				</td>
				<td>
					����� <?=(isset($mailbox['messages']) ? $mailbox['messages'] : "0")?>
				</td>
				<td>
					(<?=(isset($mailbox['bytes']) ? ($mailbox['bytes']/1048576 >= 1 ? floor($mailbox['bytes']/1048576)." ��)" : ($mailbox['bytes']/1024 >= 1 ? floor($mailbox['bytes']/1024)." ��)" : $mailbox['bytes']." �)")) : "0  �)")?>
				</td>
				<td>
					��������� ����� <?=$mailbox["lasttime"]?>
				</td>
				<td>
					<a href="delete/<?=$mailbox["username"]?>/" onclick="if (!confirm('�� �������, ��� ������ ������� ���� <?=$mailbox["username"]?>?')) return false;"><img src="/themes/images/delete.png" alt="" /></a>
				</td>
			</tr>
<?php 		} ?>
		</table>
<?php	}
		break;
		
		case "delete_request": { ?>
		<br />������ �� �������� ��������� ����� ���������� � ����� ����������� ��������� ���������������. �� ������ ��������� �� <a href="/office/mailboxes/">�������� ���������� ��������� �������</a>.
<?php	}
		break;
		
		case "checkexists": {
			echo $MODULE_DATA["output"]["exists"];
		}
		break;
		
		default: {
			
		}
		break;
	}
} ?>
<?php
if ($MODULE_OUTPUT["manage_access"] || $MODULE_OUTPUT["privileges"]["cat.create.items"]["create"]) {
	if (!function_exists("DrawCatsSelect")) {
		function DrawCatsSelect($elem_name, $data_array, $selected_item, $depth = 0) {
			if (!$depth) { ?>
	<p class="select_cat">
		<select name="<?=$elem_name?>[]" class="block">
<?php		}
			foreach ($data_array as $key => $data) {
				if (isset($data["allow"]) && $data["allow"]) {
					echo "		<option value=\"$key\"";
					if ($key == $selected_item) {
						echo " selected=\"selected\"";
					}
					echo ">" . str_repeat("&nbsp;", $depth * 4) . $data["title"] . "(".$data["descr"].")" ."</option>\n";
					DrawCatsSelect($elem_name, $data["subitems"], $selected_item, $depth + 1);
				}
			}
			if (!$depth) { ?>
		</select><a href="#" class="del_cat"> �������</a>
	</p>
<?php		}
		}
	}
	if ((($MODULE_OUTPUT["mode"] == "full_list" || $MODULE_OUTPUT["mode"] == "new_full_list") && ($MODULE_OUTPUT["display_variant"] != "add_item")) || ($MODULE_OUTPUT["privileges"]["cat.create.items"]["create"] && (($MODULE_OUTPUT["mode"] == "full_list" || $MODULE_OUTPUT["mode"] == "new_full_list") && ($MODULE_OUTPUT["display_variant"] != "add_item")))) { ?>
	<p class="actions">
		[&nbsp;<a href="<?=$EE["unqueried_uri"]?>?node=<?=$NODE_ID?>&amp;action=add_item#form">�������� ��������</a>&nbsp;]
	</p>
<?php	}

	if ($MODULE_OUTPUT["display_variant"] == "add_item") {
		$data = $MODULE_OUTPUT["form_data"]; ?>
	<a name="form">&nbsp;</a>
	<h2>���������� ���������</h2>
<?php	if ($MODULE_OUTPUT["messages"]["bad"]) {
			$array = array();
			foreach ($MODULE_OUTPUT["messages"]["bad"] as $elem) {
				if (isset($MODULE_MESSAGES[$elem])) {
					$array[] = is_array($MODULE_MESSAGES[$elem]) ? sprintf($MODULE_MESSAGES[$elem][0], $MODULE_MESSAGES[$elem][1]) : $MODULE_MESSAGES[$elem];
				}
			}
			if ($array) {
				foreach ($array as $elem) {
					echo "<p class=\"message red\">$elem</p>\n";
				}
			}
      
      echo '<script>jQuery(document).ready(function($) { var elem; document.querySelector && (elem = document.querySelector(\'p.message\')) && elem.scrollIntoView();});</script>';
		}    
    ?>
	<form action="<?=$EE["unqueried_uri"]?>" method="post" enctype="multipart/form-data" onsubmit="return checkForm(this);">
		<input type="hidden" name="MAX_FILE_SIZE" value="<?=$MODULE_OUTPUT["input_file_max_size"]?>" />
<?php
		$array = array(
			"" => array(
				"title" => "&mdash; �������� ��������� &mdash;",
				"subitems" => array(),
				),
		) + $MODULE_OUTPUT["cats"]/*[$MODULE_OUTPUT["folder_cat_id"]]["subitems"]*/;
	
		if(isset($MODULE_OUTPUT["show_catselect"]) && $MODULE_OUTPUT["show_catselect"]) 
			{ 
				/*������ �������������� ��������*/
				?>
				<input type="hidden" name="<?=$NODE_ID?>[add_item][cat_id]" value="<?=$data["cat_id"]?>" />
				<?php	
			} 
			else 
			{
				?>
				<label class="block wide"><strong>�������</strong>:</label>
				<?$cat = explode(";", $data["cat_id"]);
				DrawCatsSelect($NODE_ID . "[add_item][cat_id]", $array, $data["cat_id"]);?>
				<a href="#" class="add_cat">�������� ������</a>
				<?
			} 	
			?>
			<p>
				<label class="block wide"><strong>���������:</strong></label><span class="hint red" title="������������ ��� ���������� ����">*</span>
				<select name="<?=$NODE_ID?>[add_item][type_id]">
					<option value=""></option>
					<?foreach($MODULE_OUTPUT["types"] as $t_id => $t_name) {?>
						<option value="<?=$t_id?>" <?=($data["type_id"]==$t_id) ? "selected" : ""?>><?=$t_name?></option>
					<?}?>
				</select>
			</p>
		<p>
			<label class="block wide"><strong>����� ������:</strong></label>
			<input type="text" name="<?=$NODE_ID?>[add_item][uripart]" value="<?=$data["uripart"]?>" class="block text" />
			<small>(����� ���� �&nbsp;�������� ������ ��������, ���������: ��������� �����, �����, �����, ���� �������������; ������, ��������� <strong>������ ��&nbsp;����, �����������</strong>!)</small>
		</p>
		<p>
			<label class="block wide"><strong>���� �&nbsp;����� ����������:</strong></label>
			<input type="text" class="date_time" name="<?=$NODE_ID?>[add_item][public][date]" value="<?=$data['public_time']?>" size="18" /><br />
			<small>(������ �������� ������������� ���������� �� ������� ����/�����)</small>
		</p> 
    <?php $show_event_params = (isset($MODULE_OUTPUT["show_event_input"]) && $MODULE_OUTPUT["show_event_input"]); ?>
<?php	/*if(isset($MODULE_OUTPUT["show_event_input"]) && $MODULE_OUTPUT["show_event_input"])*/ { ?>
    <?php if(!$show_event_params) { ?>
    <p>
    <label class="block wide"><strong>��������� �������:</strong></label>

    <input type="checkbox" id="show-event-params" value="1" name="<?=$NODE_ID?>[add_item][event_params]"/>  
      <small>(������������ ��� ��������� ��������: ������ �����������, ����������� � ��������, ����������)</small>
    </p>
    <?php } ?>
		<div id="event-params"<?php if(!$show_event_params) { ?> style="display: none; "<?php } ?>>
    <p>
			<?if($show_event_params) { ?>
				<input type="hidden" value="1" name="<?=$NODE_ID?>[add_item][event_params]"> <?}?>
			<label class="block wide"><strong>���� �&nbsp;����� ������ �������:<span class="obligatorily">*</span></strong></label>
			<input type="text" class="date_time" name="<?=$NODE_ID?>[add_item][start_event][date]" value="<?=$data['start_event']?>" size="18" /><br />
			<!-- <small>(������ �������� ������������� ���������� �� ������� ����/�����)</small> -->
		</p>
		<p>
			<label class="block wide"><strong>���� �&nbsp;����� ��������� �������:</strong></label>
			<input type="text" class="date_time" name="<?=$NODE_ID?>[add_item][end_event][date]" value="<?=$data['end_event']?>" size="18" /><br />
			<small>(������ �������� ������������� ���������� �� ������� ����(��� �� ���� ��������� ������, ���� �������) � ����� 18:00(���� �� �������))</small>
		</p>
    </div>
<?php	} ?>
		<p><strong> �������� �����������: </strong><span class="hint red" title="������������ ��� ���������� ����">*</span><br>
			<script>jQuery(document).ready(function($) {AjaxEditPhoto(6);});</script>
			<input type="file" size="60" name="myfile"></br>
			<small>(��� ������ ������ ������ �������� ���� �������� ��&nbsp;����� <?php echo CF::FormatFilesize($MODULE_OUTPUT["input_file_max_size"]); ?> � 1024�1024 �������� � ������� �� ������ <strong>���������</strong><span class="hint red" title="������������ ��� ���������� ����">*</span><span class="hint red" title="������������ ��� ���������� ����">*</span><span class="hint red" title="������������ ��� ���������� ����">*</span>)</small>
			</br><input id="myForm" type="button" value="���������">
			<div class="separate"></div>
			<div class="tmp_separate hidden">
				<input type="hidden" name="<?=$NODE_ID?>[add_item][main_image][]" value="">
				<input type="hidden" name="_token" value="{{ csrf_token() }}" />
				<a class="big_photo" href="" target="_blank" onclick="">
					<img src="" alt="">
				</a><br>
				<a href="#" class="edit_photo">������������� ����</a>			
			</div>
			<br/> 
			<div id="img_message"></div>	
		</p>
		 <p>
			<label class="block wide"><strong>�������:</strong></label>
			<input type="file" name="<?=$NODE_ID?>_zip_file" class="block"/>
			<small>(��� ������ ������ ������ �������� zip ���� � ������������� � ������� jpeg)</small>
		</p>
		<p>
			<label class="block wide"><strong>���������:</strong></label><span class="hint red" title="������������ ��� ���������� ����">*</span>
			<input type="text" name="<?=$NODE_ID?>[add_item][title]" value="<?=htmlspecialchars($data["title"], ENT_COMPAT, 'cp1251');?>" class="block text" />
			<small>(��������� ���������, ��������� ����� �������, �� ����� 3-� ��������)</small>
		</p>
		<p>
			<label><strong>������� �����:</strong></label><span class="hint red" title="������������ ��� ���������� ����">*</span><br />
			<small>(������������ �&nbsp;�������)</small>
			<textarea name="<?=$NODE_ID?>[add_item][short_text]" cols="20" rows="8" class="common tiny_mce_news"><?=$data["short_text"]?></textarea>
		</p>
		<p>
			<label><strong>������ �����:</strong></label><span class="hint red" title="������������ ��� ���������� ����">*</span><br />
			<small>(������������ ��&nbsp;������� ������ ��������� ����������)</small>
			<textarea name="<?=$NODE_ID?>[add_item][full_text]" cols="20" rows="8" class="common tiny_mce_news"><?=$data["full_text"]?></textarea>
		</p>
		<?if($Auth->usergroup_id==1){?>
		<p>
			<label class="block wide"><strong>���������:</strong></label><br />
			�������:
			 <select name="<?=$NODE_ID?>[add_item][lock_position]">
				<option value=""></option>
				<option <?=$data['lock_position']==1 ? 'selected' : ''?> value="1">1</option>
				<option <?=$data['lock_position']==2 ? 'selected' : ''?> value="2">2</option>	
				<option <?=$data['lock_position']==3 ? 'selected' : ''?> value="3">3</option>
				<option <?=$data['lock_position']==4 ? 'selected' : ''?> value="4">4</option>
			</select><br />
			��:
			<input type="text" class="date_time" name="<?=$NODE_ID?>[add_item][lock_date]" value="<?=$data['lock_date']?>" size="18" /><br />
		</p>
		<?}?>
		
		<?php if (isset($MODULE_OUTPUT["use_tags"])) {?>
		<p>
			<label class="block wide"><strong>�����:</strong></label>
			<input type="text" name="<?=$NODE_ID?>[add_item][tags]" value="<?=$data["tags"]?>" class="block text" />
			<small>(����� ���������, ������������� ����� �������)</small>
		</p>
		<?php } ?>
		<p>
			<input type="submit" value="�������� ��������" />
			<input type="submit" name="<?=$NODE_ID?>[cancel]" value="������" />
		</p>
	</form>
	<script>jQuery(document).ready(function($) {InputDatetimepicker(); });</script>
<?php
	} elseif ($MODULE_OUTPUT["display_variant"] == "edit_item") {
		$data = $MODULE_OUTPUT["form_data"];
?>
	<a name="form">&nbsp;</a>
	<h2>��������� ���������</h2>
<?php

		if ($MODULE_OUTPUT["messages"]["bad"]) {
			$array = array();
			foreach ($MODULE_OUTPUT["messages"]["bad"] as $elem) {
				if (isset($MODULE_MESSAGES[$elem])) {
					$array[] = is_array($MODULE_MESSAGES[$elem]) ? sprintf($MODULE_MESSAGES[$elem][0], $MODULE_MESSAGES[$elem][1]) : $MODULE_MESSAGES[$elem];
				}
			}
			if ($array) {
				foreach ($array as $elem) {
					echo "<p class=\"message red\">$elem</p>\n";
				}
			}
          
    echo '<script>jQuery(document).ready(function($) { var elem; document.querySelector && (elem = document.querySelector(\'p.message\')) && elem.scrollIntoView();});</script>';
		}
    ?>
	<form action="<?=$EE["unqueried_uri"]?>" method="post" enctype="multipart/form-data" onsubmit="return checkForm(this);" name="newsForm">
		<input type="hidden" name="MAX_FILE_SIZE" value="<?=$MODULE_OUTPUT["input_file_max_size"]?>" />
		<input type="hidden" name="<?=$NODE_ID?>[save_item][id]" value="<?=$data["id"]?>" />
<?php

		$array = array(
			"" => array(
				"title" => "&mdash; �������� ��������� &mdash;",
				"subitems" => array(),
			),
		) + $MODULE_OUTPUT["cats"]/*[$MODULE_OUTPUT["folder_cat_id"]]["subitems"]*/;
		
		if(isset($MODULE_OUTPUT["show_catselect"]) && $MODULE_OUTPUT["show_catselect"]) { ?>
		<input type="hidden" name="<?=$NODE_ID?>[save_item][cat_id]" value="<?=$data["cat_id"]?>" />
<?php	} else {?>
			<label class="block wide"><strong>�������</strong>:</label>
			<?$cat = explode(";", $data["cat_id"]);
			foreach($cat as $cat_id) {
				DrawCatsSelect($NODE_ID . "[save_item][cat_id]", $array, $cat_id);
			}?>
			<a href="#" class="add_cat">�������� ������</a>
				
		<?} ?>
		<p>
			<label class="block wide"><strong>���������:</strong></label><span class="hint red" title="������������ ��� ���������� ����">*</span>
			<select name="<?=$NODE_ID?>[save_item][type_id]">
				<option value=""></option>
				<?foreach($MODULE_OUTPUT["types"] as $t_id => $t_name) {?>
					<option value="<?=$t_id?>" <?=($data["type_id"]==$t_id) ? "selected" : ""?>><?=$t_name?></option>
				<?}?>
			</select>
		</p>
		<p>
			<label class="block wide"><strong>����� ������:</strong></label>
			<input type="text" name="<?=$NODE_ID?>[save_item][uripart]" value="<?=$data["uripart"]?>" class="block text" />
			<small>(����� ���� �&nbsp;�������� ������ ��������, ���������: ��������� �����, �����, �����, ���� �������������; ������, ��������� <strong>������ ��&nbsp;����, �����������</strong>!)</small>
		</p>
		<p>
			<label class="block wide"><strong>���� �&nbsp;����� ����������:</strong></label>
			<input type="text" class="date_time" name="<?=$NODE_ID?>[save_item][public][date]" value="<?=$data['public_time']?>" size="18" />
		</p>
<?php $show_event_params = (isset($MODULE_OUTPUT["show_event_input"]) && $MODULE_OUTPUT["show_event_input"]); ?>
<?php	/*if(isset($MODULE_OUTPUT["show_event_input"]) && $MODULE_OUTPUT["show_event_input"])*/ { ?>
    <?php if(!$show_event_params) { ?>
    <p>
    <label class="block wide"><strong>��������� �������:</strong></label>

    <input type="checkbox" id="show-event-params" value="1" name="<?=$NODE_ID?>[save_item][event_params]" <?if($data['start_event'] != "0000-00-00 00:00:00") { ?>checked="checked" <?}?>/>
    <small>(������������ ��� ��������� ��������: ������ �����������, ����������� � ��������, ����������)</small>
    </p>
    <?php } ?>
	    
    <div id="event-params"<?php if(!$show_event_params && ($data['start_event'] == "0000-00-00 00:00:00")) { ?> style="display: none; "<?php } ?>>
	<?if($show_event_params) { ?>
	<input type="checkbox" id="show-event-params" value="1" name="<?=$NODE_ID?>[save_item][event_params]" checked="checked" <?if($show_event_params) { ?> style="display: none; "<?php }?>/><?}?>
		<p>
			<label class="block wide"><strong>���� �&nbsp;����� ������ �������:</strong></label>
			<input type="text" class="date_time" name="<?=$NODE_ID?>[save_item][start_event][date]" value="<?=$data['start_event']?>" size="18" /><br />
			<small>(������ �������� ������������� ���������� �� ������� ����/�����)</small>
		</p>
		<p>
			<label class="block wide"><strong>���� �&nbsp;����� ��������� �������:</strong></label>
			<input type="text" class="date_time" name="<?=$NODE_ID?>[save_item][end_event][date]" value="<?=$data['end_event']?>" size="18" /><br />
			<small>(������ �������� ������������� ���������� �� ������� ����(��� �� ���� ��������� �������, ���� �������) � ����� 18:00(���� �� �������))</small>
		</p>
    </div>
<?php	} ?>
		<p>
			<label class="block wide"><strong>���������:</strong></label><span class="hint red" title="������������ ��� ���������� ����">*</span>
			<input type="text" name="<?=$NODE_ID?>[save_item][title]" value="<?=htmlspecialchars($data["title"], ENT_COMPAT, 'cp1251');?>" class="block text" />
			<small>(��������� ���������, ��������� ����� �������, �� ����� 3-� ��������)</small>  
		</p>
<?php if (0) {//!is_null($data["image_data"])?>		
		<p>
			<label class="block wide"><strong>������� �����������:</strong></label>
			<input type="checkbox" name="<?=$NODE_ID?>[save_item][del_image]" />
		</p>
<?php } ?>		

		
		
		<p> <strong> �������� �����������: </strong><span class="hint red" title="������������ ��� ���������� ����">*</span><br>
			<script>jQuery(document).ready(function($) {AjaxEditPhoto(6);});</script>
			<input type="file" size="60" name="myfile"></br>
			<small>(��� ������ ������ ������ �������� ���� �������� ��&nbsp;����� <?php echo CF::FormatFilesize($MODULE_OUTPUT["input_file_max_size"]); ?> � 1024�1024 �������� � ������� �� ������ <strong>���������</strong><span class="hint red" title="������������ ��� ���������� ����">*</span><span class="hint red" title="������������ ��� ���������� ����">*</span><span class="hint red" title="������������ ��� ���������� ����">*</span>)</small>
			</br><input id="myForm" type="button" value="���������">
			<div class="separate">
				<?if(file_exists($_SERVER["DOCUMENT_ROOT"]."/images/news/".$data["id"].".jpg")) {?>
					<input type="hidden" name="<?=$NODE_ID?>[save_item][main_image][]" value="<?=$data["id"]?>">
					<a class="big_photo" href="/images/news/<?=$data["id"]?>.jpg" target="_blank" onclick="">
						<img style="width: 25%" src="/images/news/<?=$data["id"]?>.jpg" alt="">
					</a><br>
					<a href="#" class="edit_photo">������������� ����</a>	
				<?}?>
			</div>
			<div class="tmp_separate hidden">
				<input type="hidden" name="<?=$NODE_ID?>[save_item][main_image][]" value="">
				<a class="big_photo" href="" target="_blank" onclick="">
					<img src="" alt="">
				</a><br>
				<a href="#" class="edit_photo">������������� ����</a>			
			</div>
			<br/> 
			<div id="img_message"></div>	
		</p>
		
		
		
		
		
		<p>
			<label class="block wide"><strong>�������:</strong></label>
			<input type="file" name="<?=$NODE_ID?>_zip_file" class="block"/>
			<small>(��� ������ ������ ������ �������� zip ����� � ������������� � ������� jpeg)</small>
		</p>
		<p>
			<label class="block wide"><strong>������� �������:</strong></label>
			<input type="checkbox" name="<?=$NODE_ID?>[save_item][delete_gallery]" class="block"/>
		</p>
		<p>
			<label><strong>������� �����:</strong></label><span class="hint red" title="������������ ��� ���������� ����">*</span><br />
			<small>(������������ �&nbsp;�������)</small>
			<textarea name="<?=$NODE_ID?>[save_item][short_text]" cols="20" rows="8" class="common tiny_mce_news"><?=$data["short_text"]?></textarea>
		</p>
		<p>
			<label><strong>������ �����:</strong></label><span class="hint red" title="������������ ��� ���������� ����">*</span><br />
			<small>(������������ ��&nbsp;������� ������ ��������� ����������)</small>
			<textarea name="<?=$NODE_ID?>[save_item][full_text]" cols="20" rows="8" class="common tiny_mce_news"><?=$data["full_text"]?></textarea>
		</p>
		<?php if (isset($MODULE_OUTPUT["use_tags"])) {?>
		<p>
			<label class="block wide"><strong>�����:</strong></label>
			<input type="text" name="<?=$NODE_ID?>[save_item][tags]" value="<?=$data["tags"]?>" class="block text" />
			<small>(����� ���������, ������������� ����� �������)</small>
		</p>
		<?php } ?>
		<?if($Auth->usergroup_id==1){?>
		<p>
			<label class="block wide"><strong>���������:</strong></label><br />
			�������:
			 <select name="<?=$NODE_ID?>[save_item][lock_position]">
				<option value=""></option>
				<option <?=$data['lock_position']==1 ? 'selected' : ''?> value="1">1</option>
				<option <?=$data['lock_position']==2 ? 'selected' : ''?> value="2">2</option>	
				<option <?=$data['lock_position']==3 ? 'selected' : ''?> value="3">3</option>
				<option <?=$data['lock_position']==4 ? 'selected' : ''?> value="4">4</option>
			</select><br />
			��:
			<input type="text" class="date_time" name="<?=$NODE_ID?>[save_item][lock_date]" value="<?=$data['lock_date']?>" size="18" /><br />
		</p>
		<?}?>
		<p>
			<input type="submit" value="��������� ��������" />
			<input type="reset" value="�����" />
			<input type="submit" name="<?=$NODE_ID?>[cancel]" value="������" />
		</p> 
	</form>
	<script>jQuery(document).ready(function($) {InputDatetimepicker();});</script>
<?php
	}
	elseif ($MODULE_OUTPUT["display_variant"] == "edit_comment")
	{
			$data = $MODULE_OUTPUT["form_data"];
			
?>
	<a name="form"></a>
	<h2>�������������� �����������</h2>
	<form action="<?=$EE["unqueried_uri"]?>" method="post" enctype="multipart/form-data" class="manage-form">
	<p><label><strong>�����:</strong></label><br />
		<textarea name="<?=$NODE_ID?>[save_comment][text]" cols="20" rows="8" class="common tiny_mce_news"><?=$data["text"]?></textarea></p>
	<input type="hidden" name="<?=$NODE_ID?>[save_comment][id]" value="<?=$data["id"]?>">
	<p><input type="submit" value="��������� ��������" /></p>
	</form>
<?
	}
}
?>
<?header("Content-type: text/html; charset=windows-1251");?>
<?
$MODULE_MESSAGES = array(
301 => "���� '@0' ����������� ��� ����������",
302 => "����� �������� ��� ��������",
201 => "�������� ��������"
);
$CRITICAL_ERRORS = array(401);//����� ������ �� ��������� ��������� ���������� ��������

if ($MODULE_OUTPUT["messages"]["good"])
{
    $array = array();

    foreach ($MODULE_OUTPUT["messages"]["good"] as $elem)
    {
			if(is_array($elem)) {				
				foreach($elem as $err => $data) {
					if(is_array($data)){
						for($i=0;$i<count($data);$i++) {
						$MODULE_MESSAGES[$err] = str_replace("@{$i}", $data[$i], $MODULE_MESSAGES[$err]);
						}
					} elseif(!empty($data)) {
						$t = str_replace("@0", $data, $MODULE_MESSAGES[$err]);
					} else {
						$t = $MODULE_MESSAGES[$err];
					}
					$array[] = $t;
				}
			}else {
				if (isset($MODULE_MESSAGES[$elem]))
				{
						$array[] = $MODULE_MESSAGES[$elem];
				}
			}
    }

    if ($array)
    {
        foreach ($array as $elem)
        {
            echo "<p class=\"message green\">".$elem."</p>\n";
        }
    }
}

if ($MODULE_OUTPUT["messages"]["bad"])
{
    $array = array();

    foreach ($MODULE_OUTPUT["messages"]["bad"] as $elem)
    {
			if(is_array($elem)) {				
				foreach($elem as $err => $data) {
					if(is_array($data)){
						for($i=0;$i<count($data);$i++) {
						$MODULE_MESSAGES[$err] = str_replace("@{$i}", $data[$i], $MODULE_MESSAGES[$err]);
						}
					} elseif(!empty($data)) {
						$t = str_replace("@0", $data, $MODULE_MESSAGES[$err]);
					} else {
						$t = $MODULE_MESSAGES[$err];
					}
					$array[] = $t;
				}
			}else {
				if (isset($MODULE_MESSAGES[$elem]))
				{
						$array[] = $MODULE_MESSAGES[$elem];
				}
			}
    }

    if ($array)
    {
        foreach ($array as $elem)
        {
            echo "<p class=\"message red\">".$elem."</p>\n";
        }
    }

}

switch($MODULE_OUTPUT["mode"])
{
  case "main": {	?>
		<ul class="tabs">
			<li class="current" data-id="add_request">������ ������</li>
			<li data-id="register">����������� ������������</li>
			<li data-id="request_new">�����</li>
			<li data-id="request_open">� ������</li>
			<li data-id="request_close">���������</li>
			<?if($MODULE_OUTPUT["printers_managment"]){?>
				<li data-id="settings">���������</li>
				<li data-id="reports">������</li>
			<?}?>
		</ul>
		<div class="content">
			<div class="">
				<div class="errors red"></div>
				<table>
				<tbody class="cartrige">	
					<tr class="after">
						<td><input type="text" size="37" placeholder="������� ����������� �����" class="device_search"></td>
					</tr>
				</tbody>
				</table>
			</div>
		</div>
		<div class="overlay hidden"> </div>
		<div class="printers_form hidden"></div>
	<?}
	break;

	case "register": {?>
		<div class="">
			<form action="<?=$EE["unqueried_uri"]?>" method="post">
			<div class="errors red"></div>
			<table style="text-align: left;">
				<tbody>
					<tr>
						<th>�������������</th>
						<td>
							<select name="unit_id" size="1" class="unit_list register" style="width: 220px;">
								<option value="">----</option>
							</select>
							<input type="text" class="unit_search register" size="10" title="������� �����" placeholder="������� ����� ��������"/> 
						</td>
					</tr>
					<tr>
						<th>������</th>
						<td>
							<select size="1" class="building" style="width: 220px;">
								<option value="">----</option>
								<?foreach($MODULE_OUTPUT["register_data"]["buildings"] as $id => $name){?>
									<option value="<?=$id?>"><?=$name?></option>
								<?}?>
							</select>
						</td>
					</tr>
					<tr>
						<th>�������</th>
						<td>
							<input type="text" name="auditorium" class="auditorium register">
						</td>
					</tr>
					<tr>
						<th>������������� ����</th>
						<td>
							<div>
								<select name="responsible_person_id" size="1" class="responsible_person_id" style="width: 220px;">
										<option value="">----</option>
								</select> 
								<input type="text" class="teacher_search register" size="7" title="������� �����" placeholder="���"/>         
								<input type="text" class="teacher_print register" size="7" value="" title="������� ���" name="responsible_person" placeholder="������� ���" style="display: none; width: 220px; " /> 
							</div>
							<div>
									<a href="javascript: void(0)" class="cl_teacher_search" title="����� �� ���">������</a>|
									<a href="javascript: void(0)" class="cl_teacher_print"  title="����� �� ���">���� ��� � ����</a>
							</div>
						</td>
					</tr>
					<tr>
						<th>�������������</th>
						<td>
							<select size="1" class="select_manufacturer" style="width: 220px;">
								<option value="0">�������� �������������</option>
								<?foreach($MODULE_OUTPUT["register_data"]["manufacturers"] as $id => $name){?>
									<option value="<?=$id?>"><?=$name?></option>
								<?}?>
							</select>
						</td>
					</tr>
					<tr>
						<th>������</th>
						<td class="device_content">
							<select size="1" style="width: 220px;" disabled>
								<option value="">������� �������� �������������</option>
							</select>
						</td>
					</tr>
					<tr>
						<th>����������� �����</th>
						<td>
							<input type="text" name="inventory" class="inventory register">
						</td>
					</tr>
					<tr>
						<th>�������� �����</th>
						<td>
							<input type="text" name="serial" class="serial register">
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<input type="submit" value="����������������" class="register_device"/>
						</td>
					</tr>
				</tbody>
			</table>
			</form>
		</div>
	<?}
	break;	
	
	case "ajax_select_device": {?>
		<select size="1" class="select_device" name="device_id" style="width: 220px;" <?=(empty($MODULE_OUTPUT["devices"]) ? "disabled" : "")?>>
			<option value="">�������� ����������</option>
			<?foreach($MODULE_OUTPUT["devices"] as $id => $name){?>
				<option value="<?=$id?>"><?=$name?></option>
			<?}?>
		</select>
	<?}
	break;
	case "ajax_add_comment": {?>
		<?foreach($MODULE_OUTPUT["comments"] as $date => $comments) {?>
			<span class="comment_date"><?=$date?></span><br />
			<?foreach($comments as $comment) {?>
				<span class="comment_text"><?=$comment?></span><br />
			<?}?>								
		<?}?>
	<?}
	break;
	
	case "ajax_load_register_data": {$r=$MODULE_OUTPUT["request"];?>
		<tr>
			<th>����������: </th>
			<td>
				<select size="1" class="register select_device" style="width: 220px;">
					<?foreach($MODULE_OUTPUT["devices"] as $id => $name){?>
						<option value="<?=$id?>" <?=$r["device_info"]["device_id"] == $id ? "selected" : ""?>><?=$name?></option>
					<?}?>
				</select>
			</td>
		</tr>
		<tr>
			<th>����������� �����:</th>
			<td><input type="text" name="serial" class="register inventory" value="<?=$r["device_info"]["inventory"]?>"></td>
		</tr>
		<tr>
			<th>�������� �����:</th>
			<td><input type="text" name="serial" class="register serial" value="<?=$r["device_info"]["serial"]?>"></td>
		</tr>
		<tr>
			<th>������������ ����:</th>
			<td>
				<div>
					<select size="1" class="register responsible_person_id" style="display: <?=$r["device_info"]["responsible_person_id"] ? "" : "none"?>; width: 220px;">
							<option value="<?=$r["device_info"]["responsible_person_id"]?>"><?=$r["device_info"]["responsible_person_id"] ? $r["device_info"]["responsible_person"] : "---"?></option>
							<option value="">��� � ����</option>
					</select> 
					<input type="text" class="teacher_search register" size="7" title="������� �����" placeholder="���" style="display: <?=$r["device_info"]["responsible_person_id"] ? "" : "none"?>"/>         
					<input type="text" class="register teacher_print" size="7" value="<?=!$r["device_info"]["responsible_person_id"] ? $r["device_info"]["responsible_person"] : ""?>" title="������� ���" name="responsible_person" placeholder="������� ���" style="display: <?=!$r["device_info"]["responsible_person_id"] ? "" : "none"?>; width: 220px; " /> 
				</div>
				<div>
						<a href="javascript: void(0)" class="cl_teacher_search" title="����� �� ���">������</a>|
						<a href="javascript: void(0)" class="cl_teacher_print"  title="����� �� ���">���� ��� � ����</a>
				</div>
			</td>
		</tr>
		<tr>
			<th>�������������:</th>
			<td>
				<select name="unit_id" size="1" class="register unit_list" style="width: 220px; ">
					<option value="<?=$r["device_info"]["unit_id"]?>"><?=$r["device_info"]["unit"]?></option>
				</select>
				<input type="text" class="unit_search register" size="10" title="������� �����" placeholder="������� ����� ��������"/> 		
			</td>
		</tr>
		<tr>
			<th>������:</th>
			<td>
				 <select size="1" class="register building" style="width: 220px; ">
					<option value="<?=$r["device_info"]["building"]["id"]?>"><?=$r["device_info"]["building"]["name"]?>(<?=$r["device_info"]["building"]["label"]?>)</option>
					<?foreach($MODULE_OUTPUT["register_data"]["buildings"] as $id => $name){?>
						<option value="<?=$id?>"><?=$name?></option>
					<?}?>
				</select>
			</td>
		</tr>
		<tr>
			<th>�������:</th>
			<td>
				<input type="text" name="auditorium" class="register auditorium" value="<?=$r["device_info"]["auditorium"]?>">
			</td>
		</tr>
		<tr>
			<th>���� ����� � ������������:</th>
			<td><input type="text" class="register date_start report_select_date" value="<?=$r["device_info"]["date_start"]?>"></td>
		</tr>
		<tr>
			<th>���������:</th>
			<td><input type="text" class="register start_price" value="<?=$r["device_info"]["start_price"]?>"></td>
		</tr>	
	<?}
	break;
	
	case "view_form": {$r=$MODULE_OUTPUT["request"];?>			
		<div class="head">�������� ������<div class="close">X</div></div>
		<div class="form" data-id="<?=$r["id"]?>">
				<input type="hidden" class="current_status" value="<?=$r["status"]?>">
				<input type="hidden" class="current_page" value="<?=$MODULE_OUTPUT["current_page"]?>">
				<input type="hidden" class="register_id" value="<?=$r["device_info"]["id"]?>">
				<span class="red"></span>
				<table style="text-align: left; width: 100%">
					
						<tr>
							<th>���� ���������:</th>
							<td><?=CF::reverseDate($r["date_open"])?></td>
						</tr>
						<tr>
							<th>���� ��������:</th>
							<td><?=($r["date_close"] ? CF::reverseDate($r["date_close"]) : "-")?></td>
						</tr>
						<tr>
							<th>������ �����:</th>
							<td><?=$r["displayed_name"]?></td>
						</tr>						
						<?if($MODULE_OUTPUT["printers_managment"]){?>
							<tr>
								<th>������: </th>
								<td>
									<select class="status">
										<option value="�����" <?=($r["status"]=="�����" ? "selected" : "")?>>�����</option>
										<option value="� ������" <?=($r["status"]=="� ������" ? "selected" : "")?>>� ������</option>
										<option value="���������" <?=($r["status"]=="���������" ? "selected" : "")?>>���������</option>
									</select>
								</td>
							</tr>
						<?}?>
					<tbody class="register_content">
						<tr>
							<th>����������: </th>
							<td><?=$r["device_info"]["device"]?></td>
						</tr>
						<tr>
							<th>����������� �����:</th>
							<td><?=$r["device_info"]["inventory"]?></td>
						</tr>
						<tr>
							<th>�������� �����:</th>
							<td><?=$r["device_info"]["serial"]?></td>
						</tr>
						<tr>
							<th>������������ ����:</th>
							<td>
								<?=$r["device_info"]["responsible_person"]?>
							</td>
						</tr>
						<tr>
							<th>�������������:</th>
							<td><?=$r["device_info"]["unit"]?></td>
						</tr>
						<tr>
							<th>������:</th>
							<td><?=$r["device_info"]["building"]["name"]?></td>
						</tr>
						<tr>
							<th>�������:</th>
							<td><?=$r["device_info"]["auditorium"]?></td>
						</tr>
						<tr>
							<th>���� ����� � ������������:</th>
							<td><?=($r["device_info"]["date_start"] && $r["device_info"]["date_start"]!="0000-00-00") ? CF::reverseDate($r["device_info"]["date_start"]) : "-"?></td>
						</tr>
						<tr>
							<th>���������:</th>
							<td><?=$r["device_info"]["start_price"] ? $r["device_info"]["start_price"] : "-"?></td>
						</tr>
						<?if($MODULE_OUTPUT["printers_managment"]){?>
							<tr>
								<th colspan="2"><div class="edit_register_device div_button">�������������</div></td>
							</tr>		
						<?}?>							
					</tbody>
				</table>
							
							
				�����������:
					<div class="comment_frame">
						<div class="user_comment">
							<span class="comment_date"><?=CF::reverseDate($r["date_open"])?>[������������]</span><br />
							<span class="comment_text"><?=$r["reason"]?></span><br />
						</div>
						<div class="comments_list">
							<?=$r["comments"] ? "" : "-"?>
							<?foreach($r["comments"] as $date => $comments) {?>
								<span class="comment_date"><?=$date?></span><br />
								<?foreach($comments as $comment) {?>
									<span class="comment_text"><?=$comment?></span><br />
								<?}?>								
							<?}?>
						</div>
						<?if($MODULE_OUTPUT["printers_managment"]){?>
							<div class="add_comment">
								<div class="comment_input_frame"><input type="text" class="comment input"/></div>
								<div class="comment_button">OK</div>
							</div>
						<?}?>
					</div>
					<?if($MODULE_OUTPUT["printers_managment"]){?>
						<div class="comment request save_button">���������</div>
					<?}?>
		</div>			
	<?}
	break;
	case "show_requests": {	?>
		<?if($MODULE_OUTPUT["messages"]["bad"][0]) {?>
			<p class="message red"><?=$MODULE_OUTPUT["messages"]["bad"][0]?></p>
		<?break;}?>

		<table class="nsau_table printers_requests">
			<thead>
				<tr>
					<th></th>
					<th>���� ���������</th>
					<th>���� ��������</th>
					<th>���</th>
					<th>����������</th>
					<th>�������������</th>
					<th>�����������</th>
				</tr>
			</thead>
			<tbody>
				<?foreach($MODULE_OUTPUT["requests"]["data"] as $request_id => $request) {?>
					<tr data-id="<?=$request_id?>" class="request_row">
						<td class="select_request_check_box"><input type="checkbox" class="select_request"></td>
						<td class="select_request"><?=CF::reverseDate($request["date_open"])?></td>
						<td class="select_request"><?=($request["date_close"] ? CF::reverseDate($request["date_close"]) : "-")?></td>
						<td class="select_request"><?=$request["displayed_name"]?></td>
						<td class="select_request"><?=$request["device_info"]["device"]?></td>
						<td class="select_request"><?=$request["device_info"]["unit"]?></td>
						<td class="select_request">
							<div class="user_comment">
								<span class="comment_date"><?=CF::reverseDate($request["date_open"])?>[������������]</span><br />
								<span class="comment_text"><?=$request["reason"]?></span><br />
							</div>
							<?foreach($request["comments"] as $date => $comments) {?>
								<span class="comment_date"><?=$date?></span><br />
								<?foreach($comments as $comment) {?>
									<span class="comment_text"><?=$comment?></span><br />
								<?}?>								
							<?}?>
						</td>
					</tr>
				<?}?>
			</tbody>
		</table>
		<!--start pagination-->
		<div class="pager">
		<?//CF::Debug($MODULE_OUTPUT["requests"]["pager_output"])?>
			<?if (isset($MODULE_OUTPUT["requests"]["pager_output"]))
			{
				$pager_output = $MODULE_OUTPUT["requests"]["pager_output"];
				$all_pages= count($pager_output["pages_data"]); //����� ���������� ������ �� ��������
				if ($all_pages > 1)
				{
					//���������� �������� (left arrow)
					if ($pager_output["prev_page"])
					{
						?><a data-id="<?=$pager_output["pages_data"][$pager_output["prev_page"]]["db_from"]?>" title="���������� ��������" id="prev_page_link" class="prev-next-link ajax_change_page"><span class="arrow">&larr;&nbsp;</span></a>&nbsp;<?
					}
					//���������� ������� ���������
					//���� ����� �������� ������ 16, �� ������� ������ � 1 �� 34
					if ($pager_output["prev_page"]<=16) {
						$start_pg=0;
						$end_pg=34;
					}
					//����� ������� 16 ������ �� ������� � 16 �����
					else
					{
						$start_pg=$pager_output["prev_page"]-16;
						$end_pg=$pager_output["prev_page"]+16;
					}
					//������ �� ������ ��������
					if ($all_pages>16 && $pager_output["prev_page"]>16)
					{
						echo '<a data-id="1">1</a> ...';
					}
					foreach($pager_output["pages_data"] as $page)
					{
						if ($cur_page<=$end_pg && $cur_page>=$start_pg)
						{
							//������� ��������
							if($page["is_current"]==1)
								{
									?> <span class="active item"><?=$page["data_array"]["page"]?></span> <?
								}
							//��������� ��������
							if($page["is_current"]!=1)
							{
									?> <a data-id="<?=$page["data_array"]["page"]?>" class="ajax_change_page"><?=$page["data_array"]["page"]?></a> <?
							}
						}
						$cur_page++;
					}

					//������ �� ��������� ��������
					if ($all_pages>16 && $pager_output["prev_page"]<=$all_pages-16)
					{
						echo '... <a href="?page='.$all_pages.'">'.$all_pages."</a>\n";
					}
					//��������� �������� (right arrow)
					if ($pager_output["next_page"])
					{
						?>
						<a data-id="<?=$MODULE_OUTPUT["requests"]["pager_output"]["next_page"]?>" title="��������� ��������" id="next_page_link" class="prev-next-link ajax_change_page"><span class="arrow">&nbsp;&rarr;</span></a><?
					}
				}
			}
			?>
		<!--stop pagination-->
		</div>
		<?if($MODULE_OUTPUT["printers_managment"]){?>
			<div class="select_menu hidden">
				<a class="delete_selected_requests">�������</a>
				������� ������
				<select class="status_selected_requests">
					<option value="�����" <?=($request["status"]=="�����" ? "selected" : "")?>>�����</option>
					<option value="� ������" <?=($request["status"]=="� ������" ? "selected" : "")?>>� ������</option>
					<option value="���������" <?=($request["status"]=="���������" ? "selected" : "")?>>���������</option>
				</select>		
			</div>
		<?}?>
	<?}
	break;
	
	case "settings": {?>
		<div class="edit devices">
			<h1>������������� ����������</h1>
				<span class="red"></span>
				<table>
					<tr>
						<th>�������������</th>
						<td>
							<select size="1" class="select_manufacturer" style="width: 220px;">
								<option value="0">�������� �������������</option>
								<?foreach($MODULE_OUTPUT["register_data"]["manufacturers"] as $id => $name){?>
									<option value="<?=$id?>"><?=$name?></option>
								<?}?>
							</select>
						</td>
						<td rowspan="2" class="">
							<div class="div_button edit_device action_edit">�������������</div>
							<div class="div_button edit_device action_delete">�������</div> 
							<div class="div_button edit_device action_add">��������</div> 
						</td>
					</tr>
					<tr>
						<th>������</th>
						<td class="device_content">
							<select size="1" style="width: 220px;" disabled>
								<option value="">������� �������� �������������</option>
							</select>
						</td>
					</tr>
					<tbody class="device_form_content"></tbody>
				</table>
		</div><br />	
		<?function openSubunits($arr) {?>		
			<ul  class="splCont">
				<?foreach($arr as $i => $subunit) {?>
					<li  class="tree" data-id="<?=$subunit["id"]?>">
						<span><?=$subunit["name"]?></span>
						<br />
						<small>
							<a href="#" class="button_edit">[�������������]</a>
							<a href="#" class="button_add">[�������� �����]</a>
							<a href="#" class="button_delete">[�������]</a>
						</small>
						<div class="unit_content unit_<?=$subunit["id"]?>"></div>
						<?if(!empty($subunit["subunits"])) {
							openSubunits($subunit["subunits"]);
						}?>
					</li>
				<?}?>
			</ul>
		<?}?>
		<h1>������������� �������������</h1>
		<ul>
			<li data-id="0"  class="tree">
			<a href="#" class="button_add">�������� �������������</a>
			<div class="unit_content unit_0"></div>
			</li>
		</ul>
		<?openSubunits($MODULE_OUTPUT["edit_units"]);?>				
	
	<?}
	break;

	case "settings_ajax_load_edit_device_form": {$d = $MODULE_OUTPUT["device"];?>
		<tr>
			<th colspan="2">������������� ����������</th>
		</tr>
		<tr>
			<th>�������������</th>
			<td>
				<select size="1" class="manufacturer_id" style="width: 220px;">
					<?foreach($MODULE_OUTPUT["register_data"]["manufacturers"] as $id => $name){?>
						<option value="<?=$id?>" <?=$d["manufacturer_id"]==$id ? "selected" : ""?>><?=$name?></option>
					<?}?>
				</select>
			</td>
		</tr>
		<tr>
			<th>������</th>
			<td>
				<input type="text" data-id="<?=$d["id"]?>" class="register device_name" value="<?=$d["name"]?>">
			</td>
		</tr>
		<tr>
			<th><div class="div_button device_edit cancel">������</div></th>
			<th><div class="div_button device_edit save">���������</div></th>
		</tr>
	<?}
	break;
	
	case "settings_ajax_load_add_device_form": {?>
		<tr>
			<th colspan="2">����� ����������</th>
		</tr>
		<tr>
			<th>�������������</th>
			<td>
				<select size="1" class="manufacturer_id" style="width: 220px;">
					<option value="0">�������� �������������</option>
					<?foreach($MODULE_OUTPUT["register_data"]["manufacturers"] as $id => $name){?>
						<option value="<?=$id?>"><?=$name?></option>
					<?}?>
				</select>
			</td>
		</tr>
		<tr>
			<th>������</th>
			<td>
				<input type="text" class="register device_name">
			</td>
		</tr>
		<tr>	
			<th><div class="div_button device_edit cancel">������</div></th>
			<th><div class="div_button device_edit add">��������</div></th>
		</tr>
	<?}
	break;
	
	
	case "settings_ajax_add_unit": {?>
		�������������: <input type="text" class="unit_name"><br />
		��������: <input type="text" class="unit_contacts"><br />
		<a class="add_unit">��������</a> <a class="cancel_unit">������</a><br />
	<?}
	break;
	
	case "settings_ajax_edit_unit": {?>
		�������������: <input type="text" class="unit_name" value="<?=$MODULE_OUTPUT["edit_info"]["name"]?>"><br />
		��������: <input type="text" class="unit_contacts" value="<?=$MODULE_OUTPUT["edit_info"]["contacts"]?>"><br />
		<a class="save_unit">���������</a> <a class="cancel_unit">������</a><br />
	<?}
	break;
		
	case "ajax_get_device_info": {?>
		<tr class="tmp"><th>�������������:</th><td><?=$MODULE_OUTPUT["info"]["unit"]?></td></tr>
		<tr class="tmp">
			<th>������������:</th>
			<td>
			������: <?=$MODULE_OUTPUT["info"]["building"]["name"] ? $MODULE_OUTPUT["info"]["building"]["name"] : "-"?>, 
			�������: <?=$MODULE_OUTPUT["info"]["building"]["label"]?><?=$MODULE_OUTPUT["info"]["auditorium"] ? $MODULE_OUTPUT["info"]["auditorium"] : "-"?>
			</td>
		</tr>
		<tr class="tmp"><th>������������� ����</th><td><?=$MODULE_OUTPUT["info"]["responsible_person"]?></td></tr>
		<tr class="tmp"><th>����������</th><td><?=$MODULE_OUTPUT["info"]["device"]?></td></tr>
		<tr class="tmp"><th>�������� �����</th><td><?=$MODULE_OUTPUT["info"]["serial"] ? $MODULE_OUTPUT["info"]["serial"] : "-"?></td></tr>
		<tr class="tmp"><th>����������� �����</th><td><?=$MODULE_OUTPUT["info"]["inventory"]?></td></tr>
		<tr class="tmp"><th>������� ���������</th><td><input type="text" class="reason"/></td></tr>
		<tr class="tmp"><td><input type="hidden" class="device_id" value="<?=$MODULE_OUTPUT["info"]["id"]?>"/></td></tr>
		<tr class="tmp">
			<td colspan="2">
				<input type="submit" value="���������" class="add_request" />
			</td>
		</tr>
	<?}
	break;
	
	case "add_request": {?>
		<div class="content">
			<div class="">
				<div class="errors red"></div>
				<table>
				<tbody class="cartrige">	
					<tr class="after">
						<td><input type="text" size="37" placeholder="������� ����������� �����" class="device_search"></td>
					</tr>
				</tbody>
				</table>
			</div>
		</div>
	<?}
	break;
	
	case "reports": {?>
	<form action="report/" method="POST">
		����� � <br />
		<input type="text" name="date_begin" class="report_select_date"><br />
		�� <br />
		<input type="text" name="date_end" class="report_select_date"><br />
		<input type="submit" value="�������">
	</form>
	<?}
	break;
	
	case "report": {?>
	<style>
		table.effect_report_table {
			border-collapse: collapse;
			border: 1px #000 solid;
			vertical-align: middle;
			font-weight: normal;
			text-align: left;
			width: 100%;
			/*margin: 0 auto;*/
		}
		table.effect_report_table th,table.effect_report_table td {
			border: 1px #000 solid;
		}
		table.effect_report_table thead,table.effect_report_table thead tr,table.effect_report_table thead td,table.effect_report_table thead th {
			text-align: center;
		}
	</style>
	<table class="effect_report_table">
		<thead>
			<tr>
				<td colspan="3">������������</td>
				<td colspan="3">����������</td>
				<td rowspan="2">���� ���������</td>
				<td rowspan="2">������� ���������</td>
				<td rowspan="2">����������� ������</td>
				<td rowspan="2">���� �������� ���������</td>
				<td rowspan="2">��������� ���������</td>
			</tr>
			<tr>
				<td>�������������</td>
				<td>������������� ����</td>
				<td>�������</td>
				<td>������</td>
				<td>����������� �����</td>
				<td>�������� �����</td>
			</tr>
		</thead>
		<tbody>
			<?foreach($MODULE_OUTPUT["report"] as $r) {?>
				<tr>
					<td><?=$r["device_info"]["unit"]?></td>
					<td><?=$r["device_info"]["responsible_person"]?></td>
					<td><?=$r["device_info"]["building"]["label"]?><?=$r["device_info"]["auditorium"]?></td>
					<td><?=$r["device_info"]["device"]?></td>
					<td><?=$r["device_info"]["inventory"]?></td>
					<td><?=$r["device_info"]["serial"]?></td>
					<td><?=CF::reverseDate($r["date_open"])?></td>
					<td><?=$r["reason"]?></td>
					<td>
						<?foreach($r["comments"] as $date => $comments) {?>
							<span class="comment_date"><?=$date?></span><br />
							<?foreach($comments as $comment) {?>
								<span class="comment_text"><?=$comment?></span><br />
							<?}?>								
						<?}?>
					</td>
					<td><?=$r["date_close"] ? CF::reverseDate($r["date_close"]) : "-"?></td>
					<td><?=$r["status"]?></td>
				</tr>
			<?}?>
		</tbody>
	</table>
	<?}
	break;
	
}
?>
<?php
// version: 1.1
// date: 2014-01-15

foreach ($MODULE_DATA["output"]["messages"]["good"] as $data)
{
	echo "<p class=\"message\">$data</p>\n";
}

foreach ($MODULE_DATA["output"]["messages"]["bad"] as $data)
{
	echo "<p class=\"message red\">$data</p>\n";
}

if(!function_exists("OutPutFoldersOptions"))
	{
/*		function OutPutFoldersOptions($array, $selected_id = NULL)
		{
			foreach($array as $id => $item)
			{
				$sel_attr = "";
				if ($id == $selected_id) { $sel_attr = " selected"; }
				echo "<option value=\"".$id."\"".$sel_attr.">".$item["descr"]."</option>";
				if(!empty($item["subitems"]))
					OutPutFoldersOptions($item["subitems"], $selected_id);
			}
		}*/
		
		function OutPutFoldersOptions($array, $selected_id  = NULL, $depth = 0)
		{
			//die(print_r($array));
			$displayed = 0;
			$level = "";
			for ($i = $depth; $i > 0; $i--)
				$level .= '-';
			
			foreach($array as $id => $item)
			{		
				if ($id == $selected_id)
					$selected = " selected";
				else
					$selected = "";
			
				if($item["is_active"])
					echo "<option value=\"".$id."\"".$selected.">".$level." ".$item["title"]."</option>";
				else
					echo "<option value=\"".$id."\"".$selected.">".$level." ".$item["title"]." (�� �������)</option>";
				
			if(!empty($item["subitems"]))
				OutPutFoldersOptions($item["subitems"], $selected_id, $depth+1);
			}
		}
	}

if(!isset($MODULE_DATA["output"])) $MODULE_DATA["output"] = $MODULE_OUTPUT; 
if (ALLOW_AUTH)
{
	switch ($MODULE_DATA["output"]["mode"])
	{
		case "show_modules":
			?>
 			<div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-sm-6">
                        <h4>������ ���� ������ �������������</h4>
						<a href="<?=$EE["unqueried_uri"]?>0/">��������� ��������</a><br /><?
						foreach ($MODULE_DATA["output"]["modules"] as $module) 
						{
							?><a href="<?=$EE["unqueried_uri"]?><?=$module["id"]?>/"><?=$module["comment"]?></a><br /><?
						}?>
					</div>
				</div>
			</div>
			<?
			break;

		case "privileges_form":
		?>
		<div class="load_animation"></div>

	
            <div class="panel-body">
                <div class="col-sm-6">

                <div class="col-sm-6" style="width: 500px; margin: -46px; margin-top: -15px;">
				<div class="panel panel-default">
	            	<div class="panel-body">


					<?
					if($MODULE_DATA["output"]["usergroup_id"] == NULL) 
					{
      					if ($MODULE_DATA["output"]["module_name"] != "��������� ��������") 
      					{
							?><h4>������ ���� ������ ������������� ��� "<?=$MODULE_DATA["output"]["module_name"]?>"</h4><?
						}
					else 
					{
				 		?><h4>������ ���� ������ ������������� ��� ������ "��������� ��������"</h4><?
					}
					?>

			      	<style>
			      		.loading img { margin: 0px; }
				.spoiler >  input + .box3 {
				display: none;
			}
			.spoiler >  input:checked + .box3 {
				display: block;
			}
			.spoiler > input[type="checkbox"] {
			cursor: pointer;
			}
			</style>



			      	<script>
			      		jQuery(document).ready(function($) {
							Select_Object(<?=$MODULE_OUTPUT['current_module_id']?>, <?=$MODULE_OUTPUT['module_id']?>);
						});			
					</script>

					<form action="<?=$EE["unqueried_uri"]?>" method="post">
						<input type="hidden" id="m_id" name="<?=$NODE_ID?>[give_privileges][module_id]" value="<?=$MODULE_DATA["output"]["module_id"]?>" class="form-control input-sm">
						<p>��������:<br />					
			
						<select name="<?=$NODE_ID?>[give_privileges][operation_name]" id="select_operation" size="1" onchange="" class="form-control input-sm">
						<option value="---"></option>			
					?>	
					<?
						foreach ($MODULE_DATA["output"]["operations"] as $operation) 
						{
							?><option value="<?=$operation["operation_name"]?>">
							<?php
							if ($operation["comment"])
							echo $operation["comment"];
							else
							echo $operation["operation_name"];
							?>
							</option><?
						}
						?>
						</select><span class="red loading hidden" id="loading"></span>
						</p>

<!-- 						<div class="input-group input-group-sm">
                            <span class="input-group-addon" style="border-radius: 1px; border-bottom-width: 0px;">��������</span>
                            <select name="<?=$NODE_ID?>[give_privileges][operation_name]" id="select_operation" size="1" class="form-control" style="border-radius: 1px; border-bottom-width: 0px;">
                                <?
                              foreach ($MODULE_DATA["output"]["operations"] as $operation) 
								{
									?><option value="<?=$operation["operation_name"]?>">
									<?php
									if ($operation["comment"])
									echo $operation["comment"];
									else
									echo $operation["operation_name"];
									?>
									</option><?
								}
                                ?>
                            </select>
                        </div>

                        <div class="input-group input-group-sm">
                            <span class="input-group-addon" style="border-radius: 1px; border-bottom-width: 0px;">������ �������������</span>
                            <select name="<?=$NODE_ID?>[give_privileges][operation_name]" id="select_operation" size="1" class="form-control" style="border-radius: 1px; border-bottom-width: 0px;">
                              <option value="-1">��� ������������</option>
						<option value="0">�������������������� ������������</option><?
					foreach ($MODULE_DATA["output"]["usergroups"] as $usergroup) {
						?><option value="<?=$usergroup["id"]?>"><?=$usergroup["comment"]?></option><?
					}
					?>
                            </select>
                        </div> -->




					<p>������ �������������:<br />
					<select name="<?=$NODE_ID?>[give_privileges][usergroup_id]" size="1" class="form-control">
						<option value="-1">��� ������������</option>
						<option value="0">�������������������� ������������</option><?
					foreach ($MODULE_DATA["output"]["usergroups"] as $usergroup) {
						?><option value="<?=$usergroup["id"]?>"><?=$usergroup["comment"]?></option><?
					}
					?>
					</select></p>
			
					<p class="hidden">�������:<br />
			
					<select name="<?=$NODE_ID?>[give_privileges][table_name]" id="select_element" class="select_element" size="1">
      					<option value="-1">��� ��������</option>
      					<?
					
					foreach ($MODULE_DATA["output"]["name_elements"] as $element) {
			
					/*	//if(isset($_POST['$NODE_ID[give_privileges][operation_name]'])){
						?><option value="<?=$element["id"]?>"><?=$element["name"]?>
						
						</option><?
						//}  */
					}
					?>
						</select>
			      <input type="text" id="element_search" class="element_search" size="10" title="������� �����" placeholder="������� ����� ��������" style="display: none; width: 265px; " />
			      <a href="javascript: void(0)" onclick="jQuery(this).parent().find('.element_search').show(); jQuery(this).parent().find('.select_element').hide(); jQuery(this).hide(); " class="action_edit" title="����� �� ���������� ��������" id="element_search_link" style="display: none; ">������</a>
			      </p>
			
			<p>ID �������� (entry):<br />
			<input type="text" class="form-control" id="entry_id" name="<?=$NODE_ID?>[give_privileges][entry_id]" size="3" disabled>
			<!-- <input type="text" id="entry_id" name="<?=$NODE_ID?>[give_privileges_privileges][entry_id]" size="3" disabled> -->
			<input type="checkbox" name="<?=$NODE_ID?>[give_privileges][all_entries]" id="for_all_entries" onclick="if(!this.checked) document.getElementById('entry_id').removeAttribute('disabled'); else document.getElementById('entry_id').setAttribute('disabled', '1');" checked>
			��� ���� ���������</p>
			
			<p>��������:<br />
			<select name="<?=$NODE_ID?>[give_privileges][is_allowed]" size="1" class="form-control">
			<option value="1">���������</option>
			<option value="0">���������</option>
			</select></p>
			
			<button type="submit" class="btn btn-nsau">������ �����</button><?php } ?>


		<!-- 	<? $total=count($MODULE_DATA["output"]["privileges"]);  echo $total; ?>
			<h2>�������� �����</h2>
			
			<ul>
			<? 
				if (isset($MODULE_DATA["output"]["privileges"]))
					foreach ($MODULE_DATA["output"]["privileges"] as $privilege) 
				{
					?><li><? 
					if (!$privilege["is_allowed"]) { ?>���������<? } else { ?>���������<? }
						if ($privilege["usergroup"] == "-1") { ?> ���� �������������<? }
						elseif (!$privilege["usergroup"]) { ?> �������������������� �������������<? }
						else { ?> ������ ������������� "<?=$privilege["usergroup"]?>" <? } ?>
							�������� "<?=$privilege["operation"]?>"
						
								<? $test++ ;if ($privilege["entry_id"] == "-1") { ?> �� ���� ���������<? }
								else if($privilege["entry"]) { ?> �� �������� "<?=$privilege["entry"]["name"]?>"<? }
								else { ?> �� �������� <?=$privilege["entry_id"]?><? } ?>. 
				
				<a href="?delete_name=<?=$privilege["operation_code"]?>&delete_module=<?=$privilege["module_id"]?>&delete_usergroup=<?=$privilege["usergroup_id"]?>&delete_entry=<?=$privilege["entry_id"]?>&delete_allowed=<?=$privilege["is_allowed"]?>" onclick="if(!confirm('�� ������������� ������ ������� ��� ����������?')) return false;"><img src="/themes/images/delete.png"></a> </li>
				<? } ?>
			</ul> -->

			
			</div>

               </div>



				<div class="panel panel-default">
	            	<div class="panel-body">
	            	<h4>������ �������� ����</h4>
	            	<p><select class="form-control show_operations" name="filter_operation"></select></p>
	            	<button type="button" class="btn btn-nsau filter">��������� ������</button>
	                </div>
	            </div>


	        </div><br>
	            </div>

	        </div>





			<style type="text/css">
				.spoiler >  input + .box {
				display: none;
			}
			.spoiler >  input:checked + .box {
				display: block;
			}
			.spoiler > input[type="checkbox"] {
			cursor: pointer;
			}
			</style>

			<br>
			<h4>�������� �����</h4>
			<div class="table-responsive">
			  <table class="table">
			    <thead>
			      <tr>
			        <th>��������</th><th>������ �������������</th><th>�������</th><th>��������</th><th></th>
			      </tr>
			    </thead>
			    <tbody class="rules_tbody">
			        <?
					foreach ($MODULE_DATA["output"]["privileges"] as $privilege)
					{
						if ($privilege['is_allowed']==1) 
						{
							$class="success";
						}
						else
						{
							$class="danger";
						}

						echo "<tr>";
						echo "<td class='".$class."'>".$privilege['operation']."</td>";

						if ($privilege['usergroup']==-1 && $privilege['users']==0) 
						{
							echo "<td class='".$class."'>��� ������������</td>";
						}
						elseif($privilege['users']!=0)
						{
							echo "<td class='".$class."'>"; 
								?>
 								<div class="spoiler">
     								<input type="checkbox"> �������� <?=count($privilege['users']);?> �������������
 									<div class="box">
       									<?
       										$br = 0;
											foreach ($privilege["users"] as $data) 
											{
												?>
												<div class='leader_frame' id=<?=$data["id"]?>>
													<a href=<?="/people/".$data["id"]?> class='thumbnail leader_th' id=<?=$data["id"]?>>
														<?
														if (empty($data["photo"]))
														{
															?>
															<img class='pleader1' src="/images/people/no_photo.jpg">
															<?
														}
														else
														{
															?>
															<img class='pleader1' src=<?="/images/people/".$data["photo"]?>>
															<?
														}
														?>
													</a>
													<br>
													<div class='leader_name'>
														<?=$data["last_name"]."<br>".$data["name"]."<br>".$data["patronymic"]."<br>"?>
													</div>
												</div>
												<?
												if ($br <= 2) 
												{
													$br++;
												}
												else
												{
													echo "<br>";
													$br = 0;
												}
											}
       									?>
  									</div>
								</div>

								<?




							echo "</td>";
						}
						else
						{
							echo "<td class='".$class."'>".$privilege['usergroup']."</td>";
						}

						if ($privilege['entry_id']==-1) 
						{
							echo "<td class='".$class."'>��� ��������</td>";
						}
						else
						{
							echo "<td class='".$class."'>".$privilege['entry_id']."</td>";
						}

						if ($privilege['is_allowed']==1) 
						{
							echo "<td class='success'>���������</td>";
						}
						else
						{
							echo "<td class='danger'>���������</td>";
						}
						?>
						<td class=<?=$class?> align="center">
							<a href="?delete_name=<?=$privilege["operation_code"]?>&delete_module=<?=$privilege["module_id"]?>&delete_usergroup=<?=$privilege["usergroup_id"]?>&delete_entry=<?=$privilege["entry_id"]?>&delete_allowed=<?=$privilege["is_allowed"]?>" onclick="if(!confirm('�� ������������� ������ ������� ��� ����������?')) return false;"><span class="glyphicon glyphicon-remove"></span>
							</a>
						</td>
						<?
						echo "</tr>";
					}
			        ?>
			    </tbody>
			  </table>
			</div>


			<?
			break;
    case "ajax_get_elements":
    ?>
    <select name="<?=$NODE_ID?>[give_privileges][table_name]" id="select_element" class="select_element" size="1">
      <?php /* ?><option value="-1">��� ��������</option><?php */ ?>
      <?
			foreach ($MODULE_DATA["output"]["elements"] as $element) {

				//if(isset($_POST['$NODE_ID[give_privileges][operation_name]'])){
				?><option value="<?=$element["id"]?>"><?=$element["name"]?>

				</option><?
				//}
			}
			?>
			</select>
    <?php
    break;

		case "message":

			break;

		case "single_rules":
		{

		// echo "����;ID;������;���������;�������;���;��������;<br>";
		// foreach ($MODULE_OUTPUT["testt"] as $key => $value) 
		// {
		// 	echo $value['library_card'].";".$value['id'].";".$value['group'].";".$value['fac'].";".$value['last_name'].";".$value['name'].";".$value['patronymic'].";<br>";
		// }

		/*reporting*/
		?>
			<link rel="stylesheet" href="/themes/styles/bootstrap-3.3.7/css/bootstrap.css">
			<link rel="stylesheet" href="/themes/styles/bootstrap-3.3.7/css/bootstrap-theme.min.css">
			<script src="/themes/styles/bootstrap-3.3.7/js/bootstrap.min.js"></script>
			<link rel="stylesheet" href="/themes/styles/nsau_struct.css">

			<!-- fix top bar -->
			<style type="text/css">
			.menu_background {
			    -webkit-box-sizing: content-box!important;
			    -moz-box-sizing: content-box!important;
			     box-sizing: content-box!important; 
			}
			#footer {
				padding-top: 12px;
			}
			</style>

			<div class="alert alert-danger" role="alert" style="display: none;"></div>
			<div class="alert alert-success" role="alert" style="display: none;"></div>
			<div class="load_animation"></div>


		<?
        if ($MODULE_OUTPUT["bad_message"])
        {
            ?>
                <div class="alert alert-danger" role="alert"><?=$MODULE_OUTPUT["bad_message"]?></div>
                <script type="text/javascript">
                    /*autohide system info*/
                    setTimeout(function() {$('.alert-danger').fadeOut('fast');}, 3000); 
                </script>
            <?
        }

        if ($MODULE_OUTPUT["good_message"])
        {
            ?>
                <div class="alert alert-success" role="alert"><?=$MODULE_OUTPUT["good_message"]?></div>
                <script type="text/javascript">
                    /*autohide system info*/
                    setTimeout(function() {$('.alert-success').fadeOut('fast');}, 3000); 
                </script>
            <?
        }

			?>


			<div class="panel panel-default">
                <div class="panel-body" style="padding: 2px; margin-bottom: 10px; margin-top: 10px;">
                    <div class="col-sm-6">
                        <h4>������ ���� ������ ������������</h4>

            <style type="text/css">
				.spoiler >  input + .box2 {
				display: none;
			}
			.spoiler >  input:checked + .box2 {
				display: block;
			}
			.spoiler > input[type="checkbox"] {
			cursor: pointer;
			}
			</style>
					<div class="spoiler">

                         <input type="checkbox"> ��������
 									<div class="box2">
                        <!-- element id -->
                        <div class="input-group input-group-sm">
                            <span class="input-group-addon" style="border-bottom-width: 0px;">Id ��������<span class="obligatorily">*</span></span>
                            <input class="form-control input-sm element_id" type="text" style="border-radius: 1px; border-bottom-width: 0px;">
                            <span class="input-group-addon for_all" style="border-radius: 0px; cursor: pointer; border-bottom-width: 0px;">��� ���� ���������</span>
                        </div>
                        <!-- module -->
                        <div class="input-group input-group-sm">
                            <span class="input-group-addon" style="border-radius: 1px; border-bottom-width: 0px;">������<span class="obligatorily">*</span></span>
                            <select class="form-control module" style="border-radius: 1px; border-bottom-width: 0px;">
                                <?
                                foreach ($MODULE_OUTPUT["modules"] as $key) 
                                {
                                	CF::Debug($key);
                                	?><option value=<?=$key['id']?>><?="[id: ".$key['id']."] ".$key['comment']?></option><?
                                }
                                ?>
                            </select>
                        </div>
                        <!-- operation -->
                        <div class="input-group input-group-sm">
                            <span class="input-group-addon" style="border-radius: 1px; border-bottom-width: 0px;">��������<span class="obligatorily">*</span></span>
                            <select class="form-control operation" style="border-radius: 1px; border-bottom-width: 0px;">
                            </select>
                        </div>
                        <!-- user id -->
                        <div class="input-group input-group-sm">
                            <span class="input-group-addon" style="border-bottom-width: 0px;">Id ������������<span class="obligatorily">*</span></span>
                            <input class="form-control input-sm str_leader" placeholder="������� �������" type="text" list="team_list" style="border-radius: 1px; border-bottom-width: 0px;">
                            <span class="input-group-addon user_info" style="border-radius: 0px; cursor: pointer; border-bottom-width: 0px;">��� ���?</span>
                             <datalist id="team_list"></datalist>
                        </div>
                        <!-- action -->
                        <div class="input-group input-group-sm">
                            <span class="input-group-addon">��������<span class="obligatorily">*</span></span>
                            <select class="form-control action">
                            	<option value="1">���������</option>
            					<option value="0">���������</option>
                            </select>
                        </div><br>
                        <button type="button" class="btn btn-nsau add_rule">������ �����</button>
                        <button type="button" class="btn btn-success check_user">�������� ����� ��������� �����</button>
                    </div>
                    <!-- <div class="prev_leader_list"></div> -->
                    <ul class="dropdown-menu prev_leader_list" style="display: none; position: static;"></ul>
                    </div>
                    </div>

                    <style type="text/css">
                    	.prev_leader_list {
                    		padding-left: 25px;
                    	}
                    </style>
                </div>

            </div>
			<?
		}
		break;
	}
}

?>
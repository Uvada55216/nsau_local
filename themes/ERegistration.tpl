<?php
switch ($MODULE_OUTPUT["mode"])
{
	case 'teachers_registration':
	{
		?>
 		<script src="/themes/styles/bootstrap/js/jquery.min.js"></script> 
        <script src="/themes/styles/bootstrap/js/bootstrap.min.js">
        </script>
        
        <style type="text/css">
        .menu_background {
            -webkit-box-sizing: content-box!important;
            -moz-box-sizing: content-box!important;
             box-sizing: content-box!important; 
         }

         .modal {
            padding-top: 1%;
         }

        #nofooter {
            top: 0px!important;
            margin-top: 0px!important;
        }
        </style>

        <div class="panel panel-default">
            <div class="panel-heading">
              <button type="button" class="btn btn btn-xs spoiler-trigger" data-toggle="collapse"><h5>���� �� ������� ��� ��������</h5></button>
            </div>
            <div class="panel-collapse collapse out">
              <div class="panel-body">
                <p>��������� ���� ���� �������� ������ � ������ ��� ������� � ������� �������� �������� �� ������� � �������� ���������������� ����������.
                    <br>���������� <strong>���� ���� ��������</strong> ������ � ������ ��� ������� � ������� �������� �������� �� ������� � <strong>������ �� ���������� ������-�������������� ������</strong> (��. ��������, 155, ��-319)
                </p>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <button type="button" class="btn btn btn-xs spoiler-trigger" data-toggle="collapse"><h5>���� �� �������������, ��������� ��� ��������� ������� (������-��������������� ��������)</h5> </button>
            </div>
            <div class="panel-collapse collapse out">
              <div class="panel-body">
              	<?
              	if ($MODULE_OUTPUT["options"]["check_registration_enable"]) 
              	{
              		?>
	                <p> ������� ����������� �������� � ���� ��������� ����:
	                    <ul>
	                        <li>���������� ��������������� ������.</li>
	                        <li>�������� ������������ ��������� ������, ������������� �������� (�������� ������ ���������� �������� ��� ������������� ����� �� �������������).</li>
	                    </ul>
	                    ����� �������� ����������� �� �������� ����, ��������� ��� �����������, ������������ �������� ��������������� ������.
	                </p>
	                <button class="btn btn-nsau btn-md test" type="button">��������� ������</button>
	                <?
            	}
            	else
            	{
            		?>����������� � ������ ������ ����������.<?
            	}
            	?>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <button class="btn btn btn-xs spoiler-trigger" data-toggle="collapse"><h5>���� �� ��������� ����<h5></button>
            </div>
            <div class="panel-collapse collapse out">
              <div class="panel-body">
                <p>��� ����������� ��������� ����������� �� ��������������� ������� ���� ���������� ������� ����������� �����, ��������������� ��������� ������:</p>
                <ul>
				<!--<li>
		                <strong>�������������</strong> (<a href="http://nsau.edu.ru/file/73431/">�����</a> | <a href="http://nsau.edu.ru/file/73471/">�������</a>)
		            </li>
		            <li>
		                <strong>��������</strong> (<a href="http://nsau.edu.ru/file/73421/">�����</a> | <a href="http://nsau.edu.ru/file/73461/">�������</a>)
		            </li>-->
                    <li>
                        ��������� (<a href="https://nsau.edu.ru/file/1300231/">�����</a> | <a href="https://nsau.edu.ru/file/1300251/">�������</a>)
                    </li>
                </ul>
              </div>
            </div>
          </div>

        <script type="text/javascript">
            $(".spoiler-trigger").click(function() 
            {
                $(this).parent().next().collapse('toggle');
            });
        </script>

        <?
        if ($MODULE_OUTPUT["options"]["check_registration_enable"]) 
        {
	        if (isset($MODULE_OUTPUT["summary_info"]) && !empty($MODULE_OUTPUT["summary_info"])) 
	        {
	            ?>
	            <div id="modal_summary_info" class="modal fade">
	                <div class="modal-dialog" style="width: 400px;">
	                    <div class="modal-content">
	                        <div class="modal-header">
	                            <h4 class="modal-title">������ �� ����������� ����������</h4>
	                        </div>
	                        <div class="modal-body" style="text-align: left; font-size: 12px;">
	                            <div id="new_user_data">
	                                ������ ��� ����� � ������� ������ �� ������� nsau.edu.ru:<br><br>
	                                ���: <b><?=$MODULE_OUTPUT["summary_info"]["fio"]?></b> <br>
	                                E-mail: <b><?=$MODULE_OUTPUT["summary_info"]["email"]?></b>  <br>
	                                �����: <b><?=$MODULE_OUTPUT["summary_info"]["username"]?></b> <br>
	                                ������: <b><?=$MODULE_OUTPUT["summary_info"]["password"]?> </b> <br>  
	                            </div>
	                        </div>
	                        <div class="modal-footer">
	                            <button type="button" class="btn btn-nsau-grey print_new_user">�����������</button>
	                            <button type="button" class="btn btn-nsau close_summary" >Ok</button>
	                        </div>
	                        </form>
	                    </div>
	                </div>
	            </div>
	            <script type="text/javascript">
	                 $("#modal_summary_info").modal('show');
	            </script>
	            <?
	        }
	        ?>

			<style type="text/css">
			  #nofooter {
			    top: 50px!important;
			    margin-top: 0px!important;
			}
			</style>

	        <form method="POST">
	            <div id="teach_register" class="modal fade">
	            <div class="modal-dialog" style="width: 1000px;">
	                <div class="modal-content" id="teach_register_content">
	                    <div class="modal-header">
	                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                        <h4 class="modal-title">����������� ������������� ����</h4>
	                    </div>

	                    <div class="modal-body">
	                        <div class="row"> 
	                            <div class="col-sm-6">
	                                

	                            <!-- <div class="er_fio" data-original-title="��� ������ ���� ������ ���� ��������" data-toggle="tooltip" data-placement="left" aria-describedby="tooltip836805"></div> -->
	                            <div class="input-group input-group-sm">
	                                <div class="er_fam" data-original-title="<?=$MODULE_OUTPUT["options"]["er_fam"]?>" data-toggle="tooltip" data-placement="left"></div>
	                                <span class="input-group-addon" style="border-bottom-width: 0px; min-width:125px; text-align:left;">�������<span class="obligatorily"> *</span></span>
	                                <input  class="form-control input-sm name_f" type="text" name="name_f" style="border-bottom-width: 0px!important;" value="<?=$MODULE_OUTPUT["reenter"]["name_f"]?>">   
	                            </div>

	                           
	                            <div class="input-group input-group-sm">
	                                <div class="er_name" data-original-title="<?=$MODULE_OUTPUT["options"]["er_name"]?>" data-toggle="tooltip" data-placement="left" ></div>
	                                <span class="input-group-addon" style="border-bottom-width: 0px; min-width:125px; text-align:left;">���<span class="obligatorily"> *</span></span>
	                                <input class="form-control input-sm name_n" type="text" name="name_n" style="border-bottom-width: 0px!important;" value="<?=$MODULE_OUTPUT["reenter"]["name_n"]?>">
	                            </div>

	                            <div class="input-group input-group-sm">
	                                <div class="er_otc" data-original-title="<?=$MODULE_OUTPUT["options"]["er_otc"]?>" data-toggle="tooltip" data-placement="left" aria-describedby="tooltip836805"></div>
	                                <span class="input-group-addon" style="border-bottom-width: 0px; min-width:125px; text-align:left;">��������<span class="obligatorily"> *</span></span>
	                                <input class="form-control input-sm name_o" type="text" name="name_o" style="border-bottom-width: 0px!important;" value="<?=$MODULE_OUTPUT["reenter"]["name_o"]?>">
	                            </div>

	                            <div class="input-group input-group-sm">
	                                <div class="er_pol" data-original-title="<?=$MODULE_OUTPUT["options"]["er_pol"]?>" data-toggle="tooltip" data-placement="left" aria-describedby="tooltip836805"></div>
	                                <span class="input-group-addon" style="min-width:125px; text-align:left; border-bottom-width: 0px;">���<span class="obligatorily"> *</span></span>
	                                <select name="gender" class="form-control gender" style="border-bottom-width: 0px!important;">
	                                    <option value="-1"></option>
	                                    <?
	                                        if ($MODULE_OUTPUT["reenter"]["gender"] == 1) 
	                                        {
	                                            ?><option selected value="1">�������</option><?
	                                        }
	                                        else
	                                        {
	                                            ?><option value="1">�������</option><?
	                                        }

	                                        if ($MODULE_OUTPUT["reenter"]["gender"] == 2) 
	                                        {
	                                            ?><option selected value="2">�������</option><?
	                                        }
	                                        else
	                                        {
	                                            ?><option value="2">�������</option><?
	                                        }
	                                    ?>
	                                    }
	                                    ?>
	                                </select>
	                            </div>

	                            <div class="input-group input-group-sm">
	                                <div class="er_dolj" data-original-title="<?=$MODULE_OUTPUT["options"]["er_dolj"]?>" data-toggle="tooltip" data-placement="left" aria-describedby="tooltip836805"></div>
	                                <span class="input-group-addon" style="border-radius: 1px; border-bottom-width: 0px; min-width:125px; text-align:left;">���������<span class="obligatorily"> *</span></span>
	                                <select name="post" class="form-control post" style="border-radius: 1px; border-bottom-width: 0px!important;">
	                                    <option value="-1"></option>
	                                    <?foreach($MODULE_OUTPUT["teachers_post"] as $id => $val)
	                                    {
	                                        if ($id == $MODULE_OUTPUT["reenter"]["post"]) 
	                                        {
	                                            ?><option selected value="<?=$id?>"><?=$val["name"]?></option><?
	                                        }
	                                        else
	                                        {
	                                            ?><option value="<?=$id?>"><?=$val["name"]?></option><?
	                                        }
	                                    }
	                                    ?>
	                                </select>
	                            </div>
	                            <?
	                            //CF::Debug($MODULE_OUTPUT["departments"]);
	                            ?>
	                            <div class="input-group input-group-sm">
	                                <div class="er_kaf" data-original-title="<?=$MODULE_OUTPUT["options"]["er_kaf"]?>" data-toggle="tooltip" data-placement="left" aria-describedby="tooltip836805"></div>
	                                <span class="input-group-addon" style="border-radius: 1px; border-bottom-width: 0px; min-width:125px; text-align:left;">�������<span class="obligatorily"> *</span></span>
	                                <select name="department" class="form-control department" style="border-radius: 1px; border-bottom-width: 0px!important;">
	                                    <option value="-1"></option>
	                                    <?
	                                foreach($MODULE_OUTPUT["departments"] as $fac => $data)
	                                {   
	                                    echo "<optgroup label='".$fac."'>";
	                                    foreach($data as $id => $val)
	                                    {
	                                        if (!empty($MODULE_OUTPUT["reenter"]["department"]) && $id == $MODULE_OUTPUT["reenter"]["department"]) 
	                                        {
	                                            ?><option selected value="<?=$id?>"><?=$val["name"]?></option><?
	                                        }
	                                        else
	                                        {
	                                            ?><option value="<?=$id?>"><?=$val["name"]?></option><?
	                                        }
	                                    }
	                                    ?></optgroup><?
	                                }
	                                    ?>
	                                </select>
	                            </div>

	                            <div class="input-group input-group-sm">
	                                <div></div>
	                                <span class="input-group-addon" style="border-radius: 1px; border-bottom-width: 0px; min-width:125px; text-align:left;">������ �������</span>
	                                <select name="degree" class="form-control degree" style="border-radius: 1px; border-bottom-width: 0px!important;">
	                                    <option value="-1"></option>
	                                    <?foreach($MODULE_OUTPUT["teachers_degree"] as $id => $val)
	                                    {
	                                        if ($id == $MODULE_OUTPUT["reenter"]["degree"]) 
	                                        {
	                                            ?><option selected value="<?=$id?>"><?=$val["name"]?></option><?
	                                        }
	                                        else
	                                        {
	                                            ?><option value="<?=$id?>"><?=$val["name"]?></option><?
	                                        }
	                                    }
	                                    ?>
	                                </select>
	                            </div>

	                            <div class="input-group input-group-sm">
	                                <div></div>
	                                <span class="input-group-addon" style="border-radius: 1px; border-bottom-width: 0px; min-width:125px; text-align:left;">������</span>
	                                <select name="rank" class="form-control rank" style="border-radius: 1px; border-bottom-width: 0px!important;">
	                                    <option value="-1"></option>
	                                    <?foreach($MODULE_OUTPUT["teachers_rank"] as $id => $val)
	                                    {
	                                        if (!empty($MODULE_OUTPUT["reenter"]["rank"]) && $id == $MODULE_OUTPUT["reenter"]["rank"]) 
	                                        {
	                                            ?><option selected value="<?=$id?>"><?=$val["name"]?></option><?
	                                        }
	                                        else
	                                        {
	                                            ?><option value="<?=$id?>"><?=$val["name"]?></option><?
	                                        }
	                                    }
	                                    ?>
	                                </select>
	                            </div>

	                            <div class="input-group input-group-sm">
	                                <div></div>
	                                <span class="input-group-addon" style="border-radius: 1px; min-width:125px; text-align:left;">������� �����</span>
	                                 <a class="btn btn-nsau-grey btn-block select_curator_group" style="border-radius: 0px!important;">������� ���������� ������ <div class="selected_groups"></div></a>
	                            </div>
	                        </div>

	                         
	                            <div class="input-group input-group-sm" style="border-bottom-width: 0px;">
	                                <div class="alert alert-info mail_create">
	                                    �� ������ ������������ ��� ������������ �������� ���� (��� �� nsau.edu.ru, ��� � �� ����� ������ �������� �������) ��� ��:
	                                    <div class="checkbox">
	                                        <?
	                                        if (isset($MODULE_OUTPUT["reenter"]["create_new_mail"])) 
	                                        {
	                                            ?>
	                                            <label><input checked type="checkbox" name="create_new_mail" class="create_nsau_mail" value="">������� �������� ���� �� @nsau.edu.ru</label>
	                                            <?
	                                        }
	                                        else
	                                        {
	                                            ?>
	                                            <label><input type="checkbox" name="create_new_mail" class="create_nsau_mail" value="">������� �������� ���� �� @nsau.edu.ru</label>
	                                            <?                                     
	                                        }
	                                        ?>
	                                    </div>
	                                </div>
	                            </div>


	                        <div class="col-sm-6">
	                            <div class="input-group input-group-sm">
	                                 <div class="er_emailallready" data-original-title="<?=$MODULE_OUTPUT["options"]["er_emailallready"]?>" data-toggle="tooltip" data-placement="left" aria-describedby="tooltip836805"></div>
	                                 <div class="er_email" data-original-title="<?=$MODULE_OUTPUT["options"]["er_email"]?>" data-toggle="tooltip" data-placement="left" aria-describedby="tooltip836805"></div>
	                                 <div class="er_emailsobaka" data-original-title="<?=$MODULE_OUTPUT["options"]["er_emailsobaka"]?>" data-toggle="tooltip" data-placement="left" aria-describedby="tooltip836805"></div>
	                                 <div class="er_email@" data-original-title="<?=$MODULE_OUTPUT["options"]["er_email@"]?>" data-toggle="tooltip" data-placement="left" aria-describedby="tooltip836805"></div>
	                                 <div class="er_emailsp" data-original-title="<?=$MODULE_OUTPUT["options"]["er_emailsp"]?>" data-toggle="tooltip" data-placement="left" aria-describedby="tooltip836805"></div>

	                                <span class="input-group-addon" style="border-bottom-width: 0px; min-width:125px; text-align:left;">E-mail<span class="obligatorily"> *</span></span>
	                                <input class="form-control input-sm email" type="text" name="email" style="border-radius: 1px; border-bottom-width: 0px!important;" placeholder="��������: example@mail.ru" value="<?=$MODULE_OUTPUT["reenter"]["email"]?>">
	                                <span class="input-group-addon nsau_domain" style="display: none;" style="border-bottom-width: 0px!important; min-width:250px; text-align:left;">@nsau.edu.ru</span>
	                               
	                            </div>
	                            <script src="/scripts/jquery/jquery.maskedinput.js" type="text/javascript"></script>

	                            <div class="input-group input-group-sm">
	                                <div class="er_pass4" data-original-title="<?=$MODULE_OUTPUT["options"]["er_pass4"]?>" data-toggle="tooltip" data-placement="left" aria-describedby="tooltip836805"></div>
	                                <div class="er_pass" data-original-title="<?=$MODULE_OUTPUT["options"]["er_pass"]?>" data-toggle="tooltip" data-placement="left" aria-describedby="tooltip836805"></div>
	                                <span class="input-group-addon" style="border-bottom-width: 0px; min-width:125px; text-align:left;">������<span class="obligatorily"> *</span></span>
	                                <input class="form-control input-sm password" type="password" name="password" style="border-radius: 1px; border-bottom-width: 0px!important;" value="<?=$MODULE_OUTPUT["reenter"]["password"]?>">
	                            </div>

	                            <div class="input-group input-group-sm">
	                                <div class="er_repass" data-original-title="<?=$MODULE_OUTPUT["options"]["er_repass"]?>" data-toggle="tooltip" data-placement="left" aria-describedby="tooltip836805"></div>
	                                <span class="input-group-addon" style="border-bottom-width: 1px; min-width:125px; text-align:left;">������ ��� ���<span class="obligatorily"> *</span></span>
	                                <input class="form-control input-sm repassword" type="password" name="repassword" style="border-radius: 1px; border-bottom-width: 1px;">
	                            </div>

	                            <div class="input-group input-group-sm">
	                                <!--                             
	                                <div class="checkbox">
	                                  <label><input type="checkbox" value="">������������� �� ����������</label>
	                                </div> 
	                                -->
	                                <div class="checkbox">
	                                <div class="er_accept" data-original-title="<?=$MODULE_OUTPUT["options"]["er_accept"]?>" data-toggle="tooltip" data-placement="left" aria-describedby="tooltip836805"></div>
	                                  <label><input type="checkbox" class="check_accept" name="accept_send" value="">�������� �� ��������� ������������ ������</label>
	                                </div>
	                            </div>

	                            <?
	                            if ($MODULE_OUTPUT["options"]["check_captcha"]) 
	                            {
		                            ?>
		                            <div class="input-group input-group-sm">
		                                <div class="g-recaptcha" data-sitekey="6LevLxMUAAAAAKcz_AZOCeKpx92QXDBVD79wcWd5"></div>
		                                <script src='https://www.google.com/recaptcha/api.js'></script>
		                                <label class="er_capth" data-original-title="<?=$MODULE_OUTPUT["options"]["er_capth"]?>" data-toggle="tooltip" data-placement="right" aria-describedby="tooltip836805"></label>
		                            </div>
		                            <?
	                        	}
								?>
	                        </div>
	                                            </div>
	                        <div class="modal-footer register_footer">
	                            <button type="button" class="btn btn-default" data-dismiss="modal">������</button>
	                            <button type="submit" class="btn btn-nsau send_register" name="send_register" disabled>��������� ������</button>
	                        </div>
	                    </div>

                        <?
                        /*errors*/
                        if ($MODULE_OUTPUT["messages"]["bad"])
                        {
                            $array = array();

                            ?>
                            <script type="text/javascript">
                                $("#teach_register").modal('show');
                            <?

                            foreach ($MODULE_OUTPUT["messages"]["bad"] as $elem)
                            {
                            	?>
                                	$(".er_<?=$elem?>").tooltip('show');
                                <?
                            }
                        }
                        ?>
                        </script>
	                    
	                    </div>
	                </div>
	            </div>
	            <div id="modal_group_curator" class="modal fade">
	                <div class="modal-dialog" style="width: 400px;">
	                    <div class="modal-content">
	                        <div class="modal-header">
	                            <h4 class="modal-title">����� ����� ��� �����������</h4>
	                        </div>
	                        <div class="modal-body" style="text-align: left; font-size: 12px;">
	                            ��� ������ ������ ����� ������, ������� Ctrl � �������� ������ ������ �� ������
	                            <select name="kurator[]" class="form-control kurator" style="border-radius: 0px;" multiple size="20">
	                                <option value="-1"></option>
	                                <?
	                                if (isset($MODULE_OUTPUT["reenter"]["kurator"])) 
	                                {
	                                    array_unshift($MODULE_OUTPUT["reenter"]["kurator"], "---");
	                                    foreach($MODULE_OUTPUT["groups"] as $id => $val)
	                                    {
	                                        $result = array_search($id, $MODULE_OUTPUT["reenter"]["kurator"]); 
	                                        if ($result) 
	                                        {
	                                           ?><option selected value="<?=$id?>"><?=$val["name"]?></option><?
	                                        }
	                                        else
	                                        {
	                                            ?><option value="<?=$id?>"><?=$val["name"]?></option><?
	                                        }
	                                        unset($result);
	                                    }                                 
	                                }
	                                else
	                                {
	                                    foreach($MODULE_OUTPUT["groups"] as $id => $val)
	                                    {
	                                        ?><option value="<?=$id?>"><?=$val["name"]?></option><?
	                                    }
	                                }
	                                ?>
	                            </select>
	                        </div>
	                        <div class="modal-footer">
	                            <button type="button" class="btn btn-nsau accept_groups_curator" >Ok</button>
	                        </div>
	                        </form>
	                    </div>
	                </div>
	            </div>
	        </form>

	        <div id="modal_accept" class="modal fade">
	            <div class="modal-dialog" style="width: 800px;">
	                <div class="modal-content">
	                    <div class="modal-header">
	                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                        <h4 class="modal-title" style="text-align: center;">�������� <br>�� ��������� ������������ ������ ���, ����������������� � ����� �� ������������� ��� � �������� �� ������� �����</h4>
	                    </div>
	                    <div class="modal-body" style="text-align: left; font-size: 12px;">
	                   		<?=$MODULE_OUTPUT["options"]["personal_data_accept_text"]?>
	                    </div>
	                    <div class="modal-footer">
	                        <button type="button" class="btn btn-default" data-dismiss="modal">���������</button>
	                        <button type="button" class="btn btn-nsau accept" >�������</button>
	                    </div>
	                    </form>
	                </div>
	            </div>
	        </div>
			<?
		}
	}
	break;

    case 'teachers_registration_moderate':
    {
    	if ($MODULE_OUTPUT["options"]["check_moderation_enable"])
    	{
	        ?>
	        <h4>��������� ������ ����������� �������������</h4>
	        <?
	        if ($MODULE_OUTPUT['waitings_teachers']) 
	        {
	            ?><div class="alert alert-info console load_animation">
	            ���������� ��������� ��������� �������������: <b><?=count($MODULE_OUTPUT['waitings_teachers'])?></b>
	            </div><?
	        }
	        ?>
	        <style type="text/css">
	       	#nofooter {
            top: 0px!important;
            margin-top: 0px!important;
        	}
	        </style>

	        <table class="table">
	            <thead>
	                <tr>
	                    <th width="20%">���</th>
	                    <th width="30%">�������</th>
	                    <th>���� �����������</th>
	                    <th>Email</th>
	                    <th></th>
	                </tr>
	            </thead>
	            
	            <tbody>
	                <?
	                foreach ($MODULE_OUTPUT['waitings_teachers'] as $key => $value) 
	                {
	                    ?>
	                    <tr id=<?="user_".$value['id']?>>
	                        <td><a href=<?="/people/".$key?>><?=$value['displayed_name']?></a></td>
	                        <td><?=$value['kaf']?></td>
	                        <td><?=$value['create_time']?></td>
	                        <td><?=$value['email']?></td>
	                        <td>
	                            <a class="btn btn-nsau teach_accept" id=<?=$value['id']?>> �������</a>
	                            <a class="btn btn-default teach_delete" id=<?=$value['id']?>>�������</a>
	                        </td>
	                    </tr>
	                    <?
	                }
	                ?>
	            </tbody>
	          </table>
	        <?
    	}
    	else
    	{
    		?>��������� ������ �������� ���������.<?
    	}
    }
    break;

}
?>
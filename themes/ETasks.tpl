<?php
// version: 1.6
// date: 2013-11-18
$MODULE_MESSAGES = array(
	100 => "�������� ���������������.",
	101 => "������ ������ � ����.",
	102 => "�������� � ����� ������� ��� ������� � ����.",
	103 => "�� ��� ���� ���������.",
	104 => "���� ������ �������.",
	105 => "������ �� ������ �������� ��� ����������.",
	106 => "������ �����, ���������� �����."
);
$m_types = array("good", "bad");
foreach($m_types as $type) {
	if(!empty($MODULE_OUTPUT["messages"][$type])) {
		foreach ($MODULE_OUTPUT["messages"][$type] as $elem) {?>
			<li><p style="color: <?=($type=="bad") ? "red" : "green"?>"><?=$MODULE_MESSAGES[$elem]?></p></li>
		<?}
	}
}
if(!isset($MODULE_OUTPUT['mode'])) {$MODULE_OUTPUT['mode'] = 'default';}
	switch($MODULE_OUTPUT['mode']) {
		case "task_count": {
			if(isset($MODULE_OUTPUT['tasks_count'])) {
				header("Content-type: text/html; charset=windows-1251"); ?>
	<div class="tasks_count">
		<div class="tasks_count_title"><a href="http://rm.nsau.edu.ru">������</a></div>
		<p class="new_tasks">�����: <span><?=$MODULE_OUTPUT['tasks_count']['new']?></span></p>
		<p class="new_current">�� ����������: <span><?=$MODULE_OUTPUT['tasks_count']['current']?></span></p>
		<p class="new_tobechecked">�� ��������: <span><?=$MODULE_OUTPUT['tasks_count']['tobechecked']?></span></p>
		<script>jQuery(document).ready(function($) {TaskCount(<?=$MODULE_OUTPUT['module_id']?>);});</script>
	</div>			
<?php		}
		}
		break;
		case "user_task_count": {
			if(isset($MODULE_OUTPUT['tasks_count'])) {
				header('Content-Type: text/xml,  charset=windows-1251');
				$dom = new DOMDocument();
				$user_tasks = $dom->createElement('user_tasks');
				$dom->appendChild($user_tasks);
				
				$new_tasks = $dom->createElement('new_tasks');
				$new_count = $dom->createTextNode($MODULE_OUTPUT['tasks_count']['new']);
				$new_tasks->appendChild($new_count);
				
				$current_tasks = $dom->createElement('current_tasks');
				$current_count = $dom->createTextNode($MODULE_OUTPUT['tasks_count']['current']);
				$current_tasks->appendChild($current_count);
				
				$tobechecked_tasks = $dom->createElement('tobechecked_tasks');
				$tobechecked_count = $dom->createTextNode($MODULE_OUTPUT['tasks_count']['tobechecked']);
				$tobechecked_tasks->appendChild($tobechecked_count);
				
				$user_tasks->appendChild($new_tasks);
				$user_tasks->appendChild($current_tasks);
				$user_tasks->appendChild($tobechecked_tasks);
					
				$xmlString = $dom->saveXML();	
				echo $xmlString;			
			}
		}
		break;
		case "tasks": { ?>	
		<script>
			var task_statuses = new Array();
			var full_task_statuses = new Array();
			var task_statuses1 = 0;
<?php	foreach($MODULE_OUTPUT['task_statuses'] as $ind => $status) { ?>
			task_statuses[<?=$ind?>] = '<?=$status?>';
<?php	}
		foreach($MODULE_OUTPUT['full_task_statuses'] as $ind => $status) { ?>
			full_task_statuses[<?=$ind?>] = '<?=$status?>';
<?php	}
		if(is_array($MODULE_OUTPUT['task_statuses'])) { ?>
			task_statuses1 = '<?=(implode(';', $MODULE_OUTPUT['task_statuses']))?>';
<?php	} else { ?>
			task_statuses1 = '<?=$MODULE_OUTPUT['task_statuses']?>';
<?php	} ?>
			jQuery(document).ready(function($) {
				Tasks(<?=$MODULE_OUTPUT['module_id']?>,'<?=$MODULE_OUTPUT['show_mode']?>',<?=(isset($MODULE_OUTPUT['to_print'])?1:0)?>,<?=$MODULE_OUTPUT['id']?>,task_statuses,full_task_statuses,task_statuses1);
			});
		</script>
		<script>jQuery(document).ready(function($) {InputCalendar();});</script>	
<?php		if (isset($MODULE_OUTPUT['to_print']) && $MODULE_OUTPUT['to_print']) { ?>
	<link rel="stylesheet" href="<?=HTTP_THEMES?>styles/print.css" type="text/css" />
<?php 	}
		foreach ($MODULE_DATA["output"]["messages"]["good"] as $data) {
			echo "<p class=\"message\">$data</p>\n";
		}
		foreach ($MODULE_DATA["output"]["messages"]["bad"] as $data) {
			echo "<p class=\"message red\">$data</p>\n";
		}
		$MODULE_OUTPUT = $MODULE_DATA["output"]; ?>
	<div id='tasks_container'>
<?php	if ($MODULE_OUTPUT['show_mode'] == 'executor_reports') { ?>
		<div>
			<br />
			�������� �����������: 
			<select id='executors_list' onchange="showExecutorTasks(this.options[this.selectedIndex].id);">
				<!--<option></option>-->
<?php 		if ($MODULE_OUTPUT['users'] && !empty($MODULE_OUTPUT['users'])) 
				foreach ($MODULE_OUTPUT['users'] as $user)
					echo '<option '.(isset($_GET['exec']) && $_GET['exec'] == $user["id"] ? 'selected' : '').' id="'.$user["id"].'">'.$user["name"].'</option>'; ?>
			</select>
		</div>
		<br />
<?php	} else {
			if ($MODULE_OUTPUT['show_mode'] == 'my'){ ?>
		<br />
		<hr />
		<div>
			<form id="new_task_form" method="post" onsubmit="return false" action="">
				<div class="href active" onclick="changeAddTaskTableDisplay(); "><strong>�������� ������</strong></div><br />
				<div class="ajax_msg" style="display:none; color: #ff505e" id="add_task_msg_box"></div>
				<br />
				<table style="display:none;" id="add_task_table" border=0 cellpadding="3">
				    <tr>
				        <td width="120px">
				           ��� ��������: <span style="color:red">*</span>
				        </td>
				        <td align="right">   
				            <select style="display:none" id="new_task_users" name="new_task[executor]" >
<?php			foreach ($MODULE_OUTPUT["users"] as $user) {
					if ($user['id'] != -1) { // $MODULE_OUTPUT['id'] ?> 
								<option id="<?=$user["id"]?>"><?=$user["name"]?></option>
<?php 				}
				} ?>
						    </select> 
							<input type="text" style="width:250px" id="new_task_auth" />
						</td>
					</tr>
					<tr>
						<td>
							��������: <span style="color:red">*</span>
						</td>
						<td>
							<textarea id="new_task_descr" cols="50" rows="20" name="new_task[descr]"></textarea>
						</td>
					</tr>
					<td colspan=2 align="right">
						<input id="new_task_submit" type="submit" value="��������� ���������" onclick="addNewTask()">
					</td>
				</table>
			</form>
		</div>
		<hr />
<?php 		} ?>
		<div>
			<br />
			<div id="my_tasks_href" class="href title active" onclick="changeTasksMode('my')">��� ������</div>|
			<div id="for_me_tasks_href" class="href title" onclick="changeTasksMode('me')">������ ��� ���� <span><?=(isset($MODULE_OUTPUT['tasks_count']['new']) ? "(".$MODULE_OUTPUT['tasks_count']['new'].")" : "")?></span></div>|
			<div id="all_tasks_href" class="href title" onclick="changeTasksMode('<?=( (isset($MODULE_OUTPUT['edit_all']) && $MODULE_OUTPUT['edit_all']) ? 'edit_all' : 'all')?>')">��� ������</div>
		</div>
		<br />
<?php	} ?>
		<div>
			<form id="filters_form" method="post" onsubmit="return false" action="">
				<input type="hidden" id="earlier_field" name="earlier" value="<?=$MODULE_DATA["output"]["show_earlier"]?>" />
				<input type="hidden" id="page_field" name="page" value="1" />
				<fieldset style="width:250px;">
					<legend>�������� ������:</legend>
					<div id="filters_cont">
						<input onclick="updateStatusesFilter(this)" type="checkbox" data-status="new" name="tasks[new]" /> ����� <br />
						<input onclick="updateStatusesFilter(this)" type="checkbox" data-status="current" name="tasks[current]" /> �����������<br />
						<input onclick="updateStatusesFilter(this)" type="checkbox" data-status="tobechecked" name="tasks[tobechecked]" /> �� �������������<br />
						<input onclick="updateStatusesFilter(this)" type="checkbox" data-status="done" name="tasks[done]" /> ����������� <br />
					</div>
					<hr />
					<div>
						<input onclick="changeCalendarAvailable()" type="checkbox" data-status="assign_date" name="assign_date" /> ���� �����������:<br />
						�: <input id='assign_date_from' type="text" class="calendar-field date_time"  value="2010-01-01" />&nbsp;
						��: <input id='assign_date_to' type="text" class="calendar-field date_time" value="2020-01-01" />
					</div>
					<? if (isset($MODULE_OUTPUT['assigners']) && !empty($MODULE_OUTPUT['assigners']) ) {?>
					<hr />
					<div>
						��� �������� <br />
						<select style="width:250px;" onchange="updateAssignersFilter(this)"  name="assigner">
							<option></option>
							<? 	foreach ($MODULE_OUTPUT['assigners'] as $assigner) {?>
								<option value="<?=$assigner?>"><?=$assigner?></option>
							<? } ?>
						</select>
					</div>
					<? } ?>
<?php		if ($MODULE_OUTPUT['show_mode'] == 'executor_reports') { ?>
					<div><a id="print_report_href" href="" target="_blank"><img title="����������� ��� ������������� �����������" src="<?=HTTP_THEMES?>styles/printer.png" /></a></div>
<?php 		} ?>
					<br /><a style="cursor: pointer;" onclick="refreshStatusesFilter(1)">�������� ���</a> / <a style="cursor: pointer;" onclick="refreshStatusesFilter(0)">����� ���������</a>
				</fieldset>
			</form>
		</div>
		<br />
		<div>
			<div id="task_table_cont">
<?php 		if ($MODULE_OUTPUT['tasks'][$MODULE_OUTPUT['show_mode']] && !empty($MODULE_OUTPUT['tasks'][$MODULE_OUTPUT['show_mode']]) ) {?>
			    <table id="task_table" width="1000px" cellspacing="2" cellpadding="3">
					<tr height="30px" bgcolor="#BBBBBB">
					    <td align="center" width="1px"><strong>���� �����������</strong></td><td align="center"><strong>��� ��������</strong></td><td align="center"><strong>�����������</strong></td><td align="center"><strong>��������</strong></td><td align="center"><strong>������</strong></td><td width="1px" align="center"><strong>���� ����������</strong></td><td width="1px" align="center"><strong>���� ����������</strong></td><td align="center"><strong>����������� �����������</strong></td><td align="center"><strong>�����</strong></td>
					</tr>
<?php 			foreach ($MODULE_OUTPUT['tasks'][$MODULE_OUTPUT['show_mode']] as $ind=>$task) { ?>
					<tr id="<?=$task['id']?>" bgcolor=<?=($ind%2 ? "#E5DDDD" : "#F5EAEA")?>>
						<td align="center"><?=$task['creation_date']?></td>
						<td align="center">
<?php 				if ($task['status'] != 'done') {?>
							<input type="text" value="<?=$task['author_name']?>" />
							<div  class="href author-update" onclick="changeAuthorName(this)"> <img src="<?=HTTP_THEMES?>images/edit.png" title="%title%" /></div>
<?php 				} else {
						echo $task['author_name'];
					} ?>
						</td>
						<td data-task="<?=$task['id']?>" >
							<div>
								<div class="href executor-add" onclick="changeSubjectName(this, 1)"><img src="<?=HTTP_THEMES?>images/edit-add.png" title="�������� �����������" /></div>
							</div>
							<hr />
<?php				if (!empty($task['executors'])) {
						foreach($task['executors'] as $executor) { ?>
							<div style="margin-bottom:10px">
								<a href="reports/<?='#'.$executor['id']?>"><?=$executor['name']?></a>
<?php 						if ($task['status'] != 'done' && $executor['name']) {?>
								<div class="href executor-update" onclick="changeSubjectName(this, 0)"><img src="<?=HTTP_THEMES?>images/edit.png" title="%title%" /></div>
<?php 						} ?>
							</div>
<?php 					}
					}?>
						</td>
						<td><?=trim($task['description'])?><div  class="href descr-update" onclick="changeDescr(this)"> <img src="<?=HTTP_THEMES?>images/edit.png" title="%title%" /></div></td>
						<td align="center">
							<?=$MODULE_OUTPUT['statuses_titles'][$task['status']]?><?=($task['status'] == 'tobechecked' ? '<div onclick="updateStatus(this)" data-status="'.$task['status'].'" class="href status-update"><img src="'.HTTP_THEMES.'images/edit.png" title="%title%" /></div>' : '')?>
						</td>
						<td align="center" title="<?=( !is_null($task['execution_start_date']) ? '������� ������������ '.$task['execution_start_date'] : '') ?>" class='start-date'><?=$task['assignment_date']?></td>
						<td align="center" class='finish-date'><?=$task['execution_finish_date']?></td>
						<td><?=$task['executor_comment']?></td>
						<td align="center"><div  class="href" onclick="unassignTask(this)"> <img src="<?=HTTP_THEMES?>images/delete.png"></div></td>
					</tr>
<?php 			} ?>
				</table>
<?php      	} elseif ($MODULE_OUTPUT['show_mode'] == 'my'){?>
				<p id="no_tasks_msg"><strong>����� �� �������.</strong></p>
<?php 		}?>
				<div id="pager_cont">
<?php	    // Pager block
	    	if (isset($MODULE_OUTPUT["pager_output"]))  { 
				$pager_output = $MODULE_OUTPUT["pager_output"]; 
				if (count($pager_output["pages_data"]) > 1) { // ���� ���� ��� ��������, � ������� ������ �����
					echo "<br><div id='pager' class=\"pager\">\n";
					if($pager_output["prev_page"]) {
		                echo "<div class='pager-href href' data-pager_num='".$pager_output['prev_page']."' onclick='document.getElementById(\"page_field\").value=\"".$pager_output['prev_page']."\"; ' title=\"" . '���������� ��������' . "\"";
		                echo " id=\"prev_page_link\" class=\"prev-next-link\"><span class=\"arrow\">&larr;&nbsp;</span>";
		                echo $pager_output["prev_page"] ? "</div>" : "</span>";
		                echo " ";
		            }		         
		
		            foreach ($pager_output["pages_data"] as $page => $data) {
		                if (is_int($page)) {
		                    echo ($data["is_current"] ? "<strong>" : "<div class='pager-href href' data-pager_num='".$page."' onclick='document.getElementById(\"page_field\").value=\"".$page."\";'>") . $page . ($data["is_current"] ? "</strong>" : "</div>") . " ";
		                } else {
		                    echo "&hellip; ";
		                }
		            }
		
			        if ($pager_output["next_page"]) {
			            echo "<div class='pager-href href' data-pager_num='".$pager_output['next_page']."' onclick='document.getElementById(\"page_field\").value=\"".$pager_output['next_page']."\"; ' title=\"" . '��������� ��������' . "\"";
			            echo " id=\"next_page_link\" class=\"prev-next-link\"><span class=\"arrow\">&nbsp;&rarr;</span>";
			            echo "</div>";
			        }
			        echo "</div>\n";
		        } 
			} ?>
				</div>
			</div>
		</div>
		<br />
	</div>	
<?php	}
		break;
		case "apply_support_link": {
			if(!$MODULE_OUTPUT['allow_handle'] && $Auth->usergroup_id != 0 && $Auth->usergroup_id != 2) { ?>
			<div class="tasks_count" style="height: 9.5ex; ">
				<div class="tasks_count_title">
					<a href="<?=$MODULE_OUTPUT['apply_uri'];?>">������ � ���</a>
				</div>
			</div>
			<?php 
			}
		}
		break; 
		case "apply_support": {
			$examples = $MODULE_OUTPUT["examples"];
			?>
			<div class="student_registr_form">
			
			<!--<div id='tasks_container'><div class="href active" onclick="changeAddTaskTableDisplay(); " style="border-bottom-style: dashed;font-weight: normal;"><strong>�������� ������</strong></div><br />-->
			<div class="center_cont" style="margin-left: 0px; width: 48%;">
			<p>
				<a class="bayan" onclick="window.location = '/office/multimedia/';" id="link12">������ �� ������������� �����������-���������</a><br />
			</p>
			<div id="choice_links">
			<p>
				<a class="bayan" onclick="showChoice(this, 'pub')" id="link13">���������� �� �������</a><br />
			</p>
			<p>	
				<a class="bayan" onclick="showChoice(this, 'comp')" id="link14">����������, ����, �������</a><br />
			</p>
<!-- 			<p>
				<a class="bayan" href="/office/printers/">������������ ����������</a><br />
			</p> -->
			<p>
				<a class="bayan" href="/office/refill_cart/">������ �� �������� ����������</a><br />
			</p>
			</div>
			<form id="new_task_form" method="post" onsubmit="return false" action="">
				<input type="hidden" name="subject" value="" />				
				<div class="ajax_msg" style="display:none; color: #ff505e" id="add_task_msg_box"></div>
				<br />
				<table id="add_task_table" border=0 cellpadding="3" style="display: none; ">
				<tr>
						<td>
							��������: <span style="color:red">*</span>
						</td>
						<td>
							<textarea id="new_app_descr" cols="50" rows="20" name="new_app[descr]"></textarea><br/>
							������: <span id="apply_example" style="border-bottom-style: dashed; border-bottom-width: 1px; cursor: pointer;"><?=$examples[rand(0, count($examples) - 1)]?></span> 
						</td>
					</tr>
					<tr>
						<td colspan=2 align="right">
							<input id="new_apply_submit" type="submit" value="��������� ������" onclick="applySubmit()">
						</td>
					</tr>
				</table>
			</form>
			</div>			
				<div class="right_cont" id="apply_from" style="margin-top: 1em; width: 48%; display: none; ">
				<div style="background: none repeat scroll 0 0 #EAEAEA;border-radius: 3px 3px 3px 3px;margin-bottom: 20px;padding: 6px 15px 10px 30px;">
				<h3>������ ��</h3>
				<?=$Auth->user_displayed_name; ?><br />
				<?=$MODULE_OUTPUT["people"]["comment"]?>
				</div>
				</div>
			</div>
			<div style="clear: both;"></div>
			<div id="apply_success" style="display: none; "></div>
			<img src="http://nsau.edu.ru/images/rm/rm.jpg" id="comp_img"  style="display: none; " />
			<?php 
		}
		break;
		
		case "reports": {
			?>
					<table width="60%" border="1px">
						<tr>
						 <th>����</th>						 
						 <th>�������������</th>						 
						 <th>�����</th>
						 <th>�������������</th>
						 <th>������</th>
						 <th>ID</th>
						 <th>��������� <br> ��������</th>
						 <th>��������� <br> ��������������</th>
						</tr>
						<?foreach($MODULE_OUTPUT["requests"] as $man) {
							foreach($man as $request) {
							?>
								<tr class="unit">
									<td><?=$request["date_refilled"]?></td>										
									<td><?=$request["unit_name"]?></td>										
									<td><?=$request["subunit_name"]?></td>
									<td><?=$request["manufacturer"]?></td>
									<td><?=$request["cartrige_name"]?></td> 
									<td><?=$request["cartrige_uid"]?></td>
									<td><?=number_format($request["refill_price"], 2, '.', '')?> ���.</td>
									<td><?=number_format($request["restore_price"], 2, '.', '')?> ���.</td>
								</tr>
							<?}?>
								<!--<tr>
									<td colspan="4" style="text-align: right"><strong>����� �� ������ ���������</strong></td>
									<td><?=count($man)?></td>
									<td><?=number_format(array_sum(array_map(function($val) { return $val['refill_price']; },$man )), 2, '.', '')?></td>
									<td><?=number_format(array_sum(array_map(function($val) { return $val['restore_price']; },$man )), 2, '.', '')?></td>
								</tr>!-->
								<?$total_refill += number_format(array_sum(array_map(function($val) { return $val['refill_price']; },$man )), 2, '.', '')?>
								<?$total_restore += number_format(array_sum(array_map(function($val) { return $val['restore_price']; },$man )), 2, '.', '')?>
								<?$total += count($man);?>
						<?}?>
						<tr class="unit">
							<th colspan="5" rowspan="2" style="text-align: right">�����:</th>						
							<td  rowspan="2"><?=$total?></td>
							<td><?=number_format($total_refill, 2, '.', '')?> ���.</td>
							<td><?=number_format($total_restore, 2, '.', '')?> ���.</td>
						</tr>
						<tr class="unit">
							<td colspan="2"><?=number_format($total_restore+$total_refill, 2, '.', '')?> ���.</td>
						</tr>
					</table>
		<?}
		break;


		case "requests_report": {?><table width="60%" border="1px">
						<tr>
						 <th>� ������</th>						 
						 <th>����������</th>
						 <th>����������<br>�������������</th>
						 <th>�������������</th>
						 <th>�����</th>
						 <th>������<br>��������</th>
						 <th>���� ��<br>��������</th>
						 <th>������ � <br>��������</th>
						 <th>������</th>
						</tr>
						<?foreach($MODULE_OUTPUT["requests"] as $id=>$request) {?>
							<tr class="unit">
								<td><?=$request["id"]?></td>										
								<td><?=$request["manufacturer"]?> <?=$request["cartrige_name"]?></td>
								<td><?=$request["cartrige_uid"]?></td>
								<td><?=$request["unit_name"]?></td> 
								<td><?=$request["subunit_name"]?></td>
								<td><?=$request["username"]?></td>
								<td><?=$request["date_send"]?></td>
								<td><?=$request["date_refilled"]?></td>
								<td><?=$request["status"]==0 ? "������ ������" : ($request["status"]==1 ? "���� �� ��������" : "���������")?></td>
							</tr>
						<?}?>
					</table>
		<?}
		break;

		case "cartriges_report": {
			?>
					<table width="60%" border="1px">
						<tr>
						 <th>�������������</th>						 
						 <th>������</th>
						 <th>����������</th>
						</tr>
						<?foreach($MODULE_OUTPUT["requests"] as $man) {?>
							<?foreach($man as $id=>$request) {?>
								<tr class="unit">
									<td><?=$request["manufacturer"]?></td>										
									<td><?=$request["cartrige_name"]?></td>
									<td><?=$request["total"]?></td>
								</tr>
							<?}?>
						<?}?>
					</table>
		<?}
		break;




		
		case "refill_cart": {
			

		?>	
<div class="section" style="background: #FFFFFF; border:none">
	<ul class="tabs">
		<li class="">������ ������</li>
		<li class='current'>�������� ������</li>
		<?=$MODULE_OUTPUT["allow_edit"] ? "<li class=''>���������</li>" : ""?>
	</ul>
	<div class="box" width="60%" style="border: none;">
		<div id="output_message" style="display: none;"></div>
		<?
		if (($Auth->usergroup_id == 1) || ($Auth->user_id==215)) 
		{
		?>
			<a href="#" onclick="return false;" class="not_registered">�������� �� ���������������?</a>
		<?
		}
		?>
			<div class="not_registered">
				<form action="<?=$EE["unqueried_uri"]?>" method="post" width="60%">
				<table>
				<tbody class="cartrige">	
					<tr>
						<td><input type="text" size="37" placeholder="������� ���������� ������������� ���������" class="search_user"></td>
					</tr>
					<tr>
						<td align="left">
							<div class="add_user">
								<select name="register_id" size="1" id="select_user" selected='selected' style="width: 300px;" disabled>
									<option class="tmp_user hidden" value=""></option>
								</select>
							</div>
						</td>
					</tr>	
					<tr align="left" class="cartrige_info"></tr>
					<tr>
						<td align="left">
							������: <input type="checkbox" name="priority" id="priority" size="1">
						</td>
					</tr>			
					<tr>
						<td colspan="2">
							<input type="submit" value="���������" id="add_request" />
						</td>
					</tr>
				</tbody>
				</table>
				</form>
			</div>
			<div class="not_registered hidden">
				<form action="<?=$EE["unqueried_uri"]?>" method="post">
				<table style="text-align: center;">
					<tbody>
						<tr>
							<th>�������������</th>
							<td>
								<select name="unit_id" size="1" selected='selected' class="select_unit disable" style="width: 300px;">
									<option value="0">�������� �������������</option>
									<?foreach($MODULE_OUTPUT["units"] as $id => $name) {?>
									<option value="<?=$id?>"><?=$name?></option>
									<?}?>
								</select>
							</td>
							<td rowspan="4" style="display: none;" width="500px" class="register_info" >

							</td>
						</tr>
						<tr>
							<th>����� (�������)</th>
							<td>
								<select name="subunit_id" size="1" class="select_subunit disable" selected='selected' style="width: 300px;" disabled>
									<option value="0">������� �������� �������������</option>
								</select>
							</td>
						</tr>
						<tr>
							<th>��� ����������</th>
							<td>
								<select name="cartrige_id" size="1" class="select_cartrige disable" selected='selected' style="width: 300px;" disabled>
									<option value="0">�������� ����������</option>
								</select>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<input type="hidden" name="user_reg" value="1">
								<input type="submit" value="���������" class="add_register_cartrige disable"/>
							</td>
						</tr>
					</tbody>
				</table>
				</form>
			</div>
	</div>
	<div class="box visible">
		<!--start pagination-->
		<div class="pager">
			<?if (isset($MODULE_OUTPUT["pager_output"]))
			{$test = $MODULE_OUTPUT["pager_output"];
				$pager_output = $test;
				$all_pages= count($pager_output["pages_data"]); //����� ���������� ������ �� ��������
				if ($all_pages > 1)
				{
					//���������� �������� (left arrow)
					if ($pager_output["prev_page"])
					{
						?><a href="<?=$pager_output["pages_data"][$pager_output["prev_page"]]["link"]?>" title="���������� ��������" id="prev_page_link" class="prev-next-link"><span class="arrow">&larr;&nbsp;</span></a>&nbsp;<?
					}
					//���������� ������� ���������
					//���� ����� �������� ������ 16, �� ������� ������ � 1 �� 34
					if ($pager_output["prev_page"]<=16) {
						$start_pg=0;
						$end_pg=34;
					}
					//����� ������� 16 ������ �� ������� � 16 �����
					else
					{
						$start_pg=$pager_output["prev_page"]-16;
						$end_pg=$pager_output["prev_page"]+16;
					}
					//������ �� ������ ��������
					if ($all_pages>16 && $pager_output["prev_page"]>16)
					{
						echo '<a href="?page=1">1</a> ...';
					}
					foreach($pager_output["pages_data"] as $page)
					{
						if ($cur_page<=$end_pg && $cur_page>=$start_pg)
						{
							//������� ��������
							if($page["is_current"]==1)
								{
									?> <span class="active item"><?=$page["data_array"]["page"]?></span> <?
								}
							//��������� ��������
							if($page["is_current"]!=1)
							{
									?> <a href="<?=$page["link"]?>"><?=$page["data_array"]["page"]?></a> <?
							}
						}
						$cur_page++;
					}

					//������ �� ��������� ��������
					if ($all_pages>16 && $pager_output["prev_page"]<=$all_pages-16)
					{
						echo '... <a href="?page='.$all_pages.'">'.$all_pages."</a>\n";
					}
					//��������� �������� (right arrow)
					if ($pager_output["next_page"])
					{
						?>
						<a href="<?=$pager_output["pages_data"][$pager_output["next_page"]]["link"]?>" title="��������� ��������" id="next_page_link" class="prev-next-link"><span class="arrow">&nbsp;&rarr;</span></a><?
					}
				}
			}
			unset($cur_page);?>
		<!--stop pagination-->
		</div>
		<table width="100%" style="text-align: center;" class="requests"> 
			<tr style="background-color: #def;">
				<th>#</th>
				<th>���������</th>
				<th>���������� �������������</th>
				<th>�������������</th>
				<th>�����</th>
				<?if($MODULE_OUTPUT["allow_edit"]){?><th>������ ��������</th><?}?>
				<th style="cursor: pointer"><a href="?sort_by=<?=($_GET["sort_by"]=="date") ? "date_desc" : "date"?><?=($_GET["page"]) ? "&page=".$_GET["page"]."" : ""?>">������ ������</a></th>
				<th>���� �� ��������</th>
				<th>������ � ��������</th>
				<th>���������</th>
				<th style="cursor: pointer"><a href="?sort_by=<?=($_GET["sort_by"]=="status") ? "status_desc" : "status"?><?=($_GET["page"]) ? "&page=".$_GET["page"]."" : ""?>">������</a></th>
			</tr>
			<?$i=$MODULE_OUTPUT["pager_output"]["from"];?>
			<?if(!empty($MODULE_OUTPUT["requests"])) {?>
				<?foreach($MODULE_OUTPUT["requests"] as $id => $request) {?>
					<tr class="edit_register requests id_<?=$request["register_id"]?>" id="<?=$request["register_id"]?>" style="background-color: #def;" height="60px">
						<td><?=$i++?></td>
						<?if($MODULE_OUTPUT["allow_edit"]){?>
							<td class="edit_register list select" data-id="<?=$request["cartrige_id"]?>" ondblclick="clearSelection()"><?=$request["name"]?></td>
							<td class="edit_register cartrige_uid input" ondblclick="clearSelection()"><?=$request["uid"]?></td>
							<td class="edit_register units select" data-id="<?=$request["unit_id"]?>" ondblclick="clearSelection()" style="cursor: pointer">
								<?=$request["unit"]?>
							</td>
							<td class="edit_register subunit select pid_<?=$request["unit_id"]?>" data-id="<?=$request["subunit_id"]?>" ondblclick="clearSelection()" style="cursor: pointer">
								<?=$request["subunit"]?>
							</td>
							<td class="add_user_edit <?=$id?>"><?=$request["last_name"]?></td>
							<td><?=$request["date"]?></td>
							<td><?=($request["date_send"]) ? $request["date_send"] : "-"?></td>
							<td><?=($request["date_refilled"]) ? $request["date_refilled"] : "-"?></td>
							<td><?=($request["priority"]) ? "������" : "-"?></td>
							<td>
								<?if($MODULE_OUTPUT["requests"][$id]["status"]==2) {?>������ � ��������<br><?} else {?>
									<select name="status" size="1" class="change_status" selected='selected'>
										<?if($MODULE_OUTPUT["requests"][$id]["status"]!=1) {?>
										<option value="0" class="id_<?=$id?>" selected>������ ������</option><?}?>
										<option value="1" class="id_<?=$id?>" <?=$MODULE_OUTPUT["requests"][$id]["status"]==1 ? "selected" : ""?>>���� �� ��������</option>
										<?if($MODULE_OUTPUT["requests"][$id]["status"]==1) {?>
											<option value="2" class="id_<?=$id?>">������ � ��������</option><?}?>
									</select> 
								<?}?>
								<div class="status<?=$id?>" style="color: green; display: none;"></div>
								<img src="/themes/images/delete.png" style="cursor: pointer" alt="������� ������" title="������� ������" class="delete" id="img_<?=$id?>">							
							</td>
						<?} else {?>
							<td><?=$request["name"]?></td>
							<td><?=$request["uid"]?></td>
							<td><?=$request["unit"]?></td>
							<td><?=$request["subunit"]?></td>
							<td><?=$request["date"]?></td>
							<td><?=($request["date_send"]) ? $request["date_send"] : "-"?></td>
							<td><?=($request["date_refilled"]) ? $request["date_refilled"] : "-"?></td>
							<td><?=($request["priority"]) ? "������" : "-"?></td>
							<td><?=($request["status"]==1) ? "���� �� ��������" : (($request["status"]==2) ? "������ � ��������" : "������ �� ���������")?></td>
						<?}?>
					</tr>
				<?}?>
			<?} else {?>
				<tr>
					<td colspan="10">�� �� ���������� ������.</td>
				</tr>
			<?}?>
		</table>
		<!--start pagination-->
		<div class="pager">
			<?if (isset($MODULE_OUTPUT["pager_output"]))
			{
				$pager_output = $MODULE_OUTPUT["pager_output"];
				$all_pages= count($pager_output["pages_data"]); //����� ���������� ������ �� ��������
				if ($all_pages > 1)
				{
					//���������� �������� (left arrow)
					if ($pager_output["prev_page"])
					{
						?><a href="<?=$pager_output["pages_data"][$pager_output["prev_page"]]["link"]?>" title="���������� ��������" id="prev_page_link" class="prev-next-link"><span class="arrow">&larr;&nbsp;</span></a>&nbsp;<?
					}
					//���������� ������� ���������
					//���� ����� �������� ������ 16, �� ������� ������ � 1 �� 34
					if ($pager_output["prev_page"]<=16) {
						$start_pg=0;
						$end_pg=34;
					}
					//����� ������� 16 ������ �� ������� � 16 �����
					else
					{
						$start_pg=$pager_output["prev_page"]-16;
						$end_pg=$pager_output["prev_page"]+16;
					}
					//������ �� ������ ��������
					if ($all_pages>16 && $pager_output["prev_page"]>16)
					{
						echo '<a href="?page=1">1</a> ...';
					}
					foreach($pager_output["pages_data"] as $page)
					{
						if ($cur_page<=$end_pg && $cur_page>=$start_pg)
						{
							//������� ��������
							if($page["is_current"]==1)
								{
									?> <span class="active item"><?=$page["data_array"]["page"]?></span> <?
								}
							//��������� ��������
							if($page["is_current"]!=1)
							{
									?> <a href="<?=$page["link"]?>"><?=$page["data_array"]["page"]?></a> <?
							}
						}
						$cur_page++;
					}

					//������ �� ��������� ��������
					if ($all_pages>16 && $pager_output["prev_page"]<=$all_pages-16)
					{
						echo '... <a href="?page='.$all_pages.'">'.$all_pages."</a>\n";
					}
					//��������� �������� (right arrow)
					if ($pager_output["next_page"])
					{
						?>
						<a href="<?=$pager_output["pages_data"][$pager_output["next_page"]]["link"]?>" title="��������� ��������" id="next_page_link" class="prev-next-link"><span class="arrow">&nbsp;&rarr;</span></a><?
					}
				}
			}
			?>
		<!--stop pagination-->
		</div>
	</div>
	<?if($MODULE_OUTPUT["allow_edit"]) {?>
	<div class="box">
		<a href="#" class="edit register">����������� ����������</a><br>
		<a href="#" class="edit units">�������������� �������������</a><br>
		<a href="#" class="edit devices">�������������� ���������</a><br>
		<a href="#" class="edit reports">������</a><br>
		<div class="edit reports hidden">
		<form action="<?=$EE["unqueried_uri"]?>reports/" method="post">
			������������� �����<br>
			� <input type="text" id="actual_from" readonly="readonly" name="actual_from" value="" style="100px;" /><br>
			�� <input type="text" id="actual_to" readonly="readonly" name="actual_to" value="" style="100px;" /><br>
			<input name="report_type" value="stats" type="radio"> ��������� ���������� <br>
			<input name="report_type" value="cartriges" type="radio"> ��������� (������-����������) <br>
			<input name="report_type" value="requests" type="radio"> ������ <br>
			<input type="submit" value="�������" />
		</form>
		</div>
		<div class="edit register hidden">
			<h1>����������� ����������</h1>
			<form action="<?=$EE["unqueried_uri"]?>" method="post">
			<table style="text-align: center;">
				<tbody>
					<tr>
						<th>�������������</th>
						<td>
							<select name="unit_id" size="1" selected='selected' class="select_unit pid" style="width: 300px;">
								<option value="0">�������� �������������</option>
								<?foreach($MODULE_OUTPUT["units"] as $id => $name) {?>
								<option value="<?=$id?>"><?=$name?></option>
								<?}?>
							</select>
						</td>
					</tr>
					<tr>
						<th>����� (�������)</th>
						<td>
							<select name="subunit_id" size="1" class="select_subunit pid" selected='selected' style="width: 300px;" disabled>
								<option value="0">������� �������� �������������</option>
							</select>
						</td>
					</tr>
					<tr>
						<th>��� ����������</th>
						<td>
							<select name="cartrige_id" size="1" class="select_cartrige pid" selected='selected' style="width: 300px;" disabled>
								<option value="0">�������� ����������</option>
							</select>
						</td>
					</tr>
					<tr>
						<th>� ��������� <br></th>
						<td align="left">
							<input type="text" name="uid" size="4" maxlength="4">
						</td>
					</tr>
					<!--<tr>
						<th>����� �� ��������<br></th>
						<td align="left">
							<input type="text" name="limit" size="4" maxlength="4">
						</td>
					</tr>!-->
					<tr>
						<td colspan="2">
							<input type="submit" value="����������������" />
						</td>
					</tr>
				</tbody>
			</table>
			</form>
			<h1>������������������ ���������</h1>
			<table width="100%" style="text-align: center;" class="requests">
				<tbody>
					<tr style="background-color: #def;">
						<td colspan="7"><input size="43" type="text" placeholder="������� ���������� ������������� ���������" class="search_uid"></td>
					</tr>
					<tr style="background-color: #def;">
						<th>��������</th>
						<th>���������� �����</th>
						<th>�������������</th>
						<th>�����</th>
						<!--<th>����� �� ��������</th>!-->
						<th>��������</th>
						<th>���������������</th>
						<th></th>						
					</tr>					
					<?$i=1;?>
					<?if(!empty($MODULE_OUTPUT["registered"])) {?>
						<?foreach($MODULE_OUTPUT["registered"] as $id => $reg) {?>
							<tr class="edit_register" id="<?=$id?>" style="background-color: #def;">
								<td class="edit_register list select" data-id="<?=$reg["cartrige_id"]?>" ondblclick="clearSelection()"><?=$reg["cartrige_name"]?></td>
								<td class="edit_register cartrige_uid input" ondblclick="clearSelection()"><?=$reg["uid"]?></td>
								<td class="edit_register units select" data-id="<?=$reg["unit"]["id"]?>" ondblclick="clearSelection()"><?=$reg["unit"]["name"]?></td>
								<td class="edit_register subunit select pid_<?=$reg["unit"]["id"]?>" data-id="<?=$reg["subunit"]["id"]?>" ondblclick="clearSelection()"><?=$reg["subunit"]["name"]?></td>
								<td class="edit_register unactive link" ondblclick="clearSelection()">
									<?if($reg["deleted"]) {?> 
										������:<br>
										<?=$reg["date"]?>
									<?} else {?>
										<a href="#" class="unactive">�������</a>
									<?}?>
								</td>
								<!--<td class="edit_register limit input" ondblclick="clearSelection()"><?=$reg["limit"]?></td>!-->
								<td class="edit_register user_id search" ondblclick="clearSelection()" data-id="<?=$reg["user_id"]?>"><?=$reg["user_name"]?></td>
								<td class="edit_register"><img src="/themes/images/delete.png" style='cursor: pointer' alt='�������' title='�������' class='delete'></td>
								<?$i++;?>
							</tr>
						<?}?>
					<?}?>					
				</tbody>
			</table>
			<select class="tmp hidden">
				<option></option>
			</select>
		</div>
		<div class="edit units hidden">
			<h1>������������� �������������</h1>
			<span id="field_0" class="edit_field"><a href="#" class="button_add" id="add_0">[�������� �������������]</a></span>
			<div class="add_unit" id='add_unit_0' style = "display: none">
				<input type='text' value='' id='add_unit_name_0' class='edit_input'>
				<img src='/themes/images/ok.png' style='cursor: pointer' alt='��������' title='��������' class='add_unit' id='add_unit_ok_0'>
			</div>
			<span id="output_msg_0" style="display: none;"></span>
			<ul class="tmp_subunit" style="display: none">
			<li class="tmp_subunit">
				<span class="edit_field">
					<span class="tmp_name"></span>
					<img src="/themes/images/delete.png" style="cursor: pointer" alt="������� �����" title="������� �����" class="delete_unit" >
				</span>
				<br>
				<small><a href="#" class="button_edit" id="">[�������������]</a><a href="#" class="button_add" id="">[�������� �����]</a></small>
				<div class="add_unit" style = "display: none">
					<img src='/themes/images/ok.png' style='cursor: pointer' alt='��������' title='��������' class='add_unit' id='add_unit_ok_<?=$id?>'>
				</div>
				<span class="add_unit" id="" style="display: none;"></span>
				<ul class="add_unit" style="display: none"></ul>
			</li>
			</ul>
			<?foreach($MODULE_OUTPUT["edit_units"] as $id => $unit) {?>
				<ul class="edit_unit_tree">
					<li class="tree" id="li_<?=$id?>">
						<span id="field_<?=$id?>" class="edit_field" value="<?=$unit["name"]?>">
							<span><?=$unit["name"]?></span>
							<img src="/themes/images/delete.png" style="cursor: pointer" alt="������� �������������" title="������� �������������" class="delete_unit" id="delete_unit_<?=$id?>_1">
						</span>
						<br>
						<small><a href="#" class="button_edit" id="edit_field_<?=$id?>_0">[�������������]</a><a href="#" class="button_add" id="add_<?=$id?>">[�������� �����]</a></small>
						<div class="add_unit" id='add_unit_<?=$id?>' style = "display: none">
							<input type='text' value='' id='add_unit_name_<?=$id?>' class='edit_input'>
							<img src='/themes/images/ok.png' style='cursor: pointer' alt='��������' title='��������' class='add_unit' id='add_unit_ok_<?=$id?>'>
						</div>
						<span id="output_msg_<?=$id?>" style="display: none;"></span>
						<ul  class="splCont">
							<?foreach($unit["subunits"] as $i => $subunit) {?>
								<li>
									<span id="field_<?=$subunit["id"]?>" class="edit_field" value="<?=$subunit["name"]?>">
									<span><?=$subunit["name"]?></span>
									<img src="/themes/images/delete.png" style="cursor: pointer" alt="������� �����" title="������� �����" class="delete_unit" id="delete_unit_<?=$subunit["id"]?>">
									</span>
									<br>
									<small><a href="#" class="button_edit" id="edit_field_<?=$subunit["id"]?>_<?=$subunit["pid"]?>">[�������������]</a></small>
								</li>
							<?}?>
						</ul>
					</li>
				</ul>
			<?}?>
		</div>
		<div class="edit devices hidden">
			<h1>������������� ��� ����������</h1>
			<select name="cartrige" size="1" class="pid" id="select_cartrige2" selected='selected' style="width: 300px;">
				<option value="0">�������� ��������</option>
			</select>
			<img src='/themes/images/delete.png' style='cursor: pointer' alt='�������' title='�������' id='cart_delete'><br>
			<small><a href="#" id="edit_cart">[�������������]</a><a href="#" id="add_cart">[��������]</a></small>
			<div class="edit_cart" style="display:none">
				������: <input type="text" id="edit_cart_text"><br>
				<!--�������:
					<select name="device" size="1" id="edit_cart_device" selected='selected' style="width: 300px;">
						<option value="0">�������� �������</option>
					</select></br>!-->
				��������� ��������������: <input type="text" id="edit_cart_repair_price"><br>
				��������� ��������: <input type="text" id="edit_cart_refill_price"><br>
				����������: <input type="text" id="edit_cart_comment"><br>
				�������������: <select name="manufacturer" size="1" class="pid" id="edit_cart_manufacturer" selected='selected' style="width: 300px;">
												<option value="0">�������� �������������</option>
												<?foreach($MODULE_OUTPUT["manufacturers"] as $id => $val) {?>
													<option value="<?=$id?>"><?=$val?></option>
												<?}?>
											</select><br>
				<img src='/themes/images/ok.png' style='cursor: pointer' alt='��������' title='��������' id='edit_cart_ok'>
				<img src='/themes/images/delete.png' style='cursor: pointer' alt='������' title='������' id='edit_cart_cancel'>
			</div>
			<div class="add_cart" style="display:none">
				������: <input type="text" id="add_cart_text"><br>
				��������� ��������������: <input type="text" id="add_cart_repair_price"><br>
				��������� ��������: <input type="text" id="add_cart_refill_price"><br>
				����������: <input type="text" id="add_cart_comment"><br>
				�������������: <select name="manufacturer" size="1" class="pid" id="add_cart_manufacturer" selected='selected' style="width: 300px;">
												<option value="0">�������� �������������</option>
												<?foreach($MODULE_OUTPUT["manufacturers"] as $id => $val) {?>
													<option value="<?=$id?>"><?=$val?></option>
												<?}?>
											</select><br>
				<img src='/themes/images/ok.png' style='cursor: pointer' alt='��������' title='��������' id='add_cart_ok'>
				<img src='/themes/images/delete.png' style='cursor: pointer' alt='������' title='������' id='add_cart_cancel'>
			</div>
			<span class="add_cart_ok" style="color: green; display: none;" ></span>
		</div>		
	</div>
	<?}?>
</div>

		<?	
		}
		break;

		case "cart_1c": {


		if (!empty($MODULE_OUTPUT["cart_1c"]['error'])) { ?>
			<div class="alert alert-danger" role="alert"><?=$MODULE_MESSAGES[106]?></div>
			<?php return;
		}

		if(!empty($MODULE_OUTPUT["cart_1c"])) { ?>
			<link rel="stylesheet" href="/themes/styles/education.css" type="text/css"/>
			<style type="text/css">
				.preparation__tab-title {
					padding: 12px 0!important;
					margin-bottom: -2px!important;
					font-size: 19px!important;
				}
				.preparation__tab-close {
					top: 12px!important;
				}
				.preparation__tab-close {
					height: 30px!important;
					width: 30px!important;
				}

				.preparation__tab-close svg {
					width: 12px!important;
				}
				.preparation__tabs {
					margin-top: 10px!important;
				}

				@media screen and (max-width: 1023px){
					.preparation__tab-close svg {
						width: 10px;
					}
				}

				@media screen and (max-width: 1919px) {
					.preparation__tab-close {
						height: 30px!important;
						width: 30px!important;
					}
				}
			</style>
			<div class="col-md-12">
				<div class="preparation__tabs">
					<?php foreach($MODULE_OUTPUT["cart_1c"] as $k=>$values){
					$i = 1; ?>
						<div class="preparation__tab">
							<div class="preparation__tab-title">
								<?=$k?>
							</div>
							<div class="preparation__tab-close">
								<svg name="somesvg" width="15" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
									<path fill="white"
										  d="M207.6 256l107.72-107.72c6.23-6.23 6.23-16.34 0-22.58l-25.03-25.03c-6.23-6.23-16.34-6.23-22.58 0L160 208.4 52.28 100.68c-6.23-6.23-16.34-6.23-22.58 0L4.68 125.7c-6.23 6.23-6.23 16.34 0 22.58L112.4 256 4.68 363.72c-6.23 6.23-6.23 16.34 0 22.58l25.03 25.03c6.23 6.23 16.34 6.23 22.58 0L160 303.6l107.72 107.72c6.23 6.23 16.34 6.23 22.58 0l25.03-25.03c6.23-6.23 6.23-16.34 0-22.58L207.6 256z"
										  class=""></path>
								</svg>
							</div>
							<div class="preparation__tab-body">
								<table width="100%" style="text-align: center; background: #f9f9f9;" class="table table-striped">
									<tr style="background-color: #f9f9f9;">
										<th style="text-align: center;">#</th>
										<th style="text-align: center;">���������</th>
										<th style="text-align: center;">����������� �����</th>
										<th style="text-align: center;">�����. ������</th>
										<th style="text-align: center;">������</th>
									</tr>
									<?php foreach($values as $kl=>$kart){ ?>
									<tr style="background-color: #f9f9f9;">
										<td ><?=$i++?></td>
										<td ><?=$kart->getName()?></td>
										<td ><?=$kart->getNumber()?></td>
										<td ><?=$kart->getWork()?></td>
										<td ><?=($kart->getReturned() == 1) ? "������ � ��������" : "������ �� ���������"?></td>
									</tr>
									<?php } ?>
								</table>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
	<?php }
		}
		break;

		default: {
			
		}
		break;
	}

	
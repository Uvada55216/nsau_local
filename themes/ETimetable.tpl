<?php
// version: 1.00
// date: 2015-06-25
//$MODULE_OUTPUT = $MODULE_DATA["output"];

$MODULE_MESSAGES = array(
    101 => "������� ������� ��������.",
);


if ($MODULE_OUTPUT["messages"]["good"])
{
    $array = array();

    foreach ($MODULE_OUTPUT["messages"]["good"] as $elem)
    {
        if (isset($MODULE_MESSAGES[$elem]))
        {
            $array[] = $MODULE_MESSAGES[$elem];
        }
    }

    if ($array)
    {
        foreach ($array as $elem)
        {
            echo "<p class=\"message\">".$elem."</p>\n";
        }
    }
}

if ($MODULE_OUTPUT["messages"]["bad"] && isset($MODULE_OUTPUT["display_variant"]) && !in_array($MODULE_OUTPUT["display_variant"], array("add_item", "edit_item")))
{
    $array = array();

    foreach ($MODULE_OUTPUT["messages"]["bad"] as $elem)
    {
        if (isset($MODULE_MESSAGES[$elem]))
        {
            $array[] = $MODULE_MESSAGES[$elem];
        }
    }

    if ($array)
    {
        foreach ($array as $elem)
        {
            echo "<p class=\"message red\">".$elem."</p>\n";
        }
    }
}

switch($MODULE_OUTPUT["mode"])
{  
case "timetable_print":
	{
		$wdays_t = array("��","��","��","��","��","��", "��");
		if(empty($_GET["weeks"]))
			foreach($MODULE_OUTPUT["days"] as $ddid=>$d)
				$wdays[$ddid] = $wdays_t[$ddid];
		else $wdays = $wdays_t;
		$daysCol = count($wdays);
			
		$g_num = count($MODULE_OUTPUT["groups"]);
		$groups_width = 95;
		$col_width = floor($groups_width/$g_num);
		$oth = $groups_width-($col_width*$g_num);
		$weeks = array("odd", "even");
		$types = array("�","��","��");
		$subgroups = array('','(�)','(�)');
		$groups = $MODULE_OUTPUT["groups"];
		$total_rows = 0;
		$mode = array("main", "doubles", "triples");
		$xArr = end($MODULE_OUTPUT["groups"]);
		foreach($weeks as $wid=>$ww)		
			foreach($MODULE_OUTPUT["groups"] as $gr_id=>$gr) {
				foreach($gr as $parts_id => $part) {
					if($parts_id == "triples" || $parts_id == "doubles") {
						foreach($part as $parts)
							if($wid==$parts["week"]) {
								$g[$gr_id][$parts_id][$wid ? 0 : 1][$parts["pair"]][$parts["day"]] = $parts;
								$total_rows++;
							}
					}			
				}
			}
		foreach($MODULE_OUTPUT["pairs"] as $gid => $days) {
			foreach($days as $i_p=>$p) {
				$total_rows += count($p);
			}
		}
	switch(count($MODULE_OUTPUT["pairs"])) {
		case 1:
			$fontSize = 14*4;
		break;
		case 2:
			$fontSize = 12*2;
		break;
		case 3: {
			$fontSize = 12*((empty($_GET["weeks"])) ? 1.5 : 2);
			
		}	
		break;
		case 4: {
			$fontSize = 12*((empty($_GET["weeks"])) ? 1.5 : 2);
			
		}
		break;
		case 5:
			$fontSize = 12*((empty($_GET["weeks"])) ? 1.5 : 2);
		break;
	}
	
		?>
<html>
<head>

<style>
	td {
	font: normal small-caps 20px/24px arial;
	vertical-align: middle;
	}	
	tr.days {
		 font-size: <?=$fontSize*((empty($_GET["weeks"])) ? 2 : 1)?>px;
	}
	td.groups {
		 font-size: <?=$fontSize*((empty($_GET["weeks"])) ? 2 : 1)?>px; 
	}
	td.approved {
		font-size: 30px;
		border: none;
	}
	td.title {
		text-align: center;
		font-size: 55px;
		border: none;
	}
	table tr.title {
		border: none;
	}
	tr.pairs {
		text-align: center;
	}
	table {
		 border: 0px; 
	}
	tr.info td{
		border: none;
		text-align: left;
	}
</style>
</head>
<body>
<?if(!empty($MODULE_OUTPUT["groups"])) {	?>


	<table border="1px" width="<?=270?>%" height="100%">
		<tbody>
		<tr class="title">
			<td class="title" colspan="<?=(2+$daysCol)?>">
			<div class="title"> 
				<br><b>���������� ������� ������� <?=($_GET["weeks"]=="") ? "� 00.00.15 �� 00.00.15 ��. ���" : "����"?></b><br>
					<b>
						<?=$MODULE_OUTPUT["info"]["facName"]?>&nbsp
						<?foreach($MODULE_OUTPUT["pairs"] as $grId => $grArr) { echo $MODULE_OUTPUT["groups"][$grId]["group_name"].($MODULE_OUTPUT["groups"][$grId]["group_name"]==$xArr["group_name"] ? "" : ", "); if($_GET["weeks"]!="") break;}?>
						(<?=$MODULE_OUTPUT["info"]["specName"]?>)
					</b>
			 </div>
			</td>
			<td class="approved">
					&nbsp;<b>���������</b><br>
					&nbsp;<b>��������� �� ������� ������</b><br>
					&nbsp;<b>_______________�.�. �����</b>
					&nbsp;<b>�___�___________2015�.</b>
					<p></p>
			</td>
		</tr>	
		
		<tr class="days">
			<th width="2%"></th>
			<th width="1%"></th>
			<th width="1%"></th>
			<? foreach ($wdays as $day) { ?>
				<th width="16%"><strong><?=$day?></strong></th>
			<?} ?>
		</tr>	

		
		<?foreach($MODULE_OUTPUT["pairs"] as $gid => $days) {$f=0;?>
			<?$pair=1; for($i=0;$i<14;$i++) {
				$parity = $i%2; ?>					
			<?
				$fl=0;
				$rowspan_gName = 0;
				foreach($days as $i_p=>$p) {
					$rowspan_gName += (count($g[$gid]["doubles"][0][$i_p])!=0 ? 1 : 0) + (count($g[$gid]["doubles"][1][$i_p])!=0 ? 1 : 0);
					$rowspan_gName += (count($g[$gid]["triples"][0][$i_p])!=0 ? 1 : 0) + (count($g[$gid]["triples"][1][$i_p])!=0 ? 1 : 0);
					$rowspan_gName += count($p);
				}
				if(!empty($_GET["weeks"]))
					if($rowspan_gName>0)
						$rowspan_gName += 7-count($MODULE_OUTPUT["pairs"][$gid]);
					else
						$rowspan_gName += 8-count($MODULE_OUTPUT["pairs"][$gid]);
				if(empty($days[$pair-1]["odd"]) && empty($days[$pair-1]["even"]))
					$pp = 0;
				$rowspan_pairNum = count($days[$pair-1])+(count($g[$gid]["doubles"][0][$pair-1])!=0 ? 1 : 0) + (count($g[$gid]["doubles"][1][$pair-1])!=0 ? 1 : 0);
				$rowspan_pairNum += (count($g[$gid]["triples"][0][$pair-1])!=0 ? 1 : 0)+(count($g[$gid]["triples"][1][$pair-1])!=0 ? 1 : 0);
			?>
			
				 <?if(!empty($days[$pair-1][$weeks[$parity]]) ){?>
					<?foreach($mode as $mod) {?>
						<?if(($mod=="main") || !empty($g[$gid][$mod][$parity][$pair-1])) {?>
							 <tr class="pairs">
								<?if(!$f){?><td class="groups" rowspan="<?=$rowspan_gName?>">
								<?if($_GET["weeks"]=="") 
									echo $MODULE_OUTPUT["groups"][$gid]["group_name"];
								else
									echo "<b>".$MODULE_OUTPUT["groups"][$gid]["group_subname"]."</b>";?>
								</td><?$f=1; $pp = 1;}?>
								<?if(!empty($days[$pair-1])) {?>
								<?if(!$parity && !$fl){?><td rowspan="<?=$rowspan_pairNum?>"><b><?=$pair?></b></td><?$fl=1;} elseif((count($days[$pair-1])==1) && !$fl) {?>
									<td rowspan="<?=$rowspan_pairNum?>"><b><?=$pair?></b></td>
								<?$fl=1;}?>
								<td rowspan="1"><?=(!$parity) ? "�" : "�"; ?></td>
									<?foreach($wdays as $did => $day){?>
										<?
										if($mod=="main") {
											$p = $MODULE_OUTPUT["groups"][$gid][$weeks[$parity]][$did][$pair-1];
										}
										else
											$p = $g[$gid][$mod][$parity][$pair-1][$did];
											if($p["subj"]) {
												$teachers = array_diff(explode(";", $p["teacher_id"]),array(''));
												foreach ($teachers as $k => $v) $teachers[$k] = $MODULE_OUTPUT["teachers"][$v]["p2"]." ".$MODULE_OUTPUT["teachers"][$v]["p0"][0].". ".$MODULE_OUTPUT["teachers"][$v]["p1"][0].".";
												$teachers = implode(";", $teachers);
												$auds = array_diff(explode(";", $p["auditorium_id"]),array(''));
												foreach ($auds as $k => $v) $auds[$k] = $MODULE_OUTPUT["auds"][$v]["build"]."-".$MODULE_OUTPUT["auds"][$v]["name"];
												$auds = implode(";", $auds);
												$res = array();
												$res[] = "<b>".$p["subj"]."</b>";
												$res[] = $types[$p["type"]];
												$res[] = $subgroups[$p["subgroup"]];
												$res[] = $auds;
												$res[] = $teachers;
												if (!empty($p['comment_from']) && !empty($p['comment_to']) 
													&& ($p['comment_from']!="00.00.0000") && ($p['comment_to']!="00.00.0000"))
													$res[] = "� ".$p['comment_from'].' �� '.$p['comment_to'];
												foreach ($res as $k => $v) if (!$v || $v == ';') unset($res[$k]);
												$e=0;?>
												<td><?=implode(";",$res)?></td>
											<?} else {?>
												<td></td>
											<?}?>
								<?}?>
							<?}?>
							</tr>
						<?}?>
					<?}?>
				<?} elseif($pp == 0 && $parity && !empty($_GET["weeks"])) {$pp = 1;?>
					<tr class="pairs">
						<?if(!$f){?><td class="groups" rowspan="<?=$rowspan_gName?>">
							<?if($_GET["weeks"]=="") 
								echo $MODULE_OUTPUT["groups"][$gid]["group_name"];
							else
								echo "<b>".$MODULE_OUTPUT["groups"][$gid]["group_subname"]."</b>";?>
							</td><?$f=1; $pp = 1;} else {?>
						<?}?>
						<td colspan = "1"><b><?=$pair?></b></td>
						<?for($x=0;$x<8;$x++) {?>
							<td></td>
						<?}?>
					</tr>
				<?}?>				
			<?
			($parity) ? $pair++ : $pair;
			}?>
		<?}?>
		<tr class="info">
			<td colspan="<?=(2+$daysCol)?>">��������! � �������� ����� �������� ��������� ����������, ������� �� ����� ���������� �� �����.</td>
		</tr>
		<tr class="info">
			<td colspan="<?=(2+$daysCol)?>">��������� ������� �������� ����:</td>
		</tr>
		<?if(!empty($MODULE_OUTPUT["buildings"])) {?>
				<tr class="info"><td colspan="<?=(2+$daysCol)?>">		
				<?foreach($MODULE_OUTPUT["buildings"] as $b_id=>$buildings) { ?>
					
							<b><?=$buildings["label"]?></b>-<?=$buildings["name"]?>,&nbsp;						
					
					
				<?}?>
				</td></tr>
		<?}?>	
		<tr class="info">
			<td colspan="<?=(3+$daysCol)?>">�������� �����������: <b>�</b>  � ���������� ������� | <b>��</b> � ������������ ������� | <b>(�)</b> ��� <b>(�)</b> � ���������, � ������� ������� (���� ���, �� � ���� ������)</td>
		</tr>
		</tbody>		
	</table>

<?} else {?>
���������� �� ���������
<?}?>
</body>
</html>
<?
}

	break;

	case "people_menu_stimetable": {?>	
<?//////////////////////////////////////////////////////////////////////////////////////////////?>
<?//����� - ���������� ��������//////////////////////////////////////////////////////////////////////////?>		
<?//////// /////////////////////////////////////////////////////////////////////////////////////?>		
		<?header("Content-type: text/html; charset=windows-1251");?>
		<h2 style="text-align: center;<? if (!$MODULE_OUTPUT["dates"]["actual_from"] && !$MODULE_OUTPUT["dates"]["actual_to"]) { ?> display: none;<? } ?>"><strong>���������� ������� �� ������ � <?=implode(".",array_reverse(explode("-",$MODULE_OUTPUT["dates"]["actual_from"])))?> �� <?=implode(".",array_reverse(explode("-",$MODULE_OUTPUT["dates"]["actual_to"])))?></strong></h2>
		<?if(!empty($MODULE_OUTPUT["timetable"]))	{?>
	    <?php
			$weeks = array(1 => "odd", 0 => "even");
			$days = array("�����������", "�������", "�����", "�������", "�������", "�������", "�����������");
			$types = array("������", "��������", "���. ������");
			$pairs = array("09:00-10:30", "10:40-12:10", "12:50-14:20", "14:30-16:00", "16:10-17:40", "17:45-19:15", "19:20-20:50");
			
			$week = ($MODULE_OUTPUT["parity"] == "even") ? "׸����" : "��������";
			$now = $MODULE_OUTPUT["now"];
			$months = array("������", "�������", "�����", "������", "���", "����", "����", "�������", "��������", "�������", "������", "�������");
			$year = date("Y");
			$month = date("n");
			$day = date("j");?>
			<h1><?=$MODULE_OUTPUT["group"]["fac"]?></h1>
			<h2>����������� ����������: <strong><?=$MODULE_OUTPUT["group"]["specialities_code"]?> - <?=$MODULE_OUTPUT["group"]["spec"]?></strong></h2>
			<p>������ <strong><?=$MODULE_OUTPUT["group"]["name"]?></strong>, <?=$MODULE_OUTPUT["group"]["year"]?> ����<br /> </p>
			
			<?if(!empty($MODULE_OUTPUT["edit_time"])) {?>
				<p>���� ���������� ��������� - <?=$MODULE_OUTPUT["edit_time"]?></p>
			<?}?>
			<?foreach ($days as $day => $dname) {
				if (/*$MODULE_OUTPUT["mode"] == "timetable_make" || count($MODULE_OUTPUT[$wname][$day])*/ 1 /*������� ��� ����*/) {
					?><h2><?=$dname?></h2>
					<table width="100%" style="text-align: center;">
						<tr>
							<th width="2%">#</th>
							<th width="5%">����</th>
							<th width="3%">������</th>
							<th width="25%">����������</th>
							<th width="5%">���</th>
							<th width="5%">���������</th>
							<th width="12%">���������</th>
							<th width="23%">�������������</th>
						</tr>
						<? $rownum = 0;
						foreach ($pairs as $pnum => $pr) {
						foreach ($weeks as $wnum => $wname) {
							$pairzz = array(0 => "", 1 => "", 2 => ""); //��� ������ ��������
							if (isset($MODULE_OUTPUT["timetable"][$wname][$day][$pnum])) {
								$pairzz[0] = $MODULE_OUTPUT["timetable"][$wname][$day][$pnum];
								if (isset($MODULE_OUTPUT["timetable"]["doubles"][$pairzz[0]["id"]]))
									$pairzz[1] = $MODULE_OUTPUT["timetable"]["doubles"][$pairzz[0]["id"]];
								if (isset($MODULE_OUTPUT["timetable"]["triples"][$pairzz[0]["id"]]))
									$pairzz[2] = $MODULE_OUTPUT["timetable"]["triples"][$pairzz[0]["id"]];
							}

							$rowspan_count = 2;
							if (isset($MODULE_OUTPUT["timetable"]["doubles"][$MODULE_OUTPUT["timetable"]["even"][$day][$pnum]["id"]]))
								$rowspan_count++;
							if (isset($MODULE_OUTPUT["timetable"]["doubles"][$MODULE_OUTPUT["timetable"]["odd"][$day][$pnum]["id"]]))
								$rowspan_count++;
							if (isset($MODULE_OUTPUT["timetable"]["triples"][$MODULE_OUTPUT["timetable"]["odd"][$day][$pnum]["id"]]))
								$rowspan_count++;
							if (isset($MODULE_OUTPUT["timetable"]["triples"][$MODULE_OUTPUT["timetable"]["even"][$day][$pnum]["id"]]))
								$rowspan_count++;

							foreach ($pairzz as $is_double => $curpair) {
							$hide = ($is_double && !$curpair) ? "display: none; " : "";
							$clr = ($rownum % 2) ? 'cff' : 'def';
							$trstyle = ($rownum % 2) ? " style='". $hide ."background-color: #".$clr.";'" : " style='". $hide ."background-color: #".$clr.";'"; $rownum++;

								?>
								<tr<?=$trstyle?>>
									<? if(!isset($arr[$day][$pnum])) { ?><td valign="middle" rowspan="<?=$rowspan_count?>"><?=$pnum+1?></td>
									<td valign="middle" rowspan="<?=$rowspan_count?>"><?=$pr?></td><? } ?>
									<td <?=(($wnum==0) && ($is_double == 0)) ? "bgcolor='#eaeaea'" : ""; ?><?=($is_double != 0) ? "bgcolor='#cff'" : ""; ?> ><? echo ($wname=="even") ? '���' : '�����'; ?></td>
									<td <?=(($wnum==0) && ($is_double == 0)) ? "bgcolor='#eaeaea'" : ""; ?><?=($is_double != 0) ? "bgcolor='#cff'" : ""; ?> ><?=$curpair["subj"]?><? if ($curpair["comment_from"] && $curpair["comment_from"]!='00.00.0000') { ?><br /><span style="font-size: 12px;">� <?=$curpair["comment_from"]?> �� <?=$curpair["comment_to"]?></span><? } ?></td>
									<td <?=(($wnum==0) && ($is_double == 0)) ? "bgcolor='#eaeaea'" : ""; ?><?=($is_double != 0) ? "bgcolor='#cff'" : ""; ?> ><?=$types[$curpair["type"]]?></td>
									<td <?=(($wnum==0) && ($is_double == 0)) ? "bgcolor='#eaeaea'" : ""; ?><?=($is_double != 0) ? "bgcolor='#cff'" : ""; ?> ><? echo $curpair["subgroup"] ? $subgroups[$curpair["subgroup"]] : ""; ?></td>
									<td <?=(($wnum==0) && ($is_double == 0)) ? "bgcolor='#eaeaea'" : ""; ?><?=($is_double != 0) ? "bgcolor='#cff'" : ""; ?> ><? foreach ($curpair["auds"] as $aud) { ?><?=$aud["build"]." - ".$aud["name"]?><br /><? } ?></td>
									<td <?=(($wnum==0) && ($is_double == 0)) ? "bgcolor='#eaeaea'" : ""; ?><?=($is_double != 0) ? "bgcolor='#cff'" : ""; ?> ><? foreach ($curpair["teachs"] as $teach) { ?><a href="/people/<?=$teach["people_id"]?>/" target="_blank"><?=$teach["p2"]." ".substr($teach["p0"],0,1).". ".substr($teach["p1"],0,1)?>.</a><br /><? } ?></td>
								</tr>
						
						<? $arr[$day][$pnum] = 1; } } } ?>
					</table><?
				} 
			}?>
		</div>
	</form>
		<?}else {?>
			<p>���������� �� ���������. �� ���� �������� ���������� � �������� ��������� ���� ���������� <strong>�������� ����� ����������</strong>.</p>
			<h2>���������� ����������</h2>
			������������: �-106 �<br>
			�������: 267-32-36<br>		
		<?}?>
<?//////////////////////////////////////////////////////////////////////////////////////////////?>
<?//////////////////////////////////////////////////////////////////////////////////////////////?>		
<?//////// /////////////////////////////////////////////////////////////////////////////////////?>		
	<?}
	break;
	
	case "people_menu_timetable": {?>	
		<?header("Content-type: text/html; charset=windows-1251");?>
		<h2>���������� �������������</h2>
<?//////////////////////////////////////////////////////////////////////////////////////////////?>
<?//����� - ����������//////////////////////////////////////////////////////////////////////////?>		
<?//////// /////////////////////////////////////////////////////////////////////////////////////?>					
	<?if(!empty($MODULE_OUTPUT["tt"])) {
			$path = explode("/",$this->uri);
			$wdays = array("��","��","��","��","��","��", "��");
			$wpairs = array("9.00-10.30","10.40-12.10","12.50-14.20","14.30-16.00","16.10-17.40","17.45-19.15", "19.20-20.50");
			// foreach($MODULE_OUTPUT["pairs"]["days"] as $ddid=>$d)
				// $wdays[$ddid] = $wdays_t[$ddid];
			// foreach($MODULE_OUTPUT["pairs"]["pairs"] as $ppid=>$p)
				// $wpairs[$ppid] = $wpairs_t[$ppid];
			ksort($wpairs);
			$min_pid = key($wpairs);
			end($wpairs);
			$max_pid = key($wpairs);
			$base_mode = array("one", "two", "three");?>
			<table width="100%" border="1" class="" style="border-collapse: collapse; font: normal 14px arial;">
				<tr>
					<th colspan="<?=count($wpairs)+2?>">
					<?=$MODULE_OUTPUT["tt_people_name"]?>
					</th>
				</tr>
				<tr>
					<th>����</th>
					<th>������</th>
					<?foreach($wpairs as $pid=>$pair) {?>
						<th><?=$pair?></th>
					<?}?>
				</tr>
				<?foreach($wdays as $did=>$day) {
					$day_n = 0;?>
					<?if(!empty($MODULE_OUTPUT["pairs"]["days"][$did])) {?>
						<?$rowspan = $MODULE_OUTPUT["pairs"]["days"][$did][0]+$MODULE_OUTPUT["pairs"]["days"][$did][1];?>
						<?for($i=0; $i<2;$i++) {?>
							<?for($k=0;$k<$MODULE_OUTPUT["pairs"]["days"][$did][$i];$k++) {?>
								<?$f=0;?>
								<?$mod = $base_mode[$k];?>
								<?foreach($wpairs as $pair_id=>$pairs) {?>
									<?if(!$f && ($pair_id==$min_pid)){?>
										<tr align="center">
											<?if(!$day_n) {?><td rowspan="<?=$rowspan?>"><?=$day;?></td><?$day_n = 1;}?>
											<td ><?=($i) ? "�" : "�"?></td>
									<?}?>
									<?$p = $MODULE_OUTPUT["tt"][$i][$did][$pair_id][$mod]?>			
											<td>
											<?if(!empty($p)) {?>
												<?=$p["subject"]?>(<?=$p["type"]?>); <?=$p["groups"]?>; <?=$p["auditorium"]?>;<br>
												<span style="font-size: 12px"><?=($p["from"]) ? "c ".implode(".", array_reverse(explode("-",$p["from"]))) : ""?> <?=($p["to"]) ? "�� ".implode(".", array_reverse(explode("-",$p["to"]))) : ""?></span>
											<?}?>
											</td>
									<?if(!$f && ($pair_id==$max_pid)){?></tr><?$f=1;}?>
								<?}?>										
							<?}?>
						<?}?>	
					<?} else {?>
					<tr align="center">
						<td><?=$day;?></td>
						<?for($c=0;$c<(count($wpairs)+1);$c++) {?>
							<td></td>
						<?}?>
					</tr>
					
					<?}?>
				<?}?>
				<?if(count($path)<5) {?>
					<tr>
						<td colspan="<?=count($wpairs)+2?>">
							<a href="/people/teachers-timetable-print/<?=$MODULE_OUTPUT["tt_people_id"]?>/">����������� ����������</a>
						</td>
					</tr>
				<?}?>
			</table>					
		<?}
		else {?>
			<p>���������� �� ���������. �� ���� �������� ���������� � �������� ��������� ���� ���������� <strong>�������� ����� ����������</strong>.</p>

<h2>���������� ����������</h2>
������������: �-106 �<br>
�������: 267-32-36<br>
		<?}?>
<?//////////////////////////////////////////////////////////////////////////////////////////////?>
<?//////////////////////////////////////////////////////////////////////////////////////////////?>		
<?//////////////////////////////////////////////////////////////////////////////////////////////?>	
	<?}
	break;
    case "timetable_show":
    case "timetable_make":
	?><div style="position: relative; float: left; width: 69%;"><?
    {
	$subgroups = array("���", "�", "�");
	if (isset($_GET["gr"])) {
		$group = $MODULE_OUTPUT["groups"][$_GET["gr"]];
		?><script>
			jQuery(document).ready(function ($) {
				defgroup(<?=$group["form_education"]?>,<?=$group["id_faculty"]?>,<?=$group["year"]?>,<?=$group["id"]?>);
			//	install_subjects();
			});
		</script>
		<?
	}
        ?>
<?if(empty($MODULE_OUTPUT["show_mode"])) {
	$m = explode("/", $Engine->unqueried_uri);
	$n =  explode("/", $_SERVER["REQUEST_URI"]);
	
	?>
	<?if($n[1]!="raspisanie") {?>
		<?if($_GET["print"]!=1) {?>				
			<a href="?print=1"  class="tt_to_print">������ ���������� (� ������������ ������ �����)</a> 
		<?} elseif($m[1]!="student") {?>
			<a href="/office/timetable_making/"  class="tt_to_print">������������� ����������</a> 
		<?} else {?>
			<a href="/student/timetable/"  class="tt_to_print">�������� ����������</a> 
		<?}?>
	<?}?>	
	<table id="choosegroup" >
	<tr><td>
	<table style="width: 500px; background-color: <?=($_GET["print"]==1) ? "#ede" : "#eee"?>;" cellspacing="3" class="sel_gr">
		<tr>
			<td style="width: 250px;">����� ��������</td>
			<td><select onchange="selgroups();" id="edform"><option></option><option value="1">�����</option><br /><option value="2">�������</option><option value="3">����-�������</option></select></td>
		</tr>
		<tr>
			<td style="width: 250px;">���������</td>
			<td><select onchange="selgroups();" id="fac"><option></option><?php foreach ($MODULE_OUTPUT["faculties"] as $f) { ?><option value="<?=$f["id"]?>"><?=$f["name"]?></option><br /><? } ?></select></td>
		</tr>
		<tr>
			<td style="width: 250px;">����</td>
			<td><select onchange="selgroups();" id="course"><option></option><option>1</option><br /><option>2</option><br /><option>3</option><br /><option>4</option><br /><option>5</option><br /><option>6</option><br /></select></td>
		</tr>
		<tr  class="week hidden">
			<td style="width: 250px;">������</td>  
			<td>
				<table>
					<tr>
						<?for($i=0;$i<4;$i++) {?>
							<?if($_GET["print"]!=1){?>
								<td><input type="radio" value="<?=$i?>" class="week" name="week" <?=($_GET["week"]==$i) ? "checked" : ""?>><?=$i+1?> ������.<td>
							<?} else {?>
								<td><input type="checkbox" value="<?=$i?>" class="week" name="week" <?=($_GET["week"]==$i) ? "checked" : ""?>><?=$i+1?> ������.<td>
							<?}?>
						<?}?>	
					</tr>
				</table>
			</td>
		</tr>		
		<tr>
			<td style="width: 250px;">������</td>
			<?if($_GET["print"]!=1) {?>			
				<td><table><tr><td width="40%"><?php foreach ($MODULE_OUTPUT["groups"] as $gr) { ?><span data-id="<?=$gr["id"]?>" onclick="" style="display:none; float: left; cursor: pointer;" class="groups gr_<?=$gr["form_education"]?>_<?=$gr["id_faculty"]?>_<?=$gr["year"]?>"><input type="radio" value="<?=$gr["id"]?>" id="<?=$gr["id"]?>" class="select_group" name="groups"> <?=$gr["name"]?></span> <? } ?></td></tr></table></td>
			<?} else {?>
				<td><table><tr><td width="40%"><?php foreach ($MODULE_OUTPUT["groups"] as $gr) { ?><span onclick="" style="display:none; float: left;" class="groups gr_<?=$gr["form_education"]?>_<?=$gr["id_faculty"]?>_<?=$gr["year"]?>"><input type="checkbox" class="groups_to_print" value="<?=$gr["id"]?>" id="<?=$gr["id"]?>" name="groups"> <?=$gr["name"]?></span> <? } ?></td></tr>
				<tr><td  class="sel_gr <?=($_GET["print"]==1) ? "" : "hidden"?>" colspan="2">�������� �� 1 �� 4 �����.</td></tr>
				</table></td>
			<?}?>	
		</tr>
		</table></td>
	</tr></table>
<?}?>
        <?php
	$weeks = array(1 => "odd", 0 => "even");
	$days = array("�����������", "�������", "�����", "�������", "�������", "�������", "�����������");
	$types = array("������", "��������", "���. ������");
	$pairs = array("09:00-10:30", "10:40-12:10", "12:50-14:20", "14:30-16:00", "16:10-17:40", "17:45-19:15", "19:20-20:50");
	
	$week = ($MODULE_OUTPUT["parity"] == "even") ? "׸����" : "��������";
	$now = $MODULE_OUTPUT["now"];
            $months = array("������", "�������", "�����", "������", "���", "����", "����", "�������", "��������", "�������", "������", "�������");
            $year = date("Y");
            $month = date("n");
            $day = date("j");
	if (isset($_GET["gr"])) {?>
		<?if(!empty($MODULE_OUTPUT["edit_time"])) {?>
			<p>���� ���������� ��������� - <?=$MODULE_OUTPUT["edit_time"]?></p>
		<?}?>
		<form action="" method="post">
		<? if ($MODULE_OUTPUT["mode"]=="timetable_show") { ?>
			<br /><h1><?=$MODULE_OUTPUT["group"]["fac"]?></h1>
			<h2>����������� ����������: <strong><?=$MODULE_OUTPUT["group"]["specialities_code"]?> - <?=$MODULE_OUTPUT["group"]["spec"]?></strong></h2>
		<p>������ <strong><?=$MODULE_OUTPUT["group"]["name"]?></strong>, <?=$MODULE_OUTPUT["group"]["year"]?> ����<br /> <strong>���������: <? foreach ($subgroups as $snum => $sg) { if ($_GET["subgr"]!=$snum) { ?><a href="?<?if(!$MODULE_OUTPUT["show_mode"]){?>gr=<?=$_GET["gr"]?>&<?}?>subgr=<?=$snum?>"><?=$sg?></a> <? } else echo $sg." "; } ?></strong></p><p><strong><?if(!empty($MODULE_OUTPUT["odd"]) && !empty($MODULE_OUTPUT["even"]) || isset($_GET["week"])){?><a target="_blank" href="/timetable-print/?gr=z<?=$MODULE_OUTPUT["group"]["id"]?>&info=1<?=(isset($_GET["week"])) ? "&weeks=z".$_GET["week"] : ""?>">�����������</a><?}?></strong></p>
		<h2 style="text-align: center;<? if (!$MODULE_OUTPUT["dates"]["actual_from"] && !$MODULE_OUTPUT["dates"]["actual_to"]) { ?> display: none;<? } ?>"><strong>���������� ������� �� ������ � <?=implode(".",array_reverse(explode("-",$MODULE_OUTPUT["dates"]["actual_from"])))?> �� <?=implode(".",array_reverse(explode("-",$MODULE_OUTPUT["dates"]["actual_to"])))?></strong></h2>
		<? } else { ?>
			<br /><h2 style="text-align: center;"><strong>���������� ������� �� ������ � <input data-id="<?=$_GET["gr"]?>" type="text" id="actual_from" readonly="readonly" name="actual_from<?=($_GET["week"]!="") ? "_".$_GET["week"] : ""?>" value="<?=implode(".",array_reverse(explode("-",$MODULE_OUTPUT["dates"]["actual_from"])))?>" style="100px;" /> �� <input data-id="<?=$_GET["gr"]?>" type="text" id="actual_to" readonly="readonly" name="actual_to<?=($_GET["week"]!="") ? "_".$_GET["week"] : ""?>" value="<?=implode(".",array_reverse(explode("-",$MODULE_OUTPUT["dates"]["actual_to"])))?>" style="100px;" /></strong></h2>
		<? } ?>
		<select style="display:none;" id="subjlist"><optgroup><option selected="selected"></option>
									<? $kaf = ""; $fac = "";  foreach ($MODULE_OUTPUT["subjects"] as $k => $v) { 
									if ($fac != $v["fac"]) { $fac = $v["fac"]; ?>
										<optgroup label="<?=$fac?>">
									<? } ?>
									<?if ($kaf != $v["kaf"] && empty($v["kaf"])) { $kaf = "���������� �� ����������� � �������"; ?>
										<option disabled="disabled"></option><optgroup label="<?=$kaf?>">
									<? } ?>
									<?if ($kaf != $v["kaf"]) { $kaf = $v["kaf"]; ?>
										<option disabled="disabled"></option><optgroup label="<?=$kaf?>">
									<? } ?>
			<option value="<?=$k?>"<? if($curpair && $curpair["subject_id"] == $k) { ?> selected="selected"<? } ?>><?=$v["name"]?></option><? } ?></optgroup></optgroup>
		</select>
		<select style="display:none;" id="teachlist"><option></option><? foreach ($MODULE_OUTPUT["teachers"] as $k => $v) { ?><option value="<?=$k?>"><?=$v["last_name"]?> <?=substr($v["name"],0,1)?>. <?=substr($v["patronymic"],0,1)?>.</option><? } ?>
		</select>
		<select style="display:none;" id="audlist"><option></option><? foreach ($MODULE_OUTPUT["auds"] as $k => $v) { ?><option value="<?=$k?>"><?=$v["label"]?>-<?=$v["aud_name"]?></option><? } ?>
		</select>

			<?
			foreach ($days as $day => $dname) {
				if (/*$MODULE_OUTPUT["mode"] == "timetable_make" || count($MODULE_OUTPUT[$wname][$day])*/ 1 /*������� ��� ����*/) {
				?><h2><?=$dname?></h2>
				<table width="100%" style="text-align: center;">
					<tr>
						<th width="2%">#</th>
						<th width="5%">����</th>
						<th width="3%">������</th>
						<th width="25%">����������</th>
						<th width="5%">���</th>
						<th<? if ($MODULE_OUTPUT["mode"]=="timetable_make") { ?> style="width: 5%"<? } else { ?> width="5%"<? } ?>>���������</th>
						<th width="12%">���������</th>
						<th width="23%">�������������</th>
						<? if ($MODULE_OUTPUT["mode"]=="timetable_make") { ?><th width="10%">�����������</th><? } ?>
						<? if ($MODULE_OUTPUT["mode"]=="timetable_make") { ?><th width="5%">����������</th><? } ?>
						<? if ($MODULE_OUTPUT["mode"]=="timetable_make") { ?><th width="10%">�����������</th><? } ?>
					</tr>
					<? $rownum = 0;
					foreach ($pairs as $pnum => $pr) {
					foreach ($weeks as $wnum => $wname) {
						$pairzz = array(0 => "", 1 => "", 2 => ""); //��� ������ ��������
						if (isset($MODULE_OUTPUT[$wname][$day][$pnum])) {
							$pairzz[0] = $MODULE_OUTPUT[$wname][$day][$pnum];
							if (isset($MODULE_OUTPUT["doubles"][$pairzz[0]["id"]]))
								$pairzz[1] = $MODULE_OUTPUT["doubles"][$pairzz[0]["id"]];
							if (isset($MODULE_OUTPUT["triples"][$pairzz[0]["id"]]))
								$pairzz[2] = $MODULE_OUTPUT["triples"][$pairzz[0]["id"]];
						}

						$rowspan_count = 2;
						if (isset($MODULE_OUTPUT["doubles"][$MODULE_OUTPUT["even"][$day][$pnum]["id"]]))
							$rowspan_count++;
						if (isset($MODULE_OUTPUT["doubles"][$MODULE_OUTPUT["odd"][$day][$pnum]["id"]]))
							$rowspan_count++;
						if (isset($MODULE_OUTPUT["triples"][$MODULE_OUTPUT["odd"][$day][$pnum]["id"]]))
							$rowspan_count++;
						if (isset($MODULE_OUTPUT["triples"][$MODULE_OUTPUT["even"][$day][$pnum]["id"]]))
							$rowspan_count++;

						foreach ($pairzz as $is_double => $curpair) {
						$hide = ($is_double && !$curpair) ? "display: none; " : "";
						$clr = ($rownum % 2) ? 'cff' : 'def';
						$trstyle = ($rownum % 2) ? " style='". $hide ."background-color: #".$clr.";'" : " style='". $hide ."background-color: #".$clr.";'"; $rownum++;
						if ($MODULE_OUTPUT["mode"] == "timetable_make") { ?>
							<tr<?=$trstyle?> id="tr_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_double" : "_triple") ?>">
								<? if(!isset($arr[$day][$pnum])) { ?><td  valign="middle" rowspan="<?=$rowspan_count?>" class="rowspan_<?=$day?>_<?=$pnum?>"><?=$pnum+1?></td><td valign="middle" rowspan="<?=$rowspan_count?>" class="rowspan_<?=$day?>_<?=$pnum?>"><?=$pr?></td><? } ?>
								<td <?=(($wnum==0) && ($is_double == 0)) ? "bgcolor='#eaeaea'" : ""; ?><?=($is_double != 0) ? "bgcolor='#cff'" : ""; ?>><? echo ($wname=="even") ? '���' : '�����'; ?><p align="center"><img class="clear_pair" id="clear_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>" title="��������" style="cursor: pointer;" src="/themes/styles/delete.png" onclick="if(confirm('�������� ����?')) clear_row('<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>');"; /></p></td>
								<td <?=(($wnum==0) && ($is_double == 0)) ? "bgcolor='#eaeaea'" : ""; ?><?=($is_double != 0) ? "bgcolor='#cff'" : ""; ?> valign="top"><select style="width: 200px;" class="subjs" def="<?=$curpair["subject_id"]?>" name="subj_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>" id="subj_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>"><option value="<?=$curpair["subject_id"]?>" selected><?=$MODULE_OUTPUT["subjects"][$curpair["subject_id"]]["name"]?></option></select><br /><input type="text" class="search" style="width: 100px; font-style: italic; color: #aaa;" value="������� ����..." onfocus="if (this.value=='������� ����...') this.value='';" onkeyup="select_filter('subj_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>', this.value, 'subjlist');" />
									<? if ($is_double==0) { ?>
										<p style="display:<?=!$pairzz[2] && !$pairzz[1]?"block":"none"?>" id="addouble_<?=$wnum?>_<?=$day?>_<?=$pnum?>"><a cursor="pointer" onclick="show_doublepair('<?=$wnum?>','<?=$day?>_<?=$pnum?>');"><small>�������� ����</small></a></p>
									<? } elseif ($is_double==1) { ?>
										<p style="display:<?=!$pairzz[2]?"block":"none"?>" id="adtriple_<?=$wnum?>_<?=$day?>_<?=$pnum?>"><a cursor="pointer" onclick="show_triplepair('<?=$wnum?>','<?=$day?>_<?=$pnum?>');"><small>�������� ����</small></a></p>
										<p style="display:<?=$pairzz[1]?"block":"none"?>" id="deldouble_<?=$wnum?>_<?=$day?>_<?=$pnum?>"><a cursor="pointer" onclick="hide_doublepair('<?=$wnum?>','<?=$day?>_<?=$pnum?>');"><small>������� ����</small></a></p>
									<? } elseif($is_double==2) { ?>
										<p id="deldouble_<?=$wnum?>_<?=$day?>_<?=$pnum?>"><a cursor="pointer" onclick="hide_triplepair('<?=$wnum?>','<?=$day?>_<?=$pnum?>');"><small>������� ����</small></a></p><? } ?></td>
									
									
									
								<td <?=(($wnum==0) && ($is_double == 0)) ? "bgcolor='#eaeaea'" : ""; ?><?=($is_double != 0) ? "bgcolor='#cff'" : ""; ?>  valign="top"><select style="max-width: 150px" class="type" id="type_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>" onchange="subgroup_switch(this.value,<?=$wnum?>,<?=$day?>,<?=$pnum?>,<?=$is_double==0  ? "0" : ($is_double==1 ? "1" : "2") ?>);" name="type_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>"><? foreach ($types as $tid => $type) { ?><option value="<?=$tid?>"<? if($curpair && $curpair["type"] == $tid) { ?> selected="selected"<? } ?>><?=$type?></option><? } ?></select></td>

								<td <?=(($wnum==0) && ($is_double == 0)) ? "bgcolor='#eaeaea'" : ""; ?><?=($is_double != 0) ? "bgcolor='#cff'" : ""; ?>  valign="top"><span<? if (!$curpair || !$curpair["type"]) $dispnone = ' display: none;'; else $dispnone = ''; ?> style="<?=$dispnone?>" id="subgr_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>"><? foreach ($subgroups as $snum => $subgr) { ?><input type="radio" class="subgr"  name="subgr_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>" value="<?=$snum?>"<? if($curpair && $curpair["subgroup"] == $snum) { ?> checked="checked"<? } ?> /> <?=$subgr?><br/><? } ?></span></td>

								<td <?=(($wnum==0) && ($is_double == 0)) ? "bgcolor='#eaeaea'" : ""; ?><?=($is_double != 0) ? "bgcolor='#cff'" : ""; ?>  valign="top"><select class="aud_list" style="width: 120px;" id="aud_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>" name="aud_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>"><option></option><? //foreach ($MODULE_OUTPUT["auds"] as $k => $v) { ?><option value="<?=$k?>"><?=$v["label"]?> - <?=$v["aud_name"]?></option><? //} ?></select><br /><input class="search" type="text" style="width: 100px; font-style: italic; color: #aaa;" value="������� ����..." onfocus="if (this.value=='������� ����...') this.value='';" onkeyup="select_filter('aud_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>', this.value, 'audlist');" /> <input type="button" class="aud_add" id="addaud_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>"  value="��������" /><input type="hidden" class="values" name="aud_ids_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>" id="aud_ids_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>" value="<? echo ($curpair && $curpair['auditorium_id']) ? $curpair['auditorium_id'] : ''; ?>" />
									<p id="aud_names_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>">
										<? if ($curpair && $curpair["auditorium_id"]) { $tchs = explode(";", $curpair["auditorium_id"]); 
										foreach ($tchs as $tval) if ($tval) echo "<span id='aud_id_".$tval."_".$wnum."_".$day."_".$pnum.($is_double==0  ? "" : ($is_double==1 ? "_d" : "_t"))."'>- ".$MODULE_OUTPUT["auds"][$tval]["label"]." - ". $MODULE_OUTPUT["auds"][$tval]["aud_name"] ." [<a style='cursor:pointer;' class='del_aud' id='delaud_".$tval."_".$wnum."_".$day."_".$pnum.($is_double==0  ? "" : ($is_double==1 ? "_d" : "_t"))."' onclick='delfromlist(\"aud\", \"".$wnum."_".$day."_".$pnum.($is_double==0  ? "" : ($is_double==1 ? "_d" : "_t"))."\", $tval)'>x</a>]<br /></span>";
										} ?>
									</p></td>

								<td <?=(($wnum==0) && ($is_double == 0)) ? "bgcolor='#eaeaea'" : ""; ?><?=($is_double != 0) ? "bgcolor='#cff'" : ""; ?> valign="top"><select class="teach_list" style="width: 120px;" id="teach_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>" name="teach_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>"><option></option><? //foreach ($MODULE_OUTPUT["teachers"] as $k => $v) { ?><option value="<?=$k?>"><?=$v["last_name"]?> <?=substr($v["name"],0,1)?>. <?=substr($v["patronymic"],0,1)?>.</option><? //} ?></select><br /><input class="search" type="text" style="width: 100px; font-style: italic; color: #aaa;" value="������� ����..." onfocus="if (this.value=='������� ����...') this.value='';" onkeyup="select_filter('teach_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>', this.value, 'teachlist');" /> <input type="button" class="teach_add" id="addteach_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>" value="��������" /><input class="values" type="hidden" name="teach_ids_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>" id="teach_ids_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>" value="<? echo ($curpair && $curpair['teacher_id']) ? $curpair['teacher_id'] : ''; ?>" />
									<p id="teach_names_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>">
										<? if ($curpair && $curpair["teacher_id"]) { $tchs = explode(";", $curpair["teacher_id"]); 
										foreach ($tchs as $tval) if ($tval) echo "<span id='teach_id_".$tval."_".$wnum."_".$day."_".$pnum.($is_double==0  ? "" : ($is_double==1 ? "_d" : "_t"))."'>- ".$MODULE_OUTPUT["teachers"][$tval]["last_name"]." ".substr($MODULE_OUTPUT["teachers"][$tval]["name"], 0, 1).". ".substr($MODULE_OUTPUT["teachers"][$tval]["patronymic"], 0, 1).". [<a style='cursor:pointer;' class='del_teach' id='delteach_".$tval."_".$wnum."_".$day."_".$pnum.($is_double==0  ? "" : ($is_double==1 ? "_d" : "_t"))."' onclick='delfromlist(\"teach\", \"".$wnum."_".$day."_".$pnum.($is_double==0  ? "" : ($is_double==1 ? "_d" : "_t"))."\", $tval)'>x</a>]<br /></span>";
										} ?>
									</p>
								</td>

								<td <?=(($wnum==0) && ($is_double == 0)) ? "bgcolor='#eaeaea'" : ""; ?><?=($is_double != 0) ? "bgcolor='#cff'" : ""; ?> valign="top">
									c <input type="text" data-id="<?=$_GET['gr']?>" class="comm" style="width: 100px;" readonly="readonly" id="comment_from_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>" name="<?=$_GET['week']?>" value="<? echo $curpair["comment_from"]=='0000-00-00' ? '' : implode(".",array_reverse(explode("-",$curpair["comment_from"])))?>" /> �� <input type="text" data-id="<?=$_GET['gr']?>" style="width: 100px;" id="comment_to_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>" class="comm" name="<?=$_GET['week']?>" readonly="readonly" value="<? echo $curpair["comment_to"]=='0000-00-00' ? '' : implode(".",array_reverse(explode("-",$curpair["comment_to"])))?>" /></td>
								<? if ($MODULE_OUTPUT["mode"]=="timetable_make") {?><td <?=(($wnum==0) && ($is_double == 0)) ? "bgcolor='#eaeaea'" : ""; ?><?=($is_double != 0) ? "bgcolor='#cff'" : ""; ?> valign="top">
										<input type="text" value="<?=$curpair["comment"]?>" class="add_comment_input" id="addcomm_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>"><br>
										<input type="submit" class="add_comment" value="���������" id="comm_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>">
								</td><?}?>
								<? if ($MODULE_OUTPUT["mode"]=="timetable_make") { ?><td <?=(($wnum==0) && ($is_double == 0)) ? "bgcolor='#eaeaea'" : ""; ?><?=($is_double != 0) ? "bgcolor='#cff'" : ""; ?> class="show_button" valign="center"><div class="output_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>" style="color: green"></div><select size="5" class="copypair_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>" multiple="multiple" style="max-width: 70px;" name="copy_<?=$wnum?>_<?=$day?>_<?=$pnum?>[]"><? foreach ($MODULE_OUTPUT["potok"] as $key => $val) { ?><option value="<?=$key?>"><?=$val?></option><? } ?></select><!--<input type="checkbox" name="copy_<?=$wnum?>_<?=$day?>_<?=$pnum?>" onclick="if(this.checked) { if (!confirm('��������! ������� ��� �������, �� �������� ���������� ���� ���� ��� ���� ����� ������. �� �������?')) return false; }" />--><input style="display: inline-block; opacity: 0" type="button" class="copy" id="copypair_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>" value="�����������" /></td><? } ?>
							</tr>
						<? } else {  /* timetable_show */
							?>
							<tr<?=$trstyle?>>
								<? if(!isset($arr[$day][$pnum])) { ?><td valign="middle" rowspan="<?=$rowspan_count?>"><?=$pnum+1?></td>
								<td valign="middle" rowspan="<?=$rowspan_count?>"><?=$pr?></td><? } ?>
								<td <?=(($wnum==0) && ($is_double == 0)) ? "bgcolor='#eaeaea'" : ""; ?><?=($is_double != 0) ? "bgcolor='#cff'" : ""; ?> ><? echo ($wname=="even") ? '���' : '�����'; ?></td>
								<td <?=(($wnum==0) && ($is_double == 0)) ? "bgcolor='#eaeaea'" : ""; ?><?=($is_double != 0) ? "bgcolor='#cff'" : ""; ?> ><?=$curpair["subj"]?><? if ($curpair["comment_from"] && $curpair["comment_from"]!='00.00.0000') { ?><br /><span style="font-size: 12px;">� <?=$curpair["comment_from"]?> �� <?=$curpair["comment_to"]?></span><? } ?></td>
								<td <?=(($wnum==0) && ($is_double == 0)) ? "bgcolor='#eaeaea'" : ""; ?><?=($is_double != 0) ? "bgcolor='#cff'" : ""; ?> ><?=$types[$curpair["type"]]?></td>
								<td <?=(($wnum==0) && ($is_double == 0)) ? "bgcolor='#eaeaea'" : ""; ?><?=($is_double != 0) ? "bgcolor='#cff'" : ""; ?> ><? echo $curpair["subgroup"] ? $subgroups[$curpair["subgroup"]] : ""; ?></td>
								<td <?=(($wnum==0) && ($is_double == 0)) ? "bgcolor='#eaeaea'" : ""; ?><?=($is_double != 0) ? "bgcolor='#cff'" : ""; ?> ><? foreach ($curpair["auds"] as $aud) { ?><?=$aud["build"]." - ".$aud["name"]?><br /><? } ?></td>
								<td <?=(($wnum==0) && ($is_double == 0)) ? "bgcolor='#eaeaea'" : ""; ?><?=($is_double != 0) ? "bgcolor='#cff'" : ""; ?> ><? foreach ($curpair["teachs"] as $teach) { ?><a href="/people/<?=$teach["people_id"]?>/" target="_blank"><?=$teach["p2"]." ".substr($teach["p0"],0,1).". ".substr($teach["p1"],0,1)?>.</a><br /><? } ?></td>
							</tr>
						<? } ?>
					<? $arr[$day][$pnum] = 1; } } } ?>
				</table><?
			 } }
		
	?></div><!-- ��������� div ��������� ����������� ���������� --><?
	if ($MODULE_OUTPUT["mode"]=="timetable_make") { ?>
	
	
	<input type="submit" style="display: none" id="timetable_save_button" value="���������" />
	<br/>
	<br/>
	<a href="/raspisanie/?gr=<?=$_GET["gr"]?><?=($_GET["week"]!="") ? "&week=".$_GET["week"] : ""?>">���������� ����������</a>
	<? } else { ?>
	
	<div class="right_cont">
					<div id="timetable_data">
						�������&nbsp;<?=$now["mday"]?>&nbsp;<?=$months[$now["mon"]-1]?>&nbsp;<?=$now["year"]?>&nbsp;�.
						<?=$days[$now["wday"]-1]?>.&nbsp;������&nbsp;<?=$week?>
					</div>
					
					
					
                    <div id="designation">
						<h3>����������� ��������</h3>
						<ul>
<?php			foreach($MODULE_OUTPUT["buildings"] as $buildings) { ?>
							<li>
								<span><?=$buildings["label"]?></span>
								<p><?=$buildings["name"]?></p>
							</li>    
<?php			} ?>
						</ul>
                    </div>
				</div>
	<? } ?>
	</form>
	<?
	}
  break;
  }





}


?>
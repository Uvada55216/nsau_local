<?php 
// version: 3.26
// date: 2014-08-18
//$MODULE_OUTPUT = $MODULE_DATA["output"];

$MODULE_MESSAGES = array(
    101 => "������� ������� ��������.",
    102 => "������� �������� ������������� ������� ��������.",
    103 => "������������� ������� ���������.",
    104 => "������� �������� ������������� ������� ������.",
    105 => "������������� � �� �������� �������� ������� �������.",
    106 => "�������������� ������ �������.",
    301 => "������� �� ��������.",
    302 => "������� �� ��������. ���� ������� 2 ��������� ������.",
    303 => "������� �� ��������. ���������� ������ ������.",
    304 => "������ � ����� ������� ��� ����������.",
    305 => "���������� ������� ������ ������. ��� �� �����.",
    306 => "������. ���� ��������� �� ��� ����.",
    307 => "������� �������� ������������� �� ��������.",
    308 => "������������� �� ���������.",
    309 => "������� �������� ������������� �� ������.",
    310 => "������������� � �� �������� �������� �� �������.",
    311 => "������ ��� ���������� ������ �������� �������������.",
    312 => "������ ��� �������������� �������������.",
    313 => "������! �� ������� �������� ���������� ��� �� �������.",
    314 => "������! �� ������ ����� ������.",
    315 => "������! ������� �������, ��� ��� ��������.",
    316 => "������� �� ������.",
    317 => "������! ������� ������� � ���.",
    318 => "������! ������ ���� �������� ���� �� ���� �������.",
    319 => "������! �� ������ ����� ��� ������, ��� ������ �� ���������.",
    320 => "������! ����� ������ ����������.",
    321 => "���������� ������� ���������.",
    322 => "������ ���������� ������������ � ����������, ���� � ��� ���� ������������ �������.",
    323 => "������! ������������� �� �������.",
    324 => "������! ��������� �� ��������.",
    325 => "������! ������ �� ������.",
    326 => "������! ������ ������������� �� �������.",
    327 => "������! ������ ����� �����.",
    328 => "������! ������� ��� �����������.",
    329 => "������! ������� ����� ������������� ������.",
    330 => "������! ������� ���� e-mail.",
    331 => "������! �� ������ ������ e-mail.",
    332 => "������! ������������ � ������ e-mail ��� ����������.",
    333 => "��������� �������� ���� � ����������� ������ �����������.",
    334 => "������! ������ ������������� ��� ����������",
    335 => "������! �� ������� ������ ��������",
    336 => "������! ����� ������ ��� ����������!",
    337 => "������! �������� ���������� ������ ��������� �� ����� ���� ��������!",
    338 => "������ � ����� ������� ��� ����������. ������ ������ ������.",
    339 => "�� ������ ���� ������.",
    340 => "�� ������� ���������.",
    341 => "�� ��������� ����� \"�����������\".",
    401 => "��������� ���� � ������� csv.",
    402 => "�������� ���������� ����� � �����.",
    403 => "�� ������� ������ %1.",
    404 => "� ������ ���� �������� � ������ �����������.",
    405 => "� ����������� ����� ������ ���� �������� ����� �� ���� �������� - ���� �����, ���� �������.",
    406 => "�������� ���������",
    407 => "���� ������ ���� � ��������� Windows-1251",
    410 => "fam",
    411 => "name",
    412 => "otc",
    413 => "dolj",
    414 => "stav",
    415 => "email",
    416 => "pass",
    417 => "repass",
    418 => "repass",
    419 => "kaf",
    420 => "capth",
    421 => "pass4",
    422 => "emailsp",
    423 => "pol",
    424 => "fio",
    425 => "accept_send",
    426 => "email@",
    427 => "emailallready",
    428 => "emailsobaka",   
    429 => "�� ������� ������.",   
    430 => "�� ������� �������.",   
    431 => "�� ������ �������.",   
    432 => "�� ������ �������������.",
    433 => "�� ������ ���� ������.",
    434 => "�� ������ ���� ��������.",

);


if ($MODULE_OUTPUT["messages"]["good"])
{
    $array = array();

    foreach ($MODULE_OUTPUT["messages"]["good"] as $elem)
    {
        if (isset($MODULE_MESSAGES[$elem]))
        {
            $array[] = $MODULE_MESSAGES[$elem];
        }
    }

    if ($array)
    {
        foreach ($array as $elem)
        {
            echo "<p class=\"message\">".$elem."</p>\n";
        }
    }
}

if ($MODULE_OUTPUT["messages"]["bad"] && isset($MODULE_OUTPUT["display_variant"]) && !in_array($MODULE_OUTPUT["display_variant"], array("add_item", "edit_item")))
{
    $array = array();

    foreach ($MODULE_OUTPUT["messages"]["bad"] as $elem)
    {
                if(is_array($elem)) {
                    $msg = $MODULE_MESSAGES[$elem[0]];
                    foreach($elem[1] as $i => $value) {
                        $msg = str_replace("%".$i, $value, $msg);                       
                    }
                    $array[] = $msg;
                } else {
                    if (isset($MODULE_MESSAGES[$elem]))
                    {
                            $array[] = $MODULE_MESSAGES[$elem];
                    }   
                }
    }

    if ($array)
    {
        foreach ($array as $elem)
        {
            echo "<p class=\"message red\">".$elem."</p>\n";
        }
    }
}




switch ($EE["language"])
{
    case "en":
        $LANGUAGE["news"] = array(
            "read_all_news" => "Read all news",
            "all_news" => "All news",
            "latest_news" => "News",
            "date_format" => "d/m/Y",
            "datetime_format" => "d/m/Y g:i:s A",
            "read_more" => "Read more",
            "more" => "more",
            "prev" => "prev",
            "previous_page" => "Previous page",
            "next" => "next",
            "next_page" => "Next page",
            "pages" => "Pages",
            "add_comment" => "Add comment",
            "comments" => "Comments",
            "no_comments" => "There are no comments for this item currently. Your comment could be the first one!",
            "name" => "Name",
            "location" => "Location",
            "email" => "E-mail",
            "your_comment" => "Your comment",
            "confirmation_code" => "Confirmation code",
            "confirmation_code_hint" => "Please write in a code displayed on the picture.",
            "obligatory_field" => "Obligatory field",
            "all_publications" => "all publications",
            "read_all_publications" => "Read all publications",
            );
        break;

    default:
        $LANGUAGE["news"] = array(
            "read_all_news" => "������ ��� �������",
            "all_news" => "��� ���������",
            "latest_news" => "��������� ���������",
            "date_format" => "d.m.Y",
            "datetime_format" => "d.m.Y, H:i:s",
            "read_more" => "������ ���������",
            "more" => "���������",
            "prev" => "����������",
            "previous_page" => "���������� ��������",
            "next" => "���������",
            "next_page" => "��������� ��������",
            "pages" => "��������",
            "add_comment" => "�������� �����������",
            "comments" => "�����������",
            "no_comments" => "�� ������ ������ �&nbsp;����� ��������� ��� ��&nbsp;������ �����������. �� ������ �������� ������!",
            "name" => "���",
            "location" => "�����",
            "email" => "E-mail",
            "your_comment" => "��� �����������",
            "confirmation_code" => "��� �������������",
            "confirmation_code_hint" => "����������, ������� �������� ���, ����������� �� ��������.",
            "obligatory_field" => "����, ������������ ��� ����������",
            "all_publications" => "��� �������",
            "read_all_publications" => "������ ��� �������",
            );
        break;
}

switch($MODULE_OUTPUT["mode"])
{


        case "table_generator": { ?>


<!-- <a name="Table11" style="height: 1px; display: block; padding-top: 25px;"></a>
<h1>������� 11</h1>
<table class="nsau_table" itemprop="adeduOp">
    <thead>
        <tr>
            <th>���</th>
            <th>������������ �������������, ����������� ����������</th>
            <th>��� ������ ����������</th>
            <th>������� ��������� (��� �������������� ��������������� �������� ������� ������� ��������������� �ϻ)</th>
            <th>����� ���������</th>
            <th>����� ����� �������� (�������� ������� � �. �.)     </th>
            <th>����-������� ����� �������� (�. �.)</th>
            <th>������� ����� �������� (�. �.)</th>
        </tr>
        <tr>
            <th>1</th>
            <th>2</th>
            <th>3</th>
            <th>4</th>
            <th>5</th>
            <th>6</th>
            <th>7</th>
            <th>8</th>
        </tr>
    </thead>
    <tbody>
        <?foreach($MODULE_OUTPUT['data'] as $item) {?>
            <?if(!empty($item['profile'])) {?>
                <?foreach ($item['profile'] as $profile) {?>
                    <?for($i=1;$i<=$item['years'];$i++){?>
                        <tr itemprop="eduChislen">
                            <td itemprop="eduCode"><?=$item['code']?></td>
                            <td itemprop="eduName"><?=$item['name']?></td>
                            <td><?=$item['year_open']?></td>
                            <td><?=$profile?></td>
                            <td>�� <?=$i?> ��� ��������</td>
                            <td itemprop="eduForm"></td>
                            <td itemprop="eduForm"></td>
                            <td itemprop="eduForm"></td>
                        </tr>
                    <?}?>
                <?}?>
            <?}?>
        <?}?>
    </tbody>
</table> -->
<!-- <a name="Table12" style="height: 1px; display: block; padding-top: 25px;"></a>
<h1>������� 12</h1>
<table class="nsau_table" itemprop="eduPr">
    <thead>
        <tr>
            <th itemprop="eduCode" rowspan="2">���</th>
            <th itemprop="eduName" rowspan="2">������������ �������������, ����������� ����������</th>
            <th rowspan="2">��� ������ ����������</th>
            <th rowspan="2">������� ��������� (��� �������������� ��������������� �������� ������� ������� ��������������� �ϻ)</th>
            <th itemprop="eduForm" rowspan="2">����������� ����� ��������:1) �����;2) ����-�������;3) �������</th>
            <th itemprop="educationPraktik" rowspan="1" colspan='3'>������� �������� (�.�.)</th>
        </tr>
        <tr>
            <th>�������</th>
            <th>����������������</th>
            <th>������������� �������� ��� ���������� ��������� ���������������� ������</th>
        </tr>
        <tr>
            <th>1</th>
            <th>2</th>
            <th>3</th>
            <th>4</th>
            <th>5</th>
            <th>6</th>
            <th>7</th>
            <th>8</th>
        </tr>
    </thead>
    <tbody>
        <tr>
        <?foreach($MODULE_OUTPUT['data'] as $item) {?>
            <?if(!empty($item['profile'])) {?>
                <?foreach ($item['profile'] as $profile) {?>
                   
                        <tr>
                            <td itemprop="eduCode"><?=$item['code']?></td>
                            <td itemprop="eduName"><?=$item['name']?></td>
                            <td><?=$profile['year']?></td>
                            <td><?=$profile['name']?></td>
                            <td itemprop="eduForm"><?=($profile['form']==1) ? '�����' : (($profile['form']==2) ? '�������' : '����-�������')?></td>
                            <td itemprop="educationPraktik"></td>
                            <td itemprop="educationPraktik"></td>
                            <td itemprop="educationPraktik"></td>
                        </tr>
                <?}?>
            <?}?>
        <?}?>
        </tr>
    </tbody>
</table> -->
<!-- ========================================================= ���������� ��������, �������������� � ���������� ================================-->
<!-- <a name="Table7" style="height: 1px; display: block; padding-top: 25px;"></a> -->


        <?//CF::Debug($MODULE_OUTPUT['data'])?>
   <? }
    break;










        
        case "department_subjects": {?>
            <h1>������������� ����������</h1>
            <?foreach($MODULE_OUTPUT["subjects"] as $subject) {?>
                <?=$subject["name"]?><br />
            <?}?>
            <br />
        <?} break;






        ?>
    
        <? case "faculties": { ?>
        <h1><a href="/department/">������������� ����</a></h1>
        <ul>
<?php   foreach($MODULE_OUTPUT["faculties"] as $faculty) { ?>
            <li class="faculty">
<?php       if($faculty["link"]) { ?>
                <a href="<?=( ($faculty["link"][0] == '/' || substr($faculty["link"],0,7) == 'http://') ? '' : '/' ). $faculty["link"]?>" title="<?=$faculty["comment"]?>">
<?php       } ?>
                    <?=$faculty["name"]?>
<?php       if($faculty["link"]) { ?>
                </a>
<?php       } ?>
            </li>
<?php   } ?>
        </ul>
<?php   if (!isset($MODULE_OUTPUT["hide_deps_href"])) {?>
        <a href="/department/">��� �������������</a>
<?php   }
    }
    break;

    case "speciality":
    {
        if (!empty($MODULE_OUTPUT["speciality"])){

	        foreach($MODULE_OUTPUT["faculty"] as $faculty){
		        $faculty_ids[] = $faculty['id'];
	        }

	        foreach($MODULE_OUTPUT["speciality"] as $special){
		        if(!empty($special)){
			        if(isset($_GET["node"]) && isset($_GET["action"])){

			        } else {
				        if(empty($special["description"]) && empty($MODULE_OUTPUT["speciality_edit"])){ ?>
                            <p>���������� � ������ ������������� �����������</p><br/>
				        <?
				        } else {
					        if(!empty($MODULE_OUTPUT["speciality_edit"])){

						        foreach($MODULE_OUTPUT["speciality_edit"][$special['id']] as $type_sp){
							        { ?>
                                        <div class="container container--2">
                                            <h2 class="title">
										        <?=$special["code"] . " " . $special["name"]?>
                                            </h2>
                                        </div>
                                        <div class="banner">
                                            <?php if(!empty($MODULE_OUTPUT["edu_banner"])) {
                                                foreach($MODULE_OUTPUT["edu_banner"] as $bann) { ?>
                                                    <div class="banner__item">
                                                        <div class="container">
                                                            <?=($type_sp["has_vacancies"]) ? '<div class="banner__text">������� �����</div>' : '<div class="banner__text">����� �� �������</div>'?>
                                                        </div>

                                                        <?php switch ($bann["id_ban"]) {
                                                            case 1:
                                                                echo ($bann['value']!='') ? '<img src="'. $bann['value'].'" alt=""/>' : '<img src="/images/012_nsau.jpg" alt=""/>';
                                                                break;
	                                                        case 2:
		                                                        echo ($bann['value']!='') ? '<img src="'. $bann['value'].'" alt=""/>' : '<img src="/images/017_nsau.jpg" alt=""/>';
		                                                        break;
	                                                        case 3:
		                                                        echo ($bann['value']!='') ? '<img src="'. $bann['value'].'" alt=""/>' : '<img src="/images/016_nsau.jpg" alt=""/>';
		                                                        break;
                                                            default:
                                                                break;
                                                        } ?>

                                                    </div>
                                            <? }
                                            } else { ?>
                                                <div class="banner__item">
                                                    <div class="container">
                                                        <?=($type_sp["has_vacancies"]) ? '<div class="banner__text">������� �����</div>' : '<div class="banner__text">����� �� �������</div>'?>
                                                    </div>

                                                    <img src="/images/012_nsau.jpg" alt=""/>
                                                </div>

                                                <div class="banner__item">
                                                    <div class="container">
                                                        <?=($type_sp["has_vacancies"]) ? '<div class="banner__text">������� �����</div>' : '<div class="banner__text">����� �� �������</div>'?>
                                                    </div>

                                                    <img src="/images/017_nsau.jpg" alt=""/>
                                                </div>

                                                <div class="banner__item">
                                                    <div class="container">
                                                        <?=($type_sp["has_vacancies"]) ? '<div class="banner__text">������� �����</div>' : '<div class="banner__text">����� �� �������</div>'?>
                                                    </div>

                                                    <img src="/images/016_nsau.jpg" alt=""/>
                                                </div>
                                            <?php } ?>
                                        </div>

                                        <div class="info">
                                            <div class="container container--2">
                                                <div class="info__wrap">
                                                    <?php if(!empty($MODULE_OUTPUT["edu_plan"])) { ?>
                                                        <div class="info__item">
                                                            <div class="info__title">
                                                                ���� ������
                                                            </div>
                                                            <div class="info__content">
                                                                <?php foreach($MODULE_OUTPUT["edu_plan"] as $plan=>$val){ ?>
                                                                    <div class="info__num">
                                                                        <div class="info__num-circle">
                                                                            <?=$val?>
                                                                        </div>
                                                                        <div class="info__num-text">
                                                                            <?= ($plan == 'budget') ? '���������' : '����������'?> <br>
                                                                            �����
                                                                        </div>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                    <? if(!empty($MODULE_OUTPUT["edu_price"])) { ?>
                                                    <div class="info__item info__price">
                                                        <div class="info__title">
                                                            ��������� �� �������
                                                        </div>
                                                        <div class="info__content">
                                                            <? foreach($MODULE_OUTPUT["edu_price"] as $price){
															if(!empty($price['value'])){
																$num = number_format((float)$price['value'], 0, ',', ' ');
																?>
                                                            <div>
                                                                <strong>
                                                                    <?=$price['name']?> �����
                                                                </strong>
                                                                <div class="info__sum">
                                                                    <?=$num?> ���.
                                                                </div>
                                                            </div>
                                                            <? }
														} ?>
                                                        </div>
                                                    </div>
                                                    <? } ?>
											        <?
											        ksort($type_sp["exams"]);
											        $check_spo = explode(".", $special["code"]);
                                                    if(isset($type_sp["exams"]) && count($type_sp["exams"]) && $type_sp["type"] != "magistracy"){ ?>

                                                            <div class="info__item">
                                                                <div class="info__title">
                                                                    ����������� ����
                                                                </div>
                                                                <div class="info__content">
																	<? foreach($type_sp["exams"] as $key => $exam){ ?>
																		<div class="info__num info__num--2">
																			<div class="info__num-circle">
																				<?=$exam["min_vyz"]?>
																			</div>
																			<div class="info__num-text">
                                                                                <?=stristr($exam["name"], '(')? stristr($exam["name"], '(', true): $exam["name"]?>
																			</div>
																		</div>
																	<? }
																	if(!empty($MODULE_OUTPUT['exams_additonal'])){
																		foreach($MODULE_OUTPUT["exams_additonal"] as $key => $add_exam){ ?>
																			<div class="info__num info__num--2">
																				<div class="info__num-circle">
																					<?=$add_exam["min_vyz"]?>
																				</div>
																				<div class="info__num-text">
                                                                                    <?=stristr($add_exam["name"], '(')? stristr($add_exam["name"], '(', true): $add_exam["name"]?>
																				</div>
																			</div>
																		<? }
																	}
																	?>
																</div>
                                                            </div>

                                                            <div class="info__item info__exam">
																<div class="info__title">
																	��������
																</div>

																<div class="info__content ">
																	<?if ($type_sp["type"] == "secondary") { ?>
																		<strong>������������:</strong>
																		<span>������� ���� ���������</span>
																	<?}?>
																	<? foreach($type_sp["exams"] as $key => $exam){
																		switch ($key){
																			case(1): ?>
																				<strong>������������:</strong>
																				<span><?=$exam['name']?></span>
																				<? break;
																			case(2): ?>
																				<strong class="info__exam-title">������������:</strong>
																				<span><?=$exam['name']?></span>
																				<? break;
																			case(3): ?>
																				<strong class="info__exam-title">�� ������:</strong>
																				<span><?=$exam['name']?></span>
																				<? break;
																			default:
																				break;
																		}
																	}
																	if(!empty($MODULE_OUTPUT['exams_additonal'])){
																		if(count($MODULE_OUTPUT['exams_additonal']) == 1){ ?>
																			<strong class="info__exam-title">�� ������:</strong>
																			<span><?=$MODULE_OUTPUT['exams_additonal'][0]['name']?></span>
																		<? }else{
																			foreach($MODULE_OUTPUT['exams_additonal'] as $k=>$exam){
																				$nameList[] = ($k > 0) ? strtolower($exam['name']) : $exam['name'];
																			}
																			$strName = implode(', ', $nameList);?>
                                                                            <strong class="info__exam-title">�� ������:</strong>
                                                                            <span><?=$strName?></span>

																		<? }
																	}
																	?>
																</div>
															</div>
														<? } else if ($type_sp["type"] == "secondary") {?>
															<div class="info__item">
                                                                <div class="info__title">��������� ����, �������</div>
                                                                <div class="info__content">������� ���� ���������</div>
                                                            </div>

                                                        <? } else { ?>
                                                            <div class="info__item">
                                                                <div class="info__title">��������� ����, �������</div>
                                                                <div class="info__content">������������� �� �������
                                                                    ����������
                                                                    ����������� ����������
                                                                </div>
                                                            </div>
                                                        <?
                                                    } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="about section">
                                            <div class="container">
                                                <div data-switcher="2" class="switcher">
                                                    �������������

                                                    <div class="switcher__btn">
                                                        <svg width="10" xmlns="http://www.w3.org/2000/svg"
                                                             viewBox="0 0 320 512">
                                                            <path fill="white"
                                                                  d="M207.6 256l107.72-107.72c6.23-6.23 6.23-16.34 0-22.58l-25.03-25.03c-6.23-6.23-16.34-6.23-22.58 0L160 208.4 52.28 100.68c-6.23-6.23-16.34-6.23-22.58 0L4.68 125.7c-6.23 6.23-6.23 16.34 0 22.58L112.4 256 4.68 363.72c-6.23 6.23-6.23 16.34 0 22.58l25.03 25.03c6.23 6.23 16.34 6.23 22.58 0L160 303.6l107.72 107.72c6.23 6.23 16.34 6.23 22.58 0l25.03-25.03c6.23-6.23 6.23-16.34 0-22.58L207.6 256z"
                                                                  class=""></path>
                                                        </svg>
                                                    </div>
                                                </div>
                                                <div id="special" data-switcher="2" class="title--1">
                                                    � �����������
                                                </div>
                                                <div data-switcher="2" class="about__grid">

                                                    <div class="about__grid-item">
                                                        <strong>
                                                            ������� �������������
                                                        </strong>
                                                        <p>
                                                            <a href="<?=$type_sp["fac"]["link"]?>"><?=$type_sp["fac"]["name"]?></a>
                                                        </p>
                                                    </div>

                                                    <div class="about__grid-item">
                                                        <strong>
                                                            ������� �����������
                                                        </strong>
                                                        <p><?=$type_sp["qualification"]?></p>
                                                    </div>

	                                                <? if(isset($MODULE_OUTPUT["profiles"]) && count($MODULE_OUTPUT["profiles"])){ ?>
                                                        <div class="about__grid-item">
                                                            <strong>
                                                                �������
                                                            </strong>
                                                            <ul>
				                                                <? foreach($MODULE_OUTPUT["profiles"] as $tmp => $profile){
					                                                echo "<li>" . $profile['name'] . "</li>";
				                                                } ?>
                                                            </ul>
                                                        </div>
	                                                <? } ?>

								                    <? if(isset($type_sp["data_training"])){ ?>
                                                        <div class="about__grid-item">
                                                            <strong>
                                                                ���� ��������
                                                            </strong>
                                                            <p><?=$type_sp["data_training"]?></p>
                                                        </div>
								                    <? } ?>

                                                </div>

                                                <div class="mobile-line"></div>

                                            </div>
                                        </div>

                                        <?
                                        //if($type_sp["description"]){
		                                //    echo  $type_sp["description"];
	                                    //}
	                                    if($special["description"]){
		                                    echo $special["description"];
	                                    } ?>


                                        <div id="docs" class="docs section">
                                            <div class="container">
                                                <div data-switcher="8" class="switcher">
                                                    ���������
                                                    <div class="switcher__btn">
                                                        <svg width="10" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
                                                            <path fill="white"
                                                                  d="M207.6 256l107.72-107.72c6.23-6.23 6.23-16.34 0-22.58l-25.03-25.03c-6.23-6.23-16.34-6.23-22.58 0L160 208.4 52.28 100.68c-6.23-6.23-16.34-6.23-22.58 0L4.68 125.7c-6.23 6.23-6.23 16.34 0 22.58L112.4 256 4.68 363.72c-6.23 6.23-6.23 16.34 0 22.58l25.03 25.03c6.23 6.23 16.34 6.23 22.58 0L160 303.6l107.72 107.72c6.23 6.23 16.34 6.23 22.58 0l25.03-25.03c6.23-6.23 6.23-16.34 0-22.58L207.6 256z"
                                                                  class=""></path>
                                                        </svg>
                                                    </div>
                                                </div>





                                                <div data-switcher="8" class="title--1">
                                                    ��������� ����������� ����������
                                                </div>

                                                <div data-switcher="8" class="docs__wrap">
                                                    <?/*����� ���������*/
                                                        $form = array("�����", "�������");
                                                    ?>
                                                                <?
                                                                foreach ($form as $current_form )
                                                                {
                                                                    if ($MODULE_OUTPUT["new_docs"][$current_form][""][1] || $MODULE_OUTPUT["new_docs"][$current_form][""][2] || $MODULE_OUTPUT["new_docs"][$current_form][""][3])
                                                                    {
                                                                        ?>
                                                                        <div class="docs__block">
                                                                            <strong>
                                                                                ����� ���������.
                                                                            </strong>
                                                                            <span>
                                                                               ����� ��������: <?=$current_form?>
                                                                            </span>
                                                                            <div class="docs__list">
                                                                                <?
                                                                                $fgos=0;
                                                                                $plan=0;
                                                                                $graph=0;
                                                                                ?>

                                                                                <?
                                                                                for ($i=1; $i <= 3 ; $i++)
                                                                                {
                                                                                    $svejest = max(array_keys($MODULE_OUTPUT["new_docs"][$current_form][""][$i]));
                                                                                    foreach ($MODULE_OUTPUT["new_docs"][$current_form][""][$i][$svejest] as $key => $value)
                                                                                    {
                                                                                        switch ($value["doc_type_id"])
                                                                                        {
                                                                                            case 2:
                                                                                            {
                                                                                                $plan++;
                                                                                                ?>
                                                                                                <div class="docs__item">
                                                                                                    <img src="/images/11_nsau.png" alt=""/>
                                                                                                    <a href="/file/<? echo $value["file_id"]; ?>">
                                                                                                        ������� ���� <?=$value["year"] ?  $value["year"].'�.' : "--"?>
                                                                                                    </a>
                                                                                                </div>
                                                                                                <?
                                                                                            }
                                                                                            break;
                                                                                            case 3:
                                                                                            {
                                                                                                $graph++;
                                                                                                ?>
                                                                                                <div class="docs__item">
                                                                                                    <img src="/images/11_nsau.png" alt=""/>
                                                                                                    <a href="/file/<? echo $value["file_id"]; ?>">
                                                                                                        ������ �������� �������� <?=$value["year"] ?  $value["year"].'�.' : "--"?>
                                                                                                    </a>
                                                                                                </div>
                                                                                                <?
                                                                                            }
                                                                                            break;
                                                                                            case 1:
                                                                                            {
                                                                                                $fgos++;
                                                                                                ?>
                                                                                                <div class="docs__item">
                                                                                                    <img src="/images/11_nsau.png" alt=""/>
                                                                                                    <a href="/file/<? echo $value["file_id"]; ?>">
                                                                                                        ���� <?=$value["year"] ?  $value["year"].'�.' : "--"?>
                                                                                                    </a>
                                                                                                </div>
                                                                                                <?
                                                                                            }
                                                                                            break;
                                                                                        }
                                                                                    }
                                                                                } ?>
                                                                            </div>
                                                                        </div>
                                                                    <? }
                                                                } ?>

                                                    <?/*��������� �� ��������*/
                                                    if ($MODULE_OUTPUT["profiles"])
                                                    {
                                                        foreach ($MODULE_OUTPUT["profiles"] as $tmp => $profile)
                                                        {
                                                            ?>

                                                            <?
                                                            foreach ($form as $current_form )
                                                            {
                                                                if ($MODULE_OUTPUT["new_docs"][$current_form][$profile['id']][1] || $MODULE_OUTPUT["new_docs"][$current_form][$profile['id']][2] || $MODULE_OUTPUT["new_docs"][$current_form][$profile['id']][3])
                                                                {
                                                                    ?>
                                                                    <div class="docs__block">
                                                                        <strong>
                                                                            ������� <?=$profile['name']?>
                                                                        </strong>
                                                                        <span>
                                                                               ����� ��������: <?=$current_form?>
                                                                            </span>
                                                                        <div class="docs__list">
                                                                            <?
                                                                            for ($i=1; $i <= 3 ; $i++)
                                                                            {
                                                                                //������� ����� ������ �������� � ������� :)
                                                                                $svejest = max(array_keys($MODULE_OUTPUT["new_docs"][$current_form][$profile['id']][$i]));
                                                                                foreach ($MODULE_OUTPUT["new_docs"][$current_form][$profile['id']][$i][$svejest] as $key => $value)
                                                                                {
                                                                                    switch ($value["doc_type_id"])
                                                                                    {
                                                                                        case 2:
                                                                                            ?>
                                                                                            <div class="docs__item">
                                                                                                <img src="/images/11_nsau.png" alt=""/>
                                                                                                <a class="sig_files" href="/file/<? echo $value["file_id"]; ?>">
                                                                                                    ������� ���� <?=$value["year"] ?  $value["year"].'�.' : "--"?>
                                                                                                </a>
                                                                                            </div>
                                                                                            <?
                                                                                        break;

                                                                                        case 3:
                                                                                            ?>
                                                                                            <div class="docs__item">
                                                                                                <img src="/images/11_nsau.png" alt=""/>
                                                                                                <a class="sig_files" href="/file/<? echo $value["file_id"]; ?>">
                                                                                                    ������ �������� �������� <?=$value["year"] ?  $value["year"].'�.' : "--"?>
                                                                                                </a>
                                                                                            </div>
                                                                                            <?
                                                                                        break;

                                                                                        case 1:
                                                                                            ?>
                                                                                            <div class="docs__item">
                                                                                                <img src="/images/11_nsau.png" alt=""/>
                                                                                                <a class="sig_files" href="/file/<? echo $value["file_id"]; ?>">
                                                                                                    ���� <?=$value["year"] ?  $value["year"].'�.' : "--"?>
                                                                                                </a>
                                                                                            </div>
                                                                                            <?
                                                                                        break;
                                                                                    }
                                                                                }
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                <?
                                                                }
                                                            }

                                                        }

                                                    }
                                                    ?>
                                                </div>

                                                <div class="mobile-line"></div>

                                            </div>
                                        </div>


                                        <div class="progress">
                                            <div class="progress__bar">

                                            </div>
                                            <div class="progress__bar-done">

                                            </div>
                                            <div class="progress__ancors">

                                                <span class="progress__ancors-wrap">
                                                    <a href="#special">�������</a>
                                                </span>
                                                <span class="progress__ancors-wrap">
                                                    <a href="#education">��������</a>
                                                </span>
                                                <span class="progress__ancors-wrap">
                                                    <a href="#know">��� � �����?</a>
                                                </span>
                                                <span class="progress__ancors-wrap">
                                                    <a href="#preparation">��� � ���� ��������</a>
                                                </span>
                                                <span class="progress__ancors-wrap">
                                                    <a href="#practicle">��������</a>
                                                </span>
                                                <span class="progress__ancors-wrap">
                                                    <a href="#who_will_teach">��� ��� ����� �����?</a>
                                                </span>

                                                <span class="progress__ancors-wrap">
                                                    <a href="#graduate">����������</a>
                                                </span>
                                                <span class="progress__ancors-wrap">
                                                    <a href="#docs">���������</a>
                                                </span>

                                            </div>
                                        </div>

							        <? }
						        }
					        }
				        }
			        }
		        }
	        }
        } else {

        }

    }
    break;



    case 'exam_ege':
    {
        ?>
        <p class="text-center">
          <center>  <h1>����������� ���������� ������, ����������������� �� �������� ����������� ������������� ���������</h1>
        ����������� ������������� ����������� ���������� ������, ����������������� �� �������� ����� ������������� ��������� �� ���������, � ������������ � ����������� ����������� ������ ����������� ���, ������������� ��������� �� ������������ �������������. </center>
        </p>
        <?
        if ($Engine->OperationAllowed(5, "exam_ege.edit", -1, $Auth->usergroup_id))
        {
            ?>
            <div class="row"> 
                 <div class="col-sm-2"> <a class="btn btn-nsau btn-sm add_exam"><span class="glyphicon glyphicon-plus"></span> �������� �������</a></div>
                 <div class="col-sm-10 status" style="margin-top: -10px;"></div>
            </div>
            <?
        }
        ?>



        <div id="modal_summary_info" class="modal fade">
            <div class="modal-dialog" style="width: 850px; top: 100px;">
                <div class="modal-content">
                    <div class="modal-header">
                       ���������� ��������
                    </div>
                    <div class="modal-body" style="text-align: left; font-size: 12px;">
                        <div class="row"> 
                            <form method="POST">
                                <div class="col-sm-12">
                                    <?
                                    if(!empty($MODULE_OUTPUT['exam_error']))
                                    {
                                        echo "<div class='alert alert-danger'> ��� ���� ������ ���� ��������� </div>";
                                    };
                                    ?>
                                    <div class="input-group"> 
                                        <span class="input-group-addon" id="basic-addon1" style="border-bottom: 0px!important;">� �/�</span>
                                        <input type="text" class="form-control" placeholder=" " name="nomer" style="border-bottom: 0px!important;" id="modal_add_nomer">
                                    </div>

                                    <div class="input-group"> 
                                        <span class="input-group-addon" id="basic-addon1" style="border-bottom: 0px!important;">�������</span>
                                        <input type="text" class="form-control" placeholder="" name="predmet" style="border-bottom: 0px!important;" id="modal_add_predmet">
                                    </div>

                                    <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon1" style="border-bottom: 0px!important;">������� �����������</span >
                                        <select class="form-control" name="eduLevel" style="border-bottom: 0px!important;" id="modal_add_eduLevel">
                                            <?php foreach($MODULE_OUTPUT['abitEduLevel'] as $key => $value) { ?>
                                                <option value="<?=$value['id']?>" <?=($value['id'] == 2 ? " selected='selected'" : "")?>><?=$value['name']?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="input-group"> 
                                        <span class="input-group-addon" id="basic-addon1" style="border-bottom: 0px!important;">����������� ���������� ��� �������������</span >
                                        <select class="form-control" name="spec" style="border-bottom: 0px!important;" id="modal_add_spec">
                                             <option value="0">��� ����������� ���������� � �������������</option>
                                            <?
                                            foreach ($MODULE_OUTPUT['spec'] as $key => $value) 
                                            {

                                                ?>
                                                <option value="<?=$key?>"><?=$value?></option>
                                                <?
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="input-group"> 
                                        <span class="input-group-addon" id="basic-addon1" style="border-bottom: 0px!important;">����������� ����, ������������� �����</span>
                                        <input type="text" class="form-control" placeholder="" name="ball_vyz" style="border-bottom: 0px!important;" id="modal_add_ball_vyz">
                                    </div>
                                    <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon1" style="border-bottom: 0px!important;">������������ ����, ������������� �����</span>
                                        <input type="text" class="form-control" placeholder="" name="ball_max_vyz" style="border-bottom: 0px!important;" id="modal_add_ball_max_vyz">
                                    </div>
                                    <div class="input-group"> 
                                        <span class="input-group-addon" id="basic-addon1" style="border-bottom: 0px!important;">����������� ����, ������������� ����������� ������</span>
                                        <input type="text" class="form-control" placeholder="" name="ball_mins" style="border-bottom: 0px!important;" id="modal_add_ball_mins">
                                    </div>
                                    <div class="input-group"> 
                                        <span class="input-group-addon" id="basic-addon1">������</span>
                                        <input type="text" class="form-control" placeholder="" name="prikaz" id="modal_add_prikaz">
                                    </div>
                                </div>
                        </div> 

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-nsau-grey close_add">������</button>
                        <button type="button" name="add_exam" class="btn btn-nsau add_exam_modal">��������</button>
                        </form>
                    </div>
                    </form>
                </div>
            </div>
        </div>


        <div id="modal_exam_edit" class="modal fade">
            <div class="modal-dialog" style="width: 850px; top: 100px;">
                <div class="modal-content">
                    <div class="modal-header">
                       �������������� ��������
                    </div>
                    <div class="modal-body" style="text-align: left; font-size: 12px;">
                        <div class="row"> 
                            <form method="POST">
                                <div class="col-sm-12">
                                    <?
                                    if(!empty($MODULE_OUTPUT['exam_error']))
                                    {
                                        echo "<div class='alert alert-danger'> ��� ���� ������ ���� ��������� </div>";
                                    };
                                    ?>

                                    <div class="input-group"> 
                                        <span class="input-group-addon" id="basic-addon1" style="border-bottom: 0px!important;">� �/�</span>
                                        <input type="text" class="form-control" placeholder=" " name="nomer" style="border-bottom: 0px!important;" id="modal_edit_nomer">
                                    </div>

                                    <div class="input-group"> 
                                        <span class="input-group-addon" id="basic-addon1" style="border-bottom: 0px!important;">�������</span>
                                        <input type="text" class="form-control" placeholder="" name="predmet" data-id="" style="border-bottom: 0px!important;" id="modal_edit_predmet">
                                    </div>

                                    <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon1" style="border-bottom: 0px!important;">������� �����������</span >
                                        <select class="form-control" name="eduLevel" style="border-bottom: 0px!important;" id="modal_edit_eduLevel">
                                            <?php foreach($MODULE_OUTPUT['abitEduLevel'] as $key => $value) { ?>
                                                <option value="<?=$value['id']?>"><?=$value['name']?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="input-group"> 
                                        <span class="input-group-addon" id="basic-addon1" style="border-bottom: 0px!important;">����������� ���������� ��� �������������</span >
                                        <select class="form-control" name="spec" style="border-bottom: 0px!important;" id="modal_edit_spec">
                                             <option value="0">��� ����������� ���������� � �������������</option>
                                            <?
                                            foreach ($MODULE_OUTPUT['spec'] as $key => $value) 
                                            {

                                                ?>
                                                <option value="<?=$key?>"><?=$value?></option>
                                                <?
                                            }
                                            ?>
                                        </select>
                                    </div>

                                    <div class="input-group"> 
                                        <span class="input-group-addon" id="basic-addon1" style="border-bottom: 0px!important;">����������� ����, ������������� �����</span>
                                        <input type="text" class="form-control" placeholder="" name="ball_vyz" style="border-bottom: 0px!important;" id="modal_edit_ball_vyz">
                                    </div>
                                    <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon1" style="border-bottom: 0px!important;">������������ ����, ������������� �����</span>
                                        <input type="text" class="form-control" placeholder="" name="ball_max_vyz" style="border-bottom: 0px!important;" id="modal_edit_ball_max_vyz">
                                    </div>
                                    <div class="input-group"> 
                                        <span class="input-group-addon" id="basic-addon1" style="border-bottom: 0px!important;">����������� ����, ������������� ����������� ������</span>
                                        <input type="text" class="form-control" placeholder="" name="ball_mins" style="border-bottom: 0px!important;" id="modal_edit_ball_mins">
                                    </div>

                                    <div class="input-group"> 
                                        <span class="input-group-addon" id="basic-addon1">������</span>
                                        <input type="text" class="form-control" placeholder="" name="prikaz" id="modal_edit_prikaz">
                                    </div>
                                </div>
                        </div> 

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-nsau-grey close_edit">������</button>
                        <button type="button" name="add_exam" class="btn btn-nsau edit_exam_save">���������</button>
                        </form>
                    </div>
                    </form>
                </div>
            </div>
        </div>


    <style type="text/css">
        #exam_table td, th
        {
            text-align: center!important;
            vertical-align: middle!important;
        }

        .alert {
            position: absolute;
        }
    </style>

        <table class="table table-striped" id="exam_table">
            <thead>
                <tr>
                    <th>� �/�</th>
                    <th>�������</th>
                    <th>������� �����������</th>
                    <th>����������� ���������� ��� �������������</th>
                    <th>����������� ����, ������������� �����</th>
                    <th>������������ ����, ������������� �����</th>
                    <th>����������� ���� �������� ���, ������������� ����������� ������</th>
                    <th>� �������</th>
                    <?
                    if ($Engine->OperationAllowed(5, "exam_ege.edit", -1, $Auth->usergroup_id))
                    {
                        ?><th> </th><?
                    }
                    ?>
                </tr>
            </thead>
            <tbody>
                <?
                foreach ($MODULE_OUTPUT['exams'] as $key => $value) 
                {
                    ?>
                    <tr id="<?='exam_'.$key?>">
                        <td><?=$value['n_pp']?></td>
                        <td><?=$value['name']?></td>
                        <td><?=$MODULE_OUTPUT['abitEduLevel'][$value['edu_level_id']]['name']?></td>
                        <td>
                            <?
                            if ($value['spec']==0)
                            {
                                echo "��� ����������� ����������";
                            }
                            else
                            {
                                echo $MODULE_OUTPUT['spec'][$value['spec']];
                            }
                            ?>
                                
                        </td>
                        <td class="align-text-top"><?=$value['min_vyz']?></td>
                        <td><?=$value['max_vyz']?></td>
                        <td><?=$value['min_mins']?></td>
                        <td><?=$value['prikaz']?></td>
                        <?
                        if ($Engine->OperationAllowed(5, "exam_ege.edit", -1, $Auth->usergroup_id))
                        {
                            ?><td>
                            <button class="btn btn-nsau btn-xs delete_exam" type="button" id="<?=$value['id']?>"><span class="glyphicon glyphicon-trash"></span></button>
                            <button class="btn btn-nsau btn-xs edit_exam" id="<?=$value['id']?>" type="button" style="margin-top: 2px;"><span class="glyphicon glyphicon-pencil"></span></button>
                            </td><?
                        }
                        ?>
                    </tr>
                    <?
                }
                ?>
            </tbody>
        </table>

        <?
        if (!empty($MODULE_OUTPUT['exam_error']))
        {
            ?>
            <script src="/themes/styles/bootstrap/js/jquery.min.js"></script> 
            <script src="/themes/styles/bootstrap/js/bootstrap.min.js"></script>
            <script type="text/javascript">
                $("#modal_summary_info").modal('show');
            </script>
            <?
        }
        
    }
    break;


    case "library_standard": {
        /*echo "<pre>";
        print_r($MODULE_OUTPUT["specialities"]);
        echo "</pre>";*/
        /*if($MODULE_OUTPUT["type"]=="higher") { ?>
        <h2 style="text-align: center;">�������� �������������� ������� ����������������� �����������</h2>
<?php   } elseif($MODULE_OUTPUT["type"]=="secondary") { ?>
        <h2 style="text-align: center;">�������� �������������� �������� ����������������� �����������</h2>
<?php   } elseif($MODULE_OUTPUT["type"]=="bachelor") { ?>
        <h2 style="text-align: center;">����������� ���������� ����������</h2>
<?php   } elseif($MODULE_OUTPUT["type"]=="magistracy") { ?>
        <h2 style="text-align: center;">����������� ���������� ���������</h2>
<?php   }*/
        if (isset($MODULE_OUTPUT["type_tuition"]) && $MODULE_OUTPUT["type_tuition"]=="correspondence_true") { ?>
        <p align="center">�������� ������ ������������� ������� ����� ��������. <a href="?sort=all" >�������� ���</a>.</p>
<?php   } ?>
        <table class="nsau_table" align="center">
            <thead>
                <tr>
                    <td><a href="?sort=code" title="���������� �� ���� �������������"><strong>���</strong></a></td>
                    <th><a href="?sort=name" title="���������� �� ������������ �������������"><strong>������������ �������������</strong></a></th>
                    <?php /* ?><th><a href="?sort=year" title="���������� �� ���� �������� �������������">��� ��������</a></th>
          <th>������������</th>
                    <th>�����</th>
                    <th><a href="?sort=correspondence_tuition" title="������� ������������� ������� ����� ��������">�������</a></th><?php */ ?>
                </tr>
            </thead>
            <tbody>
<?php   /*$j=0;
        $dir=$MODULE_OUTPUT["specialities"];
        if ($MODULE_OUTPUT["specialities"][0]["has_vacancies"]) { ?>
            <tr style="text-align: center; font-weight:bolder; background-color: #f09695;">
                <td style="font-size:12pt; padding:5px;" colspan="6"><?=$dir[$j]["direction"]?>&nbsp;-&nbsp;<?=$dir[$j]["name_dir"]?> </td>
            </tr>
<?php   }*/
        foreach($MODULE_OUTPUT["specialities"]  as $direction_id => $direction) {
            if($direction["has_vacancies"] == 1 && count($direction['spec'])) { ?>
                <tr class="tr_highlight">
                    <th colspan="6"><?=$direction_id?>&nbsp;-&nbsp;<?=$direction["direction"]?></th>
                </tr>
<?php           foreach($direction['spec'] as $special) {
                    if($special["has_vacancies"] == 1) { ?>
                <tr>
                    <td><a href="/file/<?=$special['file_id'];?>"><strong><?=$special["code"]?></strong></a><?php /* ?>
<?php                   if($MODULE_OUTPUT["type"]=="higher") { ?>
                        <a href="/speciality/higher/<?=$special["code"]?>/<?=$special["id"]?>/"><?=$special["code"]?></a>
<?php                   } elseif($MODULE_OUTPUT["type"]=="secondary") { ?>
                        <a href="/speciality/secondary/<?=$special["code"]?>/<?=$special["id"]?>/"><?=$special["code"]?></a>
<?php                   } elseif($MODULE_OUTPUT["type"]=="bachelor") { ?>
                        <a href="/speciality/bachelor/<?=$special["code"]?>/<?=$special["id"]?>/"><?=$special["code"]?></a>
<?php                   } elseif($MODULE_OUTPUT["type"]=="magistracy") { ?>
                        <a href="/speciality/magistracy/<?=$special["code"]?>/<?=$special["id"]?>/"><?=$special["code"]?></a>
<?php                   } ?><?php */ ?>
                    </td>
                    <td><a href="/file/<?=$special['file_id'];?>"><strong><?=$special["name"]?></strong></a><?php /* ?>
<?php                   if($MODULE_OUTPUT["type"]=="higher") { ?>
                        <a href="/speciality/higher/<?=$special["code"]?>/<?=$special["id"]?>/"><?=$special["name"]?></a>
<?php                   } elseif($MODULE_OUTPUT["type"]=="secondary") { ?>
                        <a href="/speciality/secondary/<?=$special["code"]?>/<?=$special["id"]?>/"><?=$special["name"]?></a>
<?php                   } elseif($MODULE_OUTPUT["type"]=="bachelor") { ?>
                        <a href="/speciality/bachelor/<?=$special["code"]?>/<?=$special["id"]?>/"><?=$special["name"]?></a>
<?php                   } elseif($MODULE_OUTPUT["type"]=="magistracy") { ?>
                        <a href="/speciality/magistracy/<?=$special["code"]?>/<?=$special["id"]?>/"><?=$special["name"]?></a>
<?php                   }?><?php */ ?>
                        <br/>
<?php                   if ($MODULE_OUTPUT["privileges"]["specialities.handle"]==true /*&& 0*/) { ?>
                        <a href="/office/speciality_directory/edit_speciality/<?=$special["id"]?>/" target="_blank"><font size="1pt">�������������</font></a>
<?php                   } ?>
                    </td>
                    <?php /* ?><td ><?=$special["year_open"]?></td>
            <td><?php if($special["old"]) {
            switch ($special["type"]) {
                            case "secondary":
                                $type_sp="������� ������c��������� �����������";
                            break;
                            case "higher":
                                $type_sp="������ ������c��������� �����������";
                            break;
                            case "magistracy":
                                $type_sp="������������";
                            break;
                            case "bachelor":
                                $type_sp="�����������";
                            break;
                        }
            }
            else $type_sp = $special["qualification"];?>
              <?=$type_sp;?>
              </td>
<?php                   if($special["internal_tuition"]==1) { ?>
                    <td class="td_highlight">+</td>
<?php                   } else{ ?>
                    <td class="td_highlight">-</td>
<?php                   }
                        if($special["correspondence_tuition"]==1) { ?>
                    <td>+</td>
<?php                   } else{ ?>
                    <td>-</td>
<?php                   } ?> <?php */ ?>
                </tr>
<?php               }
                }
            }
        }
       ?>
            </tbody>
        </table>
<?php
    }
    break;

    case "specialities": {
        /*echo "<pre>";
        print_r($MODULE_OUTPUT["specialities"]);
        echo "</pre>";*/
        if($MODULE_OUTPUT["type"]=="higher") { ?>
        <h2 style="text-align: center;">�������� �������������� ������� ����������������� �����������</h2>
<?php   } elseif($MODULE_OUTPUT["type"]=="secondary") { ?>
        <h2 style="text-align: center;">�������� �������������� �������� ����������������� �����������</h2>
<?php   } elseif($MODULE_OUTPUT["type"]=="bachelor") { ?>
        <h2 style="text-align: center;">����������� ���������� ����������</h2>
<?php   } elseif($MODULE_OUTPUT["type"]=="magistracy") { ?>
        <h2 style="text-align: center;">����������� ���������� ���������</h2>
<?php   } elseif($MODULE_OUTPUT["type"]=="graduate") { ?>
        <h2 style="text-align: center;">����������� ���������� ����������</h2>
<?php   }
        if (isset($MODULE_OUTPUT["type_tuition"]) && $MODULE_OUTPUT["type_tuition"]=="correspondence_true") { ?>
        <p align="center">�������� ������ ������������� ������� ����� ��������. <a href="?sort=all" >�������� ���</a>.</p>
<?php   } ?>
        <table class="nsau_table" align="center" width="75%">
            <thead>
                <tr>
                    <th rowspan="2" width="10%"><a href="?sort=code" title="���������� �� ���� �������������">���</a></th>
                    <th rowspan="2" width="25%"><a href="?sort=name" title="���������� �� ������������ �������������">������������ �������������</a></th>
                    <th rowspan="2" width="10%"><a href="?sort=year" title="���������� �� ���� �������� �������������">��� ��������</a></th>

          <th rowspan="2" width="15%">������������</th>
          <th colspan="3" width="20%">����������� ���� ��������</th>
          <th rowspan="2" width="20%">���� �������� ��������������� ������������ ��������������� ���������</th>
                </tr>
                <tr>
                    <th>�����</th>
                    <th><a href="?sort=correspondence_tuition" title="������� ������������� ������� ����� ��������">�������</a></th>
                    <th>����-�������</th>
                </tr>
            </thead>
            <tbody>
<?php   /*$j=0;
        $dir=$MODULE_OUTPUT["specialities"];
        if ($MODULE_OUTPUT["specialities"][0]["has_vacancies"]) { ?>
            <tr style="text-align: center; font-weight:bolder; background-color: #f09695;">
                <td style="font-size:12pt; padding:5px;" colspan="6"><?=$dir[$j]["direction"]?>&nbsp;-&nbsp;<?=$dir[$j]["name_dir"]?> </td>
            </tr>
<?php   }*/
        foreach($MODULE_OUTPUT["specialities"]  as $direction_id => $direction) {
            if($direction["has_vacancies"] == 1 && count($direction['spec'])) { ?>
                <tr class="tr_highlight">
                    <th colspan="8" itemprop="EduDirect"><?=$direction_id?>&nbsp;-&nbsp;<?=$direction["direction"]?></th>
                </tr>
<?php           foreach($direction['spec'] as $special) {
                    if($special["has_vacancies"] == 1) { ?>
                <tr>
                    <th  itemprop="EduCode">
<?php                  // if($MODULE_OUTPUT["type"]=="higher") { ?>
                        <!-- <a href="/speciality/higher/<?=$special["code"]?>/<?=$special["id"]?>/"><?=$special["code"]?></a> -->
<?php                 //  } elseif($MODULE_OUTPUT["type"]=="secondary") { ?>
                        <!-- <a href="/speciality/secondary/<?=$special["code"]?>/<?=$special["id"]?>/"><?=$special["code"]?></a> -->
<?php                 //  } elseif($MODULE_OUTPUT["type"]=="bachelor") { ?>
                        <!-- <a href="/speciality/bachelor/<?=$special["code"]?>/<?=$special["id"]?>/"><?=$special["code"]?></a> -->
<?php                 //  } elseif($MODULE_OUTPUT["type"]=="magistracy") { ?>
                        <!-- <a href="/speciality/magistracy/<?=$special["code"]?>/<?=$special["id"]?>/"><?=$special["code"]?></a> -->
<?php                //   } elseif($MODULE_OUTPUT["type"]=="graduate") { ?>
                        <!-- <a href="/speciality/graduate/<?=$special["code"]?>/<?=$special["id"]?>/"><?=$special["code"]?></a> -->
<?php                  // } 

                         if($MODULE_OUTPUT["type"]=="higher") { ?>
                        <a href="/speciality/<?=$special["code"]?>/<?=$special["id"]?>/"><?=$special["code"]?></a>
<?php                   } elseif($MODULE_OUTPUT["type"]=="secondary") { ?>
                        <a href="/speciality/<?=$special["code"]?>/<?=$special["id"]?>/"><?=$special["code"]?></a>
<?php                   } elseif($MODULE_OUTPUT["type"]=="bachelor") { ?>
                        <a href="/speciality/<?=$special["code"]?>/<?=$special["id"]?>/"><?=$special["code"]?></a>
<?php                   } elseif($MODULE_OUTPUT["type"]=="magistracy") { ?>
                        <a href="/speciality/<?=$special["code"]?>/<?=$special["id"]?>/"><?=$special["code"]?></a>
<?php                   } elseif($MODULE_OUTPUT["type"]=="graduate") { ?>
                        <a href="/speciality/<?=$special["code"]?>/<?=$special["id"]?>/"><?=$special["code"]?></a>
<?php                   } ?>

                    </th>
                    <td itemprop="OOP_main">
<!-- <?php                   if($MODULE_OUTPUT["type"]=="higher") { ?>
                        <a href="/speciality/higher/<?=$special["code"]?>/<?=$special["id"]?>/"><?=$special["name"]?></a>
<?php                   } elseif($MODULE_OUTPUT["type"]=="secondary") { ?>
                        <a href="/speciality/secondary/<?=$special["code"]?>/<?=$special["id"]?>/"><?=$special["name"]?></a>
<?php                   } elseif($MODULE_OUTPUT["type"]=="bachelor") { ?>
                        <a href="/speciality/bachelor/<?=$special["code"]?>/<?=$special["id"]?>/"><?=$special["name"]?></a>
<?php                   } elseif($MODULE_OUTPUT["type"]=="magistracy") { ?>
                        <a href="/speciality/magistracy/<?=$special["code"]?>/<?=$special["id"]?>/"><?=$special["name"]?></a>
<?php                   } elseif($MODULE_OUTPUT["type"]=="graduate") { ?>
                        <a href="/speciality/graduate/<?=$special["code"]?>/<?=$special["id"]?>/"><?=$special["name"]?></a>
<?php                   }?> -->

<?php                   if($MODULE_OUTPUT["type"]=="higher") { ?>
                        <a href="/speciality/<?=$special["code"]?>/<?=$special["id"]?>/"><?=$special["name"]?></a>
<?php                   } elseif($MODULE_OUTPUT["type"]=="secondary") { ?>
                        <a href="/speciality/<?=$special["code"]?>/<?=$special["id"]?>/"><?=$special["name"]?></a>
<?php                   } elseif($MODULE_OUTPUT["type"]=="bachelor") { ?>
                        <a href="/speciality/<?=$special["code"]?>/<?=$special["id"]?>/"><?=$special["name"]?></a>
<?php                   } elseif($MODULE_OUTPUT["type"]=="magistracy") { ?>
                        <a href="/speciality/<?=$special["code"]?>/<?=$special["id"]?>/"><?=$special["name"]?></a>
<?php                   } elseif($MODULE_OUTPUT["type"]=="graduate") { ?>
                        <a href="/speciality/<?=$special["code"]?>/<?=$special["id"]?>/"><?=$special["name"]?></a>
<?php                   }?>


                        <br/>
<?php                   if ($MODULE_OUTPUT["privileges"]["specialities.handle"]==true /*&& 0*/) { ?>
                        <a href="/office/speciality_directory/edit_speciality/<?=$special["id"]?>/" target="_blank"><font size="1pt">�������������</font></a>
<?php                   } ?>
                    </td>
                    <td ><?=$special["year_open"]?></td>
            <td itemprop="EduLavel"><?php if($special["old"]) {
            switch ($special["type"]) {
                            case "secondary":
                                $type_sp="������� ������c��������� �����������";
                            break;
                            case "higher":
                                $type_sp="������ ������c��������� �����������";
                            break;
                            case "magistracy":
                                $type_sp="������������";
                            break;
                            case "bachelor":
                                $type_sp="�����������";
                            break;
                        }
            }
            else $type_sp = $special["qualification"];?>
              <?=$type_sp;?>
              </td>
<?php                   if($special["internal_tuition"]==1) { ?>
                    <td itemprop="EduForm" class="td_highlight"><?=!empty($special["data_training"]) ? $special["data_training"] : "+"?></td>
<?php                   } else{ ?>
                    <td itemprop="EduForm" class="td_highlight">-</td>
<?php                   }
                        if($special["correspondence_tuition"]==1) { ?>
                    <td itemprop="EduForm"><?=!empty($special["data_training_ex"]) ? $special["data_training_ex"] : "+"?></td>
<?php                   } else{ ?>
                    <td itemprop="EduForm">-</td>

<?php                   } if($special["internal_correspondence_tuition"]==1) { ?>
                    <td itemprop="EduForm"><?=!empty($special["data_training_int_ex"]) ? $special["data_training_int_ex"] : "+"?></td>
<?php                   } else{ ?>
                    <td itemprop="EduForm">-</td>

<?php                   } ?>
                    <td><?=$special["accreditation"] == "00.00.0000" ? "" : $special["accreditation"]?></td>
                </tr>
<?php               }
                }
            }
        }
        /*foreach($MODULE_OUTPUT["specialities"] as $special) {
            if($special["has_vacancies"]==1) { ?>
            <tr>
                <td style="padding:4px;" bordercolor="#000000">
<?php           if($MODULE_OUTPUT["type"]=="higher") { ?>
                    <a href="/speciality/higher/<?=$special["code"]?>/"><?=$special["code"]?></a>
<?php           } elseif($MODULE_OUTPUT["type"]=="secondary") { ?>
                    <a href="/speciality/secondary/<?=$special["code"]?>/"><?=$special["code"]?></a>
<?php           } elseif($MODULE_OUTPUT["type"]=="bachelor") { ?>
                    <a href="/speciality/bachelor/<?=$special["code"]?>/"><?=$special["code"]?></a>
<?php           } elseif($MODULE_OUTPUT["type"]=="magistracy") { ?>
                    <a href="/speciality/magistracy/<?=$special["code"]?>/"><?=$special["code"]?></a>
<?php           } ?>
                </td>
                <td align="left">
<?php           if($MODULE_OUTPUT["type"]=="higher") { ?>
                    <a href="/speciality/higher/<?=$special["code"]?>/"><?=$special["name"]?></a>
<?php           } elseif($MODULE_OUTPUT["type"]=="secondary") { ?>
                    <a href="/speciality/secondary/<?=$special["code"]?>/"><?=$special["name"]?></a>
<?php           } elseif($MODULE_OUTPUT["type"]=="bachelor") { ?>
                    <a href="/speciality/bachelor/<?=$special["code"]?>/"><?=$special["name"]?></a>
<?php           } elseif($MODULE_OUTPUT["type"]=="magistracy") { ?>
                    <a href="/speciality/magistracy/<?=$special["code"]?>/"><?=$special["name"]?></a>
<?php           }?>
                    <br/>
<?php           if ($MODULE_OUTPUT["privileges"]["specialities.handle"]==true) { ?>
                    <a href="/speciality/<?=$special["code"]?>/?node=<?=$NODE_ID?>&amp;action=edit#form"><font size="1pt">�������������</font></a>
<?php           } ?>
                </td>
                <td ><?=$special["year_open"]?></td>
<?php           if($special["internal_tuition"]==1) { ?>
                <td bgcolor="#FF9F71">+</td>
<?php           } else{ ?>
                <td bgcolor="#FF9F71">-</td>
<?php           }
                if($special["correspondence_tuition"]==1) { ?>
                <td>+</td>
<?php           } else{ ?>
                <td>-</td>
<?php           } ?>
            </tr>
<?php       }
            $j+=1;
            if (!empty($dir[$j]["direction"]) && ($dir[$j]["direction"] != $dir[$j-1]["direction"])) { ?>
            <tr style="text-align: center; font-weight:bolder; color:#000000; background-color: #f09695">
                <td style="font-size:12pt; padding:5px;" colspan="6"><?=$dir[$j]["direction"]?>&nbsp;-&nbsp;<?=$dir[$j]["name_dir"]?> </td>
            </tr>
<?php       }
        }*/ ?>
            </tbody>
        </table>
<?php
    }
    break;

    /*����� ���� ��������������. ������� '�������� ��������������� ��������'*/
    case "specialities_all": 
    {
    ?>     
    <h2 style="text-align: center;" align="cenret">���������� � ����� �������� ��������������� ������������ ��������������� ���������, � ������, �� ������� �������������� ����������� (��������)</h2>  
        <table class="nsau_table" width="95%" itemprop="eduAccred">
            <thead>
                <tr>
                    <th rowspan="2" width="10%"><a href="?sort=code" title="���������� �� ���� �������������">���</a></th>
                    <th rowspan="2" width="25%"><a href="?sort=name" title="���������� �� ������������ �������������">������������ �������������</a></th>
                    <th rowspan="2" width="15%">������� �����������</th>
                    <th rowspan="2" width="20%">���� �������� ��������������� ������������ ��������������� ���������</th>
                    <th colspan="3" width="20%">����������� ���� ��������</th>
                    <th rowspan="2" width="20%">����� �� ������� ������������ ��������</th>
                </tr>
                <tr>
                    <th>�����</th>
                    <th><a href="?sort=correspondence_tuition" title="������� ������������� ������� ����� ��������">�������</a></th>
                    <th>����-�������</th>
                </tr>


                </tr>
            </thead>

            <tbody>
                <?
                foreach($MODULE_OUTPUT["specialities"]  as $direction_id => $direction) 
                {
                    if($direction["has_vacancies"] == 1 && count($direction['spec'])) 
                    { 
                        ?>
                        <tr class="tr_highlight">
                            <th colspan="8" itemprop="EduDirect"><?=$direction_id?>&nbsp;-&nbsp;<?=$direction["direction"]?></th>
                        </tr>
                        <?
                        foreach($direction['spec'] as $special) 
                        {
                            if($special["has_vacancies"] == 1 && $special["hide_accredit"] != 1) 
                            { 
                                ?>
                                <tr>
                                    <th itemprop="eduCode">
                                        <a href=<?="/speciality/".$special['code']."/".$special['id']?>><?=$special["code"]?></a>
                                    </th>

                                    <td align="center" itemprop="eduName">
                                        <?=$special["name"]?>
                                        <br/>
                                        <?php                   
                                        if ($MODULE_OUTPUT["privileges"]["specialities.handle"]==true /*&& 0*/) 
                                            { 
                                                ?>
                                                <a href="/office/speciality_directory/edit_speciality/<?=$special["id"]?>/" target="_blank">
                                                    <font size="1pt">�������������</font>
                                                </a>
                                                <?php                  
                                            } 
                                        ?>
                                     </td>
                                    
                                    <td align="center" itemprop="eduLevel">
                                        <?php 
                                        // if($special["old"]) 
                                        // {
                                            // switch ($special["type"]) 
                                            // {
                                            //     case "secondary":
                                            //         $type_sp="������� ������c��������� �����������";
                                            //     break;
                                            //     case "higher":
                                            //         $type_sp="������ ������c��������� �����������";
                                            //     break;
                                            //     case "magistracy":
                                            //         $type_sp="������������";
                                            //     break;
                                            //     case "bachelor":
                                            //         $type_sp="�����������";
                                            //     break;
                                            // }

                                    switch ($special["type"])
                                    {
                                        case 'secondary':
                                        {
                                            $type_sp="������� ���������������� ����������� - ���������� ������������ �������� �����";
                                        }
                                        break;
                                        case 'bachelor':
                                        {

                                           $type_sp="������ ����������� - �����������";
                                        }
                                        break;
                                        case 'magistracy':
                                        {
                                             $type_sp="������ ����������� - ������������";

                                        }
                                        break;
                                        case 'graduate':
                                        {
                                            $type_sp="������ ����������� - ���������� ������ ������ ������������";
                                        }
                                        break;

                                        case 'higher':
                                        {
                                             $type_sp="������ ����������� - �����������";
                                        }
                                        break;
                                    }
                                        // }
                                        // else $type_sp = $special["qualification"];
                                        ?>
                                        <?=$type_sp;?>
                                    </td>

                                    <td align="center" itemprop="dateEnd">
                                        <?=$special["accreditation"] == "00.00.0000" ? "" : $special["accreditation"]?>
                                    </td>


                                    <td align="center" itemprop="learningTerm">
                                        <?=$special["data_training"] ? $special["data_training"] : "-"?>
                                    </td>                             
                                    <td align="center" itemprop="learningTerm">
                                        <?=$special["data_training_ex"] ? $special["data_training_ex"] : "-"?>
                                    </td> 
                                    <td align="center" itemprop="learningTerm">
                                        <?=$special["data_training_int_ex"] ? $special["data_training_int_ex"] : "-"?>
                                    </td> 


                                    <td align="center" itemprop="language">
                                        �������
                                    </td>
                                </tr>
                                <?php                       
                            }
                        }
                    }

                }
                ?>
            </tbody>
        </table>
    <?php
    }
    break;

    case "speciality_directory": { ?>
		<div class="speciality_directory">
			<?php   if(isset($MODULE_OUTPUT['sub_mode']) && $MODULE_OUTPUT['sub_mode'] == 'add_speciality') {
				$form_back = $MODULE_OUTPUT['form_back'];
				if(isset($MODULE_OUTPUT["messages"]["bad"])) {
					foreach($MODULE_OUTPUT["messages"]["bad"] as $mess) { ?>
						<p class="message red"><?=$mess?></p>
					<?php           }
				} ?>
				<form method="post" action="<?=$EE["unqueried_uri"]?>">
					<fieldset>
						<legend>�������� �������������:</legend>
						<div class="wide">
							<input type="hidden" name="action_op" value="add" />
							<div class="form-notes">
								<dl>
									<dt><label>�������� �������������:</label></dt>
									<dd><input type="text" name="speciality_name" value="<?=$form_back['speciality_name']?>" /></dd>
								</dl>
							</div>
							<div class="form-notes">
								<dl>
									<dt><label>�������� ���������</label></dt>
									<dd>

										<select name="faculty" size="1">
											<option selected="<?=(in_array($special["id_faculty"], $faculty_ids) ? '':'selected')?>">--------------------------------------------</option>
											<?php
											$faculty_checked = false;
											foreach($MODULE_OUTPUT["faculty"] as $faculty)
											{
												if ($faculty["id"]==$special["id_faculty"])
												{
													?>
													<option value="<?=$faculty["id"]?>" selected="selected"><?=$faculty["name"]?></option>
													<?php
													$faculty_checked = true;
												}
												else
												{
													?>
													<option value="<?=$faculty["id"]?>"><?=$faculty["name"]?></option>
												<?php                       }
											} ?>
										</select><br />



									</dd>
								</dl>
							</div>
							<div class="form-notes">
								<dl>
									<dt><label>��� �������������:</label></dt>
									<dd><input type="text" name="code_add" value="<?=$form_back['code_add']?>" /></dd>
								</dl>
							</div>
							<div class="clear"></div>
						</div>
						<div class="wide">
							<div class="form-notes">
								<dl>
									<dt><label>�������� �����������:</label></dt>
									<dd>
										<select name="direction" size="1">
											<option value="0"<?=($form_back['direction'] == 0 ? " selected='selected'" : "")?>>����������� �������������</option>
											<?php       foreach($MODULE_OUTPUT["directory"] as $dir) { ?>
												<option value="<?=$dir["code"]?>"<?=($form_back['direction'] == $dir["code"] ? " selected='selected'" : "")?>><?=$dir["code"]?> - <?=$dir["name"]?></option>
											<?php       } ?>
										</select>
									</dd>
								</dl>
							</div>
							<div class="clear"></div>
						</div>
						<div class="wide">
							<div class="form-notes">
								<dl>
									<dt><label>��� ��������:</label></dt>
									<dd><input type="text" name="year_open" value="<?=$form_back['year_open']?>" /></dd>
								</dl>
							</div>
							<div class="form-notes">
								<dl>
									<dt><label>������� ���������� �������������:</label></dt>
									<dd>
										<?php /* ?><select name="type" size="1">
                                        <option value="0"<?=($form_back['type'] == 0 ? " selected='selected'" : "")?>>��� �������������</option>
                                        <option value="secondary"<?=($form_back['type'] == "secondary" ? " selected='selected'" : "")?>>51 - ������� ��������������� �����������</option>
                                        <option value="higher"<?=($form_back['type'] == "higher" ? " selected='selected'" : "")?>>65 - ������ ��������������� �����������</option>
                                        <option value="bachelor"<?=($form_back['type'] == "bachelor" ? " selected='selected'" : "")?>>62 - �����������</option>
                                        <option value="magistracy"<?=($form_back['type'] == "magistracy" ? " selected='selected'" : "")?>>68 - ������������</option>
                                    </select><?php */ ?><input type="hidden" name="type" value="" />
										<select name="qualification" size="1" onchange="console.log(this.options[this.selectedIndex].getAttribute('data-old_type'));this.form.elements['type'].value = this.options[this.selectedIndex].getAttribute('data-old_type');">
											<option value="0"<?=($form_back['qualification'] == 0 ? " selected='selected'" : "")?> data-old_type="">������������</option>
											<?php foreach($MODULE_OUTPUT["qualifications"] as $qual_id => $qualif) { ?>
												<option value="<?=$qual_id?>"<?=($form_back['qualification'] == $qual_id ? " selected='selected'" : "")?> data-old_type="<?=$qualif['old_type']?>" title="<?=$MODULE_OUTPUT["spec_type_name"][$qualif['old_type']]; ?>"><?=$qualif["name"]?></option>
											<?php } ?>
										</select>&nbsp;<a href="<?=$EE["engine_uri"]?>manage_qualifications/">���������� ��������������</a>
									</dd>
								</dl>
							</div>
							<div class="clear"></div>
						</div>
						<div class="wide">
							<div class="form-notes">
								<dl>
									<dt><label>����� ����� ��������: <input type="checkbox" value="1" name="internal_tuition"<?=($form_back['internal_tuition'] ? " checked='checked'" : "")?> /></label></dt>
									<dt><label>������� ����� ��������: <input type="checkbox" value="1" name="correspondence_tuition"<?=($form_back['correspondence_tuition'] ? " checked='checked'" : "")?> /></label></dt>
									<dt><label>������ �����: <input type="checkbox" value="1" name="has_vacancies"<?=($form_back['has_vacancies'] ? " checked='checked'" : "")?> /></label></dt>
								</dl>
							</div>
							<!--                         <div class="form-notes">
                            <dl>
                                <dd><label>������ �������������</label></dd>
                                <dt>
                                <select name="old_spec" size="1" class="editable-select">
                                    <option value="0">--------------</option>
                                    <?php foreach ($MODULE_OUTPUT['old_specs'] as $dir_code => $dir) { ?>
                                    <optgroup label="<?=$dir["code"];?> - <?=$dir["title"];?>">
                                    <?php foreach ($dir["specs"] as $old_id => $old_spec) { ?>
                                    <option value="<?=$old_id; ?>"<?=($form_back['old_spec'] == $old_id ? " selected='selected'" : "")?>><?=$old_spec['code']?> - <?=$old_spec['name']?></option>
                                    <?php } ?>
                                    </optgroup>
                                    <?php } ?>
                                </select>
                                </dt>
                            </dl>
                        </div> -->
							<div class="clear"></div>
						</div>
						<div class="wide">
							<dl>
								<dt><label>�������� �����������:</label></dt>
								<dd>
									<textarea name="description"  style="width=300px; height=100px" class="wysiwyg"><?=$form_back['description']?></textarea>
								</dd>
							</dl>
						</div>
						<div class="wide">
							<input type="submit" value="��������" class="button" />
						</div>
					</fieldset>
				</form>
			<?php   } elseif(isset($MODULE_OUTPUT['sub_mode']) && $MODULE_OUTPUT['sub_mode'] == 'add_spec_type') {
				$form_back = $MODULE_OUTPUT['form_back'];
				if(isset($MODULE_OUTPUT["messages"]["bad"])) {
					foreach($MODULE_OUTPUT["messages"]["bad"] as $mess) { ?>
						<p class="message red"><?=$mess?></p>
					<?php           }
				} ?>
				<form method="post" action="<?=$EE["unqueried_uri"]?>">
					<fieldset>
						<legend>�������� ������� �������� � ������������� <?=$MODULE_OUTPUT['spec_info']["code"]?> - <?=$MODULE_OUTPUT['spec_info']["name"]?> :</legend>
						<input type="hidden" name="action_sub" value="add_spec_type" />
						<input type="hidden" name="code" value="<?=$MODULE_OUTPUT['spec_info']["code"]?>" />
						<input type="hidden" name="spec_id" value="<?=$MODULE_OUTPUT['spec_info']["id"]?>" />
						<br/>
						<table border="1" width="80%">
							<tr>
								<th bgcolor="#BBBBBB" width="7%">��� ��������</th>
								<th bgcolor="#BBBBBB" width="18%">������� ��������</th>
								<th bgcolor="#BBBBBB" width="15%">���� �������� <br> ����</th>
								<th bgcolor="#BBBBBB" width="15%">���� �������� <br> ������</th>
								<th bgcolor="#BBBBBB" width="15%">���� �������� <br> ����-������</th>
								<th bgcolor="#BBBBBB" width="5%">����� ����� ��������</th>
								<th bgcolor="#BBBBBB" width="5%">������� ����� ��������</th>
								<th bgcolor="#BBBBBB" width="5%">����-������� ����� ��������</th>
								<th bgcolor="#BBBBBB" width="5%">������ �����</th>
							</tr>
							<tr>
								<td><input type="text" class="fix_input" name="year_open_sub" value="<?=$form_back['year_open_sub']?>" /></td>
								<td>

									<?php if($special['old'] == 1) { ?><select name="type_sub" size="1" id="spec_info_type">
										<option value="0"<?=($form_back['type_sub'] == "0" ? " selected='selected'" : "")?>>��� ������ ��������</option>
										<option value="secondary"<?=($form_back['type_sub'] == "secondary" ? " selected='selected'" : "")?>>51 - ������� ��������������� �����������</option>
										<option value="higher"<?=($form_back['type_sub'] == "higher" ? " selected='selected'" : "")?>>65 - ������ ��������������� �����������</option>
										<option value="bachelor"<?=($MODULE_OUTPUT['spec_type']['type'] == "bachelor" ? " selected='selected'" : "")?>>62 - �����������</option>
										<option value="magistracy"<?=($form_back['type_sub'] == "magistracy" ? " selected='selected'" : "")?>>68 - ������������</option>
										</select><?php } else { ?>
										<input type="hidden" name="type_sub" value="" />
										<select name="qualification" size="1" onchange="console.log(this.options[this.selectedIndex].getAttribute('data-old_type'));this.form.elements['type_sub'].value = this.options[this.selectedIndex].getAttribute('data-old_type');">
											<option value="0"<?=($form_back['qualification'] == 0 ? " selected='selected'" : "")?> data-old_type="">������������</option>
											<?php foreach($MODULE_OUTPUT["qualifications"] as $qual_id => $qualif) { ?>
												<option value="<?=$qual_id?>"<?=($MODULE_OUTPUT['spec_type']['type'] == $qualif['old_type'] ? " selected='selected'" : "")?> data-old_type="<?=$qualif['old_type']?>"><?=$qualif["name"]?></option>
											<?php } ?>
										</select>
									<?php } ?>
								</td>
								<td><input type="text" class="fix_input" name="data_training" value="<?=$form_back['data_training']?>" /></td>
								<td><input type="text" class="fix_input" name="data_training_ex" value="<?=$form_back['data_training_ex']?>" /></td>
								<td><input type="text" class="fix_input" name="data_training_int_ex" value="<?=$form_back['data_training_int_ex']?>" /></td>
								<td align="center"><input type="checkbox" name="internal_tuition_sub"<?=($form_back['internal_tuition_sub'] ? " checked='checked'" : "")?> /></td>
								<td align="center"><input type="checkbox" name="correspondence_tuition_sub"<?=($form_back['correspondence_tuition_sub'] ? " checked='checked'" : "")?> /></td>
								<td align="center"><input type="checkbox" name="internal_correspondence_tuition_sub"<?=($form_back['internal_correspondence_tuition_sub'] ? " checked='checked'" : "")?> /></td>
								<td align="center"><input type="checkbox" name="has_vacancies"<?=($form_back['has_vacancies'] ? " checked='checked'" : "")?> /></td>
							</tr>
						</table>
						<br/>
						<input type="submit" value="��������" class="button" />
					</fieldset>
				</form>
			<?php } elseif(isset($MODULE_OUTPUT['sub_mode']) && $MODULE_OUTPUT['sub_mode'] == 'add_profile') {
				$form_back = $MODULE_OUTPUT['form_back'];
				if(isset($MODULE_OUTPUT["messages"]["bad"])) {
					foreach($MODULE_OUTPUT["messages"]["bad"] as $mess) { ?>
						<p class="message red"><?=$mess?></p>
					<?php           }
				} ?>
				<form method="post" action="<?=$EE["unqueried_uri"]?>">
					<fieldset>
						<legend>�������� ������� � ������������� <?=$MODULE_OUTPUT['spec_info']["code"]?> - <?=$MODULE_OUTPUT['spec_info']["name"]?>:</legend>
						<input type="hidden" name="action_sub" value="add_profile" />
						<input type="hidden" name="code" value="<?=$MODULE_OUTPUT['spec_info']["code"]?>" />
						<input type="hidden" name="spec_id" value="<?=$MODULE_OUTPUT['spec_info']["id"]?>" />
						<div class="wide">
							<input type="hidden" name="action_op" value="add" />
							<div class="form-notes">
								<dl>
									<dt><label>�������� �������:</label></dt>
									<dd><input type="text" name="profile_name" style="width:484px" value="<?=$form_back['profile_name']?>" /></dd>
								</dl>
							</div>
							<div class="clear"></div>
						</div>
						<div class="wide">
							<input type="submit" value="��������" class="button" />
						</div>
					</fieldset>
				</form>
			<?php   }  elseif(isset($MODULE_OUTPUT['sub_mode']) && $MODULE_OUTPUT['sub_mode'] == 'add_price'){
					//				$form_back = $MODULE_OUTPUT['form_back'];
				$prices = (!empty($MODULE_OUTPUT['edu_price'])) ? $MODULE_OUTPUT['edu_price'] : $MODULE_OUTPUT['edu_form'];
				if(isset($MODULE_OUTPUT['messages']['bad'])){
					foreach($MODULE_OUTPUT['messages']['bad'] as $ms){ ?>
						<p class="message red" style="width: max-content;"><?=$ms?></p>
					<? }
				} if(isset($MODULE_OUTPUT['messages']['good'])){
					foreach($MODULE_OUTPUT['messages']['good'] as $ms){ ?>
						<p class="message green" style="width: max-content;"><?=$ms?></p>
					<? }
				} ?>
				<form method="post" action="<?=$EE["unqueried_uri"]?>">
					<fieldset>
						<legend>�������� ���� � ������������� <?=$MODULE_OUTPUT['spec_info']["code"]?> - <?=$MODULE_OUTPUT['spec_info']["name"]?>:</legend>
						<input type="hidden" name="action_sub" value="add_price" />
						<div class="wide">
							<div class="form-notes">
								<dl>
									<? foreach($prices as $price) { ?>
                                        <dt style="display: inline-block; margin: 0; width: 20%"><label><?=!empty($MODULE_OUTPUT['edu_price']) ? $MODULE_OUTPUT['edu_form'][$price['id_edu_form']]['name'] : $price['name']?></label></dt>
                                    <dd style="display: inline-block; margin: 0;"><input type="text" name="price_<?=!empty($MODULE_OUTPUT['edu_price']) ? $price['id_edu_form'] : $price['id']?>" style="width:230px" value="<?= isset($price['value']) ? $price['value']: ''?>" /></dd></br>
									<? } ?>
								</dl>
							</div>
							<div class="clear"></div>
						</div>
						<div class="wide">
							<input type="submit" value="��������" class="button" />
						</div>
					</fieldset>
				</form>

			<? } elseif(isset($MODULE_OUTPUT['sub_mode']) && $MODULE_OUTPUT['sub_mode'] == 'edit_speciality' && isset($MODULE_OUTPUT['speciality']) && count($MODULE_OUTPUT['speciality'])) {
				foreach($MODULE_OUTPUT["faculty"] as $faculty) {
					$faculty_ids[] = $faculty['id'];
				};
				$special = $MODULE_OUTPUT["speciality"]; ?>
				<form action="<?=$EE["unqueried_uri"]?>" method="post">

					<a href="/speciality/<?=$special["code"]?>/<?=$MODULE_OUTPUT['speciality']['id']?>/">[�������� ����������� ����������]</a>

					<fieldset>
						<legend><?=$special["code"]?>&nbsp;-&nbsp;<?=$special["name"]?></legend>
						<input type="hidden" name="spec_id" value="<?=$MODULE_OUTPUT['speciality']['id']?>" />
						��� �������������:<br />
						<input type="text" name="code" value="<?=$special["code"]?>" /><br />�������� �������������:<br />
						<input type="text" name="speciality_name" style="width:484px" value="<?=$special["name"]?>" /><br/>�������� ���������:<br />
						<select name="faculty" size="1">
							<option selected="<?=(in_array($special["id_faculty"], $faculty_ids) ? '':'selected')?>">--------------------------------------------</option>
							<?php                   $faculty_checked = false;
							foreach($MODULE_OUTPUT["faculty"] as $faculty) {
								if ($faculty["id"]==$special["id_faculty"]) { ?>
									<option value="<?=$faculty["id"]?>" selected="selected"><?=$faculty["name"]?></option>
									<?php                           $faculty_checked = true;
								} else { ?>
									<option value="<?=$faculty["id"]?>"><?=$faculty["name"]?></option>
								<?php                       }
							} ?>
						</select><br />
						�������� �����������:<br />
						<select name="direction" size="1">
							<!--<option value="0" selected="selected"><?=$special["direction"]?></option>-->
							<option disabled>--------------------------------------------</option>
							<?php                   foreach($MODULE_OUTPUT["direction"] as $dir) {
								if ($dir["code"]==$special["direction"]) { ?>
									<option value="<?=$dir["code"]?>" selected="selected"><?=$dir["code"]?> - <?=$dir["name"]?></option>
								<?php                       } else { ?>
									<option value="<?=$dir["code"]?>"><?=$dir["code"]?> - <?=$dir["name"]?></option>
								<?php                       }
							}
							$i=0; ?>
						</select><br />
						<?php if($special['old'] == 0) { ?>
							<!--           ������ �������������: <br />
          <select name="old_spec" size="1" class="editable-select">
                                    <option value="0">--------------</option>
                                    <?php foreach ($MODULE_OUTPUT['old_specs'] as $dir_code => $dir) { ?>
                                    <optgroup label="<?=$dir["code"];?> - <?=$dir["title"];?>">
                                    <?php foreach ($dir["specs"] as $old_id => $old_spec) { ?>
                                    <option value="<?=$old_id; ?>"<?=($special['old_id'] == $old_id ? " selected='selected'" : "")?>><?=$old_spec['code']?> - <?=$old_spec['name']?></option>
                                    <?php } ?>
                                    </optgroup>
                                    <?php } ?>
                                </select><br /> -->
						<?php } ?>
						<br />
						<style type="text/css">
							#qual{
								width:160px;
							}
						</style>
						<table class="nsau_table_speciality" width="100%" >
							<tr>
								<th bgcolor="#BBBBBB" rowspan="2" width="3%">� �.�</th>
								<th bgcolor="#BBBBBB" rowspan="2" width="7%">��� ��������</th>
								<th bgcolor="#BBBBBB" rowspan="2" width="12%"><?php if($special['old']) { ?>������� ����������<?php } else { ?>������������<?php } ?></th>
								<th bgcolor="#BBBBBB" rowspan="2" width="3%">�����</th>
								<th bgcolor="#BBBBBB" rowspan="2" width="3%">�������</th>
								<th bgcolor="#BBBBBB" rowspan="2" width="3%">����-�������</th>
								<th bgcolor="#BBBBBB" rowspan="2" width="4%">�������<br />�����</th>

								<th bgcolor="#BBBBBB" colspan="3" width="30%">����������� ���� ��������</th>
								<th bgcolor="#BBBBBB" rowspan="2" width="25%">������� ��������</th>
								<th bgcolor="#BBBBBB" rowspan="2" width="10%">���� �������� ��������������� ������������</th>
							</tr>
							<tr>
								<th bgcolor="#BBBBBB" width="10%">����</th>
								<th bgcolor="#BBBBBB" width="10%">������</th>
								<th bgcolor="#BBBBBB" width="10%">����-������</th>
							</tr>
							<?php                   foreach($MODULE_OUTPUT["speciality_edit"] as $edit) {
								$i++; ?>
								<tr>
									<td align="center"><?=$i?><?php if($special['old'] == 0) { ?><input type="hidden" name="qualification[<?=$edit["qual_id"]?>]" value="<?=$edit["qual_id"]?>" /><?php } ?></td>
									<td>
										<input type="hidden" name="spec_type[<?=$special['old'] ? $edit["type"]: $edit['id']; ?>][st_type_id]" value="<?=$edit["id"]?>" />
										<input type="text" class="fix_table" name="spec_type[<?=$special['old'] ? $edit["type"]: $edit['id']; ?>][year_open]" value="<?=$edit["year_open"]?>" />
									</td>
									<td>
										<?php if($special['old'] == 1) { ?><select name="spec_type[<?=$edit["type"]?>][type]" size="1">
											<?php                       switch ($edit["type"]) {
												case secondary: ?>
													<option value="secondary" selected="selected">������� ������c��������� �����������</option>
													<option disabled='disabled'>---------------------------------------</option>
													<?php                           break;
												case higher: ?>
													<option value="higher" selected="selected">������ ������c��������� �����������</option>
													<option disabled>---------------------------------------</option>
													<?php                           break;
												case bachelor: ?>
													<option value="bachelor" selected="selected">�����������</option>
													<option disabled>---------------------------------------</option>
													<?php                           break;
												case magistracy: ?>
													<option value="magistracy" selected="selected">������������</option>
													<option disabled>---------------------------------------</option>
													<?php                           break;
											} ?>
											<option value="secondary" disabled='disabled'>������� �����c���������� �����������</option>
											<option value="higher" disabled='disabled'>������ ������c��������� �����������</option>
											<option value="bachelor" disabled='disabled'>�����������</option>
											<option value="magistracy" disabled='disabled'>������������</option>
											</select><?php } else { ?>
											<select name="spec_type[<?=$edit['id']?>][qual_id]" size="1" id="qual" onchange="console.log(this.options[this.selectedIndex].getAttribute('data-old_type'));document.getElementById('spec_type_<?=$edit['id'];?>').value = this.options[this.selectedIndex].getAttribute('data-old_type');">
												<option value="0"<?=($edit['qual_id'] == 0 ? " selected='selected'" : "")?> data-old_type="">������������</option>
												<?php foreach($MODULE_OUTPUT["qualifications"] as $qual_id => $qualif) { ?>
													<option value="<?=$qual_id?>"<?=($edit['qual_id'] == $qual_id ? " selected='selected'" : "")?> data-old_type="<?=$qualif['old_type']?>"><?=$qualif["name"]?></option>
												<?php } ?>
											</select>
											<input type="hidden" name="spec_type[<?=$edit['id']?>][type]" value="<?=$MODULE_OUTPUT["qualifications"][$edit['qual_id']]['old_type'];?>" id="spec_type_<?=$edit['id'];?>" />
										<?php } ?>
									</td>

									<td align="center"><input type="checkbox" name="spec_type[<?=$special['old'] ? $edit["type"]: $edit['id']; ?>][internal_tuition]" <?php if ($edit["internal_tuition"]==1){?>checked <?php }?> /></td>

									<td align="center"><input type="checkbox" name="spec_type[<?=$special['old'] ? $edit["type"]: $edit['id']; ?>][correspondence_tuition]" <?php if ($edit["correspondence_tuition"]==1){?>checked <?php }?> /></td>

									<td align="center"><input type="checkbox" name="spec_type[<?=$special['old'] ? $edit["type"]: $edit['id']; ?>][internal_correspondence_tuition]" <?php if ($edit["internal_correspondence_tuition"]==1){?>checked <?php }?> /></td>

									<td align="center"><input type="checkbox" name="spec_type[<?=$special['old'] ? $edit["type"]: $edit['id']; ?>][has_vacancies]" <?php if ($edit["has_vacancies"]){?>checked <?php }?> /></td>
									<td align="center"><input class="fix_table" type="text"  name="spec_type[<?=$special['old'] ? $edit["type"]: $edit['id']; ?>][data_training]" value="<?=$edit["data_training"]?>" /></td>
									<td align="center"><input class="fix_table" type="text" name="spec_type[<?=$special['old'] ? $edit["type"]: $edit['id']; ?>][data_training_ex]" value="<?=$edit["data_training_ex"]?>" /></td>
									<td align="center"><input class="fix_table" type="text" name="spec_type[<?=$special['old'] ? $edit["type"]: $edit['id']; ?>][data_training_int_ex]" value="<?=$edit["data_training_int_ex"]?>" /></td>
									<td align="center"><input class="fix_table" type="text" name="spec_type[<?=$special['old'] ? $edit["type"]: $edit['id']; ?>][qualification]" value="<?=$edit["qualification"]?>" /></td>
									<td align="center"><input class="date fix_table" type="text" name="spec_type[<?=$special['old'] ? $edit["type"]: $edit['id']; ?>][accreditation]" value="<?=$edit["accreditation"]?>" /></td>
								</tr>
							<?php                   } ?>
						</table>
						<a href="<?=$EE["engine_uri"]?>add_spec_type/<?=$special['id'];?>/">�������� <?php if($special['old'] == 0) { ?>������������<?php } else { ?>������� ����������<?php } ?></a><br />
						<br />
						<!-- <a href="<?=$EE["engine_uri"]?>docs/<?=$special['id'];?>">�������� ���������</a><br /><br /> -->
						<?php if($special['old'] == 0 && count($MODULE_OUTPUT["speciality_edit"]) > 0) { ?>

						<?php } ?>
						<?php               if(count($MODULE_OUTPUT["profiles"])) {
							?>
							�������: <br />
							<?php
							foreach($MODULE_OUTPUT["profiles"] as $profile) {
								?>
								<input type="hidden" name="profile[<?=$profile["id"]?>][id]" value="<?=$profile["id"]?>" />
								<input type="text" name="profile[<?=$profile["id"]?>][name]" style="width:484px"  value="<?=$profile["name"]?>" />
								<a title="�������" href="<?=$EE["engine_uri"]?>del_profile/<?=$profile["id"]?>/"><img src="/themes/images/delete.png" alt="�������" onclick="if(confirm('�� ������������� ������� ������� �������?')){return true;}else{return false;}" /></a>
								<br />
								<?php
							}
							?>

							<?php
						}
						?><a href="<?=$EE["engine_uri"]?>add_profile/<?=$MODULE_OUTPUT['speciality']['id']?>">�������� �������</a><br /><br />
						<?php if(isset($MODULE_OUTPUT['edu_price']) && count($MODULE_OUTPUT["edu_price"])) {
							?>
							��������� �������� �� �������:</br>
                                <?php foreach($MODULE_OUTPUT["edu_price"] as $price) { ?>
                                    <label style="display: inline-block; margin: 0; width: 10%"><?=$price['name']?></label>
                                    <input style="display: inline-block; margin: 0; width:366px" type="text" name="price_<?=$price['id']?>" value="<?=$price['value']?>" /></br>
                                <?php
                                } ?>
							<?php
						}
						?>
						<a href="<?=$EE["engine_uri"]?>add_price/<?=$MODULE_OUTPUT['speciality']['id']?>"><?=(isset($MODULE_OUTPUT['edu_price']) && count($MODULE_OUTPUT["edu_price"])) ? '������������� ': '�������� '?>��������� ��������</a><br /><br />
                        <?php
                        $emptyBann = array(
                            array('id_ban' => 1, 'value' => ''),
                            array('id_ban' => 2, 'value' => ''),
                            array('id_ban' => 3, 'value' => '')
                        );
                        $MODULE_OUTPUT["edu_banner"] = (isset($MODULE_OUTPUT['edu_banner']) && count($MODULE_OUTPUT["edu_banner"])) ? $MODULE_OUTPUT["edu_banner"] : $emptyBann;  ?>
							�������:</br>
                                <?php foreach($MODULE_OUTPUT["edu_banner"] as $bann) { ?>
                                    <input style="display: inline-block; margin: 0; width:487px" type="text" name="banner[<?=$bann['id_ban']?>]" value="<?=$bann['value']?>" placeholder="/images/1.jpg"/></br>
                                <?php
                                } ?>
						</br>
						<b>����� �������� �������������:</b><br />
						<textarea name="description" cols="60" rows="15" class="wysiwyg"><?=$special["description"]?></textarea><br />
						<?php                   foreach($MODULE_OUTPUT["speciality_edit"] as $edit) {
							if ($edit["type"] == "bachelor")
								$dir = "�����������";
							elseif ($edit["type"] == "magistracy")
								$dir = "������������";
							elseif ($edit["type"] == "higher")
								$dir = "���";
							elseif ($edit["type"] == "secondary")
								$dir = "���";
							if($special['old'] == 0) $dir = $MODULE_OUTPUT['qualifications'][$edit['qual_id']]['name'];
							?>
							<br />
							<fieldset>
								<legend><b><?php if($special['old'] == 1) { ?>�����������<?php } ?> <?=$dir?></b></legend>
								������ ���������:<br /><br />
								<?php                       if ($edit["type"] == "magistracy") {
									echo "������������� �� ������� ���������� ����������� ����������";
								} else { ?>
									<span style="display: block; float: left; width: 10%; ">������������:</span>
                                    <select name="spec_type[<?=$special['old'] ? $edit["type"]: $edit['id']; ?>][exams][obligatory]">
										<option value="0">���</option>
										<?foreach ($MODULE_OUTPUT["exams"] as $exam) { ?>
											<option value="<?=$exam["id"]?>" <?=($exam["need"]==1) ? "selected" : ""?>><?=$exam["name"].' ('.$exam["short_name"].')'?></option>
										<?}?>
									</select><br />
									<span style="display: block; float: left; width: 10%; ">����������:</span>
                                    <select name="spec_type[<?=$special['old'] ? $edit["type"]: $edit['id']; ?>][exams][prof]">
										<option value="0">���</option>
										<?foreach ($MODULE_OUTPUT["exams"] as $exam) { ?>
											<option value="<?=$exam["id"]?>" <?=($exam["need"]==2) ? "selected" : ""?>><?=$exam["name"].' ('.$exam["short_name"].')'?></option>
										<?}?>
									</select><br />
									<span style="display: block; float: left; width: 10%; ">��������������:</span>
                                    <select size="10" name="spec_type[<?=$special['old'] ? $edit["type"]: $edit['id']; ?>][exams][addictional][]" multiple="multiple">
										<?foreach ($MODULE_OUTPUT["exams"] as $exam) { ?>
											<option value="<?=$exam["id"]?>" <?=($exam["additonal"]) ? "selected" : ""?>><?=$exam["name"].' ('.$exam["short_name"].')'?></option>
										<?}?>
									</select><br /><br />

								<?  } ?>
								<textarea name="spec_type[<?=$special['old'] ? $edit["type"]: $edit['id']; ?>][description]" cols="60" rows="15" class="wysiwyg"><?=$edit["description"]?></textarea>
							</fieldset><br />
						<?php                   } ?>


						<input type="checkbox" name="hide_accredit" <?=($MODULE_OUTPUT["speciality"]["hide_accredit"] ? " checked='checked'" : "")?>>�������� ����������� � ������� ������������


						<p>
							<input type="submit" value="��������� ���������" />
							<input type="submit" name="<?=$_GET["node"]?>[save][cancel]" value="������" />
						</p>
					</fieldset>
				</form>

			<?php   } elseif(!empty($MODULE_OUTPUT["speciality_directory"])) { ?>
				<div class="action_speciality"><a href="add_speciality/">[ �������� ����������� ]</a> <a class="show_old">[ �������� ������ ]</a><a class="hide_old" hidden>[ ������ ������ ]</a></div>
				<?//CF::Debug($MODULE_OUTPUT["speciality_directory"]);?>

				<?foreach($MODULE_OUTPUT["speciality_directory"] as $faculty) {
					?>
					<style type="text/css">
						.old {
							display: none;
						}
					</style>
					<strong><?=$faculty["name"]?></strong>
					<ul>
						<?foreach($faculty["spec"] as $special) {
							?>
							<li class="<?=($special["old"]==1 ? ' old' : '')?>">
								<?if($special["handle"]) {?>
									<div class="list_title inline-block<?=(!$MODULE_OUTPUT["privileges"]["specialities.handle"]==true && !$special["handle"] ? ' disabled' : '')?> ">
										<?=$special["code"]?>&nbsp;-&nbsp;<?=$special["name"]?>
										<?if ($MODULE_OUTPUT["privileges"]["faculty.specialities.handle"][$special["id_faculty"]]==true || $special["handle"]) { ?>
											<div>
												<a title="������� �������� � ��� ������ ��������" href="del_speciality/<?=$special["id"]?>/" onclick="if(confirm('�� ������������� ������� ������� �����������?')){return true;}else{return false;}" ><img src="/themes/images/delete.png" alt="������� �������� � ��� ������ ��������" /></a>
												<a title="�������������" href="edit_speciality/<?=$special["id"]?>/"><img src="/themes/images/edit.png" alt="�������������" /></a>
												<a href="add_spec_type/<?=$special["id"]?>/" title="�������� ������� ��������"><img src="/themes/images/edit-add.png" alt="�������� ������� ��������" /></a>
												<a href="docs/<?=$special["id"]?>/" title="��������� ���������"><img src="/themes/images/download_file.png" alt="��������� ���������" /></a>
											</div>
										<?}?>
									</div>
									<?if(isset($special["spec_type"])) { ?>
										<ul>
											<?foreach($special["spec_type"] as $speciality) {
												switch ($speciality["type"]) {
													case "secondary":
														$type_sp="������� ������c��������� �����������";
														break;
													case "higher":
														$type_sp="������ ������c��������� �����������";
														break;
													case "magistracy":
														$type_sp="������������";
														break;
													case "bachelor":
														$type_sp="�����������";
														break;
												}
												if($speciality["qualification"]) $type_sp = $speciality["qualification"];?>
												<li>
													<div class="list_title inline-block<?=(!$special["handle"] ? ' disabled' : '')?>">
														<?=$speciality["year_open"]?>&nbsp;-&nbsp;<?=$type_sp?>&nbsp;<?=((0 && $speciality['has_vacancies'])?'(������ �����)':'')?>
														<?if ($special["handle"]) { ?>
															<div>
																<?if($special["spec_type_count"] > 1) { ?>
																	<a title="�������" href="del_spec_type/<?=$speciality["id"]?>/"><img src="/themes/images/delete.png" alt="�������" onclick="if(confirm('�� ������������� ������� ������� ������� ��������?')){return true;}else{return false;}" /></a>
																<?}?>
																<a title="������� ����" href="curriculum/<?=$speciality["id"]?>/"><img src="/themes/images/clipboard.png" alt="������� ����" /></a>
															</div>
														<?}?>
													</div>
												</li>
											<?}?>
										</ul>
									<?}?>
								<?}?>
							</li>
						<?}?>
					</ul>
				<?}?>

			<?php   } elseif(isset($MODULE_OUTPUT['sub_mode']) && $MODULE_OUTPUT['sub_mode'] == 'manage_qualifications') { ?>
				<form method="post" action="<?=$EE["unqueried_uri"]?>">
					<table border="1">
						<?php foreach($MODULE_OUTPUT['qualifications'] as $qual_id => $qualif) { ?>
							<tr>
								<td><input type="text" name="name[<?=$qual_id;?>]" value="<?=$qualif['name']; ?>" size="40" /></td>
								<td><select name="type[<?=$qual_id; ?>]" size="1">
										<option value="0"<?=($qualif['old_type'] == "0" ? " selected='selected'" : "")?>>---------------</option>
										<option value="secondary"<?=($qualif['old_type'] == "secondary" ? " selected='selected'" : "")?>>51 - ������� ��������������� �����������</option>
										<option value="higher"<?=($qualif['old_type'] == "higher" ? " selected='selected'" : "")?>>65 - ������ ��������������� �����������</option>
										<option value="bachelor"<?=($qualif['old_type'] == "bachelor" ? " selected='selected'" : "")?>>62 - �����������</option>
										<option value="magistracy"<?=($qualif['old_type'] == "magistracy" ? " selected='selected'" : "")?>>68 - ������������</option>
									</select></td>
								<td>
									<input type="checkbox" name="del[<?=$qual_id;?>]" value="<?=$qual_id; ?>" />
									<input type="hidden" name="id[]" value="<?=$qual_id; ?>" />
								</td>
							</tr>
						<?php } ?>
					</table>
					<p><input type="submit" value="���������" />
						<input type="hidden" name="action" value="save_qualifications" /></p>
				</form><br />
				<form method="post" action="<?=$EE["unqueried_uri"]?>">
					<fieldset>
						<legend>���������� ������������</legend>
						��������: <br />
						<input type="text" name="name" size="40" /><br />
						������ ������� ��������: <br />
						<select name="type" size="1">
							<option value="0"<?=($form_back['type'] == "0" ? " selected='selected'" : "")?>>---------------</option>
							<option value="secondary"<?=($form_back['type'] == "secondary" ? " selected='selected'" : "")?>>51 - ������� ��������������� �����������</option>
							<option value="higher"<?=($form_back['type'] == "higher" ? " selected='selected'" : "")?>>65 - ������ ��������������� �����������</option>
							<option value="bachelor"<?=($form_back['type'] == "bachelor" ? " selected='selected'" : "")?>>62 - �����������</option>
							<option value="magistracy"<?=($form_back['type'] == "magistracy" ? " selected='selected'" : "")?>>68 - ������������</option>
						</select><br />
						<input type="submit" value="��������" />
						<input type="hidden" name="action" value="add_qualification" />
					</fieldset>
				</form>
			<?php   } else { ?>
				<p>�������������� �� ����������</p>
			<?php   } ?>
		</div>
		<?php
	}
		break;


    case "curriculum": {
        if(isset($MODULE_OUTPUT['curriculum'])) {
            $curriculum = $MODULE_OUTPUT['curriculum'];
            if(isset($MODULE_OUTPUT['form_back']))
                $form_back = $MODULE_OUTPUT['form_back'];
            /*echo "<pre>";
            print_r($curriculum);
            echo "</pre>";*/
            if(isset($MODULE_OUTPUT['curriculum_allowed']) && $MODULE_OUTPUT['curriculum_allowed']) { ?>
        <script>jQuery(document).ready(function($) {Curriculum(<?=$MODULE_OUTPUT['module_id']?>);});</script>
<?php       }?>
        <div id="curriculum">
<?php       if(isset($MODULE_OUTPUT['curriculum_allowed']) && $MODULE_OUTPUT['curriculum_allowed']) { ?>
<?php           if(isset($MODULE_OUTPUT['mess']['curriculum']) && count($MODULE_OUTPUT['mess']['curriculum'])) { ?>
            <div class="wide">
<?php               foreach($MODULE_OUTPUT['mess']['curriculum'] as $mess) { ?>
                <p class="message red"><?=$mess?></p>
<?php               } ?>
            </div>
<?php           } ?>
            <form action="<?=$EE["unqueried_uri"]?>" method="post">
                <fieldset>
                    <legend>�������� ���� ���������</legend>
                    <div class="wide">
                        <div class="form-notes">
                            <dl>
                                <dt><label>���� �����:</label></dt>
                                <dd><input type="text" maxlength="10" name="sgroup_code" value="<?=(isset($form_back) ? $form_back['add_sgroup']['sgroup_code'] : "")?>" /></dd>
                            </dl>
                            <div class="wide">
                                <input type="submit" value="��������" />
                            </div>
                        </div>
                        <div class="form-notes">
                            <dl>
                                <dt><label>�������� ����� ���������:</label></dt>
                                <dd><input class="input_long" type="text" name="sgroup_name" size="50" value="<?=(isset($form_back) ? $form_back['add_sgroup']['sgroup_name'] : "")?>" /></dd>
                            </dl>
                            <div class="wide">
                                <input type="hidden" name="add_sgroup" value="1" />
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </fieldset>
            </form>
            <form action="<?=$EE["unqueried_uri"]?>" method="post">
                <fieldset>
                    <legend>������� ���� ���������</legend>
                    <div class="wide">
                        <div class="form-notes">
                            <dl>
                                <dt><label>���� ���������:</label></dt>
                                <dd>
<?php           if($curriculum['sgroup_list']) { ?>
                                    <select size="1" name="edit_sgroup_id">
<?php               foreach($curriculum['sgroup_list'] as $subject_group) { ?>
                                        <option value="<?=$subject_group['id']?>" title="<?=$subject_group['code']?>"<?=(isset($form_back['add_subject']['sgroup_id']) && $form_back['add_subject']['sgroup_id'] == $subject_group['id'] ? " selected='selected'" : "")?>><?=$subject_group['name']?></option>
<?php               } ?>
                                    </select>
<?php           } else { ?>
                                    ���� ��������� �����������.
<?php           } ?>
                                </dd>
                            </dl>
                            <div class="wide">
                                <input type="hidden" name="action_del" value="1" />
                                <input type="submit" value="�������" />
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </fieldset>
            </form>
            <form action="<?=$EE["unqueried_uri"]?>" method="post">
                <fieldset>
                    <legend>�������� ���������� � ���� ���������</legend>
                    <div class="wide">
                        <div class="form-notes">
                            <dl>
                                <dt><label>�������� ���� ���������:</label></dt>
                                <dd>
<?php           if($curriculum['sgroup_list']) { ?>
                                    <select class="select_sgroup" size="1" name="sgroup_id">
<?php               foreach($curriculum['sgroup_list'] as $subject_group) { ?>
                                        <option value="<?=$subject_group['id']?>" title="<?=$subject_group['code']?>"<?=(isset($form_back['add_subject']['sgroup_id']) && $form_back['add_subject']['sgroup_id'] == $subject_group['id'] ? " selected='selected'" : "")?>><?=$subject_group['name']?></option>
<?php               } ?>
                                    </select>
<?php           } else { ?>
                                    ���� ��������� �����������.
<?php           } ?>
                                </dd>
                            </dl>
                            <div class="wide">
                                <input type="hidden" name="add_subject" value="1" />
                            </div>
                        </div>
                        <div class="form-notes">
                            <div class="select_subject">
                                <label>�������� � ������� ���� ����������</label>
<?php           if(isset($curriculum['departments'])) { ?>
                                <ul>
<?php               foreach($curriculum['departments'] as $dep_id => $dep) { ?>
                                    <li>
                                        <label><?=$dep['name']?></label>
                                        <dl>
<?php                   foreach($dep['subjects'] as $subj_id => $subj) { ?>
                                            <dt class="subj_<?=$subj_id?>">
                                                <label title="<?=$subj?>">
                                                    <input type="checkbox" value="<?=$subj_id?>" name="subject_id[<?=$subj_id?>]"<?=(isset($form_back['add_subject']['select_subject'][$subj_id]) ? " checked='checked'" : "")?> /><?=$subj?>
                                                </label>
                                            </dt>
                                            <dd>
                                                <label>���� ����������: <span></span><input class="subject_code" type="text" maxlength="10" name="subject_code[<?=$subj_id?>]" value="<?=(isset($form_back['add_subject']['select_subject'][$subj_id]) ? $form_back['add_subject']['select_subject'][$subj_id] : "")?>" /></label>
                                            </dd>
<?php                   } ?>
                                        </dl>
                                    </li>
<?php               } ?>
                                </ul>
<?php           } ?>
                            </div>
                        </div>
                        <div class="wide">
                            <input type="submit" value="��������" id="add_cycle"/>
                        </div>
                    </div>
                </fieldset>
            </form>
<?php       } ?>
            <div class="wide">
                <div class="form-notes">
                    <p>����������� ����������, �������������:</p>
                    <p>���������:</p>
                </div>
                <div class="form-notes">
                    <p><?=$curriculum['scode']?><?=(strpos($curriculum['scode'], ".")===false) ? ".".$curriculum['stcode'] : ""?> - <?=$curriculum['sname']?></p>
                    <p><?=$curriculum['fname']?></p>
                </div>
                <div class="clear"></div>
            </div>
<?php       if(isset($curriculum['subject_group'])) {
                foreach($curriculum['subject_group'] as $group_id => $subject_group) { ?>
            <fieldset class="subject_group_item">
                <form class="form_legend edit_form del_sgroup" action="<?=$EE["unqueried_uri"]?>" method="post">
                    ����: <span class="action_del"><?=$subject_group['sgroup_name']?></span>
                    <input class="action_edit" type="text" maxlength="10" name="edit_sgroup_code" size="10" value="<?=$subject_group['sgroup_code']?>" title="����" />
                    <input class="action_edit" type="text" name="edit_sgroup_name" size="50" value="<?=$subject_group['sgroup_name']?>" title="�������� ����� ���������" />
                    <input type="hidden" name="edit_sgroup_id" value="<?=$group_id?>" />
                    <button class="input_submit action_del" type="submit" value="" title="������� (���� ��� ���������)"></button>
                    <button class="input_submit action_edit" type="submit" value="" title="��������� ���������"></button>
                    <label class="inline-block action_del" title="�������������"><input type="radio" name="action_del" value="0" /></label>
                    <label class="inline-block action_edit" title="������ ��������������"><input type="radio" name="action_del" value="1" checked='checked' /></label>
                </form>
                <div class="subject_list">
<?php               foreach($subject_group['subjects'] as $cur_id => $subjects) { ?>
                    <div class="subject_row">
                        <form class="del_sgroup edit_form" action="<?=$EE["unqueried_uri"]?>" method="post">
                            <div class="subject_action">
                                <input type="hidden" name="edit_cur_id" value="<?=$cur_id?>" />
                                <input type="hidden" name="edit_subject_id" value="<?=$subjects['subj_id']?>" />
                                <button class="input_submit action_del" type="submit" value="" title="�������"></button>
                                <button class="input_submit action_edit" type="submit" value="" title="���������"></button>
                                <label class="inline-block action_del" title="�������������"><input type="radio" name="action_del" value="0" /></label>
                                <label class="inline-block action_edit" title="������ ��������������"><input type="radio" name="action_del" value="1" checked='checked' /></label>
                            </div>
                            <div class="subject_pos">
                                <span class="action_del"><?=$subjects['pos']?></span>
                                <input class="action_edit" type="text" maxlength="5" pattern="^[0-9]+$" name="edit_subject_pos" size="3" value="<?=$subjects['pos']?>" title="������� ���������� (������ �����)" />
                            </div>
                            <div class="subject_code">
                                <span class="action_del"><?=$subject_group['sgroup_code']?>.<?=$subjects['scode']?></span>
                                <input class="action_edit" type="text" maxlength="10" name="edit_subject_code" size="10" value="<?=$subjects['scode']?>" title="���� ����������" />
                            </div>
                            <div class="subject_cn">
                                <div class="subject_name">
                                    <span class="action_del" <?=$subjects['is_hidden'] ? "style='color: gray'" : ""?>><?=$subjects['sname']?></span>
<?php           if(isset($curriculum['departments'])) { ?>
                                    <select class="action_edit" size="1" name="select_subject_id" title="�������� ����������">
<?php               foreach($curriculum['departments'] as $dep_id => $dep) { ?>
                                        <optgroup label="<?=$dep['name']?>">
<?php                   foreach($dep['subjects'] as $subj_id => $subj) {
                            if(!isset($subject_group['subjects'][$subj_id]) || $subjects['subj_id'] == $subj_id) { ?>
                                            <option value="<?=$subj_id?>"<?=($subjects['subj_id'] == $subj_id ? " selected='selected'" : "")?>><?=$subj?></option>
<?php                       }
                        }?>
                                        </optgroup>
<?php               } ?>
                                    </select>
<?php           } ?>
                                </div>
                            </div>
                            <div class="subject_dep"><?=$subjects['sdep']?></div>
                        </form>
                    </div>
<?php               } ?>
                </div>
            </fieldset>
<?php           }
            } else { ?>
            <fieldset><p>������� ���� �� ��������</p></fieldset>
<?php       } ?>
        </div>
<?php   }
    }
    break;

    case "search_spec": { ?>
        <h2>����</h2>
        <form action="/speciality/" method="get">
            <p>
                �������������:
                <input name="name_search" type="text" size="35"/> <input type="submit" value="�����" class="button" />
            </p>
        </form>
<?php
    }
    break;

    case "search_people": ?>
        <form id="search_people_form" action="/people/" method="get">
            <fieldset>
                <legend>�������</legend>
                <p>
                    <input name="person_name" type="text" size="20" /> <input type="submit" value="�����" class="button" />
                </p>
            </fieldset>
        </form>
<?php   break;

    case "people_info": 
    {
            // echo "<pre>";
            // print_r($MODULE_OUTPUT["man"]);
            // echo "</pre>";
            if(isset($MODULE_OUTPUT["info_view"])) 
            {
                if(!empty($MODULE_OUTPUT["man"])) 
                {
                    $man = $MODULE_OUTPUT["man"]; 


                    // if($Auth->user_id==128971)
                    // {
                    // CF::Debug($MODULE_OUTPUT["fuck"]);
                    // }
                    ?>
                    
                    <h4><strong>����� ����������</strong></h4>
                    <?
                    if($man["status"]!="�������") 
                    {
                        ?>

                        <p itemprop="Post">���������: 
                            <strong>
                                <?
                                if(!empty($man["post"]) && is_array($man["post"])) 
                                {

                                    foreach ($man["post"] as $dep => $post) 
                                    {
                                        $post_iter = 0;
                                        foreach($post as $name)
                                        {
                                           // echo $name;
                                            echo (!$post_iter) ? $name : ", ".$name;
                                            $post_iter++;
                                        }
                                        if (count($man["post"])>1) 
                                        {
                                            echo " (".$dep.")";
                                        }
                                        echo "<br>";
                                    }
                                    ?>
                                    <?
                                } 
                                elseif (!empty($man["post"])) 
                                {
                                   echo $man["post"];
                                }
                                else 
                                {
                                    echo "��� ������";
                                }
                                ?>    
                            </strong>
                        </p>
                        <?
                    } 
                    elseif($man["status"]=="�������") 
                    {   
                        ?>
                        <p><a href="<?=$man["fac_link"]?>"><?=$man["fac"]?></a></p>
                        <?
                        if ($man["fac"]=="����� �� ���������� ������-�������������� ������") 
                        {
                            ?><p><a href="https://nsau.edu.ru/nir/depart_teaching/aspirantura/napravleniya-podgotovki/"><?=$man["spec_code"]?> - <?=$man["spec"]?>, <?=CF::lowCaseOne($man["qual"])?></a>.</p><?
                        }
                        else
                        {
                            ?><p><a href="<?=$man["spec_link"]."#".$man["spec_code"]?>"><?=$man["spec_code"]?> - <?=$man["spec"]?>, <?=CF::lowCaseOne($man["qual"])?></a>.</p><?
                        }
                        ?>
                        <p>������: <?=$man["group"]?>, <?=$man["course"]?> ����.</p><?
                    }
                    ?>
                    <!--
                    <p itemprop="Telephone">�������: <strong id="phone"><?if(!empty($man["phone"])) {?>+7 (383) <?=mb_substr($man["phone"],0,3)?>-<?=mb_substr($man["phone"],3,2)?>-<?=mb_substr($man["phone"],5,2)?><?} else echo "��� ������";?></strong></p>
                    <p itemprop="e-mail">E-mail: <strong><?if(!empty($man["email"])) {?><?=$man["email"]?><?} else echo "��� ������";?></strong></p>
                    <p itemprop="">������������: <strong><?if(!empty($man["location"])) {?><?=$MODULE_OUTPUT["buildings"][$man["location_building"]]["label"]?><?=$man["location"]?> (<a href="/directory/map/"><?=$MODULE_OUTPUT["buildings"][$man["location_building"]]["name"]?></a>)<?} else echo "��� ������";?></strong></p>

                    <?=($man["post"] && 0 ? "<p><strong>���������:</strong> ".$man["post"]."</p>" : "")?>
                    <?  if ($man["status_id"] == 2) {?>
                        <?="<p>���� ������: <strong><span  itemprop='GenExperience'>".($man["exp_full"] ? (intval(date('Y'))- $man["exp_full"]).( ( (((intval(date('Y'))- $man["exp_full"])%10) < 5) && (((intval(date('Y'))- $man["exp_full"])%10) != 0) && ((intval(date('Y'))- $man["exp_full"]) > 14 || (intval(date('Y'))- $man["exp_full"]) < 5) ) ? ( ((intval(date('Y'))- $man["exp_full"])%10) > 1 ? " ����": " ���" ) : " ���") : " ��� ������")."</span> </strong><br />"?>
                        <?="&nbsp;� ��� ����� �������������� (������-��������������):<strong><span itemprop='SpecExperience'>".($man["exp_teach"] ? (intval(date('Y'))- $man["exp_teach"]).( ( (((intval(date('Y'))- $man["exp_teach"])%10) < 5) && (((intval(date('Y'))- $man["exp_teach"])%10) != 0) && ((intval(date('Y'))- $man["exp_teach"]) > 14 || (intval(date('Y'))- $man["exp_teach"]) < 5) ) ? ( ((intval(date('Y'))- $man["exp_teach"])%10) > 1 ? " ����": " ���" ) : " ���") : " ��� ������")."</span></strong><br>"?>
                        �������: <strong itemprop="Degree"><?=$MODULE_OUTPUT["teacher"]["degree"][$man["degree"]]["name"]?></strong><br>
                        ������: <strong itemprop="AcademStat"><?$i=1;foreach($man["academ_stat"] as $id) {if($i==1) echo $MODULE_OUTPUT["teacher"]["rank"][$id]["name"].($i<count($man["academ_stat"]) ? ", " : ""); else echo strtolower($MODULE_OUTPUT["teacher"]["rank"][$id]["name"]).($i<count($man["academ_stat"]) ? ", " : ""); $i++;}?></strong></p><br>
                    <?}?>!-->

            <?php           if(isset($MODULE_OUTPUT["people_fac_dep"])) { ?>
                        <dl>
                            <dt>����� ������:</dt>
            <?php               foreach($MODULE_OUTPUT["people_fac_dep"] as $fac) { ?>
                            <dd>
                                <dl>
                                    <dt><a href="<?=(($fac['uri'][0]=="/") || (strpos($fac["uri"], "http")!==false)) ? "" : "/"?><?=$fac['uri']?>"><?=$fac['name']?></a></dt>
            <?php                   foreach($fac['departments'] as $dep) { ?>
                                    <dd><a href="<?=$dep['uri']?>"><?=$dep['name']?></a></dd>
            <?php                   } ?>
                                </dl>
                            </dd>
            <?php               } ?>
                        </dl>
            <?php           }
                            /*if(isset($MODULE_OUTPUT["subjects"])) { ?>
                            <dl>
                                <dt>������������� ����������:</dt>
            <?php               foreach($MODULE_OUTPUT["subjects"] as $subj) { ?>
                                <dd><?=$subj?></dd>
            <?php               } ?>
                            </dl>
            <?php           }*/

            ?>



                        <?}
                    } else { ?>
                    <div class="shadow_rbg">
                        <div class="people_info">
            <?php       if(!empty($MODULE_OUTPUT["man"])) {
                            $man = $MODULE_OUTPUT["man"]; ?>
                            <div class="photo">
            <?php           if($man["photo"]) { ?>
                                <a href="/images/people/<?=$man["original_photo"]?>" target="_blank" onclick="return hs.expand(this);">
                                    <img src="/images/people/<?=$man["photo"]?>" alt="" />
                                </a>
            <?php           } else { ?>
                                <img src="/images/people/no_photo.jpg" alt="" />
            <?php           } ?>
                            </div>
                            <div class="people_name"><a href="/people/<?=$man["id"]?>"><span><?=$man["last_name"]?></span><br /><?=$man["name"]?> <?=$man["patronymic"]?></a></div>

                            <small><a href="/people/edit/<?=$man["id"]?>">[������������� �������]</a></small>
            <?php       } else { ?>
                            <div class="photo">
                                <img src="/images/people/no_photo.jpg" alt="" />
                            </div>
            <?php           if(isset($MODULE_OUTPUT['user_displayed_name'])) { ?>
                            <div class="people_name"><?=$MODULE_OUTPUT['user_displayed_name']?></div>
            <?php           } else { ?>
                            <p>���������� �����������.</p>
            <?php           }
                        } ?>
                        </div>
                        <div class="shadow_tr">&nbsp;</div>
                        <div class="shadow_b">
                            <div class="shadow_br">&nbsp;</div>
                            <div class="shadow_bl">&nbsp;</div>
                        </div>
                    </div>
            <?php
                    }
    }
    break;

 case "people": {
  if(!empty($MODULE_OUTPUT["man"])) {
        if ($MODULE_OUTPUT["allow_peopledit"]) { ?>
            <script>jQuery(document).ready(function($) {AjaxEditPhoto(<?=$MODULE_OUTPUT["module_id"]?>);});</script>

        <?}?>
        <?$man = $MODULE_OUTPUT["man"];?>

        <input type="hidden" class="people_id" value="<?=$man["id"]?>">
        <?//����� ����. ��� ��������, �������� ��� ������������ �����////////////////////////////////////////////////?>
    <div class="people shadow_rbg">
            <div id="test_photo"></div>
            <div class="people_photo float-left">
                <em class="hidden"></em>
                <?if($man["photo"]) { ?>
                    <a class="big_photo" href="/images/people/<?=$man["original_photo"]?>" target="_blank" onclick="return hs.expand(this);">
                        <img src="/images/people/<?=$man["photo"]?>" alt="" />
                    </a>
                    <?if($MODULE_OUTPUT["allow_peopledit"] && ($Auth->people_id==$man["id"] || $Auth->usergroup_id==1)) { ?>
                        <a href="javascript:void(0);" class="edit_photo">������������� ����</a>
                    <?}?>
                <?} else { ?>
                    <img src="/images/people/no_photo.jpg" alt="" />

                <?} ?>
            </div>
            <h1 itemprop="fio"><?=$man["last_name"]?> <?=$man["name"]?> <?=$man["patronymic"]?></h1>
            <?if($man["people_cat"]!=1) {?>
                <?if($man["status_id"]==9){?><p><strong>������</strong></p><?}?>
                <p itemprop="Post">���������: 
                        <?
                        if(!empty($man["post"])) 
                        {
                            if(is_array($man['post'])) 
                            {
                                $i=1;
                                foreach($man['post'] as $d_id => $posts) 
                                {

                                    foreach($posts as $p_id) 
                                    {

                                        ?>
                                        <strong><?=$MODULE_OUTPUT["teacher"]["post"][$p_id]["name"]?></strong>
                                        <?
                                        if(count($man['post']) > 1 || count($posts) > 1) 
                                        {
                                            ?>
                                            <small>(<?=$MODULE_OUTPUT["departments"][$d_id]?>)<?=(count($man['post']) > $i || count($posts) > $i) ? ',' : ''?></small> 
                                            <?
                                        }
                                        $i++;
                                    }
                                }
                            } 
                            else 
                            {
                                ?>
                                <strong><?=$man["post"]?></strong>
                                <?
                            }
                            ?>
                            <!-- <?=($man["people_cat"]==2) ? $MODULE_OUTPUT["teacher"]["post"][$man["post"]]["name"] : $man["post"]?> -->
                            <?
                        } 
                        else echo "��� ������";?>
                        
                    
                </p>
                <p itemprop="Telephone">�������: <strong id="phone"><?if(!empty($man["phone"])) {?>+7 (383) <?=mb_substr($man["phone"],0,3)?>-<?=mb_substr($man["phone"],3,2)?>-<?=mb_substr($man["phone"],5,2)?><?} else echo "��� ������";?></strong></p>
                <p itemprop="e-mail">E-mail: <strong><?if(count($man["email"])>0) {?><?foreach($man["email"] as $i => $email){?><a href="mailto:<?=$email?>"><?=$email?></a><?=($i<(count($man["email"])-1)) ? ", " : ""?><?}} else echo "��� ������";?></strong></p>
                <p itemprop="">������������: <strong><?if(!empty($man["location"])) {?><?=$MODULE_OUTPUT["buildings"][$man["location_building"]]["label"]?>-<?=$man["location"]?> (<a href="/directory/map/"><?=$MODULE_OUTPUT["buildings"][$man["location_building"]]["name"]?></a>)<?} else echo "��� ������";?></strong></p>
            <?} else {?>
                <?//���������� ����� ����?>
                <?if($man["status"] == "����������" && !empty($MODULE_OUTPUT["abit_specs"])) { ?>
                <p><strong>����������</strong></p>
                    <p>�������������:

                        <?$i = 1;
                        foreach ($MODULE_OUTPUT["abit_specs"] as $spec) {
                            if (!empty($spec["code"])) {
                                echo '<a href="'.$spec['abit_list_base_href'].$spec["type_id"].'" title="������ ����������� �� �������������  &quot;'.$spec["name"].'&quot;">'.$spec["code"];
                                echo '</a>';
                                    $to_show = array();
                                if (!empty($spec["name"]) && $spec["name"])
                                    $to_show[] = '<a href="'.$spec['abit_list_base_href'].$spec["type_id"].'" title="������ ����������� �� �������������  &quot;'.$spec["name"].'&quot;">'.$spec["name"].'</a>';
                                if (!empty($spec["ege"]) && $spec["ege"])
                                    $to_show[] = "������: ".$spec["ege"];
                                if (count($to_show))
                                    echo " (".implode(', ', $to_show).")";
                                if ($i < count($MODULE_OUTPUT["abit_specs"]))
                                    echo ", ";
                            }
                        $i++;
                        }?>
                    </p>

                <?}?>
                <?//end?>
                <?if($man["status_id"]!=7) {?>
                    <?//������ �����
                    if (substr($man["group_name"], 0, 1) == "a" || substr($man["group_name"], 0, 1) == "�")
                    {
                        ?><p><strong>��������</strong></p><?
                    }
                    else
                    {
                        ?><p><strong>�������</strong></p><?
                    }
                    ?>
                        <p>����������� ����������: <strong><?=$man["specialities_code"]?> - <?=$man["spec_name"]?></strong></p>
                        <p>������� ����������: <strong><?=$man["qual_name"]?></strong></p>
                        <p>��� �����������: <strong><?=($man["year"] && $man["year"]!="0000" && $man["status"]=="�������") ? $man["year"] : "��� ����������"?></strong></p>
                        <p>������: <strong><?=$man["group_name"] ? "<a href=\"/student/timetable/groups/".$man["id_group"] ."/\">".$man["group_name"]."</a>" : "��� ����������"?></strong></p>
                    <?//end?>
                <?}?>
            <?}?>
            <?//�������������/�������?>
                <?
                if ($MODULE_OUTPUT["allow_peopledit"])
                    {
                        //������� ����������� ������ ����, � �� ������ ���������
                        if($Auth->usergroup_id==3)
                        {
                            if ($Auth->people_id==$man["id"])
                            {
                                ?><p class="actions"><a href="/people/edit/<?=$man["id"]?>/">�������������</a><?
                            }
                        }
                        else
                        {
                            if ($Auth->people_id==$man["id"] || $Auth->usergroup_id==1)
                            {
                                ?><p class="actions"><a href="/people/edit/<?=$man["id"]?>/">�������������</a><?
                            }
                        }
                    }
                if($Auth->usergroup_id==1)
                    {
                        ?> | <a href="/people/delete/<?=$man["id"]?>/">�������</a> | <?
                    }

                if ($MODULE_OUTPUT["allow_people_password_restore"]==1 && $MODULE_OUTPUT["restore_option"])
                {
                    ?>
                    <a data-toggle="modal" data-target="#myModal" class="restore_password" id=<?=$man['id']?>>������������ ����� � ������</a>
                    <div class="modal fade" id="myModal" role="dialog" style="margin-top: 100px;">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">�</button> 
                                    <h4 class="modal-title">����� ������ ��� ����� � ������� ������</h4> 
                                </div> 
                                <div class="modal-body" id="new_restore_data">
                                    
                                </div> 
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-nsau-grey print_new_data"> 
                                        �����������
                                    </button>
                                    <button type="button" class="btn btn-nsau-grey" data-dismiss="modal"> 
                                        �������
                                    </button>
                                </div> 
                            </div> 
                        </div> 
                    </div> 
                    <?
                }
                ?>
                </p>
            <?//end?>
            <div class="shadow_tr">&nbsp;</div>
            <div class="shadow_b">
                <div class="shadow_br">&nbsp;</div>
                <div class="shadow_bl">&nbsp;</div>
            </div>
        </div>
        <div class="people_content">
            <?//////////////////////////////////////////////////////////////////////////////////////////////?>
            <?//����� - ����� ����//////////////////////////////////////////////////////////////////////////?>
            <?//������ /////////////////////////////////////////////////////////////////////////////////////?>
                <?//����� ����?>
                    <?if ($man["status_id"] == 2) {?>
                        <br><strong itemprop="Degree"><?foreach($man["degree"] as $id => $val){ if($id>0) echo CF::lowCaseOne($MODULE_OUTPUT["teacher"]["degree"][$val]["name"]).($id<(count($man["degree"])-1) ? ", " : ""); else  echo $MODULE_OUTPUT["teacher"]["degree"][$val]["name"].($id<(count($man["degree"])-1) ? ", " : "");} if((count($man["academ_stat"])>0) && (count($man["degree"])>0)) echo ",";?></strong>
                        <strong itemprop="AcademStat"><?$i=1;foreach($man["academ_stat"] as $id) {if(($i==1) && empty($man["degree"])) echo $MODULE_OUTPUT["teacher"]["rank"][$id]["name"].($i<count($man["academ_stat"]) ? ", " : ""); elseif(isset($man["academ_stat"])) echo CF::lowCaseOne($MODULE_OUTPUT["teacher"]["rank"][$id]["name"]).($i<count($man["academ_stat"]) ? ", " : ""); $i++;}?></strong>
                        <?="<p><strong>���� ������: </strong><span  itemprop='GenExperience'>".($man["exp_full"] ? (intval(date('Y'))- $man["exp_full"]).( ( (((intval(date('Y'))- $man["exp_full"])%10) < 5) && (((intval(date('Y'))- $man["exp_full"])%10) != 0) && ((intval(date('Y'))- $man["exp_full"]) > 14 || (intval(date('Y'))- $man["exp_full"]) < 5) ) ? ( ((intval(date('Y'))- $man["exp_full"])%10) > 1 ? " ����": " ���" ) : " ���") : " ��� ������")."</span> <br />"?>
                        <?="&nbsp;<strong>� ��� ����� �������������� (������-��������������):</strong><span itemprop='SpecExperience'>".($man["exp_teach"] ? (intval(date('Y'))- $man["exp_teach"]).( ( (((intval(date('Y'))- $man["exp_teach"])%10) < 5) && (((intval(date('Y'))- $man["exp_teach"])%10) != 0) && ((intval(date('Y'))- $man["exp_teach"]) > 14 || (intval(date('Y'))- $man["exp_teach"]) < 5) ) ? ( ((intval(date('Y'))- $man["exp_teach"])%10) > 1 ? " ����": " ���" ) : " ���") : " ��� ������")."</span><br>"?>
                    <?}?>
                <?//end?>

                <?//������������� ����������?>
                    <?
                    if (isset($MODULE_OUTPUT["displayed_departments"])) {?>
                        <p itemprop="TeachingDiscipline">
                        <strong>����� ������: </strong><br />
                        <?foreach ($MODULE_OUTPUT["displayed_departments"] as $key => $value) {
                            if ($value["url"])
                                echo "<a href=\"".$value["url"]."\">".$value["name"]."</a>";
                            else
                                echo $value["name"]." ";
                            if (count($MODULE_OUTPUT["displayed_subjects"][$key]) != 0) {
                                asort($MODULE_OUTPUT["displayed_subjects"][$key]);
                                echo "<br><strong>������������� ����������:</strong> <br />";
                                foreach ($MODULE_OUTPUT["displayed_subjects"] as $dep_id => $subject) {
                                    if ($dep_id==$key) {
                                        if(!empty($subject)) {
                                            echo "";
                                            $j=0;
                                            foreach ($subject as $x => $value) {
                                                echo $value;
                                                $j++;
                                                 echo "</br>";
                                            }
                                            echo "";
                                        }
                                    }
                                }
                            } else echo "</br>";
                        }?>
                        </p>
                    <?}?>
                <?//end?>

                <?//������� �����?>
                <?if(!empty($MODULE_OUTPUT["curator"])){?>
                    <strong>������� �����: </strong>
                    <?foreach($MODULE_OUTPUT["curator"] as $i => $gr_name) {?>
                        <?=$gr_name?><?=($i<(count($MODULE_OUTPUT["curator"])-1)) ? ", " : ""?>
                    <?}?>
                <?}?>
                <?//end?>


                <?//����������?>
                    <?php echo $man["comment"] ? " ".$man["comment"] ." " : ""?>
                <?//end?>

            <?//////////////////////////////////////////////////////////////////////////////////////////////?>
            <?//////////////////////////////////////////////////////////////////////////////////////////////?>
            <?//////////////////////////////////////////////////////////////////////////////////////////////?>
        </div>

        <?
        }
        else
        {
                        ?>
            <form action="/people/" method="get">
            <p>
            �������:
            <input name="person_name" type="text" size="35" placeholder="���" value="<?php echo isset($_GET["person_name"]) ? $_GET["person_name"] : "" ?>"/> <input type="submit" value="�����" class="button"/>
            </p>
            </form>
            <?
            if(!empty($MODULE_OUTPUT["people"]))
            {
                ?>
                <ul>
                <?php
                foreach($MODULE_OUTPUT["people"] as $man)
                {
                    ?>
                    <li><a href="/people/<?=$man["id"]?>/"><?=$man["last_name"]?> <?=$man["name"]?> <?=$man["patronymic"]?></a></li>
                    <?php

                } //(<?=$man["test"]=$man["order"]
                ?>
                </ul>
                <?php
            }
            else
            {
                if(isset($_GET["person_name"]))
                {
                ?>
                <p>�� ������� ����� � ������ ����������� ������.</p>
                <?php
                }
                else
                {
                ?>
                <p>������� � ���� ������ ������ � ��������.</p>
                <?php
                }
            };
            if ($MODULE_OUTPUT["allow_addman"]) { ?><p class="actions"><a href="/people/add/">�������� ��������</a></p><? }

        };
    }
    break;



    case "people_menu_main": {?>
    <?header("Content-type: text/html; charset=windows-1251");?>
    <?$man = $MODULE_OUTPUT["man"];    ?>
<?//////////////////////////////////////////////////////////////////////////////////////////////?>
<?//����� - ����� ����//////////////////////////////////////////////////////////////////////////?>
<?//������ /////////////////////////////////////////////////////////////////////////////////////?>

    <?//����� ����?>
        <?if ($man["status_id"] == 2) {?>
            <br><strong itemprop="Degree"><?foreach($man["degree"] as $id => $val){ if($id>0) echo CF::lowCaseOne($MODULE_OUTPUT["teacher"]["degree"][$val]["name"]).($id<(count($man["degree"])-1) ? ", " : ""); else  echo $MODULE_OUTPUT["teacher"]["degree"][$val]["name"].($id<(count($man["degree"])-1) ? ", " : "");} if((count($man["academ_stat"])>0) && (count($man["degree"])>0)) echo ",";?></strong>
            <strong itemprop="AcademStat"><?$i=1;foreach($man["academ_stat"] as $id) {if(($i==1) && empty($man["degree"])) echo $MODULE_OUTPUT["teacher"]["rank"][$id]["name"].($i<count($man["academ_stat"]) ? ", " : ""); elseif(isset($man["academ_stat"])) echo CF::lowCaseOne($MODULE_OUTPUT["teacher"]["rank"][$id]["name"]).($i<count($man["academ_stat"]) ? ", " : ""); $i++;}?></strong>
            <?="<p><strong>���� ������: </strong><span  itemprop='GenExperience'>".($man["exp_full"] ? (intval(date('Y'))- $man["exp_full"]).( ( (((intval(date('Y'))- $man["exp_full"])%10) < 5) && (((intval(date('Y'))- $man["exp_full"])%10) != 0) && ((intval(date('Y'))- $man["exp_full"]) > 14 || (intval(date('Y'))- $man["exp_full"]) < 5) ) ? ( ((intval(date('Y'))- $man["exp_full"])%10) > 1 ? " ����": " ���" ) : " ���") : " ��� ������")."</span> <br />"?>
            <?="&nbsp;<strong>� ��� ����� �������������� (������-��������������):</strong><span itemprop='SpecExperience'>".($man["exp_teach"] ? (intval(date('Y'))- $man["exp_teach"]).( ( (((intval(date('Y'))- $man["exp_teach"])%10) < 5) && (((intval(date('Y'))- $man["exp_teach"])%10) != 0) && ((intval(date('Y'))- $man["exp_teach"]) > 14 || (intval(date('Y'))- $man["exp_teach"]) < 5) ) ? ( ((intval(date('Y'))- $man["exp_teach"])%10) > 1 ? " ����": " ���" ) : " ���") : " ��� ������")."</span><br>"?>
        <?}?>
    <?//end?>

    <?//������������� ����������?>
        <?if (!empty($MODULE_OUTPUT["displayed_departments"])) {?>
            <p itemprop="TeachingDiscipline">
            <strong>����� ������: </strong><br />
            <?foreach ($MODULE_OUTPUT["displayed_departments"] as $key => $value) {
                if ($value["url"])
                    echo "<a href=\"".$value["url"]."\">".$value["name"]."</a>";
                else
                    echo $value["name"]." ";
                if (count($MODULE_OUTPUT["displayed_subjects"][$key]) != 0) {
                    asort($MODULE_OUTPUT["displayed_subjects"][$key]);
                    echo "<br><strong>������������� ����������:</strong> <br />";
                    foreach ($MODULE_OUTPUT["displayed_subjects"] as $dep_id => $subject) {
                        if ($dep_id==$key) {
                            if(!empty($subject)) {
                                echo "";
                                $j=0;
                                foreach ($subject as $x => $value) {
                                    echo $value;
                                    $j++;
                                     echo "</br>";
                                }
                                echo "";
                            }
                        }
                    }
                } else echo "</br>";
            }?>
            </p>
        <?}?>
    <?//end?>
    <?//������� �����?>
    <?if(!empty($MODULE_OUTPUT["curator"])){?>
        <strong>������� �����: </strong>
        <?foreach($MODULE_OUTPUT["curator"] as $i => $gr_name) {?>
            <?=$gr_name?><?=($i<(count($MODULE_OUTPUT["curator"])-1)) ? ", " : ""?>
        <?}?>
    <?}?>
    <?//end?>

    <?//����������?>
        <?php echo $man["comment"] ? " ".$man["comment"] ." " : ""?>
    <?//end?>

<?//////////////////////////////////////////////////////////////////////////////////////////////?>
<?//////////////////////////////////////////////////////////////////////////////////////////////?>
<?//////////////////////////////////////////////////////////////////////////////////////////////?>
    <?}
    break;

    case "people_menu_in_development" : {?>
    <?header("Content-type: text/html; charset=windows-1251");?>
        <p>��������, ������ ������ ��������� � ������ ����������.</p>
    <?}
    break;

    /*
    //������ ������ ����������� ����� ��� ���������
    case "people_menu_student_work" : {?>
    <?header("Content-type: text/html; charset=windows-1251");?>
        <div class="sw_content">
            <?foreach($MODULE_OUTPUT["sw"] as $wtype => $works) {?>
                <fieldset>
                <legend><h2><?=$MODULE_OUTPUT["nfv"][$wtype]?></h2></legend>
                <?foreach($works as $wid => $work) {?>
                    <?=$work["subject"] ? "����������: ".$work["subject"].", " : ""?> <?=$work["subject"] ? "�������� ������" : "�������� ������"?> :<?=$work["work_name"]?>, ������� ������������:
                    <?if(!empty($work["teacher_people_id"])) {?><a href="/people/<?=$work["teacher_people_id"]?>"><?=$work["teacher"]?></a><?} else {?><?=$work["teacher"]?><?}?>

                    <?if(!empty($work["files"])){?>
                    (<?foreach($work["files"] as $fid=>$fname){?> <a href="/file/<?=$fid?>"><?=$fname?></a><?}?>)
                    <?}?>
                    <br/>
                <?}?>
                </fieldset>
            <?}?>

            <?//=CF::Debug($MODULE_OUTPUT["sw"]);?>
        </div>
    <?}
    break;
    */

    /*����������� ����� ��� ���������*/
    case "people_menu_student_work" : {?>
    <?header("Content-type: text/html; charset=windows-1251");?>

        <div class="sw_content">
            <?foreach($MODULE_OUTPUT["sw"] as $wtype => $works)
            
            {?>
                <style>
                            .kind_action {
                    margin-top: 10px;
                    margin-bottom: -12px;
                }
                </style>

                <?
                // if ($Auth->user_id=='128971') 
                // {
                //    CF::Debug($works);
                // }
                ?>
                <div class='kind_action'>
                <center><legend><h1><?=$MODULE_OUTPUT["nfv"][$wtype]?></h1></legend></center>
                </div>
                <table class="nsau_table employees izop" width="100%">
                    <thead>
                        <tr style="word-break: break-all;">
                            <th width="15%" >�������� ������</th>
                            <th width="10%" >������� ������������/�������������</th>
                            <th width="10%" >��������</th>
                            <th width="10%" >��������</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?
                        foreach ($works as $wid => $work)
                        {
                            ?>
                            <tr>
                                <td><?=$work["subject"] =$work["work_name"] ?></td>
                                <td>
                                    <?if(!empty($work["teacher_people_id"])) {?><a href="/people/<?=$work["teacher_people_id"]?>"><?=$work["teacher"]?></a><?} else {?><?=$work["teacher"]?><?}?>
                                </td>
                                <td>
                                <?if(!empty($work["files"]))
                                {?>
                                    <?foreach($work["files"] as $fid=>$fname)
                                    {
                                        if ($work['feedback_id']!=$fid) 
                                        {
                                            ?><a href="/file/<?=$fid?>">�������</a><br><?
                                        }
                                    }
                                    ?>
                                    <?
                                }?>
                                </td>
                                <td>
                                <?if(!empty($work["files"]))
                                {?>
                                    <?foreach($work["files"] as $fid=>$fname)
                                    {
                                        if ($work['feedback_id']==$fid) 
                                        {
                                            ?><a href="/file/<?=$fid?>">�������</a><br><?
                                        }
                                    }
                                    ?>
                                    <?
                                }?>    
                                </td>
                            </tr>
                            <?
                        }
                        ?>
                    </tbody>
                </table>
            <?}?>
        </div>
    <?}

    break;

   /*
   //������ ������ ����������� ����� ��� �������������
    case "people_menu_students_work" : {?>
    <?header("Content-type: text/html; charset=windows-1251");?>
        <div class="sw_content">
            <?foreach($MODULE_OUTPUT["sw"] as $wtype => $works) {?>
                <fieldset>
                <legend><h2><?=$MODULE_OUTPUT["nfv"][$wtype]?></h2></legend>
                <?foreach($works as $wid => $work) {?>
                    <?=$work["subject"] ? "����������: ".$work["subject"].", " : ""?> <?=$work["subject"] ? "�������� ������" : "�������� ������"?> :<?=$work["work_name"]?>, �����: <a href="/people/<?=$work["student_people_id"]?>"><?=$work["student"]?></a>(<?=$work["group_name"]?>)
                    <?if(!empty($work["files"])){?>
                    (<?foreach($work["files"] as $fid=>$fname){?> <a href="/file/<?=$fid?>"><?=$fname?></a><?}?>)
                    <?}?>
                    <br />
                <?}?>
                </fieldset>
            <?}?>
            <?//=CF::Debug($MODULE_OUTPUT["sw"]);?>
        </div>
    <?}
    break;*/

    /*����������� ����� ��� ��������������*/
    case "people_menu_students_work" : {?>
    <?header("Content-type: text/html; charset=windows-1251");?>
        <div class="sw_content">
            <?foreach($MODULE_OUTPUT["sw"] as $wtype => $works) {?>
                            <?
                // if ($Auth->user_id=='128971') 
                // {
                //    CF::Debug($works);
                // }
                ?>
                <legend><h2><?=$MODULE_OUTPUT["nfv"][$wtype]?></h2></legend>

                <table class="nsau_table employees izop" width="100%">
                    <thead>
                        <tr style="word-break: break-all;">
                            <th width="15%" >�������� ������</th>
                            <th width="10%" >�����</th>
                            <th width="10%" >��������</th>
                            <th width="10%" >��������</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?
                        foreach ($works as $wid => $work)
                        {
                            ?>
                            <tr>
                                <td><?=$work["subject"] =$work["work_name"] ?></td>
                                <td>
                                                                    <?if(!empty($work["student_people_id"])) {?>
                                    <a href="/people/<?=$work["student_people_id"]?>">
                                    <?=$work["student"]?></a> <br> ������: <?=$work["group_name"]?>
                                                                    <?} else {?>
                                                                        <?=$work["student"]?>
                                                                    <?}?>
                                </td>
                                <td>
                                <?if(!empty($work["files"]))
                                {?>
                                    <?foreach($work["files"] as $fid=>$fname)
                                    {
                                        if ($work['feedback_id']!=$fid) 
                                        {
                                            ?><a href="/file/<?=$fid?>">�������</a><br><?
                                        }
                                    }
                                    ?>
                                    <?
                                }?>
                                </td>
                                <td>
                                <?if(!empty($work["files"]))
                                {?>
                                    <?foreach($work["files"] as $fid=>$fname)
                                    {
                                        if ($work['feedback_id']==$fid) 
                                        {
                                            ?><a href="/file/<?=$fid?>">�������</a><br><?
                                        }
                                    }
                                    ?>
                                    <?
                                }?>    
                                </td>
                            </tr>
                            <?
                        }
                        ?>
                    </tbody>
                </table>
            <?}?>
        </div>
    <?}
    break;


    /*����������� �����������*/
    case "people_menu_sub_author" : {?>
    <?header("Content-type: text/html; charset=windows-1251");?>
        <div class="sw_content">


<?
            if (isset($MODULE_OUTPUT['sub_author']))
            {
                if (!empty($MODULE_OUTPUT['sub_author']))
                {
                ?>
                    <link rel="stylesheet" type="text/css" href="/scripts/fancybox/dist/jquery.fancybox.css">
                    <script src="/scripts/fancybox/dist/jquery.fancybox.min.js"></script>

                    <script type="text/javascript">
                        $("[data-fancybox]").fancybox({

                        });
                    </script>


                <?
                    $kind_action=4; //������� ������� �� ���������� ������� � 4
                    while ($kind_action<=9) //�� 9 ���������
                    {
                        foreach ($MODULE_OUTPUT['sub_author'] as $key)
                        {
                            if ($key['kind_act'] == $kind_action)
                            {
                                echo "<div class='kind_action'>";
                                switch ($kind_action)
                                {
                                    case "4":echo "<h1>������� � ������-������������ ������������</h1>";
                                    break;

                                    case "5": echo "<h1>������� � ��������� ��������</h1>";
                                    break;

                                    case "6": echo "<h1>������� � ������ ��� �������� �� ���������� ���</h1>";
                                    break;

                                    case "7": echo "<h1>����������</h1>";
                                    break;

                                    case "8": echo "<h1>�������</h1>";
                                    break;

                                    case "9": echo "<h1>���������</h1>";
                                    break;
                                }
                                echo "</div>"; ?>
                                <table class="nsau_table employees izop" id="main_table" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="10%">��������</th>
                                            <th width="10%">����</th>
                                            <th width="10%">�����</th>
                                            <th width="10%">�������</th>
                                            <th width="3%" >����</th>
                                        </tr>
                                    </thead><?
                                    foreach ($MODULE_OUTPUT['sub_author'] as $folio) //������� ���������� ������� �������
                                    {
                                        if ($folio["kind_act"]==$kind_action)
                                        {
                                            ?>
                                            <tbody>
                                                <form name="nir_blank_folio" action="" method="post"><?
                                                echo "<tr>";
                                                echo "<td align='center'>".$folio["name"]."</td>";

                                                $data=$folio["date"];
                                                echo "<td align='center'>".$data."</td>";


                                                echo "<td align='center'>".$folio["author"]."</td>";
                                                echo "<td align='center'>".$folio["sub_author"]."</td>";

                                                $source_path="/files/folio/".$folio["file"]; //��������� ���� �� �����
                                                $path_parts = pathinfo($source_path); //������ ����������

                                                //����� ������������ �����, ���� �����������
                                                if ($path_parts['extension']=='jpeg' || $path_parts['extension']=='jpg' || $path_parts['extension']=='png' || $path_parts['extension']=='gif' || $path_parts['extension']=='bmp')
                                                {
                                                    if ($MODULE_OUTPUT['user_check']=='0') //��� �����
                                                    {
                                                        //echo "<td><img src=$source_path class='source_img' onclick='alert(\"����� �� �������� �������������������� �������������\");'/>"; 
                                                    }
                                                    else //��� �������������
                                                    {
                                                        echo "<td align='center'>
                                                        <a href=".$source_path.">�������</a>";
                                                    }
                                                }

                                                elseif ($path_parts['extension']=='pdf')
                                                {
                                                    if ($MODULE_OUTPUT['user_check']=='0') //��� �����
                                                    {
                                                        //echo "<td align='center'><img src='/themes/images/pdf_icon.png' class='source_doc' onclick='alert(\"����� �� �������� �������������������� �������������\");'/>"; 
                                                    }
                                                    else //��� �������������
                                                    {
                                                        echo "<td align='center'><a href=".$source_path.">�������</a></td>";
                                                    }
                                                }
                                                else
                                                {
                                                    echo "<td></td>";
                                                }
                                                echo "</tr>";
                                                echo "</form>";
                                                /*����� ������������ �������*/
                                        }
                                    }
                                    ?>
                                            </tbody>
                                </table>
                                <?
                                break 1;
                            }
                        }
                    $kind_action++;
                    }
                }
                else
                {
                    echo "<div class='nothing_message'>���������� �����������</div>";
                }
            }


?>



        </div>
    <?}
    break;




    case "people_menu_education" : {?>
    <?header("Content-type: text/html; charset=windows-1251");?>
<?//////////////////////////////////////////////////////////////////////////////////////////////?>
<?//����� - �����������/////////////////////////////////////////////////////////////////////////?>
<?//////// /////////////////////////////////////////////////////////////////////////////////////?>





                                <div class="Education">
                                    <?
                                    //CF::Debug($MODULE_OUTPUT["edu_levels"]);
                                    ?>
                                    <h2>�������� �� �����������</h2>
                                    <table border="1" cellpadding="4" cellspacing="0" width="100%" class="nsau_table">
                                        <thead class="edu">
                                            <th width="35%">��������������� ����������</th>
                                            <th>������� �����������</th>
                                            <th width="20%">��� ���������</th>
                                            <th width="35%">�������������</th>
                                            <th width="10%">������������</th>
                                        </thead>
                                        <?$education =  $MODULE_OUTPUT["education"];?>
                                        <?if(!empty($education)) 
                                        {?>
                                            <?foreach($education as $id=>$edu) {?>
                                                <tr>
                                                    <td><?=$edu["university"]?></td>
                                                    <td>
                                                        <?
                                                        foreach ($MODULE_OUTPUT["edu_levels"] as $key => $value) 
                                                        {
                                                           if ($key == $edu["edu_level"]) 
                                                           {
                                                               echo $value;
                                                           }
                                                        }
                                                        ?>
                                                    </td>
                                                    <td><?=$edu["year_end"]?></td>
                                                    <td><?=$edu["speciality"]?></td>
                                                    <td><?=$edu["qualification"]?></td>
                                                </tr>
                                            <?}?>
                                        <?}
                                        ?>
                                        <tr class="<?=(!empty($education)) ? " hidden" : ""?>">
                                            <td colspan="5">��� ����������</td>
                                        </tr>
                                    </table>
                                </div>

                                <?$addictional_education = $MODULE_OUTPUT["addictional_education"];?>
                                <div class="AddictionalEducation <?=(empty($addictional_education)) ? " hidden" : ""?>">
                                    <h2>�������� �� ������ �������� � �������</h2>
                                    <table border="1" cellpadding="4" cellspacing="0" width="100%" class="nsau_table">
                                        <thead class="add_edu">
                                            <th width="30%">�������� � ���������� ������ ������� ��� ������</th>
                                            <th width="30%">��������������� ����������</th>
                                            <th width="20%">���� ������ ���������</th>
                                            <th width="20%">���� ���������� ������ ������� ��� ������</th>
                                        </thead>
                                        <?if(!empty($addictional_education)) {?>
                                            <?foreach($addictional_education as $id=>$add_edu) {?>
                                                <tr>
                                                    <td><?=$add_edu["certificate"]?></td>
                                                    <td><?=$add_edu["university"]?></td>
                                                    <td><?=$add_edu["date_doc"]?></td>
                                                    <td><?=$add_edu["date_rank"] == "00.00.0000" ? "-" : $add_edu["date_rank"]?></td>
                                                </tr>
                                            <?}?>
                                        <?}?>
                                    </table>
                                </div>







<?//////////////////////////////////////////////////////////////////////////////////////////////?>
<?//////////////////////////////////////////////////////////////////////////////////////////////?>
<?//////////////////////////////////////////////////////////////////////////////////////////////?>
    <?}
    break;

    case "people_menu_prof_development": {?>
    <?header("Content-type: text/html; charset=windows-1251");?>
<?//////////////////////////////////////////////////////////////////////////////////////////////?>
<?//����� - ��������� ������������//////////////////////////////////////////////////////////////?>
<?//////// /////////////////////////////////////////////////////////////////////////////////////?>

    <?//��������� ������������?>
        <?if($man["people_cat"]!=1) {?>
            <div class="ProfDevelopment">
                <h2>�������� � ��������� ������������ � ���������������� �������������e</h2>
                <table border="1" cellpadding="4" cellspacing="0" width="70%" data-id="<?=$man["id"]?>" class="nsau_table" style="text-align: center;">
                    <thead>
                        <th>�������� ���������</th>
                        <th>��� � ��� ��������</th>
                        <th>����� ���������, �����</th>
                        <th>���� ������ ���������</th>
                        <th>���� ��������� ���������</th>
                        <th>����� ���</th>
                        <th>��� ���������</th>
                    </thead>
                    <?if(!empty($MODULE_OUTPUT["prof_development"])) {?>
                        <?foreach($MODULE_OUTPUT["prof_development"] as $id=>$prof) {?>
                            <tr itemprop="ProfDevelopment"  class="prof">
                                <td><?=$prof["course"]?></td>
                                <td><?=$prof["location"]?></td>
                                <td><?=$prof["hours"]?></td>
                                <td><?=$prof["date_begin"]?></td>
                                <td><?=$prof["date_end"]?></td>
                                <td><?=$prof["edu_type"]?></td>
                                <td><?=$prof["doc_type"]?></td>
                            </tr>
                        <?}?>
                    <?}?>
                    <tr class="empty_prof<?=(!empty($MODULE_OUTPUT["prof_development"])) ? " hidden" : ""?>">
                        <td colspan="8">��� ����������</td>
                    </tr>

                </table>
            </div>
        <?}?>
    <?//end?>

<?//////////////////////////////////////////////////////////////////////////////////////////////?>
<?//////////////////////////////////////////////////////////////////////////////////////////////?>
<?//////////////////////////////////////////////////////////////////////////////////////////////?>

    <?}
    break;



    case "people_menu_files": {?>
    <?//header("Content-type: text/html; charset=windows-1251");?>
<?//////////////////////////////////////////////////////////////////////////////////////////////?>
<?//����� - ����� ������������//////////////////////////////////////////////////////////////////?>
<?//////// /////////////////////////////////////////////////////////////////////////////////////?>
    <?//����� ������������?>
        <?/*CF::Debug($MODULE_OUTPUT["files_list"]);*/if(isset($MODULE_OUTPUT["files_list"]["subjects"]) || isset($MODULE_OUTPUT["files_list"]["unattached"]["folders"]) || isset($MODULE_OUTPUT["files_list"]["unattached"]["other"]["files"]) || isset($MODULE_OUTPUT["files_list"]["autors"])) {?>
            <br/>
            <div class="file_list_cont" >
                <script>jQuery(document).ready(function($) {FoldersList()});</script>
                <div class="section float-left" >
                    <h2 style="background: #FFFFFF;">����� ������������</h2>
                    
                        <span style="float: left;">�����������:</span>
                        
                        <ul class="tabs" style=" margin-top: -20px;">
                            <? $current = "current"; $visible = "visible"; ?>
                            <?if ((isset($MODULE_OUTPUT["files_list"]["subjects"]) || isset($MODULE_OUTPUT["files_list"]["unattached"]["other"]["files"])) || isset($MODULE_OUTPUT["files_list"]["autors"])) { ?>
                                <li class="current">�� ���������</li><? $current = ""; ?>
                            <?}?>
                            <?if(isset($MODULE_OUTPUT["files_list"]["unattached"]["folders"])) { ?>
                                <li class="<?=$current?>">�� ������</li>
                            <? } elseif( empty($MODULE_OUTPUT["files_list"]["unattached"]["other"]["files"]) && empty($MODULE_OUTPUT["files_list"]["autors"]) && empty($MODULE_OUTPUT["files_list"]["subjects"]))  $visible = "";  ?>
                            <?if(isset($MODULE_OUTPUT["files_list"]["unattached"]["students_works"])) { ?>
                                <li class="<?=$current?>">������ ���������</li>
                            <? } elseif( empty($MODULE_OUTPUT["files_list"]["unattached"]["other"]["files"]) && empty($MODULE_OUTPUT["files_list"]["autors"]) && empty($MODULE_OUTPUT["files_list"]["subjects"]))  $visible = "";  ?>
                        </ul>
                        <br \>
                    
                    
                    <div class="box <?=(!empty($MODULE_OUTPUT["files_list"]["subjects"])  || isset($MODULE_OUTPUT["files_list"]["unattached"]["other"]["files"]) || isset($MODULE_OUTPUT["files_list"]["autors"])) ? "visible" : ""?>">
                        <?if (!empty($MODULE_OUTPUT["files_list"]["subjects"])  || isset($MODULE_OUTPUT["files_list"]["unattached"]["other"]["files"]) || isset($MODULE_OUTPUT["files_list"]["autors"])) {
                            foreach ($MODULE_OUTPUT["files_list"]["subjects"] as $subject_id => $files) {  ?>
                                <div id="subject_id_<?=$subject_id?>" class = "folder_list">
                                    <p><?=$MODULE_OUTPUT["files_list"]["subjects"][$subject_id]["name"]?> <small>(<?=$MODULE_OUTPUT["files_list"]["subjects"][$subject_id]["department_name"]?>)</small></p>
                                </div>
                                <div id="subj_id_<?=$subject_id?>" style="display: none;" class = "slide_file_list">
                                    <? foreach ($MODULE_OUTPUT["files_list"]["subjects"][$subject_id]["files"] as $file) { ?>
                                        <div class="file_list_row" id="file_row_<?=$file["id"]?>">
                                        <div class="left_column">
                                            <?if(isset($file["sig_id"])) {?>
                                                <a class="sig" href="/file/sig/<?=$file["sig_id"]?>" title="������� ���"></a>
                                            <?} elseif($file["with_sig"] == 1) {?>
                                                <a class="sig" title="�������� ���"></a>
                                            <?}?>
                                            <a class="file_name" href="/file/<?=$file["id"]?>/"><?=$file["name"]?></a>  (�������: <?=$file["download_count"]?>)</div>
                                    </div>
                                    <!--��������!-->
                                    <?if(isset($MODULE_OUTPUT["files_list"]["autors"]["subjects"][$MODULE_OUTPUT["files_list"]["subjects"][$subject_id]["id"]]["files"])) {?>
                                            <? foreach ($MODULE_OUTPUT["files_list"]["autors"]["subjects"][$MODULE_OUTPUT["files_list"]["subjects"][$subject_id]["id"]]["files"] as $file) { ?>
                                                <div class="file_list_row" id="file_row_<?=$file["id"]?>">
                                                <div class="left_column">
                                                    <?if(isset($file["sig_id"])) {?>
                                                        <a class="sig" href="/file/sig/<?=$file["sig_id"]?>" title="������� ���"></a>
                                                    <?} elseif($file["with_sig"] == 1) {?>
                                                        <a class="sig" title="�������� ���"></a>
                                                    <?}?>
                                                    <a class="file_name" href="/file/<?=$file["id"]?>/"><?=$file["name"].".".$file["filename"]?></a>  (�������: <?=$file["downloads"]?>)</div>
                                                </div>
                                            <? } ?>
                                            <?unset($MODULE_OUTPUT["files_list"]["autors"]["subjects"][$MODULE_OUTPUT["files_list"]["subjects"][$subject_id]["id"]])?>
                                    <?}?>
                                    <!--END ��������!-->
                                    <? } ?>
                                </div>
                            <!--��������!-->
                            <?}?>
                            <?if(isset($MODULE_OUTPUT["files_list"]["autors"]["subjects"])) {?>
                                <? foreach ($MODULE_OUTPUT["files_list"]["autors"]["subjects"] as $sub) { $subject_id++;?>
                                    <div id="subject_id_<?=$subject_id?>" class = "folder_list">
                                        <p><?=$sub["name"]?><small> (<?=$sub["dep_name"]?>) </small></p>
                                    </div>
                                    <div id="subj_id_<?=$subject_id?>" style="display: none;" class = "slide_file_list">
                                        <? foreach ($sub["files"] as $file) { ?>
                                            <div class="file_list_row" id="file_row_<?=$file["id"]?>">
                                            <div class="left_column">
                                                <?if(isset($file["sig_id"])) {?>
                                                    <a class="sig" href="/file/sig/<?=$file["sig_id"]?>" title="������� ���"></a>
                                                <?} elseif($file["with_sig"] == 1) {?>
                                                    <a class="sig" title="�������� ���"></a>
                                                <?}?>
                                                <a class="file_name" href="/file/<?=$file["id"]?>/"><?=$file["name"].".".$file["filename"]?></a>  (�������: <?=$file["downloads"]?>)</div>
                                            </div>
                                        <? } ?>
                                    </div>
                                <?}?>
                            <?}?>
                            <!--END ��������!-->
                            <?if($MODULE_OUTPUT["files_list"]["unattached"]["other"]["files"]  ) {?>
                                <div class = "slide_file_list_other">
                            <?}?>
                            <?foreach ($MODULE_OUTPUT["files_list"]["unattached"]["other"]["files"] as $file) { ?>
                                <div class="file_list_row" id="file_row_<?=$file["id"]?>">
                                    <div class="left_column">
                                        <?if(isset($file["sig_id"])) {?>
                                            <a class="sig" href="/file/sig/<?=$file["sig_id"]?>" title="������� ���"></a>
                                        <?} elseif($file["with_sig"] == 1) {?>
                                            <a class="sig" title="�������� ���"></a>
                                        <?}?>
                                        <a class="file_name" href="/file/<?=$file["id"]?>/"><?=$file["name"]?></a> (�������: <?=$file["download_count"]?>)</div>
                                </div>
                            <?}?>
                            <!--��������!-->
                            <?if(isset($MODULE_OUTPUT["files_list"]["autors"]["other"])) {?>
                                    <? foreach ($MODULE_OUTPUT["files_list"]["autors"]["other"] as $file) { ?>
                                        <div class="file_list_row" id="file_row_<?=$file["id"]?>">
                                            <div class="left_column">
                                                <?if(isset($file["sig_id"])) {?>
                                                    <a class="sig" href="/file/sig/<?=$file["sig_id"]?>" title="������� ���"></a>
                                                <?} elseif($file["with_sig"] == 1) {?>
                                                    <a class="sig" title="�������� ���"></a>
                                                <?}?>
                                                <a class="file_name" href="/file/<?=$file["id"]?>/"><?=$file["name"].".".$file["filename"]?></a>  (�������: <?=$file["downloads"]?>)</div>
                                        </div>
                                    <? } ?>
                            <?}?>
                            <!--END ��������!-->
                            <?if($MODULE_OUTPUT["files_list"]["unattached"]["other"]["files"]){?>
                                </div>
                            <?}
                        }
                        else {?>
                            ����� �����������
                        <?}?>
                    </div>
                    <div class="box <?=(!empty($MODULE_OUTPUT["files_list"]["subjects"])  || isset($MODULE_OUTPUT["files_list"]["unattached"]["other"]["files"]) || isset($MODULE_OUTPUT["files_list"]["autors"])) ? "" : "visible"?>">
                        <?if(!empty($MODULE_OUTPUT["files_list"]["unattached"]["folders"])) {
                            foreach ($MODULE_OUTPUT["files_list"]["unattached"]["folders"] as $folder_id => $files) {  ?>
                                <div class = "folder_list" id="folder_id_<?=$folder_id?>">
                                    <p><?=$MODULE_OUTPUT["files_list"]["unattached"]["folders"][$folder_id]["name"]?></p>
                                </div>
                                <div id="subj_id_<?=$folder_id?>" style="display: none;" class = "slide_file_list">
                                    <?foreach ($MODULE_OUTPUT["files_list"]["unattached"]["folders"][$folder_id]["files"] as $file) { ?>
                                        <div class="file_list_row" id="file_row_<?=$file["id"]?>">
                                            <div class="left_column">
                                                <?if(isset($file["sig_id"])) {?>
                                                    <a class="sig" href="/file/sig/<?=$file["sig_id"]?>" title="������� ���"></a>
                                                <?} elseif($file["with_sig"] == 1) {?>
                                                    <a class="sig" title="�������� ���"></a>
                                                <?}?>
                                                <a class="file_name" href="/file/<?=$file["id"]?>/"><?=$file["name"]?></a> (�������: <?=$file["download_count"]?>)</div>
                                        </div>
                                    <?}?>
                                </div>
                            <?}
                        }
                        else echo "����� �����������";?>
                    </div>
                    <div class="box <?=(!empty($MODULE_OUTPUT["files_list"]["subjects"])  || isset($MODULE_OUTPUT["files_list"]["unattached"]["other"]["files"]) || isset($MODULE_OUTPUT["files_list"]["autors"]) || isset($MODULE_OUTPUT["files_list"]["unattached"]["folders"])) ? "" : "visible"?>">
                        <?if(!empty($MODULE_OUTPUT["files_list"]["unattached"]["students_works"]["files"])) {
                            foreach ($MODULE_OUTPUT["files_list"]["unattached"]["students_works"]["files"] as $file_id => $file) {  ?>
                                <div class="file_list_row" id="file_row_<?=$file_id?>">
                                    <div class="left_column">
                                        <?if(isset($file["sig_id"])) {?>
                                            <a class="sig" href="/file/sig/<?=$file["sig_id"]?>" title="������� ���"></a>
                                        <?} elseif($file["with_sig"] == 1) {?>
                                            <a class="sig" title="�������� ���"></a>
                                        <?}?>
                                        <a class="file_name" href="/file/<?=$file["id"]?>/"><?=$file["name"]?></a> (�������: <?=$file["download_count"]?>)</div>
                                </div>
                            <?}
                        }
                        else echo "����� �����������";?>
                    </div>
                </div>
            </div>
        <?}
        else {?>
            <p>��� ����������� ������.</p>
        <?}?>
    <?//end?>

<?//////////////////////////////////////////////////////////////////////////////////////////////?>
<?//////////////////////////////////////////////////////////////////////////////////////////////?>
<?//////////////////////////////////////////////////////////////////////////////////////////////?>

<?
    }
    break;









    case "delete_people": {
        $name = $MODULE_DATA["output"]["person_name"];
        $people_id = $MODULE_DATA["output"]["people_id"];
        if (isset($MODULE_DATA["output"]["is_user"])) { ?>
        ��� ������������ <?=$name?> ���������� ������� ������. ��������:<br />
        <a href="?confirm=1">�������, �������� ������� ������</a><br />
        <a href="?confirm=2">������� ������ � ������� �������</a><br />
        <a href="/people/<?=$people_id?>/">�� �������</a>
<?php   } else { ?>
        �� �������, ��� ������ ������� ������ � �������� <?=$name?>?<br />
        <a href="?confirm=1">��</a><br />
        <a href="/people/<?=$people_id?>/">���</a>
<?php   }
    }
    break;
    

    case "edit_people": 
    {
        if ($MODULE_OUTPUT["allow_peopledit"]) { ?>
        <script>jQuery(document).ready(function($) {GlobalPeople();AjaxEditPhoto(<?=$MODULE_OUTPUT["module_id"]?>);});</script>
<?php   }
        if(isset($MODULE_OUTPUT["display_variant"]["add_item"])) {
            $display_variant = $MODULE_OUTPUT["display_variant"]["add_item"];
        } else {

            $display_variant = $MODULE_DATA["output"]["editperson"];
            $display_variant["education"] = $MODULE_OUTPUT["education"];
        }
        $submode = $MODULE_DATA["output"]["submode"];
        $man = $MODULE_DATA["output"]["editperson"]; ?>



        <input type="hidden" value="<?=$man["id"]?>" class="h_people_id"/>
        <form id="people_form" class="people_cat_<?=$man["people_cat"]?>" method="post" enctype="multipart/form-data" onsubmit="if(document.getElementById('add_pass').value != document.getElementById('repeatpass').value) {alert('������ ���������� �������'); return false;}">
<?php   if($man["people_cat"] == 2) { ?>
            <h2>�������������� ���������� � ������������� <?=$man["last_name"]?> <?=(isset($man["name"]) ? $man["name"] : "")?> <?=(isset($man["patronymic"]) ? $man["patronymic"] : "")?></h2>
<?php   } else { ?>
            <h2>������������� ���������� � ������������</h2>
<?php   } ?>
            <fieldset>
                <legend class="form_title">����� ����������</legend>
<?php   if($Auth->usergroup_id==1) { ?>
                <div class="wide">
<?php       if(!isset($submode)) { ?>
                    <div class="form-notes">
                        <dl>
                            <dt><label>��������� ������������:</label></dt>
                            <dd>
                                <select id="people_cat_select" class="change_people_cat" size="1">
                                    <option value="1"<?=((!isset($display_variant) || (isset($display_variant) && $display_variant["people_cat"] == 1)) ? " selected='selected'" : "")?>>�������</option>
                                    <option value="2"<?=(isset($display_variant) && $display_variant["people_cat"] == 2 ? " selected='selected'" : "")?>>�������������</option>
                                    <option value="3"<?=(isset($display_variant) && $display_variant["people_cat"] == 3 ? " selected='selected'" : "")?>>��������� ������� ������</option>
                                </select>
                            </dd>
                        </dl>
                    </div>
<?php       } ?>
                </div>
<?php   }?>
                <div class="wide">
                    <div class="form-notes">
                        <dl>
                            <?if($Auth->usergroup_id!=3) {?>
                                <dt><label>�������:<span class="obligatorily">*</span></label></dt>
                                <dd><input name="last_name" type="text" class="input_long" value="<?=$man["last_name"]?>" /></dd>
                                <dt><label>���:<span class="obligatorily">*</span></label></dt>
                                <dd><input name="first_name" type="text" class="input_long" value="<?=$man["name"]?>" /></dd>
                                <dt><label>��������:<span class="obligatorily">*</span></label></dt>
                                <dd><input name="patronymic" type="text" class="input_long" value="<?=$man["patronymic"]?>" /></dd>
                            <?}?>
                        </dl>
                    </div>
                    <div class="form-notes">
                        <dl>
                            <dt><label>����������:</label><small><br/>(�������� ���� �������� �� ����� 2,00 ��, � 1024�1024 ��������.)</small></dt>
                            <span style="color: red;" id="max_size"></span>
                            <dd>
                                <div class="user_photo float-right">
<?php   if(isset($man["photo"]) && $man["photo"]) { ?>
                                    <em class="hidden"></em>
                                    <a href="/images/people/<?=$man["original_photo"]?>" target="_blank" title="������������� ����������">
                                        <img src="/images/people/<?=$man["photo"]?>" alt="" />
                                    </a>
<?php   } else { ?>
                                    <img src="/images/people/no_photo.jpg" alt="" />
<?php   } ?>
                                </div>
                                <span id="photoinput"><input name="user_photo" type="file" id="user_photo" /></span>
                                <input name="delete_photo" onclick="if(this.checked) document.getElementById('photoinput').className='hidden'; else document.getElementById('photoinput').className='';" type="checkbox"> ������� ����������
                            </dd>
                            <dt><label>���:</label></dt>
                            <dd><input type="radio" name="male" value="1"<?=($man["male"] ? " checked=\"checked\"" : "")?> /> �/� <input type="radio" name="male" value="0"<?=($man["male"] ? "" : " checked=\"checked\"")?> /></dd>
<?if($Auth->usergroup_id!=3) {?>
                            <?php   if($man["people_cat"] != 2) { ?>
                            <dt><label>������:</label></dt>
                            <dd>
                                <select name="status" size="1">
                                    <!--option value="0"></option-->
<?php       foreach ($MODULE_DATA["output"]["statuses"] as $st) {
                if($st["people_cat"] == $man["people_cat"]) { ?>
                                    <option value="<?=$st["id"]?>"<?=($man["status_id"]==$st["id"] ? " selected='selected'" : "")?> class="people_cat_<?=$st["people_cat"]?>"><?=$st["name"]?></option>
<?php           }
            } ?>
                                </select>
                            </dd>
<?php   } ?>
<?} else {?><br><br><br><br><br><?}?>
                        </dl>
                    </div>
                </div>
                <div class="wide">
                    <?if($man["people_cat"]!=1) {?>
                        <dl>

                            <dt><label>�������(�������):</label></dt>
                            <dd><input type="text" id="phone" name="phone" value="<?=(!empty($display_variant["phone"])) ? $display_variant["phone"] : $man["phone"]?>"></dd>
                        </dl>
                        <dl>
                            <dt><label>E-mail:</label></dt>
                            <dd><input type="text" name="email_info" value="<?=(!empty($display_variant["email_info"])) ? $display_variant["email_info"] : $man["email_info"]?>"></dd>
                        </dl>
                        <dl>
                            <dt><label>������������:</label></dt>
                            <dd>
                                ������ (�����������������):
                                <select style="width:10%" name="location_building">
                                    <option value=""></option>
                                    <?foreach($MODULE_OUTPUT["buildings"] as $id => $val) {?>
                                        <option <?=((!empty($display_variant["location_building"]) ? $display_variant["location_building"] : $man["location_building"])==$id) ? "selected" : ""?> value="<?=$id?>"><?=$val["name"]?> (<?=$val["label"]?>)</option>
                                    <?}?>
                                </select>

                            �������:
                            <input type="text" name="location" value="<?=(!empty($display_variant["location"])) ? $display_variant["location"] : $man["location"]?>">

                            </dd>
                        </dl>
                        <?if($man["people_cat"]!=2) {?>
                            <dl>
                                <dt><label>���������:<span class="obligatorily">*</span></label></dt>
                                <dd><input type="text" name="post" value="<?=(!empty($display_variant["post"])) ? $display_variant["post"] : $man["post"]?>"></dd>
                            </dl>
                        <?}?>
                        <dl>
                            <dt><label>�����������:<small>
                            <!-- (��� ���������� �� ����� ����� ���������� ��������� � ��������������� ���� �����) -->
                        </small></label></dt>
                            <dd><textarea name="comment" class="wysiwyg" cols="40" rows="7" wrap="virtual"><?=$man["comment"]?></textarea></dd>
                        </dl>
                    <?}?>
                </div>
            </fieldset>


            <?

            //��������� ������������ ��� ������������ ����� �������������
            if ($Engine->OperationAllowed(0, "qual.up", $row->id, $man['usergroup']))
            {
                ?>
                    <div class="ProfDevelopment wide">
                        <dl>
                            <dt><label>�������� � ��������� ������������ � ���������������� �������������e</label>
                                </dt></dl>

                        <table border="1" cellpadding="4" cellspacing="0" width="100%" data-id="<?=$man["id"]?>" class="nsau_table">
                            <thead class="prof_dev">
                                <th>�������� ���������</th>
                                <th>��� � ��� ��������</th>
                                <th>����� ���������, �����</th>
                                <th>���� ������ ���������</th>
                                <th>���� ��������� ���������</th>
                                <th>����� ���</th>
                                <th width="15%">��� ���������</th>
                                <th width="10%"></th>
                            </thead>
                            <?if(!empty($MODULE_OUTPUT["prof_development"])) {?>
                                <?foreach($MODULE_OUTPUT["prof_development"] as $id=>$prof) {?>
                                    <tr data-id="<?=$id?>" class="prof">
                                        <td class="course input"><?=$prof["course"]?></td>
                                        <td class="location input"><?=$prof["location"]?></td>
                                        <td class="hours input"><?=$prof["hours"]?></td>
                                        <td class="date_begin input date"><?=$prof["date_begin"]?></td>
                                        <td class="date_end input date"><?=$prof["date_end"]?></td>
                                        <td class="edu_type select"><?=$prof["edu_type"]?></td>
                                        <td class="doc_type select"><?=$prof["doc_type"]?></td>
                                        <td>
                                            <input type="submit" class="delete_prof_development" value="�������">
                                        </td>
                                    </tr>
                                <?}?>
                            <?}?>
                            <tr class="empty_prof<?=(!empty($MODULE_OUTPUT["prof_development"])) ? " hidden" : ""?>">
                                <td colspan="8">��� ����������</td>
                            </tr>

                            <tr>
                                <td><input type="text" class="fix_table" id="course"></td>
                                <td><input type="text" class="fix_table" id="location"></td>
                                <td><input type="text" class="fix_table" id="hours"></td>
                                <td><input type="text" class="fix_table" id="date_begin"></td>
                                <td><input type="text" class="fix_table" id="date_end"></td>
                                <td>
                                    <select id="edu_type" class="fix_table">
                                        <option value="1">��������� ������������</option>
                                        <option value="2">���������������� ��������������</option>
                                        <option value="3">����������</option>
                                    </select>
                                </td>
                                <td>
                                    <select id="doc_type" class="fix_table">
                                        <option value="1">������ (�� ����� 250 �����)</option>
                                        <option value="2">������������� (�� 16 �� 250 �����)</option>
                                        <option value="3">���������� (�� 4 �� 16 �����)</option>

                                    </select>
                                </td>
                                <td>
                                    <input type="submit" value="��������" id="add_prof_development">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="8">
                                ��� �������������� ������ ���������� ������� ������� ����� ���� �������� ����������� �������.����� ��������� ��������� ����� ������ ������ ENTER.
                                </td>
                            </tr>
                            <tr data-id="" class="ptmp hidden">
                                <td class="pcourse"></td>
                                <td class="plocation"></td>
                                <td class="phours"></td>
                                <td class="pdate_begin"></td>
                                <td class="pdate_end"></td>
                                <td class="pedu_type"></td>
                                <td class="pdoc_type"></td>
                                <td>
                                    <input type="submit" value="�������" class="delete_prof_development">
                                </td>
                            </tr>
                        </table>
                    </div>
                <?
            }

            ?>
<?php   if($man["people_cat"] == 2 && (!isset($submode) || $submode == 2)) { ?>
            <fieldset id="teacher_subform">
                <legend  class="form_title">������ �������������</legend>
                <div class="wide">
                    <table class="nsau_table">
                       <thead>
                           <tr>
                               <th>���������</th>
                               <th>�������</th>
                               <th>�������</th>
                           </tr>
                       </thead>
                       <tbody>
                            <?if(!empty($display_variant['department_post'])) {$man['department_post'] = $display_variant['department_post'];?>
                                <?foreach ($man['department_post'] as $d_id => $posts) {?>
                                    <?foreach ($posts as $p_id) {?>
                                       <tr class = 'post_row'>
                                           <td>
                                                <select name="new_post_itemprop[]" class="select_long">
                                                    <?foreach($MODULE_OUTPUT["teacher"]["post"] as $id => $val) {?>
                                                        <option <?=(($p_id==$id) ? "selected" : "")?> value="<?=$id?>"><?=$val["name"]?></option>
                                                    <?}?>
                                                </select>
                                            </td>
                                           <td>
                                                <select name="new_department_itemprop[]" class="select_long">
                                                    <?foreach($MODULE_OUTPUT['departments'] as $id => $val) {?>
                                                        <option <?=(($d_id==$id) ? "selected" : "")?> value="<?=$id?>"><?=$val?></option>
                                                    <?}?>
                                                </select>
                                           </td>
                                           <td class="del_department_post" style='cursor: pointer'>X</td>
                                       </tr>
                                    <?}?>
                                <?}?>
                            <?} else {?>
                                <tr class = 'post_row'>
                                   <td>
                                        <select name="new_post_itemprop[]" class="select_long">
                                             <option value="0">---</option>
                                            <?foreach($MODULE_OUTPUT["teacher"]["post"] as $id => $val) {?>
                                                <option <?=(($p_id==$id) ? "selected" : "")?> value="<?=$id?>"><?=$val["name"]?></option>
                                            <?}?>
                                        </select>
                                    </td>
                                   <td>
                                        <select name="new_department_itemprop[]" class="select_long">
                                            <option value="0">---</option>
                                            <?foreach($MODULE_OUTPUT['departments'] as $id => $val) {?>
                                                <option <?=(($d_id==$id) ? "selected" : "")?> value="<?=$id?>"><?=$val?></option>
                                            <?}?>
                                        </select>
                                   </td>
                                   <td class="del_department_post" style='cursor: pointer'>X</td>
                               </tr>
                            <?}?>
                            <tr>
                               <td colspan='3' style='cursor: pointer' class='add_department_post'>�������� ���������</td>
                            </tr>
                       </tbody>
                    </table>
                    <br>
                    <div class="form-notes">
                        <dl>
<!--                             <dt><label style="color: #57C512">���������, �������, ������: <small>(��� ���������� �� ����� ����� ���������� ��������� � ��������������� ���� �����)</small></label></dt>
                            <dd><input type="text" name="prad" size="35" value="<?=(isset($man["prad"]) ? $man["prad"] : "")?>" /></dd> -->
                            <dt><label>�������:</label></dt>
                            <dd>
                                <select name="degree[]" multiple class="select_long">
                                    <option value="0"></option>
                                    <?foreach($MODULE_OUTPUT["teacher"]["degree"] as $id => $val) {?>
                                        <option <?=(in_array($id, (!empty($display_variant["degree"]) ? $display_variant["degree"] : $man["degree"])) ? "selected" : "")?> value="<?=$id?>"><?=$val["name"]?></option>
                                    <?}?>
                                </select>
                            </dd>
                            <dt><label>������:</label></dt>
                            <dd>
                                <select name="academ_stat[]" multiple class="select_long">
                                    <option value="0"></option>
                                    <?foreach($MODULE_OUTPUT["teacher"]["rank"] as $id => $val) {?>
                                        <option <?=(in_array($id, (!empty($display_variant["academ_stat"]) ? $display_variant["academ_stat"] : $man["academ_stat"])) ? "selected" : "")?> value="<?=$id?>"><?=$val["name"]?></option>
                                    <?}?>
                                </select>

                            </dd>

                                <dt><label class="<?=$Auth->usergroup_id!=1 ? "hidden" : ""?>">���������:</label></dt>
                                <dd class="<?=$Auth->usergroup_id!=1 ? "hidden" : ""?>">
                                    <select name="post" size="1" class="select_long">
                                        <option value="����������"<?=($man['post'] == '����������' ? ' selected="selected"' : '')?>>����������</option>
                                        <option value="�������������"<?=($man['post'] == '�������������' ? ' selected="selected"' : '')?>>�������������</option>
                                        <option value="���������� ������������"<?=($man['post'] == '���������� ������������' ? ' selected="selected"' : '')?>>���������� ������������</option>
                                        <option value="��������"<?=($man['post'] == '��������' ? ' selected="selected"' : '')?>>��������, ������� ��������, ����, ��������</option>
                                    </select>
                                </dd>
                        </dl>
                    </div>
                    <div class="form-notes">
                        <dl>
                            <dt><label>��� ������ ������ �����:<span class="obligatorily">*</span></label></dt>
                            <dd><input type="text" name="exp_full" value="<?=(isset($display_variant["exp_full"]) ? $display_variant["exp_full"] : $man["exp_full"])?>" /></dd>
                            <dt><label>��� ������ ���������������, ������-��������������� �����:</label></dt>
                            <dd><input type="text" name="exp_teach" value="<?=(isset($display_variant["exp_teach"]) ? $display_variant["exp_teach"] : $man["exp_teach"])?>" /></dd>
                        </dl>
                    </div>
                </div>





                <div class="wide">
                    <div class="form-notes">
<?php       if(1) { ?>
                        <dl>
                            <dt><label>�������� �������:<span class="obligatorily">*</span></label></dt>
                            <dd>
                                <ul id="department_select" class="select_long">
<?php           foreach($MODULE_OUTPUT["faculties"] as $faculties) { ?>
                                    <li>
                                        <dl>
                                            <dt><?=$faculties["fac_name"]?></dt>
<?php               foreach($faculties["departments"] as $department) { ?>
                                            <dd id="depid_<?=$department["id"]?>"<?=(isset($man["department"][$department["id"]]) ? " class='hidden'" : "")?> title="<?=$department["name"]?>"><?=$department["name"]?></dd>
<?php               } ?>
                                        </dl>
                                    </li>
<?php           } ?>
                                </ul>
                            </dd>
                        </dl>
<?php       } ?>
                    </div>
                <div class="form-notes">

                        <dl>
                            <dt><label>�������� ����������:<span class="obligatorily">*</span></dt>
                            <dd>
                                <ul id="subject_select" class="select_long" >



                            <?=empty($MODULE_OUTPUT["departments_subj"]) ? "<li><dl><dt id='als_dep_name'></dt>" : ""?>
<?php           foreach($MODULE_OUTPUT["departments_subj"] as $dep_name => $subjects ) { ?>
                                    <li>
                                        <dl>
                                            <dt id="als_dep_name"></dt>

                                            <dt><?=$dep_name?></dt>
<?php               foreach($subjects as $subj) { ?>
                                            <dd id="subid_<?=$subj["id"]."_".$MODULE_OUTPUT["departments_subj"][$dep_name][0]["department_id"]?>" <?=isset($man["subjects"][$MODULE_OUTPUT["departments_subj"][$dep_name][0]["department_id"]][$subj["id"]]) ? " class='hidden'" : ""?> title="<?=$subj["name"]?>"><?=$subj["name"]?></dd>
<?php               } ?>
                                        </dl>
                                    </li>
<?php           } ?>
<?=empty($MODULE_OUTPUT["departments_subj"]) ? "</dl></li>" : ""?>
                                </ul>
                            </dd>
                        </dl>
                </div>
            </div>



                <div class="wide">
                    <div class="form-notes">
<?php       if(1) { ?>
                        <dl>
                            <dt><label>������������� ����� �������� �� �������:</label></dt>
                            <dd>
                                <ul id="department_list">

                                    <?$departments = isset($display_variant["department"]) ? $display_variant["department"] : $man["department"];?>
                                    <?$subjects = isset($display_variant["subject"]) ? $display_variant["subject"] : $man["subjects"];?>
                                    <?foreach($departments as $dep_id => $dep) { ?>
                                        <li><b><?=$dep?></b><input type="hidden" value="<?=$dep_id?>" name="department[<?=$dep_id?>]" /><em class="inline-block"></em>
                                        <ul id="subject_list_<?=$dep_id?>">

                                            <?if (!empty($subjects[$dep_id])) { ?>

                                                <?foreach ($subjects[$dep_id] as $subj_id => $subj_name) { ?>


                                                    <li><?=$subj_name?><input type="hidden" value="<?=$subj_id?>" name="subject[<?=$dep_id?>][<?=$subj_id?>]" /><em class="inline-block"></em> </li>




                                                <?}?>
                                            <?}?>
                                        </ul>


                                    <?}?>
                                </ul>
                            </dd>
                        </dl>
<?php       } else { ?>
                        <dl>
                            <dt><label>������������� ����� �������� �� �������:</label></dt>
                            <dd>
                                <ul>
<?php           foreach($MODULE_OUTPUT["current_department"] as $dep_id => $dep) { ?>
                                    <li><?=$dep?><input type="hidden" value="<?=$dep_id?>" name="department[<?=$dep_id?>]" /><em class="inline-block"></em></li>
<?php           } ?>
                                </ul>
                            </dd>
                        </dl>
<?php       } ?>
                    </div>

                </div>




<? /*
<div class="wide">
<pre>

/*foreach ($_POST["subject"] as $subj => $subj2) {
echo $subj."--".$subj2."<br/>";
foreach($subj2 as $key => $val)
echo "[".$val."]";
}


//print_r($man["subjects"]);

<div id="content12">-1-</div>
</pre>
</div>              <?=((isset($display_variant["is_curator"]) ? $display_variant["is_curator"] : $man["is_curator"]) ? "none;" : "block;") ?>
*/ ?>





                <?$man["curator_groups"] = isset($display_variant["curator_groups"]) ? $display_variant["curator_groups"] : $man["curator_groups"];?>
                <div class="wide">
                    <div class="form-notes">
                        <dl>
                            <dt><label>����������� ������:</label><input name="curator_selector" id="curator_selector" type="checkbox" <?=((isset($display_variant["is_curator"]) ? $display_variant["is_curator"] : $man["is_curator"]) ? "checked='checked'" : "") ?> /></dt>
                        </dl>


                        <div class="form-notes" id="curator_groups_cont" style="width: 200%; display: <?=((isset($display_variant["is_curator"]) ? $display_variant["is_curator"] : $man["is_curator"]) ? "block;" : "none;") ?>">
                            <dl>
                                <dt><label>������ ��������:</label></dt>
                                <dd>
                                    <select id="curator_groups" size="8" multiple="true" >
                                        <options>
                                            <?php   foreach($man["department"] as $dep_id => $dep) { ?>
                                            <optgroup id="<?=$dep_id?>" label="<?=$dep?>">
                                                <?php if (isset($man["curator_groups"]) && !empty($man["curator_groups"][$dep_id]))
                                                        foreach ($man["curator_groups"][$dep_id] as $group) { ?>
                                                    <option value="<?=$group['id']?>"><?=$group['name']?></option>
                                                    <? }
                                                    else { ?>
                                                    <option></option>
                                                <?php } ?>
                                            </optgroup>
                                            <?php }  ?>
                                        </options>
                                    </select>
                                    <div id="curator_groups_selector" > >> </div>
                                    <?php   if (isset($man["curator_groups"]))
                                        foreach($man["department"] as $dep_id => $dep)
                                            foreach ($man["curator_groups"][$dep_id] as $group) { ?>
                                                <input type="hidden" name="curator_groups[<?=$dep_id?>][<?=$group['id']?>]" value="<?=$group['id']?>" />
                                    <?php }?>
                                </dd>
                            </dl>
                            <dl>
                                <dt><label>������ ����� <small>(���������� ������� ������ � ������ �� ������� <<)</small>:</label></dt>
                                <dd>
                                    <div id="all_groups_selector" style="display: inline-block; vertical-align: top; font-weight:bold; cursor: pointer; text-decoration: underline;"> << </div>
                                    <select id="all_groups" size="16" multiple="true" name="curator_groups[]" style="width: 200px;">
                                        <options>
                                            <?php foreach ($man["all_groups"] as $group) { ?>
                                                <option class="fix_table" value="<?=$group['id']?>"><?=$group['name']?></option>
                                            <? } ?>
                                        </options>
                                    </select>
                                </dd>
                            </dl>
                        </div>
                    </div>
                </div>


                <br>
                    <div class="Education wide">
                        <div class="panel panel-nsau">
                            <div class="panel-heading">
                                �������� �� �����������<span class="obligatorily">*</span>
                            </div>
                            <div class="panel-body">
                                <table data-id="<?=$man["id"]?>" class="table table-striped">
                                    <thead class="edu">
                                        <th width="9%">��������������� ����������</th>
                                        <th width="19%">������� �����������</th>
                                        <th width="19%">��� ���������</th>
                                        <th width="19%">�������������</th>
                                        <th width="19%">������������</th>
                                        <th width="5%"></th>
                                    </thead>

                                    <?$education = (!empty($display_variant["education"])) ? $display_variant["education"] : $MODULE_OUTPUT["education"];?>
                                    <?if(!empty($education)) 
                                    {
                                        ?>
                                        <?foreach($education as $id=>$edu) {?>
                                            <tr data-id="<?=$id?>" class="edu">
                                                <td>
                                                    <div onclick="$(this).children('input').removeClass('hidden');$(this).children('span').hide();"> 
                                                        <span><?=$edu["university"]?></span>
                                                        <input class="fix_input form-control hidden" id='<?="_".$edu["university"]?>'' type="text" name="e_university[]" value='<?=$edu["university"]?>' size="13">
                                                    </div>
                                                </td>
                                                <td>
                                                    <!-- <input class="fix_input" type="text" name="e_edu_level[]" hidden value="<?=$edu["edu_level"]?>"> -->
                                                    <div onclick="$(this).children('select').removeClass('hidden');$(this).children('span').hide();"> 
                                                        <select name="e_edu_level[]" id="edu_level" class="form-control hidden" style="margin: 0px;">  
                                                            <option value="0"></option>
                                                           <?
                                                           foreach ($MODULE_OUTPUT['edu_levels'] as $key => $value) 
                                                           {

                                                                if ($key == $edu["edu_level"]) 
                                                                {
                                                                    $edu_level_value=$value;
                                                                    ?><option selected value=<?=$key?>><?=$value?></option><?
                                                                }
                                                                else
                                                                {
                                                                    ?><option value=<?=$key?>><?=$value?></option><?
                                                                }  
                                                           }
                                                           ?>                      
                                                        </select>
                                                        <span><h5><?=!empty($edu_level_value) ? $edu_level_value : "������� ����������� �� ������ (������� ��� �� �������)"   ?></h5></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div onclick="$(this).children('input').removeClass('hidden');$(this).children('span').hide();"> 
                                                        <span><?=$edu["year_end"]?></span>
                                                        <input class="edu_year fix_input form-control hidden" type="text" name="e_year_end[]" value="<?=$edu["year_end"]?>" size="4">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div onclick="$(this).children('input').removeClass('hidden');$(this).children('span').hide();"> 
                                                        <span><?=$edu["speciality"]?></span>
                                                        <input class="fix_input form-control hidden" type="text" name="e_speciality[]" value="<?=$edu["speciality"]?>" size="12">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div onclick="$(this).children('input').removeClass('hidden');$(this).children('span').hide();"> 
                                                        <span><?=$edu["qualification"]?></span>
                                                        <input class="fix_input form-control hidden" type="text" name="e_qualification[]" value="<?=$edu["qualification"]?>" size="12">
                                                    </div>
                                                </td>
                                                <td>
                                                    <input class="delete_education btn btn-nsau btn-sm" value="�������" size="7">
                                                </td>
                                            </tr>
                                        <?}?>
                                        <?
                                    }
                                    ?>
                                    <tr class="empty_edu<?=(!empty($education)) ? " hidden" : ""?>">
                                        <td colspan="5">��� ����������</td>
                                    </tr>
                                    <tr>
                                        <td><input type="text" class="fix_table form-control" id="university"></td>

                                        <td>
                                            <select id="edu_level" class="form-control" style="margin: 0px;">
                                               <?
                                               foreach ($MODULE_OUTPUT['edu_levels'] as $key => $value) 
                                               {
                                                   ?><option value=<?=$key?>><?=$value?></option><?
                                               }
                                               ?>                           
                                            </select>
                                        </td>

                                        <td><input type="text" class="fix_table edu_year form-control" id="year_end"></td>
                                        <td><input type="text" class="fix_table form-control" id="speciality"></td>
                                        <td><input type="text" class="fix_table form-control" id="qualification"></td>
                                        <td>
                                            <input value="��������" id="add_education" class="btn btn-nsau btn-sm" size="7">
                                        </td>
                                    </tr>
                                    <tr data-id="" class="etmp hidden">
                                        <td class="puniversity"><input type="text" class="form-control" name="e_university[]" size="13"></td>
                                        <td class="pedu_level">
                                            <select name="e_edu_level[]" class="form-control" style="margin: 0px;">
                                               <?
                                               foreach ($MODULE_OUTPUT['edu_levels'] as $key => $value) 
                                               {
                                                   ?><option value=<?=$key?>><?=$value?></option><?
                                               }
                                               ?>                              
                                            </select>
                                        </td>
                                        <td class="pyear_end"><input type="text" class="edu_year form-control" name="e_year_end[]" size="4"></td>
                                        <td class="pspeciality"><input type="text" class="form-control" name="e_speciality[]" size="12"></td>
                                        <td class="pqualification"><input type="text" class="form-control" name="e_qualification[]" size="12"></td>
                                        <td>
                                            <input type="submit" value="�������" class="delete_education">
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                    </div>

                    <div class="ProfDevelopment wide">
                        <div class="panel panel-nsau">
                            <div class="panel-heading">
                                �������� � ��������� ������������ � ���������������� �������������e
                            </div>
                            <div class="panel-body">
                                <table data-id="<?=$man["id"]?>" class="table table-striped" style="table-layout: fixed; word-wrap: break-word;">
                                    <thead class="prof_dev">
                                        <th>�������� ���������</th>
                                        <th>��� � ��� ��������</th>
                                        <th>����� ���������, �����</th>
                                        <th>���� ������ ���������</th>
                                        <th>���� ��������� ���������</th>
                                        <th>����� ���</th>
                                        <th>��� ���������</th>
                                        <th></th>
                                    </thead>
                                    <tbody>
                                        <tr id="prof_dev_body" hidden></tr>
                                        <?if(!empty($MODULE_OUTPUT["prof_development"])) {

                                            ?>
                                            <?foreach($MODULE_OUTPUT["prof_development"] as $id=>$prof) {?>
                                                <tr data-id="<?=$id?>" class="prof">
                                                    <td class="course input" style="width:10%"><?=$prof["course"]?></td>
                                                    <td class="location input"><?=$prof["location"]?></td>
                                                    <td class="hours input"><?=$prof["hours"]?></td>
                                                    <td class="date_begin input date"><?=$prof["date_begin"]?></td>
                                                    <td class="date_end input date"><?=$prof["date_end"]?></td>
                                                    <td class="edu_type select"><?=$prof["edu_type"]?></td>
                                                    <td class="doc_type select"><?=$prof["doc_type"]?></td>
                                                    <td>
                                                        <input class="delete_prof_development btn btn-nsau btn-sm" value="�������" style="width: 85%!important;">
                                                    </td>
                                                </tr>
                                            <?}?>
                                        <?}?>
                                        <tr class="empty_prof<?=(!empty($MODULE_OUTPUT["prof_development"])) ? " hidden" : ""?>">
                                            <td colspan="8">��� ����������</td>
                                        </tr>

                                        <tr>
                                            <td><input type="text" class="fix_table form-control" id="course" ></td>
                                            <td><input type="text" class="fix_table form-control" id="location" ></td>
                                            <td><input type="text" class="fix_table form-control" id="hours"></td>
                                            <td><input type="text" class="fix_table form-control" id="date_begin"></td>
                                            <td><input type="text" class="fix_table form-control" id="date_end"></td>
                                            <td>
                                                <select id="edu_type" class="fix_table form-control" style="margin:0px;">
                                                    <option value="1">��������� ������������</option>
                                                    <option value="2">���������������� ��������������</option>
                                                    <option value="3">����������</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="doc_type" class="fix_table form-control" style="margin:0px;">
                                                    <option value="1">������ (�� ����� 250 �����)</option>
                                                    <option value="2">������������� (�� 16 �� 250 �����)</option>
                                                    <option value="3">���������� (�� 4 �� 16 �����)</option>

                                                </select>
                                            </td>
                                            <td>
                                                <input value="��������" id="add_prof_development" class="btn btn-nsau btn-sm" style="width: 85%!important;">
                                            </td>
                                        </tr>
                                        <tr data-id="" class="ptmp hidden">
                                            <td class="pcourse"></td>
                                            <td class="plocation"></td>
                                            <td class="phours"></td>
                                            <td class="pdate_begin"></td>
                                            <td class="pdate_end"></td>
                                            <td class="pedu_type"></td>
                                            <td class="pdoc_type"></td>
                                            <td>
                                                <input value="�������" class="delete_prof_development btn btn-nsau btn-sm" style="width: 85%!important;">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="AddictionalEducation wide">
                        <div class="panel panel-nsau">
                            <div class="panel-heading">
                                �������� �� ������ �������� � �������
                            </div>

                            <div class="panel-body">
                                <table width="100%" data-id="<?=$man["id"]?>" class="table table-striped">
                                    <thead class="add_edu">
                                        <th width="15%">�������� � ���������� ������ ������� ��� ������</th>
                                        <th width="20%">��������������� ����������</th>
                                        <th width="20%">���� ������ ���������</th>
                                        <th width="20%">���� ���������� ������ ������� ��� ������</th>
                                        <th width="15%"></th>
                                    </thead>

                                    <?$addictional_education = (!empty($display_variant["addictional_education"])) ? $display_variant["addictional_education"] : $MODULE_OUTPUT["addictional_education"];?>
                                    <?
                                    if(!empty($addictional_education)) 
                                    {   
                                        ?>
                                        <?
                                        foreach($addictional_education as $id=>$add_edu) 
                                        {
                                            ?>
                                            <?$add_edu["certificate"] = !empty($add_edu["certificate"]) ? $add_edu["certificate"] : $add_edu["certificate_rank"]?>
                                            <tr data-id="<?=$id?>" class="addictional_edu">
                                                <td>


                                                <div onclick="$(this).children('select').removeClass('hidden');$(this).children('span').hide();"> 
                                                    <select class="fix_input form-control hidden" name="a_certificate[]" style="margin:0px;">
                                                        <option value="0"></option>
                                                        <?foreach($MODULE_OUTPUT["teacher"]["add"]["degree"] as $cid => $cval) {
                                                            if ((($add_edu["certificate"]==$cval["id"]) && ($add_edu["type"]==$cval["type"]))) 
                                                            {
                                                                $sveden_docs = $cval["name"];
                                                            }
                                                            ?>
                                                            <option <?=((($add_edu["certificate"]==$cval["id"]) && ($add_edu["type"]==$cval["type"])) ? "selected" : "")?> value="<?=$cval["id"]?>;<?=$cval["type"]?>"><?=$cval["name"]?></option>
                                                        <?}?>
                                                    </select>
                                                    <span><?=$sveden_docs?></span>
                                                </div>
                                                </td>
                                                <td>                                                
                                                    <div onclick="$(this).children('input').removeClass('hidden');$(this).children('span').hide();"> 
                                                        <span><?=$add_edu["university"]?></span>
                                                        <input class="fix_input form-control hidden" type="text" name="a_university[]" value="<?=$add_edu["university"]?>">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div onclick="$(this).children('input').removeClass('hidden');$(this).children('span').hide();"> 
                                                        <span><?=$add_edu["date_doc"]?></span>
                                                        <input class="date fix_input form-control hidden" type="text" name="a_date_doc[]" value="<?=$add_edu["date_doc"]?>">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div onclick="$(this).children('input').removeClass('hidden');$(this).children('span').hide();"> 
                                                        <span><?=$add_edu["date_rank"]?></span>
                                                        <input class="date fix_input form-control hidden" type="text" name="a_date_rank[]" value="<?=$add_edu["date_rank"]?>">
                                                    </div>
                                                </td>
                                                <td>
                                                    <input class="delete_add_education btn btn-nsau btn-sm" value="�������" style="width: 85%!important;">
                                                </td>
                                            </tr>
                                        <?}?>
                                    <?}?>
                                    <tr class="empty_add_edu<?=(!empty($addictional_education)) ? " hidden" : ""?>">
                                        <td colspan="5">��� ����������</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <select class="fix_input form-control" id="certificate" style="margin: 0px;">
                                                <option value="0"></option>
                                                <?foreach($MODULE_OUTPUT["teacher"]["add"]["degree"] as $cid => $cval) {?>
                                                    <option value="<?=$cval["id"]?>;<?=$cval["type"]?>"><?=$cval["name"]?></option>
                                                <?}?>
                                            </select>
                                        </td>
                                        <td><input type="text" class="fix_table form-control" id="a_university"></td>
                                        <td><input type="text" class="fix_table date form-control" id="date_doc"></td>
                                        <td><input type="text" class="fix_table date form-control" id="date_rank"></td>
                                        <td>
                                            <input class="btn btn-nsau btn-sm" value="��������" id="add_add_education" style="width: 85%!important;">
                                        </td>
                                    </tr>
                                    <tr data-id="" class="atmp hidden">
                                        <td class="acertificate">
                                            <select class="fix_table form-control" name="a_certificate[]">
                                                <option value="0"></option>
                                                <?foreach($MODULE_OUTPUT["teacher"]["add"]["degree"] as $cid => $cval) {?>
                                                    <option value="<?=$cval["id"]?>;<?=$cval["type"]?>"><?=$cval["name"]?></option>
                                                <?}?>
                                            </select>   </td>
                                        <td class="auniversity"><input class="fix_table form-control" type="text" name="a_university[]"></td>
                                        <td class="adate_doc"><input class="fix_table form-control" type="text" name="a_date_doc[]"></td>
                                        <td class="adate_rank"><input class="fix_table form-control" type="text" name="a_date_rank[]"></td>
                                        <td>
                                            <input value="�������" class="delete_add_education btn btn-nsau btn-sm" style="width: 85%!important;">
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>

        <div class="EducationalProgram wide">
            <div class="panel panel-nsau">
                <div class="panel-heading">
                    ������� � ���������� ��������������� ��������
                </div>

                <div class="panel-body">
                    <table width="100%" data-id="<?=$man["id"]?>" class="table table-striped">
                        <thead>
                            <th width="25%">��������� ��������������� ���������</th>
                        </thead>
                        <tbody class="add_edu_prog">
                        <tr class="empty_edu_prog<?=(!empty($MODULE_OUTPUT['educational_programs'])) ? " hidden" : ""?>">
                            <td colspan="5">��� ����������</td>
                        </tr>
                        <?php if(!empty($MODULE_OUTPUT['educational_programs'])) {
                            foreach($MODULE_OUTPUT['educational_programs'] as $program) { ?>
                                <tr id="<?=$program['id']?>" class="educational_program">
                                    <td><input class="fix_table form-control" type="text" name="" value="<?=$program['code'].' - '.$program['name']?>"></td>
                                    <td style="text-align: center;">
                                        <input value="�������" class="edit_educational_program btn btn-nsau btn-sm" data-id="<?=$program['id']?>" data-mode="delete" style="width: 60%!important;">
                                    </td>
                                </tr>
                            <?php } ?>
                        <?php } ?>
                        <?php if(!empty($MODULE_OUTPUT['faculties'])){ ?>
                                <tr class="addictional_edu">
                                    <td style="width: 100%; padding-top: 5%;">
                                        <div>
                                            <select class="form-control">
                                                <option value="0">�������� ��������������� ���������:</option>
                                                <?php foreach($MODULE_OUTPUT['faculties'] as $id=>$faculty){ ?>
                                                    <optgroup label="<?=$faculty['fac_name']?>">
                                                        <?php if(!empty($faculty['specialities'])){
                                                            foreach($faculty['specialities'] as $fac){ ?>
                                                                <option value="<?=$fac["id"]?>"><?=$fac['code'].' - '.$fac["name"]?></option>
                                                            <?php }
                                                        } ?>
                                                    </optgroup>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </td>
                                    <td style="width: auto; padding-top: 6%;">
                                        <input class="edit_educational_program btn btn-nsau btn-sm" data-mode="insert" value="��������">
                                    </td>
                                </tr>
                        <?php } ?>
                        <tr id="" class="atmp hidden">
                            <td><input class="fix_table form-control" type="text" name=""></td>
                            <td style="text-align: center;">
                                <input value="�������" class="edit_educational_program btn btn-nsau btn-sm" data-id="" data-mode="delete" style="width: 60%!important;">
                            </td>
                        </tr>
                    </tbody>
                    </table>
                </div>
            </div>
        </div>

                        <div class="wide">
                            <div class="form-notes">
                                <dl>
<?php if($Engine->OperationAllowed($MODULE_OUTPUT["module_id"], "teachers.retired.handle", -1, $Auth->usergroup_id)){ ?>
                                    <dt><input type="checkbox" name="retired" value="1" <?=($man["status_id"] == 9 ? "checked='checked'" : "") ?> /> ������</dt>
                <?php } else { ?>
                <input type="hidden" name="retired" value="<?=($man["status_id"] == 9 ? 1 : 0) ?>" />
                <?php } ?>
<?php if($Engine->OperationAllowed(5, "teachers.decree.handle", -1, $Auth->usergroup_id)){ ?>
                                     <dt><input type="checkbox" name="decret" value="1" <?=($man["status_id"] == 10 ? "checked='checked'" : "") ?> /> ������</dt>
<?}?>
                                </dl>
                            </div>
                        </div>

            </fieldset>
<?php   }
        if($man["people_cat"] == 1 && (!isset($submode) || $submode == 1)) { ?>
            <fieldset id="student_subform">
                <legend  class="form_title">������ ��������</legend>
                <div class="wide">
                    <div class="form-notes">
                        <dl>
                            <?if($Auth->usergroup_id==1) {?>
                                <dt><label>������:</label></dt>
                                <dd>
                                    <select name="group" size="1">
                                        <option value="0"></option>
    <?php       foreach ($MODULE_DATA["output"]["groups"] as $gr) { ?>
                                        <option value="<?=$gr["id"]?>"<?=($man["id_group"]==$gr["id"] ? " selected='selected'" : "")?>><?=$gr["name"]?></option>
    <?php       } ?>
                                    </select>
                                </dd>
                            <?}?>
                            <dt><label>���������:</label></dt>
                            <dd>
                                <input name="subgroup" type="radio" value=""<?=(empty($man["subgroup"]) ? " checked='checked'" : "")?> /> ���
                                <input name="subgroup" type="radio" value="�"<?=($man["subgroup"] == '�' ? " checked='checked'" : "")?> /> �
                                <input name="subgroup" type="radio" value="�"<?=($man["subgroup"] == '�' ? " checked='checked'" : "")?> /> �
                            </dd>
                        </dl>
                    </div>
                    <div class="form-notes">
                        <dl>
                            <!--dt><label>�������������:</label></dt>
                            <dd>
                                <select name="speciality" size="1" class="select_long">
                                    <option value="0"></option>
<?php       foreach ($MODULE_DATA["output"]["specialities"] as $spec) { ?>
                                    <option value="<?=$spec["id"]?>"<?=($MODULE_DATA["output"]["type_id"]==$spec["id"] ? " selected='selected'" : "")?>><?=$spec["type"]?></option>
<?php       } ?>
                                </select>
                            </dd-->
                            <dt><label>��� �����������:</label></dt>
                            <dd><input name="st_year" type="text" value="<?=isset($man["year"]) ? $man["year"] : "" ?>" /></dd>
                            <?if(($Auth->usergroup_id==1)||($Auth->usergroup_id==52)) {?>
                            <dt><label>����:</label><br><small class="red">������������ ���������� �������� � ����� ����� 9</small></dt>
                            <dd><input name="library_card" type="text" value="<?=isset($man["library_card"]) ? $man["library_card"] : "" ?>" /></dd>
                            <?php   } ?>
                        </dl>
                    </div> 
                    <div class="form-notes">
                        <dl>
                            <dt><label>Email:</label></dt>
                            <dd><input name="email" type="text" value="<?=isset($man["email"]) ? $man["email"] : "" ?>" /></dd>
                        </dl>
                    </div>
                </div>
            </fieldset>
<?php   } ?>
<?if(!$Engine->OperationAllowed(5, "people.edit.auth", -1, $Auth->usergroup_id)) {?>                        <input type="hidden" value="edit_people" name="mode" />

                        <input type="submit" value="������" class="button"/>
<? } else { ?>
            <fieldset>
                <legend  class="form_title">������ ������� ������</legend>
<?php   $user_group = $MODULE_DATA["output"]["users"][$MODULE_DATA["output"]["cur_user"]]['usergroup_id'];
        /*if(!$MODULE_DATA["output"]["cur_user"]) { ?>
                <div class="wide">
                    <dl>
                        <dt><label>�������� � ������� ������� ������������:</label></dt>
                        <dd>
                            <input type="radio" name="user" value="0" onclick="show_fields(this.value);"<?=($MODULE_DATA["output"]["cur_user"] ? "" : " checked='checked'")?> />������� �����<br />
                            <input type="radio" name="user" value="1" onclick="show_fields(this.value);"<?=($MODULE_DATA["output"]["cur_user"] ? " checked='checked'" : "")?> />��������� � ������������<br />
                            <input type="radio" name="user" value="2" onclick="show_fields(this.value);" />�� ���������
                        </dd>
                    </dl>
                </div>
                <div class="wide" id="olduser"<?=($MODULE_DATA["output"]["cur_user"] ? " style='display:block;'" : " style='display:none;'")?>>
                    <dl>
                        <dt><label>�������� ������� ������:</label></dt>
                        <dd>
                            <select name="user_select" size="1">
                                <option value="0"></option>
<?php       foreach ($MODULE_DATA["output"]["users"] as $user) {
                if($MODULE_DATA["output"]["cur_user"] == $user["id"]) ?>
                                <option value="<?=$user["id"]?>"<?=($MODULE_DATA["output"]["cur_user"] == $user["id"] ? " selected='selected'" : "")?>><?=$user["displayed_name"]?></option>
<?php       } ?>
                            </select>
                        </dd>
                    </dl>
                </div>
<?php   } else {*/ ?>
                <input type="hidden" name="user_select" value="<?=(isset($MODULE_DATA["output"]["cur_user"]) ? $MODULE_DATA["output"]["cur_user"] : 0)?>" />
                <input type="hidden" name="user" value="0" />
<?php   //} ?>
                <div class="wide" id="newuser">
                    <div class="form-notes">
                        <dl>
                            <dt><label>�����:</label></dt>
                            <dd><input name="username" type="text" value="<?=(!empty($display_variant["username"])) ? $display_variant["username"] : $man["username"]?>" />
            <?php if($Engine->OperationAllowed(0, "userGroup.users.loginAs", $user_group, $Auth->usergroup_id)){?><a href="<?=$EE["engine_uri"];?>?<?=$_SESSION['private_node_id'];?>[loginas]=<?=(isset($MODULE_DATA["output"]["cur_user"]) ? $MODULE_DATA["output"]["cur_user"] : 0)?>">����� ��� ������������</a><?php } ?></dd>
                            <dt><label>E-mail:</label></dt>
                            <dd><input name="email" type="text" value="<?=(!empty($display_variant["email"])) ? $display_variant["email"] : $man["email"]?>" /></dd>
                        </dl>
                    </div>
                    <div class="form-notes">
                        <dl>
                            <dt>
                                <label>������:<span id="genpass"></span></label>
                                <span id="copypass"><a onclick="copypass('genpass','add_pass','repeatpass');">������������ ���� ������<span id="instead" class="instead"></span></a></span>
                            </dt>
                            <dd>
                                <input name="password1" id="add_pass" type="password"  autocomplete="off" />
                                <a onclick="passgen('genpass',3,1,1,'add_pass');">������������� ������</a>
                            </dd>
                            <dt><label>��������� ������:</label></dt>
                            <dd><input name="password2" id="repeatpass" type="password" /></dd>
                        </dl>
                    </div>
                    <?php if($Auth->usergroup_id==1) { ?><div class="wide">
                        <dl id="usergroup" >
                            <dt><label>������ �������������:</label></dt>
                            <dd>
                                <select name="usergroup" size="1">
                                    <option value="0"></option>
<?php   foreach ($MODULE_DATA["output"]["usergroups"] as $st) { ?>
                                    <option value="<?=$st["id"]?>"<?=($st["id"] == ((!empty($display_variant["usergroup"])) ? $display_variant["usergroup"] :$man["usergroup"]) ? "selected='selected'" : "")?>><?=$st["comment"]?></option>
<?php   } ?>
                                </select>
                            </dd>
                        </dl>
                    </div>
                </div><?php } ?>
                <div class="wide">
                    <p>

                        <input type="hidden" value="edit_people" name="mode" />
                        <input type="submit" value="������" class="button check_forms"/>
                    </p>
                </div>
            </fieldset>
<?}?>
        </form>
<?php
    }
    break;
case "teachers_timetable":
    {
        if(!empty($MODULE_OUTPUT["tt"])) {
            $path = explode("/",$this->uri);
            $wdays = array("��","��","��","��","��","��", "��");
            $wpairs = array("9.00-10.30","10.40-12.10","12.50-14.20","14.30-16.00","16.10-17.40","17.45-19.15", "19.20-20.50");
            // foreach($MODULE_OUTPUT["pairs"]["days"] as $ddid=>$d)
                // $wdays[$ddid] = $wdays_t[$ddid];
            // foreach($MODULE_OUTPUT["pairs"]["pairs"] as $ppid=>$p)
                // $wpairs[$ppid] = $wpairs_t[$ppid];
            ksort($wpairs);
            $min_pid = key($wpairs);
            end($wpairs);
            $max_pid = key($wpairs);
            $base_mode = array("one", "two", "three");?>
            <table width="100%" border="1" class="" style="border-collapse: collapse; font: normal 14px arial;">
                <tr>
                    <th colspan="<?=count($wpairs)+2?>">
                    <?=$MODULE_OUTPUT["tt_people_name"]?>
                    </th>
                </tr>
                <tr>
                    <th>����</th>
                    <th>������</th>
                    <?foreach($wpairs as $pid=>$pair) {?>
                        <th><?=$pair?></th>
                    <?}?>
                </tr>
                <?foreach($wdays as $did=>$day) {
                    $day_n = 0;?>
                    <?if(!empty($MODULE_OUTPUT["pairs"]["days"][$did])) {?>
                        <?$rowspan = $MODULE_OUTPUT["pairs"]["days"][$did][0]+$MODULE_OUTPUT["pairs"]["days"][$did][1];?>
                        <?for($i=0; $i<2;$i++) {?>
                            <?for($k=0;$k<$MODULE_OUTPUT["pairs"]["days"][$did][$i];$k++) {?>
                                <?$f=0;?>
                                <?$mod = $base_mode[$k];?>
                                <?foreach($wpairs as $pair_id=>$pairs) {?>
                                    <?if(!$f && ($pair_id==$min_pid)){?>
                                        <tr align="center">
                                            <?if(!$day_n) {?><td rowspan="<?=$rowspan?>"><?=$day;?></td><?$day_n = 1;}?>
                                            <td ><?=($i) ? "�" : "�"?></td>
                                    <?}?>
                                    <?$p = $MODULE_OUTPUT["tt"][$i][$did][$pair_id][$mod]?>
                                            <td>
                                            <?if(!empty($p)) {?>
                                                <?=$p["subject"]?>(<?=$p["type"]?>); <?=$p["groups"]?>; <?=$p["auditorium"]?>;<br>
                                                <span style="font-size: 12px"><?=($p["from"]) ? "c ".implode(".", array_reverse(explode("-",$p["from"]))) : ""?> <?=($p["to"]) ? "�� ".implode(".", array_reverse(explode("-",$p["to"]))) : ""?></span>
                                            <?}?>
                                            </td>
                                    <?if(!$f && ($pair_id==$max_pid)){?></tr><?$f=1;}?>
                                <?}?>
                            <?}?>
                        <?}?>
                    <?} else {?>
                    <tr align="center">
                        <td><?=$day;?></td>
                        <?for($c=0;$c<(count($wpairs)+1);$c++) {?>
                            <td></td>
                        <?}?>
                    </tr>

                    <?}?>
                <?}?>
                <?if(count($path)<5) {?>
                    <tr>
                        <td colspan="<?=count($wpairs)+2?>">
                            <a href="/people/teachers-timetable-print/<?=$MODULE_OUTPUT["tt_people_id"]?>/">����������� ����������</a>
                        </td>
                    </tr>
                <?}?>
            </table>
        <?}?>
<?}
    break;

    case "add_people": {
        if ($MODULE_OUTPUT["allow_peopledit"]) { ?>
        <script>jQuery(document).ready(function($) {GlobalPeople();});</script>
<?php   }
        if(isset($MODULE_OUTPUT["display_variant"]["add_item"])) {
            $display_variant = $MODULE_OUTPUT["display_variant"]["add_item"];
        }
        $submode = $MODULE_DATA["output"]["submode"];
        if(isset($MODULE_OUTPUT["is_exist"])) {
            $exist = $MODULE_OUTPUT["is_exist"];
            //foreach($MODULE_OUTPUT["is_exist"] as $exist) { ?>
                <form method="post" name="is_exist">
                    <fieldset>
                        <p>
<?php           if(isset($exist["people"]["dep_identic"])) {
                    echo "�������������&nbsp;";
                    echo $exist["people"]["last_name"]."&nbsp;";
                    if(isset($exist["people"]["name"])) echo $exist["people"]["name"]."&nbsp;";
                    if(isset($exist["people"]["patronymic"])) echo $exist["people"]["patronymic"];
                    if(isset($exist["people"]["dep_identic"])) echo ", ���������� � ��������:&nbsp;";
                } else {
                    echo "������������&nbsp;";
                    echo $exist["people"]["last_name"]."&nbsp;";
                    if(isset($exist["people"]["name"])) echo $exist["people"]["name"]."&nbsp;";
                    if(isset($exist["people"]["patronymic"])) echo $exist["people"]["patronymic"];
                }
                if(isset($exist["people"]["dep_identic"])) {
                    $dep_str = null;
                    foreach($exist["people"]["department"]["old"] as $id => $dep_name) {
                        if(!is_null($dep_str)) {
                            $dep_str .= ", ".$dep_name;
                        } else {
                            $dep_str = $dep_name;
                        }
                    }
                    echo $dep_str;
                }
                echo "&nbsp;��� ����������."; ?>
                        </p>
                        <p>
                            <input type="hidden" value="<?=$exist["people_cat"]?>" name="people_cat" />
                            <input type="hidden" value="<?=$exist["people"]["last_name"]?>" name="last_name" />
                            <input type="hidden" value="<?=$exist["people"]["name"]?>" name="first_name" />
                            <input type="hidden" value="<?=$exist["people"]["patronymic"]?>" name="patronymic" />
                            <input type="hidden" value="<?=$exist["people"]["male"]?>" name="male" />
                            <input type="hidden" value="<?=$exist["status"]?>" name="status" />
                            <input type="hidden" value="<?=$exist["phone"]?>" name="phone" />
                            <input type="hidden" value="<?=$exist["email_info"]?>" name="email_info" />
                            <input type="hidden" value="<?=$exist["location"]?>" name="location" />
                            <input type="hidden" value="<?=$exist["location_building"]?>" name="location_building" />
                            <input type="hidden" value="<?=$exist["post_itemprop"]?>" name="post_itemprop" />
                            <input type="hidden" value="<?=$exist["post"]?>" name="post" />
                            <?foreach($exist["academ_stat"] as $val) {?>
                                <input type="hidden" value="<?=$val?>" name="academ_stat[]" />
                            <?}?>
                            <?foreach($exist["degree"] as $val) {?>
                                <input type="hidden" value="<?=$val?>" name="degree[]" />
                            <?}?>
                            <?//�����������?>
                            <?foreach($exist["e_university"] as $val) {?>
                                <input type="hidden" value="<?=$val?>" name="e_university[]" />
                            <?}?>
                            <?foreach($exist["e_speciality"] as $val) {?>
                                <input type="hidden" value="<?=$val?>" name="e_speciality[]" />
                            <?}?>
                            <?foreach($exist["e_year_end"] as $val) {?>
                                <input type="hidden" value="<?=$val?>" name="e_year_end[]" />
                            <?}?>
                            <?foreach($exist["e_qualification"] as $val) {?>
                                <input type="hidden" value="<?=$val?>" name="e_qualification[]" />
                            <?}?>
                            <?//��� �����������?>
                            <?foreach($exist["a_university"] as $val) {?>
                                <input type="hidden" value="<?=$val?>" name="a_university[]" />
                            <?}?>
                            <?foreach($exist["a_date_doc"] as $val) {?>
                                <input type="hidden" value="<?=$val?>" name="a_date_doc[]" />
                            <?}?>
                            <?foreach($exist["a_date_rank"] as $val) {?>
                                <input type="hidden" value="<?=$val?>" name="a_date_rank[]" />
                            <?}?>
                            <?foreach($exist["a_certificate"] as $val) {?>
                                <input type="hidden" value="<?=$val?>" name="a_certificate[]" />
                            <?}?>
                            <?//////////////?>

                            <textarea class="hidden" name="comment"><?=$exist["comment"]?></textarea>
<?php           if($exist["people_cat"] == 1) { ?>
                            <input type="hidden" value="<?=$exist["group"]?>" name="group" />
                            <input type="hidden" value="<?=$exist["subgroup"]?>" name="subgroup" />
                            <input type="hidden" value="<?=$exist["speciality"]?>" name="speciality" />
                            <input type="hidden" value="<?=$exist["year"]?>" name="year" />
<?php           } elseif($exist["people_cat"] == 2) {
                    /*if(isset($exist["people"]["department"]["old"])) {
                        $i = 0;
                        foreach($exist["people"]["department"]["old"] as $id => $dep_name) { ?>
                            <input type="hidden" value="<?=$id?>" name="department[<?=$i?>]" />
<?php                       $i++;
                        }
                    }*/
                    if(isset($exist["people"]["department"]["new"])) {
                        foreach($exist["people"]["department"]["new"] as $i => $dep_id) { ?>
                            <input type="hidden" value="<?=$dep_id?>" name="department[<?=$i?>]" />
<?php                   }
                    } ?>
                            <input type="hidden" value="<?=$exist["exp_full"]?>" name="exp_full" />
                            <input type="hidden" value="<?=$exist["exp_teach"]?>" name="exp_teach" />
                            <input type="hidden" value="<?=$exist["prad"]?>" name="prad" />
                            <input type="hidden" value="<?=$exist["post"]?>" name="post" />
<?php           } else { ?>

<?php           } ?>
                            <input type="hidden" value="<?=$exist["user"]?>" name="user" />
<?php           if(isset($exist["people"]["usergroup"])) { ?>
                            <input type="hidden" value="<?=$exist["usergroup"]?>" name="usergroup" />
<?php           } ?>
<?php           if(isset($exist["people"]["user_select"])) { ?>
                            <input type="hidden" value="<?=$exist["user_select"]?>" name="user_select" />
<?php           } ?>
                            <input type="hidden" value="<?=$exist["username"]?>" name="username" />
                            <input type="hidden" value="<?=$exist["email"]?>" name="email" />
                            <input type="hidden" value="<?=$exist["password1"]?>" name="password1" />
                            <input type="hidden" value="<?=$exist["password2"]?>" name="password2" />
                            <label>�� �������, ��� ����� �������� ��� ���?</label>
                            <input type="submit" value="��" name="check" class="button" />
                            <input type="submit" value="���" name="check" class="button" />
                            <label>&nbsp;&nbsp;��� ������� �� <a href="/people/<?=$exist["people"]["id"]?>/">��������������</a>.</label>
                        </p>
                    </fieldset>

                </form>
<?php       //}
        } ?>
        <form id="people_form" class="people_cat_<?=(isset($submode) ? $submode : "1")?>" method="post" enctype="multipart/form-data" onsubmit="if(document.getElementById('add_pass').value != document.getElementById('repeatpass').value) {alert('������ ���������� �������'); return false;}">
<?php   if(!isset($submode) || $submode != 2) { ?>

            <h2>�������� ��������</h2>
<?php   } else { ?>
            <h2>�������� �������������</h2>
<?php   } ?>
            <fieldset>
                <legend class="form_title">����� ����������</legend>
<?php   if(!isset($submode)) { ?>
                <div class="wide">
                    <div class="form-notes">
                        <dl>
                            <dt><label>��������� ������������:</label></dt>
                            <dd>
                                <select id="people_cat_select" name="people_cat" size="1">
                                    <option value="1"<?=((!isset($display_variant) || (isset($display_variant) && $display_variant["people_cat"] == 1)) ? " selected='selected'" : "")?>>�������</option>
                                    <option value="2"<?=(isset($display_variant) && $display_variant["people_cat"] == 2 ? " selected='selected'" : "")?>>�������������</option>
                                    <option value="3"<?=(isset($display_variant) && $display_variant["people_cat"] == 3 ? " selected='selected'" : "")?>>��������� ������� ������</option>
                                </select>
                            </dd>
                        </dl>
                    </div>
                    <div class="form-notes">
                        <dl id="responsible">
                            <dt><label>�������������:</label></dt>
                            <dd>
                                <input type="checkbox" name="responsible"<?=(isset($display_variant["responsible"]) ? " checked='checked'" : "")?> />
                            </dd>
                        </dl>
                    </div>
                </div>
<?php   } else { ?>
                <input type="hidden" name="people_cat" value="<?=$submode?>" />
<?php   } ?>
                <div class="wide">
                    <div class="form-notes">
                        <dl>
                            <dt><label>�������:<span class="obligatorily">*</span></label></dt>
                            <dd><input name="last_name" type="text" class="input_long" value="<?=(isset($display_variant) ? $display_variant["last_name"] : "")?>" /></dd>
                            <dt><label>���:<span class="obligatorily">*</span></label></dt>
                            <dd><input name="first_name" type="text" class="input_long" value="<?=(isset($display_variant) ? $display_variant["first_name"] : "")?>" /></dd>
                            <dt><label>��������:<span class="obligatorily">*</span></label></dt>
                            <dd><input name="patronymic" type="text" class="input_long" value="<?=(isset($display_variant) ? $display_variant["patronymic"] : "")?>" /></dd>
                        </dl>
                    </div>
                    <div class="form-notes">
                        <dl>
                            <dt><label>����������:</label></dt>
                            <dd>
                                <div class="user_photo float-right"><img src="/images/people/no_photo.jpg" alt="" /></div>
                                <span id="photoinput"><input name="user_photo" type="file" id="user_photo" /></span>
                            </dd>
                            <dt><label>���:</label></dt>
                            <dd><input type="radio" name="male" value="1"<?=(((isset($display_variant) && $display_variant["male"] == 1) || !isset($display_variant)) ? " checked='checked'" : "")?> /> �/� <input type="radio" name="male" value="0"<?=(isset($display_variant) && $display_variant["male"] == 0 ? " checked='checked'" : "")?> /></dd>
                            <dt class="add_status"><label>������:</label></dt>
                            <dd class="add_status">
                                <ul>
<?php   $i = 0;
        foreach ($MODULE_DATA["output"]["statuses"] as $st) {
            if(!isset($submode) || (isset($submode) && $submode == $st["people_cat"])) { ?>
                                    <li class="people_cat_<?=$st["people_cat"]?><?=(isset($display_variant) && $display_variant["status"] == $st["id"] ? " selected" : "")?>">
                                        <input type="radio" name="status" value="<?=$st["id"]?>"<?=(!isset($display_variant) && $i == 0 ? " checked='checked'" : "")?> /><?=$st["name"]?>
                                    </li>
<?php           $i++;
            }
        } ?>
                                </ul>
                            </dd>
                        </dl>
                    </div>
                </div>
                <div class="wide">

                    <?if($man["people_cat"]!=1) {?>
                        <dl class="not_for_student">
                            <dt><label>�������(�������):</label></dt>

                            <dd><input type="text" id="phone" name="phone" value="<?=(isset($display_variant) ? $display_variant["phone"] : "")?>"></dd>
                        </dl>
                        <dl class="not_for_student">
                            <dt><label>E-mail:</label></dt>
                            <dd><input type="text" name="email_info" value="<?=(isset($display_variant) ? $display_variant["email_info"] : "")?>"></dd>
                        </dl>
                        <dl class="not_for_student">
                            <dt><label>������������:</label></dt>
                            <dd>
                                ������ (�����������������):
                                <select style="width:10%" name="location_building">
                                    <option value=""></option>
                                    <?foreach($MODULE_OUTPUT["buildings"] as $id => $val) {?>
                                        <option <?=($display_variant["location_building"]==$id) ? "selected" : ""?> value="<?=$id?>"><?=$val["name"]?> (<?=$val["label"]?>)</option>
                                    <?}?>
                                </select>

                            �������:
                            <input type="text" name="location" value="<?=$display_variant["location"]?>">

                            </dd>
                        </dl>

                        <dl class="add_status">
                            <dt><label>���������:<span class="obligatorily">*</span></label></dt>
                            <dd><input type="text" name="post_add" value="<?=$display_variant["post"]?>"></dd>
                        </dl>

                        <dl>
                    </dl>
                    <?}?>
                </div>
            </fieldset>
<?php   if(!isset($submode) || $submode == 2) { ?>
            <fieldset id="teacher_subform">
                <legend class="form_title">������ �������������</legend>
                <div class="wide">
                     <table class="nsau_table">
                       <thead>
                           <tr>
                               <th>���������</th>
                               <th>�������</th>
                               <th>�������</th>
                           </tr>
                       </thead>
                       <tbody>
                            <?if(!empty($display_variant['department_post'])) {$man['department_post'] = $display_variant['department_post'];?>
                                <?foreach ($man['department_post'] as $d_id => $p_id) {?>
                                   <tr class = 'post_row'>
                                       <td>
                                            <select name="new_post_itemprop[]" class="select_long">
                                                <?foreach($MODULE_OUTPUT["teacher"]["post"] as $id => $val) {?>
                                                    <option <?=(($p_id==$id) ? "selected" : "")?> value="<?=$id?>"><?=$val["name"]?></option>
                                                <?}?>
                                            </select>
                                        </td>
                                       <td>
                                            <select name="new_department_itemprop[]" class="select_long">
                                                <?foreach($MODULE_OUTPUT['departments'] as $id => $val) {?>
                                                    <option <?=(($d_id==$id) ? "selected" : "")?> value="<?=$id?>"><?=$val?></option>
                                                <?}?>
                                            </select>
                                       </td>
                                       <td class="del_department_post" style='cursor: pointer'>X</td>
                                   </tr>
                                <?}?>
                            <?} else {?>
                                <tr class = 'post_row'>
                                   <td>
                                        <select name="new_post_itemprop[]" class="select_long">
                                             <option value="0">---</option>
                                            <?foreach($MODULE_OUTPUT["teacher"]["post"] as $id => $val) {?>
                                                <option <?=(($p_id==$id) ? "selected" : "")?> value="<?=$id?>"><?=$val["name"]?></option>
                                            <?}?>
                                        </select>
                                    </td>
                                   <td>
                                        <select name="new_department_itemprop[]" class="select_long">
                                            <option value="0">---</option>
                                            <?foreach($MODULE_OUTPUT['departments'] as $id => $val) {?>
                                                <option <?=(($d_id==$id) ? "selected" : "")?> value="<?=$id?>"><?=$val?></option>
                                            <?}?>
                                        </select>
                                   </td>
                                   <td class="del_department_post" style='cursor: pointer'>X</td>
                               </tr>
                            <?}?>
                            <tr>
                               <td colspan='3' style='cursor: pointer' class='add_department_post'>�������� ���������</td>
                            </tr>
                       </tbody>
                    </table>
                    <div class="form-notes">
                        <dl>
                            <dt><label>�������:</label></dt>
                            <dd>
                                <select name="degree[]" multiple class="select_long">
                                    <option value="0"></option>
                                    <?foreach($MODULE_OUTPUT["teacher"]["degree"] as $id => $val) {?>
                                        <option <?=(in_array($id, $display_variant["degree"]) ? "selected" : "")?> value="<?=$id?>"><?=$val["name"]?></option>
                                    <?}?>
                                </select>
                            </dd>
                            <dt><label>������:</label></dt>
                            <dd>
                                <select name="academ_stat[]" multiple class="select_long">
                                    <option value="0"></option>
                                    <?foreach($MODULE_OUTPUT["teacher"]["rank"] as $id => $val) {?>
                                        <option <?=(in_array($id, $display_variant["academ_stat"]) ? "selected" : "")?> value="<?=$id?>"><?=$val["name"]?></option>
                                    <?}?>
                                </select>
                            </dd>
                            <dt><label>���������:</label></dt>
                            <dd>
                                <select name="post" size="1" class="select_long">


                                    <option value="����������"<?=($man['post'] == '����������' ? ' selected="selected"' : '')?>>����������</option>
                                    <option value="�������������"<?=($man['post'] == '�������������' ? ' selected="selected"' : '')?>>�������������</option>
                                    <option value="���������� ������������"<?=($man['post'] == '���������� ������������' ? ' selected="selected"' : '')?>>���������� ������������</option>
                                    <option value="��������"<?=($man['post'] == '��������' ? ' selected="selected"' : '')?>>��������, ������� ��������, ����, ��������</option>
                                </select>
                            </dd>
                        </dl>
                    </div>
                    <div class="form-notes">
                        <dl>
                            <dt><label>��� ������ ������ �����:<span class="obligatorily">*</span></label></dt>
                            <dd><input type="text" name="exp_full" value="<?=(isset($display_variant["exp_full"]) ? $display_variant["exp_full"] : "")?>" /></dd>
                            <dt><label>��� ������ ���������������, ������-��������������� �����:</label></dt>
                            <dd><input type="text" name="exp_teach" value="<?=(isset($display_variant["exp_teach"]) ? $display_variant["exp_teach"] : "")?>" /></dd>
                        </dl>
                    </div>
                </div>







                <div class="wide">
                    <div class="form-notes">
<?php       if(!$MODULE_OUTPUT["current_department_id"]) { ?>
                        <dl>
                            <dt><label>�������� �������:</label></dt>
                            <dd>
                                <ul id="department_select" class="select_long">
<?php           foreach($MODULE_OUTPUT["faculties"] as $faculties) { ?>
                                    <li>
                                        <dl>
                                            <dt><?=$faculties["fac_name"]?></dt>
<?php               foreach($faculties["departments"] as $department) { ?>
                                            <dd id="depid_<?=$department["id"]?>"<?=(isset($display_variant["department"][$department["id"]]) ? " class='hidden'" : "")?> title="<?=$department["name"]?>"><?=$department["name"]?></dd>
<?php               } ?>
                                        </dl>
                                    </li>
<?php           } ?>
                                </ul>
                            </dd>
<?php       } ?>
                        </dl>
                    </div>
                    <div class="form-notes">
                        <dl>
                            <dt><label>�������� ����������:</label></dt>
                            <dd>
                                <ul id="subject_select" class="select_long" >



                            <?=empty($MODULE_OUTPUT["departments_subj"]) ? "<li><dl><dt id='als_dep_name'></dt>" : ""?>
<?php           foreach($MODULE_OUTPUT["departments_subj"] as $dep_name => $subjects ) { ?>
                                    <li>
                                        <dl>
                                            <dt id="als_dep_name"></dt>

                                            <dt><?=$dep_name?></dt>
<?php               foreach($subjects as $subj) { ?>
                                            <dd id="subid_<?=$subj["id"]."_".$MODULE_OUTPUT["departments_subj"][$dep_name][0]["department_id"]?>" <?=isset($man["subjects"][$MODULE_OUTPUT["departments_subj"][$dep_name][0]["department_id"]][$subj["id"]]) ? " class='hidden'" : ""?> title="<?=$subj["name"]?>"><?=$subj["name"]?></dd>
<?php               } ?>
                                        </dl>
                                    </li>
<?php           } ?>
<?=empty($MODULE_OUTPUT["departments_subj"]) ? "</dl></li>" : ""?>
                                </ul>
                            </dd>
                        </dl>
                    </div>
                </div>


                <div class="wide">
                    <div class="form-notes">
<?php       if(!$MODULE_OUTPUT["current_department_id"]) { ?>
                        <dl>
                            <dt><label>������������� ����� �������� �� �������:</label></dt>
                            <dd>
                                <ul id="department_list">
<?php           if(isset($display_variant)) {
                    foreach($display_variant["department"] as $dep_id => $dep) { ?>
                                    <li><b><?=$dep?></b><input type="hidden" value="<?=$dep_id?>" name="department[<?=$dep_id?>]" /><em class="inline-block"></em></li>
                                    <?if(!empty($display_variant["subject"][$dep_id])) {?>
                                    <ul id="subject_list_<?=$dep_id?>">
                                        <?foreach($display_variant["subject"][$dep_id] as $id=>$subj) {?>
                                            <li>
                                                <input type="hidden" name="subject[<?=$dep_id?>][<?=$id?>]" value="<?=$id?>">
                                                <?=$subj?>
                                                <em class="inline-block"></em>
                                            </li>
                                        <?}?>
                                    </ul>
                                    <?}?>
<?php               }
                } ?>
                                </ul>
                            </dd>
                        </dl>
<?php       } else { ?>
                        <dl>
                            <dt><label>������������� ����� �������� �� ������� <?=$MODULE_OUTPUT["current_department_name"]?></label></dt>
                            <dd>
                                <input type="hidden" name="department[0]" value="<?=$MODULE_OUTPUT["current_department_id"]?>">
                            </dd>
                        </dl>
<?php       } ?>
                    </div>
                </div>


                <?$man["curator_groups"] = isset($display_variant["curator_groups"]) ? $display_variant["curator_groups"] : $man["curator_groups"];?>
                <?$man["department"] = $display_variant["department"];?>
                <div class="wide">
                    <div class="form-notes">
                        <dl>
                            <dt><label>����������� ������:</label><input name="curator_selector" id="curator_selector" type="checkbox" <?=((isset($display_variant["is_curator"]) ? $display_variant["is_curator"] : $man["is_curator"]) ? "checked='checked'" : "") ?> /></dt>
                        </dl>


                        <div class="form-notes" id="curator_groups_cont" style="width: 100%; display: <?=((isset($display_variant["is_curator"]) ? $display_variant["is_curator"] : $man["is_curator"]) ? "block;" : "none;") ?>">
                            <dl>
                                <dt><label>������ ��������:</label></dt>
                                <dd>
                                    <select id="curator_groups" size="8" multiple="true" >
                                        <options>
                                            <?php   foreach($man["department"] as $dep_id => $dep) { ?>
                                            <optgroup id="<?=$dep_id?>" label="<?=$dep?>">
                                                <?php if (isset($man["curator_groups"]) && !empty($man["curator_groups"][$dep_id]))
                                                        foreach ($man["curator_groups"][$dep_id] as $group) { ?>
                                                    <option value="<?=$group['id']?>"><?=$group['name']?></option>
                                                    <? }
                                                    else { ?>
                                                    <option></option>
                                                <?php } ?>
                                            </optgroup>
                                            <?php }  ?>
                                        </options>
                                    </select>
                                    <div id="curator_groups_selector" > >> </div>
                                    <?php   if (isset($man["curator_groups"]))
                                        foreach($man["department"] as $dep_id => $dep)
                                            foreach ($man["curator_groups"][$dep_id] as $group) { ?>
                                                <input type="hidden" name="curator_groups[<?=$dep_id?>][<?=$group['id']?>]" value="<?=$group['id']?>" />
                                    <?php }?>
                                </dd>
                            </dl>
                            <dl>
                                <dt><label>������ �����:</label></dt>
                                <dd>
                                    <div id="all_groups_selector" style="display: inline-block; vertical-align: top; font-weight:bold; cursor: pointer; text-decoration: underline;"> << </div>
                                    <select id="all_groups" size="16" multiple="true" name="curator_groups[]" >
                                        <options>
                                            <?php foreach ($MODULE_OUTPUT["all_groups"] as $group) { ?>
                                                <option class="fix_table" value="<?=$group['id']?>"><?=$group['name']?></option>
                                            <? } ?>
                                        </options>
                                    </select>
                                </dd>
                            </dl>
                        </div>
                    </div>
                </div>







                <div class="Education wide">
                        <dl>
                            <dt><label>�������� �� �����������<span class="obligatorily">*</span></label></dt>
                        </dl>
                    <table border="1" cellpadding="4" cellspacing="0" width="100%" data-id="<?=$man["id"]?>" class="nsau_table">
                        <thead class="edu">
                            <th width="30%">��������������� ����������</th>
                            <th width="15%">��� ���������</th>
                            <th width="30%">�������������</th>
                            <th width="10%">������������</th>
                            <th width="15%"></th>
                        </thead>
                        <?$education = (!empty($display_variant["education"])) ? $display_variant["education"] : $MODULE_OUTPUT["education"];?>
                        <?if(!empty($education)) {?>
                            <?foreach($education as $id=>$edu) {?>
                                <tr data-id="<?=$id?>" class="edu">
                                    <td><input class="fix_input" type="text" name="e_university[]" value="<?=$edu["university"]?>"></td>
                                    <td><input class="edu_year fix_input" type="text" name="e_year_end[]" value="<?=$edu["year_end"]?>"></td>
                                    <td><input class="fix_input" type="text" name="e_speciality[]" value="<?=$edu["speciality"]?>"></td>
                                    <td><input class="fix_input" type="text" name="e_qualification[]" value="<?=$edu["qualification"]?>"></td>
                                    <td>
                                        <input type="submit" class="delete_education" value="�������">
                                    </td>
                                </tr>
                            <?}?>
                        <?}?>
                        <tr class="empty_edu<?=(!empty($education)) ? " hidden" : ""?>">
                            <td colspan="5">��� ����������</td>
                        </tr>
                        <tr>
                            <td><input type="text" class="fix_table" id="university"></td>
                            <td><input type="text" class="fix_table edu_year" id="year_end"></td>
                            <td><input type="text" class="fix_table" id="speciality"></td>
                            <td><input type="text" class="fix_table" id="qualification"></td>
                            <td>
                                <input type="submit" value="��������" id="add_education">
                            </td>
                        </tr>
                        <tr data-id="" class="etmp hidden">
                            <td class="puniversity"><input  class="fix_input" type="text" name="e_university[]"></td>
                            <td class="pyear_end"><input type="text" class="edu_year fix_input" name="e_year_end[]"></td>
                            <td class="pspeciality"><input class="fix_input" type="text" name="e_speciality[]"></td>
                            <td class="pqualification"><input class="fix_input" type="text" name="e_qualification[]"></td>
                            <td>
                                <input type="submit" value="�������" class="delete_education">
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="AddictionalEducation">
                        <label>�������� �� ������ �������� � �������</label>
                        <table border="1" cellpadding="4" cellspacing="0" width="100%" data-id="<?=$man["id"]?>" class="nsau_table">
                            <thead class="add_edu">
                                <th width="15%">�������� � ���������� ������ ������� ��� ������</th>
                                <th width="20%">��������������� ����������</th>
                                <th width="20%">���� ������ ���������</th>
                                <th width="20%">���� ���������� ������ ������� ��� ������</th>
                                <th width="15%"></th>
                            </thead>
                            <?$addictional_education = (!empty($display_variant["addictional_education"])) ? $display_variant["addictional_education"] : $MODULE_OUTPUT["addictional_education"];?>
                            <?if(!empty($addictional_education)) {?>
                                <?foreach($addictional_education as $id=>$add_edu) {?>
                                    <tr data-id="<?=$id?>" class="addictional_edu">
                                        <td>
                                        <?$cert = explode(";", $add_edu["certificate"]);?>
                                            <select class="fix_input" name="a_certificate[]">
                                                <option value="0"></option>
                                                <?foreach($MODULE_OUTPUT["teacher"]["add"]["degree"] as $cid => $cval) {?>
                                                    <option <?=((($cert[0]==$cval["id"]) && ($cert[1]==$cval["type"])) ? "selected" : "")?> value="<?=$cval["id"]?>;<?=$cval["type"]?>"><?=$cval["name"]?></option>
                                                <?}?>
                                            </select>
                                        </td>
                                        <td><input type="text" class="fix_input" name="a_university[]" value="<?=$add_edu["university"]?>"></td>
                                        <td><input class="date fix_input" type="text" name="a_date_doc[]" value="<?=$add_edu["date_doc"]?>"></td>
                                        <td><input class="date fix_input" type="text" name="a_date_rank[]" value="<?=$add_edu["date_rank"]?>"></td>
                                        <td>
                                            <input type="submit" class="delete_add_education" value="�������">
                                        </td>
                                    </tr>
                                <?}?>
                            <?}?>
                            <tr class="empty_add_edu<?=(!empty($addictional_education)) ? " hidden" : ""?>">
                                <td colspan="5">��� ����������</td>
                            </tr>
                            <tr>
                                <td>
                                    <select class="fix_input" id="certificate">
                                        <option value="0"></option>
                                        <?foreach($MODULE_OUTPUT["teacher"]["add"]["degree"] as $cid => $cval) {?>
                                            <option value="<?=$cval["id"]?>;<?=$cval["type"]?>"><?=$cval["name"]?></option>
                                        <?}?>
                                    </select>
                                </td>
                                <td><input type="text" class="fix_table" id="a_university"></td>
                                <td><input type="text" class="fix_table date" id="date_doc"></td>
                                <td><input type="text" class="fix_table date" id="date_rank"></td>
                                <td>
                                    <input type="submit" value="��������" id="add_add_education">
                                </td>
                            </tr>
                            <tr data-id="" class="atmp hidden">
                                <td class="acertificate">
                                    <select class="fix_input" name="a_certificate[]">
                                        <option value="0"></option>
                                        <?foreach($MODULE_OUTPUT["teacher"]["add"]["degree"] as $cid => $cval) {?>
                                            <option value="<?=$cval["id"]?>;<?=$cval["type"]?>"><?=$cval["name"]?></option>
                                        <?}?>
                                    </select>   </td>
                                <td class="auniversity"><input class="fix_input" type="text" name="a_university[]"></td>
                                <td class="adate_doc"><input class="fix_input" type="text" name="a_date_doc[]"></td>
                                <td class="adate_rank"><input class="fix_input" type="text" name="a_date_rank[]"></td>
                                <td>
                                    <input type="submit" value="�������" class="delete_add_education">
                                </td>
                            </tr>
                        </table>
                    </div>
            </fieldset>
<?php   }
        if(!isset($submode) || $submode == 1) { ?>
            <fieldset id="student_subform">
                <legend class="form_title">������ ��������</legend>
                <div class="wide">
                    <div class="form-notes">
                        <dl>
                            <dt><label>������:</label></dt>
                            <dd>
                                <select name="group" size="1">
                                    <option value="0"></option>
<?php   foreach ($MODULE_DATA["output"]["groups"] as $gr) { ?>
                                    <option value="<?=$gr["id"]?>"<?=(isset($display_variant) && $display_variant["group"] == $gr["id"] ? " selected='selected'" : "")?>><?=$gr["name"]?></option>
<?php   } ?>
                                </select>
                            </dd>
                            <dt><label>���������:</label></dt>
                            <dd>
                                <input name="subgroup" type="radio" value="0"<?=((!isset($display_variant) || (isset($display_variant) && $display_variant["subgroup"] == 0)) ? " checked='checked'" : "")?> /> ���
                                <input name="subgroup" type="radio" value="�"<?=(isset($display_variant) && $display_variant["subgroup"] == "�" ? " checked='checked'" : "")?> /> �
                                <input name="subgroup" type="radio" value="�"<?=(isset($display_variant) && $display_variant["subgroup"] == "�" ? " checked='checked'" : "")?> /> �
                            </dd>
                        </dl>
                    </div>
                    <div class="form-notes">
                        <dl>
                            <!--dt><label>�������������:</label></dt>
                            <dd>
                                <select name="speciality" size="1" class="select_long">
                                    <option value="0"></option>
<?php   foreach ($MODULE_DATA["output"]["specialities"] as $spec) { ?>
                                    <option value="<?=$spec["id"]?>"<?=(isset($display_variant) && $display_variant["speciality"] == $spec["id"] ? " selected='selected'" : "")?>><?=$spec["type"]?></option>
<?php   } ?>
                                </select>
                            </dd-->
                            <dt><label>��� �����������:</label></dt>
                            <dd><input name="st_year" type="text" value="<?=(isset($display_variant) ? $display_variant["st_year"] : "")?>" /></dd>
                        </dl>
                    </div>
                </div>
            </fieldset>
<?php   } ?>
            <fieldset>
                <legend class="form_title">������ ������� ������</legend>
<?php   /*if(!isset($submode) || $submode != 2) { ?>
                <div class="wide">
                    <dl>
                        <dt><label>�������� � ������� ������� ������������:</label></dt>
                        <dd>
                            <input type="radio" name="user" value="0" onclick="show_fields(this.value);"<?=((!isset($display_variant) || (isset($display_variant) && $display_variant["user"] == 0)) ? " checked='checked'" : "")?> />������� �����<br />
                            <input type="radio" name="user" value="1" onclick="show_fields(this.value);"<?=(isset($display_variant) && $display_variant["user"] == 1 ? " checked='checked'" : "")?> />��������� � ������������<br />
                            <input type="radio" name="user" value="2" onclick="show_fields(this.value);"<?=(isset($display_variant) && $display_variant["user"] == 2 ? " checked='checked'" : "")?> />�� ���������
                        </dd>
                    </dl>
                </div>
                <div class="wide" id="olduser">
                    <dl>
                        <dt><label>�������� ������� ������:</label></dt>
                        <dd>
                            <select name="user_select" size="1">
                                <option value="0"></option>
<?php       foreach ($MODULE_DATA["output"]["users"] as $user) { ?>
                                <option value="<?=$user["id"]?>"<?=(isset($display_variant) && $display_variant["user"] == 1 && $display_variant["user_select"] == $user["id"] ? " selected='selected'" : "")?>><?=$user["displayed_name"]?></option>
<?php       } ?>
                            </select>
                        </dd>
                    </dl>
                </div>
<?php   } else {*/ ?>
                <input type="hidden" name="user" value="0"/>
<?php   //} ?>
                <div class="wide" id="newuser">
                    <div class="form-notes">
                        <dl>
                            <dt><label>�����:</label></dt>
                            <dd><input name="username" type="text" value="<?=(isset($display_variant) ? $display_variant["username"] : "")?>" /></dd>
                            <dt><label>E-mail:</label></dt>
                            <dd><input name="email" type="text" value="<?=(isset($display_variant) ? $display_variant["email"] : "")?>" /></dd>
                        </dl>
                    </div>
                    <div class="form-notes">
                        <dl>
                            <dt>
                                <label>������:<span id="genpass"></span></label>
                                <span id="copypass"><a onclick="copypass('genpass','add_pass','repeatpass');">������������ ���� ������<span id="instead" class="instead"></span></a></span>
                            </dt>
                            <dd>
                                <input name="password1" id="add_pass" type="password"  autocomplete="off" />
                                <a onclick="passgen('genpass',3,1,1,'add_pass');">������������� ������</a>
                            </dd>
                            <dt><label>��������� ������:</label></dt>
                            <dd><input name="password2" id="repeatpass" type="password" /></dd>
                        </dl>
                    </div>
                    <div class="wide">
                        <dl id="usergroup">
                            <dt><label>������ �������������:</label></dt>
                            <dd>
                                <select name="usergroup" size="1">
                                    <option value="0"></option>
<?php   foreach ($MODULE_DATA["output"]["usergroups"] as $st) { ?>
                                    <option value="<?=$st["id"]?>"<?=(isset($display_variant) && $display_variant["usergroup"] == $st["id"] && ($display_variant["people_cat"] == 3 || ($display_variant["people_cat"] == 2 && isset($display_variant["responsible"]))) ? " selected='selected'" : "")?>><?=$st["comment"]?></option>
<?php   } ?>
                                </select>
                            </dd>
                        </dl>
                    </div>
                </div>
                <div class="wide">
                    <p>
                        <input type="hidden" value="add_people" name="mode" />
                        <input type="submit" value="������" class="button"/>
                    </p>
                </div>
            </fieldset>
        </form>
<?php
    }
    break;
    case "prof_choice": { ?>
        <script src="/scripts/rekom.js"></script>
        <div id="education" style="display:block;"><p>
        <h3>���� �����������</h3> <span style="color:#555;"><small>������� ��� ������� ������� �����������</small></span><br />
        <a class="dash" id="link1" onclick="show_choice(this, 'show_cont_sec');">������</a> <br />
        <a class="dash" id="link2" onclick="show_choice(this, 'show_pre_level');">�������-����������������</a> <br />
        <a class="dash" id="link3" onclick="show_choice(this, 'show_pre_level');">�������</a> <br />
        </p></div>
        <div id="pre_level" style="display:none;"><p>
        <h3>�������� ������� ����������</h3>
        <span style="color:#555;"><small>����� ������� �� ������ ����� �� ��������� ��������?</small></span><br />
        <a class="dash" id="link4" onclick="show_choice(this, 'show_ege_int');">��������</a> <br />
        <a class="dash" id="link5" onclick="show_choice(this, 'show_ege_int');">����������</a> <br />
        </p></div>
        <div id="cont_sec" style="display:none;"><p>
        <h3>�� ������:</h3>
        <span style="color:#555;"><small>� ����� ������� �� ������ ������ ��������?</small></span><br />
        <a class="dash" id="link6" onclick="show_choice(this, 'show_magistr');">���������� ����������� (������������)</a> <br />
        <a class="dash" id="link7" onclick="show_choice(this, 'show_pre_level_second');">�������� ������ �����������</a> <br />
        </p></div>
        <div id="pre_level_second" style="display:none;"><p>
        <h3>�������� ������� ����������</h3>
        <span style="color:#555;"><small>����� ������� �� ������ ����� �� ��������� ��������?</small></span><br />
        <a class="dash" id="link8" onclick="show_choice(this, 'show_ege_int', 1);">��������</a> <br />
        <a class="dash" id="link9" onclick="show_choice(this, 'show_ege_int', 1);">����������</a> <br />
        </p></div>
        <div id="magistr" style="display:none;"><p>
        <h3>����������� ������������:</h3>
        <ul>
        <?
        foreach ($MODULE_OUTPUT["mag_spec"] as $mag) {
            ?><li><a href="/speciality/mag/<?=$mag["code"]?>/"><?=$mag["name"]?></a><?
        }
        ?>
        </ul>
        </p></div>
        <div id="ege_int" style="display:none;"><p>
        <h3>�����:</h3>
        <a class="dash" id="link10" onclick="show_choice(this, 'show_ege');">�� ����������� ���</a> <br />
        <a class="dash" id="link11" onclick="show_choice(this, 'show_interests');">�� ���������</a> <br />
        </div></p>
        <p><div id="ege" style="display:none;">
        <h3>������� ���� ����� �� ���:</h3>
        <form action="rekom/" method="post">
        <table cols="2" border="0">
        <tr><td><input type="checkbox" name="ege_rus_on" id="ege_rus_on" checked onclick="return false;"> ������� ����:</td><td><input type="text" size="3" name="ege_rus" id="ege_rus"></td></tr>
        <tr><td><input type="checkbox" name="ege_math_on" id="ege_math_on" /> ����������:</td><td><input type="text" size="3" name="ege_math" id="ege_math"></td></tr>
        <tr><td><input type="checkbox" name="ege_inf_on" id="ege_inf_on" onclick="if(this.checked) document.getElementById('ege_inf').removeAttribute('disabled'); else document.getElementById('ege_inf').setAttribute('disabled', 1);"> �����������:</td><td><input type="text" size="3" name="ege_inf" id="ege_inf" disabled></td></tr>
        <tr><td><input type="checkbox" name="ege_hist_on" id="ege_hist_on" onclick="if(this.checked) document.getElementById('ege_hist').removeAttribute('disabled'); else document.getElementById('ege_hist').setAttribute('disabled', 1);"> �������:</td><td><input type="text" size="3" name="ege_hist" id="ege_hist" disabled></td></tr>
        <tr><td><input type="checkbox" name="ege_phys_on" id="ege_phys_on" onclick="if(this.checked) document.getElementById('ege_phys').removeAttribute('disabled'); else document.getElementById('ege_phys').setAttribute('disabled', 1);"> ������:</td><td><input type="text" size="3" name="ege_phys" id="ege_phys" disabled></td></tr>
        <tr><td><input type="checkbox" name="ege_biol_on" id="ege_biol_on" onclick="if(this.checked) document.getElementById('ege_biol').removeAttribute('disabled'); else document.getElementById('ege_biol').setAttribute('disabled', 1);"> ��������:</td><td><input type="text" size="3" name="ege_biol" id="ege_biol" disabled></td></tr>
        <!--<tr><td><input type="checkbox" name="ege_chem_on" id="ege_chem_on" onclick="if(this.checked) document.getElementById('ege_chem').removeAttribute('disabled'); else document.getElementById('ege_chem').setAttribute('disabled', 1);"> �����:</td><td><input type="text" size="3" name="ege_chem" id="ege_chem" disabled></td></tr>
        <tr><td><input type="checkbox" name="ege_geo_on" id="ege_geo_on" onclick="if(this.checked) document.getElementById('ege_geo').removeAttribute('disabled'); else document.getElementById('ege_geo').setAttribute('disabled', 1);"> ���������:</td><td><input type="text" size="3" name="ege_geo" id="ege_geo" disabled></td></tr>-->
        <tr><td><input type="checkbox" name="ege_lang_on" id="ege_lang_on" onclick="if(this.checked) document.getElementById('ege_lang').removeAttribute('disabled'); else document.getElementById('ege_lang').setAttribute('disabled', 1);"> ����������� ����:</td><td><input type="text" size="3" name="ege_lang" id="ege_lang" disabled></td></tr>
        <tr><td><input type="checkbox" name="ege_soc_on" id="ege_soc_on" onclick="if(this.checked) document.getElementById('ege_soc').removeAttribute('disabled'); else document.getElementById('ege_soc').setAttribute('disabled', 1);"> ��������������:</td><td><input type="text" size="3" name="ege_soc" id="ege_soc" disabled></td></tr>
        </table><br />
        <input type="button" value="���������" onclick="document.getElementById('finalrek').style.display = 'block'; process();">

        <div id="finalrek" style="display:none;">
        <div id="divrekoms"></div>
        </div>
        </form>
        </p></div>
        <div id="interests" style="display:none;"><p>
        <h3>����������� ������:</h3>
        <ul>
        <?
        foreach ($MODULE_OUTPUT["schools"] as $sch) {
            ?><li><a class="dash sch" onclick="show_choice(this, '<?=$sch["code"]?>')"><?=$sch["name"]?></a></li><?
        }
        ?>
        </ul>
        </p></div>
        <div id="rekom">
        <?
        foreach ($MODULE_OUTPUT["high_spec"] as $key => $speclist) {
            ?><div name="rek" id="<?=$key?>" style="display:none;"><h3>�����������:</h3>
        <? foreach ($speclist as $spec) { ?>
            <a href="/speciality/<?=$spec["code"]?>/"><?=$spec["name"]?></a><br />
        <? } ?></div><? }
        ?></div><div id="flagdiv"></div><?
    }
    break;

    case "search_subjects": { ?>
        <div id="search_subjects">
            <form action="/subjects/" method="get">
                <fieldset>
                    <legend>����������</legend>
                    <p>
                        ����������:
                        <input name="subject_name" id="subject_name" placeholder="����������, ������� �������� ����������" title="����������, ������� �������� ����������" type="text" size="43" />
                        <input type="submit" value="�����" class="button" onclick="if(document.getElementById('subject_name').value=='') { document.getElementById('btc').appendChild(document.getElementById('subject_name').tooltip); return false; }"/> <a href="/library/ebooks/e-lib-sys-nsau/poisk-resursov-ebs/" target="_blank">����������� �����</a>
                    </p>
                </fieldset>
            </form>
        </div>
<?php
    }
    break;

    case "rekom": {
        header('Content-Type: text/xml; charset=windows-1251');
        echo '<?xml version="1.0" encoding="cp1251" standalone="yes"?>';
        echo '<response>';
        foreach ($MODULE_OUTPUT["rekoms"] as $key => $rekom) {
            $rekom = explode(" - ", $rekom);
            echo '<rekom>&lt;a href="/speciality/'.$rekom[0].'/"&gt;'.$rekom[1].'&lt;/a&gt;</rekom>';
        }
        if(!empty($MODULE_OUTPUT["error"])) {?>
            <p><?=$MODULE_OUTPUT["error"]?></p>
        <?}
        echo '</response>';
    }
    break;

    case "subjects": {
        if(!empty($MODULE_OUTPUT["subject"]))
        {
            $man = $MODULE_OUTPUT["subject"];
            ?>
            <h2><?php echo $MODULE_OUTPUT["subject"]["name"] ?></h2><br>
            <?php
            if (isset($MODULE_OUTPUT["subject"]["files"]))
            {
                ?><ul><?php
                foreach ($MODULE_OUTPUT["subject"]["files"] as $file)
                {
                ?>
                <li><a href="/file/<?php echo $file["id"]; ?>/" title="<?php echo $file["descr"]; ?>"><?php echo (isset($file["descr"]) && $file["descr"] ) ? $file["descr"] : $file["name"]; ?></a> <?php if($file["user_id"] && 0) { ?><a href="/people/<?php echo $file["user_id"]; ?>/"><?php } ?><?php if (0) echo $file["user_name"]; ?><?php if($file["user_id"] && 0) { ?></a><?php } ?>
                <?php if ($file["author"]) echo '&nbsp;�����: <strong>' .$file['author']. '</strong>.'; if ($file["year"]) echo '&nbsp;��� �������: <strong>' .$file['year']. '</strong>';?>
                <?php if ($file["volume"]) echo '&nbsp;�����: <strong>' .$file['volume']. '</strong> ���.'; if ($file["edition"]) echo '&nbsp;�����: <strong>' .$file['edition']. '</strong> ���.';?>
                <?php if ($file["place"]) echo '&nbsp;����� �������: <strong>' .$file['place'].'</strong>';?>
                <?php
                }
            ?>
            </ul>
            <?php
            }
        }
        else
        {
            ?>
            <form action="/subjects/" method="get">
            <p>
            ����������:
            <input name="subject_name" type="text" size="35" value="<?php echo isset($_GET["name"]) ? $_GET["name"] : "" ?>"/> <input type="submit" value="�����" class="button"/>
            </p>
            </form>
            <?
            if(!empty($MODULE_OUTPUT["subjects"]))
            {
                ?>
                <ul>
                <?php
                foreach($MODULE_OUTPUT["subjects"] as $subj)
                {
                    ?>
                    <li><?php if($subj["num_files"] > 0) { ?><a href="/subjects/<?=$subj["id"]?>/"><?php } ?><?=$subj["name"]?> <?php if($subj["num_files"] > 0) { ?></a><?php } ?></li>
                    <?php
                }
                ?>
                </ul>
                <?php
            }
            else
            {
                if(isset($_GET["name"]))
                {
                ?>
                <p>�� ������� ��������� � ������ ����������� ������.</p>
                <?php
                }
                else
                {
                ?>
                <p>������� � ���� ������ ������ � ����������.</p>
                <?php
                }
            };
        };
    }
    break;

    case "search_group": { ?>
        <div id="search_group">
            <form action="/student/timetable/groups/" method="get">
                <fieldset>
                    <legend>������</legend>
                    <p>
                        ������:
                        <input name="group" placeholder="����������, ������� �������� ������" title="����������, ������� �������� ������" id="group_name" type="text" size="43" />
                        <input type="submit" value="�����" class="button" onclick="if(document.getElementById('group_name').value=='') { document.getElementById('btc').appendChild(document.getElementById('group_name').tooltip); Locate; return false; }"/>
                    </p>
                </fildset>
            </form>
        </div>
<?php
    }
    break;

    case "groups": { ?>
        <script>jQuery(document).ready(function($) {InputCalendar();});</script>
<?php   $days = array("�����������", "�������", "�����", "�������", "�������", "�������");
        $timepars = array("09:00 - 10:30","10:40 - 12:10","12:50 - 14:20","14:30 - 16:00","16:10 - 17:40","17:45 - 19:15","19:20 - 20:50");
        $months = array("������", "�������", "�����", "������", "���", "����", "����", "�������", "��������", "�������", "������","�������");
        if(isset($MODULE_OUTPUT["edit_timetable"])) {
            $data = $MODULE_OUTPUT["edit_timetable"];   ?>
            <div class="EEngine EE_add_timetable">
                <form method="post">
                    <fieldset>
                        <legend>�������������� ������:</legend>
                        <div class="wide">
                            <input type="hidden" name="id" value="<?=$data["id"]?>" />
                            <div class="notes_column">
                                <div class="form-notes title-notes">
                                    <label>���� ������:</label>
                                </div>
                                <div class="center-notes">
                                    <div class="cn">
                                        <div class="form-notes">
                                            <select name="dow">
<?php       for($i=0; $i<6; $i++) {
                if($data["dow"] == $i+1) { ?>
                                                <option value="<?=$i+1?>" selected="selected"><?=$days[$i]?></option>
<?php           } else { ?>
                                                <option value="<?=$i+1?>"><?=$days[$i]?></option>
<?php           }
            } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="notes_column">
                                <div class="form-notes title-notes">
                                    <label>����� ����:</label>
                                </div>
                                <div class="center-notes">
                                    <div class="cn">
                                        <div class="form-notes">
                                            <select name="num">
                                                <option value="1" selected="selected">1</option>
<?php       for($i=1; $i<8; $i++) {
                if($data["num"] == $i) { ?>
                                                <option value="<?=$i?>" selected="selected"><?=$i?></option>
<?php           } else { ?>
                                                <option value="<?=$i?>"><?=$i?></option>
<?php           }
            } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wide">
                            <div class="notes_column">
                                <div class="wide">
                                    <label>������/�������� ��� ������ ������:</label>
                                </div>
                                <div class="wide">
                                    <input type="radio" name="parity" <?=($data["parity"] == "both" ? 'checked="checked" ' : '')?>value="0" />������ ������
                                </div>
                                <div class="wide">
                                    <input type="radio" name="parity" <?=($data["parity"] == "odd" ? 'checked="checked" ' : '')?>value="2" />������
                                </div>
                                <div class="wide">
                                    <input type="radio" name="parity" <?=($data["parity"] == "even" ? 'checked="checked" ' : '')?>value="1" />��������
                                </div>
                            </div>
                            <div class="notes_column">
                                <div class="wide">
                                    <label>����� ���������, ���� ����</label>
                                </div>
                                <div class="wide">
                                    <input type="radio" name="subgroup" <?=(empty($data["subgroup"]) ? 'checked="checked" ' : '')?>value="0" />���
                                </div>
                                <div class="wide">
                                    <input type="radio" name="subgroup" <?=(!empty($data["subgroup"]) && $data["subgroup"] == "�" ? 'checked="checked" ' : '')?>value="�" />�
                                </div>
                                <div class="wide">
                                    <input type="radio" name="subgroup" <?=(!empty($data["subgroup"]) && $data["subgroup"] == "�" ? 'checked="checked" ' : '')?>value="�" />�
                                </div>
                            </div>
                        </div>
                        <div class="wide">
                            <div class="notes_column">
                                <div class="wide">
                                    <label>����������:</label>
                                </div>
                                <div class="wide">
                                    <select name="subject">
<?php       foreach($MODULE_OUTPUT["dep_subjects"] as $dep_subj) { ?>
                                        <optgroup label="<?=$dep_subj['dep_name']?>">
<?php           foreach($dep_subj['subjects'] as $subject) { ?>
                                            <option value="<?=$subject["id"]?>"<?=($subject["id"] == $data["subject_id"] ? " selected='selected'" : "")?>><?=$subject["name"]?></option>
<?php           } ?>
                                        </optgroup>
<?php       } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="notes_column">
                                <div class="wide">
                                    <label>���������:</label>
                                </div>
                                <div class="wide">
                                    <select name="auditorium">
<?php       $id=0;
            foreach($MODULE_OUTPUT["auditorium"] as $auditorium) {
                if(!$id) {
                    $id++; ?>
                                        <optgroup label="<?=$auditorium["building_name"]?>">
<?php           }
                if($id != $auditorium["building_id"]) { ?>
                                        </optgroup>
                                        <optgroup label="<?=$auditorium["building_name"]?>">
<?php               $id++;
                }
                if($data["auditorium_id"] == $auditorium["id"]) { ?>
                                            <option selected="selected" value="<?=$auditorium["id"]?>"><?=$auditorium["name"]?></option>
<?php
                } else { ?>
                                            <option value="<?=$auditorium["id"]?>"><?=$auditorium["name"]?></option>
<?php           }
            } ?>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="wide">
                            <div class="form-notes">
                                <label>��� �������:</label>&nbsp;
                                <input type="radio" name="lesson_view" <?=($data["lesson_view"] == "1" ? 'checked="checked" ' : '')?>value="1" />������&nbsp;&nbsp;
                                <input type="radio" name="lesson_view" <?=($data["lesson_view"] == "0" ? 'checked="checked" ' : '')?>value="0" />��������
                            </div>
                        </div>
                        <div class="wide">
                            <input type="submit" value="���������" class="button" />
                        </div>
                    </fieldset>
                </form>
            </div>
<?php
        } elseif(!empty($MODULE_OUTPUT["group"])) {
                                            // echo "<pre>";
                        // print_r($MODULE_OUTPUT["files_list"]);
                        // echo "</pre>";
            $group = $MODULE_OUTPUT["group"];
            $days = array("�����������", "�������", "�����", "�������", "�������", "�������");
            $timepars = array("09:00 - 10:20","10:30 - 11:50","12:30 - 13:50","14:00 - 15:20","15:30 - 16:50","17:00 - 18:20","18:30 - 19:50");
            $months = array("������", "�������", "�����", "������", "���", "����", "����", "�������", "��������", "�������", "������","�������");
            $week = ($MODULE_OUTPUT["parity"] == "even") ? "׸����" : "��������";
            $now = $MODULE_OUTPUT["now"];
            $url_parity = "";
            $url_subgroup = "";
            if(isset($_GET["subgr"]))
                $url_subgroup = "subgr=".$_GET["subgr"]."&amp;";
            if(isset($_GET["parity"]))
                $url_parity = "parity=".$_GET["parity"]."&amp;"; ?>













































































































































































































































































































































































<?php       if($MODULE_OUTPUT["edit_acces"]) { ?>
                <div class="EEngine EE_add_timetable">
                    <!--form method="post">
                        <fieldset>
                            <legend>�������� ������ � ����������:</legend>
                            <div class="wide">
                                <div class="notes_column">
                                    <div class="form-notes title-notes">
                                        <label>���� ������:</label>
                                    </div>
                                    <div class="center-notes">
                                        <div class="cn">
                                            <div class="form-notes">
                                                <select name="dow">
<?php           /*for($i = 0; $i < 6; $i++) { ?>
                                                    <option value="<?=$i+1?>"><?=$days[$i]?></option>
<?php           } */?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="notes_column">
                                    <div class="form-notes title-notes">
                                        <label>����� ����:</label>
                                    </div>
                                    <div class="center-notes">
                                        <div class="cn">
                                            <div class="form-notes">
                                                <select name="num">
                                                    <option value="1" selected="selected">1</option>
<?php          /* for($i=1; $i<8; $i++) { ?>
                                                    <option value="<?=$i?>"><?=$i?></option>
<?php           } */?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wide">
                                <div class="notes_column">
                                    <div class="wide">
                                        <label>������/�������� ��� ������ ������:</label>
                                    </div>
                                    <div class="wide">
                                        <input type="radio" name="parity" checked="checked" value="0" />������ ������
                                    </div>
                                    <div class="wide">
                                        <input type="radio" name="parity" value="2" />������
                                    </div>
                                    <div class="wide">
                                        <input type="radio" name="parity" value="1" />��������
                                    </div>
                                </div>
                                <div class="notes_column">
                                    <div class="wide">
                                        <label>����� ���������, ���� ����</label>
                                    </div>
                                    <div class="wide">
                                        <input type="radio" name="subgroup" checked="checked" value="0" />���
                                    </div>
                                    <div class="wide">
                                        <input type="radio" name="subgroup" value="�" />�
                                    </div>
                                    <div class="wide">
                                        <input type="radio" name="subgroup" value="�" />�
                                    </div>
                                </div>
                                <div class="notes_column">
                                    <br />
                                    <div class="wide">
                                        <label>���� ��������</label>
                                    </div>
                                    C:
                                    <input name='date_from' type="text" class="date_time"  value="<?=date('Y-m-d')?>" />
                                    ��:
                                    <input name='date_to' type="text" class="date_time"  value="<?=date('Y-m-d')?>" />
                                    <br />
                                </div>
                            </div>
                            <div class="wide">
                                <div class="form-notes">
                                    <label>��� �������:</label>&nbsp;
                                    <input type="radio" name="lesson_view" value="1" />������&nbsp;&nbsp;
                                    <input type="radio" name="lesson_view" checked="checked" value="0" />��������
                                </div>
                            </div>
                            <div class="wide">
                                <div class="notes_column">
                                    <div class="wide">
                                        <label>����������:</label>
                                    </div>
                                    <div class="wide">
                                        <select name="subject">
<?php   /*  foreach($MODULE_OUTPUT["dep_subjects"] as $dep_subj) { ?>
                                        <optgroup label="<?=$dep_subj['dep_name']?>">
<?php           foreach($dep_subj['subjects'] as $subject) { ?>
                                            <option value="<?=$subject["id"]?>"><?=$subject["name"]?></option>
<?php           } ?>
                                        </optgroup>
<?php       }*/ ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="notes_column">
                                    <div class="wide">
                                        <label>���������:</label>
                                    </div>
                                    <div class="wide">
                                        <select name="auditorium">
<?php           /*$id=0;
                foreach($MODULE_OUTPUT["auditorium"] as $auditorium) {
                    if(!$id) {
                        $id++; ?>
                                            <optgroup label="<?=$auditorium["building_name"]?>">
<?php               }
                    if($id != $auditorium["building_id"]) { ?>
                                            </optgroup>
                                            <optgroup label="<?=$auditorium["building_name"]?>">
<?php                   $id++;
                    } ?>
                                                <option value="<?=$auditorium["id"]?>"><?=$auditorium["name"]?></option>
<?php           } */?>
                                            </optgroup>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="wide">
                                <input type="submit" value="�������� ������" class="button" />
                            </div>
                        </fieldset>
                    </form-->
















                </div>
<?php       }
        } else { ?>
            <form action="/student/timetable/groups/" method="get">
                <p>
                    ������:
                    <input name="group" type="text" size="35" value="<?php echo isset($_GET["group"]) ? $_GET["group"] : "" ?>" />
                    <input type="submit" value="�����" class="button" />
                </p>
            </form>
<?php       if(!empty($MODULE_OUTPUT["groups"])) { ?>
            <ul>
<?php           foreach($MODULE_OUTPUT["groups"] as $group) { ?>
                <li><a href="/student/timetable/groups/<?=$group["id"]?>/">������ <?=$group["name"]?>, <?=$group["faculty"]?></a></li>
<?php           } ?>
            </ul>
<?php       } else {
                if(isset($_GET["group"])) { ?>
            <p>�� ������� ����� � ������ ����������� ������.</p>
<?php           } else { ?>
            <p>������� � ���� ������ �������� ������.</p>
<?php           }
            };
        };
    }
    break;

    case "timetable_groups": 
    {
        if(!isset($MODULE_OUTPUT["groups_mode"])) 
        {
            ?>
        
            <style type="text/css">
                dd {
                    margin: 0 0 0 0px; 
                } 
                #add_group {
                    padding-top: 0px;
                }
            </style>

            <div class="row"> 
                <!-- ����� ���������� ������ -->
                <div class="col-sm-5">
                    <form method="post" name="add_group" id="add_group">
                        <div class="panel panel-nsau"> 
                            <div class="panel-heading">���������� ������</div> 
                            <div class="panel-body">
                                    <dd id="new_group_name"><input type="text" name="group_name" class="form-control" placeholder="��� ������" id="new_group">
                                        <a class="btn btn-nsau btn-sm ajax_edit_group" href="">�������������</a>
                                    </dd>
                                    <dd id="group_faculties">
                                        <select name="faculties" size="1" class="form-control" id="napr">
                                            <option  value="0" selected="selected" id="option_prev">����������� ����������</option>
                                            <?

                                            foreach($MODULE_OUTPUT["faculties"] as $faculty) 
                                            { 

                                                ?>
                                                <optgroup label="<?=$faculty["fac_name"]?>" id="<?=isset($faculty["fac_pos"]) ? "opt_".$faculty["fac_pos"] : "opt_666" ?>">
                                                    <?
                                                    foreach($faculty["spec"] as $spec) 
                                                    { 
                                                        ?>
                                                        <option value="<?=$spec["id_faculty"]?>|<?=$spec["code"]?>"><?=$spec["code"]?>-<?=$spec["name"]?></option>
                                                        <?
                                                    } 
                                                    ?>
                                                </optgroup>
                                                <?
                                            } 
                                            ?>
                                        </select>
                                    </dd>
                                    <dd id="form_education" >
                                        <select name="form_edu" class="form-control">
                                            <option value="0" selected="selected">����� ��������</option>
                                            <?      
                                            $form_edu = array(1=>"�����", 2=>"�������", 3=>"����-�������");
                                            foreach($form_edu as $form_code => $form_edu)
                                            {
                                                ?>
                                                <option value="<?=$form_code?>"><?=$form_edu?></option>
                                                <?
                                            } ?>
                                        </select>
                                    </dd>
                                    <dd id="group_year">
                                        <select name="year" size="1" class="form-control" id="group_course">
                                            <option value="1" selected="selected">������ ����</option>
                                            <option value="2">������ ����</option>
                                            <option value="3">������ ����</option>
                                            <option value="4">�������� ����</option>
                                            <option value="5">����� ����</option>
                                            <option value="6">������ ����</option>
                                        </select>
                                    </dd>
                                    <dd id="group_qualification">
                                        <select name="qualif" class="form-control">
                                            <?       
                                            $qualif = array(68=>"������������", 65=>"�����������", 62=>"�����������", 51=>"������� �����������", ""=>"�����������");
                                            foreach($qualif as $qual_code => $qualif) 
                                            {
                                                if($qual_code == 62) 
                                                { 
                                                    ?>
                                                    <option value="<?=$qual_code?>" selected="selected"><?=$qualif?></option>
                                                    <?
                                                } 
                                                elseif ($qualif == "�����������") 
                                                {
                                                    ?>
                                                    <option value="<?=$qual_code?>" id="asp"><?=$qualif?></option>
                                                    <?
                                                }
                                                else
                                                { 
                                                    ?>
                                                    <option value="<?=$qual_code?>"><?=$qualif?></option>
                                                    <?
                                                }
                                            } 
                                            ?>
                                        </select>
                                    </dd>
                            <input type="submit" value="��������" class="button" />
                            </div> 
                        </div>
                    </form> 
                    <?
                    if ($MODULE_OUTPUT["add_group_status"]=="ok") 
                    {
                        ?>
                        <div class="alert alert-success">������ ���������</div>
                        <?
                    }
                    elseif(!empty($MODULE_OUTPUT["messages"]["bad"])) 
                    {
                            foreach($MODULE_OUTPUT["messages"]["bad"] as $msg) 
                            {
                                ?>
                                <div class="alert alert-danger">
                                <?=$MODULE_MESSAGES[$msg]?></div>
                                <?
                            }
                    }
                    ?>
                </div>
                <!-- ����� ���� ����� -->
                <div class="col-sm-7">
                    <div class="row">
                        <div id="Accordian" class="panel-group">
                            <?
                            foreach($MODULE_OUTPUT["timetable_groups"] as $f_id => $fac) 
                            {
                                ?>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#Accordian" href="#<?=$f_id?>">
                                                <h5><?=isset($fac[1][0]["faculty_name"]) ? $fac[1][0]["faculty_name"] : "��������� �� ������"?></h5>
                                            </a>
                                        </h4>
                                    </div>
                                    <div class="panel-collapse collapse out" id="<?=$f_id?>">
                                        <div class="panel-body">



                                            <table class="table table-striped">
                                                <thead>
                                                    <tr><th>����� �����</th><th>������� �����</th><th>����-������� �����</th><th>��� �����</th></tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                        <?
                                                        foreach($fac as $form => $groups) 
                                                        {
                                                            foreach($groups as $group) 
                                                            {
                                                            if ($form==1) 
                                                            {
                                                                ?>
                                                                <a id="group_link" href="edit/<?=$group["id"]?>/" style="color: <?=($group["hidden"] ? "gray" : "")?>" ><?=$group["name"]?></a> <a class="btn btn-nsau delete_group_button btn-sm" href="delete/<?=$group["id"]?>/" onclick="return confirm('�� �������, ��� ������ ������� ��������� �������?');"}><span class="glyphicon glyphicon-trash"></span></a>
                                                                <!-- [<a href="/student/timetable/groups/<?=$group["id"]?>/">������� � ���������� ������</a>] -->
                                                                <br />
                                                                <?
                                                            }
                                                            }
                                                        }
                                                        ?>
                                                        </td>
                                                        <td>
                                                        <?
                                                        foreach($fac as $form => $groups) 
                                                        {
                                                            foreach($groups as $group) 
                                                            {
                                                            if ($form==2) 
                                                            {
                                                                ?>
                                                               <a id="group_link" href="edit/<?=$group["id"]?>/" style="color: <?=($group["hidden"] ? "gray" : "")?>" ><?=$group["name"]?></a> <a class="btn btn-nsau delete_group_button btn-sm" href="delete/<?=$group["id"]?>/" onclick="return confirm('�� �������, ��� ������ ������� ��������� �������?');"}><span class="glyphicon glyphicon-trash"></span></a>
                                                                <!-- [<a href="/student/timetable/groups/<?=$group["id"]?>/">������� � ���������� ������</a>] -->
                                                                <br />
                                                                <?
                                                            }
                                                            }
                                                        }
                                                        ?>
                                                        </td>
                                                        <td>
                                                        <?
                                                        foreach($fac as $form => $groups) 
                                                        {
                                                            foreach($groups as $group) 
                                                            {
                                                            if ($form==3) 
                                                            {
                                                                ?>
                                                                <a id="group_link" href="edit/<?=$group["id"]?>/" style="color: <?=($group["hidden"] ? "gray" : "")?>" ><?=$group["name"]?></a> <a class="btn btn-nsau delete_group_button btn-sm" href="delete/<?=$group["id"]?>/" onclick="return confirm('�� �������, ��� ������ ������� ��������� �������?');"}><span class="glyphicon glyphicon-trash"></span></a>
                                                                <!-- [<a href="/student/timetable/groups/<?=$group["id"]?>/">������� � ���������� ������</a>] -->
                                                                <br />
                                                                <?
                                                            }
                                                            }
                                                        }
                                                        ?>
                                                        </td>
                                                        <td>
                                                        <?
                                                        foreach($fac as $form => $groups) 
                                                        {
                                                            foreach($groups as $group) 
                                                            {
                                                            if ($form!=1 && $form!=2 && $form!=3) 
                                                            {
                                                                ?>
                                                                <a id="group_link" href="edit/<?=$group["id"]?>/" style="color: <?=($group["hidden"] ? "gray" : "")?>" ><?=$group["name"]?></a> <a class="btn btn-nsau delete_group_button btn-sm" href="delete/<?=$group["id"]?>/" onclick="return confirm('�� �������, ��� ������ ������� ��������� �������?');"}><span class="glyphicon glyphicon-trash"></span></a>
                                                                <!-- [<a href="/student/timetable/groups/<?=$group["id"]?>/">������� � ���������� ������</a>] -->
                                                                <br />
                                                                <?
                                                            }
                                                            }
                                                        }
                                                        ?>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <?                          
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <?
        } 
        else 
        {
            switch($MODULE_OUTPUT["groups_mode"]) 
            {
                case "edit": 
                {
                    foreach($MODULE_OUTPUT["edit_group"] as $group) 
                    { 
                        ?>
                        <form name="edit_group" method="post" id="edit_group">
                            <fieldset>
                                <legend>�������������� ���������� � ������ <?=$group["name"]?> (<?=$group["faculty_name"]?>)</legend>
                                <div class="row"> 
                                    <div class="col-sm-4">

                                        <div id="group_name">                                        
                                            <div class="input-group"> 
                                                <span class="input-group-addon" id="basic-addon1" style="border-bottom: 0px!important;">�������� ������</span>
                                                <input type="text" class="form-control" name="group_name" value="<?=$group["name"]?>" style="border-bottom: 0px!important;"/>
                                            </div>

                                        </div>
                                        <div id="group_year">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1">����</span>
                                                <select name="year" size="1" class="form-control">
                                                    <?
                                                    $year_arry = array('������ ����','������ ����','������ ����','�������� ����','����� ����','������ ����');
                                                    for($i = 1; $i <= 6; $i++)
                                                    {
                                                        if($i == $group["year"])
                                                        { 
                                                            ?>
                                                            <option value="<?=$i?>" selected="selected"><?=$year_arry[$i-1]?></option>
                                                            <?
                                                        } 
                                                        else 
                                                        { 
                                                            ?>
                                                            <option value="<?=$i?>"><?=$year_arry[$i-1]?></option>
                                                            <?
                                                        }
                                                    }?>
                                                </select>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="checkbox-nsau"> 
                                            <input type="checkbox" id="check1" name="hide" value="1" <?=($group["hidden"] ? "checked" : "")?>>
                                            <label for="check1">������</label> 
                                        </div>
                                    </div>


                                    <div class="col-sm-8">
                                        <div id="group_faculties">
                                            <div class="input-group"> 
                                                <span class="input-group-addon" id="basic-addon1" style="border-bottom: 0px!important;">�������������, � �������� ��������� ������</span>
                                                <select name="faculty" style="max-width: 100%;border-bottom: 0px!important" class="form-control">
                                                    <option value='0'>�� ������� �������������</a>
                                                    <?
                                                    foreach($MODULE_OUTPUT["faculties"] as $faculty) 
                                                    { 
                                                        ?>
                                                        <optgroup label="<?=$faculty["fac_name"]?>">
                                                            <?
                                                            foreach($faculty["spec"] as $spec) 
                                                            {
                                                                if(($spec["code"] == $group["spec_code"]) && ($spec["id_faculty"] == $group["id_faculty"])) 
                                                                {
                                                                    $x=1; 
                                                                    ?>
                                                                    <option value="<?=$spec["id_faculty"]?>|<?=$spec["code"]?>" selected="selected"><?=$spec["code"]?>-<?=$spec["name"]?></option>
                                                                    <?                   
                                                                }
                                                                else 
                                                                { 
                                                                    ?>
                                                                    <option value="<?=$spec["id_faculty"]?>|<?=$spec["code"]?>"><?=$spec["code"]?>-<?=$spec["name"]?></option>
                                                                    <?                   
                                                                }
                                                            } 
                                                            if((empty($group["spec_code"]) || ($spec["code"] != $group["spec_code"]) && !$x) && ($spec["id_faculty"] == $group["id_faculty"])) 
                                                            {
                                                                ?>
                                                                <option value="<?=$spec["id_faculty"]?>|<?=$group["spec_code"]?>" selected="selected">�� ������� �������������</a>
                                                                <?
                                                            }
                                                            ?>
                                                        </optgroup>
                                                        <?
                                                    } ?>
                                                </select>
                                            </div>
                                        </div>  
                                        <div id="group_qualification">
                                            <div class="input-group"> 
                                                <span class="input-group-addon" id="basic-addon1" style="border-bottom: 0px!important;">������� ���������� ������</span>
                                                <select name="qualif" style="border-bottom: 0px!important" class="form-control">
                                                    <?php
                                                    $qualif = array(68=>"������������", 65=>"�����������", 62=>"�����������", 51=>"������� �����������", ""=>"�����������");
                                                    foreach($qualif as $qual_code => $qualif) 
                                                    {
                                                        if($qual_code == $group["qualification"]) 
                                                        { 
                                                            ?>
                                                            <option value="<?=$qual_code?>" selected="selected"><?=$qualif?></option>
                                                            <?
                                                        } 
                                                        else 
                                                        { 
                                                            ?>
                                                            <option value="<?=$qual_code?>"><?=$qualif?></option>
                                                            <?
                                                        }
                                                    }?>
                                                </select>
                                            </div>
                                            <div id="form_educat">
                                                <div class="input-group"> 
                                                    <span class="input-group-addon" id="basic-addon1">�������� ����� ��������</span>
                                                    <select name="form_edu" class="form-control">
                                                        <?
                                                        if($group["form_education"]==!NULL) 
                                                        {
                                                            $form_edu = array(  1=>"�����", 2=>"�������", 3=>"����-�������" );
                                                            foreach($form_edu as $form_code => $form_edu) 
                                                            {
                                                                if($form_code == $group["form_education"]) 
                                                                {
                                                                    ?>
                                                                    <option value="<?=$form_code?>" selected="selected"><?=$form_edu?></option>
                                                                    <?
                                                                } 
                                                                else 
                                                                { 
                                                                    ?>
                                                                    <option value="<?=$form_code?>"><?=$form_edu?></option>
                                                                    <? 
                                                                }
                                                            }
                                                        }

                                                        if($group["form_education"]==NULL) 
                                                        {
                                                            $form_edu = array( 1=>"�����", 2=>"�������" , 3=>"����-�������" , NULL=>" ");
                                                            foreach($form_edu as $form_code => $form_edu) 
                                                            {
                                                                if($form_code == $group["form_education"]) 
                                                                {
                                                                    ?>
                                                                    <option value="<?=$form_code?>" selected="selected"><?=$form_edu?></option>
                                                                    <?
                                                                } 
                                                                else 
                                                                { 
                                                                    ?>
                                                                    <option value="<?=$form_code?>"><?=$form_edu?></option>
                                                                    <? 
                                                                }
                                                            }
                                                        }?>
                                                    </select>
                                            </div>  



                                            <div id="form_educat">
                                                <div class="input-group"> 
                                                    <span class="input-group-addon" id="basic-addon1">�������� �������</span>
                                                    <select name="profile" class="form-control">
                                                        
                                                        <?
                                                            if ($MODULE_OUTPUT['profiles']) 
                                                            {
                                                                ?><option value="">�������� �������</option><?
                                                                foreach($MODULE_OUTPUT['profiles'] as $k => $v) 
                                                                {
                                                                    if ($group['profile'] == $v['id']) 
                                                                    {
                                                                        ?>
                                                                        <option value="<?=$v['id']?>" selected="selected"><?=$v['name']?></option>
                                                                        <?
                                                                    }
                                                                    else
                                                                    {
                                                                        ?>
                                                                        <option value="<?=$v['id']?>"><?=$v['name']?></option>
                                                                        <?
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                ?><option value="">�������� ���</option><?
                                                            }
                                                        ?>
                                                    </select>
                                            </div> 
                                        </div>
                                    </div>


                               
                              
                            </fieldset>
                                <input type="hidden" value="edit" name="mode" />
                                <input type="hidden" value="<?=$group["id"]?>" name="id" />
                                <input type="submit" value="���������" class="button btn btn-nsau btn-md" />
                        </form>
                        <?
                    }
                    break;
                }
            }
        }
        break;
    }
    case "timetable_faculties":
    {
        if(!isset($MODULE_OUTPUT["faculty_mode"]))
        {
            ?>
            <h2>������ �����������:</h2>
            <ul>
            <?php
        foreach($MODULE_OUTPUT["timetable_faculties"] as $faculty)
        {
            ?>
            <li><a href="edit/<?=$faculty["id"]?>/" title="<?=$faculty["comment"]?>"><?=$faculty["name"]?></a> [<a href="delete/<?=$faculty["id"]?>/" onclick="return confirm('�� �������, ��� ������ ������� ��������� �������?');"}>�������</a>]</li>
            <?php
            if(isset($faculty["subfaculties"]))
            {
                ?>
                <ul>
                <?php
                foreach($faculty["subfaculties"] as $subfaculty)
                {
                    ?>
                    <li><a href="edit/<?=$subfaculty["id"]?>/"><?=$subfaculty["name"]?></a> [<a href="delete/<?=$subfaculty["id"]?>/" onclick="return confirm('�� �������, ��� ������ ������� ��������� �������?');"}>�������</a>]</li>
                    <?php
                }
                ?>
                </ul>
                <?php
            }
        }
        ?>
        </ul>
        <br />
        <h2>�������� ��������� ��� ��������:</h3>
        <form method="post" name="add_faculty">
        <h3>�������� ���������� ��� ���������:</h3>
        <input type="text" name="faculty_name" class="input_long" />
        <h3>������� � ������:</h3>
        <input type="text" name="pos" class="input" />
        <h3>�������� ��������:</h3>
        <input type="text" name="faculty_shortname" />
        <h3>��� ���������:</h3>
        <input type="text" name="foundation_year" />
        <h3>��������:</h3>
        <input type="text" name="comment" class="input_long" />
        <h3>�������� �������� (���� ����), � �������� ����� ���������� ������ ���������:</h3>
        <select name="pid" size="1">
        <option value="0" selected="selected">�������� ��������:</option>
        <?php
        foreach($MODULE_OUTPUT["timetable_faculties"] as $faculty)
        {
            ?>
            <option value="<?=$faculty["id"]?>"><?=$faculty["name"]?></option>
            <?php
        }
        ?>
        </select>
        <h3>����:</h3>
        <input type="text" name="faculty_link" /> <br />
        <input type="checkbox" name="is_active" checked="checked"> ���� �����������
        <br />
        <input type="submit" value="��������" class="button" />
        </form>
        <?php
        }
        else
        {
            switch($MODULE_OUTPUT["faculty_mode"])
            {
                case "edit":
                {
                    foreach($MODULE_OUTPUT["edit_faculty"] as $faculty)
                    {
                        ?>
                        <h2>�������������� ���������� � ���������� <?=$faculty["name"]?>:</h2>
                        <form name="edit_faculty" method="post">
                        <h3>�������� ����������:</h3>
                        <input type="text" name="faculty_name" value="<?=$faculty["name"]?>" class="input_long" />
                        <h3>��������� ��� ��������, � �������� ��������� ������ ���������:</h3>
                        <select name="pid" size="1">
                        <?php
                        if(!$faculty["pid"])
                        {
                            ?>
                            <option value="0" selected="selected">�������� ���������</option>
                            <?php
                        }
                        foreach($MODULE_OUTPUT["faculties"] as $faculties)
                        {

                            if($faculty["pid"] == $faculties["id"])
                            {?>
                                <option value="<?=$faculties["id"]?>" selected="selected"><?=$faculties["name"]?></option>
                                <?php
                            }
                            else
                            {   ?>
                                <option value="<?=$faculties["id"]?>"><?=$faculties["name"]?></option>
                                <?php
                            }
                        }
                        ?>
                        </select><br />
                        <h3>������� � ������:</h3>
                        <input type="text" name="pos" value="<?=$faculty["pos"]?>" />
                        <h3>���� ����������:</h3>
                        <input type="text" name="faculty_link" value="<?=$faculty["link"]?>" />
                        <h3>��� ���������:</h3>
                        <input type="text" name="foundation_year" value="<?=$faculty["foundation_year"]?>" />
                        <h3>��������:</h3>
                        <input type="text" name="comment" class="input_long" value="<?=$faculty["comment"]?>" /><br />
                        <input type="checkbox" name="is_active"<? if ($faculty["is_active"]) { ?>checked="checked"<? } ?>> ���� �����������
                        <input type="hidden" value="edit" name="mode" />
                        <input type="hidden" value="<?=$faculty["id"]?>" name="id" /><br />
                        <input type="submit" value="���������" class="button" />
                        </form>
                        <?php
                    }
                    break;
                }
            }
        }
        break;
    }


case "timetable_print":
    {
        $wdays_t = array("��","��","��","��","��","��", "��");
        if(empty($_GET["weeks"]))
            foreach($MODULE_OUTPUT["days"] as $ddid=>$d)
                $wdays[$ddid] = $wdays_t[$ddid];
        else $wdays = $wdays_t;
        $daysCol = count($wdays);

        $g_num = count($MODULE_OUTPUT["groups"]);
        $groups_width = 95;
        $col_width = floor($groups_width/$g_num);
        $oth = $groups_width-($col_width*$g_num);
        $weeks = array("odd", "even");
        $types = array("�","��","��");
        $subgroups = array('','(�)','(�)');
        $groups = $MODULE_OUTPUT["groups"];
        $total_rows = 0;
        $mode = array("main", "doubles", "triples");
        $xArr = end($MODULE_OUTPUT["groups"]);
        foreach($weeks as $wid=>$ww)
            foreach($MODULE_OUTPUT["groups"] as $gr_id=>$gr) {
                foreach($gr as $parts_id => $part) {
                    if($parts_id == "triples" || $parts_id == "doubles") {
                        foreach($part as $parts)
                            if($wid==$parts["week"]) {
                                $g[$gr_id][$parts_id][$wid ? 0 : 1][$parts["pair"]][$parts["day"]] = $parts;
                                $total_rows++;
                            }
                    }
                }
            }
        foreach($MODULE_OUTPUT["pairs"] as $gid => $days) {
            foreach($days as $i_p=>$p) {
                $total_rows += count($p);
            }
        }
    switch(count($MODULE_OUTPUT["pairs"])) {
        case 1:
            $fontSize = 14*4;
        break;
        case 2:
            $fontSize = 12*2;
        break;
        case 3: {
            $fontSize = 12*((empty($_GET["weeks"])) ? 1.5 : 2);

        }
        break;
        case 4: {
            $fontSize = 12*((empty($_GET["weeks"])) ? 1.5 : 2);

        }
        break;
        case 5:
            $fontSize = 12*((empty($_GET["weeks"])) ? 1.5 : 2);
        break;
    }

        ?>
<html>
<head>

<style>
    td {
    font: normal small-caps 20px/24px arial;
    vertical-align: middle;
    }
    tr.days {
         font-size: <?=$fontSize*((empty($_GET["weeks"])) ? 2 : 1)?>px;
    }
    td.groups {
         font-size: <?=$fontSize*((empty($_GET["weeks"])) ? 2 : 1)?>px;
    }
    td.approved {
        font-size: 30px;
        border: none;
    }
    td.title {
        text-align: center;
        font-size: 55px;
        border: none;
    }
    table tr.title {
        border: none;
    }
    tr.pairs {
        text-align: center;
    }
    table {
         border: 0px;
    }
    tr.info td{
        border: none;
        text-align: left;
    }
</style>
</head>
<body>
<?if(!empty($MODULE_OUTPUT["groups"])) {    ?>


    <table border="1px" width="<?=270?>%" height="100%">
        <tbody>
        <tr class="title">
            <td class="title" colspan="<?=(2+$daysCol)?>">
            <div class="title">
                <br><b>���������� ������� ������� <?=($_GET["weeks"]=="") ? "� 00.00.15 �� 00.00.15 ��. ���" : "����"?></b><br>
                    <b>
                        <?=$MODULE_OUTPUT["info"]["facName"]?>&nbsp
                        <?foreach($MODULE_OUTPUT["pairs"] as $grId => $grArr) { echo $MODULE_OUTPUT["groups"][$grId]["group_name"].($MODULE_OUTPUT["groups"][$grId]["group_name"]==$xArr["group_name"] ? "" : ", "); if($_GET["weeks"]!="") break;}?>
                        (<?=$MODULE_OUTPUT["info"]["specName"]?>)
                    </b>
             </div>
            </td>
            <td class="approved">
                    &nbsp;<b>���������</b><br>
                    &nbsp;<b>��������� �� ������� ������</b><br>
                    &nbsp;<b>_______________�.�. �����</b>
                    &nbsp;<b>�___�___________2015�.</b>
                    <p></p>
            </td>
        </tr>

        <tr class="days">
            <th width="2%"></th>
            <th width="1%"></th>
            <th width="1%"></th>
            <? foreach ($wdays as $day) { ?>
                <th width="16%"><strong><?=$day?></strong></th>
            <?} ?>
        </tr>


        <?foreach($MODULE_OUTPUT["pairs"] as $gid => $days) {$f=0;?>
            <?$pair=1; for($i=0;$i<14;$i++) {
                $parity = $i%2; ?>
            <?
                $fl=0;
                $rowspan_gName = 0;
                foreach($days as $i_p=>$p) {
                    $rowspan_gName += (count($g[$gid]["doubles"][0][$i_p])!=0 ? 1 : 0) + (count($g[$gid]["doubles"][1][$i_p])!=0 ? 1 : 0);
                    $rowspan_gName += (count($g[$gid]["triples"][0][$i_p])!=0 ? 1 : 0) + (count($g[$gid]["triples"][1][$i_p])!=0 ? 1 : 0);
                    $rowspan_gName += count($p);
                }
                if(!empty($_GET["weeks"]))
                    if($rowspan_gName>0)
                        $rowspan_gName += 7-count($MODULE_OUTPUT["pairs"][$gid]);
                    else
                        $rowspan_gName += 8-count($MODULE_OUTPUT["pairs"][$gid]);
                if(empty($days[$pair-1]["odd"]) && empty($days[$pair-1]["even"]))
                    $pp = 0;
                $rowspan_pairNum = count($days[$pair-1])+(count($g[$gid]["doubles"][0][$pair-1])!=0 ? 1 : 0) + (count($g[$gid]["doubles"][1][$pair-1])!=0 ? 1 : 0);
                $rowspan_pairNum += (count($g[$gid]["triples"][0][$pair-1])!=0 ? 1 : 0)+(count($g[$gid]["triples"][1][$pair-1])!=0 ? 1 : 0);
            ?>

                 <?if(!empty($days[$pair-1][$weeks[$parity]]) ){?>
                    <?foreach($mode as $mod) {?>
                        <?if(($mod=="main") || !empty($g[$gid][$mod][$parity][$pair-1])) {?>
                             <tr class="pairs">
                                <?if(!$f){?><td class="groups" rowspan="<?=$rowspan_gName?>">
                                <?if($_GET["weeks"]=="")
                                    echo $MODULE_OUTPUT["groups"][$gid]["group_name"];
                                else
                                    echo "<b>".$MODULE_OUTPUT["groups"][$gid]["group_subname"]."</b>";?>
                                </td><?$f=1; $pp = 1;}?>
                                <?if(!empty($days[$pair-1])) {?>
                                <?if(!$parity && !$fl){?><td rowspan="<?=$rowspan_pairNum?>"><b><?=$pair?></b></td><?$fl=1;} elseif((count($days[$pair-1])==1) && !$fl) {?>
                                    <td rowspan="<?=$rowspan_pairNum?>"><b><?=$pair?></b></td>
                                <?$fl=1;}?>
                                <td rowspan="1"><?=(!$parity) ? "�" : "�"; ?></td>
                                    <?foreach($wdays as $did => $day){?>
                                        <?
                                        if($mod=="main") {
                                            $p = $MODULE_OUTPUT["groups"][$gid][$weeks[$parity]][$did][$pair-1];
                                        }
                                        else
                                            $p = $g[$gid][$mod][$parity][$pair-1][$did];
                                            if($p["subj"]) {
                                                $teachers = array_diff(explode(";", $p["teacher_id"]),array(''));
                                                foreach ($teachers as $k => $v) $teachers[$k] = $MODULE_OUTPUT["teachers"][$v]["p2"]." ".$MODULE_OUTPUT["teachers"][$v]["p0"][0].". ".$MODULE_OUTPUT["teachers"][$v]["p1"][0].".";
                                                $teachers = implode(";", $teachers);
                                                $auds = array_diff(explode(";", $p["auditorium_id"]),array(''));
                                                foreach ($auds as $k => $v) $auds[$k] = $MODULE_OUTPUT["auds"][$v]["build"]."-".$MODULE_OUTPUT["auds"][$v]["name"];
                                                $auds = implode(";", $auds);
                                                $res = array();
                                                $res[] = "<b>".$p["subj"]."</b>";
                                                $res[] = $types[$p["type"]];
                                                $res[] = $subgroups[$p["subgroup"]];
                                                $res[] = $auds;
                                                $res[] = $teachers;
                                                if (!empty($p['comment_from']) && !empty($p['comment_to'])
                                                    && ($p['comment_from']!="00.00.0000") && ($p['comment_to']!="00.00.0000"))
                                                    $res[] = "� ".$p['comment_from'].' �� '.$p['comment_to'];
                                                foreach ($res as $k => $v) if (!$v || $v == ';') unset($res[$k]);
                                                $e=0;?>
                                                <td><?=implode(";",$res)?></td>
                                            <?} else {?>
                                                <td></td>
                                            <?}?>
                                <?}?>
                            <?}?>
                            </tr>
                        <?}?>
                    <?}?>
                <?} elseif($pp == 0 && $parity && !empty($_GET["weeks"])) {$pp = 1;?>
                    <tr class="pairs">
                        <?if(!$f){?><td class="groups" rowspan="<?=$rowspan_gName?>">
                            <?if($_GET["weeks"]=="")
                                echo $MODULE_OUTPUT["groups"][$gid]["group_name"];
                            else
                                echo "<b>".$MODULE_OUTPUT["groups"][$gid]["group_subname"]."</b>";?>
                            </td><?$f=1; $pp = 1;} else {?>
                        <?}?>
                        <td colspan = "1"><b><?=$pair?></b></td>
                        <?for($x=0;$x<8;$x++) {?>
                            <td></td>
                        <?}?>
                    </tr>
                <?}?>
            <?
            ($parity) ? $pair++ : $pair;
            }?>
        <?}?>
        <tr class="info">
            <td colspan="<?=(2+$daysCol)?>">��������! � �������� ����� �������� ��������� ����������, ������� �� ����� ���������� �� �����.</td>
        </tr>
        <tr class="info">
            <td colspan="<?=(2+$daysCol)?>">��������� ������� �������� ����:</td>
        </tr>
        <?if(!empty($MODULE_OUTPUT["buildings"])) {?>
                <tr class="info"><td colspan="<?=(2+$daysCol)?>">
                <?foreach($MODULE_OUTPUT["buildings"] as $b_id=>$buildings) { ?>

                            <b><?=$buildings["label"]?></b>-<?=$buildings["name"]?>,&nbsp;


                <?}?>
                </td></tr>
        <?}?>
        <tr class="info">
            <td colspan="<?=(3+$daysCol)?>">�������� �����������: <b>�</b>  � ���������� ������� | <b>��</b> � ������������ ������� | <b>(�)</b> ��� <b>(�)</b> � ���������, � ������� ������� (���� ���, �� � ���� ������)</td>
        </tr>
        </tbody>
    </table>

<?} else {?>
���������� �� ���������
<?}?>
</body>
</html>
<?
}
    break;
    case "timetable_show":
    case "timetable_make":
    ?><div style="position: relative; float: left; width: 69%;"><?
    {
    $subgroups = array("���", "�", "�");
    if (isset($_GET["gr"])) {
        $group = $MODULE_OUTPUT["groups"][$_GET["gr"]];
        ?><script>
            jQuery(document).ready(function ($) {
                defgroup(<?=$group["form_education"]?>,<?=$group["id_faculty"]?>,<?=$group["year"]?>,<?=$group["id"]?>);
            //  install_subjects();
            });
        </script>
        <?
    }
        ?>
<?if(empty($MODULE_OUTPUT["show_mode"])) {
    $m = explode("/", $Engine->unqueried_uri);
    $n =  explode("/", $_SERVER["REQUEST_URI"]);

    ?>
    <?if($n[1]!="raspisanie") {?>
        <?if($_GET["print"]!=1) {?>
            <a href="?print=1"  class="tt_to_print">������ ���������� (� ������������ ������ �����)</a>
        <?} elseif($m[1]!="student") {?>
            <a href="/office/timetable_making/"  class="tt_to_print">������������� ����������</a>
        <?} else {?>
            <a href="/student/timetable/"  class="tt_to_print">�������� ����������</a>
        <?}?>
    <?}?>
    <table id="choosegroup" >
    <tr><td>
    <table style="width: 500px; background-color: <?=($_GET["print"]==1) ? "#ede" : "#eee"?>;" cellspacing="3" class="sel_gr">
        <tr>
            <td style="width: 250px;">����� ��������</td>
            <td><select onchange="selgroups();" id="edform"><option></option><option value="1">�����</option><br /><option value="2">�������</option><option value="3">����-�������</option></select></td>
        </tr>
        <tr>
            <td style="width: 250px;">���������</td>
            <td><select onchange="selgroups();" id="fac"><option></option><?php foreach ($MODULE_OUTPUT["faculties"] as $f) { ?><option value="<?=$f["id"]?>"><?=$f["name"]?></option><br /><? } ?></select></td>
        </tr>
        <tr>
            <td style="width: 250px;">����</td>
            <td><select onchange="selgroups();" id="course"><option></option><option>1</option><br /><option>2</option><br /><option>3</option><br /><option>4</option><br /><option>5</option><br /><option>6</option><br /></select></td>
        </tr>
        <tr  class="week hidden">
            <td style="width: 250px;">������</td>
            <td>
                <table>
                    <tr>
                        <?for($i=0;$i<4;$i++) {?>
                            <?if($_GET["print"]!=1){?>
                                <td><input type="radio" value="<?=$i?>" class="week" name="week" <?=($_GET["week"]==$i) ? "checked" : ""?>><?=$i+1?> ������.<td>
                            <?} else {?>
                                <td><input type="checkbox" value="<?=$i?>" class="week" name="week" <?=($_GET["week"]==$i) ? "checked" : ""?>><?=$i+1?> ������.<td>
                            <?}?>
                        <?}?>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 250px;">������</td>
            <?if($_GET["print"]!=1) {?>
                <td><table><tr><td width="40%"><?php foreach ($MODULE_OUTPUT["groups"] as $gr) { ?><span data-id="<?=$gr["id"]?>" onclick="" style="display:none; float: left; cursor: pointer;" class="groups gr_<?=$gr["form_education"]?>_<?=$gr["id_faculty"]?>_<?=$gr["year"]?>"><input type="radio" value="<?=$gr["id"]?>" id="<?=$gr["id"]?>" class="select_group" name="groups"> <?=$gr["name"]?></span> <? } ?></td></tr></table></td>
            <?} else {?>
                <td><table><tr><td width="40%"><?php foreach ($MODULE_OUTPUT["groups"] as $gr) { ?><span onclick="" style="display:none; float: left;" class="groups gr_<?=$gr["form_education"]?>_<?=$gr["id_faculty"]?>_<?=$gr["year"]?>"><input type="checkbox" class="groups_to_print" value="<?=$gr["id"]?>" id="<?=$gr["id"]?>" name="groups"> <?=$gr["name"]?></span> <? } ?></td></tr>
                <tr><td  class="sel_gr <?=($_GET["print"]==1) ? "" : "hidden"?>" colspan="2">�������� �� 1 �� 4 �����.</td></tr>
                </table></td>
            <?}?>
        </tr>
        </table></td>
    </tr></table>
<?}?>
        <?php
    $weeks = array(1 => "odd", 0 => "even");
    $days = array("�����������", "�������", "�����", "�������", "�������", "�������", "�����������");
    $types = array("������", "��������", "���. ������");
    $pairs = array("09:00-10:30", "10:40-12:10", "12:50-14:20", "14:30-16:00", "16:10-17:40", "17:45-19:15", "19:20-20:50");

    $week = ($MODULE_OUTPUT["parity"] == "even") ? "׸����" : "��������";
    $now = $MODULE_OUTPUT["now"];
            $months = array("������", "�������", "�����", "������", "���", "����", "����", "�������", "��������", "�������", "������", "�������");
            $year = date("Y");
            $month = date("n");
            $day = date("j");
    if (isset($_GET["gr"])) {
        ?><form action="" method="post">
        <? if ($MODULE_OUTPUT["mode"]=="timetable_show") { ?>
        <!-- <?if(empty($MODULE_OUTPUT["odd"]) && empty($MODULE_OUTPUT["even"])) {?>
            <p>���������� �� ���������. �� ���� �������� ���������� � ���� ����������.</p>
            <h2>���������� ����������</h2>
            ������������: �-106 �<br>
            �������: 267-32-36<br> -->
      <?}?>
            <br /><h1><?=$MODULE_OUTPUT["group"]["fac"]?></h1>
            <h2>����������� ����������: <strong><?=$MODULE_OUTPUT["group"]["specialities_code"]?> - <?=$MODULE_OUTPUT["group"]["spec"]?></strong></h2>
        <p>������ <strong><?=$MODULE_OUTPUT["group"]["name"]?></strong>, <?=$MODULE_OUTPUT["group"]["year"]?> ����<br /> <strong>���������: <? foreach ($subgroups as $snum => $sg) { if ($_GET["subgr"]!=$snum) { ?><a href="?<?if(!$MODULE_OUTPUT["show_mode"]){?>gr=<?=$_GET["gr"]?>&<?}?>subgr=<?=$snum?>"><?=$sg?></a> <? } else echo $sg." "; } ?></strong></p><p><strong><?if(!empty($MODULE_OUTPUT["odd"]) && !empty($MODULE_OUTPUT["even"]) || isset($_GET["week"])){?><a target="_blank" href="/timetable-print/?gr=z<?=$MODULE_OUTPUT["group"]["id"]?>&info=1<?=(isset($_GET["week"])) ? "&weeks=z".$_GET["week"] : ""?>">�����������</a><?}?></strong></p>
        <h2 style="text-align: center;<? if (!$MODULE_OUTPUT["dates"]["actual_from"] && !$MODULE_OUTPUT["dates"]["actual_to"]) { ?> display: none;<? } ?>"><strong>���������� ������� �� ������ � <?=implode(".",array_reverse(explode("-",$MODULE_OUTPUT["dates"]["actual_from"])))?> �� <?=implode(".",array_reverse(explode("-",$MODULE_OUTPUT["dates"]["actual_to"])))?></strong></h2>
        <? } else { ?>
            <br /><h2 style="text-align: center;"><strong>���������� ������� �� ������ � <input data-id="<?=$_GET["gr"]?>" type="text" id="actual_from" readonly="readonly" name="actual_from<?=($_GET["week"]!="") ? "_".$_GET["week"] : ""?>" value="<?=implode(".",array_reverse(explode("-",$MODULE_OUTPUT["dates"]["actual_from"])))?>" style="100px;" /> �� <input data-id="<?=$_GET["gr"]?>" type="text" id="actual_to" readonly="readonly" name="actual_to<?=($_GET["week"]!="") ? "_".$_GET["week"] : ""?>" value="<?=implode(".",array_reverse(explode("-",$MODULE_OUTPUT["dates"]["actual_to"])))?>" style="100px;" /></strong></h2>
        <? } ?>
        <select style="display:none;" id="subjlist"><optgroup><option selected="selected"></option>
                                    <? $kaf = ""; $fac = "";  foreach ($MODULE_OUTPUT["subjects"] as $k => $v) {
                                    if ($fac != $v["fac"]) { $fac = $v["fac"]; ?>
                                        <optgroup label="<?=$fac?>">
                                    <? } ?>
                                    <?if ($kaf != $v["kaf"] && empty($v["kaf"])) { $kaf = "���������� �� ����������� � �������"; ?>
                                        <option disabled="disabled"></option><optgroup label="<?=$kaf?>">
                                    <? } ?>
                                    <?if ($kaf != $v["kaf"]) { $kaf = $v["kaf"]; ?>
                                        <option disabled="disabled"></option><optgroup label="<?=$kaf?>">
                                    <? } ?>
            <option value="<?=$k?>"<? if($curpair && $curpair["subject_id"] == $k) { ?> selected="selected"<? } ?>><?=$v["name"]?></option><? } ?></optgroup></optgroup>
        </select>
        <select style="display:none;" id="teachlist"><option></option><? foreach ($MODULE_OUTPUT["teachers"] as $k => $v) { ?><option value="<?=$k?>"><?=$v["last_name"]?> <?=substr($v["name"],0,1)?>. <?=substr($v["patronymic"],0,1)?>.</option><? } ?>
        </select>
        <select style="display:none;" id="audlist"><option></option><? foreach ($MODULE_OUTPUT["auds"] as $k => $v) { ?><option value="<?=$k?>"><?=$v["label"]?>-<?=$v["aud_name"]?></option><? } ?>
        </select>

            <!-- <?
            foreach ($days as $day => $dname) {
                if (/*$MODULE_OUTPUT["mode"] == "timetable_make" || count($MODULE_OUTPUT[$wname][$day])*/ 1 /*������� ��� ����*/) {
                ?><h2><?=$dname?></h2>
                <table width="100%" style="text-align: center;">
                    <tr>
                        <th width="2%">#</th>
                        <th width="5%">����</th>
                        <th width="3%">������</th>
                        <th width="25%">����������</th>
                        <th width="5%">���</th>
                        <th<? if ($MODULE_OUTPUT["mode"]=="timetable_make") { ?> style="width: 5%"<? } else { ?> width="5%"<? } ?>>���������</th>
                        <th width="12%">���������</th>
                        <th width="23%">�������������</th>
                        <? if ($MODULE_OUTPUT["mode"]=="timetable_make") { ?><th width="10%">�����������</th><? } ?>
                        <? if ($MODULE_OUTPUT["mode"]=="timetable_make") { ?><th width="5%">����������</th><? } ?>
                        <? if ($MODULE_OUTPUT["mode"]=="timetable_make") { ?><th width="10%">�����������</th><? } ?>
                    </tr>
                    <? $rownum = 0;
                    foreach ($pairs as $pnum => $pr) {
                    foreach ($weeks as $wnum => $wname) {
                        $pairzz = array(0 => "", 1 => "", 2 => ""); //��� ������ ��������
                        if (isset($MODULE_OUTPUT[$wname][$day][$pnum])) {
                            $pairzz[0] = $MODULE_OUTPUT[$wname][$day][$pnum];
                            if (isset($MODULE_OUTPUT["doubles"][$pairzz[0]["id"]]))
                                $pairzz[1] = $MODULE_OUTPUT["doubles"][$pairzz[0]["id"]];
                            if (isset($MODULE_OUTPUT["triples"][$pairzz[0]["id"]]))
                                $pairzz[2] = $MODULE_OUTPUT["triples"][$pairzz[0]["id"]];
                        }

                        $rowspan_count = 2;
                        if (isset($MODULE_OUTPUT["doubles"][$MODULE_OUTPUT["even"][$day][$pnum]["id"]]))
                            $rowspan_count++;
                        if (isset($MODULE_OUTPUT["doubles"][$MODULE_OUTPUT["odd"][$day][$pnum]["id"]]))
                            $rowspan_count++;
                        if (isset($MODULE_OUTPUT["triples"][$MODULE_OUTPUT["odd"][$day][$pnum]["id"]]))
                            $rowspan_count++;
                        if (isset($MODULE_OUTPUT["triples"][$MODULE_OUTPUT["even"][$day][$pnum]["id"]]))
                            $rowspan_count++;

                        foreach ($pairzz as $is_double => $curpair) {
                        $hide = ($is_double && !$curpair) ? "display: none; " : "";
                        $clr = ($rownum % 2) ? 'cff' : 'def';
                        $trstyle = ($rownum % 2) ? " style='". $hide ."background-color: #".$clr.";'" : " style='". $hide ."background-color: #".$clr.";'"; $rownum++;
                        if ($MODULE_OUTPUT["mode"] == "timetable_make") { ?>
                            <tr<?=$trstyle?> id="tr_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_double" : "_triple") ?>">
                                <? if(!isset($arr[$day][$pnum])) { ?><td  valign="middle" rowspan="<?=$rowspan_count?>" class="rowspan_<?=$day?>_<?=$pnum?>"><?=$pnum+1?></td><td valign="middle" rowspan="<?=$rowspan_count?>" class="rowspan_<?=$day?>_<?=$pnum?>"><?=$pr?></td><? } ?>
                                <td <?=(($wnum==0) && ($is_double == 0)) ? "bgcolor='#eaeaea'" : ""; ?><?=($is_double != 0) ? "bgcolor='#cff'" : ""; ?>><? echo ($wname=="even") ? '���' : '�����'; ?><p align="center"><img class="clear_pair" id="clear_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>" title="��������" style="cursor: pointer;" src="/themes/styles/delete.png" onclick="if(confirm('�������� ����?')) clear_row('<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>');"; /></p></td>
                                <td <?=(($wnum==0) && ($is_double == 0)) ? "bgcolor='#eaeaea'" : ""; ?><?=($is_double != 0) ? "bgcolor='#cff'" : ""; ?> valign="top"><select style="width: 200px;" class="subjs" def="<?=$curpair["subject_id"]?>" name="subj_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>" id="subj_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>"><option value="<?=$curpair["subject_id"]?>" selected><?=$MODULE_OUTPUT["subjects"][$curpair["subject_id"]]["name"]?></option></select><br /><input type="text" class="search" style="width: 100px; font-style: italic; color: #aaa;" value="������� ����..." onfocus="if (this.value=='������� ����...') this.value='';" onkeyup="select_filter('subj_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>', this.value, 'subjlist');" />
                                    <? if ($is_double==0) { ?>
                                        <p style="display:<?=!$pairzz[2] && !$pairzz[1]?"block":"none"?>" id="addouble_<?=$wnum?>_<?=$day?>_<?=$pnum?>"><a cursor="pointer" onclick="show_doublepair('<?=$wnum?>','<?=$day?>_<?=$pnum?>');"><small>�������� ����</small></a></p>
                                    <? } elseif ($is_double==1) { ?>
                                        <p style="display:<?=!$pairzz[2]?"block":"none"?>" id="adtriple_<?=$wnum?>_<?=$day?>_<?=$pnum?>"><a cursor="pointer" onclick="show_triplepair('<?=$wnum?>','<?=$day?>_<?=$pnum?>');"><small>�������� ����</small></a></p>
                                        <p style="display:<?=$pairzz[1]?"block":"none"?>" id="deldouble_<?=$wnum?>_<?=$day?>_<?=$pnum?>"><a cursor="pointer" onclick="hide_doublepair('<?=$wnum?>','<?=$day?>_<?=$pnum?>');"><small>������� ����</small></a></p>
                                    <? } elseif($is_double==2) { ?>
                                        <p id="deldouble_<?=$wnum?>_<?=$day?>_<?=$pnum?>"><a cursor="pointer" onclick="hide_triplepair('<?=$wnum?>','<?=$day?>_<?=$pnum?>');"><small>������� ����</small></a></p><? } ?></td>



                                <td <?=(($wnum==0) && ($is_double == 0)) ? "bgcolor='#eaeaea'" : ""; ?><?=($is_double != 0) ? "bgcolor='#cff'" : ""; ?>  valign="top"><select style="max-width: 150px" class="type" id="type_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>" onchange="subgroup_switch(this.value,<?=$wnum?>,<?=$day?>,<?=$pnum?>,<?=$is_double==0  ? "0" : ($is_double==1 ? "1" : "2") ?>);" name="type_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>"><? foreach ($types as $tid => $type) { ?><option value="<?=$tid?>"<? if($curpair && $curpair["type"] == $tid) { ?> selected="selected"<? } ?>><?=$type?></option><? } ?></select></td>

                                <td <?=(($wnum==0) && ($is_double == 0)) ? "bgcolor='#eaeaea'" : ""; ?><?=($is_double != 0) ? "bgcolor='#cff'" : ""; ?>  valign="top"><span<? if (!$curpair || !$curpair["type"]) $dispnone = ' display: none;'; else $dispnone = ''; ?> style="<?=$dispnone?>" id="subgr_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>"><? foreach ($subgroups as $snum => $subgr) { ?><input type="radio" class="subgr"  name="subgr_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>" value="<?=$snum?>"<? if($curpair && $curpair["subgroup"] == $snum) { ?> checked="checked"<? } ?> /> <?=$subgr?><br/><? } ?></span></td>

                                <td <?=(($wnum==0) && ($is_double == 0)) ? "bgcolor='#eaeaea'" : ""; ?><?=($is_double != 0) ? "bgcolor='#cff'" : ""; ?>  valign="top"><select class="aud_list" style="width: 120px;" id="aud_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>" name="aud_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>"><option></option><? //foreach ($MODULE_OUTPUT["auds"] as $k => $v) { ?><option value="<?=$k?>"><?=$v["label"]?> - <?=$v["aud_name"]?></option><? //} ?></select><br /><input class="search" type="text" style="width: 100px; font-style: italic; color: #aaa;" value="������� ����..." onfocus="if (this.value=='������� ����...') this.value='';" onkeyup="select_filter('aud_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>', this.value, 'audlist');" /> <input type="button" class="aud_add" id="addaud_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>"  value="��������" /><input type="hidden" class="values" name="aud_ids_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>" id="aud_ids_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>" value="<? echo ($curpair && $curpair['auditorium_id']) ? $curpair['auditorium_id'] : ''; ?>" />
                                    <p id="aud_names_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>">
                                        <? if ($curpair && $curpair["auditorium_id"]) { $tchs = explode(";", $curpair["auditorium_id"]);
                                        foreach ($tchs as $tval) if ($tval) echo "<span id='aud_id_".$tval."_".$wnum."_".$day."_".$pnum.($is_double==0  ? "" : ($is_double==1 ? "_d" : "_t"))."'>- ".$MODULE_OUTPUT["auds"][$tval]["label"]." - ". $MODULE_OUTPUT["auds"][$tval]["aud_name"] ." [<a style='cursor:pointer;' class='del_aud' id='delaud_".$tval."_".$wnum."_".$day."_".$pnum.($is_double==0  ? "" : ($is_double==1 ? "_d" : "_t"))."' onclick='delfromlist(\"aud\", \"".$wnum."_".$day."_".$pnum.($is_double==0  ? "" : ($is_double==1 ? "_d" : "_t"))."\", $tval)'>x</a>]<br /></span>";
                                        } ?>
                                    </p></td>

                                <td <?=(($wnum==0) && ($is_double == 0)) ? "bgcolor='#eaeaea'" : ""; ?><?=($is_double != 0) ? "bgcolor='#cff'" : ""; ?> valign="top"><select class="teach_list" style="width: 120px;" id="teach_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>" name="teach_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>"><option></option><? //foreach ($MODULE_OUTPUT["teachers"] as $k => $v) { ?><option value="<?=$k?>"><?=$v["last_name"]?> <?=substr($v["name"],0,1)?>. <?=substr($v["patronymic"],0,1)?>.</option><? //} ?></select><br /><input class="search" type="text" style="width: 100px; font-style: italic; color: #aaa;" value="������� ����..." onfocus="if (this.value=='������� ����...') this.value='';" onkeyup="select_filter('teach_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>', this.value, 'teachlist');" /> <input type="button" class="teach_add" id="addteach_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>" value="��������" /><input class="values" type="hidden" name="teach_ids_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>" id="teach_ids_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>" value="<? echo ($curpair && $curpair['teacher_id']) ? $curpair['teacher_id'] : ''; ?>" />
                                    <p id="teach_names_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>">
                                        <? if ($curpair && $curpair["teacher_id"]) { $tchs = explode(";", $curpair["teacher_id"]);
                                        foreach ($tchs as $tval) if ($tval) echo "<span id='teach_id_".$tval."_".$wnum."_".$day."_".$pnum.($is_double==0  ? "" : ($is_double==1 ? "_d" : "_t"))."'>- ".$MODULE_OUTPUT["teachers"][$tval]["last_name"]." ".substr($MODULE_OUTPUT["teachers"][$tval]["name"], 0, 1).". ".substr($MODULE_OUTPUT["teachers"][$tval]["patronymic"], 0, 1).". [<a style='cursor:pointer;' class='del_teach' id='delteach_".$tval."_".$wnum."_".$day."_".$pnum.($is_double==0  ? "" : ($is_double==1 ? "_d" : "_t"))."' onclick='delfromlist(\"teach\", \"".$wnum."_".$day."_".$pnum.($is_double==0  ? "" : ($is_double==1 ? "_d" : "_t"))."\", $tval)'>x</a>]<br /></span>";
                                        } ?>
                                    </p>
                                </td>

                                <td <?=(($wnum==0) && ($is_double == 0)) ? "bgcolor='#eaeaea'" : ""; ?><?=($is_double != 0) ? "bgcolor='#cff'" : ""; ?> valign="top">
                                    c <input type="text" data-id="<?=$_GET['gr']?>" class="comm" style="width: 100px;" readonly="readonly" id="comment_from_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>" name="<?=$_GET['week']?>" value="<? echo $curpair["comment_from"]=='0000-00-00' ? '' : implode(".",array_reverse(explode("-",$curpair["comment_from"])))?>" /> �� <input type="text" data-id="<?=$_GET['gr']?>" style="width: 100px;" id="comment_to_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>" class="comm" name="<?=$_GET['week']?>" readonly="readonly" value="<? echo $curpair["comment_to"]=='0000-00-00' ? '' : implode(".",array_reverse(explode("-",$curpair["comment_to"])))?>" /></td>
                                <? if ($MODULE_OUTPUT["mode"]=="timetable_make") {?><td <?=(($wnum==0) && ($is_double == 0)) ? "bgcolor='#eaeaea'" : ""; ?><?=($is_double != 0) ? "bgcolor='#cff'" : ""; ?> valign="top">
                                        <input type="text" value="<?=$curpair["comment"]?>" class="add_comment_input" id="addcomm_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>"><br>
                                        <input type="submit" class="add_comment" value="���������" id="comm_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>">
                                </td><?}?>
                                <? if ($MODULE_OUTPUT["mode"]=="timetable_make") { ?><td <?=(($wnum==0) && ($is_double == 0)) ? "bgcolor='#eaeaea'" : ""; ?><?=($is_double != 0) ? "bgcolor='#cff'" : ""; ?> class="show_button" valign="center"><div class="output_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>" style="color: green"></div><select size="5" class="copypair_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>" multiple="multiple" style="max-width: 70px;" name="copy_<?=$wnum?>_<?=$day?>_<?=$pnum?>[]"><? foreach ($MODULE_OUTPUT["potok"] as $key => $val) { ?><option value="<?=$key?>"><?=$val?></option><? } ?></select><!--<input type="checkbox" name="copy_<?=$wnum?>_<?=$day?>_<?=$pnum?>" onclick="if(this.checked) { if (!confirm('��������! ������� ��� �������, �� �������� ���������� ���� ���� ��� ���� ����� ������. �� �������?')) return false; }" />--><input style="display: inline-block; opacity: 0" type="button" class="copy" id="copypair_<?=$wnum?>_<?=$day?>_<?=$pnum?><?=$is_double==0  ? "" : ($is_double==1 ? "_d" : "_t") ?>" value="�����������" /></td><? } ?>
                            </tr>
                        <? } else {  /* timetable_show */
                            ?>
                            <tr<?=$trstyle?>>
                                <? if(!isset($arr[$day][$pnum])) { ?><td valign="middle" rowspan="<?=$rowspan_count?>"><?=$pnum+1?></td>
                                <td valign="middle" rowspan="<?=$rowspan_count?>"><?=$pr?></td><? } ?>
                                <td <?=(($wnum==0) && ($is_double == 0)) ? "bgcolor='#eaeaea'" : ""; ?><?=($is_double != 0) ? "bgcolor='#cff'" : ""; ?> ><? echo ($wname=="even") ? '���' : '�����'; ?></td>
                                <td <?=(($wnum==0) && ($is_double == 0)) ? "bgcolor='#eaeaea'" : ""; ?><?=($is_double != 0) ? "bgcolor='#cff'" : ""; ?> ><?=$curpair["subj"]?><? if ($curpair["comment_from"] && $curpair["comment_from"]!='00.00.0000') { ?><br /><span style="font-size: 12px;">� <?=$curpair["comment_from"]?> �� <?=$curpair["comment_to"]?></span><? } ?></td>
                                <td <?=(($wnum==0) && ($is_double == 0)) ? "bgcolor='#eaeaea'" : ""; ?><?=($is_double != 0) ? "bgcolor='#cff'" : ""; ?> ><?=$types[$curpair["type"]]?></td>
                                <td <?=(($wnum==0) && ($is_double == 0)) ? "bgcolor='#eaeaea'" : ""; ?><?=($is_double != 0) ? "bgcolor='#cff'" : ""; ?> ><? echo $curpair["subgroup"] ? $subgroups[$curpair["subgroup"]] : ""; ?></td>
                                <td <?=(($wnum==0) && ($is_double == 0)) ? "bgcolor='#eaeaea'" : ""; ?><?=($is_double != 0) ? "bgcolor='#cff'" : ""; ?> ><? foreach ($curpair["auds"] as $aud) { ?><?=$aud["build"]." - ".$aud["name"]?><br /><? } ?></td>
                                <td <?=(($wnum==0) && ($is_double == 0)) ? "bgcolor='#eaeaea'" : ""; ?><?=($is_double != 0) ? "bgcolor='#cff'" : ""; ?> ><? foreach ($curpair["teachs"] as $teach) { ?><a href="/people/<?=$teach["people_id"]?>/" target="_blank"><?=$teach["p2"]." ".substr($teach["p0"],0,1).". ".substr($teach["p1"],0,1)?>.</a><br /><? } ?></td>
                            </tr>
                        <? } ?>
                    <? $arr[$day][$pnum] = 1; } } } ?>
                </table><?
             } }

    ?> --></div><!-- ��������� div ��������� ����������� ���������� --><?
    if ($MODULE_OUTPUT["mode"]=="timetable_make") { ?>


    <input type="submit" style="display: none" id="timetable_save_button" value="���������" />
    <br/>
    <br/>
    <a href="/raspisanie/?gr=<?=$_GET["gr"]?><?=($_GET["week"]!="") ? "&week=".$_GET["week"] : ""?>">���������� ����������</a>
    <? } else { ?>

    <div class="right_cont">
                    <div id="timetable_data">
                        �������&nbsp;<?=$now["mday"]?>&nbsp;<?=$months[$now["mon"]-1]?>&nbsp;<?=$now["year"]?>&nbsp;�.
                        <?=$days[$now["wday"]-1]?>.&nbsp;������&nbsp;<?=$week?>
                    </div>



                    <div id="designation">
                        <h3>����������� ��������</h3>
                        <ul>
<?php           foreach($MODULE_OUTPUT["buildings"] as $buildings) { ?>
                            <li>
                                <span><?=$buildings["label"]?></span>
                                <p><?=$buildings["name"]?></p>
                            </li>
<?php           } ?>
                        </ul>
                    </div>
                </div>
    <? } ?>
    </form>
    <?
    }

        break;
    }

    case "pulpit_list": {
        if(isset($MODULE_OUTPUT["pulpit"])) { ?>
            <div class="shadow_rbg">
<?php       if(isset($MODULE_OUTPUT["faculty_name"])) { ?>
                <h2>�������:</h2>
<?php       } ?>
                <ul class="shadow_cont">
<?php       foreach($MODULE_OUTPUT["pulpit"] as $pulpit) {
                if($pulpit["url"] == null) { ?>
                    <li><?=$pulpit["name"]?></li>
<?php           } else { ?>
                    <li><a href="<?=$pulpit["url"]?>"><?=$pulpit["name"]?></a></li>
<?php           }
            } ?>
                </ul>
                <div class="shadow_tr">&nbsp;</div>
                <div class="shadow_b">
                    <div class="shadow_br">&nbsp;</div>
                    <div class="shadow_bl">&nbsp;</div>
                </div>
            </div>
<?php   }
        break;
    }
    case "timetable_departmens":
    {
        if(!isset($MODULE_OUTPUT["departments_mode"]))
        {
        if(isset($MODULE_OUTPUT["timetable_departments"]))
        {
            $id = 0;
            ?>
            <h2>������ ������ �� �����������:</h2>
            <ul>
            <?php
            foreach($MODULE_OUTPUT["timetable_departments"] as $department)
            {
                if($department["faculty_id"] != $id)
                {
                    ?>
                    </ul><li><?=$department["faculty_name"]?><ul>
                    <?php
                    $id = $department["faculty_id"];
                }
                ?>
                <li> <a href="edit/<?=$department["id"]?>/" <?php if(!$department['is_active']) { ?>  style="color: gray"<?php } ?>><?=$department["name"]?></a> [<a href="delete/<?=$department["id"]?>/" onclick="return confirm('�� �������, ��� ������ ������� ��������� �������?');"}>�������</a>]</li>
                <?php
            }
            ?>
            </ul>
            <?php
        }
        ?>
        <br />
        <h2>�������� �������:</h2>
        <form method="post" name="add_department">
        <h3>�������� �������:</h3>
        <input type="text" name="department_name" size="60" />
        <h3>����� �������:</h3>
        <input type="text" name="department_url" size="60" />
        <h3>�������� �������� ��� ���������, � �������� ��������� �������:</h3>
        <select name="faculty_id" size="1">
        <option value="0" selected="selected">�������� ���������:</option>
        <?php
        foreach($MODULE_OUTPUT["faculties"] as $faculty)
        {
            ?>
            <option value="<?=$faculty["id"]?>"><?=$faculty["name"]?></option>
            <?php
        }
        ?>
        </select>
        <br />
        <h3><input type="checkbox" name="is_active" value="1" /> ������� �������</h3>
        <input type="submit" value="��������" class="button" />
        </form>
        <?php
        }
        else
        {
            switch($MODULE_OUTPUT["departments_mode"])
            {
                case "edit":
                {
                    foreach($MODULE_OUTPUT["edit_department"] as $department)
                    {
                        ?>
                        <h2>�������������� ���������� � ������� <?=$department["name"]?> (<?=$department["faculty_name"]?>):</h2>
                        <form name="edit_department" method="post">
                        <h3>�������� �������:</h3>
                        <input type="text" name="department_name" value="<?=$department["name"]?>" size="60" />
                        <h3>����� �������:</h3>
                        <input type="text" name="department_url" value="<?=$department["url"]?>" size="60" />
                        <h3>���������, � �������� ��������� �������:</h3>
                        <select name="faculty">
                        <?php
                        foreach($MODULE_OUTPUT["faculties"] as $faculty)
                        {
                            if($faculty["id"] == $department["faculty_id"])
                            {?>
                                <option value="<?=$faculty["id"]?>" selected="selected"><?=$faculty["name"]?></option>
                                <?php
                            }
                            else
                            {?>
                                <option value="<?=$faculty["id"]?>"><?=$faculty["name"]?></option>
                                <?php
                            }
                        }
                        ?>
                        </select><br />
                        <h3><input type="checkbox" name="is_active" value="1" <?php echo $department["is_active"] ? " checked=\"checked\"" : ""; ?> /> ������� �������</h3>
                        <input type="hidden" value="edit" name="mode" />
                        <input type="hidden" value="<?=$department["id"]?>" name="id" />
                        <input type="submit" value="���������" class="button" />
                        </form>
                        <?php
                    }
                    break;
                }
            }
        }
        break;
    }

    case "select_teachers": {
        if (count($MODULE_OUTPUT["select_teachers"])) {
        //���������
        $un = array();
		foreach ($MODULE_OUTPUT["select_teachers"] as $key => $vl) {
            if(empty($vl['pos'])){
                $un[] = $vl;
                unset($MODULE_OUTPUT["select_teachers"][$key]);
                continue;
            }
            $volume[$key] = $vl['pos'];
        }

        array_multisort($volume, SORT_ASC, SORT_NUMERIC, $MODULE_OUTPUT["select_teachers"]);
        $MODULE_OUTPUT["select_teachers"] = array_merge($MODULE_OUTPUT["select_teachers"], $un);
?>
        <div class="select_teachers">
            <h2>������ �������:</h2>
            <ul>
<?php       foreach($MODULE_OUTPUT["select_teachers"] as $teachers) {
    if($teachers["st_id"] != 9) {
    ?>
                <li>
                    <a class="name-color" href="/people/<?=$teachers["id"]?>/"><strong><?=$teachers["last_name"]?></strong> <?=$teachers["name"]?> <?=$teachers["patronymic"]?></a><?=(!empty($teachers["academ_stat"]) || !empty($teachers["post_itemprop"]) || !empty($teachers["degree"])) ? ", " : ""?>
                    <?=!empty($teachers["post_itemprop"]) ? (!empty($teachers["degree"]) || !empty($teachers["academ_stat"]) ? $teachers["post_itemprop"].", " : $teachers["post_itemprop"]) : ""?>
                    <?=!empty($teachers["degree"]) ? (!empty($teachers["post_itemprop"]) ? (!empty($teachers["academ_stat"]) ? CF::LowCaseOne($teachers["degree"]).", " : CF::LowCaseOne($teachers["degree"])) : $teachers["degree"]) : ""?>
                    <?=!empty($teachers["academ_stat"]) ? (!empty($teachers["post_itemprop"]) || !empty($teachers["degree"]) ? CF::LowCaseOne($teachers["academ_stat"]) : $teachers["academ_stat"]) : ""?>

                </li>
<?php       }
}?>
            </ul>
            <hr class="sub_hr" />
        </div>
<?php   }
    }
    break;

    case "dep_subjects": {


        if(isset($MODULE_OUTPUT["subjects_file"])) {
            if(!isset($MODULE_OUTPUT["faculties_name"])) { ?>
            <h2>������������ ������:</h2>
<?php       } else { ?>
<?php       }
        if(isset($_GET['list_type'], $MODULE_OUTPUT["spec_file"]) && $_GET['list_type'] == 'spec') { ?>
        <div><a href="?list_type=subj">�� �����������</a> &nbsp;&nbsp;&nbsp;<strong>�� ����������� ����������</strong></div>
        <div class="subject_file_list">
            <ul>
<?php       foreach($MODULE_OUTPUT["spec_file"] as $spec_id => $spec) { ?>
                <li>
<?php           if(!isset($spec['files'])) { ?>
                    <?=$spec['code']?> - <?=$spec['name']?>
<?php           } else { ?>
                    <a name="<?=$spec['code'];?>" onclick="jQuery(this).parent().toggleClass('active');" data-id="<?=$spec_id; ?>"><?=$spec['code']?> - <?=$spec['name']?></a>
                    <ul>
<?php               foreach($spec['files'] as $file_id => $file_info) {
                        $title = "";
                        if(isset($file_info['descr'])) {
                            $title .= "��������: ".$file_info['descr']."";
                        }
                        if(isset($file_info['view'])) {
                            if(!empty($title)) {
                                $title .= "&nbsp;|&nbsp;&nbsp;";
                            }
                            $title .= "��� �����: ".$file_info['view']."";
                        }
                        if(isset($file_info['education'])) {
                            if(!empty($title)) {
                                $title .= "&nbsp;|&nbsp;&nbsp;";
                            }
                            $title .= "����� ��������: ".$file_info['education']."";
                        }
                        if(empty($title)) {
                            $title = "���������� �����������";
                        } ?>
                        <li>
                            <?if(isset($file_info['sig_id'])) {?>
                                <a class="sig" href="/file/sig/<?=$file_info['sig_id']?>" title="������� ���"></a>
                            <?} elseif($file_info["with_sig"] == 1) {?>
                                <a class="sig" title="�������� ���"></a>
                            <?}?>
                            <a target="_blank" href="/file/<?=$file_id?>/" title="<?=$title?>"><?=$file_info['name']?></a>
                            <span>����������: <?=$file_info['subj']?></span>
                        </li>
<?php               } ?>
                    </ul>
                </li>
<?php           } ?>
<?php       } ?>
            </ul>
            <hr class="sub_hr" />
        </div>
<?php       } else { ?>
        <div><strong>�� �����������</strong> &nbsp;&nbsp;&nbsp;<a href="?list_type=spec">�� ����������� ����������</a></div>
        <div class="subject_file_list">
            <ul>
<?php       foreach($MODULE_OUTPUT["subjects_file"] as $subj_id => $subj) { ?>
                <li>
<?php           if(!isset($subj['files'])) { ?>
                    <?=$subj['name']?>
<?php           } else { ?>
                    <a href="javascript:void(0)" onclick="jQuery(this).parent().toggleClass('active');" data-id="<?=$subj_id; ?>"><?=$subj['name']?></a>
                    <ul>
<?php               foreach($subj['files'] as $file_id => $file_info) {
                        $title = "";
                        if(isset($file_info['descr'])) {
                            $title .= "��������: ".$file_info['descr']."";
                        }
                        if(isset($file_info['view'])) {
                            if(!empty($title)) {
                                $title .= "&nbsp;|&nbsp;&nbsp;";
                            }
                            $title .= "��� �����: ".$file_info['view']."";
                        }
                        if(isset($file_info['education'])) {
                            if(!empty($title)) {
                                $title .= "&nbsp;|&nbsp;&nbsp;";
                            }
                            $title .= "����� ��������: ".$file_info['education']."";
                        }
                        if(empty($title)) {
                            $title = "���������� �����������";
                        } ?>
                        <li>
                            <?if(isset($file_info['sig_id'])) {?>
                                <a class="sig" href="/file/sig/<?=$file_info['sig_id']?>" title="������� ���"></a>
                            <?} elseif($file_info["with_sig"] == 1) {?>
                                <a class="sig" title="�������� ���"></a>
                            <?}?>
                            <a target="_blank" href="/file/<?=$file_id?>/" title="<?=$title?>"><?=$file_info['name']?></a>
<?php                   if(isset($file_info['spec_code'],$file_info['spec_name'])) { ?>
                            <span>����������� ����������: <?=$file_info['spec_code']?> - <?=$file_info['spec_name']?></span><?}?>
                            <?if(($Auth->usergroup_id!=0) && ($Auth->usergroup_id!=3)){?>,
                            <span>��������:  <a href="/people/<?=$file_info['people_id']?>/"><?=$file_info['people_name']?></a></span><?}?>
                        </li>
<?php               } ?>
                    </ul>
                </li>
<?php           } ?>
<?php       } ?>
            </ul>
            <hr class="sub_hr" />
        </div>
<?php       } ?>
<?php   }
        if (count($MODULE_OUTPUT["subjects"])) { ?>
        <div>
            <h2>���������� �������:</h2>
            <div>
                <ul>
<?php       foreach($MODULE_OUTPUT["subjects"] as $subj) {
                if ($subj['has_files_attached']) { ?>
                    <li><a href="<?=$subj["id"]?>/"><?=$subj["name"]?></a></li>
<?php           } else { ?>
                    <li><?=$subj["name"]?></li>
<?php           }
            } ?>
                </ul>
            </div>
        </div>
<?php   }
        if (count($MODULE_OUTPUT["specs"])) { ?>
        <div>
            <h2>����������� ����������:</h2>
            <div>
                <ul>
<?php       foreach($MODULE_OUTPUT["specs"] as $spec) { ?>
                    <li><a href="specs/<?=$spec["id"]?>/"><?=$spec["code"]." ".$spec["name"]?></a></li>
<?php       } ?>
                </ul>
            </div>
        </div>
<?php   }
    }
    break;

    case "dep_subj_views": {
        if (count($MODULE_OUTPUT["views"])) { ?>
        <h1><?=$MODULE_OUTPUT["department"]["name"]?></h1>
        <h2><?=(isset($MODULE_OUTPUT["subject"]["name"]) ? $MODULE_OUTPUT["subject"]["name"] : '').(isset($MODULE_OUTPUT["spec"]["id"]) ? $MODULE_OUTPUT["spec"]["code"].'-'.$MODULE_OUTPUT["spec"]["name"] : '')?></h2>
        <b>������-������������ �������� ���������</b>
        <br />
        <ul>
<?php       foreach($MODULE_OUTPUT["views"] as $view) {
                if ($view["file_id"]) { ?>
            <li><a href="/file/<?=$view["file_id"]?>/"><?=$view["file_descr"] ? $view["file_descr"] : $view["view_name"]?></a></li>
<?php           }
            } ?>
        </ul>
<?php   }
    }
    break;
    case "izop_ekonom":{?>
            <h2>�����������</h2>
        <ul>

        <? foreach($MODULE_OUTPUT["specialities"] as $special1) {
            if($special1["code"]=='080109'){?>

                <li><a href="javascript:void(0)" onclick="jQuery(this).parent().toggleClass('active');"><?=$special1["code"]?> - <?=$special1["name"]?></a>
                <ul>
                <?
            foreach($MODULE_OUTPUT["code1"] as $code) {

            if(($code["education"]=='�������')&&($code["spec_code"]=='080109')&&($code["spec_type"]=='65')){
            ?>


                <li>
                    <? if(isset($code["descr"])){
                ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["descr"]?></a><span>  ����������: <?=$code["p_n"]?></span>

            <?} else{       ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["name"]?></a> <span>  ����������: <?=$code["p_n"]?> </span> <? }?>


                </li>
                <?}}?>

            </ul>



                </li>


            <?

        }
        if($special1["code"]=='080502'){?>

                <li><a href="javascript:void(0)" onclick="jQuery(this).parent().toggleClass('active');"><?=$special1["code"]?> - <?=$special1["name"]?></a>
                <ul>
                <?
            foreach($MODULE_OUTPUT["code1"] as $code) {

            if(($code["education"]=='�������')&&($code["spec_code"]=='080502')&&($code["spec_type"]=='65')){
            ?>


                <li>
                    <? if(isset($code["descr"])){
                ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["descr"]?></a><span>  ����������: <?=$code["p_n"]?></span>

            <?} else{       ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["name"]?></a> <span>  ����������: <?=$code["p_n"]?> </span> <? }?>


                </li>

                    <?}}?>
            </ul>



                </li>


            <?

        }
            if($special1["code"]=='080105'){?>

                <li><a href="javascript:void(0)" onclick="jQuery(this).parent().toggleClass('active');"><?=$special1["code"]?> - <?=$special1["name"]?></a>
                <ul>
                <?
            foreach($MODULE_OUTPUT["code1"] as $code) {

            if(($code["education"]=='�������')&&($code["spec_code"]=='080105')&&($code["spec_type"]=='65')){
            ?>


                <li>
                    <? if(isset($code["descr"])){
                ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["descr"]?></a><span>  ����������: <?=$code["p_n"]?></span>

            <?} else{       ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["name"]?></a> <span>  ����������: <?=$code["p_n"]?> </span> <? }?>


                </li>

                <?} }?>
            </ul>



                </li>


            <?

        }
        }?>

        </ul>



    <h2>�����������</h2>
        <ul>

        <? foreach($MODULE_OUTPUT["specialities"] as $special1) {
            if($special1["code"]=='080100'){?>

                <li><a href="javascript:void(0)" onclick="jQuery(this).parent().toggleClass('active');"><?=$special1["code"]?> - <?=$special1["name"]?></a>
                <ul>
                <?
            foreach($MODULE_OUTPUT["code1"] as $code) {

            if(($code["education"]=='�������')&&($code["spec_code"]=='080100')&&($code["spec_type"]=='62')){
            ?>


                <li>
                    <? if(isset($code["descr"])){
                ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["descr"]?></a><span>  ����������: <?=$code["p_n"]?></span>

            <?} else{       ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["name"]?></a> <span>  ����������: <?=$code["p_n"]?> </span> <? }?>


                </li>

                    <? }}?>
            </ul>



                </li>


            <?

        }
            if($special1["code"]=='080200'){?>

                <li><a href="javascript:void(0)" onclick="jQuery(this).parent().toggleClass('active');"><?=$special1["code"]?> - <?=$special1["name"]?></a>
                <ul>
                <?
            foreach($MODULE_OUTPUT["code1"] as $code) {

            if(($code["education"]=='�������')&&($code["spec_code"]=='080200')&&($code["spec_type"]=='62')){
            ?>


                <li>
                    <? if(isset($code["descr"])){
                ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["descr"]?></a><span>  ����������: <?=$code["p_n"]?></span>

            <?} else{       ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["name"]?></a> <span>  ����������: <?=$code["p_n"]?> </span> <? }?>


                </li>

                <?} }?>
            </ul>



                </li>


            <?

        }
        }?>

        </ul>



    <?
    }


            break;
         case "izop_engineer":{?>
            <h2>�����������</h2>
        <ul>

        <? foreach($MODULE_OUTPUT["specialities"] as $special1) {
            if($special1["code"]=='110301'){?>

                <li><a href="javascript:void(0)" onclick="jQuery(this).parent().toggleClass('active');"><?=$special1["code"]?> - <?=$special1["name"]?></a>
                <ul>
                <?
            foreach($MODULE_OUTPUT["code1"] as $code) {

            if(($code["education"]=='�������')&&($code["spec_code"]=='110301')&&($code["spec_type"]=='65')){
            ?>


                <li>
                    <? if(isset($code["descr"])){
                ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["descr"]?></a><span>  ����������: <?=$code["p_n"]?></span>

            <?} else{       ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["name"]?></a> <span>  ����������: <?=$code["p_n"]?> </span> <? }?>


                </li>
                    <?} }?>

            </ul>



                </li>


            <?

        }
        if($special1["code"]=='110304'){?>

                <li><a href="javascript:void(0)" onclick="jQuery(this).parent().toggleClass('active');"><?=$special1["code"]?> - <?=$special1["name"]?></a>
                <ul>
                <?
            foreach($MODULE_OUTPUT["code1"] as $code) {

            if(($code["education"]=='�������')&&($code["spec_code"]=='110304')&&($code["spec_type"]=='65')){
            ?>


                <li>
                    <? if(isset($code["descr"])){
                ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["descr"]?></a><span>  ����������: <?=$code["p_n"]?></span>

            <?} else{       ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["name"]?></a> <span>  ����������: <?=$code["p_n"]?> </span> <? }?>


                </li>
                <?} }?>

            </ul>



                </li>


            <?

        }

            if($special1["code"]=='110302'){?>

                <li><a href="javascript:void(0)" onclick="jQuery(this).parent().toggleClass('active');"><?=$special1["code"]?> - <?=$special1["name"]?></a>
                <ul>
                <?
            foreach($MODULE_OUTPUT["code1"] as $code) {

            if(($code["education"]=='�������')&&($code["spec_code"]=='110302')&&($code["spec_type"]=='65')){
            ?>


                <li>
                    <? if(isset($code["descr"])){
                ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["descr"]?></a><span>  ����������: <?=$code["p_n"]?></span>

            <?} else{       ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["name"]?></a> <span>  ����������: <?=$code["p_n"]?> </span> <? }?>


                </li>

            <?} }?>
            </ul>



                </li>


            <?

        }

        if($special1["code"]=='190601'){?>

                <li><a href="javascript:void(0)" onclick="jQuery(this).parent().toggleClass('active');"><?=$special1["code"]?> - <?=$special1["name"]?></a>
                <ul>
                <?
            foreach($MODULE_OUTPUT["code1"] as $code) {

            if(($code["education"]=='�������')&&($code["spec_code"]=='190601')&&($code["spec_type"]=='65')){
            ?>


                <li>
                    <? if(isset($code["descr"])){
                ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["descr"]?></a><span>  ����������: <?=$code["p_n"]?></span>

            <?} else{       ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["name"]?></a> <span>  ����������: <?=$code["p_n"]?> </span> <? }?>


                </li>
            <? }}?>

            </ul>



                </li>


            <?

        }
        }?>

        </ul>




    <h2>�����������</h2>
        <ul>

        <? foreach($MODULE_OUTPUT["specialities"] as $special1) {
            if($special1["code"]=='110800'){?>

                <li><a href="javascript:void(0)" onclick="jQuery(this).parent().toggleClass('active');"><?=$special1["code"]?> - <?=$special1["name"]?></a>
                <ul>
                <?
            foreach($MODULE_OUTPUT["code1"] as $code) {

            if(($code["education"]=='�������')&&($code["spec_code"]=='110800')&&($code["spec_type"]=='62')){
            ?>


                <li>
                    <? if(isset($code["descr"])){
                ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["descr"]?></a><span>  ����������: <?=$code["p_n"]?></span>

            <?} else{       ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["name"]?></a> <span>  ����������: <?=$code["p_n"]?> </span> <? }?>


                </li>

                <?} }?>
            </ul>



                </li>


            <?

        }

            if($special1["code"]=='190600'){?>

                <li><a href="javascript:void(0)" onclick="jQuery(this).parent().toggleClass('active');"><?=$special1["code"]?> - <?=$special1["name"]?></a>
                    <ul>
                <?
            foreach($MODULE_OUTPUT["code1"] as $code) {

            if(($code["education"]=='�������')&&($code["spec_code"]=='190600')&&($code["spec_type"]=='62')){
            ?>


        <li>
                    <? if(isset($code["descr"])){
                ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["descr"]?></a><span>  ����������: <?=$code["p_n"]?></span>

            <?} else{       ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["name"]?></a> <span>  ����������: <?=$code["p_n"]?> </span> <? }?>


                </li>
                <?} }?>

            </ul>



                </li>


            <?

        }
        }?>

        </ul>







    <?
    }


            break;


         case "izop_bio":{?>
            <h2>�����������</h2>
        <ul>

        <? foreach($MODULE_OUTPUT["specialities"] as $special1) {
            if($special1["code"]=='200503'){?>

                <li><a href="javascript:void(0)" onclick="jQuery(this).parent().toggleClass('active');"><?=$special1["code"]?> - <?=$special1["name"]?></a>
                <ul>
                <?
            foreach($MODULE_OUTPUT["code1"] as $code) {

            if(($code["education"]=='�������')&&($code["spec_code"]=='200503')&&($code["spec_type"]=='65')){
            ?>


                <li>
                    <? if(isset($code["descr"])){
                ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["descr"]?></a><span>  ����������: <?=$code["p_n"]?></span>

            <?} else{       ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["name"]?></a> <span>  ����������: <?=$code["p_n"]?> </span> <? }?>


                </li>
                <?} }?>

            </ul>



                </li>


            <?
            }
            if($special1["code"]=='110400'){?>

                <li><a href="javascript:void(0)" onclick="jQuery(this).parent().toggleClass('active');"><?=$special1["code"]?> - <?=$special1["name"]?></a>
                <ul>
                <?
            foreach($MODULE_OUTPUT["code1"] as $code) {

            if(($code["education"]=='�������')&&($code["spec_code"]=='110400')&&($code["spec_type"]=='65')){
            ?>


                <li>
                    <? if(isset($code["descr"])){
                ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["descr"]?></a><span>  ����������: <?=$code["p_n"]?></span>

            <?} else{       ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["name"]?></a> <span>  ����������: <?=$code["p_n"]?> </span> <? }?>


                </li>

                <?} }?>
            </ul>



                </li>


            <?

        }


            if($special1["code"]=='110305'){?>

                <li><a href="javascript:void(0)" onclick="jQuery(this).parent().toggleClass('active');"><?=$special1["code"]?> - <?=$special1["name"]?></a>
                <ul>
                <?
            foreach($MODULE_OUTPUT["code1"] as $code) {

            if(($code["education"]=='�������')&&($code["spec_code"]=='110305')&&($code["spec_type"]=='65')){
            ?>


                <li>
                    <? if(isset($code["descr"])){
                ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["descr"]?></a><span>  ����������: <?=$code["p_n"]?></span>

            <?} else{       ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["name"]?></a> <span>  ����������: <?=$code["p_n"]?> </span> <? }?>


                </li>
            <?} }?>

            </ul>



                </li>


            <?

        }

        if($special1["code"]=='260501'){?>

                <li><a href="javascript:void(0)" onclick="jQuery(this).parent().toggleClass('active');"><?=$special1["code"]?> - <?=$special1["name"]?></a>
                <ul>
                <?
            foreach($MODULE_OUTPUT["code1"] as $code) {

            if(($code["education"]=='�������')&&($code["spec_code"]=='260501')&&($code["spec_type"]=='65')){
            ?>


                <li>
                    <? if(isset($code["descr"])){
                ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["descr"]?></a><span>  ����������: <?=$code["p_n"]?></span>

            <?} else{       ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["name"]?></a> <span>  ����������: <?=$code["p_n"]?> </span> <? }?>


                </li>
                <?} }?>

            </ul>



                </li>


            <?

        }

        if($special1["code"]=='110201'){?>

                <li><a href="javascript:void(0)" onclick="jQuery(this).parent().toggleClass('active');"><?=$special1["code"]?> - <?=$special1["name"]?></a>
                <ul>
                <?
            foreach($MODULE_OUTPUT["code1"] as $code) {

            if(($code["education"]=='�������')&&($code["spec_code"]=='110201')&&($code["spec_type"]=='65')){
            ?>


                <li>
                    <? if(isset($code["descr"])){
                ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["descr"]?></a><span>  ����������: <?=$code["p_n"]?></span>

            <?} else{       ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["name"]?></a> <span>  ����������: <?=$code["p_n"]?> </span> <? }?>


                </li>

                <?} }?>
            </ul>



                </li>


            <?

        }

        if($special1["code"]=='250203'){?>

                <li><a href="javascript:void(0)" onclick="jQuery(this).parent().toggleClass('active');"><?=$special1["code"]?> - <?=$special1["name"]?></a>
                <ul>
                <?
            foreach($MODULE_OUTPUT["code1"] as $code) {

            if(($code["education"]=='�������')&&($code["spec_code"]=='250203')&&($code["spec_type"]=='65')){
            ?>


                <li>
                    <? if(isset($code["descr"])){
                ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["descr"]?></a><span>  ����������: <?=$code["p_n"]?></span>

            <?} else{       ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["name"]?></a> <span>  ����������: <?=$code["p_n"]?> </span> <? }?>


                </li>
                <?} }?>

            </ul>



                </li>


            <?

        }
        if($special1["code"]=='111201'){?>

                <li><a href="javascript:void(0)" onclick="jQuery(this).parent().toggleClass('active');"><?=$special1["code"]?> - <?=$special1["name"]?></a>
                <ul>
                <?
            foreach($MODULE_OUTPUT["code1"] as $code) {

            if(($code["education"]=='�������')&&($code["spec_code"]=='111201')&&($code["spec_type"]=='65')){
            ?>


                <li>
                    <? if(isset($code["descr"])){
                ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["descr"]?></a><span>  ����������: <?=$code["p_n"]?></span>

            <?} else{       ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["name"]?></a> <span>  ����������: <?=$code["p_n"]?> </span> <? }?>


                </li>
                <?} }?>

            </ul>



                </li>


            <?

        }

        }
        ?>

        </ul>








    <h2>�����������</h2>
        <ul>

        <? foreach($MODULE_OUTPUT["specialities"] as $special1) {
            if($special1["code"]=='110400'){?>

                <li><a href="javascript:void(0)" onclick="jQuery(this).parent().toggleClass('active');"><?=$special1["code"]?> - <?=$special1["name"]?></a>
                <ul>
                <?
            foreach($MODULE_OUTPUT["code1"] as $code) {

            if(($code["education"]=='�������')&&($code["spec_code"]=='110400')&&($code["spec_type"]=='62')){
            ?>


                <li>
                    <? if(isset($code["descr"])){
                ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["descr"]?></a><span>  ����������: <?=$code["p_n"]?></span>

            <?} else{       ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["name"]?></a> <span>  ����������: <?=$code["p_n"]?> </span> <? }?>


                </li>

                <?}
                }?>
            </ul>



                </li>


            <?

        }

        if($special1["code"]=='250700'){?>

                <li><a href="javascript:void(0)" onclick="jQuery(this).parent().toggleClass('active');"><?=$special1["code"]?> - <?=$special1["name"]?></a>
                <ul>
                <?
            foreach($MODULE_OUTPUT["code1"] as $code) {

            if(($code["education"]=='�������')&&($code["spec_code"]=='250700')&&($code["spec_type"]=='62')){
            ?>


                <li>
                    <? if(isset($code["descr"])){
                ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["descr"]?></a><span>  ����������: <?=$code["p_n"]?></span>

            <?} else{       ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["name"]?></a> <span>  ����������: <?=$code["p_n"]?> </span> <? }?>


                </li>
                <?} }?>

            </ul>



                </li>


            <?

        }
            if($special1["code"]=='111900'){?>

                <li><a href="javascript:void(0)" onclick="jQuery(this).parent().toggleClass('active');"><?=$special1["code"]?> - <?=$special1["name"]?></a>
                <ul>
                <?
            foreach($MODULE_OUTPUT["code1"] as $code) {

            if(($code["education"]=='�������')&&($code["spec_code"]=='111900')&&($code["spec_type"]=='62')){
            ?>


                <li>
                    <? if(isset($code["descr"])){
                ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["descr"]?></a><span>  ����������: <?=$code["p_n"]?></span>

            <?} else{       ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["name"]?></a> <span>  ����������: <?=$code["p_n"]?> </span> <? }?>


                </li>
                <?} }?>

            </ul>



                </li>


            <?

            }

                if($special1["code"]=='250100'){?>

                <li><a href="javascript:void(0)" onclick="jQuery(this).parent().toggleClass('active');"><?=$special1["code"]?> - <?=$special1["name"]?></a>
                <ul>
                <?
            foreach($MODULE_OUTPUT["code1"] as $code) {

            if(($code["education"]=='�������')&&($code["spec_code"]=='250100')&&($code["spec_type"]=='62')){
            ?>


                <li>
                    <? if(isset($code["descr"])){
                ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["descr"]?></a><span>  ����������: <?=$code["p_n"]?></span>

            <?} else{       ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["name"]?></a> <span>  ����������: <?=$code["p_n"]?> </span> <? }?>


                </li>

                <?} }?>
            </ul>



                </li>


            <?

        }

                if($special1["code"]=='221700'){?>

                <li><a href="javascript:void(0)" onclick="jQuery(this).parent().toggleClass('active');"><?=$special1["code"]?> - <?=$special1["name"]?></a>
                <ul>
                <?
            foreach($MODULE_OUTPUT["code1"] as $code) {

                if(($code["education"]=='�������')&&($code["spec_code"]=='221700')&&($code["spec_type"]=='62')){
            ?>


            <li>
                    <? if(isset($code["descr"])){
                ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["descr"]?></a><span>  ����������: <?=$code["p_n"]?></span>

            <?} else{       ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["name"]?></a> <span>  ����������: <?=$code["p_n"]?> </span> <? }?>


                </li>
            <?  }
            }?>

            </ul>



                </li>


            <?

            }

                if($special1["code"]=='110900'){?>

                <li><a href="javascript:void(0)" onclick="jQuery(this).parent().toggleClass('active');"><?=$special1["code"]?> - <?=$special1["name"]?></a>
                <ul>
                <?
            foreach($MODULE_OUTPUT["code1"] as $code) {

            if(($code["education"]=='�������')&&($code["spec_code"]=='110900')&&($code["spec_type"]=='62')){
            ?>


                <li>
                    <? if(isset($code["descr"])){
                ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["descr"]?></a><span>  ����������: <?=$code["p_n"]?></span>

            <?} else{       ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["name"]?></a> <span>  ����������: <?=$code["p_n"]?> </span> <? }?>


                </li>
                <?} }?>

            </ul>



                </li>


            <?

        }

            if($special1["code"]=='260800'){?>

                <li><a href="javascript:void(0)" onclick="jQuery(this).parent().toggleClass('active');"><?=$special1["code"]?> - <?=$special1["name"]?></a>
                <ul>
                <?
            foreach($MODULE_OUTPUT["code1"] as $code) {

            if(($code["education"]=='�������')&&($code["spec_code"]=='260800')&&($code["spec_type"]=='62')){
            ?>


                <li>
                    <? if(isset($code["descr"])){
                ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["descr"]?></a><span>  ����������: <?=$code["p_n"]?></span>

            <?} else{       ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["name"]?></a> <span>  ����������: <?=$code["p_n"]?> </span> <? }?>


                </li>
                <?} }?>

            </ul>



                </li>


            <?

        }

        if($special1["code"]=='111100'){?>

                <li><a href="javascript:void(0)" onclick="jQuery(this).parent().toggleClass('active');"><?=$special1["code"]?> - <?=$special1["name"]?></a>
                <ul>
                <?
            foreach($MODULE_OUTPUT["code1"] as $code) {

            if(($code["education"]=='�������')&&($code["spec_code"]=='111100')&&($code["spec_type"]=='62')){
            ?>


                <li>
                    <? if(isset($code["descr"])){
                ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["descr"]?></a><span>  ����������: <?=$code["p_n"]?></span>

            <?} else{       ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["name"]?></a> <span>  ����������: <?=$code["p_n"]?> </span> <? }?>


                </li>

                    <?} }?>
            </ul>



                </li>


            <?

        }

        }?>

        </ul>


    <?
    }
    break;

        case "izop_government":{?>
            <h2>�����������</h2>
        <ul>

        <? foreach($MODULE_OUTPUT["specialities"] as $special1) {
            if($special1["code"]=='080504'){?>

                <li><a href="javascript:void(0)" onclick="jQuery(this).parent().toggleClass('active');"><?=$special1["code"]?> - <?=$special1["name"]?></a>
                <ul>
                <?
            foreach($MODULE_OUTPUT["code1"] as $code) {

            if(($code["education"]=='�������')&&($code["spec_code"]=='080504')&&($code["spec_type"]=='65')){
            ?>


                <li>
                    <? if(isset($code["descr"])&&($code["descr"]!=NULL)){
                ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["descr"]?></a><span>  ����������: <?=$code["p_n"]?></span>

            <?} else{       ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["name"]?></a> <span>  ����������: <?=$code["p_n"]?> </span> <? }?>


                </li>




                <?}
            }
            ?>
                </ul>
                </li>


        <?


        }

        if($special1["code"]=='080505'){?>

                <li><a href="javascript:void(0)" onclick="jQuery(this).parent().toggleClass('active');"><?=$special1["code"]?> - <?=$special1["name"]?></a>
                <ul>
                <?
            foreach($MODULE_OUTPUT["code1"] as $code) {

            if(($code["education"]=='�������')&&($code["spec_code"]=='080505')&&($code["spec_type"]=='65')){
            ?>


                <li>
                    <? if(isset($code["descr"])){
                ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["descr"]?></a><span>  ����������: <?=$code["p_n"]?></span>

            <?} else{       ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["name"]?></a> <span>  ����������: <?=$code["p_n"]?> </span> <? }?>


                </li>
                <?} }?>

            </ul>



                </li>


            <?

        }
    }?>

        </ul>





    <h2>�����������</h2>
        <ul>

        <? foreach($MODULE_OUTPUT["specialities"] as $special1) {
            if($special1["code"]=='081100'){?>

                <li><a href="javascript:void(0)" onclick="jQuery(this).parent().toggleClass('active');"><?=$special1["code"]?> - <?=$special1["name"]?></a>
                <ul>
                <?
            foreach($MODULE_OUTPUT["code1"] as $code) {

            if(($code["education"]=='�������')&&($code["spec_code"]=='081100')&&($code["spec_type"]=='62')){
            ?>


                <li>
                    <? if(isset($code["descr"])){
                ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["descr"]?></a><span>  ����������: <?=$code["p_n"]?></span>

            <?} else{       ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["name"]?></a> <span>  ����������: <?=$code["p_n"]?> </span> <? }?>


                </li>
                <?} }?>

            </ul>



                </li>


            <?

        }


            if($special1["code"]=='080400'){?>

                <li><a href="javascript:void(0)" onclick="jQuery(this).parent().toggleClass('active');"><?=$special1["code"]?> - <?=$special1["name"]?></a>
                <ul>
                <?
            foreach($MODULE_OUTPUT["code1"] as $code) {

            if(($code["education"]=='�������')&&($code["spec_code"]=='080400')&&($code["spec_type"]=='62')){
            ?>


                <li>
                    <? if(isset($code["descr"])){
                ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["descr"]?></a><span>  ����������: <?=$code["p_n"]?></span>

            <?} else{       ?>
                <a target="_blank" href="/file/<?=$code["file_id"]?>/" > <?=$code["name"]?></a> <span>  ����������: <?=$code["p_n"]?> </span> <? }?>


                </li>

                        <?}
            }?>
            </ul>



                </li>


            <?

        }
        }?>

        </ul>


    <?
    }


            break;
    case "timetable_teachers": {
        $rate = array(0.25, 0.5, 0.75, 1, 1.25, 1.5);
        if(!isset($MODULE_OUTPUT["teachers_mode"])) {
                    if (isset($MODULE_OUTPUT["allow_handle"]) || count($MODULE_OUTPUT["department"])) {
                        if(isset($MODULE_OUTPUT["display_variant"]["add_item"]))
                            $display_variant = $MODULE_OUTPUT["display_variant"]["add_item"]; ?>
                    <form method="post" action="search_teacher/">
                        <fieldset>
                            <legend>����� ����� ������������ �������������</legend>
                            <div class="wide">
                                <p>
                                    <input type="text" name="search_teacher" class="input_long">
                                    <input type="submit" class="button" value="�����">
                                </p>
                            </div>
                        </fieldset>
                    </form>
            <?}
            //CF::Debug($MODULE_OUTPUT["timetable_teachers"]);
            if(isset($MODULE_OUTPUT["timetable_teachers"])) {
                $id = 0; ?>

<?if($Engine->OperationAllowed(5, "people.add.teacher", $Auth->user_id, $Auth->usergroup_id)) {?>           <a href="add/">�������� �������������</a> <?}?>

                <h2>������ ������:</h2>
        <ul>


<? foreach ($MODULE_OUTPUT["timetable_teachers2"] as $fac_id => $fac) { ?>
    <li><strong><?=$fac["fac_name"]?></strong>
        <ul>
<?php           foreach ($fac["departments"] as $dep_id => $dep) { ?>
            <li><?=!$dep["is_active"] ? "<span style='color: grey'>" : "<span>"?><?=$dep["dep_name"]?></span>
                <ul>
<?php               foreach($dep["teachers"] as $teachers) 
{

                    //if ($teachers['is_active']==1) 
                   // {
                        $DB->SetTable("nsau_people", "np");
                        $DB->AddCondFS("np.user_id", "=", $Auth->user_id);
                        $DB->AddTable("nsau_teachers", "nt");
                        $DB->AddCondFF("np.id", "=", "nt.people_id");
                        $DB->AddField("nt.department_id", "d_id");
                        $DB->AddField("np.status_id", "status_id");
                        $this_teacher = $DB->FetchAssoc($DB->Select(1));

                        $this_teacher_deps = explode(";", $this_teacher["d_id"]);
                        $t_d = explode(";", $teachers["department_id"]);
                        $dismissed_handle = false;
                        foreach($t_d as $val) 
                        {
                            $dismissed_handle += $Engine->OperationAllowed(34, "contracts.dismissed.handle", $val, $Auth->usergroup_id);
                            if($Engine->OperationAllowed(34, "contracts.dismissed.owndep.handle", -1, $Auth->usergroup_id) && (in_array($val, $this_teacher_deps))) {
                                $dismissed_handle = 1;
                            }
                        }

                        if (isset($MODULE_OUTPUT["allow_handle"][$dep_id]) && $MODULE_OUTPUT["allow_handle"][$dep_id])
                        { 
                            ?>
                            <li>
                                <?

                                ?>
                                <a href="/people/<?=$teachers["id"]?>/"><?php if($teachers["status"] == 9 || $teachers["status"] == 10) {?><span class="user_inactive"><?php } ?><?=$teachers["last_name"]?> <?=$teachers["name"]?> <?=$teachers["patronymic"]?><?php if($teachers["status"] == 9) {?></span><?php } ?></a>
                                <a href="edit/<?=$teachers["id"]?>/<?=$teachers["department_id"]?>/"><img src="/themes/images/edit.png" /></a>
                                <?if(
                                (($Auth->usergroup_id==1) || ($Engine->OperationAllowed(34, "contracts.handle", $Auth->user_id, $Auth->usergroup_id) || ($dismissed_handle && ($teachers["status"] == 9))) )
                                && !empty($teachers["user_id"])){?><a href="/office/effect/?user_id=<?=$teachers["user_id"]?>">[����������� ��������]</a><?}?>
                                <? /*<a href="delete/<?=$teachers["id"]?>/<?=$teachers["department_id"]?>/" onclick="return confirm('�� �������, ��� ������ ������� ��������� �������?');"><img src="/themes/images/delete.png" /></a>
                            */?>
                            </li>
                            <?php
                        } 
                        else 
                        { 
                            ?>
                            <li>
                                <a href="/people/<?=$teachers["id"]?>/"><?=$teachers["last_name"]?> <?=$teachers["name"]?> <?=$teachers["patronymic"]?></a>
                                <?if((($Auth->usergroup_id==1) || ($Engine->OperationAllowed(34, "contracts.handle", $Auth->user_id, $Auth->usergroup_id) || ($dismissed_handle && ($teachers["status"] == 9))) )
                                && !empty($teachers["user_id"])){?><a href="/office/effect/?user_id=<?=$teachers["user_id"]?>">&nbsp;[����������� ��������]</a><?}?>
                            </li>
                            <?php
                        }
                    //}


                    } 
                    ?>
                </ul>
            </li>
<?php           } ?>
        </ul>
    </li>
<?}?>

        </ul>
<?php       } ?>

<?php   if(isset($MODULE_OUTPUT["is_exist"])) {
            $exist = $MODULE_OUTPUT["is_exist"]["people"];
            //foreach($MODULE_OUTPUT["is_exist"] as $exist) { ?>
        <form method="post" name="is_exist">
            <fieldset>
                <p>
                    ������������� <?=$exist["last_name"]?>&nbsp;<?=(isset($exist["name"]) ? $exist["name"] : "")?>&nbsp;<?=(isset($exist["patronymic"]) ? $exist["patronymic"] : "")?><?=(isset($exist["dep_identic"]) ? ", ���������� � ��������:" : "")?>
<?php           if(isset($exist["dep_identic"])) {
                    foreach($exist["department"] as $id => $dep_name) { ?>
                    <?=$dep_name?>
<?php               }
                } ?>
                    ��� ����������. �� �������, ��� ����� �������� ��� ���?
                </p>
                <p>
                    <input type="hidden" value="<?=$exist["last_name"]?>" name="last_name" />
                    <input type="hidden" value="<?=$exist["name"]?>" name="first_name" />
                    <input type="hidden" value="<?=$exist["patronymic"]?>" name="patronymic" />
                    <input type="hidden" value="<?=$exist["male"]?>" name="male" />
                    <input type="hidden" value="<?=$MODULE_OUTPUT["is_exist"]["exp_full"]?>" name="exp_full" />
                    <input type="hidden" value="<?=$MODULE_OUTPUT["is_exist"]["exp_teach"]?>" name="exp_teach" />
                    <input type="hidden" value="<?=$MODULE_OUTPUT["is_exist"]["prad"]?>" name="prad" />
                    <textarea class="hidden" name="comment"><?=$MODULE_OUTPUT["is_exist"]["comment"]?></textarea>
                    <input type="hidden" value="<?=$MODULE_OUTPUT["is_exist"]["username"]?>" name="username" />
                    <input type="hidden" value="<?=$MODULE_OUTPUT["is_exist"]["email"]?>" name="email" />
                    <input type="hidden" value="<?=$MODULE_OUTPUT["is_exist"]["password1"]?>" name="password1" />
                    <input type="hidden" value="<?=$MODULE_OUTPUT["is_exist"]["password2"]?>" name="password2" />
<?php           if(isset($MODULE_OUTPUT["is_exist"]["department"])) {
                    $i = 0;
                    foreach($MODULE_OUTPUT["is_exist"]["department"] as $id => $dep_name) { ?>
                    <input type="hidden" value="<?=$dep_name?>" name="department[<?=$id?>]" />
<?php                   $i++;
                    }
                } ?>
                    <input type="submit" value="��" name="check" class="button" />
                    <input type="submit" value="���" name="check" class="button" />
                </p>
            </fieldset>
        </form>
<?php       //}
        }


    } else {
            switch($MODULE_OUTPUT["teachers_mode"]) {
                case "search_teacher":  {
                    //CF::Debug($MODULE_OUTPUT["search_teachers"]);
                    ?>
        <h2>������ ��������� ��������������:</h2>
<?php
                    //���� ������� ���.�������, �� ���:
                    if(isset($MODULE_OUTPUT["auth_usergroup"]) && $MODULE_OUTPUT["auth_usergroup"] == 6) { ?>
        <h3>(�������� ���, ������� ������ �������� � ����� �������)</h3>
        <form name="add_teachers" method="post">
            <ul>
<?php
                        $i=1;
                        foreach($MODULE_OUTPUT["search_teachers"] as $teacher) {
                            if($teacher["in_current_department"]) { ?>
                <li>[��� ��������] <?=$teacher["last_name"]?> <?=$teacher["name"]?> <?=$teacher["patronymic"]?></li>
<?php                       } else { ?>
                <li><input type="hidden" name="teachers_id[<?=$i?>][id]" value="<?=$teacher["id"]?>"><input type="checkbox" name="teachers_id[<?=$i?>][check]"> <?=$teacher["last_name"]?> <?=$teacher["name"]?> <?=$teacher["patronymic"]?></li>
<?php                           $i++;
                            }
                        } ?>
            </ul>
            <input type="hidden" name="department_id" value="<?=$MODULE_OUTPUT["current_department"]?>" />
            <input type="submit" value="��������� ���������" />
        </form>
<?php               } else { ?>
        <h3>�������� �������������:</h3>
        <ul>
<?php                   foreach($MODULE_OUTPUT["search_teachers"] as $teacher) { ?>
            <li><a href="/people/<?=$teacher["id"]?>/"><?=$teacher["last_name"]?> <?=$teacher["name"]?> <?=$teacher["patronymic"]?></a></li> 
<?php                   } ?>
        </ul>
<?php               }
                }
                break;

                case "edit": {
                    //CF::Debug($MODULE_OUTPUT);
                    foreach($MODULE_OUTPUT["edit_teacher"] as $teacher) { ?>
        <script>jQuery(document).ready(function($) {Send_Forms();});</script>
        <form name="edit_teacher" method="post" onsubmit="return sendform(this);" enctype="multipart/form-data">
            <fieldset>
                <legend>
                    �������������� ���������� � ������������� <?=$teacher["last_name"]?> <?=(isset($teacher["name"]) ? $teacher["name"] : "")?> <?=(isset($teacher["patronymic"]) ? $teacher["patronymic"] : "")?>
                </legend>
                <div class="wide">
                    <div class="form-notes">
                        <h3>������� �������������:</h3>
                        <p><input type="text" name="last_name" value="<?=$teacher["last_name"]?>" /></p>
                        <h3>��� �������������:</h3>
                        <p><input type="text" name="first_name" value="<?=$teacher["name"]?>" /></p>
                        <h3>�������� �������������:</h3>
                        <p><input type="text" name="patronymic" value="<?=$teacher["patronymic"]?>" /></p>
                        <h3>����������:</h3>
                        <p>
                            <span id="photoinput"><input name="edit_photo" type="file"/></span>
                            <input type="checkbox" name="delete_photo" onclick="if(this.checked) document.getElementById('photoinput').style.display='none'; else document.getElementById('photoinput').style.display='inline';" /> ������� ����������
                        </p>
                        <h3>���:</h3>
                        <p>
                            <input type="radio" name="male" value="1"<?=($teacher["male"] ? " checked=\"checked\"" : "")?> />� / �
                            <input type="radio" name="male" value="0"<?=($teacher["male"] ? "" : " checked=\"checked\"")?> />
                        </p>
                    </div>
                    <div class="form-notes">
                        <h3>���������, �������, ������:</h3>
                        <p><input type="text" name="prad" class="input_long" value="<?=$teacher["prad"]?>" /></p>
                        <h3>���������:</h3>
                        <p>
                            <select name="post" size="1" class="select_long">
                                <option value="����������"<?=($teacher['post'] == '����������' ? ' selected="selected"' : '')?>>����������</option>
                                <option value="�������������"<?=($teacher['post'] == '�������������' ? ' selected="selected"' : '')?>>�������������</option>
                                <option value="��������"<?=($teacher['post'] == '��������' ? ' selected="selected"' : '')?>>��������, ������� ��������, ����, ��������</option>
                            </select>
                        </p>
                        <h3>��� ������ ������ �����:</h3>
                        <p><input type="text" name="exp_full" value="<?=$teacher["exp_full"]?>" /></p>
                        <h3>��� ������ ���������������, ������-��������������� �����:</h3>
                        <p><input type="text" name="exp_teach" value="<?=$teacher["exp_teach"]?>" /></p>
                    </div>
                </div>
                <div class="wide">
                    <h3>�������������� ����������:</h3>
                    <p><textarea name="comment" class="wysiwyg" cols="50" rows="10"><?=$teacher["comment"]?></textarea></p>
                </div>
                <div class="wide">
<?php                   if(!isset($MODULE_OUTPUT["current_department_id"])) { ?>
                    <h3>�������� �������:</h3>
                    <p>
                        <input type="hidden" name="department[0][old]" value="<?=(isset($teacher["department"][0]) ? $teacher["department"][0] : "0")?>" />
                        <select name="department[0][new]" size="1">
<?php                       if(isset($teacher["department"][0])) { ?>
                            <option value="0">������� �� �������</option>
<?php                       } else { ?>
                            <option value="0" selected="selected">�������� �������:</option>
<?php                       }
                            foreach($MODULE_OUTPUT["faculties"] as $faculties) { ?>
                            <optgroup label="<?=$faculties["fac_name"]?>">
<?php                           foreach($faculties["departments"] as $department) { ?>
                                <option value="<?=$department["id"]?>"<?=((isset($teacher["department"][0]) && $teacher["department"][0] == $department["id"]) ? " selected=\"selected\"" : "")?>><?=$department["name"]?></option>
<?php                           } ?>
                            </optgroup>
<?php                       } ?>
                        </select>
                    </p>
                    <h3>�������� ������ �������(���� ����. � ���� ������ ��������� ������� ������ ���� ����������):</h3>
                    <p>
                        <input type="hidden" name="department[1][old]" value="<?=(isset($teacher["department"][1]) ? $teacher["department"][1] : "0")?>" />
                        <select name="department[1][new]" size="1">
<?php                       if(isset($teacher["department"][1])) { ?>
                            <option value="0">������� �� �������</option>
<?php                       } else { ?>
                            <option value="0" selected="selected">�������� �������:</option>
<?php                       }
                            foreach($MODULE_OUTPUT["faculties"] as $faculties) { ?>
                            <optgroup label="<?=$faculties["fac_name"]?>">
<?php                           foreach($faculties["departments"] as $department) { ?>
                                <option value="<?=$department["id"]?>"<?=((isset($teacher["department"][1]) && $teacher["department"][1] == $department["id"]) ? " selected=\"selected\"" : "")?>><?=$department["name"]?></option>
<?php                           } ?>
                            </optgroup>
<?php                       } ?>
                        </select>
                    </p>
<?php                   } else { ?>
                    <p>
                        <input type="hidden" name="department[0]" value="<?=$MODULE_OUTPUT["current_department_id"]?>">
                    </p>
<?php                   } ?>
                    </div>
                    <div class="wide">
                        <div class="form-notes">
                            <h3>��� ������������ ��� ������� � �������:</h3>
                            <p>
                                <input type="hidden" name="isset_user" value="<?=(isset($teacher["username"]) ? 1 : 0)?>" autocomplete="off" />
                                <input type="text" name="username" value="<?=(isset($teacher["username"]) ? $teacher["username"] : "")?>" autocomplete="off" />
                            </p>
                            <h3>����������� ����� (E-mail):</h3>
                            <p><input type="text" name="email" value="<?=(isset($teacher["email"]) ? $teacher["email"] : "")?>" autocomplete="off" /></p>
                        </div>
                        <div class="form-notes">
<?php                   //if(empty($teacher["password"])) { ?>
                            <h3>
                                ������:
                                <span id="genpass"></span>
                                <span id="copypass">
                                    <a onclick="copypass('genpass','add_pass','repeatpass');">
                                        ������������ ���� ������<span id="instead" class="instead"></span>
                                    </a>
                                </span>
                            </h3>
                            <p>
                                <input id="add_pass" name="password1" value="<?=(isset($teacher["password"])?$teacher["password"]:'')?>" type="password" autocomplete="off" />
                                <a onclick="passgen('genpass',3,1,1,'add_pass');">������������� ������</a>
                            </p>
                            <h3>��������� ������:</h3>
                            <p><input id="repeatpass" type="password" value="<?=(isset($teacher["password"])?$teacher["password"]:'')?>" name="password2" autocomplete="off" /></p>
<?php                   //} ?>
                        </div>
                    </div>
                    <div class="wide">
                        <p>
                            <input type="hidden" value="edit" name="mode" />
                            <input type="hidden" value="<?=$teacher["id"]?>" name="id" />
                            <input type="submit" value="���������" class="button" />
                        </p>
                    </div>
                </fieldset>
            </form>
<?php               }
                }
                break;
                    }
        }

    }
break;
    case "ajax_delete_subj": {
        if(isset($MODULE_OUTPUT["ajax_delete_subj"])) {
            header('Content-Type: text/xml,  charset=windows-1251');
            $dom = new DOMDocument("1.0", "windows-1251");
            $del_subj = $dom->createElement('del_subj');
            $dom->appendChild($del_subj);
            $status = $dom->createTextNode($MODULE_OUTPUT['ajax_delete_subj']);
            $del_subj->appendChild($status);
            if($MODULE_OUTPUT['error_msg']) {
                $error_msg  = $dom->createElement("error_msg");
                $msg = $dom->createTextNode(CF::Win2Utf($MODULE_OUTPUT['error_msg']));
                $msg = $dom->createCDATASection(CF::Win2Utf($MODULE_OUTPUT['error_msg']));
                $error_msg->appendChild($msg);
                $del_subj->appendChild($error_msg);
            }
            $xmlString = $dom->saveXML();
            echo $xmlString;
        }
    }
    break;

    case "timetable_subjects": {
        if(!isset($MODULE_OUTPUT["subjects_mode"])) {
            /*if(isset($MODULE_OUTPUT["subjects"])) {
                $id = 0; ?>
        <h2>������ ��������� �� ��������:</h2>
         <ul>
<?php           foreach($MODULE_OUTPUT["subjects"] as $subject) {
                    if($subject["department_id"] != $id) { ?>
        </ul>
        <li><?=$subject["department_name"]?><ul>
<?php                   $id = $subject["department_id"];
                    } ?>
        <li> <a href="edit/<?=$subject["id"]?>/"><?=$subject["name"]?></a> [<a href="delete/<?=$subject["id"]?>/" onclick="return confirm('�� �������, ��� ������ ������� ��������� �������?');"}>�������</a>]</li>
<?php           } ?>
        </ul>
<?php       }*/ ?>
        <script>jQuery(document).ready(function($) {TimetableSubjects(<?=$MODULE_OUTPUT['module_id']?>);});</script>

          <p><?=(isset($MODULE_OUTPUT["error"])) ? "<br /><h3>".$MODULE_MESSAGES[$MODULE_OUTPUT["error"]]."</h3>": ""?></p>
        <h2>�������� ����������:</h2>
        <form  method="post" name="add_subject">
            <h3>�������� ����������:</h3>
            <input type="text" name="subject_name" class="input_long" />
            <h3>�������� �������, � ������� ��������� ����������:</h3>
            <select name="department_id" size="1">
                <option value="0" selected="selected">�������� �������:</option>
<?php       /*foreach($MODULE_OUTPUT["departments"] as $department) {
                ?>
                <option value="<?=$department["id"]?>"><?=$department["name"]?></option>
                <?php
            }*/
            $id=0;
                        
            foreach($MODULE_OUTPUT["departments"] as $department) {
                if($department["faculty_id"]!=$id) {
                    if($id) { ?>
                    </optgroup>
<?php               } ?>
                    <optgroup label="<?=$department["faculty_name"]?>">
<?php               $id=$department["faculty_id"];
                } ?>
                <option value="<?=$department["id"]?>"><?=$department["name"]?></option>
<?php       } ?>
            </select><br />
            <input type="submit" value="��������" class="button" />
        </form>

        <br/>
<?php       if(isset($MODULE_OUTPUT["fac_dep_subj"])) {
                foreach($MODULE_OUTPUT["fac_dep_subj"] as $fac) {
                    if (!empty($fac["dep_list"])) { ?>
        <h2><?=$fac["fac_name"]?></h2>
<?php                   }
                    foreach($fac["dep_list"] as $dep) { ?>
        <h3><?=$dep["is_active"]!=1 ? "<span style='color: grey'>" : "<span>"?><?=$dep["dep_name"]?></span></h3>
        <ul class='subj_list'>
<?php                   foreach($dep["subj_list"] as $subj) { ?>
            <li id='subj_item_<?=$subj["subj_id"]?>'>
            <a class='dep<?=$dep["dep_id"]?>' href='edit/<?=$subj["subj_id"]?>/' <?=$subj["is_hidden"] ? "style='color: grey'" : ""?>><?=$subj["subj_name"]?></a>    <small>[<a href='delete/<?=$subj["subj_id"]?>/' class='delet_subj'>�������</a>]<?if(!$subj["is_hidden"]){?>[<a href='hide/<?=$subj["subj_id"]?>/' class='hide_subj'>������</a>]<?} else {?>[<a href='show/<?=$subj["subj_id"]?>/' class='show_subj'>��������</a>]<?}?></small>
            </li>
<?php                   } ?>
        </ul>
<?php               }
                }
            }
            if(isset($MODULE_OUTPUT["nodep_subj"])) { ?>
        <h2>����������, �� ����������� � �������</h2>
        <ul class='subj_list'>
<?php           foreach($MODULE_OUTPUT["nodep_subj"] as $nodep_subj) { ?>
            <li id='subj_item_<?=$nodep_subj["subj_id"]?>'>
                <a href='edit/<?=$nodep_subj["subj_id"]?>/'><?=$nodep_subj["subj_name"]?></a> <small>[<a href='delete/<?=$nodep_subj["subj_id"]?>/' class='delet_subj'>�������</a>]<?if(!$nodep_subj["is_hidden"]){?>[<a href='hide/<?=$nodep_subj["subj_id"]?>/' class='hide_subj'>������</a>]<?} else {?>[<a href='show/<?=$nodep_subj["subj_id"]?>/' class='show_subj'>��������</a>]<?}?></small>
            </li>
<?php           } ?>
        </ul>
<?php       } ?>

<?php   } else {
            switch($MODULE_OUTPUT["subjects_mode"]) {
                case "edit": {
                    foreach($MODULE_OUTPUT["edit_subject"] as $subject) { ?>
        <p><?=(isset($MODULE_OUTPUT["error"])) ? "<br /><h3 class='message'>".$MODULE_MESSAGES[$MODULE_OUTPUT["error"]]."</h3>": ""?></p>
        <h2>�������������� ���������� � ���������� <?=$subject["name"]?>:</h2>
        <form name="edit_subject" method="post">
            <h3>�������� ����������:</h3>
            <input type="text" name="subject_name" class="input_long" value="<?=$subject["name"]?>" />
            <h3>�������, � ������� ��������� ����������:</h3>
            <select name="department">
              <option value="0">�������, � ������� ��������� ����������:</option><?php $id=0;?>
<?php               if($subject["department_id"]!=0){
                                foreach($MODULE_OUTPUT["departments"] as $department) {
                                                                    if($department["faculty_id"]!=$fac_id) {?>
                                                                        <optgroup label="<?=$department["faculty_name"]?>"></optgroup>
                                                                        <? $fac_id = $department["faculty_id"];
                                                                    }
                                                                    if($department["faculty_id"]==$fac_id) {
                                                                        if($department["id"] == $subject["department_id"]) { ?>
                                                                            <option value="<?=$department["id"]?>" selected="selected"><?=$department["name"]?></option>
                                                                        <?} else  { ?>
                                                                            <option value="<?=$department["id"]?>"><?=$department["name"]?></option>
<?php                                   }
                                                                    }
                                                                }
                            } else {
                                $facs = array();
                                foreach($MODULE_OUTPUT["departments"] as $department) {
                                    if(!in_array($department["faculty_id"], $facs)) {
                                        if(($department["id"] == $subject["department_id"]) || ($subject["department_id"]==0)) {?>
                                            <optgroup label="<?=$department["faculty_name"]?>"></optgroup>
                                        <? $fac_id = $department["faculty_id"];
                                                $facs[] = $fac_id;
                                        }

                                        foreach($MODULE_OUTPUT["departments"] as $department) {
                                            if($department["faculty_id"]==$fac_id) {
                                                if($department["id"] == $subject["department_id"]) { ?>
                                                    <option value="<?=$department["id"]?>" selected="selected"><?=$department["name"]?></option>
                                                <?} else  { ?>
                                                    <option value="<?=$department["id"]?>"><?=$department["name"]?></option>
        <?php                           }
                                            }
                                        }
                                    }
                                }
                            }
                                ?>

            </select><br />
                        <h3>��������� ��� ��������� �� ������ ����������:</h3>
                            <?if($MODULE_OUTPUT["allow_transfer"]) {?>
                                <select name="move_binding" class="to_select <?=$subject["department_id"]?>">
                                    <?foreach($MODULE_OUTPUT["departments"] as $ppc) {?>
                                        <?if($ppc["faculty_name"]!=$mda){?><optgroup label="<?=$ppc["faculty_name"]?>"></optgroup><?$mda = $ppc["faculty_name"];}?>
                                            <optgroup label="<?=$ppc["name"]?>">
                                                <?foreach($MODULE_OUTPUT["all_subjects"][$ppc["id"]] as $subj) {
                                                    if($subject["id"]==$subj["id"]) {?>
                                                        <option value="<?=$subj["id"]?>" selected="selected" class="from_id <?=$subject["id"]?>"><?=$subj["name"]?></option>
                                                    <?} else {?>
                                                    <option value="<?=$subj["id"]?>"><?=$subj["name"]?></option><?}?>
                                                <?}?>
                                            </optgroup>
                                    <?}?>
                                </select>
                            <?} else {?>
                                <select name="move_binding" class="to_select <?=$subject["department_id"]?>">
                                    <?foreach($MODULE_OUTPUT["subjects"] as $subj) {
                                        if($subject["id"]==$subj["id"]) {?>
                                            <option value="<?=$subj["id"]?>" selected="selected" class="from_id <?=$subject["id"]?>"><?=$subj["name"]?></option>
                                        <?} else {?>
                                        <option value="<?=$subj["id"]?>"><?=$subj["name"]?></option><?}?>
                                    <?}?>
                                </select><input type="submit" id="move_subj_attach" value="���������" class="button" />
                            <?}?>
                            <input type="submit" id="move_subj_attach" value="���������" class="button" />
                            <div class="move_log hidden">
                                <span class="move_tmp"></span>
                                <input type="submit" value="��" id="move_ok" class="button" />
                                <input type="submit" value="������" id="move_cancel" class="button" />
                                <br>
                            </div>
                            <div class="output_msg hidden"></div>
                            <table class="tmp_table hidden" style="width: 100%">
                                <tr>
                                    <th>����������</th>
                                    <th>�������������</th>
                                    <th>���</th>
                                    <th>�����������</th>
                                    <th class="tmp_th">������ ���������</th>
                                </tr>
                                <tr class="tmp_table_tr hidden" style="background-color: #def" height="20px">
                                    <td class="td_subj" style="width: 15%"></td>
                                    <td class="td_spec" style="width: 35%"></td>
                                    <td class="td_code" style="width: 7%"></td>
                                    <td class="td_direc" style="width: 8%"></td>
                                    <td class="td_group" style="width: 35%"></td>
                                </tr>
                            </table>
                            <table class="tmp_table_file hidden" style="width: 100%">
                                <tr>
                                    <th>����������</th>
                                    <th>�������������</th>
                                    <th>��� �������������</th>
                                    <th>��� �����</th>
                                    <th>���� ��������</th>
                                </tr>
                                <tr class="tmp_table_tr hidden" style="background-color: #def" height="20px">
                                    <td class="td_subj" style="width: 15%"></td>
                                    <td class="td_spec" style="width: 35%"></td>
                                    <td class="td_code" style="width: 15%"></td>
                                    <td class="td_name" style="width: 20%"></td>
                                    <td class="td_user" style="width: 15%"></td>
                                </tr>
                            </table>
                        <br>
            <input type="hidden" value="edit" name="mode" />
            <input type="hidden" value="<?=$subject["id"]?>" name="id" />
            <input type="submit" id="save_button" value="���������" class="button" />
        </form>
                <pre>
                <?//print_r($MODULE_OUTPUT);?>
                </pre>
<?php               }
                }
                break;
            }
        }
    }
    break;

    case "timetable_auditorium": { ?>
        <script>jQuery(document).ready(function($) {TimetableAuditorium();});</script>
        <!--script type="text/javascript" src="<?=HTTP_COMMON_SCRIPTS?>jquery.editable-select.js"></script-->
<?php   $type = array("��������", "��������", "�������� �����������");
        $using = array("������������", "������", "��");
        if(!isset($MODULE_OUTPUT["auditorium_mode"])) {?>
                <h2>�������� ���������:</h2>
        <form onsubmit="$(this).find('input.editable-select').attr('name', '');" method="post" name="add_auditorium">
            <h3>����� ���������:</h3>
            <input type="text" name="auditorium_name" />
            <h3>�������� ������, � ������� ����������� ������ ���������:</h3>
            <select name="building_id" size="1">
                <option value="0" selected="selected">�������� ������:</option>
<?php       foreach($MODULE_OUTPUT["buildings"] as $building) { ?>
                <option value="<?=$building["id"]?>"><?=$building["name"]?></option>
<?php       } ?>
            </select>
            <h3>������� ��������������� ���������:</h3>
            <input type="text" name="roominess" />
            <h3>�������, � ������� ��������� ��������� (���� ����):</h3>
            <select name="department_id" size="1" class="editable-select">
                <option value="0" selected="selected">�������� �������:</option>
<?php       foreach($MODULE_OUTPUT["departments"] as $department) { ?>
                <option value="<?=$department["id"]?>"><?=$department["name"]?></option>
<?php       } ?>
            </select>
            <h3>�������� ��� ���������:</h3>
            <select name="type" size="1">
<?php       for($i=0; $i<3; $i++) { ?>
                <option value="<?=$i?>"><?=$type[$i]?></option>
<?php       } ?>
            </select>
            <h3>�������� ��������� ���������:</h3>
            <select name="is_using" size="1">
<?php       for($i=0; $i<3; $i++) { ?>
                <option value="<?=$i?>"><?=$using[$i]?></option>
<?php       } ?>
            </select><br />
            <input type="checkbox" name="multimedia" value="1" /> ���������� �������������� ������������<br />
                        <input type="checkbox" name="for_request" value="1" /> �������� ������ ������ �� �������������� ������������<br />
            <input  type="submit" value="��������" class="button" />
        </form><br>
                <? if(isset($MODULE_OUTPUT["auditorium"])) {
                $id = 0; ?>
        <h2>������ ��������� �� ��������:</h2>
        <ul>
<?php           foreach($MODULE_OUTPUT["auditorium"] as $build) { ?>
            <li>
                <?=$build['bild_name']?>
<?php               if(isset($build['bild_audit'])) { ?>
                <ul>
<?php                   foreach($build['bild_audit'] as $audit) { ?>
                    <li>
                        <a href="edit/<?=$audit["id"]?>/"<?php if($audit["multimedia"]) {?> title="����������� �������������� ������������"<?php } ?>><?=$build["bild_label"]?> - <?=$audit["name"]?></a> ��������������: <?=$audit["roominess"]?>;&nbsp;
<?php                       for($i=0; $i<3; $i++) {
                                if($i == $audit["type"]) { ?>
                        ��� ���������: <?=$type[$i]?>;
<?php                           }
                            } ?>
                        ���������: <?=$using[$audit["is_using"]]?>;&nbsp;
                        <?php if($audit["multimedia"]) {?>�����������: ����;<?php } ?><?php if($audit["for_request"]) {?> ������ ������: ��;<?php } ?>
                        [<a href="delete/<?=$audit["id"]?>/" onclick="return confirm('�� �������, ��� ������ ������� ��������� �������?');"}>�������</a>]
                    </li>
<?php                   } ?>
                </ul>
<?php               } ?>
            </li>
<?php           } ?>

        </ul>
<?php       } ?>
<?php   } else {
            switch($MODULE_OUTPUT["auditorium_mode"]) {
                case "edit": {
                    foreach($MODULE_OUTPUT["edit_auditorium"] as $auditorium) { ?>
        <h2>�������������� ���������� �� ��������� <?=$auditorium["name"]?>:</h2>
        <form name="edit_auditorium" method="post">
            <h3>����� ���������:</h3>
            <input type="text" name="auditorium_name" value="<?=$auditorium["name"]?>" />
            <h3>������, � ������� ����������� ���������:</h3>
            <select name="building">
<?php                   foreach($MODULE_OUTPUT["buildings"] as $building) {
                            if($building["id"] == $auditorium["building_id"]) { ?>
                <option value="<?=$building["id"]?>" selected="selected"><?=$building["name"]?></option>
<?php                       } else { ?>
                <option value="<?=$building["id"]?>"><?=$building["name"]?></option>
<?php                       }
                        } ?>
            </select>
            <h3>������� ��������������� ���������:</h3>
            <input type="text" name="roominess" value="<?=$auditorium["roominess"]?>" />
            <h3>�������, � ������� ��������� ��������� (���� ����):</h3>
            <select name="department_id" class="editable-select">
<?php                   if(!$auditorium["department_id"]) { ?>
                <option selected="selected" value="0">�������� �������</option>
<?php                   } else { ?>
                <option value="0">��� �������</option>
<?php                   }
                        foreach($MODULE_OUTPUT["departments"] as $department) {
                            if($department["id"] == $auditorium["department_id"]) { ?>
                <option selected="selected" value="<?=$department["id"]?>"><?=$department["name"]?></option>
<?php                       } else { ?>
                <option value="<?=$department["id"]?>"><?=$department["name"]?></option>
<?php                       }
                        } ?>
            </select>
            <h3>��� ���������:</h3>
            <select name="type" size="1">
<?php                   for($i=0; $i<3; $i++) {
                            if($i == $auditorium["type"]) { ?>
                <option value="<?=$i?>" selected="selected"><?=$type[$i]?></option>
<?php                       } else { ?>
                <option value="<?=$i?>"><?=$type[$i]?></option>
<?php                       }
                        } ?>
            </select>
            <h3>��������� ���������:</h3>
            <select name="is_using" size="1">
<?php                   for($i=0; $i<3; $i++) {
                            if($i == $auditorium["is_using"]) { ?>
                <option value="<?=$i?>" selected="selected"><?=$using[$i]?></option>
<?php                       } else { ?>
                <option value="<?=$i?>"><?=$using[$i]?></option>
<?php                       }
                        } ?>
            </select><br />
            <input type="checkbox" name="multimedia" value="1" <?=($auditorium["multimedia"] ? " checked=\"checked\"" : "") ?>/> ���������� �������������� ������������<br />
                        <input type="checkbox" name="for_request" value="1" <?=($auditorium["for_request"] ? " checked=\"checked\"" : "") ?>/> �������� ������ ������ �� �������������� ������������<br />
            <input type="hidden" value="edit" name="mode" />
            <input type="hidden" value="<?=$auditorium["id"]?>" name="id" />
            <input type="submit" value="���������" class="button" />
        </form>
<?php               }
                }
                break;
            }
        }
    }
    break;

    /*case "timetable_show": { ?>
        <h1>����������</h1>
        <div class="faculties_groups_block">
<?php   foreach($MODULE_OUTPUT["faculties"] as $faculty) { ?>
            <div class="faculties_groups">
                <h2><span><?=$faculty["name"]?></span><em></em></h2>
                <div class="table_groups">
                    <table>
                        <thead>
                            <tr>
                                <th>����</th>
                                <th colspan="6">������</th>
                            </tr>
                        </thead>
                        <tbody>
<?php       for($i = 1; $i < 7; $i++) { ?>
                            <tr>
                                <th><?=$i?></th>
<?php           $td_num = 0;
                if(isset($faculty["groups"]) && isset($faculty["groups"][$i])) {
                    foreach($faculty["groups"][$i] as $group) {
                        $td_num++; ?>
                                <td><a href="groups/<?=$group["id"]?>/"><?=$group["name"]?></a></td>
<?php               }
                }
                for(;$td_num < 6; $td_num++) { ?>
                                <td></td>
<?php           } ?>
                            </tr>
<?php       } ?>
                        </tbody>
                    </table>
                </div>
            </div>
<?php   } ?>
        </div>
<?php   break;
    }*/

    case "groups_list": { ?>
        <script>jQuery(document).ready(function($) {Send_Forms();});</script>
<?php   if(isset($MODULE_OUTPUT["faculty_list"])) { ?>
        <h2>���������� ������:</h2>
        <form name="new_group" method="post">
            <h3>������� ����� ������:</h3>
            <input type="text" name="group_name" />
            <h3>�������� ���������</h3>
            <select name="faculty">
<?php       foreach($MODULE_OUTPUT["faculty_list"] as $faculty) { ?>
                <option value="<?=$faculty["id"]?>"><?=$faculty["name"]?></option>
<?php       } ?>
            </select><br />
            <h3>����</h3>
            <select name="year">
                <option value="1">������ ����</option>
                <option value="2">������ ����</option>
                <option value="3">������ ����</option>
                <option value="4">�������� ����</option>
                <option value="5">����� ����</option>
                <option value="6">������ ����</option>
            </select><br />
            <input type="submit" value="�������� ������" />
        </form>
<?php   }
        if(isset($MODULE_OUTPUT["faculties"])) { ?>
        <a href="add_group/">�������� ������.</a>
        <div class="faculties_groups_block">
<?php   foreach($MODULE_OUTPUT["faculties"] as $faculty) { ?>
            <div class="faculties_groups">
                <h2><span><?=$faculty["name"]?></span><em></em></h2>
                <div class="table_groups">
                    <table>
                        <thead>
                            <tr>
                                <th>����</th>
                                <th colspan="6">������</th>
                            </tr>
                        </thead>
                        <tbody>
<?php       for($i = 1; $i < 7; $i++) { ?>
                            <tr>
                                <th><?=$i?></th>
<?php           $td_num = 0;
                if(isset($faculty["groups"]) && isset($faculty["groups"][$i])) {
                    foreach($faculty["groups"][$i] as $group) {
                        $td_num++; ?>
                                <td><a href="<?=$group["id"]?>/"><?=$group["name"]?></a></td>
<?php               }
                }
                for(;$td_num < 6; $td_num++) { ?>
                                <td></td>
<?php           } ?>
                            </tr>
<?php       } ?>
                        </tbody>
                    </table>
                </div>
            </div>
<?php   } ?>
        </div>
<?php   }
        if(isset($MODULE_OUTPUT["students_list"]) || isset($MODULE_OUTPUT["students_list_mode"])) { ?>
        <a href="edit_group/">������������� ������.</a><br />
        <a href="delete_group/" onclick="return confirm('�� �������, ��� ������ ������� ��������� �������?');">������� ������(��� �������� ������� ������ ����� ���������� ���� ��������� ��� �������� �� � ������ �����).</a>
        <h2>������ ���������
<?php       if(isset($MODULE_OUTPUT["group_name"])) {
                echo ", ���������� � ������ ";
                echo $MODULE_OUTPUT["group_name"];
            }
            if(isset($MODULE_OUTPUT["faculty_name"])) {
                echo "(".$MODULE_OUTPUT["faculty_name"].")";
            } ?>
            :
        </h2>
<?php       if(isset($MODULE_OUTPUT["students_list"])) {
                foreach($MODULE_OUTPUT["students_list"] as $student) { ?>
        <a href="<?=$student["id"]?>/"><?=$student["last_name"]?> <?=$student["name"]?> <?=$student["patronymic"]?></a><br />
<?php               $id_speciality = $student["id_speciality"];
                }
            } ?>
        <h2>���������� �������� � ������:</h2>
        <form name="add_student" method="post" onsubmit="return sendform(this);">
            <h3>�������:</h3>
            <input type="text" name="last_name" />
            <h3>���:</h3>
            <input type="text" name="first_name" />
            <h3>��������:</h3>
            <input type="text" name="patronymic" />
            <input type="hidden" name="group_id" value="<?=$MODULE_OUTPUT["group_id"]?>" />
            <h3>���:</h3>
            <input type="radio" value="1" name="male" />� / �<input type="radio" value="0" name="male" />
            <h3>�������������:</h3>
            <select name="id_speciality">
<?php       if(isset($MODULE_OUTPUT["specialities"])) {
                if(!isset($id_speciality)) { ?>
                <option value="0" selected="selected" disabled="disabled">�������� �������������</option>
<?php           }
                foreach($MODULE_OUTPUT["specialities"] as $speciality) {
                    if(isset($id_speciality)) {
                        if($id_speciality == $speciality["code"]) { ?>
                <option value="<?=$id_speciality?>" selected="selected"><?=$speciality["name"]?> (<?=$speciality["code"]?>)</option>
<?php                   } else { ?>
                <option value="<?=$speciality["code"]?>"><?=$speciality["name"]?> (<?=$speciality["code"]?>)</option>
<?php                   }
                    } else { ?>
                <option value="<?=$speciality["code"]?>"><?=$speciality["name"]?> (<?=$speciality["code"]?>)</option>
<?php               }
                }
            } ?>
            </select>
            <h3>��� �����������:</h3>
            <input type="text" name="year" />
            <h3>��������� (���� ����):</h3>
            <input type="radio" value="�" name="subgroup" />� / �<input type="radio" value="�" name="subgroup" />
            <h3>��� ������������ ��� ������� � �������:</h3>
            <input type="text" name="username" />
            <h3>����������� ����� (E-mail):</h3>
            <input type="text" name="email" />
            <h3>������:</h3>
            <input type="password" name="password1" />
            <h3>��������� ������:</h3>
            <input type="password" name="password2" /><br />
            <input type="submit" value="�������� �������� � ������" class="button" />
        </fomr>
<?php   }
        if(isset($MODULE_OUTPUT["edit_group"])) {
            foreach($MODULE_OUTPUT["edit_group"] as $group) { ?>
            <h2>�������������� ���������� � ������ <?=$group["name"]?> (<?=$group["faculty_name"]?>):</h2>
            <form name="edit_group" method="post">
                <h3>�������� ������:</h3>
                <input type="text" name="group_name" value="<?=$group["name"]?>" />
                <h3>���������, � �������� ��������� ������:</h3>
                <select name="faculty">
<?php           foreach($MODULE_OUTPUT["faculties_edit_group"] as $faculty) {
                    if($faculty["id"] == $group["id_faculty"]) { ?>
                    <option value="<?=$faculty["id"]?>" selected="selected"><?=$faculty["name"]?></option>
<?php               } else { ?>
                    <option value="<?=$faculty["id"]?>"><?=$faculty["name"]?></option>
<?php               }
                } ?>
                </select>
                <input type="hidden" value="edit" name="mode" />
                <input type="hidden" value="<?=$group["id"]?>" name="id" />
                <h3>����</h3>
                <input type="text" name="year" value="<?=$group["year"]?>" /><br />
                <input type="submit" value="���������" class="button" />
            </form>
<?php       }
        }
        if(isset($MODULE_OUTPUT["edit_student"])) {
            $student = $MODULE_OUTPUT["edit_student"]; ?>
        <h2>�������������� ���������� � ��������:</h2>
        <a href="expulsion/" onclick="return confirm('�� �������, ��� ������ ��������� ������� ��������?');">��������� ��������.</a><br />
        <a href="marks/">�������� ������</a>
        <input type="hidden" name="id_student" value="<?=$student["id"]?>" />
        <form name="edit_student" method="post" onsubmit="return sendform(this);">
            <h3>�������:</h3>
            <input type="text" name="last_name" value="<?=$student["last_name"]?>" />
            <h3>���:</h3>
            <input type="text" name="first_name" value="<?=$student["name"]?>" />
            <h3>��������:</h3>
            <input type="text" name="patronymic" value="<?=$student["patronymic"]?>" />
            <h3>���:</h3>
<?php       if(isset($student["male"])) {
                if($student["male"]) { ?>
            <input type="radio" value="1" name="male" checked="checked" />� / �<input type="radio" value="0" name="male" />
<?php           } else { ?>
            <input type="radio" value="1" name="male" />� / �<input type="radio" value="0" name="male" checked="checked" />
<?php           }
            } else { ?>
            <input type="radio" value="1" name="male" />� / �<input type="radio" value="0" name="male" />
<?php       } ?>
            <h3>�������������:</h3>
            <select name="id_speciality">
<?php       if(isset($MODULE_OUTPUT["specialities"])) {
                foreach($MODULE_OUTPUT["specialities"] as $speciality) {
                    //if(isset($student["id_speciality"]))
                    {
                        //if($student["id_speciality"] == $speciality["id_speciality"])
                    if($student["id_speciality"] == $speciality["code"]) { ?>
                <option value="<?=$speciality["code"]?>" selected="selected"><?=$speciality["name"]?> (<?=$speciality["code"]?>)</option>
<?php               } else { ?>
                <option value="<?=$speciality["code"]?>"><?=$speciality["name"]?> (<?=$speciality["code"]?>)</option>
<?php               }
                    }
                    /*else { ?>
                <option value="<?=$student["id_speciality"]?>"><?=$speciality["name"]?> (<?=$speciality["code"]?>)</option>
<?php               }*/
                }
            } ?>
            </select>
            <h3>��� �����������:</h3>
            <input type="text" name="year" value="<?=$student["year"]?>" />
            <h3>������:</h3>
            <select name="group">
<?php       foreach($MODULE_OUTPUT["faculties_list"] as $faculty) { ?>
                <optgroup label="<?=$faculty["name"]?>">
<?php           foreach($faculty["groups"] as $group) {
                    if($student["id_group"] == $group["id"]) { ?>
                    <option value="<?=$group["id"]?>" selected="selected"><?=$group["name"]?></option>
<?php               }  else { ?>
                    <option value="<?=$group["id"]?>"><?=$group["name"]?></option>
<?php               }
                } ?>
                </optgroup>
<?php       } ?>
            </select>
            <h3>��������� (���� ����):</h3>
<?php       if(isset($student["male"])) {
                if($student["subgroup"] == "�") { ?>
            <input type="radio" value="�" name="subgroup" checked="checked" />� / �<input type="radio" value="�" name="subgroup" />
<?php           } else { ?>
            <input type="radio" value="�" name="subgroup" />� / �<input type="radio" value="�" name="subgroup" checked="checked" />
<?php           }
            } else { ?>
            <input type="radio" value="�" name="subgroup" />� / �<input type="radio" value="�" name="subgroup" />
<?php       } ?>
            <h3>��� ������������ ��� ������� � �������:</h3>
<?php       if(isset($student["username"])) { ?>
            <input type="text" name="username" value="<?=$student["username"]?>" />
<?php       } else { ?>
            <input type="text" name="username" value="" />
<?php       } ?>
            <br />
            <h3>����������� ����� (E-mail):</h3>
<?php       if(isset($student["email"])) { ?>
            <input type="text" name="email" value="<?=$student["email"]?>" />
<?php       } else { ?>
            <input type="text" name="email" value="" />
<?php       } ?>
            <br />
<?php       if(/*empty($student["password"])*/1) { ?>
            <h3>������:</h3>
            <input type="password" name="password1" />
            <h3>��������� ������:</h3>
            <input type="password" name="password2" />
<?php       } ?>
            <br />
            <input type="submit" value="��������� ���������" class="button" />
        </fomr>
<?php   }
        if(isset($MODULE_OUTPUT["marks"])) {
            $marks = $MODULE_OUTPUT["marks"];
            $months = array("������", "�������", "�����", "������", "���", "����", "����", "�������", "��������", "�������", "������", "�������");
            $year = date("Y");
            $month = date("n");
            $day = date("j"); ?>
        <h2><?=$MODULE_OUTPUT["student_last_name"]?> <?=$MODULE_OUTPUT["student_name"]?> <?=$MODULE_OUTPUT["student_patronymic"]?></h2>
<?php       if(!empty($MODULE_OUTPUT["marks"])) { ?>
        <h2>�������� ������ ��������</h2>
        <table class="timetable">
            <tr class="table_top">
                <th>�������� ����������</th>
                <th>������</th>
                <th>����</th>
                <th>��������</th>
            </tr>
<?php           $i=1;
                foreach($MODULE_OUTPUT["marks"] as $mark) {
                    if($mark["term"] == $i) { ?>
            <tr class="week_day">
                <th colspan="4">������� <?=$i?></th>
            </tr>
<?php                   $i++;
                    } ?>
            <tr>
                <td><?=$mark["subject"]?></td>
                <td><?=$mark["mark"]?></td>
                <td><?=$mark["date"]?></td>
                <td><a href="edit/<?=$mark["id"]?>/">�������������</a> / <a href="delete/<?=$mark["id"]?>/" onclick="return confirm('�� �������, ��� ������ ������� ��������� �������?');">�������</a></td>
            </tr>
<?php           } ?>
        </table>
<?php       } ?>
        <h2>�������� ������</h2>
        <form method="post">
            <input type="hidden" name="student_id" value="<?=$MODULE_OUTPUT["student_id"]?>" />
            <h3>�������� ����������:</h3>
            <select name="subject">
                <option value="0">�������� ����������</option>
<?php       foreach($MODULE_OUTPUT["subjects"] as $subject) { ?>
                <option value="<?=$subject["id"]?>"><?=$subject["name"]?></option>
<?php       } ?>
            </select>
            <h3>������ (��� ������� � ������)</h3>
            <select name="mark">
                <option selected="selected" disabled="disabled" value="0">�������� ������</option>
<?php       for($k=2; $k<6; $k++) { ?>
                <option value="<?=$k?>"><?=$k?></option>
<?php       } ?>
                <option value="�������">�������</option>
                <option value="�� �������">�� �������</option>
            </select><br />
            <h3>����</h3>
            <select name="day">
<?php       for($i=1; $i<=31; $i++) {
                if($i != $day) { ?>
                <option value="<?=$i?>"><?=$i?></option>
<?php           } else { ?>
                <option value="<?=$i?>" selected="selected"><?=$i?></option>
<?php           }
            } ?>
            </select>
            <select name="month">
<?php       for($i=0; $i<12; $i++) {
                if($i+1 != $month) { ?>
                <option value="<?=$i+1?>"><?=$months[$i]?></option>
<?php           } else { ?>
                <option value="<?=$i+1?>" selected="selected"><?=$months[$i]?></option>
<?php           }
            } ?>
            </select>
            <select name="year">
<?php       for($i=$year-5; $i<$year; $i++) { ?>
                <option value="<?=$i?>"><?=$i?></option>
<?php       } ?>
                <option selected="selected"><?=$year?></option>
            </select><br />
            <h3>�������</h3>
            <select name="term">
                <option selected="selected" disabled="disabled">�������� �������</option>
<?php       for($k=1; $k<13; $k++) { ?>
                <option value="<?=$k?>"><?=$k?></option>
<?php       } ?>
            </select><br />
            <input type="submit" value="�������� ������" class="button" />
        </form>
<?php   }
        if(isset($MODULE_OUTPUT["edit_mark"])) {
            $mark = $MODULE_OUTPUT["edit_mark"];
            $months = array("������", "�������", "�����", "������", "���", "����", "����", "�������", "��������", "�������", "������", "�������");
            $year = date("Y"); ?>
        <h2>�������������� ������:</h2>
        <form method="post" name="edit_mark">
            <input type="hidden" name="mark_id" value="<?=$mark["id"]?>" />
            <h3>�������� ����������:</h3>
            <select name="subject">
<?php       foreach($MODULE_OUTPUT["subjects"] as $subject) {
                if($subject["id"] == $mark["subject_id"]) { ?>
                <option value="<?=$subject["id"]?>" selected="selected"><?=$subject["name"]?></option>
<?php           } else { ?>
                <option value="<?=$subject["id"]?>"><?=$subject["name"]?></option>
<?php           }
            } ?>
            </select>
            <h3>������ (��� ������� � ������)</h3>
            <select name="mark">
<?php       for($k=2; $k<6; $k++) {
                if($mark["mark"]==$k) { ?>
                <option value="<?=$k?>" selected="selected"><?=$k?></option>
<?php           } else { ?>
                <option value="<?=$k?>"><?=$k?></option>
<?php           }
            }
            if($mark["mark"]=="�������") { ?>
                <option value="�������" selected="selected">�������</option>
                <option value="�� �������">�� �������</option>
<?php       } else {
                if($mark["mark"]=="�� �������") { ?>
                <option value="�������">�������</option>
                <option value="�� �������" selected="selected">�� �������</option>
<?php           } else { ?>
                <option value="�������">�������</option>
                <option value="�� �������">�� �������</option>
<?php           }
            } ?>
            </select>
            <h3>����:</h3>
            <select name="day">
<?php       for($i=1; $i<=31; $i++) {
                if($i != $mark["day"]) { ?>
                <option value="<?=$i?>"><?=$i?></option>
<?php           } else { ?>
                <option value="<?=$i?>" selected="selected"><?=$i?></option>
<?php           }
            } ?>
            </select>
            <select name="month">
<?php       for($i=0; $i<12; $i++) {
                if($i+1 != $mark["month"]) { ?>
                <option value="<?=$i+1?>"><?=$months[$i]?></option>
<?php           } else { ?>
                <option value="<?=$i+1?>" selected="selected"><?=$months[$i]?></option>
<?php           }
            } ?>
            </select>
            <select name="year">
<?php       for($i=$year-10; $i<=$year; $i++) {
                if($i != $mark["year"]) { ?>
                <option value="<?=$i?>"><?=$i?></option>
<?php           } else { ?>
                <option value="<?=$i?>" selected="selected"><?=$i?></option>
<?php           }
            } ?>
            </select><br />
            <h3>�������</h3>
            <select name="term">
<?php       for($k=1; $k<13; $k++) {
                if($mark["term"]==$k) { ?>
                <option value="<?=$k?>" selected="selected"><?=$k?></option>
<?php           } else { ?>
                <option value="<?=$k?>"><?=$k?></option>
<?php           }
            } ?>
            </select> <br />
            <input type="submit" value="��������� ������" class="button" />
        </form>
<?php   }
    }
    break;

    case "timetable_subjects_by_departments": {
        //if($Engine->OperationAllowed($MODULE_OUTPUT['module_id'], "nsau.js.access", -1, $Auth->usergroup_id)) { ?>
        <script>jQuery(document).ready(function($) {TimetableSubjectsByDepartments();InputCalendar();});</script>
<?php   //}
        if(isset($MODULE_OUTPUT["departments_list"])) { ?>
        <ul>
<?php       foreach($MODULE_OUTPUT["departments_list"] as $faculty) { ?>
            <li>
                <?=$faculty['fname']?>
<?php           if(count($faculty['dep_list'])) { ?>
                <ul>
<?php               foreach($faculty['dep_list'] as $dep_id => $department) { ?>
                    <li><a href="show/<?=$dep_id?>/"><?=$department?></a></li>
<?php               } ?>
                </ul>
<?php           } ?>
            </li>
<?php       } ?>
        </ul>
<?php   }
        $rate = array('0.25', '0.5', '0.75', '1.0', '1.25', '1.5');
        if(isset($MODULE_OUTPUT["show_mode"]) && $MODULE_OUTPUT["show_mode"]=="show") {
            if(isset($MODULE_OUTPUT["subjects_by_departments_list"]))  {
                $department = str_replace("�������", "�������", $MODULE_OUTPUT["department_name"]); ?>
        <h1>������������� ��������� ����� ��� <?=$department?></h1>
        <table class="timetable">
            <tr class="table_top">
                <th rowspan="4"><img src="/themes/images/delete.png"></th>
                <th rowspan="4">����������</th>
                <th rowspan="4">�-�</th>
                <th rowspan="4">�<br />�<br/>�<br/>�</th>
                <th colspan="2">�.�.�. �������������</th>
                <th colspan="2">� �����</th>
                <th rowspan="4">� ���.</th>
                <th colspan="5">�������������� �������� � �������������</th>
                <th rowspan="4">����������</th>
                <th rowspan="4">�<br />�<br />�<br />�<br />�<br />�</th>
            </tr>
            <tr class="table_top">
                <th rowspan="3">������</th>
                <th rowspan="3">��������</th>
                <th rowspan="3">�������</th>
                <th rowspan="3">����������</th>
                <th colspan="4">������� � ������(����, ����)</th>
                <th rowspan="3">�<br />�<br />�<br />�<br />�<br />�<br />�</th>
            </tr>
            <tr class="table_top">
                <th colspan="3">��������</th>
                <th rowspan="2">������� ������ ����</th>
            </tr>
            <tr class="table_top">
                <th><span title="����������">�</span></th>
                <th><span title="�������������">�</span></th>
                <th><span title="�����������">�</span></th>
            </tr>
<?php           $i=0;
                foreach($MODULE_OUTPUT["subjects_by_departments_list"] as $data) {
                    $i++; ?>
            <tr>
                <td><a href="delete/<?=$data["id"]?>/" onclick="return confirm('�� �������, ��� ������ ������� ��������� �������?');"><img src="/themes/images/delete.png"></a></td>
                <td><a href="edit/<?=$data["id"]?>/"><?=$data["subject"]?></a></td>
                <td><span title="<?=$data["faculty"]?>"><?=$data["faculty_short"]?></span></td>
                <td><?=$data["year"]?></td>
<?php               if(!$data["type"]) { ?>
                <td><?=$data["last_name"]?> <?=$data["name"]?> <?=$data["patronymic"]?></td>
                <td>&nbsp;</td>
<?php               } else {
                        if($data["type"]==1) { ?>
                <td>&nbsp;</td>
                <td><?=$data["last_name"]?> <?=$data["name"]?> <?=$data["patronymic"]?></td>
<?php                   } else { ?>
                <td><?=$data["last_name"]?> <?=$data["name"]?> <?=$data["patronymic"]?></td>
                <td><?=$data["last_name"]?> <?=$data["name"]?> <?=$data["patronymic"]?></td>
<?php                   }
                    } ?>
                <td><?=$data["groups"]?></td>
                <td><?=$data["subgroups"]?></td>
                <td><?=$data["auditorium"]?></td>
                <td><?=($data["rectors"] ? "+" : "&nbsp;")?></td>
                <td><?=($data["prorectors"] ? "+" : "&nbsp;")?></td>
                <td><?=($data["decanats"] ? "+" : "&nbsp;")?></td>
                <td><?=($data["academic_senate"] ? "+" : "&nbsp;")?></td>
                <td><?=($data["state"] ? "+" : "&nbsp;")?></td>
                <td><?=$data["comments"]?></td>
                <td><?=$data["rate"]?></td>
            </tr>
<?php           } ?>
        </table>
<?php       }
            if(!isset($MODULE_OUTPUT["subjects_list"])) { ?>
        <h3>� ��������� ������� �� ��������� ���������. ������� �������� ����������.</h3>
<?php       }
            if(isset($MODULE_OUTPUT["subjects_list"])) { ?>
        <h2>�������� ������ � �������������</h2>
        <form method="post" onsubmit="return sendform_sbd(this);">
            <h3>�������� ����������:</h3>
            <select name="subject">
                <option value="0" selected="selected" disabled="disabled">�������� ����������</option>
<?php           foreach($MODULE_OUTPUT["subjects_list"] as $subject) { ?>
                <option value="<?=$subject["id"]?>"><?=$subject["name"]?></option>
<?php           } ?>
            </select>
            <h3>�������� ���������</h3>
            <select name="faculty">
                <option value="0" selected="selected" disabled="disabled">�������� ���������</option>
<?php           foreach($MODULE_OUTPUT["faculty_list"] as $faculty) { ?>
                <option value="<?=$faculty["id"]?>"><?=$faculty["name"]?></option>
<?php           } ?>
            </select>
            <h3>�������� ����:</h3>
            <select name="year">
<?php           for($i=1; $i<=6; $i++) { ?>
                <option value="<?=$i?>">���� <?=$i?></option>
<?php           } ?>
            </select>
            <h3>�������� �������������:</h3>
            <select name="teacher_id">
                <option value="0" selected="selected" disabled="disabled">�������� �������������</option>
<?php           foreach($MODULE_OUTPUT["teachers_list"] as $teacher) { ?>
                <option value="<?=$teacher["id"]?>"><?=$teacher["last_name"]?> <?=$teacher["name"]?> <?=$teacher["patronymic"]?></option>
<?php           } ?>
            </select>
            <h3>������ � (���) ��������:</h3>
            <input type="radio" name="type" value="2" checked="checked" />������ � ��������<br />
            <input type="radio" name="type" value="0" />������ / �������� <input type="radio" name="type" value="1" />
            <h3>� ������� (������ �������� ����� ����� � �������):</h3>
            <input type="text" name="groups" />
            <h3>� ���������� (������ �������� ����� ����� � �������):</h3>
            <input type="text" name="subgroups" />
            <h3>������ ��������� (�������� ����� ����� � �������):</h3>
            <input type="text" name="auditorium" />
            <h3>C:</h3>
            <input name='date_from' type="text" class="date_time"  value="2012-01-01" />
            <h3>��:</h3>
            <input name='date_to' type="text" class="date_time"  value="2012-12-31" />
            <h3>������� � ������ ��������:</h3>
            <input type="checkbox" name="rectors" /> ����������<br />
            <input type="checkbox" name="prorectors" /> �������������<br />
            <input type="checkbox" name="decanats" /> �����������<br />
            <input type="checkbox" name="academic_senate" />������� � ������ ������� ������ ����<br />
            <h3>�������� �� ������� ��������������:</h3>
            <input type="radio" name="state" value="0" />��� / �� <input type="radio" name="state" value="1" checked="checked"/>
            <h3>������ ���������� ������</h3>
            <select name="rate">
                <option selected="selected" value="0" disabled="disabled">�������� ������ ���������� ������</option>
<?php           foreach($rate as $r) { ?>
                <option value="<?=$r?>"><?=$r?></option>
<?php           } ?>
            </select>
            <h3>�������� ����������:</h3>
            <input type="text" name="comments" /><br />
            <input type="submit" value="��������" class="button" />
        </form>
<?php       }
        }
        if(isset($MODULE_OUTPUT["show_mode"]) && $MODULE_OUTPUT["show_mode"]=="edit") {
            $data = $MODULE_OUTPUT["edit_row"]; ?>
        <h2>�������������� ������</h2>
        <form method="post" onsubmit="return sendform_sbd();">
            <input type="hidden" name="id" value="<?=$data["id"]?>">
            <h3>�������� ����������:</h3>
            <select name="subject">
                <option value="0" selected="selected" disabled="disabled">�������� ����������</option>
<?php       foreach($MODULE_OUTPUT["subjects_list"] as $subject) {
                if($subject["id"] == $data["subject_id"]) { ?>
                <option selected="selected" value="<?=$subject["id"]?>"><?=$subject["name"]?></option>
<?php           } else { ?>
                <option value="<?=$subject["id"]?>"><?=$subject["name"]?></option>
<?php           }
            } ?>
            </select>
            <h3>�������� ���������</h3>
            <select name="faculty">
                <option value="0" selected="selected" disabled="disabled">�������� ���������</option>
<?php       foreach($MODULE_OUTPUT["faculty_list"] as $faculty) {
                if($faculty["id"] == $data["faculty_id"]) { ?>
                <option selected="selected" value="<?=$faculty["id"]?>"><?=$faculty["name"]?></option>
<?php           } else { ?>
                 <option value="<?=$faculty["id"]?>"><?=$faculty["name"]?></option>
<?php           }
            } ?>
            </select>
            <h3>�������� ����:</h3>
            <select name="year">
<?php       for($i=1; $i<=6; $i++) {
                if($i == $data["year"]) { ?>
                <option selected="selected" value="<?=$i?>">���� <?=$i?></option>
<?php           } else { ?>
                <option value="<?=$i?>">���� <?=$i?></option>
<?php           }
            } ?>
            </select>
            <h3>�������� �������������:</h3>
            <select name="teacher_id">
                <option value="0" selected="selected">�������� �������������</option>
<?php       foreach($MODULE_OUTPUT["teachers_list"] as $teacher) {
                if($teacher["id"] == $data["teacher_id"]) { ?>
                <option selected="selected" value="<?=$teacher["id"]?>"><?=$teacher["last_name"]?> <?=$teacher["name"]?> <?=$teacher["patronymic"]?></option>
<?php           } else { ?>
                <option value="<?=$teacher["id"]?>"><?=$teacher["last_name"]?> <?=$teacher["name"]?> <?=$teacher["patronymic"]?></option>
<?php           }
            } ?>
            </select>
            <h3>������ � (���) ��������:</h3>
<?php       if(!$data["type"]) { ?>
            <input type="radio" name="type" value="2" />������ � ��������<br />
            <input type="radio" name="type" value="0" checked="checked" />������ / �������� <input type="radio" name="type" value="1" />
<?php       } else {
                if($data["type"] == 1) { ?>
            <input type="radio" name="type" value="2" />������ � ��������<br />
            <input type="radio" name="type" value="0" />������ / �������� <input type="radio" name="type" value="1" checked="checked" />
<?php           } else { ?>
            <input type="radio" name="type" value="2" checked="checked" />������ � ��������<br />
            <input type="radio" name="type" value="0" />������ / �������� <input type="radio" name="type" value="1" />
<?php           }
            } ?>
            <h3>� ������� (������ �������� ����� ����� � �������):</h3>
            <input type="text" name="groups" value="<?=$data["groups"]?>" />
            <h3>� ���������� (������ �������� ����� ����� � �������):</h3>
            <input type="text" name="subgroups" value="<?=$data["subgroups"]?>" />
            <h3>������ ��������� (�������� ����� ����� � �������):</h3>
            <input type="text" name="auditorium" value="<?=$data["auditorium"]?>" />
            <h3>C:</h3>
            <input name='date_from' type="text" class="date_time"  value="<?=$data["date_from"]?>" />
            <h3>��:</h3>
            <input name='date_to' type="text" class="date_time"  value="<?=$data["date_to"]?>" />
            <h3>������� � ������ ��������:</h3>
            <input type="checkbox" name="rectors" <?=($data["participation_rectors"] ? 'checked="checked"' : '')?> /> ����������<br />
            <input type="checkbox" name="prorectors" <?=($data["participation_prorectors"] ? 'checked="checked"' : '')?> /> �������������<br />
            <input type="checkbox" name="decanats" <?=($data["participation_decanats"] ? 'checked="checked"' : '')?> /> �����������<br />
            <input type="checkbox" name="academic_senate" <?=($data["participation_academic_senate"] ? 'checked="checked"' : '')?> />������� � ������ ������� ������ ����<br />
            <h3>�������� �� ������� ��������������:</h3>
<?php       if($data["state"]) { ?>
            <input type="radio" name="state" value="0" />��� / �� <input type="radio" name="state" value="1" checked="checked" />
<?php       } else { ?>
            <input type="radio" name="state" value="0" checked="checked" />��� / �� <input type="radio" name="state" value="1" />
<?php       } ?>
            <h3>������ ���������� ������</h3>
            <select name="rate">
<?php       foreach($rate as $r) {
                if($r == $data["rate"]) { ?>
                <option selected="selected" value="<?=$r?>"><?=$r?></option>
<?php           } else { ?>
                <option value="<?=$r?>"><?=$r?></option>
<?php           }
            } ?>
            </select>
            <h3>����������:</h3>
            <input type="text" name="comments" value="<?=$data["comments"]?>" /><br />
            <input type="submit" value="���������" class="button" />
        </form>
<?php   }
    }
    break;

    case "timetable_graphics": {
        //if($Engine->OperationAllowed($MODULE_OUTPUT['module_id'], "nsau.js.access", -1, $Auth->usergroup_id)) { ?>
        <script>jQuery(document).ready(function($) {SendformGraphics();});</script>
<?php   //}
        $month = array("������", "�������", "����", "������", "���", "����", "����", "������", "��������", "�������", "������", "�������");
        $months = array("������", "�������", "�����", "������", "���", "����", "����", "�������", "��������", "�������", "������", "�������");
        if(isset($MODULE_OUTPUT["calendar"])) { ?>
        <a href="<?=$MODULE_OUTPUT["calendar"]["back_link"]?>">��������� � ��������� �������</a>
        <h2>�������������� ���������</h2>
        <form method="post">
            <table class="graphics_table">
                <tr class="table_top">
                    <th rowspan="2">� ������</th>
                    <th colspan="3">���� ������ ������</th>
                    <th colspan="3">���� ����� ������</th>
                </tr>
                <tr class="table_top">
                    <th>����</th>
                    <th>�����</th>
                    <th>���</th>
                    <th>����</th>
                    <th>�����</th>
                    <th>���</th>
                </tr>
<?php       $year = date("Y");
            foreach($MODULE_OUTPUT["calendar_data"] as $data) {
                $date = explode("-",$data["begin_date"]); ?>
                <tr>
                    <td><?=$data["num_week"]?></td>
                    <td>
                        <input type="hidden" name="begin_date[<?=$data["num_week"]?>][num_week]" value="<?=$data["num_week"]?>"/>
                        <select name="begin_date[<?=$data["num_week"]?>][day]">
<?php           for($i=1; $i<=31; $i++) {
                    if($i != $date[2]) { ?>
                            <option value="<?=$i?>"><?=$i?></option>
<?php               } else { ?>
                            <option value="<?=$i?>" selected="selected"><?=$i?></option>
<?php               }
                } ?>
                        </select>
                    </td>
                    <td>
                        <input type="hidden" name="begin_date[<?=$data["num_week"]?>][num_week]" value="<?=$data["num_week"]?>"/>
                        <select name="begin_date[<?=$data["num_week"]?>][month]">
<?php           for($i=0; $i<12; $i++) {
                    if($i+1 != $date[1]) { ?>
                            <option value="<?=$i+1?>"><?=$month[$i]?></option>
<?php               } else { ?>
                            <option value="<?=$i+1?>" selected="selected"><?=$month[$i]?></option>
<?php               }
                } ?>
                        </select>
                    </td>
                    <td>
                        <input type="hidden" name="begin_date[<?=$data["num_week"]?>][num_week]" value="<?=$data["num_week"]?>"/>
                        <select name="begin_date[<?=$data["num_week"]?>][year]">
<?php           for($i=$year-2; $i<$year+3; $i++) {
                    if($i != $date[0]) { ?>
                            <option value="<?=$i?>"><?=$i?></option>
<?php               } else { ?>
                            <option selected="selected" value="<?=$i?>"><?=$i?></option>
<?php               }
                } ?>
                        </select>
                    </td>
<?php           $date_end = explode("-",$data["end_date"]); ?>
                    <td>
                        <input type="hidden" name="end_date[<?=$data["num_week"]?>][num_week]" value="<?=$data["num_week"]?>"/>
                        <select name="end_date[<?=$data["num_week"]?>][day]">
<?php           for($i=1; $i<=31; $i++) {
                    if($i != $date_end[2]) { ?>
                            <option value="<?=$i?>"><?=$i?></option>
<?php               } else { ?>
                            <option value="<?=$i?>" selected="selected"><?=$i?></option>
<?php               }
                } ?>
                        </select>
                    </td>
                    <td>
                        <input type="hidden" name="end_date[<?=$data["num_week"]?>][num_week]" value="<?=$data["num_week"]?>"/>
                        <select name="end_date[<?=$data["num_week"]?>][month]">
<?php           for($i=0; $i<12; $i++) {
                    if($i+1 != $date_end[1]) { ?>
                            <option value="<?=$i+1?>"><?=$month[$i]?></option>
<?php               } else { ?>
                            <option value="<?=$i+1?>" selected="selected"><?=$month[$i]?></option>
<?php               }
                } ?>
                        </select>
                    </td>
                    <td>
                        <input type="hidden" name="end_date[<?=$data["num_week"]?>][num_week]" value="<?=$data["num_week"]?>"/>
                        <select name="end_date[<?=$data["num_week"]?>][year]">
<?php           for($i=$year-2; $i<$year+3; $i++) {
                    if($i != $date_end[0]) { ?>
                            <option value="<?=$i?>"><?=$i?></option>
<?php               } else { ?>
                            <option selected="selected" value="<?=$i?>"><?=$i?></option>
<?php               }
                } ?>
                        </select>
                    </td>
                </tr>
<?php       } ?>
            </table>
            <input type="submit" value="���������" class="button" />
        </form>
<?php   }
        if(isset($MODULE_OUTPUT["graphics_list"])) { ?>
        <h1>������ ��������:</h1>
<?php       $i=-1;
            foreach($MODULE_OUTPUT["graphics_list"] as $graphic) {
                if($i==0 && $graphic["type"]) { ?>
        <h3>������� ��� ���������</h3>
<?php               $i++;
                }
                if($i<0) { ?>
        <h3>������� ��� �������</h3>
<?php               $i++;
                } ?>
        <p><a href="<?=$graphic["id"]?>/"><?=$graphic["name"]?></a> [<a href="edit/<?=$graphic["id"]?>/">�������������</a>][<a href="delete/<?=$graphic["id"]?>/" onclick="return confirm('�� �������, ��� ������ ������� ��������� �������?');">�������</a>]</p>
<?php       } ?>
        <h2>�������� ������</h2>
        <form method="post">
            <h3>�������� �������:</h3>
            <input name="graphic_name" size="50" type="text" /><br />
            <h3>������ ��� ��������:</h3>
            <input type="radio" name="type" value="0" checked="checked" /> ������ / �������� <input type="radio" name="type" value="1" />
            <h3>���� ������ ������:</h3>
            <select name="begin_day">
                <option value="0" selected="selected">����</option>
<?php       for($i=1; $i<=31; $i++) { ?>
                <option value="<?=$i?>"><?=$i?></option>
<?php       } ?>
            </select>
            <select name="begin_month">
                <option value="0" selected="selected">�����</option>
<?php       for($i=0; $i<=11; $i++) { ?>
                <option value="<?=$i?>"><?=$months[$i]?></option>
<?php       } ?>
            </select>
            <select name="begin_year">
                <option value="0" selected="selected">���</option>
<?php       $year=date("Y");
            for($i=$year-5; $i<=$year+5; $i++) { ?>
                <option value="<?=$i?>"><?=$i?></option>
<?php       } ?>
            </select><br />
            <h3>���� ��������� ������:</h3>
            <select name="end_day">
                <option value="0" selected="selected">����</option>
<?php       for($i=1; $i<=31; $i++) { ?>
                <option value="<?=$i?>"><?=$i?></option>
<?php       } ?>
            </select>
            <select name="end_month">
                <option value="0" selected="selected">�����</option>
<?php       for($i=0; $i<=11; $i++) { ?>
                <option value="<?=$i+1?>"><?=$months[$i]?></option>
<?php       } ?>
            </select>
            <select name="end_year">
                <option value="0" selected="selected">���</option>
<?php       $year=date("Y");
            for($i=$year-5; $i<=$year+5; $i++) { ?>
                <option value="<?=$i?>"><?=$i?></option>
<?php       } ?>
            </select> <br />
            <input value="��������" type="submit" class="button"/>
        </form>
<?php   }
        if(isset($MODULE_OUTPUT["graphics_list_data"])) {
            $data = $MODULE_OUTPUT["graphics_list_data"]; ?>
        <h2>�������������� ����������� � �������:</h2>
        <form method="post">
            <h3>��������� �������:</h3>
            <input name="graphic_name" type="text" size="50" value="<?=$data["name"]?>" /> <br />
            <h3>������ ��� ��������:</h3>
<?php       if($data["type"]) { ?>
            <input type="radio" name="type" value="0" /> ������ / �������� <input type="radio" name="type" value="1" checked="checked" />
<?php       } else { ?>
            <input type="radio" name="type" value="0" checked="checked" /> ������ / �������� <input type="radio" name="type" value="1" />
<?php       }
            $begin_date = explode("-",$data["begin_session"]);
            $end_date = explode("-",$data["end_session"]); ?>
            <h3>���� ������ ������:</h3>
            <select name="begin_day">
                <option value="0" selected="selected">����</option>
<?php       for($i=1; $i<=31; $i++) {
                if($begin_date[2]==$i) { ?>
                <option value="<?=$i?>" selected="selected"><?=$i?></option>
<?php           } else { ?>
                <option value="<?=$i?>"><?=$i?></option>
<?php           }
            } ?>
            </select>
            <select name="begin_month">
                <option value="0" selected="selected">�����</option>
<?php       for($i=0; $i<=11; $i++) {
                if($begin_date[1] == $i+1) { ?>
                <option value="<?=$i+1?>" selected="selected"><?=$months[$i]?></option>
<?php           } else { ?>
                <option value="<?=$i+1?>"><?=$months[$i]?></option>
<?php           }
            } ?>
            </select>
            <select name="begin_year">
                <option value="0" selected="selected">���</option>
<?php       $year=date("Y");
            for($i=$year-5; $i<=$year+5; $i++) {
                if($begin_date[0] == $i) { ?>
                <option value="<?=$i?>" selected="selected"><?=$i?></option>
<?php           } else { ?>
                <option value="<?=$i?>"><?=$i?></option>
<?php           }
            } ?>
            </select> <br />
            <h3>���� ��������� ������:</h3>
            <select name="end_day">
                <option value="0" selected="selected">����</option>
<?php       for($i=1; $i<=31; $i++) {
                if($end_date[2] == $i) { ?>
                <option value="<?=$i?>" selected="selected"><?=$i?></option>
<?php           } else { ?>
                <option value="<?=$i?>"><?=$i?></option>
<?php           }
            } ?>
            </select>
            <select name="end_month">
                <option value="0" selected="selected">�����</option>
<?php       for($i=0; $i<=11; $i++) {
                if($end_date[1] == $i+1) { ?>
                <option value="<?=$i+1?>" selected="selected"><?=$months[$i]?></option>
<?php           } else { ?>
                <option value="<?=$i+1?>"><?=$months[$i]?></option>
<?php           }
            } ?>
            </select>
            <select name="end_year">
                <option value="0" selected="selected">���</option>
<?php       $year=date("Y");
            for($i=$year-5; $i<=$year+5; $i++) {
                if($end_date[0] == $i) { ?>
                <option value="<?=$i?>" selected="selected"><?=$i?></option>
<?php           } else { ?>
                <option value="<?=$i?>"><?=$i?></option>
<?php           }
            } ?>
            </select> <br />
            <input type="submit" value="��������� ���������" class="button">
        </form>
<?php   }
        if(isset($MODULE_OUTPUT["graphics_data"])) { ?>
        <h2>�������� ������� <?=$MODULE_OUTPUT["g_name"]?></h2>
        <a href="calendar/">���������</a>
<?php       if(!empty($MODULE_OUTPUT["begin_session"]) && !empty($MODULE_OUTPUT["end_session"])) {
                $begin_date = explode("-",$MODULE_OUTPUT["begin_session"]);
                $end_date = explode("-",$MODULE_OUTPUT["end_session"]); ?>
        <h4><a href="<?=$MODULE_OUTPUT["uri"]?>edit/<?=$MODULE_OUTPUT["g_id"]?>/">������: � <?=$begin_date[2]?>/<?=$begin_date[1]?><?=$begin_date[0]?> �� <?=$end_date[2]?>/<?=$end_date[1]?>/<?=$end_date[0]?></a></h4>
<?php       }
            if(!$MODULE_OUTPUT["g_type"]) { ?>
        <form name="graphics" method="post">
            <table class="graphics_table">
                <tr class="table_top">
                    <th>�������</th>
                    <th>�-���</th>
                    <th>����������</th>
                    <th><img src="/gen_ru_img.php?text=�� �����&width=100"></th>
                    <th><img src="/gen_ru_img.php?text=���-�� ���������&width=200"></th>
                    <th>��� �������</th>
                    <th class="groups"><img src="/gen_ru_img.php?text=���-�� ��.,�����.&width=200"></th>
                    <th>�<br />�<br />�<br />�<br />�<br /><br />�<br />�<br />�<br />�<br />�</th>
<?php           foreach($MODULE_OUTPUT["dates"] as $week) {
                    if($week["num_week"]>17) {
                        break;
                    }
                    $date1 = explode("-",$week["begin_date"]);
                    $date2 = explode("-",$week["end_date"]);
                    $m1 = $date1[1] + 0;
                    $m2 = $date2[1] +0;
                    $text = $date1[2]." - ".$date2[2]; ?>
                    <th><span title="<?=$month[$m2-1]?>"><img src="/gen_img.php?text=<?=$text?>"></span><br /><?=$week["num_week"]?><br /><br /><?=($week["num_week"]+1)%2+1?></th>
<?php           } ?>
                    <th>�<br />�<br />�<br />�<br />�<br />�<br />�</th>
                    <th>�<br />�<br />�<br />�<br />�<br />�<br />�<br />�<br />�</th>
                </tr>
<?php           foreach($MODULE_OUTPUT["graphics_data"] as $data) {
                    $department = str_replace("������� ", "", $data["department_name"]); ?>
                <tr>
                    <td rowspan="2"><a href="edit/<?=$data["id_graphic"]?>/"><?=$department?></a></td>
                    <td rowspan="2"><span title="<?=$data["faculty_name"]?>"><?=$data["faculty_short_name"]?></span></td>
                    <td rowspan="2"><?=$data["subject_name"]?><br /><span title="�������"><a href="delete/<?=$data["id_graphic"]?>/" onclick="return confirm('�� �������, ��� ������ ������� ��������� �������?');"><img src="/themes/images/delete.png"></a></span></td>
                    <td rowspan="2">
<?php               foreach($data["groups"] as $group) { ?>
                        <?=$group["group_name"]?>;&nbsp;
<?php               } ?>
                    </td>
                    <td rowspan="2"><?=$data["sum_students"]?></td>
                    <td colspan="2">���</td>
                    <td><?=$data["lecture_hours"]?><input type="hidden" name="<?=$data["id_graphic"]?>[name]" value="<?=$data["id_graphic"]?>"></td>
<?php               $i=1;
                    if(!empty($data["lect"])) {
                        foreach($data["lect"] as $lect) {
                            while($lect["week_num"]!=$i && $i<18) { ?>
                    <td><input type="text" size="1" style="width: 1em;" name="<?=$data["id_graphic"]?>[l][<?=$i?>]" /></td>
<?php                           $i++;
                            }
                            if($lect["week_num"]==$i) { ?>
                    <td><input type="text" size="1" style="width: 1em;" name="<?=$data["id_graphic"]?>[l][<?=$i?>]" value="<?=$lect["hours_per_week"]?>" /></td>
<?php                       }
                            $i++;
                        }
                    }
                    while($i<18) { ?>
                    <td><input type="text" size="1" style="width: 1em;" name="<?=$data["id_graphic"]?>[l][<?=$i?>]" /></td>
<?php                   $i++;
                    }
                    if($data["attestation_type"]) { ?>
                    <td rowspan="2">+</td><td rowspan="2">&nbsp;</td>
<?php               } else { ?>
                    <td rowspan="2">&nbsp;</td><td rowspan="2">+</td>
<?php               } ?>
                </tr>
                <tr>
                    <td><?=(!$data["type"] ? "�����" : "�����")?></td>
                    <td><?=$data["kol_groups"]?></td>
                    <td><?=$data["practice_hours"]?></td>
<?php               $i=1;
                    foreach($data["pr"] as $lect) {
                        while($lect["week_num"]!=$i && $i<18) { ?>
                    <td><input type="text" size="1" style="width: 1em;" name="<?=$data["id_graphic"]?>[p][<?=$i?>]" /></td>
<?php                       $i++;
                        }
                        if($lect["week_num"]==$i) { ?>
                    <td><input type="text" size="1" style="width: 1em;" name="<?=$data["id_graphic"]?>[p][<?=$i?>]" value="<?=$lect["hours_per_week"]?>" /></td>
<?php                   }
                        $i++;
                    }
                    while($i<18) { ?>
                    <td><input type="text" size="1" style="width: 1em;" name="<?=$data["id_graphic"]?>[p][<?=$i?>]" /></td>
<?php                   $i++;
                    } ?>
                </tr>
<?php           } ?>
            </table>
            <input type="submit" value="��������� ���������" class="button" />
        </form>
<?php       } else { ?>
        <form name="graphics" method="post">
            <table class="graphics_table">
                <tr class="table_top">
                    <th>�������</th>
                    <th>����������</th>
                    <th><img src="/gen_ru_img.php?text=�� �����&width=100"></th>
                    <th><img src="/gen_ru_img.php?text=���-�� ���������&width=200"></th>
                    <th>��� �������</th>
                    <th class="groups"><img src="/gen_ru_img.php?text=���-�� ��.,�����.&width=200"></th>
                    <th>�<br />�<br />�<br />�<br />�<br /><br />�<br />�<br />�<br />�<br />�</th>
                    <th>��</th>
                    <th>��</th>
<?php           foreach($MODULE_OUTPUT["dates"] as $week) {
                    if($week["num_week"]>17) {
                        break;
                    }
                    $date1 = explode("-",$week["begin_date"]);
                    $date2 = explode("-",$week["end_date"]);
                    $m1 = $date1[1] + 0;
                    $m2 = $date2[1] + 0;
                    $text = $date1[2]." - ".$date2[2]; ?>
                    <th><span title="<?=$month[$m2-1]?>"><img src="/gen_img.php?text=<?=$text?>"></span><br /><?=$week["num_week"]?><br /><br /><?=($week["num_week"]+1)%2+1?></th>
<?php           } ?>
                    <th>�<br />�<br />�<br />�<br />�.</th>
                    <th>�<br />�<br />�<br />�.</th>
                    <th>�<br />�<br />�<br />�<br />�<br />�<br />�</th>
                    <th>�<br />�<br />�<br />�<br />�<br />�<br />�<br />�<br />�</th>
                </tr>
<?php           foreach($MODULE_OUTPUT["graphics_data"] as $data) {
                    $department = str_replace("������� ", "", $data["department_name"]); ?>
                <tr>
                    <td rowspan="2"><a href="edit/<?=$data["id_graphic"]?>/"><?=$department?></a></td>
                    <td rowspan="2"><?=$data["subject_name"]?><br /><span title="�������"><a href="delete/<?=$data["id_graphic"]?>/" onclick="return confirm('�� �������, ��� ������ ������� ��������� �������?');"><img src="/themes/images/delete.png"></a></span></td>
                    <td rowspan="2">
<?php               foreach($data["groups"] as $group) { ?>
                        <?=$group["group_name"]?>;&nbsp;
<?php               } ?>
                    </td>
                    <td rowspan="2"><?=$data["sum_students"]?></td>
                    <td colspan="2">���</td><td><?=$data["lecture_hours"]?></td>
                    <td rowspan="2"><?=$data["ust_lecture"]?></td>
                    <td rowspan="2"><?=$data["ust_practice"]?><input type="hidden" name="<?=$data["id_graphic"]?>[name]" value="<?=$data["id_graphic"]?>"></td>
<?php               $i=1;
                    if(!empty($data["lect"])) {
                        foreach($data["lect"] as $lect) {
                            while($lect["week_num"]!=$i && $i<18) { ?>
                    <td><input type="text" size="1" style="width: 1em;" name="<?=$data["id_graphic"]?>[l][<?=$i?>]" /></td>
<?php                           $i++;
                            }
                            if($lect["week_num"]==$i) { ?>
                    <td><input type="text" size="1" style="width: 1em;" name="<?=$data["id_graphic"]?>[l][<?=$i?>]" value="<?=$lect["hours_per_week"]?>" /></td>
<?php                       }
                            $i++;
                        }
                    }
                    while($i<18) { ?>
                    <td><input type="text" size="1" style="width: 1em;" name="<?=$data["id_graphic"]?>[l][<?=$i?>]" /></td>
<?php                   $i++;
                    }
                    if($data["Contr"]) { ?>
                    <td rowspan="2">+</td>
<?php               } else { ?>
                    <td rowspan="2">&nbsp;</td>
<?php               }
                    if($data["Curse"]) { ?>
                    <td rowspan="2">+</td>
<?php               } else { ?>
                    <td rowspan="2">&nbsp;</td>
<?php               }
                    if($data["attestation_type"]) { ?>
                    <td rowspan="2">+</td><td rowspan="2">&nbsp;</td>
<?php               } else { ?>
                    <td rowspan="2">&nbsp;</td><td rowspan="2">+</td>
<?php               } ?>
                </tr>
                <tr>
                    <td><?=(!$data["type"] ? "�����" : "�����")?></td>
                    <td><?=$data["kol_groups"]?></td>
                    <td><?=$data["practice_hours"]?></td>
<?php               $i=1;
                    foreach($data["pr"] as $lect) {
                        while($lect["week_num"]!=$i && $i<18) { ?>
                    <td><input type="text" size="1" style="width: 1em;" name="<?=$data["id_graphic"]?>[p][<?=$i?>]" /></td>
<?php                       $i++;
                        }
                        if($lect["week_num"]==$i) { ?>
                    <td><input type="text" size="1" style="width: 1em;" name="<?=$data["id_graphic"]?>[p][<?=$i?>]" value="<?=$lect["hours_per_week"]?>" /></td>
<?php                   }
                        $i++;
                    }
                    while($i<18) { ?>
                    <td><input type="text" size="1" style="width: 1em;" name="<?=$data["id_graphic"]?>[p][<?=$i?>]" /></td>
<?php                   $i++;
                    } ?>
                </tr>
<?php           } ?>
            </table>
            <input type="submit" value="��������� ���������" class="button"/>
        </form>
<?php       } ?>
        <h2>�������� ����� ������ � ������</h2>
        <form method="post" name="new_graphic" onsubmit="return sendform_graphics(this);">
            <h3>�������� ����������</h3>
            <select name="subject_id">
                <option value="0" selected="selected" disabled="disabled">�������� ����������</option>
<?php       $j=0;
            foreach($MODULE_OUTPUT["subjects"] as $subject) {
                if(!$j) { ?>
                <optgroup label="<?=$subject["department_name"]?> (<?=$subject["faculty_short_name"]?>)">
<?php               $j = $subject["department_id"];
                } else {
                    if($j != $subject["department_id"]) {
                        $j = $subject["department_id"]; ?>
                </optgroup>
                <optgroup label="<?=$subject["department_name"]?> (<?=$subject["faculty_short_name"]?>)">
<?php               }
                } ?>
                    <option value="<?=$subject["subject_id"]?>"><?=$subject["subject_name"]?></option>
<?php       } ?>
                </optgroup>
            </select>
            <h3>������� ������ ����� (����� ������ ������ �������� ����� � �������):</h3>
            <input name="groups" />
            <h3>��� �������:</h3>
            <input type="radio" value="0" checked="checked" name="type" /> ��������� / ������������
            <input type="radio" value="1" name="type" />
            <h3>���������� ����� (��� ��������, ���� ������� ������������)</h3>
            <input type="text" name="kol_groups" value="1" />
            <h3>���������� ����� �� �������:</h3>
            <input type="text" name="lecture_hours" value="0" /> ����������<br />
            <input type="text" name="practice_hours" value="0" /> ������������ �������
<?php       if($MODULE_OUTPUT["g_type"]) { ?>
            <h3>���������� ������������ �������:</h3>
            <input type="text" name="ust_lecture" value="0" /> ������������ ������ (��)<br />
            <input type="text" name="ust_practice" value="0" /> ������������ �������� (��)
            <h3>��������, ���� ����:</h3>
            <input type="checkbox" name="Contr" /> ����������� <br />
            <input type="checkbox" name="Curse" /> �������� <br />
<?php       } ?>
            <h3>�������� ��� ����������:</h3>
            <input type="radio" value="0" checked="checked" name="attestation_type" /> ������� / �����
            <input type="radio" value="1" name="attestation_type" /> <br />
            <input type="submit" value="�������� ������" class="button" />
        </form>
<?php   }
        if(isset($MODULE_OUTPUT["edit_graphic"])) {
            $data = $MODULE_OUTPUT["edit_graphic"]; ?>
        <h2>�������������� ������� <?=$data["g_name"]?></h2>
        <form method="post" onsubmit="return sendform_graphics(this);">
            <h3>����������:</h3>
            <select name="subject_id">
<?php       $j=0;
            foreach($MODULE_OUTPUT["subjects"] as $subject) {
                if(!$j) { ?>
                <optgroup label="<?=$subject["department_name"]?> (<?=$subject["faculty_short_name"]?>)">
<?php               $j = $subject["department_id"];
                } else {
                    if($j != $subject["department_id"]) {
                        $j = $subject["department_id"]; ?>
                </optgroup>
                <optgroup label="<?=$subject["department_name"]?> (<?=$subject["faculty_short_name"]?>)">
<?php               }
                }
                if($data["subject_id"] == $subject["subject_id"]) { ?>
                    <option selected="selected" value="<?=$subject["subject_id"]?>"><?=$subject["subject_name"]?></option>
<?php           } else { ?>
                    <option value="<?=$subject["subject_id"]?>"><?=$subject["subject_name"]?></option>
<?php           }
            } ?>
                </optgroup>
            </select>
            <h3>������ �����:</h3>
<?php       $groups_list=null;
            foreach($data["groups"] as $group) {
                $groups_list = $groups_list.$group["group_name"].";";
            } ?>
            <input type="text" name="groups" value="<?=$groups_list?>" />
            <h3>��� �������:</h3>
<?php       if($data["type"]) { ?>
            <input type="radio" name="type" value="0" /> ��������� / ������������ <input type="radio" name="type" value="1" checked="checked" />
<?php       } else { ?>
            <input type="radio" name="type" value="0" checked="checked" /> ��������� / ������������ <input type="radio" name="type" value="1" />
<?php       } ?>
            <h3>���������� ����� (��� ��������, ���� ������� ������������):</h3>
            <input type="text" name="kol_groups" value="<?=$data["kol_groups"]?>" />
            <h3>���������� ����� �� �������:</h3>
            <input type="text" name="lecture_hours" value="<?=$data["lecture_hours"]?>" /> ����������<br />
            <input type="text" name="practice_hours" value="<?=$data["practice_hours"]?>" />������������
<?php       if($data["g_type"]) { ?>
            <h3>���������� ������������ �������:</h3>
            <input type="text" name="ust_lecture" value="<?=$data["ust_lecture"]?>" /> ������������ ������ (��)<br />
            <input type="text" name="ust_practice" value="<?=$data["ust_practice"]?>" /> ������������ �������� (��)
            <h3>��������, ���� ����:</h3>
<?php           if($data["Contr"]) { ?>
            <input type="checkbox" name="Contr" checked="checked" /> ����������� <br />
<?php           } else { ?>
            <input type="checkbox" name="Contr" /> ����������� <br />
<?php           }
                if($data["Curse"]) { ?>
            <input type="checkbox" name="Curse" checked="checked" /> �������� <br />
<?php           } else { ?>
            <input type="checkbox" name="Curse" /> �������� <br />
            <input type="checkbox" name="Curse" /> �������� <br />
<?php           } ?>
<?php       } ?>
            <h3>�������� ��� ����������:</h3>
<?php       if($data["attestation_type"]) { ?>
            <input type="radio" name="attestation_type" value="0" /> ������� / ����� <input type="radio" name="attestation_type" value="1" checked="checked" />
<?php       } else { ?>
            <input type="radio" name="attestation_type" value="0" checked="checked" /> ������� / ����� <input type="radio" name="attestation_type" value="1" />
<?php       } ?>
            <br />
            <input type="submit" value="��������� ���������" class="button" />
        </form>
<?php   }
    }
    break;

    case "faculties_files": {
        if(isset($MODULE_OUTPUT["faculties_files"])) {
            $fac = $MODULE_OUTPUT["faculties_files"];
            //echo "<h1>".$MODULE_OUTPUT["faculties_name"]."</h1>";
            foreach($fac as $data) {
                echo "<h2>".$data["faculties_name"]."</h2>";
                foreach($data["subject_list"] as $subject_list) {
                    echo "<h3>".$subject_list["subject_name"]."</h3>";
                    echo "<ul>";
                    foreach($subject_list["file_list"] as $file_list) {
                        if(isset($file_list["file_descr"]) && $file_list["file_descr"] != "")
                            echo "<li><a href=\"/file/".$file_list["file_id"]."/\">".$file_list["file_descr"]."</a></li>";
                        else
                            echo "<li><a href=\"/file/".$file_list["file_id"]."/\">".$file_list["file_name"]."</a></li>";
                    }
                    echo "</ul>";
                }
            }
        }
        break;
    }
    case "department_files": {
        if(isset($MODULE_OUTPUT["department_files"])) {
            $data = $MODULE_OUTPUT["department_files"];
            echo "<h1>".$MODULE_OUTPUT["department_name"]."</h1>";
            foreach($data as $subject_list) {
                echo "<h2>".$subject_list["subject_name"]."</h2>";
                echo "<ul>";
                foreach($subject_list["file_list"] as $file_list) {
                    if(isset($file_list["file_descr"]) && $file_list["file_descr"] != "")
                        echo "<li><a href=\"/file/".$file_list["file_id"]."/\">".$file_list["file_descr"]."</a></li>";
                    else
                        echo "<li><a href=\"/file/".$file_list["file_id"]."/\">".$file_list["file_name"]."</a></li>";
                }
                echo "</ul>";
            }
        }
        break;
    }

    case "faculty_specialities": {
        if(isset($MODULE_OUTPUT["faculty_spec"])) {
            echo "<h2>�������� ��������������</h2>";
            foreach($MODULE_OUTPUT["faculty_spec"] as $type => $list) {
                if($type == "secondary")
                    $type = "������� ���������������� �����������";
                elseif($type == "magistracy")
                    $type = "������������";
                elseif($type == "bachelor")
                    $type = "�����������";
                elseif($type == "higher")
                    $type = "������ ���������������� �����������";
                elseif($type == "graduate")
                    $type = "�����������";
                echo "<h4>".$type."</h4>";
                echo "<ul>";
                foreach($list as $item) {
                    echo "<li><a href=\"/speciality/".$item["code"]."/\">".$item["name"]."</a></li>";
                }
                echo "</ul>";
            }
        }
    }
    break;

    case "student_registr": 
    {
        if($MODULE_OUTPUT['allowed']) 
        {
            if(isset($MODULE_OUTPUT["display_variant"]["add_item"])) 
            {
                $display_variant = $MODULE_OUTPUT["display_variant"]["add_item"];
            } 
            ?>

            <div class="student_registr_form">
                <?php
                if(isset($MODULE_OUTPUT['active_request']))
                {
                    if($MODULE_OUTPUT['active_request'] == 1) 
                    { 
                        ?>
                        <p class="gray-radius-block">
                            <strong>
                                � ��������� ����� ��� ������ ����� ��������� �����������, � �� ��� �������� ���� ������ ��������������� ������.
                            </strong>
                        </p>
                        <?php           
                    } 
                    else 
                    { 
                        ?>
                        <p class="gray-radius-block">
                            <strong>��� e-mail ��� ����������.</strong>
                        </p>
                        <?
                    }
                } 
                else
                { 
                    ?>

            
                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <button type="button" class="btn btn btn-xs spoiler-trigger" data-toggle="collapse"><h5>���� �� �������</h5></button>
                        </div>
                        <div class="panel-collapse collapse out">
                          <div class="panel-body">
                            <p>��������� <strong>���� ���� ��������</strong> ������ � ������ ��� ������� � ������� �������� �������� �� ������� � �������� ���������������� ����������.</p>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <button type="button" class="btn btn btn-xs test" data-toggle="collapse"><h5>���� �� �������������, ��������� ��� ��������� �������</h5> </button>
                        </div>
                        <div class="panel-collapse collapse out">
                          <div class="panel-body">
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <button class="btn btn btn-xs spoiler-trigger" data-toggle="collapse"><h5>���� �� �������� ��� ��������� ����<h5></button>
                        </div>
                        <div class="panel-collapse collapse out">
                          <div class="panel-body">
                            <p>��� ����������� ��������� ����������� �� ��������������� ������� ���� ���������� ������� ����������� �����, ��������������� ��������� ������:</p>
                            <ul>
                                <li>
                                    <strong>�������������</strong> (<a href="http://nsau.edu.ru/file/73431/">�����</a> | <a href="http://nsau.edu.ru/file/73471/">�������</a>)
                                </li>
                                <li>
                                    <strong>��������</strong> (<a href="http://nsau.edu.ru/file/73421/">�����</a> | <a href="http://nsau.edu.ru/file/73461/">�������</a>)
                                </li>
                                <li>
                                    <strong>���������</strong> (<a href="http://nsau.edu.ru/file/73441/">�����</a> | <a href="http://nsau.edu.ru/file/73481/">�������</a>)
                                </li>
                            </ul>
                          </div>
                        </div>
                      </div>
           

                    <style type="text/css">
                        .container {
                          margin-top: 10px;

                        }
                        .panel {
                          box-shadow: none !important;
                           width: 100%;

                        }

                        .panel label {
                            cursor: pointer;
                        }
                    </style>

                    <script type="text/javascript">
                                                $(".spoiler-trigger").click(function() {
                            $(this).parent().next().collapse('toggle');
                        });
                    </script>


<!--                     <script src="/scripts/rekom.js"></script>
                    <h2><span class="bayan test"><a id="link12">���� �� �������������</a></span></h2>
                    <h2><span class="bayan"><a id="link12"  onclick="show_choice(this, 'show_form_ed');">���� �� �������</a></span></h2>
                    <div id="f_education" style="display:none;">
                        <p>
                            <div class="form_title">��������� <strong>���� ���� ��������</strong> ������ � ������ ��� ������� � ������� �������� �������� �� ������� � �������� ���������������� ����������.
                            </div>
                            <br />
                            <br />
                        </p>
                    </div>


                    <h2><span class="bayan" id="">���� �� �������� ��� ��������� ����</span></h2>
                    <div class="bayan_cont hidden">
                        <p>��� ����������� ��������� ����������� �� ��������������� ������� ���� ���������� ������� ����������� �����, ��������������� ��������� ������:</p>
                        <ul>
                            <li>
                                <strong>�������������</strong> (<a href="http://nsau.edu.ru/file/73431/">�����</a> | <a href="http://nsau.edu.ru/file/73471/">�������</a>)
                            </li>
                            <li>
                                <strong>��������</strong> (<a href="http://nsau.edu.ru/file/73421/">�����</a> | <a href="http://nsau.edu.ru/file/73461/">�������</a>)
                            </li>
                            <li>
                                <strong>���������</strong> (<a href="http://nsau.edu.ru/file/73441/">�����</a> | <a href="http://nsau.edu.ru/file/73481/">�������</a>)
                            </li>
                        </ul>

                        <p>����� ������ ������������ ����� ���� � ������� PDF � ������ ��� ����������. ��� ��� �������� � ���������� ������������ ��������� Adobe Reader �� ���� 9-� ������, ���� Foxit Phantom �� ���� 2-� ������.</p>
                        <p>������ ����� ���������� ��������� ��� ������ <strong>������� ����������-�������������� �������</strong> � �������� � ������������� ���� <a href="http://nsau.edu.ru/people/521888/">�������� ������� ���������</a> (��-304�) ��� ���������� � <a href="http://nsau.edu.ru/directory/responsible/">�������������� ����</a> �� �������������.</p>
                        <h2><strong></strong>��� ��� �����������?</h2>
                        <p>������������������ ������������ �����:</p>
                        <ul>
                            <li>������������ ���������� � ����;</li>
                            <li>��������� ������� ��������� � ���������;</li>
                            <li>�������� � ����������� ������ �� �������;</li>
                        </ul>
                        <p>������������������ ������������ ����� ����������� �������� ������ � �������� ���������� (�������, ������������ � ��.)</p>
                    </div>
                        <?php 
                } 
                ?>
                <script>
                    jQuery(document).ready(function($) {
                        //$('.student_registr_form .bayan_cont').hide();
                        $('.student_registr_form .bayan').click(function() {
                            $(this).parent().next('.bayan_cont').toggle();
                        });

                    });
                </script> -->





            </div>
            <?php
        }
    }
    break;
    case 'bootstrap_clear':
    {
        echo "<link rel='stylesheet' href='/themes/styles/bootstrap_clear/css/bootstrap.css' type='text/css'/>";
        echo "<link rel='stylesheet' href='/themes/styles/bootstrap_clear/css/nsau_fixer.css' type='text/css'/>";
        echo "<script type='text/javascript' src='/scripts/jq_1.11.3/jquery.min.js'></script>";
        echo "<script type='text/javascript' src='/themes/styles/bootstrap_clear/js/bootstrap.min.js'></script>";
       echo "<link rel='stylesheet' href='/themes/styles/bootstrap_clear/css/less/bootstrap.css' type='text/css'/>";
    }
    break;
    
    case 'bootstrap':
    {
        echo "<link rel='stylesheet' href='/themes/styles/bootstrap/css/bootstrap.css' type='text/css'/>";
        echo "<script type='text/javascript' src='/scripts/jq_1.11.3/jquery.min.js'></script>";
        echo "<script type='text/javascript' src='/themes/styles/bootstrap/js/bootstrap.min.js'></script>";
    }
    break;

    case 'bootstrap_less':
    {
        echo "<link rel='stylesheet' href='/themes/styles/bootstrap_less/less/bootstrap.css' type='text/css'/>";
        echo "<script type='text/javascript' src='/scripts/jq_1.11.3/jquery.min.js'></script>";
        echo "<script type='text/javascript' src='/themes/styles/bootstrap_less/dist/js/bootstrap.min.js'></script>";
    }
    break;

    case 'nsau_wifi':
    {
        ?>
        <style type="text/css">
            .loading {    
            background-color: #E1E1E1;
            background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' class='lds-comets' width='30px' height='30px' viewBox='0 0 100 100' preserveAspectRatio='xMidYMid' style='background: none;'%3E%3Cg transform='rotate%28236.575 50 50%29'%3E%3CanimateTransform attributeName='transform' type='rotate' repeatCount='indefinite' values='360 50 50;0 50 50' keyTimes='0;1' dur='1.5s' keySplines='0.5 0.5 0.5 0.5' calcMode='spline'/%3E%3Cpath fill='%23c4c4c4' d='M91,74.1C75.6,98,40.7,102.4,21.2,81c11,9.9,26.8,13.5,40.8,8.7c7.4-2.5,13.9-7.2,18.7-13.3 c1.8-2.3,3.5-7.6,6.7-8C90.5,67.9,92.7,71.5,91,74.1z'/%3E%3Cpath fill='%23bd0017' d='M50.7,5c-4,0.2-4.9,5.9-1.1,7.3c1.8,0.6,4.1,0.1,5.9,0.3c2.1,0.1,4.3,0.5,6.4,0.9c5.8,1.4,11.3,4,16,7.8 C89.8,31.1,95.2,47,92,62c4.2-13.1,1.6-27.5-6.4-38.7c-3.4-4.7-7.8-8.7-12.7-11.7C66.6,7.8,58.2,4.6,50.7,5z'/%3E%3Cpath fill='%23ffd398' d='M30.9,13.4C12,22.7,2.1,44.2,7.6,64.8c0.8,3.2,3.8,14.9,9.3,10.5c2.4-2,1.1-4.4-0.2-6.6 c-1.7-3-3.1-6.2-4-9.5C10.6,51,11.1,41.9,14.4,34c4.7-11.5,14.1-19.7,25.8-23.8C37,11,33.9,11.9,30.9,13.4z'/%3E%3C/g%3E%3C/svg%3E");
            background-size:35px 25px;
            background-position: right center;
            background-repeat: no-repeat;
            pointer-events:none;
        }
        </style>
        <div class="row"> 
            <div class="col-sm-4">
                <div class="input-group" style="padding-bottom: 2px;"> 
                    <span class="input-group-addon" id="basic-addon1">�����</span>
                    <input type="text" class="form-control wifi_login">
                </div>
               
                <div class="input-group" style="padding-bottom: 2px;"> 
                    <span class="input-group-addon" id="basic-addon1">������</span>
                    <input type="text" class="form-control wifi_password">
                </div>         
            </div>
            <div class="col-sm-8">
                <button class="btn btn-nsau btn-md set_wifi_password" type="button"></button>
            </div>
        </div>

        <?
    }
    break;

    case 'modules_options':
    {
        if ($Auth->usergroup_id==1) 
        {
            ?>

            <style type="text/css">
                .nav-tabs {
                    padding-bottom: 0px!important;
                }
            </style>
                <form method="POST">
                    <ul class="nav nav-tabs">

                        <li class="active">
                            <a data-toggle="tab" href="#students_upload">���������� � �������� ���������</a>
                        </li>
                        <li><a data-toggle="tab" href="#ensau">����� ���������</a></li>
                        <li><a data-toggle="tab" href="#registration">��������� �����������</a></li>
                        <li class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                ������
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                              <!-- <li><a data-toggle="tab" href="#password_restore">�������������� ������</a></li> -->
                              <!-- <li><a data-toggle="tab" href="#panel4">������ 4</a></li> -->
                            </ul>
                        </li>
                    </ul>
                    
                    <div class="tab-content">
                        <!-- ������ ������ ������������ -->
                        <div id="ensau" class="tab-pane fade in">
                            <br>
                            <?
                            foreach ($MODULE_OUTPUT["modules_options"] as $module_id => $options) 
                            {
                                if ($module_id == 5) 
                                {
                                    foreach ($options as $option => $value) 
                                    {
                                        if (strlen($value['value'])>1) 
                                        {
                                            if (strlen($value['value'])>80) 
                                            {
                                                ?>
                                                <div class="form-group">
                                                  <label for="<?=$option?>"><?=$value['description']?></label>
                                                  <textarea class="form-control" rows="5" id="<?=$option?>" name="<?=$option?>"><?=$value['value']?></textarea>
                                                </div>
                                                <?
                                            }
                                            else
                                            {
                                                ?>
                                                <div class="input-group"> 
                                                    <span class="input-group-addon" id="basic-addon1"><?=$value['description']?></span>
                                                    <input type="text" class="form-control" value="<?=$value['value']?>" name="<?=$option?>">
                                                </div>
                                                <?
                                            }
                                        }
                                        else
                                        {
                                            if ($value['value']==1) 
                                            {
                                                ?>
                                                <div class="checkbox-nsau"> 
                                                <input type="checkbox" id="<?=$option?>" name="<?=$option?>" checked >
                                                <label for="<?=$option?>"><?=$value['description']?></label>
                                            </div>

                                                <br>
                                                <?
                                            }
                                            else
                                            {
                                                ?>
                                                <div class="checkbox-nsau"> 
                                                <input name="<?=$option?>" type="checkbox" id="<?=$option?>">
                                                <label for="<?=$option?>"><?=$value['description']?></label>
                                            </div>
                                                <br>
                                                <?           
                                            }
                                        }
                                        //echo $option."-".$value."<br>";
                                    }
                                }
                            }
                            ?>
                        </div>
                        <!-- ������ ����������� -->
                        <div id="registration" class="tab-pane fade">
                            <br>
                            <?
                            foreach ($MODULE_OUTPUT["modules_options"] as $module_id => $options) 
                            {
                                if ($module_id == 50) 
                                {
                                    ?>
                                    <style type="text/css">
                                        .input-group {
                                            padding-bottom: 1px;
                                        }
                                    </style>
                                        <?
                                        foreach ($options as $option => $value) 
                                        {
                                            if (strlen($value['value'])>1) 
                                            {
                                                if (strlen($value['value'])>80) 
                                                {
                                                    ?>
                                                    <div class="form-group">
                                                      <label for="<?=$option?>"><?=$value['description']?></label>
                                                      <textarea class="form-control" rows="5" id="<?=$option?>" name="<?=$option?>"><?=$value['value']?></textarea>
                                                    </div>
                                                    <?
                                                }
                                                else
                                                {
                                                    ?>
                                                    <div class="input-group"> 
                                                        <span class="input-group-addon" id="basic-addon1"><?=$value['description']?></span>
                                                        <input type="text" class="form-control" value="<?=$value['value']?>" name="<?=$option?>">
                                                    </div>
                                                    <?
                                                }
                                            }
                                            else
                                            {
                                                if ($value['value']==1) 
                                                {
                                                    ?>
                                                    <div class="checkbox-nsau"> 
                                                    <input type="checkbox" id="<?=$option?>" name="<?=$option?>" checked >
                                                    <label for="<?=$option?>"><?=$value['description']?></label>
                                                </div>
                                                    <br>
                                                    <?
                                                }
                                                else
                                                {
                                                    ?>
                                                    <div class="checkbox-nsau"> 
                                                    <input name="<?=$option?>" type="checkbox" id="<?=$option?>" >
                                                    <label for="<?=$option?>"><?=$value['description']?></label>
                                                </div>
                                                    <br>
                                                    <?           
                                                }
                                            }
                                            //echo $option."-".$value."<br>";
                                        }
                                        ?>
                                    <?
                                }
                            }
                            ?>
                        </div>





                        <div id="students_upload" class="tab-pane fade in active">
                            <div id='app'>
                                <table class="ui single line table selectable fixed">
                                    {{result_files.name}}
                                    <thead>
                                        <tr>
                                            <th>����</th>
                                            <th>�����</th>
                                            <th>���������</th>
                                            <th>�������</th>
                                            <th>����</th>
                                        </tr>
                                    </thead>

                                    <tbody v-for='file in files'>
                                        <tr>
                                            <td>{{file.date}}</td>
                                            <td>{{file.time}}</td>
                                            <td>{{file.facult}}</td>
                                            <td>{{file.count}}</td>
                                            <td> <a v-bind:href="file.file">�������</a></td>
                                        </tr>  
                                    </tbody>
                                </table>
                            </div>       
                        </div>









                        <div id="password_restore" class="tab-pane fade">

                        </div>
                        <div id="panel4" class="tab-pane fade">
                            <h3>������ 4</h3>
                            <p>���������� 4 ������...</p>
                        </div>
                    </div>
                    <input type="submit" name="save_options">
                </form>
            
            <?
        }
    }
    break;

    case 'employees': {?>
        <div class="employees <?=(empty($_GET["page"]) ? "hidden" : "")?>">
            <h2>������ �������������� ���������� ��������������� �����������</h2>
            <input type="text" class="teacher_search" placeholder="����� ���"><br /><br />

            <?php if(!empty($MODULE_OUTPUT["filter"])) { ?>
            <h2>����� �����������</h2>
            <table class="nsau_table" style="width: 100%;" itemprop="" border="0" align="center">
                <thead>
                <tr class="tr_highlight_berry">
                <?php if(!empty($MODULE_OUTPUT["filter"]["post"])){ ?>
                    <th colspan="2" style="text-align: center;">���������</th>
                <?php } ?>
                <?php if(!empty($MODULE_OUTPUT["filter"]["subjects"])){ ?>
                    <th colspan="2" style="text-align: center;">������������� ����������</th>
                <?php } ?>
                <?php if(!empty($MODULE_OUTPUT["filter"]["level"])){ ?>
                    <th colspan="2" style="text-align: center;">������� �����������</th>
                <?php } ?>
                <?php if(!empty($MODULE_OUTPUT["filter"]["degree"])){ ?>
                    <th colspan="2" style="text-align: center;">������ �������</th>
                <?php } ?>
                <?php if(!empty($MODULE_OUTPUT["filter"]["rank"])){ ?>
                    <th colspan="2" style="text-align: center;">������ ������</th>
                <?php } ?>
                <?php if(!empty($MODULE_OUTPUT["filter"]["program"])){ ?>
                    <th colspan="2" style="text-align: center;">��������� ��</th>
                <?php } ?>
                </tr>
                </thead>
                <tbody>
                <tr>
			        <?php if(!empty($MODULE_OUTPUT["filter"]["post"])){ ?>
                        <td style="text-align: center; border-right: none;">
                            <select class="form-control filter" name="teacher_post" style="width: 99.8%;">
                                <option value="0" selected="selected">-</option>
                                <?php foreach($MODULE_OUTPUT["filter"]["post"] as $id => $post){ ?>
                                        <option value="<?=$id?>"><?=$post?></option>
                                <?php } ?>
                            </select>
                        </td>
                        <td style="text-align: center; border-left: none;">
                            <a title="������� ������ ���������" data-filter="teacher_post" href="#" onclick="return false;"><img src="/themes/images/delete.png" alt="������� ������ ��������"></a>
                        </td>
			        <?php } ?>
                    <?php if(!empty($MODULE_OUTPUT["filter"]["subjects"])){ ?>
                        <td style="text-align: center; border-right: none;">
                            <select class="form-control filter" name="teachers_subject" style="width: 99.8%;">
                                <option value="0" selected="selected">-</option>
                                <?php foreach($MODULE_OUTPUT["filter"]["subjects"] as $id => $subjects){ ?>
                                        <option value="<?=$id?>"><?=$subjects?></option>
                                <?php } ?>
                            </select>
                        </td>
                         <td style="text-align: center; border-left: none;">
                            <a title="������� ������ ������������� ����������" data-filter="teachers_subject" href="#" onclick="return false;"><img src="/themes/images/delete.png" alt="������� ������ ������������� ����������"></a>
                        </td>
                    <?php } ?>
                    <?php if(!empty($MODULE_OUTPUT["filter"]["level"])){ ?>
                        <td style="text-align: center; border-right: none;">
                            <select class="form-control filter" name="teachers_level" style="width: 99.8%;">
                                <option value="0" selected="selected">-</option>
                                <?php foreach($MODULE_OUTPUT["filter"]["level"] as $id => $level){ ?>
                                    <option value="<?=$id?>"><?=$level?></option>
                                <?php } ?>
                            </select>
                        </td>
                         <td style="text-align: center; border-left: none;">
                            <a title="������� ������ ������� �����������" data-filter="teachers_level" href="#" onclick="return false;"><img src="/themes/images/delete.png" alt="������� ������ ������� �����������"></a>
                        </td>
                    <?php } ?>
                    <?php if(!empty($MODULE_OUTPUT["filter"]["degree"])){ ?>
                        <td style="text-align: center; border-right: none;">
                            <select class="form-control filter" name="teachers_degree" style="width: 99.8%;">
                                <option value="0" selected="selected">-</option>
                                <?php foreach($MODULE_OUTPUT["filter"]["degree"] as $id => $degree){ ?>
                                    <option value="<?=$id?>"><?=$degree?></option>
                                <?php } ?>
                            </select>
                        </td>
                         <td style="text-align: center; border-left: none;">
                            <a title="������� ������ ������ �������" data-filter="teachers_degree" href="#" onclick="return false;"><img src="/themes/images/delete.png" alt="������� ������ ������ �������"></a>
                        </td>
                    <?php } ?>
                    <?php if(!empty($MODULE_OUTPUT["filter"]["rank"])){ ?>
                        <td style="text-align: center; border-right: none;">
                            <select class="form-control filter" name="teachers_rank" style="width: 99.8%;">
                                <option value="0" selected="selected">-</option>
                                <?php foreach($MODULE_OUTPUT["filter"]["rank"] as $id => $rank){ ?>
                                    <option value="<?=$id?>"><?=$rank?></option>
                                <?php } ?>
                            </select>
                        </td>
                         <td style="text-align: center; border-left: none;">
                            <a title="������� ������ ������ ������" data-filter="teachers_rank" href="#" onclick="return false;"><img src="/themes/images/delete.png" alt="������� ������ ������ ������"></a>
                        </td>
                    <?php } ?>
                    <?php if(!empty($MODULE_OUTPUT["filter"]["program"])){ ?>
                        <td style="text-align: center; border-right: none;">
                            <select class="form-control filter" name="teachers_program" style="width: 99.8%;">
                                <option value="0" selected="selected">-</option>
                                <?php foreach($MODULE_OUTPUT["filter"]["program"] as $id => $program){ ?>
                                        <option value="<?=$id?>"><?=$program["code"] ." - ". $program["name"]?></option>
                                <?php } ?>
                            </select>
                        </td>
                         <td style="text-align: center; border-left: none;">
                            <a title="������� ������ ��������� ��" data-filter="teachers_program" href="#" onclick="return false;"><img src="/themes/images/delete.png" alt="������� ������ ��������� ��"></a>
                        </td>
                    <?php } ?>
                </tr>
                </tbody>
            </table><br /><br />
            <?php } ?>


            <div class="teachers">
                <table class="nsau_table employees" width="100%" itemprop="teachingStaff">
                    <thead>
                        <tr>
                            <th width="10%"><div>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp���&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</div></th>
                            <th width="5%"><div>���������</div></th>
                            <th width="14%"><div>������������� ����������</div></th>

                            <th width="8%"><div>������� �����������</div></th>
                            <th width="8%"><div>�������������, ������������</div></th>

                            <th width="8%"><div>������ �������</div></th>
                            <th width="8%"><div>������ ������</div></th>
                            <th width="20%"><div>����������� ����������</div></th>
                            <th width="25%"><div>��������� ������������</div></th>
                            <th width="5%"><div>����� ���� ������, ���</div></th>
                            <th width="5%"><div>���� ������ �� �������� �����, ���</div></th>
                            <th width="5%"><div>��������� ��</div></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?foreach($MODULE_OUTPUT["teachers"] as $pid => $people){?>
                            <tr>
                                <td itemprop="fio"><?=$people["last_name"]?> <?=$people["name"]?> <?=$people["patronymic"]?></td>
                                <td itemprop="post"><?=$people["post"]?>
                                    
                                </td>
                                <td itemprop="teachingDiscipline">
                                    <?foreach($people["subj"] as $i => $subj) {?>
                                        <span><?=++$i?>.&nbsp<?=$subj?></span><br />
                                    <?}?>
                                </td>

                                <td itemprop="teachingLevel">
                                    <?
                                    foreach ($people['edu'] as $key => $value) 
                                    {
                                       $qual = mb_strtolower($value['lvl'] ,'windows-1251');
                                       echo $qual." <br><br>";
                                    }
                                    ?>
                                </td>

                                <td itemprop="teachingQual">
                                    <?
                                    foreach ($people['edu'] as $key => $value)
                                    {
                                       $qual = mb_strtolower($value['spec'].', ' ,'windows-1251');
                                       $qual .= mb_strtolower($value['qual'] ,'windows-1251');
                                       echo $qual."<br>";
                                    }
                                    ?>
                                </td>
                                <td itemprop="degree">
                                    <?foreach($people["degree"] as $i => $degr) {?>
                                        <span><?=++$i?>.&nbsp<?=$degr?></span><br />
                                    <?}?>
                                </td>
                                <td itemprop="academStat">
                                    <?foreach($people["rank"] as $i => $rank) {?>
                                        <span><?=++$i?>.&nbsp<?=$rank?></span><br />
                                    <?}?>
                                </td>
                                <td itemprop="employeeQualification">
                                    <?foreach($people["edu"] as $i => $edu) {?>
                                        <span><?=++$i?>.&nbsp�����������: ������, <?=$edu["edu"]?>. ������������: <?=$edu["qual"]?>. �������������: <?=$edu["spec"]?></span><br />
                                    <?}?>
                                </td>
                                <td itemprop="profDevelopment">
                                    <?foreach($people["aedu"] as $i => $aedu) {?>
                                        <span><?=++$i?>. <?=$aedu["edu_type"]?>, <?=$aedu["location"]?>, <?=$aedu["course"]?></span><br />
                                    <?}?>
                                </td>
                                <td itemprop="genExperience"><?=(!empty($people["exp_full"]) && ($people["exp_full"]!=date("Y"))) ? date("Y")-$people["exp_full"] : (($people["exp_full"]==date("Y")) ? "1" : "")?></td>
                                <td itemprop="specExperience"><?=(!empty($people["exp_teach"]) && ($people["exp_teach"]!=date("Y"))) ? date("Y")-$people["exp_teach"] : (($people["exp_teach"]==date("Y")) ? "1" : "")?></td>
                                <td itemprop="educationProgram">
                                    <? foreach($people["programs"] as $i => $program) {
                                        $prstr = $program['code'].' - ';
                                        $prstr .= mb_strtolower($program['name'].'; ' ,'windows-1251');
                                        echo $prstr."<br />";
                                    } ?>
                                </td>
                            </tr>
                        <?}?>
                    </tbody>
                </table>
            </div>

            <?  $pager_output = $MODULE_OUTPUT["pager_output"];     if (count($pager_output["pages_data"]) > 1) { // ���� ���� ��� ��������, � ������� ������ �����
                echo "<div class=\"pager\"><p>\n";
                if($pager_output["prev_page"]) {
                        echo "<a href=\"{$pager_output["pages_data"][$pager_output["prev_page"]]["link"]}\" title=\"" . $LANGUAGE["news"]["previous_page"] . "\"";
                        echo " id=\"prev_page_link\" class=\"prev-next-link\"><span class=\"arrow\">&larr;&nbsp;</span>";
                        echo $pager_output["prev_page"] ? "</a>" : "</span>";
                        echo " ";
                }?>
                <?if($pager_output["current_page"]>8){?>
                    <a href="?page=1">1</a>
                <?}?>
                <?foreach ($pager_output["pages_data"] as $page => $data) {
                        if (is_int($page)) {
                                echo ($data["is_current"] ? "<strong>" : "<a href=\"{$data["link"]}\">") . $page . ($data["is_current"] ? "</strong>" : "</a>") . " ";
                        }
                        else {
                                echo "&hellip; ";
                        }
                }?>
                <?if($pager_output["pages_num"]>($pager_output["current_page"]+6)){?>
                    <a href="?page=<?=$pager_output["pages_num"]?>"><?=$pager_output["pages_num"]?></a>
                <?}?>
                <?if ($pager_output["next_page"]) {
                        echo "<a href=\"{$pager_output["pages_data"][$pager_output["next_page"]]["link"]}\" title=\"" . $LANGUAGE["news"]["next_page"] . "\"";
                        echo " id=\"next_page_link\" class=\"prev-next-link\">" /*. $LANGUAGE["news"]["next"]*/ . "<span class=\"arrow\">&nbsp;&rarr;</span>";
                        echo "</a>";
                }?>

                <input type="text" placeholder="�" size="2" class="to_page"><input type="submit" onclick="document.location.href = '?page='+jQuery('.to_page').val();" value="�������">
                <?echo "    </p>\n";?>
                <?echo "</div>\n";
            }?>
    </div>
    <?}
    break;
    
    case 'employees_ajax_search': {?>
        <?header("Content-type: text/html; charset=windows-1251");?>
                <table class="nsau_table employees" width="100%" itemprop="teachingStaff">
                    <thead>
                        <tr>
                            <th width="10%"><div>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp���&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</div></th>
                            <th width="5%"><div>���������</div></th>
                            <th width="14%"><div>������������� ����������</div></th>
                            <th width="8%"><div>������� �����������</div></th>
                            <th width="8%"><div>������������</div></th>
                            <th width="8%"><div>������ �������</div></th>
                            <th width="8%"><div>������ ������</div></th>
                            <th width="20%"><div>����������� ����������</div></th>
                            <th width="25%"><div>��������� ������������</div></th>
                            <th width="5%"><div>����� ���� ������, ���</div></th>
                            <th width="5%"><div>���� ������ �� �������� �����, ���</div></th>
                            <th width="5%"><div>��������� ��</div></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?foreach($MODULE_OUTPUT["teachers"] as $pid => $people){?>
                            <tr>
                                <td itemprop="fio"><?=$people["last_name"]?> <?=$people["name"]?> <?=$people["patronymic"]?></td>
                                <td itemprop="post"><?=$people["posts"]?></td>
                                <td itemprop="teachingDiscipline">
                                    <?foreach($people["subj"] as $i => $subj) {?>
                                        <span><?=++$i?>.&nbsp<?=$subj?></span><br />
                                    <?}?>
                                </td>

                                <td itemprop="teachingLevel">
                                    <?
                                    foreach ($people['edu'] as $key => $value) 
                                    {
                                       $qual = mb_strtolower($value['lvl'] ,'windows-1251');
                                       echo $qual." <br><br>";
                                    }
                                    ?>
                                </td>

                                <td itemprop="teachingQual">
                                    <?
                                    foreach ($people['edu'] as $key => $value) 
                                    {
                                        $qual = mb_strtolower($value['spec'].', ' ,'windows-1251');
                                        $qual .= mb_strtolower($value['qual'] ,'windows-1251');
                                        echo $qual."<br>";
                                    }
                                    ?>
                                </td>
                                <td itemprop="degree">
                                    <?foreach($people["degree"] as $i => $degr) {?>
                                        <span><?=++$i?>.&nbsp<?=$degr?></span><br />
                                    <?}?>
                                </td>
                                <td itemprop="academStat">
                                    <?foreach($people["rank"] as $i => $rank) {?>
                                        <span><?=++$i?>.&nbsp<?=$rank?></span><br />
                                    <?}?>
                                </td>
                                <td itemprop="employeeQualification">
                                    <?foreach($people["edu"] as $i => $edu) {?>
                                        <span><?=++$i?>.&nbsp�����������: ������, <?=$edu["edu"]?>. ������������: <?=$edu["qual"]?>. �������������: <?=$edu["spec"]?></span><br />
                                    <?}?>
                                </td>
                                <td itemprop="profDevelopment">
                                    <?foreach($people["aedu"] as $i => $aedu) {?>
                                        <span><?=++$i?>. <?=$aedu["edu_type"]?>, <?=$aedu["location"]?>, <?=$aedu["course"]?></span><br />
                                    <?}?>
                                </td>
                                <td itemprop="genExperience"><?=(!empty($people["exp_full"]) && ($people["exp_full"]!=date("Y"))) ? date("Y")-$people["exp_full"] : (($people["exp_full"]==date("Y")) ? "1" : "")?></td>
                                <td itemprop="specExperience"><?=(!empty($people["exp_teach"]) && ($people["exp_teach"]!=date("Y"))) ? date("Y")-$people["exp_teach"] : (($people["exp_teach"]==date("Y")) ? "1" : "")?></td>
                                <td itemprop="educationProgram">
                                    <? foreach($people["programs"] as $i => $program) {
                                        $prstr = $program['code'].' - ';
                                        $prstr .= mb_strtolower($program['name'].'; ' ,'windows-1251');
                                        echo $prstr."<br />";
                                    } ?>
                                </td>
                            </tr>
                        <?}?>
                    </tbody>
                </table>
    <?}
    break;

    case 'ajax_students_progress': {?>
        <?header("Content-type: text/html; charset=windows-1251");?>
        <?foreach($MODULE_OUTPUT["progress"] as $sid => $semester) {?>
            <?if(count($MODULE_OUTPUT["progress"])>1) {?><p><?=$sid?> �������.</p><?}?>
            <table class="nsau_table" width="100%">
                <thead>
                    <tr>
                        <th width="">����������</th>
                        <th width="">������������</th>
                        <th width="">���</th>
                        <th width="">������</th>
                    </tr>
                </thead>
                <tbody>
                    <?
                    $semester= array_map("unserialize", array_unique( array_map("serialize", $semester )));
                    foreach($semester as $people) {?>
                        <tr>
                            <td><?=$people["subject"]?></td>
                            <td><?=$people["hours"]?></td>
                            <td><?=$people["type"]?></td>
                            <td><?=$people["value"]?></td>
                        </tr>
                    <?}?>
                </tbody>
            </table>
        <?}?>
        <?if(empty($MODULE_OUTPUT["progress"])) {?>
            <p>��� ������</p>
        <?}?>
    <?}
    break;
    case 'students_progress': {//CF::Debug($MODULE_OUTPUT["progress"]);?>
        <p>������: <?=$MODULE_OUTPUT["group"]?>, <?=$MODULE_OUTPUT["course"]?> ����.</p>
        <p>

            ������ ��
            <select class="change_semester">
                <?for($i=1;$i<=$MODULE_OUTPUT["course"]*2;$i++) {?>
                    <option value="<?=$i?>"><?=$i?></option>
                <?}?>
                    <option value="all">1-<?=$MODULE_OUTPUT["course"]*2?></option>
            </select>
            �������.
        </p>
        <div class="progress_content">
            <table class="nsau_table" width="100%">
                <thead>
                    <tr>
                        <th width="">���������</th>
                        <th width="">������������</th>
                        <th width="">���</th>
                        <th width="">������</th>
                    </tr>
                </thead>
                <tbody>

                    <?
                    $data= array_map("unserialize", array_unique( array_map("serialize", $MODULE_OUTPUT["progress"][1]) ));

                    //CF::Debug($test);
                    foreach($data as $people) {?>
                        <tr>
                            <td><?=$people["subject"]?></td>
                            <td><?=$people["hours"]?></td>
                            <td><?=$people["type"]?></td>
                            <td><?=$people["value"]?></td>
                        </tr>
                    <?}?>
                    <?if(empty($data)) {?>
                        <tr>
                            <td colspan="4">��� ������</td>
                        </tr>
                    <?}?>
                </tbody>
            </table>
        </div>
    <?}
    break;
    case 'student_moderation': {
        if(isset($MODULE_OUTPUT['student_list']) && count($MODULE_OUTPUT['student_list'])) {
            /*echo "<pre>";
            print_r($MODULE_OUTPUT['student_list']);
            echo "</pre>";*/ ?>
            <div class="student_moderation">
                <table>
                    <thead>
                        <tr>
                            <td class="student_name">�.�.�.</td>
                            <td class="student_group">������</td>
                            <td class="student_year">��� �����������</td>
                            <td class="student_email">e-mail</td>
                            <td class="student_library_card">� ���. ������</td>
                            <td class="student_date">���� ������ ������</td>
                            <td class="student_item_form" colspan="2">���������������� / ���������</td>
                        </tr>
                    </thead>
                    <tbody>
<?php       foreach($MODULE_OUTPUT['student_list'] as $id => $students) { ?>
                        <tr>
                            <td class="student_name"><?=$students['last_name']?><br /><?=$students['name']?><br /><?=$students['patronymic']?></td>
                            <td class="student_group"><?=$students['group']?> <?=$students['subgroup']?></td>
                            <td class="student_year"><?=$students['year']?></td>
                            <td class="student_email"><?=$students['email']?></td>
                            <td class="student_library_card"><?=$students['library_card']?></td>
                            <td class="student_date"><?=$students['create_time']?></td>
                            <td class="student_item_form student_register">
                                <form action="<?=$EE["unqueried_uri"]?>" method="post">
                                    <input type="hidden" value="<?=$id?>" name="student_temp_id" />
                                    <input type="hidden" value="<?=$students['user_id']?>" name="user_id" />
                                    <input type="submit" value="" title="����������������" name="register" onclick="return confirm('�� ������� � ���, ��� ������ ���������������� ������� ��������?');" />
                                </form>
                            </td>
                            <td class="student_item_form student_delete">
                                <form action="<?=$EE["unqueried_uri"]?>" method="post">
                                    <input type="hidden" value="<?=$id?>" name="student_temp_id" />
                                    <input type="hidden" value="<?=$students['user_id']?>" name="user_id" />
                                    <input type="submit" value="" title="���������" name="delete" onclick="return confirm('�� ������� � ���, ��� ������ �������� ������� �������� � �����������?');" />
                                </form>
                            </td>
                        </tr>
<?php       } ?>
                    </tbody>
                </table>
            </div>
<?php   }
    }
    break;

    case "department_curators":
        if (isset($MODULE_OUTPUT["curators_info"])) { ?>
        <p>�������� ������������ �����:</p>
    <ul>
<?php
        foreach($MODULE_OUTPUT["curators_info"] as $info) { ?>
                <li><?=implode(',', $info["groups"])?> ��. - <a href="<?=$info["href"]?>"><span class="name-color"><?=$info["name"]?></span></a></li>

<?php       }
    ?>
    </ul>
    <?php
    }
  ?>
  <?php foreach($MODULE_OUTPUT["curators_info"] as $info) {
  if($info["text"]){
  ?>
  <p style="text-align:: center;"><strong><?=$info["name"] ?></strong></p>
  <hr />
  <?=$info["text"]?>
  <?php
    }
  } ?>
  <?php
    break;
    case "countdown": {?>
    <script src="/scripts/countdown.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="<?=$EE["http_styles"]?>coconut_style2.css?<?=filemtime($_SERVER['DOCUMENT_ROOT'].$EE['http_styles'].'coconut_style2.css')?>">

        <script type="text/javascript">
            jQuery(document).ready(function () {
                $("#circular-countdown").coconut({
                    color : "#b70202",
                      startDate: "28/07/2022",
                      endDate: "03/08/2022",
                    S_size : "small",
                    M_size : "small",
                    H_size : "small",
                    D_size : "small",
                    endTime: "18:00:00",
                });
            });
        </script>

        <div id="wrapper-countdown" class="countdown" style="box-shadow: rgb(194, 194, 194) 0px 0px 10px -2px; padding-top: 8px;display: block;width: 43%; margin: 2% auto;">
            <div class="title-countdown top-t">��������� � �������� �� ���������� ����������� �� 3.08.2022</div>
            <div class="title-countdown" style="margin: 25px 10px;">��������:</div>
            <div id="circular-countdown"></div>
            <div class="title-countdown">
            <div class="time">���</div>
            <div class="time">����</div>
            <div class="time">������</div>
            <div class="time">�������</div></div>
        </div>

    <?}?>
    <?break;

    case 'wmuslider':
        {
        ?>
                        <!-- slider  -->
    <div class="wmuSlider example3">
        <div class="wmuSliderWrapper">


            <article>
            <a href="http://mcx.ru/" target="_blank">
                <img class="slider_img" src="images/slider/03_1.PNG" />
                <div class="capt">���������� ��</div>
            </a>
            </article>

             <article>
                <a href="http://www.fsvps.ru/" target="_blank">
                <img class="slider_img" src="images/slider/22.PNG" />
                <div class="capt">����������������</div>
                </a>
            </article>

            <article>
            <a href="https://minobrnauki.gov.ru/" target="_blank">
                <img class="slider_img" src="images/slider/02.PNG" />
                  <div class="capt">�����������</div>
             </a>
            </article>

            <article>
            <a href="http://obrnadzor.gov.ru/" target="_blank">
                <img class="slider_img" src="images/slider/01.PNG" />
                <div class="capt">������������</div>
               </a>
            </article>
            <article>
            <a href="http://rostrud.ru/" target="_blank">
                <img class="slider_img" src="images/slider/05.PNG" />
                <div class="capt">�������</div>
            </a>
            </article>
              <article>
            <a href="http://mcx.nso.ru/" target="_blank">
                <img class="slider_img" src="images/slider/logo_nsk_2.jpg" />
                <div class="capt">���������� ���</div>
            </a>
            </article>

            <article>
                <a href="http://zakupki.gov.ru/" target="_blank">
                <img class="slider_img" src="images/slider/20.PNG" />
                <div class="capt">����������</div>
                </a>
            </article>

            <article>
            <a href="http://agrovuz.ru/" target="_blank">
                <img class="slider_img" src="images/slider/07.PNG" />
                <div class="capt">�������</div>
            </a>
            </article>

            <article>
            <a href="http://rssm.su/" target="_blank">
                <img class="slider_img" src="images/slider/16_1.PNG" />
                <div class="capt_small">����</div>
            </a>
            </article>

            <article>
            <a href="http://edu.ru/" target="_blank">
                <img class="slider_img" src="images/slider/09.PNG" />
                <div class="capt_small">���������� �����������</div>
            </a>
            </article>

            <article>
            <a href="http://window.edu.ru/" target="_blank">
                <img class="slider_img" src="images/slider/10.PNG" />
                <div class="capt_small">������ ����</div>
            </a>
            </article>

            <article>
            <a href="http://fcior.edu.ru/" target="_blank">
                <img class="slider_img" src="images/slider/12.PNG" />
                <div class="capt_small">����������� ����� ���</div>
            </a>
            </article>

            <article>
            <a href="http://scienceport.ru/" target="_blank">
                <img class="slider_img" src="images/slider/14.PNG" />
                <div class="capt_small">����� � ����������� ������ �������</div>
            </a>
            </article>

            <article>
            <a href="http://�����.��/" target="_blank">
                <img class="slider_img" src="images/slider/15.PNG" />
                <div class="capt_small">�����</div>
            </a>
            </article>

            <article>
            <a href="http://spo.wil.ru/" target="_blank">
                <img class="slider_img" src="images/slider/19.PNG" />
                <div class="capt_small">��������� ������������ �����������</div>
            </a>
            </article>

            <article>
            <a href="http://pravo.minjust.ru/" target="_blank">
                <img class="slider_img" src="images/slider/5mm.jpg" />
                <div class="capt_small">��� ��</div>
            </a>
            </article>

            <article>
            <a href="http://vayenshtefan.ru/" target="_blank">
                <img class="slider_img" style="margin-top: 10px;" src="images/slider/23.gif" />
                <div class="capt_small" style="margin-top: 5px;">��� �����������</div>
            </a>
            </article>
             <article>
            <a href="http://rcmp-nso.ru" target="_blank">
                <img class="slider_img" style="margin-top: 10px;" src="images/slider/rpmi.jpg" />
                <div class="capt_small" style="margin-top: 5px;">����</div>
            </a>
            </article>

            <article>
            <a href="http://www.avroradg.ru/" target="_blank">
                <img class="slider_img" src="images/slider/24.png" />
                <div class="capt_small" style="margin-top: 5px;">��� ������</div>
            </a>
            </article>
              
            <article>
            <a href="https://xn--2020-k4dg3e.xn--p1ai/" target="_blank">
                <img class="slider_img" src="images/slider/year2020.png" />
                <div class="capt">��� ������ � ����� - 2020</div>
            </a>
            </article>         
<!--             <article>
                <a href="">
                <img class="slider_img" src="images/slider/06.PNG" />
                <div class="capt">����!</div>
                </a>
            </article> -->

        </div>
    </div>

    <script type="text/javascript" src="scripts/jquery.wmuSlider.min.js"></script>
    <link rel="stylesheet" href="themes/styles/slider.css" type="text/css" media="screen" />
    <script>
        if( screen.width <= 1024 )
        {
            var items = 2;
        }
        else
        {
            var items = 5;
        }
        $('.example3').wmuSlider(
        {
            touch: Modernizr.touch,
            animation: 'slide',
                        slideshow: false,
            items: items
        });
    </script>
    <!-- end slider -->
        <?
        }
    break;

    case "sw_group_search": {?>
    <?header("Content-type: text/html; charset=windows-1251");?>
        <select class="form-control sw_group_select" name="sw_group_select" style="margin-top: 0px; width: 99.8%;">
            <?foreach($MODULE_OUTPUT["group2"] as $group) {?>
                <option value="<?=$group["id"]?>"><?=$group["name"]?></option>
            <?}?>
        </select>
    <?}?>
    <?break;

    case "sw_group_select": {?>
    <?header("Content-type: text/html; charset=windows-1251");?>

    <?$group = $MODULE_OUTPUT["group"];?>
            
    <?if(!empty($group["faculty"])) {?>
    <div class="alert alert-success" style="margin-bottom: 3px;">
        <?=$group["faculty"]?><br />
        <?=$group["speciality"]?><br />
        <?=$group["form"]?>
    </div>
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1" style="border-bottom: 0px!important; min-width: 150px;">�������</span>
                <select class="form-control sw_department" name="sw_department" style="border-bottom: 0px!important;" required>
                <option value="">�������</option>
                <?
                    foreach ($group["departments"] as $fac_name => $kaf )
                    {
                        ?>
                            <optgroup label="<?=$fac_name?>">
                            <?
                                foreach($kaf as $dep_id => $dep_name)
                                {
                                    ?>
                                    <option value="<?=$dep_id?>"><?=$dep_name?></option>
                                    <?
                                }
                            ?>
                        <?
                    }
                    ?>
            </select> 
            </div>

            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1" style="min-width: 150px;">��� ��������</span>
                <select class="form-control sw_student_fio" name="sw_student_fio" style="margin-top: 0px;" required>
                    <option value="">�������</option>
                    <?foreach($group["students"] as $stud_id => $stud_name) {?>
                        <?if(!empty($stud_id)) {?>
                            <option value="<?=$stud_id?>"><?=$stud_name?></option>
                        <?}?>
                    <?}?>
                </select>
                <input type="text" class="form-control student_print" size="7" value="<?=$sw["sw_student_name"]?>" title="������� ���" name="sw_student_name" placeholder="������� ���" style="display: <?=$sw["sw_student_name"] ? "block" : "none"?>; width: 100%; " />
            </div>

            <a href="javascript: void(0)" class="btn btn-nsau-nsau btn-xs cl_student_print"  title="����� �� ���">���� ��� � ����</a>


        <?} else {?>
                <p class="red">������ �� �������.</p>
        <?}?>
    <?}?>
    <?break;

    case "sw_edit_form": {?>
    <?header("Content-type: text/html; charset=windows-1251");?>
    <?$sw = $MODULE_OUTPUT["sw"]; ?>

    <div class="panel panel-nsau" style="margin-bottom: 0px; width: 900px;"> 
        <div class="panel-heading">������������� ������</div> 
        <div class="panel-body">
            <form action="/office/student_work/" method="POST" enctype="multipart/form-data" onsubmit="return formSubmit()">
                <div class="row"> 
                    <div class="col-sm-6">
                        <div class="close">X</div>
                        <input type="hidden" name="edit" value="<?=$sw["id"]?>">

                        <p class="red err_msg hidden"></p>

                    
                        <div class="input-group"> 
                            <span class="input-group-addon" id="basic-addon1" style="border-bottom: 0px; min-width: 150px;">��� �������</span>
                            <input type="text" style="border-bottom: 0px!important;" class="form-control sw_year_end date_year" name="sw_year_end" value="<?=$sw["year"]?>" required><span class="red date_error"></span>
                        </div>

                        <div class="input-group"> 
                            <span class="input-group-addon" id="basic-addon1" style="border-bottom: 0px!important; min-width: 150px;">����� ������</span>
                            <input type="text" class="form-control sw_group" value="<?=$sw["group_name"]?>" style="border-bottom: 0px!important;" required>
                        </div>


                        <div class="sw_search_group_content">
                            <select class="form-control sw_group_select" name="sw_group_select" style="margin-bottom: 0px; margin-top: 0px; width: 99.8%;">
                                <?foreach($MODULE_OUTPUT["group2"] as $group) {?>
                                    <option value="<?=$group["id"]?>"><?=$group["name"]?></option>
                                <?}?>
                            </select>
                        </div>


                        <div class="sw_select_group_content">
                            <?$group = $MODULE_OUTPUT["group"];?>

                            <div class="alert alert-success" style="margin-bottom: 0px;">
                                <?=$group["faculty"]?><br />
                                <?=$group["speciality"]?><br />
                                <?=$group["form"]?>
                            </div>

                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1" style="border-bottom: 0px!important; min-width: 150px;">�������</span>
                                <select class="form-control sw_department" name="sw_department" style="border-bottom: 0px!important;" required>
                                    <?
                                    foreach($group["departments"] as $key => $value) 
                                    {
                                        foreach ($value as $dep_id => $dep_name) 
                                        {
                                            ?>
                                            <option <?=$dep_id==$sw["department_id"] ? "selected" : ""?> value="<?=$dep_id?>"><?=$dep_name?></option>
                                            <?
                                        }
                                    }
                                    ?>
                                        <option <?=43==$sw["department_id"] ? "selected" : ""?> value="43">������� ������������� � ��������������� ��������������� ����������</option>
                                        <option <?=108==$sw["department_id"] ? "selected" : ""?> value="108">������� ��������� � ���������� ��������� ������������</option>
                                </select> 
                            </div>

                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1" style="border-bottom: 0px!important; min-width: 150px;">��� ��������</span>
                                <?
                                if(!empty($sw["student_id"]) && !empty($group["students"][$sw["student_id"]])) 
                                {
                                    ?>
                                    <select class="form-control sw_student_fio" name="sw_student_fio" style="border-bottom: 0px!important;" required>
                                        <?
                                        foreach($group["students"] as $stud_id => $stud_name) 
                                        {
                                            ?>
                                            <option <?=$stud_id==$sw["student_id"] ? "selected" : ""?> value="<?=$stud_id?>"><?=$stud_name?></option>
                                            <?
                                        }
                                        ?>
                                    </select>
                                    <?
                                } 
                                else 
                                {
                                    ?>
                                    <input type="text" class="form-control student_print" size="7" value="<?=$sw["student_name"]?>" title="������� ���" name="sw_student_name" placeholder="������� ���" style="width: 100%; " />
                                    <?
                                }
                                ?>
                            </div>
                        </div>

                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1" style="border-bottom: 0px!important; min-width: 150px;">��� ������</span>
                            <select class="form-control sw_type" style="border-bottom: 0px!important;" name="sw_type" required>
                                <option <?=$sw["work_type"]==32 ? "selected" : ""?> value="32">��������� ���������������� ������</option>
                                <!-- <option <?=$sw["work_type"]==33 ? "selected" : ""?> value="33">��������� ������</option> -->
                                <option <?=$sw["work_type"]==34 ? "selected" : ""?> value="34">�������� ������</option>
                                <option <?=$sw["work_type"]==35 ? "selected" : ""?> value="35">�������� ������</option>
                                <option <?=$sw["work_type"]==36 ? "selected" : ""?> value="36">�������</option>
                                <option <?=$sw["work_type"]==37 ? "selected" : ""?> value="37">����� � ��������</option>
                        <!--                     <option <?=$sw["work_type"]==38 ? "selected" : ""?> value="38">�������� �� �������� ������</option>
                                <option <?=$sw["work_type"]==39 ? "selected" : ""?> value="39">�������� �� �������� ������</option> -->
                                <option <?=$sw["work_type"]==41 ? "selected" : ""?> value="41">������� ������, ����������</option>
                            </select>
                        </div>

                        <div class="sw_subj_container <?=empty($sw["subject_name"]) ? "hidden" : ""?>">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1" style="min-width: 150px;">����������</span>
                                <select size="1" id="esubject_list" class="form-control editable-select" name="sw_subj" style="margin-top: 0px;" required>
                                        <option selected value="<?=$sw["subject"]?>"><?=$sw["subject_name"]?></option>
                                </select>
                   
                            
                            <input class="form-control" type="text" id="esubject_search" size="10" title="������� �����" placeholder="������� ����� ��������" style="display: none;" /> 
                            </div>
                            <a href="javascript: void(0)" onclick="jQuery('#esubject_search').toggle(); jQuery('#esubject_list').toggle();" title="����� �� ���������� ��������" class="btn btn-nsau-nsau btn-xs">������</a>
                        </div>

                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1" style="border-bottom: 0px!important; min-width: 150px;">�������� ������</span>
                            <input type="text" class="form-control sw_name" name="sw_name" style="border-bottom: 0px!important;" value='<?=$sw["work_name"]?>' required>
                        </div>

                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1" style="min-width: 150px; font-size: 14px; padding: 2px 7px 1px 8px;" >������� ������������/<br>�������������
                            </span>
                            <select name="sw_teacher" size="1" class="form-control editable-select teacher_list" style="margin-top: 0px;display: <?=!empty($sw["teacher_id"]) ? "block" : "none"?>">
                                <option value="<?=$sw["teacher_id"]?>"><?=$sw["teacher_name"]?></option>
                            </select>

                            <input type="text" class="form-control teacher_search" size="7" title="������� �����" placeholder="���" style="display: none; z-index: 9999;" />

                            <input type="text" class="form-control teacher_print" size="7" value="<?=$sw["sw_teacher_name"]?>" title="������� ���" name="sw_teacher_name" placeholder="������� ���" style="display: <?=$sw["sw_teacher_name"] ? "block" : "none"?>; width: 100%; " />
                        </div>
                            

                        <a href="javascript: void(0)" class="btn btn-nsau-nsau btn-xs cl_teacher_search" title="����� �� ���">������</a>
                        <a href="javascript: void(0)" class="btn btn-nsau-nsau btn-xs cl_teacher_print"  title="����� �� ���">���� ��� � ����</a>
                    </div>
                    <div class="col-sm-6">        
                        <p style="cursor: pointer" class="add_file hidden">�������� ����</p>
                        <div class="alert alert-info" style="margin-bottom: 3px;">���������� ���������� ������: .pdf</div>

                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1" style="border-bottom: 0px!important; min-width: 200px;">���� ������</span>
                            <input type="file" name="main_file" class="form-control file_form" id="file_form_file" accept="application/pdf" style="border-bottom: 0px!important; ">            
                        </div>

                        <?

                        if ($Auth->usergroup_id != 3) 
                        {
                            ?>
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1" style="min-width: 200px;">���� ������/��������</span>
                                <input type="file" name="feedback_file" class="form-control file_form" id="file_form_feedback" accept="application/pdf">
                            </div>
                            <? 
                        }
                        ?>
                        <br>

                        <?foreach($MODULE_OUTPUT["files"] as $file_id=>$file)
                        {
                            if ($file['report']==0) 
                            {
                                ?>
                                <div class="alert alert-nsau-grey"> 
                                    <div class="file_form_row">
                                        <span id="del_file_work" class="descr" data-id="<?=$file_id?>">���� ������: <?=$file["name"]?></span>
                                        <?php/**&nbsp <span class="btn btn-nsau-grey btn-sm del_loaded_file"  style="cursor: pointer" data-id="<?=$file_id?>">x</span>*/?>
                                    </div>
                                </div>
                                <?
                            }
                            else
                            {
                                ?>
                                <div class="alert alert-nsau-grey"> 
                                    <div class="file_form_row">
                                        <span class="descr">���� ������: <?=$file["name"]?></span>
                                        <?
                                        if ($Auth->usergroup_id != 3) 
                                        {
                                        ?>
                                        &nbsp <span id="del_file_feedback" class="btn btn-nsau-grey btn-sm del_loaded_file"  style="cursor: pointer" data-id="<?=$file_id?>">x</span>
                                        <?
                                        }
                                    ?>
                                    </div>
                                </div>
                                <?   
                            }
                        }?>
                        <input type="submit"  class="check_edit_form" value="���������">
                        <input type="submit" data-action="close" value="������">
                    </div>

                </div>

            </form>
        </div>
    </div>

    <?}?>
    <?break;






    case "sw_add_form": {?>
    <?header("Content-type: text/html; charset=windows-1251");?>
       
        <div class="panel panel-nsau" style="margin-bottom: 0px; width: 900px;"> 
            <div class="panel-heading">�������� ������</div> 
            <div class="panel-body">
                <form action="/office/student_work/" method="POST" enctype="multipart/form-data">
                      <div class="row"> 
                    <div class="col-sm-6">
                    <div class="close" style="color: #ffffff"><strong>X</strong></div>
                        <p class="red err_msg hidden"></p>

                        <div class="input-group"> 
                            <span class="input-group-addon" id="basic-addon1" style="border-bottom: 0px; min-width: 150px;">��� ��������� ������</span>
                            <input type="text" style="border-bottom: 0px!important;" class="form-control sw_year_end date_year" name="sw_year_end" size=18 required><span class="red date_error"></span>
                        </div>

                        <div class="input-group"> 
                            <span class="input-group-addon" id="basic-addon1" style="border-bottom: 0px!important; min-width: 150px;">����� ������</span>
                            <input type="text" class="form-control sw_group" style="border-bottom: 0px!important;" required>
                        </div>


                        <div class="sw_search_group_content"></div>
                        <div class="sw_select_group_content"></div>


                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1" style="border-bottom: 0px!important; min-width: 150px;">��� ������</span>
                            <select class="form-control sw_type" name="sw_type" style="border-bottom: 0px!important;" required>
                                <option value="">�������</option>
                                <option value="32">��������� ���������������� ������</option>
                                <!-- <option value="33">��������� ������</option> -->
                                <option value="34">�������� ������</option>
                                <option value="35">�������� ������</option>
                                <option value="36">�������</option>
                                <option value="37">����� � ��������</option>
<!--                                 <option value="38">�������� �� �������� ������</option>
                                <option value="39">�������� �� �������� ������</option> -->
                                <option value="41">������� ������, ����������</option>
                            </select>
                        </div>



                    
                    <div class="sw_subj_container <?=empty($sw["subject_name"]) ? "hidden" : ""?>">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1" style="border-bottom: 0px!important; min-width: 150px;">����������</span>
                            <select size="1" id="esubject_list" class="form-control editable-select" name="sw_subj" style="margin-top: 0px; border-bottom: 0px!important;">
                                    <option>---</option>
                                    <!-- <option selected value="<?=$sw["subject"]?>"><?=$sw["subject_name"]?></option> -->
                            </select>
                            <input type="text" class="form-control" id="esubject_search" size="10" title="������� �����" placeholder="������� ����� ��������" style="display: none;" />
                        </div>
                        
           <!--               <a href="javascript: void(0)" onclick="jQuery('#esubject_search').toggle(); jQuery('#esubject_list').toggle();" title="����� �� ���������� ��������" class="btn btn-nsau-nsau btn-xs">������</a> -->
                    </div>




                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1" style="border-bottom: 0px!important; min-width: 150px;">�������� ������</span>
                            <input type="text" class="form-control sw_name" name="sw_name" style="border-bottom: 0px!important;" required>
                        </div>

                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1" style="min-width: 150px;">������� ������������/�������������
                            </span>
                            <select name="sw_teacher" size="1" class="form-control editable-select teacher_list" style="display: block; margin-top: 0px;" required>
                                <option value=""></option>
                            </select>
                            <input type="text" class="form-control teacher_search" size="7" title="������� �����" placeholder="���" style="display: none; z-index: 9999;">
                            <input type="text" class="form-control teacher_print" size="7"  title="������� ���" name="sw_teacher_name" placeholder="������� ���" style="display: none;">
                        </div>


                        <a href="javascript: void(0)" class="btn btn-nsau-nsau btn-xs cl_teacher_search" title="����� �� ���">������</a>
                        <a href="javascript: void(0)" class="btn btn-nsau-nsau btn-xs cl_teacher_print"  title="����� �� ���">���� ��� � ����</a>

    </div>

                    <div class="col-sm-6">
                        <p style="cursor: pointer" class="add_file hidden">�������� ����</p>
                        <div class="alert alert-info" style="margin-bottom: 3px;">���������� ���������� ������: .pdf</div>

                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1" style="border-bottom: 0px!important; min-width: 200px;">���� ������</span>
                            <input type="file" name="main_file" class="form-control file_form" id="file_form_file" accept="application/pdf" style="border-bottom: 0px!important; " required>
                        </div>
                        <div class="files_form"></div>
                        

                        <?
                        
                        if ($Auth->usergroup_id != 3) 
                        {
                            ?>
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1" style="min-width: 200px;">���� ������/��������</span>
                                <input type="file" name="feedback_file" class="form-control file_form" id="file_form_feedback" accept="application/pdf">
                            </div>
                            <? 
                        }
                        ?>

                        <input type="submit" class="check_form" value="��������">
                        <input type="submit" data-action="close" value="������">
                   </div>
                </form>
            </div> 
        </div>


    <?}
    break;


    case "student_work": 
    {
        ?>
        <br>
        <br>
        <style type="text/css">
        table .btn 
        {
            height: 35px!important;
            padding-top: 9px;
        }

        .table > tbody > tr > td {
             vertical-align: middle;
        }
        </style>
        <a href="#" class="btn btn-nsau btn-md add_student_work"> <span class="glyphicon glyphicon-file"></span> �������� ������</a>


        <div class="row"> 
            <div class="col-sm-12">   
                <?
                foreach($MODULE_OUTPUT["sw"] as $wtype => $works) 
                {
                    ?>
                    <legend style="margin-top: 4px; padding-bottom: 5px; margin-bottom: 0px;" align="center"><?=$MODULE_OUTPUT["nfv"][$wtype]?></legend>
                    <table class="table table-striped">
                        <col width="1%">
                        <col width="30%">
                        <col width="30%">
                        <col width="10%">
                        <col width="10%">



                        <thead>
                            <tr class="active"><th></th><th >��������</th><th>�����</th><th>��������/�����</th><th ></th></tr>
                        </thead>
                        <tbody>
                            <?
                            foreach($works as $wid => $work) 
                            {
                                ?>
                                <tr id="<?=$wid?>">
                                    <td>
                                        <?
                                        if ($work['user_id']==$Auth->user_id) 
                                        {
                                            ?><span class="glyphicon glyphicon-user" style="color: green" title="������ ��������� ��"></span><?
                                        }
                                        else
                                        {

                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <?=$work["work_name"]?>   
                                    </td> 
                                    <td>
                                        <?
                                        if(!empty($work["student_people_id"]))
                                        {
                                            ?><a href="/people/<?=$work["student_people_id"]?>"><?=$work["student"]?></a><?
                                        } 
                                        else 
                                        {
                                            ?>
                                            <?=$work["student"]?>
                                            <?
                                        }
                                        if (isset($work["group_name"])) 
                                        {
                                            echo " (".$work["group_name"].")";
                                        }
                                        ?> 
                                        <br> 
                                        <?
                                        if(!empty($work["files"]))
                                        {
                                            foreach($work["files"] as $fid=>$fname)
                                            {
                                                if($fid!=$work['feedback_id'])
                                                {?>
                                                <a href="/file/<?=$fid?>"><?=$fname?></a>
                                                <?}
                                            }
                                        }
                                        else
                                        {
                                            ?>
                                            ���� �����������
                                            <?
                                        }
                                        ?>


                                    </td>
                                    <td>
                                        <?
                                        if (isset($work['feedback_id'])) 
                                        {
                                            ?>
                                            <!-- <span class="glyphicon glyphicon-ok" style="color: green;">   -->
                                            <a href="/file/<?=$work['feedback_id']?>" class="btn btn-default" style="padding-top: 8px!important;"><span class="glyphicon glyphicon-save-file" style="color: green" title="������� ��������"></span>
                                            <!-- <span class="glyphicon glyphicon-floppy-save"></span> -->
                                        </a>
                                            </span>
                                            <?
                                        }
                                        else
                                        {
                                            ?>
                                            <a href="#" data-id="<?=$wid?>" class="btn btn-default edit"><span class="glyphicon glyphicon-remove" style="color: #C00C21;" title="�������� �����������"></span></a>
                                            <?
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <a href="#" data-id="<?=$wid?>" class="btn btn-default edit"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                                        <a href="#" data-id="<?=$wid?>" class="btn btn-default delete"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                                    </td>
                                </tr>
                                <?
                            }
                            ?>
                        </tbody>
                    </table>
                    <?
                }
                ?>       
            </div>
        </div>

        

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="//ajax.aspnetcdn.com/ajax/jquery.ui/1.10.3/jquery-ui.min.js"></script>
        <link rel="stylesheet" href="/themes/styles/jquery-ui.css">


        <!-- <div class="overlay hidden"> </div> -->
        <div class="sw_add_form hidden" id="draggable" style="cursor: pointer;"></div>
        <div class="sw_edit_form hidden" id="draggable2" style="cursor: pointer;"></div>

        <script type="text/javascript">
        $(function() {
            
            $('#draggable').draggable();
            $('#draggable2').draggable();
        });
        </script>

        <div class="file_form_row tmp hidden">
            <span class="descr"></span>
            &nbsp <span class="del_file" style="cursor: pointer;">x</span>
        </div>
    <?}?>
    <?break;
    case "students_upload": {?>
        <h1>�������� ������ ���������</h1>
        <form action="/office/zagruzka-studentov/" method="POST" enctype="multipart/form-data">
            <input type="file" name="students">
            <input value="���������" type="submit">
        </form> <br>

        <h1>�������� ������������ ���������</h1>
        <form name="upload_progress" action="" method="post"  enctype="multipart/form-data">
            <input type="file" name="progress_file" accept="document/csv">
            <input name="upload_progress" value="���������" type="submit">
        </form>
        <div>
                <?if(!empty($MODULE_OUTPUT["no_people"])) {?>
                    ��� ������ ��������� �� ������� ������� ������.
                    <table class="nsau_table">
                        <thead>
                            <tr>
                                <td>���</td>
                                <td>����</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?foreach($MODULE_OUTPUT["no_people"] as $code => $people) {?>
                                <tr>
                                    <td><?=$people?></td>
                                    <td><?=$code?></td>
                                </tr>
                            <?}?>
                        </tbody>
                    </table>
                <?}?>
                
        <?
        echo "<p class=\"message red\">".$MODULE_OUTPUT['error']."</p>\n";
        echo "<p class=\"message\">".$MODULE_OUTPUT['ins']."</p>\n";
        echo "<p class=\"message\">".$MODULE_OUTPUT['del']."</p>\n";
        ?>
        </div>

        <?if(!empty($MODULE_OUTPUT["new_students"])) {?>
            <h2>��������� ��������� ��������:</h2>
            <table class="nsau_table" width = "70%">
                <thead>
                    <tr>
                        <th>������</th>
                        <th>���</th>
                        <th>�����</th>
                        <th>������</th>
                    </tr>
                </thead>
                <tbody>
                    <?foreach($MODULE_OUTPUT["new_students"] as $form => $students) {?>
                        <tr><td colspan="4"><?=($form==1) ? "�����" : (($form==2) ? "�������" : "����-�������")?> ����� (<a href="<?=$MODULE_OUTPUT["download_files"][$form]?>">�������</a>)</td></tr>
                        <?foreach($students as $i => $student) {?>
                            <tr>
                                <td><?=$student["group"]?></td>
                                <td><?=$student["fio"]?></td>
                                <td><?=$student["login"]?></td>
                                <td><?=$student["passwd"]?></td>
                            </tr>
                        <?}?>
                    <?}?>
                </tbody>
            </table>
        <?}?>
        <?if(!empty($MODULE_OUTPUT["updated_students"])) {?>
            <h2>��������� ��������� ��������:</h2>
            <table class="nsau_table" width = "70%">
                <thead>
                    <tr>
                        <th rowspan="3">����</th>
                    </tr>
                    <tr>
                        <th colspan="2">��</th>
                        <th colspan="2">�����</th>
                    </tr>
                    <tr>
                        <th>������</th>
                        <th>���</th>
                        <th>������</th>
                        <th>���</th>
                    </tr>
                </thead>
                <tbody>
                    <?foreach($MODULE_OUTPUT["updated_students"] as $form => $students) {?>
                        <tr><td colspan="5"><?=($form==1) ? "�����" : (($form==2) ? "�������" : "����-�������")?> �����</td></tr>
                        <?foreach($students as $code => $student) {?>
                            <tr>
                                <td><?=$code?></td>
                                <td><?=$student["before"]["group"]?></td>
                                <td><?=$student["before"]["fio"]?></td>
                                <td><?=$student["after"]["group"]?></td>
                                <td><?=$student["after"]["fio"]?></td>
                            </tr>
                        <?}?>
                    <?}?>
                </tbody>
            </table>
        <?}?>
        <?if(!empty($MODULE_OUTPUT["deleted_students"])) {?>
            <h2>������� ��������� ��������:</h2>
            <table class="nsau_table" width = "70%">
                <thead>
                    <tr>
                        <th>������</th>
                        <th>����</th>
                        <th>���</th>
                    </tr>
                </thead>
                <tbody>
                    <?foreach($MODULE_OUTPUT["deleted_students"] as $form => $students) {?>
                        <tr><td colspan="3"><?=($form==1) ? "�����" : (($form==2) ? "�������" : "����-�������")?> �����</td></tr>
                        <?foreach($students as $i => $student) {?>
                            <tr>
                                <td><?=$student["group"]?></td>
                                <td><?=$student["code"]?></td>
                                <td><?=$student["fio"]?></td>
                            </tr>
                        <?}?>
                    <?}?>
                </tbody>
            </table>
        <?}?>
<?}
    break;


}

?>

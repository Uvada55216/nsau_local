<?
header("Content-type: text/html; charset=windows-1251");

switch ($MODULE_OUTPUT["mode"])
{
	case "admin_ui":
		{
			//-------------------------------------students_progress tool--------------------------------------------------------
			?>
			<link rel="stylesheet" href="https://static.zinoui.com/1.5/themes/silver/zino.core.css">
	        <link rel="stylesheet" href="https://static.zinoui.com/1.5/themes/silver/zino.datatable.css">
	        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	        <script src="https://static.zinoui.com/1.5/compiled/zino.datasource.min.js"></script>
	        <script src="https://static.zinoui.com/1.5/compiled/zino.datatable.min.js"></script>
	        <link rel="stylesheet" href="https://static.zinoui.com/1.5/themes/silver/zino.button.css">
	        <link rel="stylesheet" href="https://static.zinoui.com/1.5/themes/silver/zino.resizable.css">
	        <link rel="stylesheet" href="https://static.zinoui.com/1.5/themes/silver/zino.overlay.css">
	        <script src="https://static.zinoui.com/1.5/compiled/zino.position.min.js"></script>
	        <script src="https://static.zinoui.com/1.5/compiled/zino.draggable.min.js"></script>
	        <script src="https://static.zinoui.com/1.5/compiled/zino.resizable.min.js"></script>
	        <script src="https://static.zinoui.com/1.5/compiled/zino.button.min.js"></script>
	        <script src="https://static.zinoui.com/1.5/compiled/zino.overlay.min.js"></script>
	        <script src="https://static.zinoui.com/js/front.min.js"></script>

	        <div id="st_progress" title="students_progress toool v. 1.0" style="display: none">

				<?
					if (isset($MODULE_OUTPUT["data"])) 
					{
						?>
							<script type="text/javascript">
					        
					        	window.onload=function(){
		 							$("#st_progress").zinoOverlay("open");
								}
							</script>
						<?
					}
				?>

				<fieldset>
					<legend>students_progress tool</legend>
						<form method="POST">
							�������:
							<input type="text" name="semester" value="<?=(isset($_POST['semester'])?$_POST['semester']:'')?>">
							�������� id:
							<input type="text" name="id_min" value="<?=(isset($_POST['id_min'])?$_POST['id_min']:'')?>">:
							<input type="text" name="id_max" value="<?=(isset($_POST['id_max'])?$_POST['id_max']:'')?>">
							<input type="checkbox" name="show_dubl" value="<?=(isset($_POST['show_dubl'])?$_POST['show_dubl']:'')?>">���������� ���������
							<input type="submit" name="diapazon_select" value="�����">
							<br><br>
							<input type="submit" name="dublicate_delete" value="������� ���������"> [���� �������� �� ������, ������� ��������� �� ����� ��������]<br><br>
							<input type="submit" name="diapazon_delete" value="������� ��������� ������"> [���� �������� �� ������, ������� ���� �������!] <br><br>
							<input type="submit" name="null_delete" value="������� 0 �������"> [������� ������ � 0 ���������]
						</form>
				</fieldset>
				<div id="datatable"></div>
				

				<script type="text/javascript">
					$(function () {
					    var ds = zino.DataSource({
					        fields: ["id", "subject", "code", "hours", "type", "value"],
					        dataType: "array",
					        data: [
					        		<?php
								    foreach ($MODULE_OUTPUT["data"] as $one => $two)
									{
										echo "
										{
											id: ".$one.", 
											subject: '".$two['subject']."',
											code: '".$two['code']."',
											hours: '".$two['hours']."',
											type: '".$two['type']."',
											value: '".$two['value']."',
										}
										,";
									}
					            	?>
							]
					    });
					    $("#datatable").zinoDatatable({
					        dataSource: ds,
					        summary: "",
					        caption: <?echo "\"�������� �������: [".count($MODULE_OUTPUT["data"])."]. ����������: [".$MODULE_OUTPUT["dublicate"]."]\"";?>
					    });
					});
				</script>  
			</div>

			<input type="button" id="button_1" value="����� ������������ ���������" />
			


			<script type="text/javascript">
				$(function () {
				    $("#st_progress").zinoOverlay({
				        width: 1000,
				        height: 700,
				        draggable: true,
				        resizable: true,
				        autoOpen: false,
				        buttons: {
				            "Close": function (button) {
				                $(this).zinoOverlay("close");
				            }
				        }
					});

			    $("#button_1").click(function () {
			        $("#st_progress").zinoOverlay("open");
			    });


			    $(":input[type='button']").zinoButton();
				});
			</script>
			<?
			//-------------------------------------------------------------------------------------------------------------------


			//---------------------------------------priveleges for curator------------------------------------------------------

			?>
<a href=""></a>
			<div id="curators" title="Curators table" style="display: none">
				<div id="datatable2"></div>
				<script type="text/javascript">
				$(function () {
				    var ds = zino.DataSource({
				        fields: ["Group", "Id_group", "Curator", "People_id", "Auth_id", "Teachers_id"],
				        dataType: "array",
				        data: [
	            			<?php
						    foreach ($MODULE_OUTPUT["curators"] as $one => $two)
							{
								echo "
								{
									Group: '".$two['group_name']."', 
									Id_group: '".$two['group_id']."',
									Curator: '".$two['curator']."',
									People_id: '".$two['people']."',
									Auth_id: '".$two['auth']."',
									Teachers_id: '".$two['teachers']."'
								}
								,";
							}
			            	?>
						]
				    });
				    $("#datatable2").zinoDatatable({
				        dataSource: ds,
				    });
				});
				</script> 
			</div>

			<input type="button" id="button_2" value="������ ��������� �����" />

			<script type="text/javascript">
			$(function () {
			    $("#curators").zinoOverlay({
			        width: 520,
			        height: 650,
			        draggable: true,
			        resizable: true,
			        autoOpen: false,
			        buttons: {
			            "�������": function (button) {
			                $(this).zinoOverlay("close");
			            }
			        }
				});
			    $("#button_2").click(function () {
			        $("#curators").zinoOverlay("open");
			    });
			    $(":input[type='button']").zinoButton();
			});
			</script>   
			<?
			//-------------------------------------------------------------------------------------------------------------------
		}
	break;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">   
<!--[if lt IE 7 ]><html class="no-js ie6" xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru"><![endif]-->
<!--[if IE 7 ]><html class="no-js ie7" xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru"><![endif]-->
<!--[if IE 8 ]><html class="no-js ie8" xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru"><![endif]-->
<!--[if IE 9 ]><html class="no-js ie9" xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru"><!--<![endif]-->
<head>
	<title><?php include $EE["theme"] . "_page_title" . TEMPLATE_EXT ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=<?=CODEPAGE_HTML?>" />
    <meta http-equiv="Content-Language" content="<?=$EE["language"]?>" />
    <meta name="description" content="<?=$EE["meta_descr"]?>" />
    <meta name="keywords" content="<?=$EE["meta_keywords"]?>" />
    <meta name="author" content="Andrey Kudryashov (poganini): mail[sobachka]poganini.ru" />
    <meta http-equiv="imagetoolbar" content="no" />
	
    <link rel="icon" href="<?=$EE["http_theme"]?>favicon2011.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="<?=$EE["http_theme"]?>favicon2011.ico" type="image/x-icon" />
	
    <script type="text/javascript" src="<?=HTTP_COMMON_SCRIPTS?>modernizr-2.0.6.min.js"></script>
    <link rel="stylesheet" href="<?=$EE["http_styles"]?>landscape_screen.css" type="text/css" />
    <!--[if lte IE 7]><link rel="stylesheet" href="<?=$EE["http_styles"]?>landscape_ie-fix.css" type="text/css" /><![endif]-->
    <?php include $EE["theme"] . "_head_extra" . TEMPLATE_EXT ?>    
</head>

<!--[if lt IE 7 ]><body class="no-js ie6"><![endif]-->
<!--[if IE 7 ]><body class="no-js ie7"><![endif]-->
<!--[if IE 8 ]><body class="no-js ie8"><![endif]-->
<!--[if IE 9 ]><body class="no-js ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><body class="no-js"><!--<![endif]-->
<!--[if lte IE 6]><div id="no-ie6">
	<div id="no-ie6-img"></div>
	<p>�� ����������� ���������� ������� Internet Explorer 6. ��������� ������� <a href="http://www.microsoft.com/rus/windows/internet-explorer/">����������</a>.</p>
	<a id="no-ie6_close" href="#" title="�������">&times;</a>
</div><![endif]-->
	<div id="nofooter">
		<div id="nofooter_bg">
			<div id="left_bg">
				<div id="left_top_bg"></div>
				<div id="left_cn_bg">
					<div></div>
				</div>
				<div id="left_bottom_bg"><div></div></div>
			</div>
			<div id="right_bg">
				<div id="right_top_bg"></div>
				<div id="right_cn_bg">
					<div></div>
				</div>
				<div id="right_bottom_bg"></div>
			</div>
		</div>
		<div class="thousand-wrapper">
		    <div id="header">
				<?php
				if($EE['folder_id'] == 697) {
				?>
				<div id="logo_text">
					<span>
						<span></span>
						����������� �����
					</span>
				</div>
				<div id="logo">
					<span></span>
				</div>
				<?php
				} else {
				?>
				<div id="logo_text">
					<a href="<?=$EE['footsteps'][0]['uri']?>">
						<span></span>
						����������� �����
					</a>
				</div>
				<div id="logo">
					<a href="<?=$EE['footsteps'][0]['uri']?>"></a>
				</div>
				<?php
				}
				?>
				<div id="header_menu">
					<div>
						<?php
						$NODEGROUP = "topmenu";
                        if (!empty($EE["modules_data"][$NODEGROUP]))
                            include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT;    
                        ?>
					</div>
				</div>	
				<div id="header_sub_menu">
					<div>
						<?php
						$NODEGROUP = "submenu";
                        if (!empty($EE["modules_data"][$NODEGROUP]))
                            include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT;    
                        ?>
					</div>
				</div>
				<p>����� ���������� �� ���� ������������ ������ ����!</p>
				<div id="header_bg1"></div>
				<div id="header_bg2"></div>
				<div id="contact">
					<div id="contact_top_bg"></div>
					<div id="contact_cn_bg">
						<div id="contact_cont">
							<h1>���� ��������:</h1>
							<p>
								630090,&nbsp;�.&nbsp;�����������,<br />
								��.&nbsp;�����������,&nbsp;160<br />
								��.&nbsp;��-10<br />
								���:&nbsp;(383)&nbsp;264-20-85<br />
								e-mail:<a href="mailto:lcngau2015@yandex.ru">lcngau2015@yandex.ru</a>
							</p><br />
							<a id="shem" href="/landscape/contacts/">�����&nbsp;�������</a>
						</div>
						<div class="clear"></div>
					</div>
					<div id="contact_bottom_bg"></div>
				</div>
		    </div><!--header-->
			<div id="content">
				<div id="left_menu">
					<div id="top_shadow"></div>
					<div id="cn_shadow">
						<div id="left_menu_top_bg"></div>
						<div id="left_menu_bottom_bg"></div>
						<div id="ugolok"></div>
						<div id="left_menu_cont">
							<h1>������:</h1>
							<?php
							$NODEGROUP = "left_top";
							if (!empty($EE["modules_data"][$NODEGROUP]))
							    include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT;    
							?>
						</div>
					</div>
					<div id="bottom_shadow"></div>
				</div>
				<div class="content">
					<h1>
						/
						<?php 
						if(isset($EE['footsteps'][1])) {
							echo $EE['footsteps'][1]['title'];
						} else {
							echo $EE['footsteps'][0]['title'];
						}
						?>
						/
						
					</h1>
					<?php
					$NODEGROUP = "content";
					if (!empty($EE["modules_data"][$NODEGROUP]))
						include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT;    
					?>
				</div>
				<div class="clear"></div>
		    </div><!--content-->
			<div class="clear"></div>
		</div>
		<div id="footer-pusher"></div>
	</div>
    <div id="footer">        
        <div class="thousand-wrapper">
			<div id="footer_cont">
				<!-- Yandex.Metrika counter -->
				<script type="text/javascript">
				(function (d, w, c) {
				    (w[c] = w[c] || []).push(function() {
					try {
					    w.yaCounter7937029 = new Ya.Metrika({id:7937029,
						    webvisor:true,
						    clickmap:true,
						    trackLinks:true,
						    accurateTrackBounce:true});
					} catch(e) { }
				    });
				
				    var n = d.getElementsByTagName("script")[0],
					s = d.createElement("script"),
					f = function () { n.parentNode.insertBefore(s, n); };
				    s.type = "text/javascript";
				    s.async = true;
				    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";
				
				    if (w.opera == "[object Opera]") {
					d.addEventListener("DOMContentLoaded", f, false);
				    } else { f(); }
				})(document, window, "yandex_metrika_callbacks");
				</script>
				<noscript><div><img src="//mc.yandex.ru/watch/7937029" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
				<!-- /Yandex.Metrika counter -->
            
				<div id="copyright">&copy; 2012&ndash;2013 <a href="http://nsau.edu.ru/">������������� ��������������� �������� �����������</a>
				<br />
					<a href="/vds/" itemprop="copy" >������ ��� ������������</a>
				</div>
			</div>
		</div> 
    </div>    
</body>
</html>

<!DOCTYPE html>
<html lang="ru">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<title><?php include $EE["theme"] . "_page_title" . TEMPLATE_EXT ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=<?=CODEPAGE_HTML?>" />
    <meta name="description" content="<?=$EE["meta_descr"]?>" />
    <meta name="keywords" content="<?=$EE["meta_keywords"]?>" />
    <meta http-equiv="imagetoolbar" content="no" />
    <link rel="icon" href="<?=$EE["http_theme"]?>favicon2011.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="<?=$EE["http_theme"]?>favicon2011.ico" type="image/x-icon" />
	<meta name='yandex-verification' content='771e5652e16b9536' />
	<?php /* head_extra */
	    clearstatcache();
        include $EE["theme"] . "_head_extra" . TEMPLATE_EXT;
    ?>
</head>
<body>
<div class="content-wrapper">
	<header>
		<div class="header-first">
			<div class="container">
				<div class="header-first__wrap">
					<div class="header-first__left">
						<div class="header-first__language">
							<a href="/en/" class="header-first__social-item" title="">En</a>
						</div>
						<div class="header-first__social">
							<a href="https://vk.com/nsau_press" class="header-first__social-item" title="����������� �������� Vkontakte" target="_blank">
								<svg xmlns="http://www.w3.org/2000/svg" style="margin-bottom: 2px" width="18" height="18" viewBox="0 0 24 24">
									<path fill="white" d="M13.162 18.994c.609 0 .858-.406.851-.915-.031-1.917.714-2.949 2.059-1.604 1.488 1.488 1.796 2.519 3.603 2.519h3.2c.808 0 1.126-.26 1.126-.668 0-.863-1.421-2.386-2.625-3.504-1.686-1.565-1.765-1.602-.313-3.486 1.801-2.339 4.157-5.336 2.073-5.336h-3.981c-.772 0-.828.435-1.103 1.083-.995 2.347-2.886 5.387-3.604 4.922-.751-.485-.407-2.406-.35-5.261.015-.754.011-1.271-1.141-1.539-.629-.145-1.241-.205-1.809-.205-2.273 0-3.841.953-2.95 1.119 1.571.293 1.42 3.692 1.054 5.16-.638 2.556-3.036-2.024-4.035-4.305-.241-.548-.315-.974-1.175-.974h-3.255c-.492 0-.787.16-.787.516 0 .602 2.96 6.72 5.786 9.77 2.756 2.975 5.48 2.708 7.376 2.708z" /></svg>
							</a>
							<a href="https://t.me/ngaulife" class="header-first__social-item" title="����������� �������� Telegram" target="_blank">
								<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 32 32">
									<path fill="white" d="M29.919 6.163l-4.225 19.925c-0.319 1.406-1.15 1.756-2.331 1.094l-6.438-4.744-3.106 2.988c-0.344 0.344-0.631 0.631-1.294 0.631l0.463-6.556 11.931-10.781c0.519-0.462-0.113-0.719-0.806-0.256l-14.75 9.288-6.35-1.988c-1.381-0.431-1.406-1.381 0.288-2.044l24.837-9.569c1.15-0.431 2.156 0.256 1.781 2.013z" />
								</svg>
							</a>
							<a href="https://rutube.ru/channel/24117186/" class="header-first__social-item" title="����������� �������� Rutube" target="_blank">
								<svg width="21" height="21" viewBox="0 0 48 48" fill="white" style="margin-bottom: 1px" xmlns="http://www.w3.org/2000/svg">
									<path fill-rule="evenodd" clip-rule="evenodd" d="M14 12C14 10.8954 14.8954 10 16 10L26 10C30.4183 10 34 13.5817 34 18C34 21.3747 31.9104 24.2614 28.9544 25.4368L33.7889 35.1056C34.2828 36.0935 33.8824 37.2949 32.8944 37.7889C31.9065 38.2828 30.7051 37.8824 30.2111 36.8944L24.7639 26H18L18 36C18 37.1046 17.1046 38 16 38C14.8954 38 14 37.1046 14 36L14 12ZM18 22L26 22C28.2091 22 30 20.2091 30 18C30 15.7909 28.2091 14 26 14L18 14L18 22Z" fill="white" />
								</svg>
							</a>

						</div>
						<div class="header-first__mail">
							<div class="header-first__rector">
								<a href="/about/contacts/" class="" target="_blank">
									<i class="fa-solid fa-envelope"></i>
									<span>��������� �������</span>
								<a/>

								<div class="header-first__rector-list">

									<a href="/abitur/answers/" class="header-first__rector-item">
										����������
									</a>

									<a href="/student/answers/" class="header-first__rector-item">
										�������
									</a>

									<a href="/about/contacts/" class="header-first__rector-item">
										���������
									</a>
									
									<a href="/vacancies/" class="header-first__rector-item">
										��������
									</a>

								</div>
							</div>
							<div>
								<a href="/department/commission/" class="header-first__comission hide-320" target="_blank">�������� ��������</a>
							</div>
						</div>
					</div>
					<div class="header-first__right">
						<a href="/student/timetable/" class="header-first__timetable" target="_blank">
							<img src="/images/header-first/5.png" alt="" />
							<span class="hide-640">
                                    ���������� �������
                            </span>
						</a>
                        <?php
                         /* ����� �� ����� */
                        ?>
						<div class="header-first__search">
							<img src="/images/header-first/6.png" alt="����� �� �����" />
                            <form id="FormSearch" method="get" action="/search/" accept-charset="UTF-8">
                                <input type="search" name="search" class="header-first__search-field" placeholder="����� �� �����">
                            </form>
                        </div>

						<a href="/vds/" class="header-first__look" itemprop="copy" title="������ ����� ��� ������������">
							<img src="/images/header-first/7.png" alt="������ ����� ��� ������������" />
						</a>

						<?php /* login */
							$NODEGROUP = "login";
							if ($EE["modules_data"][$NODEGROUP]) include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT;
						?>

					</div>
				</div>
			</div>
		</div>
		<div class="header-second">
			<div class="container">
				<div class="header-second__wrap">
					<a href="/" class="header-second__logo">
						<img src="/images/header-second.png" alt="" />
					</a>
					<div class="header-second__menu">
						<?php
							$NODEGROUP = "topmenu";
							if ($EE["modules_data"][$NODEGROUP]) include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT;
						?>
					</div>
				</div>
			</div>
		</div>
	</header>
	<section id="cs-banners">
		<?php
			$NODEGROUP = "baners";
			if ($EE["modules_data"][$NODEGROUP]) include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT;
		?>
	</section>
    <?php
        $NODEGROUP = "content";
        if ($EE["modules_data"][$NODEGROUP]){
                    include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT;
        }else { ?>
            <div class="main">
            <div class="container">
                <div class="main__wrap">
                    <div class="main__content">

                        <?php
                            $NODEGROUP = "left_top";
                            if ($EE["modules_data"][$NODEGROUP]) include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT;
                        ?>

                        <?php
                            $NODEGROUP = "news_announce";
                            if ($EE["modules_data"][$NODEGROUP]) include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT;
                        ?>
                    </div>

                    <?php
                        $NODEGROUP = "right";
                        if ($EE["modules_data"][$NODEGROUP]) include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT;
                    ?>

                </div>
            </div>
        </div>
    <?php } ?>
    <section id="cs-partners">
        <?php
		    $NODEGROUP = "partners";
			if ($EE["modules_data"][$NODEGROUP]) include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT;
		?>

    </section>
    <section id="cs-contacts">
        <?php
		    $NODEGROUP = "contacts";
			if ($EE["modules_data"][$NODEGROUP]) include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT;
		?>
    </section>
	<footer>
        <?php
		    $NODEGROUP = "footer_block";
			if ($EE["modules_data"][$NODEGROUP]) include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT;
		?>
	</footer>
</div>
<div id="preloader">
	<div class="preloader"></div>
</div>
</body>
</html>
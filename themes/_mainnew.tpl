<!DOCTYPE html> 
<!--[if lt IE 7 ]><html class="no-js ie6" lang="ru"><![endif]-->
<!--[if IE 7 ]><html class="no-js ie7" lang="ru"><![endif]-->
<!--[if IE 8 ]><html class="no-js ie8" lang="ru"><![endif]-->
<!--[if IE 9 ]><html class="no-js ie9" lang="ru"><![endif]-->
<!--[if (gt IE 7)|!(IE)]><html lang="ru"><!--<![endif]-->
<head>
	<title><?php include $EE["theme"] . "_page_title" . TEMPLATE_EXT ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=<?=CODEPAGE_HTML?>" />
    <meta name="description" content="<?=$EE["meta_descr"]?>" />
    <meta name="keywords" content="<?=$EE["meta_keywords"]?>" />
      <meta http-equiv="imagetoolbar" content="no" />	 

    <?	$NODEGROUP = "head_tag";
	if ($EE["modules_data"][$NODEGROUP]) { ?>
		<?include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT; ?>
	<?}	?>

    <link rel="icon" href="<?=$EE["http_theme"]?>favicon2011.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="<?=$EE["http_theme"]?>favicon2011.ico" type="image/x-icon" />
		<meta name='yandex-verification' content='771e5652e16b9536' />
    <script type="text/javascript" src="<?=HTTP_COMMON_SCRIPTS?>modernizr-2.0.6.min.js"></script>
    	
	<link rel="stylesheet" href="<?=$EE["http_styles"]?>screen_2011.css?<?=filemtime($_SERVER['DOCUMENT_ROOT'].$EE['http_styles'].'screen_2011.css')?>" type="text/css" />
	<?php	clearstatcache(); ?>
    <link rel="stylesheet" href="<?=$EE["http_styles"]?>ee.css" type="text/css" />
	<!--[if lte IE 8]><link rel="stylesheet" href="<?=$EE["http_styles"]?>ie-fix.css" type="text/css" /><![endif]-->
    <?php include $EE["theme"] . "_head_extra" . TEMPLATE_EXT ?>
	
	<link rel="stylesheet" type="text/css" href="<?=$EE["http_styles"]?>contentrotator.css" media="screen" />
	<script type="text/javascript" src="<?=HTTP_COMMON_SCRIPTS?>jquery_min.js"></script>
	<script src="<?=HTTP_COMMON_SCRIPTS?>jquery-ui-personalized-1.5.3.packed.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$("#rotator > ul").tabs({fx:{opacity: "toggle"}}).tabs("rotate", 4000, true);
		});
	</script>
<!-- �����������, �������� � ���� ������ -->
  <style type="text/css">
   div.header_msg {
		width: 40%;
		text-align: center;
	    height: 200px;
	    position: absolute;
	    top: 40%;
	    left: 30%;
		z-index: 100;
	 }
  </style>

</head>
<body>
<!-- ��������, �������� �� ��� ������. ��� ������������� ��������� -->
<? 
if ( (stristr($_SERVER['HTTP_USER_AGENT'], 'MSIE 6.0') !=false) ||(stristr($_SERVER['HTTP_USER_AGENT'], 'MSIE 7.0') != false)) {?>
<div id="no-ie6">
	<div id="no-ie6-img"></div>
	<p>�� ����������� ���������� ������� Internet Explorer <!--[if IE 6]>6<![endif]--><!--[if IE 7]>7<![endif]-->.
	��������� ������� <a href="http://www.microsoft.com/rus/windows/internet-explorer/">����������</a>.</p>
	<a id="no-ie6_close" href="#" title="�������">&times;</a>
</div>
<?php
}
include $EE["theme"] . "_cross_auth" . TEMPLATE_EXT ?>
    <div id="nofooter">
		<div class="menu_background">
			<div class="office_menu">
			<?php 	//}
			$NODEGROUP = "login"; 
			 if ($EE["modules_data"][$NODEGROUP])
	            include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT;
			?>
			</div>
		</div>
		<div class="wrapper">
			<!--div id="page_title2"><div><span><a href="https://nsau.edu.ru/department/commission/">�������� ��������</a></span><br/>(383) 347-41-14</div></div-->
		    <div id="header" class="header">
<!-- ���� -->
				<a id="logo" class="header__logo" href="/">
					<span><span>&nbsp;</span></span>
				</a>
			<?php	$NODEGROUP = "daystogo";
				if ($EE["modules_data"][$NODEGROUP])
					include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT; 
			?>

 
 </style>
<div class="header-block">
	 <div class="header-icon-block header-block__item">
	 	<a href="/en/" class="social-link link-en"><div class="social-link__icon">EN</div></a>
	 	<a href="https://vk.com/nsau_press" class="social-link" title="����������� �������� Vkontakte" target="_blank">
	 		<img src="/themes/images/vk.png" alt="��������� ������������� ���" class="social-link__icon">
	 	</a>
	 	<a href="https://t.me/ngaulife" class="social-link" title="����������� �������� Telegram" target="_blank">
	 		<img src="/themes/images/telegram.png" alt="��������� ������������� ���" class="social-link__icon">
	 	</a>
	 	<a href="https://rutube.ru/channel/24117186/" class="social-link" title="����������� �������� Rutube" target="_blank">
	 		<img src="/themes/images/rutube.png" alt="Rutube ������������� ��� " class="social-link__icon">
	 	</a>
	 	<a href="/map/" class="social-link" title="����� �����">
	 		<img src="/themes/images/site-map.png" alt="����� �����" class="social-link__icon">
	 	</a>
	 	<a href="/vds/" class="social-link" itemprop="copy" title="������ ����� ��� ������������">
	 		<img src="/themes/images/eye.png" alt="������ ����� ��� ������������" class="social-link__icon">
	 	</a>
	 </div>
					
	<!-- ����� �� ����� -->
	<!--  -->
					<div  id="login_search" class="header__search header-block__item">
						<?php 	//if ($EE["unqueried_uri"] == "/" && !$Auth->logged_in) { ?>
							<!--a href="" /><div class="tasks_count warning"></div></a-->
						<form id="FormSearch" method="get" action="/search/" accept-charset="UTF-8">
						  <input type="search" name="search" placeholder="����� �� �����">
						  <button type="submit" class="submit" type="submit" value="������"></button>
						</form>
					
							
			    </div>
</div>
 
		    
		     </div>
		    <!--header-->
		    <!-- ������� ���� -->
		    	<div id="header_menu">
					<?php 	$NODEGROUP = "topmenu";
					        if ($EE["modules_data"][$NODEGROUP])
					            include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT;    
					?>
					<div id="menu_left_bg">&nbsp;</div>
				</div>	
		    <div id="content">
	
<?php 	if ($EE["modules_data"]["left_submenu"] || $EE["modules_data"]["left_top"]) { ?>
				<div class="left_cont"><!-- Left Cont -->
<?php		$NODEGROUP = "left_submenu";?>

<?
			if ($EE["modules_data"][$NODEGROUP]) { ?>
						<div class="submenu vertical_menu">
							
<?php		    include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT; ?>
							<div class="clear">&nbsp;</div>
						</div>
<?php		}
			$NODEGROUP = "left_top";
			if ($EE["modules_data"][$NODEGROUP])
			    include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT; 

	 		$NODEGROUP = "baners";
			if ($EE["modules_data"][$NODEGROUP]) { ?>
					<div id="baners">
<?php   	        include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT; ?>
					</div>
<?php   	}
			$NODEGROUP = "conference";
			if ($EE["modules_data"][$NODEGROUP]) { ?>
						<div  id="conference">
<?php		    include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT; ?>
							
						</div>
<?php		} 
			$NODEGROUP = "left_bottom";
			if ($EE["modules_data"][$NODEGROUP])
			    include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT; ?>
				</div><!-- End Left Cont -->
				<div class="center_cont">
					<div class="cn"><!-- Center Cont -->
					

<?php							
			$NODEGROUP = "submenu";
			if ($EE["modules_data"][$NODEGROUP]) { ?>
						<div class="submenu horisontal_menu">
							<!--div class="corner_TL">&nbsp;</div>
							<div class="corner_TR">&nbsp;</div>
							<div class="corner_BL">&nbsp;</div>
							<div class="corner_BR">&nbsp;</div-->
<?php		    include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT; ?>
							<div class="clear">&nbsp;</div>
						</div>
<?php		}
			$NODEGROUP = "news_announce";
			if ($EE["modules_data"][$NODEGROUP]) { ?>
						<div class="" id="news_annonse">
<?php			include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT; ?>
							
						</div>
<?php		}
			$NODEGROUP = "top_content";
			if ($EE["modules_data"][$NODEGROUP])
			    include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT;
			$NODEGROUP = "dissertation";
			if ($EE["modules_data"][$NODEGROUP]) { ?>
						<div class="shadow_rbg" id="dissertation">
<?php		    include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT; ?>
							<div class="shadow_tr">&nbsp;</div>
							<div class="shadow_b">
								<div class="shadow_br">&nbsp;</div>
								<div class="shadow_bl">&nbsp;</div>
							</div>
						</div>
<?php		} 
	 		$NODEGROUP = "topicality";
			if ($EE["modules_data"][$NODEGROUP]) { ?>
						<div id="topicality" class="shadow_rbg">
							<div id="topicality_cont">		
<?php		include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT; ?>
							</div>
							<div class="shadow_tr">&nbsp;</div>
							<div class="shadow_b">
								<div class="shadow_br">&nbsp;</div>
								<div class="shadow_bl">&nbsp;</div>
							</div>
						</div>
<?php		} 
			$NODEGROUP = "content";
			if ($EE["modules_data"][$NODEGROUP])
			    include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT;  

	 		 ?>
					</div>
				</div><!-- End Center Cont $EE["modules_data"]["people_menu"] || -->
				<div class="clear">&nbsp;</div>
			</div><!--content-->

<?php	} else {
			$NODEGROUP = "people_content";
			if ($EE["modules_data"][$NODEGROUP]) { ?>
				<div class="people_output">
	<?php		include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT; 
					$NODEGROUP = "people_menu";
					if ($EE["modules_data"][$NODEGROUP]) { ?>
						<div class="submenu people_menu fix_table">
							<!--div class="corner_TL">&nbsp;</div>
							<div class="corner_TR">&nbsp;</div>
							<div class="corner_BL">&nbsp;</div>
							<div class="corner_BR">&nbsp;</div-->
<?php	 			    include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT; ?>
							<div class="clear">&nbsp;</div>
						</div>
<?php 		}?>		
				</div>
<?php	}		
			
			$NODEGROUP = "lenivi_anton";
			if ($EE["modules_data"][$NODEGROUP]) { ?>
						<div class="lenivi anton">
<?php		    include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT; ?>
							<div class="clear">&nbsp;</div>
						</div>
<?php		}	
			
			
			$NODEGROUP = "submenu";
			if ($EE["modules_data"][$NODEGROUP]) { ?>
						<div class="submenu horisontal_menu">
							<!--div class="corner_TL">&nbsp;</div>
							<div class="corner_TR">&nbsp;</div>
							<div class="corner_BL">&nbsp;</div>
							<div class="corner_BR">&nbsp;</div-->
<?php		    include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT; ?>
							<div class="clear">&nbsp;</div>
						</div>
<?php		}			
			$NODEGROUP = "top_content";
			if ($EE["modules_data"][$NODEGROUP])
			    include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT;

			$NODEGROUP = "content";
			if ($EE["modules_data"][$NODEGROUP] && $EE["engine_uri"] !== '/speciality/')
			    include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT; ?>

                <div class="clear">&nbsp;</div>
		    </div><!--content-->

		<? } ?>

	<?
		$NODEGROUP = "footer_block";
			if ($EE["modules_data"][$NODEGROUP])
				include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT;
		?>
		</div>
		<? $NODEGROUP = "content";
			if ($EE["modules_data"][$NODEGROUP] && $EE["engine_uri"] == '/speciality/')
			include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT;

		?>
		<div id="footer-pusher">&nbsp;</div>
    	</div>
    	<!--nofooter -->
    	<div id="footer">    
			<div class="wrapper">
				<div id="main-counter">
				<?php
					if (0) {
				?>
				<!--LiveInternet counter--><script type="text/javascript"><!--
				document.write("<a href='http://www.liveinternet.ru/click' "+
				"target=_blank><img src='//counter.yadro.ru/hit?t23.1;r"+
				escape(document.referrer)+((typeof(screen)=="undefined")?"":
				";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
				screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
				";h"+escape(document.title.substring(0,80))+";"+Math.random()+
				"' alt='' title='LiveInternet: �������� ����� ����������� ��"+
				" �������' "+
				"border='0' width='88' height='15'><\/a>")
				//--></script><!--/LiveInternet-->
				<?php
					}
				?>
				</div>

				<div id="copyright">
					<div class="footer-left">
						&copy;&nbsp;1998-<?php echo date('Y') ?> ������������� ��������������� �������� ����������� <br>
						630039, �. �����������, ��. �����������, 160<br/>
						������������� � ����������� ��� <a href="http://nsau.edu.ru/images/about/smi_license.jpg">�� ���77-43853 �� 9 ������� 2011 ����.</a>
					</div>
					<div class="footer-right">
						<div class="age"><span class="age_limit">12+</span></div>
						<a href="/press-centr/"> �����-�����</a><br>
						<a href="/support/"> ������ ����������� ���������</a><br>
						<a href="/department/commission/">�������� ��������</a>
					</div>
				</div>
		</div>
	
    </div>
</body>
</html>

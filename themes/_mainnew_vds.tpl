<!DOCTYPE html> 
<!--[if lt IE 7 ]><html class="no-js ie6" lang="ru"><![endif]-->
<!--[if IE 7 ]><html class="no-js ie7" lang="ru"><![endif]-->
<!--[if IE 8 ]><html class="no-js ie8" lang="ru"><![endif]-->
<!--[if IE 9 ]><html class="no-js ie9" lang="ru"><![endif]-->
<!--[if (gt IE 7)|!(IE)]><html lang="ru"><!--<![endif]-->
<head>
	<title><?php include $EE["theme"] . "_page_title" . TEMPLATE_EXT ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=<?=CODEPAGE_HTML?>" />
    <meta name="description" content="<?=$EE["meta_descr"]?>" />
    <meta name="keywords" content="<?=$EE["meta_keywords"]?>" />
      <meta http-equiv="imagetoolbar" content="no" />	 
    <link rel="icon" href="<?=$EE["http_theme"]?>favicon2011.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="<?=$EE["http_theme"]?>favicon2011.ico" type="image/x-icon" />
		<meta name='yandex-verification' content='771e5652e16b9536' />
    <script type="text/javascript" src="<?=HTTP_COMMON_SCRIPTS?>modernizr-2.0.6.min.js"></script>
    	
	<link rel="stylesheet" href="<?=$EE["http_styles"]?>screen_vds.css?<?=filemtime($_SERVER['DOCUMENT_ROOT'].$EE['http_styles'].'screen_vds.css')?>" type="text/css" />
<?php	clearstatcache(); ?>
    <link rel="stylesheet" href="<?=$EE["http_styles"]?>ee.css" type="text/css" />
	<!--[if lte IE 8]><link rel="stylesheet" href="<?=$EE["http_styles"]?>ie-fix.css" type="text/css" /><![endif]-->
    <?php include $EE["theme"] . "_head_extra" . TEMPLATE_EXT ?>
	
	
	
	
	<link rel="stylesheet" type="text/css" href="<?=$EE["http_styles"]?>contentrotator.css" media="screen" />
<script type="text/javascript" src="<?=HTTP_COMMON_SCRIPTS?>jquery_min.js"></script>
<script src="<?=HTTP_COMMON_SCRIPTS?>jquery-ui-personalized-1.5.3.packed.js" type="text/javascript"></script>
<script src="<?=HTTP_COMMON_SCRIPTS?>jquery.cookie.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#rotator > ul").tabs({fx:{opacity: "toggle"}}).tabs("rotate", 4000, true);
		var classes = {"textNorm":"rollover1", "textMid":"rollover2", "textLarge":"rollover3", "backgroundWhite":"rollover4", "backgroundBlack":"rollover5", "backgroundBlue":"rollover6"};
		$("table.vds").find("p.t").live("click", function() {
			$("body").attr("class", "");
			$.cookie('textClass', '', { expires: 7, path: '/' });
			$.cookie('bodyColorClass', '', { expires: 7, path: '/' });
			$("a."+classes["backgroundWhite"]).css("background-position", "0 -31px");
			$("a.rollover2").css("background-position", "0 0px");
			$("a.rollover3").css("background-position", "0 0px");
			$("a.rollover5").css("background-position", "0 0px");
			$("a.rollover6").css("background-position", "0 0px");
			$("a."+classes["textNorm"]).css("background-position", "0 -31px");
		});
		$("table.vds").find("a:not(.justlink)").live("click", function() {
			$(this).css("background-position", "0 -31px");
			switch($(this).attr("class")) {
				case "rollover1": {
					$("body").addClass("textNorm");
					$("body").removeClass("textMid");
					$("body").removeClass("textLarge");
					$(this).css("background-position", "0 -31px");
					$("a.rollover2").css("background-position", "0 0px");
					$("a.rollover3").css("background-position", "0 0px");
					$.cookie('textClass', 'textNorm', { expires: 7, path: '/' });
				}
				break;
				case "rollover2": {
					$("body").addClass("textMid");
					$("body").removeClass("textNorm");
					$("body").removeClass("textLarge");
					$(this).css("background-position", "0 -31px");
					$("a.rollover1").css("background-position", "0 0px");
					$("a.rollover3").css("background-position", "0 0px");
					$.cookie('textClass', 'textMid', { expires: 7, path: '/' });
				}
				break;
				case "rollover3": {
					$("body").addClass("textLarge");
					$("body").removeClass("textMid");
					$("body").removeClass("textNorm");
					$(this).css("background-position", "0 -31px");
					$("a.rollover1").css("background-position", "0 0px");
					$("a.rollover2").css("background-position", "0 0px");
					$.cookie('textClass', 'textLarge', { expires: 7, path: '/' });
				}
				break;
				case "rollover4": {
					$("body").addClass("backgroundWhite");
					$("body").removeClass("backgroundBlack");
					$("body").removeClass("backgroundBlue");
					$(this).css("background-position", "0 -31px");
					$("a.rollover5").css("background-position", "0 0px");
					$("a.rollover6").css("background-position", "0 0px");
					$.cookie('bodyColorClass', 'backgroundWhite', { expires: 7, path: '/' });
				}
				break;
				case "rollover5": {
					$("body").addClass("backgroundBlack");
					$("body").removeClass("backgroundWhite");
					$("body").removeClass("backgroundBlue");
					$(this).css("background-position", "0 -31px");
					$("a.rollover4").css("background-position", "0 0px");
					$("a.rollover6").css("background-position", "0 0px");
					$.cookie('bodyColorClass', 'backgroundBlack', { expires: 7, path: '/' });
				}
				break;
				case "rollover6": {
					$("body").addClass("backgroundBlue");
					$("body").removeClass("backgroundWhite");
					$("body").removeClass("backgroundBlack");
					$(this).css("background-position", "0 -31px");
					$("a.rollover4").css("background-position", "0 0px");
					$("a.rollover5").css("background-position", "0 0px");
					$.cookie('bodyColorClass', 'backgroundBlue', { expires: 7, path: '/' });
				}
				break;
			}
			return false;
		});

		var bodyColor = $.cookie('bodyColorClass') == undefined ? "" : $.cookie('bodyColorClass');
		var textColor = ($.cookie('textClass') == undefined) ? "" : $.cookie('textClass');
		$("body").addClass(textColor);
		$("body").addClass(bodyColor);
		if(textColor.length!=0) {
			$("a."+classes[textColor]).css("background-position", "0 -31px");
		}
		else {
			$.cookie('textClass', '', { expires: 7, path: '/' });
			$("a.rollover1").css("background-position", "0 -31px");
		}
		if(bodyColor.length!=0) {
			$("a."+classes[bodyColor]).css("background-position", "0 -31px");
		}
		else {
			$("a.rollover4").css("background-position", "0 -31px");
			$.cookie('bodyColorClass', '', { expires: 7, path: '/' });
		}
	});
</script>



</head>
<body>
<? 
if ( (stristr($_SERVER['HTTP_USER_AGENT'], 'MSIE 6.0') !=false) ||(stristr($_SERVER['HTTP_USER_AGENT'], 'MSIE 7.0') != false)) {?>
<div id="no-ie6">
	<div id="no-ie6-img"></div>
	<p>�� ����������� ���������� ������� Internet Explorer <!--[if IE 6]>6<![endif]--><!--[if IE 7]>7<![endif]-->.
	��������� ������� <a href="http://www.microsoft.com/rus/windows/internet-explorer/">����������</a>.</p>
	<a id="no-ie6_close" href="#" title="�������">&times;</a>
</div>
<?php
}
include $EE["theme"] . "_cross_auth" . TEMPLATE_EXT ?>
    <div id="nofooter">
		<div class="menu_background">
			<table class="vds" cellpadding="0" cellspacing="0" border="0" width="900" height="100%" align=center>
<tr><td ><img src="/themes/images/spacer.gif" width="4" height="47"></td><td   width=100%>
<table class="vds"><tr>
<td><p class="t"><nobr>������ ������:</nobr></td>
<td><a href="../1" class="rollover1"></a></td>
<td><a href="../2" class="rollover2"></a></td>
<td><a href="../3" class="rollover3"></a></td>
<td width="100"></td>
<td><p class="t"><nobr>����� �����:</nobr></td>
<td><a href="../1" class="rollover4"></a></td>
<td><a href="../4" class="rollover5"></a></td>
<td><a href="../5" class="rollover6"></a></td>
<td width="195"></td>
<td><p class="t"><nobr>������� ������:</nobr></td>
<td><a class="justlink" href="/"><img src="/themes/images/home.png" border="0"></a></td>
</tr></table></td>
</tr></table>
			</div>
		<div class="wrapper">
		    <div id="header">
				<a id="logo" href="/">
					<span><span>&nbsp;</span></span>
				</a>

			<div id="page_title"><div>������������� ��������������� �������� �����������</div></div>
				
		
				
				
				
							
		    </div><!--header-->
		    <div id="content">
	
<?php 	if ($EE["modules_data"]["left_submenu"] || $EE["modules_data"]["left_top"]) { ?>
				<div class="left_cont"><!-- Left Cont -->
<?php		$NODEGROUP = "left_submenu";?>

<?
			if ($EE["modules_data"][$NODEGROUP]) { ?>
						<div class="submenu vertical_menu">
							<div class="corner_TL">&nbsp;</div>
							<div class="corner_TR">&nbsp;</div>
							<div class="corner_BL">&nbsp;</div>
							<div class="corner_BR">&nbsp;</div>
<?php		    include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT; ?>
							<div class="clear">&nbsp;</div>
						</div>
<?php		}
			$NODEGROUP = "left_top";
			if ($EE["modules_data"][$NODEGROUP])
			    include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT; 

	 		$NODEGROUP = "baners";
			if ($EE["modules_data"][$NODEGROUP]) { ?>
					<div id="baners">
<?php   	        include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT; ?>
					</div>
<?php   	}
			$NODEGROUP = "conference";
			if ($EE["modules_data"][$NODEGROUP]) { ?>
						<div class="shadow_rbg" id="conference">
<?php		    include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT; ?>
							<div class="shadow_tr">&nbsp;</div>
							<div class="shadow_b">
								<div class="shadow_br">&nbsp;</div>
								<div class="shadow_bl">&nbsp;</div>
							</div>
						</div>
<?php		} 
			$NODEGROUP = "left_bottom";
			if ($EE["modules_data"][$NODEGROUP])
			    include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT; ?>
				</div><!-- End Left Cont -->
				<div class="center_cont">
					<div class="cn"><!-- Center Cont -->
					

<?php							
			$NODEGROUP = "submenu";
			if ($EE["modules_data"][$NODEGROUP]) { ?>
						<div class="submenu horisontal_menu">
							<div class="corner_TL">&nbsp;</div>
							<div class="corner_TR">&nbsp;</div>
							<div class="corner_BL">&nbsp;</div>
							<div class="corner_BR">&nbsp;</div>
<?php		    include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT; ?>
							<div class="clear">&nbsp;</div>
						</div>
<?php		}
			$NODEGROUP = "news_announce";
			if ($EE["modules_data"][$NODEGROUP]) { ?>
						<div class="shadow_rbg" id="news_annonse">
<?php			include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT; ?>
							<div class="shadow_tr">&nbsp;</div>
							<div class="shadow_b">
								<div class="shadow_br">&nbsp;</div>
								<div class="shadow_bl">&nbsp;</div>
							</div>
						</div>
<?php		}
			$NODEGROUP = "top_content";
			if ($EE["modules_data"][$NODEGROUP])
			    include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT;
			$NODEGROUP = "dissertation";
			if ($EE["modules_data"][$NODEGROUP]) { ?>
						<div class="shadow_rbg" id="dissertation">
<?php		    include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT; ?>
							<div class="shadow_tr">&nbsp;</div>
							<div class="shadow_b">
								<div class="shadow_br">&nbsp;</div>
								<div class="shadow_bl">&nbsp;</div>
							</div>
						</div>
<?php		} 
	 		$NODEGROUP = "topicality";
			if ($EE["modules_data"][$NODEGROUP]) { ?>
						<div id="topicality" class="shadow_rbg">
							<div id="topicality_cont">		
<?php		include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT; ?>
							</div>
							<div class="shadow_tr">&nbsp;</div>
							<div class="shadow_b">
								<div class="shadow_br">&nbsp;</div>
								<div class="shadow_bl">&nbsp;</div>
							</div>
						</div>
<?php		} 
			$NODEGROUP = "content";
			if ($EE["modules_data"][$NODEGROUP])
			    include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT;  

	 		$NODEGROUP = "footer_block";
			if ($EE["modules_data"][$NODEGROUP])
				include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT; ?>
					</div>
				</div><!-- End Center Cont $EE["modules_data"]["people_menu"] || -->
<?php	} else {
			$NODEGROUP = "people_content";
			if ($EE["modules_data"][$NODEGROUP]) { ?>
				<div class="people_output">
	<?php		include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT; 
					$NODEGROUP = "people_menu";
					if ($EE["modules_data"][$NODEGROUP]) { ?>
						<div class="submenu people_menu fix_table">
							<div class="corner_TL">&nbsp;</div>
							<div class="corner_TR">&nbsp;</div>
							<div class="corner_BL">&nbsp;</div>
							<div class="corner_BR">&nbsp;</div>
<?php	 			    include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT; ?>
							<div class="clear">&nbsp;</div>
						</div>
<?php 		}?>		
				</div>
<?php	}		
			
			$NODEGROUP = "submenu";
			if ($EE["modules_data"][$NODEGROUP]) { ?>
						<div class="submenu horisontal_menu">
							<div class="corner_TL">&nbsp;</div>
							<div class="corner_TR">&nbsp;</div>
							<div class="corner_BL">&nbsp;</div>
							<div class="corner_BR">&nbsp;</div>
<?php		    include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT; ?>
							<div class="clear">&nbsp;</div>
						</div>
<?php		}			
			$NODEGROUP = "top_content";
			if ($EE["modules_data"][$NODEGROUP])
			    include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT; 
			$NODEGROUP = "content";
			if ($EE["modules_data"][$NODEGROUP])
			    include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT;
		} ?>
				<div class="clear">&nbsp;</div>
		    </div><!--content-->
			
			
			
			
		</div>
		<div id="footer-pusher">&nbsp;</div>
    </div><!--nofooter -->
    <div id="footer">    
		<div class="wrapper">
			<div id="main-counter">
			<?php
			if (0) {
			?>
			<!--LiveInternet counter--><script type="text/javascript"><!--
			document.write("<a href='http://www.liveinternet.ru/click' "+
			"target=_blank><img src='//counter.yadro.ru/hit?t23.1;r"+
			escape(document.referrer)+((typeof(screen)=="undefined")?"":
			";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
			screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
			";h"+escape(document.title.substring(0,80))+";"+Math.random()+
			"' alt='' title='LiveInternet: �������� ����� ����������� ��"+
			" �������' "+
			"border='0' width='88' height='15'><\/a>")
			//--></script><!--/LiveInternet-->
			<?php
			}
			?>
			</div>
		<div id="copyright">
			<div class="footer-left">630039, �. �����������, ��. �����������, 160<br/>
<a href="/department/commission/">�������� ��������:</a> ������� ������, 2 ����, ���. 235, +7 (383) 347-41-14<br/>
�������� �������� ������� ����� ��������: +7 (383) 267-38-04<br/>
������� ��������� �� �������������: +7 (383) 264-16-78<br/>
������ ������� � �����������: <a href="/entrant/answers/">�����������/</a><br/>
<a href="/entrant/answers/">������������</a> / <a href="/nir/depart_teaching/obschaya-inf/kontakty/">����������� </a><br/>
<span class="age_limit">12+</span>
</div>
			<div class="footer-right">
&copy;&nbsp;1998 - <?php echo date('Y') ?> ������������� ��������������� �������� �����������.
�� �������� ���������������� ������� ����������� � <a href="/support/"> ������ ����������� ���������</a>.
������������� � ����������� ��� <a href="http://nsau.edu.ru/images/about/smi_license.jpg">�� ���77-43853 �� 9 ������� 2011 ����.</a> ������ ����������� ������� �� ������� � ����� �����, �������������� ���������� � �������� ������������ (������������).<br/>
<a href="/directory/responsible/">������������� �� ����������</a> | <a href="/department/cit/spravki/">����������� ���� �������</a>

</div>
				<div></div>
			</div>
		</div>
<?php
if (!AT_HOME) {
?>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter7937029 = new Ya.Metrika({id:7937029, enableAll: true, webvisor:true});
        } catch(e) {}
    });
    
    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/7937029" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->	        
<?php
}
?>		
    </div><!--footer-->
</body>
</html>

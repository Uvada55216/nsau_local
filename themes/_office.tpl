<!DOCTYPE html> 
<!--[if lt IE 7 ]><html lang="ru" class="no-js ie6"><![endif]-->
<!--[if IE 7 ]><html lang="ru" class="no-js ie7"><![endif]-->
<!--[if IE 8 ]><html lang="ru" class="no-js ie8"><![endif]-->
<!--[if IE 9 ]><html lang="ru" class="no-js ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="ru" class="no-js"><!--<![endif]-->
<head>
	<title><?php include $EE["theme"] . "_page_title" . TEMPLATE_EXT ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=<?=CODEPAGE_HTML?>" />
    <meta name="description" content="<?=$EE["meta_descr"]?>" />
    <meta name="keywords" content="<?=$EE["meta_keywords"]?>" />
    <meta http-equiv="imagetoolbar" content="no" />

    <?	$NODEGROUP = "head_tag";
	if ($EE["modules_data"][$NODEGROUP]) { ?>
		<?include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT; ?>
	<?}	?>

    <link rel="icon" href="<?=$EE["http_theme"]?>favicon2011.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="<?=$EE["http_theme"]?>favicon2011.ico" type="image/x-icon" />
	
    <script type="text/javascript" src="<?=HTTP_COMMON_SCRIPTS?>modernizr-2.0.6.min.js"></script>
	
	<link rel="stylesheet" href="<?=$EE["http_styles"]?>screen_2011.css?<?=filemtime($_SERVER['DOCUMENT_ROOT'].$EE['http_styles'].'screen_2011.css')?>" type="text/css" />
	<?php	clearstatcache(); ?>
    <link rel="stylesheet" href="<?=$EE["http_styles"]?>ee.css" type="text/css" />
	<!--[if lte IE 9]><link rel="stylesheet" href="<?=$EE["http_styles"]?>ie-fix.css" type="text/css" /><![endif]-->
    <?php include $EE["theme"] . "_head_extra" . TEMPLATE_EXT ?>
</head>

<body>
<!--[if lte IE 7]><div id="no-ie6">
	<div id="no-ie6-img"></div>
	<p>�� ����������� ���������� ������� Internet Explorer <![endif]--><!--[if IE 6]>6<![endif]--><!--[if IE 7]>7<![endif]--><!--[if lte IE 7]>. ��������� ������� <a href="http://www.microsoft.com/rus/windows/internet-explorer/">����������</a>.</p>
	<a id="no-ie6_close" href="#" title="�������">&times;</a>
</div><![endif]-->
    <div id="nofooter">
		<div class="menu_background">
			<div class="office_menu">
			<?php 	//}
			$NODEGROUP = "login"; 
			 if ($EE["modules_data"][$NODEGROUP])
	            include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT;
			?>
			</div>
		</div>
		<div class="wrapper">
		    <div id="header" class="header">
<!-- ���� -->
				<a id="logo" class="header__logo" href="/">
					<span><span>&nbsp;</span></span>
				</a>
			<?php	$NODEGROUP = "daystogo";
				if ($EE["modules_data"][$NODEGROUP])
					include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT; 
			?>


<div class="header-block">
	 <div class="header-icon-block header-block__item">
	 		<a href="https://vk.com/nsau_press" class="social-link" target="_blank">
	 			<img src="/themes/images/vk.png" alt="��������� �����-����� �������������� ���" class="social-link__icon">
	 		</a>
	 		<a href="https://www.instagram.com/nsau_press/" class="social-link" target="_blank">
	 			<img src="/themes/images/instagram.png" alt="���������� �����-����� �������������� ���" class="social-link__icon" >
	 		</a>
	 		<a href="https://t.me/ngaulife" class="social-link" target="_blank">
	 			<img src="/themes/images/telegram.png" alt="��������� �����-����� �������������� ���" class="social-link__icon">
	 		</a>
	 		<a href="https://www.youtube.com/channel/UCl6aOEZT9TxibaXzK7zIYXA" class="social-link" target="_blank">
	 			<img src="/themes/images/youtube.png" alt="���� �����-����� �������������� ��� " class="social-link__icon">
	 		</a>
	 		<a href="/map/" class="social-link">
	 			<img src="/themes/images/site-map.png" alt="����� �����" class="social-link__icon">
	 		</a>
	 		<a href="/vds/" class="social-link" itemprop="copy">
	 			<img src="/themes/images/eye.png" alt="������ ����� ��� ������������" class="social-link__icon"></a>
	 	</div>
					
	<!-- ����� �� ����� -->
	<!--  -->
					<div  id="login_search" class="header__search header-block__item">
						<?php 	//if ($EE["unqueried_uri"] == "/" && !$Auth->logged_in) { ?>
							<!--a href="" /><div class="tasks_count warning"></div></a-->
						<form id="FormSearch" method="get" action="/search/" accept-charset="UTF-8">
						  <input type="search" name="search" placeholder="����� �� �����">
						  <button type="submit" class="submit" type="submit" value="������"></button>
						</form>
					
							
			    </div>
</div>
 
		    
		     </div>
		    <!--header-->
		    <!-- ������� ���� -->
		    	<div id="header_menu">
					<?php 	$NODEGROUP = "topmenu";
					        if ($EE["modules_data"][$NODEGROUP])
					            include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT;    
					?>
					<div id="menu_left_bg">&nbsp;</div>
				</div>	
		   
		    <div id="content" class="teacher_office">
<?php 	if ($EE["modules_data"]["left_top"] || $EE["modules_data"]["left_submenu"]) { ?>
				<div class="left_cont">
<?php		$NODEGROUP = "left_submenu";
			if ($EE["modules_data"][$NODEGROUP]) { ?>
						<div class="submenu vertical_menu">
							<div class="corner_TL">&nbsp;</div>
							<div class="corner_TR">&nbsp;</div>
							<div class="corner_BL">&nbsp;</div>
							<div class="corner_BR">&nbsp;</div>
<?php		    include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT; ?>
							<div class="clear">&nbsp;</div>
						</div>
<?php		}
			$NODEGROUP = "left_top";
			if ($EE["modules_data"][$NODEGROUP])
			    include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT; ?>
				</div>
				<div class="center_cont">
					<div class="cn">
<?php		$NODEGROUP = "submenu";
			if ($EE["modules_data"][$NODEGROUP]) { ?>
						<div class="submenu horisontal_menu">
							<div class="corner_TL">&nbsp;</div>
							<div class="corner_TR">&nbsp;</div>
							<div class="corner_BL">&nbsp;</div>
							<div class="corner_BR">&nbsp;</div>
<?php		    include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT; ?>
							<div class="clear">&nbsp;</div>
						</div>
<?php		}			
			$NODEGROUP = "content";
			if ($EE["modules_data"][$NODEGROUP])
			    include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT; ?>
					</div>
				</div>
<?php	} else {
			$NODEGROUP = "submenu";
			foreach($EE["modules_data"][$NODEGROUP] as $node) {
				if(count($node["output"]["items"])>0) {
					$showmenu = true;
					break;
				} else $showmenu = false;
			}
			
			if ($EE["modules_data"][$NODEGROUP] && $showmenu) { ?>
						<div class="submenu horisontal_menu">
							<div class="corner_TL">&nbsp;</div>
							<div class="corner_TR">&nbsp;</div>
							<div class="corner_BL">&nbsp;</div>
							<div class="corner_BR">&nbsp;</div>
<?php		    include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT; ?>
							<div class="clear">&nbsp;</div>
						</div>
<?php		}			
			$NODEGROUP = "content";
			if ($EE["modules_data"][$NODEGROUP])
			    include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT;
		} ?>
				<div class="clear">&nbsp;</div>
		    </div><!--content-->
		</div>
		<div id="footer-pusher">&nbsp;</div>
    </div><!--nofooter -->
   <div id="footer">    
		<div class="wrapper">
			<div id="main-counter">
			
			</div>
				<div id="copyright">
					<div class="footer-left">
						&copy;&nbsp;1998-<?php echo date('Y') ?> ������������� ��������������� �������� ����������� <br>
						630039, �. �����������, ��. �����������, 160<br/>
						������������� � ����������� ��� <a href="http://nsau.edu.ru/images/about/smi_license.jpg">�� ���77-43853 �� 9 ������� 2011 ����.</a>
					</div>
					<div class="footer-right">
						<div class="age"><span class="age_limit">12+</span></div>
						<a href="/press-centr/"> �����-�����</a><br>
						<a href="/support/"> ������ ����������� ���������</a><br>
						<a href="/about/department/commission/">�������� ��������</a>
					</div>
				</div>
		</div>
<?php
if (!AT_HOME) {
?>
        
<?php
}
?>		
    </div><!--footer-->
</body>
</html>

<table class="table-bordered" width="100%">
	<thead>
		<tr>
			<th width="2%" rowspan="2">�</th>
			<th width="15%" rowspan="2">���</th>
			<th width="20%" colspan="3">����� �� <br>����������� ���</th>
			<th width="20%" colspan="3">����� �� <br>��������� ����</th> 
			<th width="5%" rowspan="2">�������������� ����������</th>
			<th width="5%" rowspan="2">����� ������</th>
			<th width="5%" rowspan="2">�������� �� �����������</th>
			<th width="5%" rowspan="2">�������� �� ����������</th>
			<th width="18%" rowspan="2">��������</th>
			<th width="5%" rowspan="2">���-� � ����������</th>
		</tr>
		<tr>
			<?for($i=0; $i<2; $i++) {?>
				<?foreach ($moduleData['speciality']->exams as $key => $value) {?>
					<th width="5%"><?=$key[0]?></th>
				<?}?>
			<?}?>
		</tr> 
	</thead>
	
	<tbody>
		<?//���������� ����� �� ������ ��?>
			<?if($moduleData['speciality']->formEducation == '�����') {?>
				<?if($moduleData['speciality']->specialityCode == '35.03.06') {?>
					<tr class="separator">
						<td colspan="14"><strong>�� ��������������������� ����������</strong></td>
					</tr>		
					<tr class="greenLine">
						<td colspan="2">����� �������</td>
						<td colspan="11">-</td>
						<td>��������</td>
					</tr>
				<?}?>
				<?if($moduleData['speciality']->specialityCode == '20.03.02') {?>
					<tr class="separator">
						<td colspan="14"><strong>�� ��������������������� ����������</strong></td>
					</tr>		
					<tr class="greenLine">
						<td colspan="2">�������� ���� �����</td>
						<td colspan="11">-</td>
						<td>��������</td>
					</tr>
				<?}?>
			<?}?>
		<?//END?>
		<?foreach($moduleData['abits'] as $foundation => $abits) {?>
			<tr class="separator">
				<td colspan="14"><strong><?=$foundation?></strong></td>
			</tr>
			<?if(empty($abits)) {?>
				<tr>
					<td colspan="14"><p style="text-align: center; padding-bottom: 0px">��� �������� ���������</p></td>
				</tr>
			<?}?>
			<?foreach ($abits as $key => $Abit) {?>
				<tr class="<?=$Abit->styleClass?>">
					<td><?=++$j?></td>
					<td style="text-align: left"><a href="<?=$this->engineUri?>people/<?=$Abit->peopleId?>"><?=$Abit->name?></a></td>
					<?foreach ($moduleData['speciality']->exams as $key => $value) {?>
						<?if(!empty($Abit->ege[$key])) {?>
							<th width="5%"><?=$Abit->ege[$key]?></th>
						<?} else {?>
							<td>-</td>
						<?}?>
					<?}?>
					<?foreach ($moduleData['speciality']->exams as $key => $value) {?>
						<?if(!empty($Abit->exam[$key])) {?>
							<th width="5%"><?=$Abit->exam[$key]?></th>
						<?} else {?>
							<td>-</td>
						<?}?>
					<?}?>
					<td><?=$Abit->achievementsScore?></td>
					<td><?=$Abit->totalScore?></td>
					<td><?=($Abit->originalEducationDocuments) ? "<strong>��������</strong>" : "�����"?></td>
					<td><?=($Abit->agreement) ? "��" : "���"?></td>
					<td><?=$Abit->organization?></td>
					<td><?=$Abit->status?></td>
				</tr>
			<?}$j=0;?>
		<?}?>																						
	</tbody>
</table>
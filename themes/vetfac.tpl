<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">   
<!--[if lt IE 7 ]><html class="no-js ie6" xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru"><![endif]-->
<!--[if IE 7 ]><html class="no-js ie7" xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru"><![endif]-->
<!--[if IE 8 ]><html class="no-js ie8" xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru"><![endif]-->
<!--[if IE 9 ]><html class="no-js ie9" xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru"><!--<![endif]-->
<head>
	<title><?php include $EE["theme"] . "_page_title" . TEMPLATE_EXT ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=<?=CODEPAGE_HTML?>" />
    <meta http-equiv="Content-Language" content="<?=$EE["language"]?>" />
    <meta name="description" content="<?=$EE["meta_descr"]?>" />
    <meta name="keywords" content="<?=$EE["meta_keywords"]?>" />
    <meta name="author" content="Andrey Kudryashov (poganini): mail[sobachka]poganini.nsk.ru" />
    <meta http-equiv="imagetoolbar" content="no" />
    <link rel="icon" href="<?=$EE["http_theme"]?>favicon2011.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="<?=$EE["http_theme"]?>favicon2011.ico" type="image/x-icon" />    
    <script type="text/javascript" src="<?=HTTP_COMMON_SCRIPTS?>modernizr-2.0.6.min.js"></script>
    <link rel="stylesheet" href="<?=$EE["http_styles"]?>vetfac.css" type="text/css" media="screen" />    
    <link rel="stylesheet" href="<?=$EE["http_styles"]?>ee.css" type="text/css" media="screen" />
	
    <?php include $EE["theme"] . "_head_extra" . TEMPLATE_EXT ?>    
</head>        
<body>      
	<div id="nofooter">
		<div class="menu_background">
			<div class="office_menu">
		<?php 	//}
		$NODEGROUP = "login"; 
		 if ($EE["modules_data"][$NODEGROUP])
            include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT;
?>
		</div></div>
		<div id="shapka">
			<div id="top">
				<div id="part_left"></div>
			</div> 
			<div id="main_menu">
				<?php
					$NODEGROUP = "topmenu";
					if (($EE["modules_data"][$NODEGROUP]))
					    include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT;    
				?>
			</div>
			<div id="main_menu_left"></div>
			<div id="part_right"></div>                          
			<div id="top_img">
			    <a href="/vetfac/"><img class="footer_img" src="/themes/images/vetfac/home1.gif" alt="home" /></a>
				<a href="mailto:ivm_nsau@mail.ru"><img class="footer_img" src="/themes/images/vetfac/mail1.gif" alt="mail" /></a>
				<img class="footer_img" src="/themes/images/vetfac/map1.gif" alt="map" />
			</div>
			<a href="http://nsau.edu.ru/"><div id="ngau"></div></a>
			
						<div class="vsds" ><strong><a href="/vds/" itemprop="copy" >������ ��� ������������</a>
</strong> 
</div>
			<div id="three_color"></div>
	
			<div id="vet_med">�������� ������������ �������� � �������������</div>
		</div> 
		<div id="left_colomn">
		<!-- ������� ��������� -->
            <div id="razdely">
                <div id="left_blue_head">
                    <div class="white_text">�������</div>
                </div>
                <div id="right_blue_head"></div>
                <div id="left_bottom"></div>
                <div id="right_bottom"></div>
				<?php
					$NODEGROUP = "left_top";
					if (($EE["modules_data"][$NODEGROUP]))
					    include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT;    
				?>                
            </div>
		</div>
		<div id="content-wrapper">
        <div id="content">
        <!-- �������� ������� -->
<!--         <div id="search">����� �� �����: <input type="text" size="50" class="search_input" />    <input type="image" src="/themes/images/button.gif" class="button" /></div> -->
        
        <div id="right_area">
        <!--
        <div id="auth">
            <div class="left_blue_head2"><div class="white_text">�����������</div></div>
            <div class="right_blue_head2"></div>
            <div class="left_bottom2"></div>
            <div class="right_bottom2"></div>
            <p>���� ��� ��������� � ����������� ���������</p>
            <p class="auth_p">�����&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="login" class="search_input_auth" /></p>
            <p class="auth_p">������&nbsp;&nbsp;&nbsp;<input type="text" name="password" class="search_input_auth" /></p>
            <input type="image" src="/themes/images/vetfac/button_enter.gif" id="button_enter" />
        </div>
        <div id="contacts">
            <div class="left_blue_head2"><div class="white_text">��������</div></div>
            <div class="right_blue_head2"></div>
            <div class="left_bottom2"></div>
            <div class="right_bottom2"></div>
            <img src="/themes/images/vetfac/phone.gif" alt="phone" id="phone" />
            <p id="adress">630036, �����������,<br />
            ��.&nbsp;�����������, 160<br />
            <strong>���: (383)&nbsp;267-09-07</strong></p>
            <p id="email">E-mail: <strong><a href="mailto:ivm_nsau@mail.ru">ivm_nsau@mail.ru</a></strong></p>
        </div>-->
        </div>
        
        
        
        <div id="left_area">
        
        <?php
			$NODEGROUP = "submenu";
		    if (($EE["modules_data"][$NODEGROUP])) { ?>
						<div class="submenu horisontal_menu">
							<div class="corner_TL">&nbsp;</div>
							<div class="corner_TR">&nbsp;</div>
							<div class="corner_BL">&nbsp;</div>
							<div class="corner_BR">&nbsp;</div>
<?php			include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT; ?>
							<div class="clear">&nbsp;</div>
						</div>
<?php	    }
			$NODEGROUP = "content";
		    if (($EE["modules_data"][$NODEGROUP]))
		    include $EE["theme"] . "_nodegroup" . TEMPLATE_EXT;    
		?>
<!--        <div class="center_field">
            <div class="left_blue_head2"><div class="white_text">������� ���������</div></div>
            <div class="right_blue_head2"></div>
            <div class="left_bottom2"></div>
            <div class="right_bottom2"></div>
            <h2>30 ��� ���������� - ��������� ������������ ��������</h2>
            <div class="news_item_text"><p>28 ������ 2009 �. � ������� ���� �������� ������� (��. �����������, 160) ������� ������������� �����������, ����������� 30-����� ��������� ������������ �������� � ������� �����������. 
            ����������� � ���������������� ��� ���������� ������� ������� � ������������� ������������.������� � ������������� ������������.</p>
            <p class="news_item_public">������������ 28.11.2009</p>
            <p class="more">����������� <a href="/"><img src="/themes/images/vetfac/more.gif" alt="�����������" /></a></p>
            </div>
            
            <h2>������ ��������� ���������</h2>
            <div class="news_item_text"><p>�� ��������� ������� ������ ������������, ������������ �������, ���������� ��������� ������������ �������� ������� �������� ����� �������. ����������� �� � ��������� � ������ ������� � �������� ���������.</p>
            <p class="news_item_public">������������ 28.11.2009</p>
            <p class="more">����������� <a href="/"><img src="/themes/images/vetfac/more.gif" alt="�����������" /></a></p>
            </div>
            
        </div> -->
        </div>
        <div class="pusher"></div>
        
        </div>
    </div>
    
    <div id="footer-pusher"></div>
</div>     
 <div id="footer">    
		<div class="wrapper">
			
		<div id="copyright">
        
			<div class="footer-left">630039, �. �����������, ��. �����������, 160<br/>
                <a href="/about/department/commission/">�������� ��������:</a> ������� ������, 2 ����, ���. 235, +7 (383) 347-41-14<br/>
                                                        �������� �������� ������� ����� ��������: +7 (383) 267-38-04<br/>
                                                        ������� ��������� �� �������������: +7 (383) 264-16-78<br/>
                                                        ������ ������� � �����������: <a href="/entrant/answers/">����������� </a>/<br/>
                <a href="/entrant/answers/">������������</a> / <a href="/nir/depart_teaching/obschaya-inf/kontakty/">����������� </a><br/>
                <span class="age_limit">12+</span>
            </div>

			<div class="footer-right">
&copy;&nbsp;1998 - <?php echo date('Y') ?> ������������� ��������������� �������� �����������.
�� �������� ���������������� ������� ����������� � <a href="/support/"> ������ ����������� ���������</a>.
������������� � ����������� ��� <a href="http://nsau.edu.ru/images/about/smi_license.jpg">�� ���77-43853 �� 9 ������� 2011 ����.</a> ������ ����������� ������� �� ������� � ����� �����, �������������� ���������� � �������� ������������ (������������).<br/>
<a href="/directory/responsible/">������������� �� ����������</a> | <a href="/department/cit/spravki/">����������� ���� �������</a>

</div>
				
			</div>
		</div>
<?php
if (!AT_HOME) {
?>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter7937029 = new Ya.Metrika({id:7937029, enableAll: true, webvisor:true});
        } catch(e) {}
    });
    
    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/7937029" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->	        
<?php
}
?>		
    </div><!--footer-->
</body>
</html>
